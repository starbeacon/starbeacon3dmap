﻿/*
 * @author Valentin Simonov / http://va.lent.in/
 */

using UnityEngine;
using TouchScript.Gestures.TransformGestures;

namespace TouchScript.Examples.CameraControl
{
    /// <exclude />
    public class CameraController : MonoBehaviour
    {
        public ScreenTransformGesture TwoFingerMoveGesture;
        public ScreenTransformGesture ManipulationGesture;
        public float PanSpeed = 200f;
        public float RotationSpeed = 200f;
        public float ZoomSpeed = 10f;

        private Transform pivot;
        private Transform cam;
        private void Awake()
        {
            pivot = transform.Find("Pivot");
            cam = transform.Find("Pivot/Camera");
        }

        private void OnEnable()
        {
            TwoFingerMoveGesture.Transformed += twoFingerTransformHandler;
            ManipulationGesture.Transformed += manipulationTransformedHandler;
        }

        private void OnDisable()
        {
            TwoFingerMoveGesture.Transformed -= twoFingerTransformHandler;
            ManipulationGesture.Transformed -= manipulationTransformedHandler;
        }

        private void manipulationTransformedHandler(object sender, System.EventArgs e)
        {
            var rotation = Quaternion.Euler(ManipulationGesture.DeltaPosition.y / Screen.height * RotationSpeed,
                -ManipulationGesture.DeltaPosition.x / Screen.width * RotationSpeed,
                0);//ManipulationGesture.DeltaRotation);
            var destRotation = pivot.localRotation * rotation;

            destRotation = Quaternion.Euler(ClampAngle(destRotation.eulerAngles.x, -15f, 40f),
                destRotation.eulerAngles.y,
                destRotation.eulerAngles.z);

            pivot.localRotation = destRotation; //*= rotation;

            cam.transform.localPosition += Vector3.forward*(ManipulationGesture.DeltaScale - 1f)*ZoomSpeed;
        }

        private void twoFingerTransformHandler(object sender, System.EventArgs e)
        {
            pivot.localPosition += pivot.rotation*TwoFingerMoveGesture.DeltaPosition*PanSpeed;
        }

        private float ClampAngle(float angle, float min, float max) {
            if (angle < 90 || angle > 270) {       // if angle in the critic region...
                if (angle > 180) angle -= 360;  // convert all angles to -180..+180
                if (max > 180) max -= 360;
                if (min > 180) min -= 360;
            }
            angle = Mathf.Clamp(angle, min, max);
            if (angle < 0) angle += 360;  // if angle negative, convert to 0..360
            return angle;
        }

        //public float ClampAngle(float angle, float min, float max) {
        //    angle = Mathf.Repeat(angle, 360);
        //    min = Mathf.Repeat(min, 360);
        //    max = Mathf.Repeat(max, 360);
        //    bool inverse = false;
        //    var tmin = min;
        //    var tangle = angle;
        //    if (min > 180) {
        //        inverse = !inverse;
        //        tmin -= 180;
        //    }
        //    if (angle > 180) {
        //        inverse = !inverse;
        //        tangle -= 180;
        //    }
        //    var result = !inverse ? tangle > tmin : tangle < tmin;
        //    if (!result)
        //        angle = min;

        //    inverse = false;
        //    tangle = angle;
        //    var tmax = max;
        //    if (angle > 180) {
        //        inverse = !inverse;
        //        tangle -= 180;
        //    }
        //    if (max > 180) {
        //        inverse = !inverse;
        //        tmax -= 180;
        //    }

        //    result = !inverse ? tangle < tmax : tangle > tmax;
        //    if (!result)
        //        angle = max;
        //    return angle;
        //}

    }
}