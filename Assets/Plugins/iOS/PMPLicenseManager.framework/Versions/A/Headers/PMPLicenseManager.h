//
//  SPLicenseManager.h
//  SPLicenseManager
//
//  Created by KY Wong on 14/12/2015.
//  Copyright © 2015 Cherrypicks Alpha Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum PMPLicenseValidationError {
    PMPLicenseValidationErrorAppIDNotFound,
    PMPLicenseValidationErrorBundleIDInvalid,
    PMPLicenseValidationErrorLicenceInvalid,
    PMPLicenseValidationErrorLicenseExpired,
    PMPLicenseValidationErrorNetworkError,
    PMPLicenseValidationErrorUnknown
} PMPLicenseValidationError;

typedef void (^OnSuccessValidation)();
typedef void (^OnFailValidation)(PMPLicenseValidationError error);

@interface PMPLicenseManager : NSObject
@property (nonatomic, strong) NSString* mapSDKVersion;
@property (nonatomic, strong, readonly) NSString* version;
@property (nonatomic, assign) BOOL onlineMode;
@property (nonatomic, assign) BOOL enableLog;
+ (id)shared;
- (void)beginValidation:(OnSuccessValidation)didSuccess didFail:(OnFailValidation)didFail;
@end
