//
//  PMPUserSavedFlightResponse.h
//
//  Created by   on 6/3/2017
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMPSavedFlight.h"


@interface PMPUserSavedFlightResponse : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) PMPSavedFlight *result;
@property (nonatomic, strong) NSString *errorMessage;
@property (nonatomic, strong) NSString *errorCode;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end
