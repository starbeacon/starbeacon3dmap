#import "DataModels.h"



// In this header, you should import all the public headers of your framework using statements like #import <SPServerManager/PublicHeader.h>

/*
typedef NS_ENUM(NSUInteger, PMPProximityServerType) {
    PMPServerType_HKIA_PROXIMITY_DEV,
    PMPServerType_HKIA_PROXIMITY_SIT,
    PMPServerType_HKIA_PROXIMITY_PROD
};*/

@interface PMPProximityServerManager : NSData


+ (PMPProximityServerManager *)sharedInstance;
@property (nonatomic, strong) PMPSaveUserLocationResponse* lastSaveUserLocationResponse;
@property (nonatomic, strong) PMPUserSavedFlightResponse* lastUserSavedFlightResponse;
//@property (nonatomic, assign) PMPProximityServerType serverType;
//String mtelUserId, String pushToken, String AAZoneId
- (void)saveUserLocation:(int)AAZoneId success:(void (^)(id response))success failure:(void (^)())failure;
- (void)saveUserTokenWithMtelUserId:(NSString*)mtelUserId pushToken:(NSString*)pushToken language:(NSInteger)language success:(void (^)(id response))success failure:(void (^)())failure;
- (BOOL)saveUserFlightWithMtelUserId:(NSString*)mtelUserId pushToken:(NSString*)pushToken mtelRecordId:(NSString*)mtelRecordId flightNo:(NSString*)flightNo flightPrefferedId:(NSString*)flightPrefferedId flightDate:(NSString*)flightDate flightTime:(NSString*)flightTime isArrival:(NSString*)isArrival enable:(NSInteger)enable gid:(NSString*)gid landID:(NSInteger)landID;
- (void)getUserSavedFlightWithSuccess:(void (^)(PMPUserSavedFlightResponse *response))success failure:(void (^)())failure;


- (NSString*)serverURL;

@end
