//
//  PMPDevices.h
//
//  Created by   on 23/5/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PMPDeviceOptions;

@interface PMPDevices : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double devicesIdentifier;
@property (nonatomic, assign) double x;
@property (nonatomic, strong) PMPDeviceOptions *deviceOptions;
@property (nonatomic, assign) double y;
@property (nonatomic, assign) double z;
@property (nonatomic, assign) double bleZonalPushUuidId;
@property (nonatomic, assign) double mapDeviceTypeId;
@property (nonatomic, assign) double deviceGroupId;
@property (nonatomic, strong) NSString *deviceName;
@property (nonatomic, assign) double majorNo;
@property (nonatomic, assign) double mapId;
@property (nonatomic, assign) double bleWayFindingUuidId;
@property (nonatomic, assign) double status;
@property (nonatomic, assign) double aaZoneId;
@property (nonatomic, assign) double zoneId;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
