//
//  PMPMapImage.h
//
//  Created by   on 6/3/2017
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPMapImage : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double width;
@property (nonatomic, assign) double threshold;
@property (nonatomic, assign) double zoomLevel;
@property (nonatomic, assign) double height;
@property (nonatomic, strong) NSString *version;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
