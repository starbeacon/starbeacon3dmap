//
//  PMPBusinessHours.h
//
//  Created by Andrew Man on 7/8/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPBusinessHours : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double languageId;
@property (nonatomic, strong) NSString *content;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
