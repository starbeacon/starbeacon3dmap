//
//  PMPPois+Extension.h
//  PMPServerManager
//
//  Created by KY Wong on 20/5/2016.
//  Copyright © 2016 Cherrypicks Alpha Ltd. All rights reserved.
//

#import <PMPServerManager/PMPServerManager.h>

@interface PMPPois (Extension)
@property (nonatomic, strong, readonly) NSArray *nodeIDs ;
@end
