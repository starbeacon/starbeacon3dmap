//
//  PMPPromotions.h
//
//  Created by   on 23/5/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, PMPPromotionMessageType) {
    PMPPromotionMessageType_NONE = 0,
    PMPPromotionMessageType_WelcomeMesaage,
    PMPPromotionMessageType_WelcomeMesaageSeaToAir,
    PMPPromotionMessageType_WelcomeMessageTransfer,
    PMPPromotionMessageType_WelcomeMessageArrival,
    PMPPromotionMessageType_LastAELNotification,
    PMPPromotionMessageType_AELnotOperating
};

@interface PMPPromotions : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *endAt;
@property (nonatomic, strong) NSString *startAt;
@property (nonatomic, assign) double promotionsIdentifier;
@property (nonatomic, assign) double promotionType;
@property (nonatomic, assign) double referenceId;
@property (nonatomic, strong) NSArray *referenceIds;
@property (nonatomic, assign) double deviceGroupId;
@property (nonatomic, assign) double activeRadius;
@property (nonatomic, strong) NSArray *message;
@property (nonatomic, strong) NSString *pinImage;
@property (nonatomic, strong) NSString *referenceType;
@property (nonatomic, strong) NSString *bannerIcon;
@property (nonatomic, strong) NSString *externalId;
@property (nonatomic) BOOL showInNotificationCenter;
@property (nonatomic, strong) NSArray* actionUrl;
@property (nonatomic, assign) double messageType;
@property (nonatomic, copy) NSArray* promotionMessages;
@property (nonatomic, strong) NSArray *deviceMajors;

@property (nonatomic, assign) NSInteger receivedMajor;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
