//
//  PMPResponse.h
//
//  Created by   on 10/8/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPResponse : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double mapScale;
@property (nonatomic, assign) double mapLat;
@property (nonatomic, strong) NSArray *mapDeviceTypes;
@property (nonatomic, strong) NSArray *maps;
@property (nonatomic, strong) NSArray *pathTypes;
@property (nonatomic, strong) NSArray *customStepMessages;
@property (nonatomic, strong) NSArray *pois;
@property (nonatomic, strong) NSArray *uuids;
@property (nonatomic, strong) NSArray *notifications;
@property (nonatomic, strong) NSArray *aosChangeFloorFilters;
@property (nonatomic, strong) NSArray *stepMessages;
@property (nonatomic, assign) double zoneDidChangeLoggingTimeout;
@property (nonatomic, strong) NSArray *aaZones;
@property (nonatomic, strong) NSArray *iosChangeFloorFilters;
@property (nonatomic, strong) NSArray *zones;
@property (nonatomic, assign) double mapNumSearchPaths;
@property (nonatomic, strong) NSArray *devices;
@property (nonatomic, strong) NSArray *nodes;
@property (nonatomic, assign) double pushMessageTimeout;
@property (nonatomic, strong) NSArray *promotions;
@property (nonatomic, assign) double mapDirection;
@property (nonatomic, assign) double errorCode;
@property (nonatomic, strong) NSArray *markers;
@property (nonatomic, strong) NSArray *paths;
@property (nonatomic, assign) double zoneLoggingTimeout;
@property (nonatomic, strong) NSArray *poiCategories;
@property (nonatomic, strong) NSString *tileUrlPrefix;
@property (nonatomic, assign) double mapLng;
@property (nonatomic, assign) double mapNumRecentSearch;
@property (nonatomic, assign) double mapDefaultZoom;
@property (nonatomic, strong) NSArray *brands;
@property (nonatomic, strong) NSArray *tags;
@property (nonatomic, strong) NSArray *areas;
@property (nonatomic, strong) NSArray *gateIds;
@property (nonatomic, strong) NSArray* deviceGroups;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
