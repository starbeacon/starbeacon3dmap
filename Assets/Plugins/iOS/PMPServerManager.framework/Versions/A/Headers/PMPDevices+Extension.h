//
//  PMPDevices+Extension.h
//  PMPServerManager
//
//  Created by KY Wong on 20/5/2016.
//  Copyright © 2016 Cherrypicks Alpha Ltd. All rights reserved.
//

#import <PMPServerManager/PMPServerManager.h>
#import "PMPDevices.h"

@interface PMPDevices (Extension)

@property (nonatomic, strong, readonly) NSString *bleWayFindingUuid;
@property (nonatomic, strong, readonly) NSString *bleZonalPushUuid;
@property (nonatomic, assign) double threshold;
@property (nonatomic, assign) double status;

- (NSString*)bleWayFindingUuidFilterByUuids:(NSArray*)uuids;
- (NSString*)bleZonalPushUuidFilterByUuids:(NSArray*)uuids;
@end
