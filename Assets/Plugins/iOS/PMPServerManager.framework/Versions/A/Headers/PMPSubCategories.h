//
//  PMPSubCategories.h
//
//  Created by andrew  on 11/11/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPSubCategories : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double subCategoriesIdentifier;
@property (nonatomic, strong) NSArray *name;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
