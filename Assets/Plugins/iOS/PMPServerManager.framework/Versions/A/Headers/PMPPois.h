//
//  PMPPois.h
//
//  Created by   on 16/8/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPPois : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double mapLng;
@property (nonatomic, strong) NSArray *location;
@property (nonatomic, assign) BOOL notCovered;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *shopUrl;
@property (nonatomic, strong) NSArray *tags;
@property (nonatomic, strong) NSArray *poiCategoryIds;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *telephone;
@property (nonatomic, assign) double mapId;
@property (nonatomic, strong) NSArray *name;
@property (nonatomic, strong) NSArray *businessHours;
@property (nonatomic, strong) NSString *markerImage;
@property (nonatomic, assign) double poisIdentifier;
@property (nonatomic, assign) double x;
@property (nonatomic, strong) NSArray *nodeIds;
@property (nonatomic, assign) double mapZoomMax;
@property (nonatomic, assign) double y;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, assign) double z;
@property (nonatomic, strong) NSString *externalId;
@property (nonatomic, assign) double zIndex;
@property (nonatomic, assign) double mapLat;
@property (nonatomic, strong) NSArray *lastStepMessage;
@property (nonatomic, assign) double mapZoomMin;
@property (nonatomic, strong) NSArray *poisDescription;
@property (nonatomic, assign) double brandIdentifier;
@property (nonatomic, assign) BOOL restricted;
@property (nonatomic, assign) double areaIdentifier;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;
- (NSString*)prepareCalling;

@end
