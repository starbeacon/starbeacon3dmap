//
//  PMPDetails.h
//
//  Created by   on 23/5/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPDetails : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double languageId;
@property (nonatomic, strong) NSString *content;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;
+ (instancetype)detailWithLangId:(double)langId content:(NSString*)content;
@end
