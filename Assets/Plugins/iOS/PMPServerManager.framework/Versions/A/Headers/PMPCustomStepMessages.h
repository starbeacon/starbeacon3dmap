//
//  PMPCustomStepMessages.h
//
//  Created by   on 23/5/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPCustomStepMessages : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double customStepMessagesIdentifier;
@property (nonatomic, strong) NSArray *paths;
@property (nonatomic, strong) NSArray *message;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
