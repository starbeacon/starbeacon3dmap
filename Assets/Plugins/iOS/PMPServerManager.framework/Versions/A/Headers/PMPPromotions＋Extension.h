//
//  PMPPromotions＋Extension.h
//  PMPServerManager
//
//  Created by KY Wong on 20/5/2016.
//  Copyright © 2016 Cherrypicks Alpha Ltd. All rights reserved.
//

#import <PMPServerManager/PMPServerManager.h>
#import "PMPPromotions.h"

typedef enum {
    PMPPromotionType_ZONAL = 1,
    PMPPromotionType_POI,
    PMPPromotionType_DEVICE,
} PMPPromotionType;

@interface PMPPromotions (Extension)
+ (PMPPromotions *)findPromotionByProximityID:(int)proximityId;
+ (PMPPromotions *)findPromotionByPOIID:(int)poiID;
@end
