//
//  SDKVersion.h
//
//  Created by KY Wong on 17/12/2015.
//  Copyright © 2015 Cherrypicks Alpha Ltd. All rights reserved.
//

#define SDK_VERSION_SERVER_MANAGER          @"1.0.0a"
#define SDK_VERSION_BEACON_MANAGER          @"1.0.0a"
#define SDK_VERSION_PROXIMITY_MANAGER       @"1.0.0a"
#define SDK_VERSION_LOCATION_MANAGER        @"1.0.0a"
#define SDK_VERSION_LICENSE_MANAGER         @"1.0.0a"
#define SDK_VERSION                         @"1.0.0a"