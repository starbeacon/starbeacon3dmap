//
//  PMPSavedFlight.h
//
//  Created by   on 6/3/2017
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPSavedFlight : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *preferredIdentifier;
@property (nonatomic, strong) NSString *gateCode;
@property (nonatomic, assign) double travelTime;
@property (nonatomic, strong) NSString *gate;
@property (nonatomic, strong) NSString *flightNo;
@property (nonatomic, assign) double bestOfTime;
@property (nonatomic, strong) NSString *transferDesk;
@property (nonatomic, strong) NSString *aisle;
@property (nonatomic, strong) NSString *mtelRecordId;
@property (nonatomic, strong) NSString *flightStatus;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;
- (NSString*)departureTime;

@end
