//
//  PMPBrands.h
//
//  Created by andrew  on 11/11/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPBrands : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *poiCategoryIds;
@property (nonatomic, assign) double brandsIdentifier;
@property (nonatomic, strong) NSArray *name;
@property (nonatomic, strong) NSArray *eshopUrl;
@property (nonatomic, strong) NSArray *brandsDescription;
@property (nonatomic, copy) NSString* externalId;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
