//
//  PMPDeviceOptions.h
//
//  Created by   on 23/5/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPDeviceOptions : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *outdoorPathRatio;
@property (nonatomic, strong) NSString *nearbyOutdoorNodes;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
