//
//  PMPAreas.h
//
//  Created by andrew  on 11/11/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPAreas : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double areasIdentifier;
@property (nonatomic, strong) NSArray *name;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
