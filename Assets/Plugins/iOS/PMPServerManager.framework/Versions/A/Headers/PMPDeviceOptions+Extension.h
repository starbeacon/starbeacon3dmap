//
//  PMPDeviceOptions+Extension.h
//  PMPServerManager
//
//  Created by KY Wong on 24/5/2016.
//  Copyright © 2016 Cherrypicks Alpha Ltd. All rights reserved.
//

#import <PMPServerManager/PMPServerManager.h>
#import "PMPDeviceOptions.h"

@interface PMPDeviceOptions (Extension)

@property (nonatomic, strong, readonly) NSArray* nearbyOutdoorNodeIds;

@end
