//
//  SPResponseData.h
//  SPServerManager
//
//  Created by KY Wong on 16/12/2015.
//  Copyright © 2015 Cherrypicks Alpha Ltd. All rights reserved.
//
#import "PMPResponse.h"

@interface PMPResponseData : PMPResponse
@property (nonatomic, strong, readonly) NSArray *beacons_push;
@property (nonatomic, strong, readonly) NSArray *beacons_positioning;
//@property (nonatomic, strong, readonly) NSArray *beacons_entrance;
@property (nonatomic, strong, readonly) NSDictionary* areaDict;
@property (nonatomic, strong, readonly) NSDictionary* deviceGroupDict;
@property (nonatomic, strong, readonly) NSDictionary* deviceDictByMajor;
@property (nonatomic, strong, readonly) NSArray* flattenPoiCatagories;
@property (nonatomic, strong, readonly) NSDictionary* poiCategoriesDict;
@property (nonatomic, strong, readonly) NSDictionary* brandDict;
@property (nonatomic, strong, readonly) NSDictionary* transportationDict;
@end
