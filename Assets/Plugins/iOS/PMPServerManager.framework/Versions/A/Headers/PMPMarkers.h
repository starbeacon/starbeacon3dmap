//
//  PMPMarkers.h
//
//  Created by   on 23/5/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPMarkers : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double mapId;
@property (nonatomic, strong) NSArray *content;
@property (nonatomic, assign) double x;
@property (nonatomic, assign) double y;
@property (nonatomic, assign) double markersIdentifier;
@property (nonatomic, assign) double mapZoomMax;
@property (nonatomic, assign) double z;
@property (nonatomic, assign) double mapZoomMin;
@property (nonatomic, assign) id url;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
