//
//  PMPMaps.h
//
//  Created by   on 14/7/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPMaps : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double mapsIdentifier;
@property (nonatomic, strong) NSString *pathColor;
@property (nonatomic, strong) NSString *backgroundColor;
@property (nonatomic, strong) NSArray *duplicatedTileIds;
@property (nonatomic, assign) BOOL defaultProperty;
@property (nonatomic, assign) double width;
@property (nonatomic, assign) BOOL staticMap;
@property (nonatomic, assign) double zoomMin;
@property (nonatomic, assign) double zoomMax;
@property (nonatomic, assign) double height;
@property (nonatomic, assign) double tileSize;
@property (nonatomic, strong) NSString *tileVersion;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, copy) NSArray* images;
@property (nonatomic, assign) double defaultLocationX;
@property (nonatomic, assign) double defaultLocationY;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
