//
//  PMPResult.h
//
//  Created by   on 28/2/2017
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPResult : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double promotionDisplayUntilTimestamp;
@property (nonatomic, assign) double bestOfTime;
@property (nonatomic, assign) double travelTime;
@property (nonatomic, strong) NSString *gate;
@property (nonatomic, strong) NSString *flightNo;
@property (nonatomic, strong) NSString *std;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
