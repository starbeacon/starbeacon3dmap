//
//  PMPStepMessages.h
//
//  Created by   on 23/5/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPStepMessages : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double stepMessagesIdentifier;
@property (nonatomic, strong) NSArray *message;
@property (nonatomic, strong) NSString *updatedAt;
@property (nonatomic, strong) NSString *createdAt;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
