//
//  PMPSaveUserLocationResponse.h
//
//  Created by   on 28/2/2017
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PMPResult;

@interface PMPSaveUserLocationResponse : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) PMPResult *result;
@property (nonatomic, strong) NSString *errorMessage;
@property (nonatomic, strong) NSString *errorCode;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
