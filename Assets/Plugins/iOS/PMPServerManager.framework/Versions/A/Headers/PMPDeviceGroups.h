//
//  PMPDeviceGroups.h
//
//  Created by andrew  on 8/12/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPDeviceGroups : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double deviceGroupsIdentifier;
@property (nonatomic, assign) NSInteger areaId;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
