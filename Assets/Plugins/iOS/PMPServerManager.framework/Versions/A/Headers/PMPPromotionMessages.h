//
//  PMPPromotionMessages.h
//
//  Created by   on 8/3/2017
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPPromotionMessages : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *message;
@property (nonatomic, strong) NSArray *details;
@property (nonatomic, strong) NSArray *actionUrls;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
