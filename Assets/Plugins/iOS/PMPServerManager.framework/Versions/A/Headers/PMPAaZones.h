//
//  PMPAaZones.h
//
//  Created by   on 2/3/2017
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPAaZones : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double aaZonesIdentifier;
@property (nonatomic, strong) NSString *zoneName;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
