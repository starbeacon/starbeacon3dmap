//
//  DataModels+Extension.h
//  PMPServerManager
//
//  Created by KY Wong on 20/5/2016.
//  Copyright © 2016 Cherrypicks Alpha Ltd. All rights reserved.
//

#ifndef DataModels_Extension_h
#define DataModels_Extension_h

#import "DataModels.h"
#import "PMPDevices+Extension.h"
#import "PMPPois+Extension.h"
#import "PMPPromotions＋Extension.h"
#import "PMPDeviceOptions+Extension.h"

#endif /* DataModels_Extension_h */
