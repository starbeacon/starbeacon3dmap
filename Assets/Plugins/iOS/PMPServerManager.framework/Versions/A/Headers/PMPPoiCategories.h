//
//  PMPPoiCategories.h
//
//  Created by   on 23/6/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPPoiCategories : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double poiCategoriesIdentifier;
@property (nonatomic, strong) NSArray *message;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, assign) BOOL hasDetails;
@property (nonatomic, assign) BOOL isGate;
@property (nonatomic, strong) NSArray* subCategories;
@property (nonatomic, assign) PMPPoiCategories* parentPoiCategory;
@property (nonatomic, copy) NSString* externalId;
@property (nonatomic, assign) BOOL isInvisible;
@property (nonatomic, assign) BOOL isUnsearchable;
@property (nonatomic, assign) BOOL isStartPoint;
@property (nonatomic, assign) BOOL isOversea;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
