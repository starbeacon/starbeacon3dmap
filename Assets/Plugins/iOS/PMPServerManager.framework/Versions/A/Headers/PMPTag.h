//
//  PMPTag.h
//  PMPServerManager
//
//  Created by gordonwong on 22/6/2017.
//  Copyright © 2017 Cherrypicks Alpha Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMPTag : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSArray *idsArray;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
@end
