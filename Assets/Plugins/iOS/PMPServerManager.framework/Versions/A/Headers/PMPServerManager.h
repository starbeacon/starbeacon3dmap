//
//  SPServerManager.h
//  SPServerManager
//
//  Created by KY Wong on 15/12/2015.
//  Copyright © 2015 Cherrypicks Alpha Ltd. All rights reserved.
//

//#import <UIKit/UIKit.h>
#import <PMPServerManager/PMPResponseData.h>
#import <PMPServerManager/DataModels+Extension.h>

/*
// In this header, you should import all the public headers of your framework using statements like #import <SPServerManager/PublicHeader.h>
typedef NS_ENUM(NSUInteger, PMPServerType) {
    PMPServerType_Local,
    PMPServerType_Production,
    PMPServerType_Development,
    PMPServerType_SJG_DEV,
    PMPServerType_SJG_UAT,
    PMPServerType_SJG_PRO,
    PMPServerType_AirportDEMO,
    PMPServerType_AirportQA_CDN,
    PMPServerType_AirportQA,
    PMPServerType_AirportDev,
    PMPServerType_AirportSIT,
    PMPServerType_AirportProd,
    PMPServerType_CPDev,
    PMPServerType_HKIA_DEMO_SIT,
    PMPServerType_HKIA_DEMO_QA
};
*/
extern NSString* const PMPServerManagerDownloadJSONFailNotification;

@interface PMPServerManager : NSData

//@property (nonatomic, assign) PMPServerType serverType;
@property (nonatomic, strong, readonly) PMPResponseData* serverResponse;
@property (nonatomic, strong, readonly) NSString* version;
@property (nonatomic, readonly, getter=isDataOutdated) BOOL dataOutdated;
@property (nonatomic, readonly, getter=isRequesting) BOOL requesting;

+ (PMPServerManager *)sharedInstance;
- (void)downloadJson:(int)projectID firstTime:(BOOL)firstTime success:(void (^)(id response))success failure:(void (^)())failure;
- (void)fetchCacheJson:(void (^)(id response))success failure:(void (^)())failure;
- (void)getBrandsDataAsync:(NSString*)lang success:(void (^)(id response))success failure:(void (^)())failure;
- (void)getArtAndCultureDataAsync:(NSString*)lang success:(void (^)(id response))success failure:(void (^)())failure;
- (void)getTagsWithProjectId:(int)projectId success:(void (^)(id response))success failure:(void (^)())failure;
//- (void)reportZoneChange:(int)zoneId background:(BOOL)background major:(int)major deviceName:(NSString*) deviceName deviceOSVersion:(NSString*)deviceOSVersion success:(void (^)())success failure:(void (^)())failure;
- (NSString*)baseURL;
- (NSString*)serverURL;

@end
