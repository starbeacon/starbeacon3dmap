//
//  PMPNodes.h
//
//  Created by   on 23/5/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPNodes : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double mapId;
@property (nonatomic, assign) double status;
@property (nonatomic, assign) double x;
@property (nonatomic, assign) double y;
@property (nonatomic, assign) double nodesIdentifier;
@property (nonatomic, assign) double poiId;
@property (nonatomic, assign) double z;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
