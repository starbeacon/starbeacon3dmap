//
//  PMPPaths.h
//
//  Created by   on 23/5/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPPaths : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double computedDistance;
@property (nonatomic, assign) id durationReverse;
@property (nonatomic, assign) double pathsIdentifier;
@property (nonatomic, assign) double directed;
@property (nonatomic, assign) BOOL outDoor;
@property (nonatomic, assign) double pathTypeId;
@property (nonatomic, assign) double endNodeId;
@property (nonatomic, assign) double computedDuration;
@property (nonatomic, assign) double startNodeId;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
