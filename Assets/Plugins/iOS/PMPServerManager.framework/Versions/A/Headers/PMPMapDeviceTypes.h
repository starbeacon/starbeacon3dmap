//
//  PMPMapDeviceTypes.h
//
//  Created by   on 23/5/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PMPMapDeviceTypes : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double mapDeviceTypesIdentifier;
@property (nonatomic, assign) double threshold;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
