#ifdef __cplusplus
extern "C" {
#endif
//    struct Node{
//        double mapId;
//        double x;
//        double y;
//        double nodesIdentifier;
//        double z;
//    };
//
//    struct Path{
//        double startNodeId;
//        double endNodeId;
//    };
    
    typedef void (*OnDidLocationUpdatedCallback)(int beaconType, int locationX, int locationY, int locationZ, const char* mapId);
//    typedef void (*DownloadJSONResultCallback)(bool success, Node *nodeArray, int nodeArraySize , Path *pathArray, int pathArraySize);
    typedef void (*DownloadJSONResultCallback)(bool success, const char* response);
//    typedef void (*DownloadTagDataResultCallback)(bool success, const char* response);
    
    void pmp_location_manager_init(int projectId);
    void set_on_did_location_updated_callback(OnDidLocationUpdatedCallback callback);
    void set_download_json_result_callback(DownloadJSONResultCallback callback);
    void set_download_tag_data_result_callback(DownloadJSONResultCallback callback);
#ifdef __cplusplus
}
#endif
