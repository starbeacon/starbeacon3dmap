//
//  SPProximityUUIDS.h
//  SPLocationManager
//
//  Created by KY Wong on 9/12/2015.
//  Copyright © 2015 Cherrypicks Alpha Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMPProximityUUIDS : NSObject <NSFastEnumeration>
@property (nonatomic, assign, readonly) NSInteger count;

- (void)removeAll;
- (void)addProximityUUID:(id)uuid;
- (id)objectAtIndex:(NSUInteger)idx;
- (NSUInteger)indexOfObjectPassingTest:(BOOL (^)(id obj, NSUInteger idx, BOOL *stop))predicate;
@end
