//
//  SPBeacon.h
//  SPLocationManager
//
//  Created by KY Wong on 9/12/2015.
//  Copyright © 2015 Cherrypicks Alpha Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "PMPLocation.h"

#define USER_DICT_KEY_PROXIMITY_UUID @"proximityUUID"
#define USER_DICT_KEY_MAJOR @"major"
#define USER_DICT_KEY_MINOR @"minor"
#define USER_DICT_KEY_ACCURACY @"accuracy"
#define USER_DICT_KEY_RSSI @"rssi"
#define USER_DICT_KEY_BEACON_TYPE @"beaconType"
#define USER_DICT_KEY_BEACON_INFO_THRESHOLD @"beaconInfoThreshold"
#define USER_DICT_KEY_LOCAIOTN_X @"location_x"
#define USER_DICT_KEY_LOCAIOTN_Y @"location_y"
#define USER_DICT_KEY_LOCAIOTN_Z @"location_z"
#define USER_DICT_KEY_LOCAIOTN_NAME @"location_name"
#define USER_DICT_KEY_OUTDOOR_PATH @"outdoor_path"
#define USER_DICT_KEY_OUTDOOR_PATH_RATIO @"outdoor_path_ratio"
#define USER_DICT_KEY_MAP_SCALE_RATIO @"mapScaleRatio"
@interface PMPBeacon : NSObject
/*
 *  proximityUUID
 *
 *  Discussion:
 *    Proximity identifier associated with the beacon.
 *
 */
@property (readonly, nonatomic, copy) NSUUID *proximityUUID;

/*
 *  major
 *
 *  Discussion:
 *    Most significant value associated with the beacon.
 *
 */
@property (readonly, nonatomic, copy) NSNumber *major;

/*
 *  minor
 *
 *  Discussion:
 *    Least significant value associated with the beacon.
 *
 */
@property (readonly, nonatomic, copy) NSNumber *minor;

/*
 *  proximity
 *
 *  Discussion:
 *    Proximity of the beacon from the device.
 *
 */
@property (readonly, nonatomic) CLProximity proximity;

/*
 *  accuracy
 *
 *  Discussion:
 *    Represents an one sigma horizontal accuracy in meters where the measuring device's location is
 *    referenced at the beaconing device. This value is heavily subject to variations in an RF environment.
 *    A negative accuracy value indicates the proximity is unknown.
 *
 */
@property (readonly, nonatomic) CLLocationAccuracy accuracy;

/*
 *  rssi
 *
 *  Discussion:
 *    Received signal strength in decibels of the specified beacon.
 *    This value is an average of the RSSI samples collected since this beacon was last reported.
 *
 */
@property (readonly, nonatomic) NSInteger rssi;


/*
 *  scale accuracy
 *
 *  Discussion:
 *    Represents an one sigma horizontal accuracy in meters where the measuring device's location is
 *    referenced at the beaconing device. This value is heavily subject to variations in an RF environment.
 *    A negative accuracy value indicates the proximity is unknown.
 *
 */
@property (readonly, nonatomic) CLLocationAccuracy scaleAccuracy;


/*
 *  beacon type
 *
 *  Discussion:
 *    Beacon type.
 *
 */
@property (readonly, nonatomic) NSInteger type;

/*
 *  threshold
 *
 *  Discussion:
 *    Beacon threshold.
 *
 */
@property (readonly, nonatomic) double threshold;

/*
 *  mapScaleRatio
 *
 *  Discussion:
 *    Map scale ratio.
 *
 */
@property (readonly, nonatomic) double mapScaleRatio;


/*
 *  outdoor path ratio
 *
 *  Discussion:
 *    Beacon outdoor path ratio.
 *
 */
@property (readonly, nonatomic) double outdoorPathRatio;

/*
 *  outdoor path
 *
 *  Discussion:
 *    Beacon outdoor path.
 *
 */
@property (readonly, nonatomic, strong) NSArray* outdoorPath;

/*
 *  Location
 *
 *  Discussion:
 *    Location of the beacon place.
 *
 */
@property (readonly, nonatomic, strong) PMPLocation *location;

/*
 *  Battery Level
 *
 *  Discussion:
 *    Battery Level.
 *
 */
@property (readonly, nonatomic) int batteryLevel;

@property (nonatomic, assign, getter=isUsingForTrilateration) BOOL usingForTrilateration;

- (id)initWithCLBeacon:(CLBeacon*)beacon;

#ifdef APRIL_BEACON
- (id)initWithABeacon:(NSObject*)obj;
#endif

- (id)initWithDictionary:(NSDictionary*)userDict;

+ (instancetype)transmissionWithCLBeacon:(PMPBeacon *)CLBeacon;

- (id)combineWithLocation:(PMPBeacon*)beacon;
@end
