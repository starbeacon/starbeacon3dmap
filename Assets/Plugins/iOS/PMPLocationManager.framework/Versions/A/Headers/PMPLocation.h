//
//  PMPLocation.h
//  PMPLocationManager
//
//  Created by KY Wong on 9/12/2015.
//  Copyright © 2015 Cherrypicks Alpha Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PMPLocation : NSObject <NSCoding>

@property (assign, nonatomic) CGFloat x;
@property (assign, nonatomic) CGFloat y;
@property (assign, nonatomic) CGFloat z;
@property (strong, nonatomic) NSString* name;

- (instancetype)initWithX:(CGFloat)x
                        y:(CGFloat)y
                        z:(CGFloat)z
                    name:(NSString*)name;

- (double)distanceFromPoint:(CGPoint)point;
- (double)distanceFromPoint:(CGPoint)point scale:(double)scale;
@end
