//
//  SPLocationManager.h
//  SPLocationManager
//
//  Created by KY Wong on 9/12/2015.
//  Copyright © 2015 Cherrypicks Alpha Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMPLocation.h"
#import "PMPBeacon.h"
#import "PMPProximityUUIDS.h"

typedef NS_ENUM(NSInteger, PMPBeaconType) {
    PMPBeaconTypeUnknown = -1,
    PMPBeaconTypePositioning = 1,
    PMPBeaconTypePush,
    PMPBeaconTypeEntry,
    PMPBeaconTypeLiftInside,
    PMPBeaconTypeEntryIndoor,
    PMPBeaconTypeRSSIFilter,
    PMPBeaconTypeZonalPositioning,
    PMPBeaconTypePOI,
    PMPBeaconTypeShuttleBusInside
};

typedef NS_ENUM(NSInteger, PMPLocationManagerSDKType) {
    PMPLocationManagerSDKTypeUndefined = 0,
    PMPLocationManagerSDKTypeApple = 1,
    PMPLocationManagerSDKTypeAprilBrother
};

@protocol PMPLocationManagerDelegate <NSObject>
@optional
- (void)onDidBeaconsUpdated:(NSArray *)beacons;
- (NSArray*)onDidPlaybackBeaconsUpdated;
- (void)onDidPlaybackBeaconsFinished;
- (void)onDidLocationUpdated:(PMPBeaconType)beaconType location:(PMPLocation*)location;
- (void)onDidTransmissionsUpdated:(NSArray*)transmissions;
- (void)onDidCompassUpdated:(double)direction;
- (void)didUpdateLocations:(NSArray<CLLocation *> *)locations;
- (void)onDidLocationExitRegion;
- (void)onError:(NSError*)error;
@end

typedef void (^CompassCalibratedHandler)(double result);

@interface PMPLocationManager : NSObject

+ (instancetype)shared;

- (void)start;
- (void)stop;
- (void)startPlayBack:(NSTimeInterval)frequency;
- (void)stopPlayBack;

- (void)calibrateCompass:(CompassCalibratedHandler)handler;

- (void)addDelegate:(id<PMPLocationManagerDelegate>)delegate;
- (void)removeDelegate:(id<PMPLocationManagerDelegate>)delegate;

- (void)addFloorSwitchingFilter:(int)deviceId;
- (void)removeFloorSwitchingFilter:(int)deviceId;
- (void)clearAllFloorSwitching;


- (void)mapViewWillAppear;
- (void)mapViewWillDisappear;
- (float)lastLocationX;
- (float)lastLocationY;

/*
 *  proximityUUIDS
 *  Discussion:
 *  SPBeacon must be used
 */
@property (nonatomic, strong) PMPProximityUUIDS* proximityUUIDS;
@property (nonatomic, assign) PMPLocationManagerSDKType beaconSDKType;

@property (nonatomic, assign) BOOL enableMaxNumOfBeaconsDetection;
@property (nonatomic, assign) NSInteger maxNumOfBeaconsDetection;

@property (nonatomic, assign) BOOL enableBeaconAccuracyFilter;
@property (nonatomic, assign) NSInteger beaconAccuracyFilter;

@property (nonatomic, assign) BOOL enableMovingAverage;
@property (nonatomic, assign) NSInteger numOfMovingAverageSample;
@property (nonatomic, assign) NSInteger movingAverageTimeToRemove;

@property (nonatomic, assign) BOOL enableMaxDistanceMoving;
@property (nonatomic, assign) double maxDistanceMoving;

@property (nonatomic, assign) BOOL enableClosetPathFinding;
@property (nonatomic, assign) NSInteger closetPathFindingTimeGuard;

/* force to enable now. Note you cannot stop it */
@property (nonatomic, assign) BOOL enableFloorSwitchingFilters;
@property (nonatomic, assign) NSInteger numOfFloorBeaconDetection;

@property (nonatomic, assign) BOOL enableWalkingDetection;
@property (nonatomic, assign) NSInteger numOfStationaryToResume;

@property (nonatomic, assign) BOOL enableLowPassFilter;
@property (nonatomic, assign) double lowPassFilterFactor; // Default value is 0.3

@property (nonatomic, assign) BOOL enableForwardPredict;
/* In Meter */
@property (nonatomic, assign) double forwardPredictOffset;

@property (nonatomic, assign) BOOL enableCacheBeacon;
/* In seconds */
@property (nonatomic, assign) NSInteger cacheBeaconTimeout;

@property (nonatomic, assign) BOOL enableBluetoothAlertPrompt;

@property(assign, nonatomic) BOOL enableBackgroundLocationUpdates;

@property (nonatomic, assign) BOOL enableLog;

@property (nonatomic, strong, readonly) NSString* version;
@property (nonatomic) NSTimeInterval indoorLocationTimeout;

@property (nonatomic,assign) BOOL enableKalmanFilter;
@end
