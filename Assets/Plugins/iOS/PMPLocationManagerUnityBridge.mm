#import "PMPLocationManagerUnityBridge.h"
#import <PMPLocationManager/PMPLocationManager.h>
#import <PMPServerManager/PMPServerManager.h>
#import <PMPServerManager/PMPResponseData.h>
#import <PMPServerManager/PMPDevices.h>

OnDidLocationUpdatedCallback didLocationUpdatedCallback = NULL;
DownloadJSONResultCallback downloadJSONResultCallback = NULL;
DownloadJSONResultCallback downloadTagDataResultCallback = NULL;

@interface PMPLocationManagerUnityBridge : NSObject<PMPLocationManagerDelegate>

@end
static PMPLocationManagerUnityBridge *locationManagerUnityBridge;

void pmp_location_manager_init(int projectId){
    if(!locationManagerUnityBridge){
        locationManagerUnityBridge = [[PMPLocationManagerUnityBridge alloc] init];
    }
    
    [[PMPLocationManager shared] stop];
    [[PMPLocationManager shared].proximityUUIDS removeAll];
    [[PMPLocationManager shared] removeDelegate:locationManagerUnityBridge];
    
    [PMPLocationManager shared].enableLog = YES;
    [PMPLocationManager shared].enableMaxNumOfBeaconsDetection = NO;
    [PMPLocationManager shared].maxNumOfBeaconsDetection = 8;
    [PMPLocationManager shared].enableBeaconAccuracyFilter = YES;
    [PMPLocationManager shared].beaconAccuracyFilter = 30;
    [PMPLocationManager shared].enableLowPassFilter = NO;
    [PMPLocationManager shared].enableWalkingDetection = NO;
    [[PMPLocationManager shared] addDelegate:locationManagerUnityBridge];
    
    [[PMPServerManager sharedInstance] downloadJson:projectId firstTime:YES success:^(id response) {
        PMPResponseData *serverResponse = [PMPServerManager sharedInstance].serverResponse;
        double outDoorPathRatio = 0;
        for (PMPDevices* beacons in serverResponse.devices) {
            if(!beacons.bleWayFindingUuid || [beacons.bleWayFindingUuid isEqualToString: @""])continue;
            NSMutableArray* outdoorPath = [[NSMutableArray alloc] init];
            
            if (beacons.deviceOptions.nearbyOutdoorNodes) {
                outDoorPathRatio = [beacons.deviceOptions.outdoorPathRatio doubleValue];
                for(NSString* outdoorNodeId in beacons.deviceOptions.nearbyOutdoorNodeIds) {
                    NSInteger index = [serverResponse.nodes indexOfObjectPassingTest:^BOOL(PMPNodes*  _Nonnull node, NSUInteger idx, BOOL * _Nonnull stop) {
                        if (node.nodesIdentifier == [outdoorNodeId doubleValue]) {
                            *stop = YES;
                            return YES;
                        }
                        return NO;
                    }];
                    if (index != NSNotFound) {
                        NSLog(@"beacons %@", beacons);
                        PMPNodes* node = [serverResponse.nodes objectAtIndex:index];
                        [outdoorPath addObject:@{@"x":[NSNumber numberWithDouble:node.x], @"y":[NSNumber numberWithDouble:node.y]}];
                    }
                }
            }
            
            NSMutableDictionary* NSMutableDict = [NSMutableDictionary dictionary];
            [NSMutableDict setObject:[[NSUUID alloc] initWithUUIDString:beacons.bleWayFindingUuid] forKey:USER_DICT_KEY_PROXIMITY_UUID];
            [NSMutableDict setObject: @(beacons.majorNo) forKey:USER_DICT_KEY_MAJOR];
            [NSMutableDict setObject: @(0) forKey:USER_DICT_KEY_MINOR];
            [NSMutableDict setObject: @(0) forKey:USER_DICT_KEY_ACCURACY];
            [NSMutableDict setObject: @(0) forKey:USER_DICT_KEY_RSSI];
            [NSMutableDict setObject: @(beacons.x) forKey:USER_DICT_KEY_LOCAIOTN_X];
            [NSMutableDict setObject: @(beacons.y) forKey:USER_DICT_KEY_LOCAIOTN_Y];
            [NSMutableDict setObject: @(beacons.z) forKey:USER_DICT_KEY_LOCAIOTN_Z];
            [NSMutableDict setObject: [NSString stringWithFormat:@"%d", (int)beacons.mapId] forKey:USER_DICT_KEY_LOCAIOTN_NAME];
            [NSMutableDict setObject: @((int)beacons.mapDeviceTypeId) forKey:USER_DICT_KEY_BEACON_TYPE];
            [NSMutableDict setObject: @(beacons.threshold) forKey:USER_DICT_KEY_BEACON_INFO_THRESHOLD];
            [NSMutableDict setObject: @(outDoorPathRatio) forKey:USER_DICT_KEY_OUTDOOR_PATH_RATIO];
            [NSMutableDict setObject: outdoorPath forKey:USER_DICT_KEY_OUTDOOR_PATH];
            [NSMutableDict setObject: @(9.08) forKey:USER_DICT_KEY_MAP_SCALE_RATIO];
            
            NSDictionary *dict = [NSDictionary dictionaryWithDictionary:NSMutableDict];
            [[PMPLocationManager shared].proximityUUIDS addProximityUUID:[[PMPBeacon alloc] initWithDictionary:dict]];
        }
        
//        Node nodeArray[[serverResponse.nodes count]];
//        for(int i = 0; i < [serverResponse.nodes count] ; i++){
//            PMPNodes *node = [serverResponse.nodes objectAtIndex:i];
//            Node n;
//            n.x = node.x;
//            n.y = node.y;
//            n.z = node.z;
//            n.mapId = node.mapId;
//            n.nodesIdentifier = node.nodesIdentifier;
//            nodeArray[i] = n;
//        }
        
//        NSLog(@"nodeArray length: %lu", sizeof(nodeArray)/sizeof(nodeArray[0]));
        
//        Path pathArray[[serverResponse.paths count]];
//        for(int i = 0; i < [serverResponse.paths count]; i++){
//            PMPPaths *path = [serverResponse.paths objectAtIndex:i];
//            Path p;
//            p.startNodeId = path.startNodeId;
//            p.endNodeId = path.endNodeId;
//
//            pathArray[i] = p;
//        }
//        NSLog(@"pathArray length: %lu", sizeof(pathArray)/sizeof(pathArray[0]));
        
        NSLog(@"start Engine");
        [[PMPLocationManager shared] start];
        NSString *responseString = [[NSString alloc] initWithData:response
                                                         encoding:NSUTF8StringEncoding];
        if(downloadJSONResultCallback != NULL){
            downloadJSONResultCallback(true,[responseString UTF8String]);
//            downloadJSONResultCallback(true, nodeArray,(int)sizeof(nodeArray)/sizeof(nodeArray[0]), pathArray,(int)sizeof(pathArray)/sizeof(pathArray[0]));
        }

    } failure:^{
        if(downloadJSONResultCallback != NULL){
//            Node emptyNodeArray[] = {};
//            Path emptyPathArray[] = {};
//            downloadJSONResultCallback(false, emptyNodeArray,0, emptyPathArray,0);
            downloadJSONResultCallback(false,[@"" UTF8String]);
        }
    }];
    
    [[PMPServerManager sharedInstance] getTagsWithProjectId:projectId success:^(id response) {
        NSString *responseString = [[NSString alloc] initWithData:response
                                                         encoding:NSUTF8StringEncoding];
//        NSLog(@"getTagsWithProjectId :%@", responseString);
        
        if(downloadTagDataResultCallback != NULL){
            downloadTagDataResultCallback(true, [responseString UTF8String]);
        }
        
    } failure:^{
        if(downloadTagDataResultCallback != NULL){
            downloadTagDataResultCallback(false, [@"" UTF8String]);
        }
    }];
    
}

void set_on_did_location_updated_callback(OnDidLocationUpdatedCallback callback){
    didLocationUpdatedCallback = callback;
}

void set_download_json_result_callback(DownloadJSONResultCallback callback){
    downloadJSONResultCallback = callback;
}

void set_download_tag_data_result_callback(DownloadJSONResultCallback callback){
    downloadTagDataResultCallback = callback;
}

@implementation PMPLocationManagerUnityBridge

- (void)onDidLocationUpdated:(PMPBeaconType)beaconType location:(PMPLocation*)location{
    NSLog(@"%@", [NSString stringWithFormat:@"onDidLocationUpdated x:%f, y:%f name: %@",location.x,location.y,location.name]);
    if(didLocationUpdatedCallback != NULL){
        didLocationUpdatedCallback(beaconType,location.x, location.y,location.z,[location.name UTF8String]);
    }
}

@end
