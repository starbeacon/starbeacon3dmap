//
//  PMPBeaconManager.h
//  SPLocationManager
//
//  Created by KY Wong on 10/12/2015.
//  Copyright © 2015 Cherrypicks Alpha Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class PMPBeaconManager;

typedef NS_ENUM(NSInteger, PMPBeaconManagerSDKType) {
    PMPBeaconManagerSDKTypeUndefined = 0,
    PMPBeaconManagerSDKTypeApple = 1,
};

/**
 
 The CLBeaconRegionManagerDelegate protocol defines the delegate methods to respond for related events.
 */

@protocol PMPBeaconManagerDelegate <NSObject>

@optional

/**
 * Delegate method invoked during ranging.
 * Allows to retrieve NSArray of all discoverd beacons
 * represented with CLBeaconRegion objects.
 *
 * @param manager beacon manager
 * @param beacons all beacons as CLBeaconRegion objects
 * @param region beacon region
 *
 * @return void
 */
- (void)beaconManager:(nonnull PMPBeaconManager *)manager
      didRangeBeacons:(nullable NSArray *)beacons
             inRegion:(nullable CLRegion *)region;

/**
 * Delegate method invoked when ranging fails
 * for particular region. Related NSError object passed.
 *
 * @param manager beacon manager
 * @param region beacon region
 * @param error object containing error info
 *
 * @return void
 */
- (void)beaconManager:(nonnull PMPBeaconManager *)manager
rangingBeaconsDidFailForRegion:(nullable CLRegion *)region
            withError:(nullable NSError *)error;


/**
 * Delegate method invoked when monitoring fails
 * for particular region. Related NSError object passed.
 *
 * @param manager beacon manager
 * @param region beacon region
 * @param error object containing error info
 *
 * @return void
 */
- (void)beaconManager:(nonnull PMPBeaconManager *)manager
monitoringDidFailForRegion:(nullable CLRegion *)region
            withError:(nullable NSError *)error;
/**
 * Method triggered when iOS device enters beacon
 * beacon region during monitoring.
 *
 * @param manager beacon manager
 * @param region beacon region
 *
 * @return void
 */
- (void)beaconManager:(nonnull PMPBeaconManager *)manager
       didEnterRegion:(nullable CLRegion *)region;


/**
 * Method triggered when iOS device leaves beacon
 * beacon region during monitoring.
 *
 * @param manager beacon manager
 * @param region beacon region
 *
 * @return void
 */
- (void)beaconManager:(nonnull PMPBeaconManager *)manager
        didExitRegion:(nullable CLRegion *)region;

/**
 * Method triggered when beacon region state
 * was determined using requestStateForRegion:
 *
 * @param manager beacon manager
 * @param state beacon region state
 * @param region beacon region
 *
 * @return void
 */
- (void)beaconManager:(nonnull PMPBeaconManager *)manager
    didDetermineState:(CLRegionState)state
            forRegion:(nullable CLRegion *)region;

/**
 * Method triggered when device moving
 *
 *
 * @param manager beacon manager
 * @param degree device degree
 * @return void
 */

- (void)beaconManager:(nonnull PMPBeaconManager *)manager
       didUpdateHeading:(nullable CLHeading *)newHeading;

/*
 *  beaconManager:didUpdateLocations:
 *
 *  Discussion:
 *    Invoked when new locations are available.  Required for delivery of
 *    deferred locations.  If implemented, updates will
 *    not be delivered to locationManager:didUpdateToLocation:fromLocation:
 *
 *    locations is an array of CLLocation objects in chronological order.
 */
- (void)beaconManager:(nonnull PMPBeaconManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_6_0);

@end

@interface PMPBeaconManager : NSObject
@property(assign, nonatomic, nullable) id<PMPBeaconManagerDelegate> delegate;
@property(assign, nonatomic) BOOL allowsBackgroundLocationUpdates __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_9_0) __TVOS_PROHIBITED __WATCHOS_PROHIBITED;
@property(strong, nonatomic, readonly, nonnull) NSString* version;

- (nonnull id)initWithSDKType:(PMPBeaconManagerSDKType)beaconSDKType;
/*
 *  requestWhenInUseAuthorization
 *
 *  Discussion:
 *      When +authorizationStatus == kCLAuthorizationStatusNotDetermined,
 *      calling this method will trigger a prompt to request "when-in-use"
 *      authorization from the user.  If possible, perform this call in response
 *      to direct user request for a location-based service so that the reason
 *      for the prompt will be clear.  Any authorization change as a result of
 *      the prompt will be reflected via the usual delegate callback:
 *      -locationManager:didChangeAuthorizationStatus:.
 *
 *      If received, "when-in-use" authorization grants access to the user's
 *      location via -startUpdatingLocation/-startRangingBeaconsInRegion while
 *      in the foreground.  If updates have been started when going to the
 *      background, then a status bar banner will be displayed to maintain
 *      visibility to the user, and updates will continue until stopped
 *      normally, or the app is killed by the user.
 *
 *      "When-in-use" authorization does NOT enable monitoring API on regions,
 *      significant location changes, or visits, and -startUpdatingLocation will
 *      not succeed if invoked from the background.
 *
 *      When +authorizationStatus != kCLAuthorizationStatusNotDetermined, (ie
 *      generally after the first call) this method will do nothing.
 *
 *      If the NSLocationWhenInUseUsageDescription key is not specified in your
 *      Info.plist, this method will do nothing, as your app will be assumed not
 *      to support WhenInUse authorization.
 */
- (void)requestWhenInUseAuthorization __OSX_AVAILABLE_STARTING(__MAC_NA, __IPHONE_8_0);

/*
 *  requestAlwaysAuthorization
 *
 *  Discussion:
 *      When +authorizationStatus == kCLAuthorizationStatusNotDetermined,
 *      calling this method will trigger a prompt to request "always"
 *      authorization from the user.  If possible, perform this call in response
 *      to direct user request for a location-based service so that the reason
 *      for the prompt will be clear.  Any authorization change as a result of
 *      the prompt will be reflected via the usual delegate callback:
 *      -locationManager:didChangeAuthorizationStatus:.
 *
 *      If received, "always" authorization grants access to the user's
 *      location via any CLLocationManager API, and grants access to
 *      launch-capable monitoring API such as geofencing/region monitoring,
 *      significante location visits, etc.  Even if killed by the user, launch
 *      events triggered by monitored regions or visit patterns will cause a
 *      relaunch.
 *
 *      "Always" authorization presents a significant risk to user privacy, and
 *      as such requesting it is discouraged unless background launch behavior
 *      is genuinely required.  Do not call +requestAlwaysAuthorization unless
 *      you think users will thank you for doing so.
 *
 *      When +authorizationStatus != kCLAuthorizationStatusNotDetermined, (ie
 *      generally after the first call) this method will do nothing.
 *
 *      If the NSLocationAlwaysUsageDescription key is not specified in your
 *      Info.plist, this method will do nothing, as your app will be assumed not
 *      to support Always authorization.
 */
- (void)requestAlwaysAuthorization __OSX_AVAILABLE_STARTING(__MAC_NA, __IPHONE_8_0) __TVOS_PROHIBITED;
/*
 *  stopMonitoringForRegion:
 *
 *  Discussion:
 *      Stop monitoring the specified region.  It is valid to call stopMonitoringForRegion: for a region that was registered
 *      for monitoring with a different location manager object, during this or previous launches of your application.
 *
 *      This is done asynchronously and may not be immediately reflected in monitoredRegions.
 */
- (void)stopMonitoringForRegion:(nullable CLRegion *)region __OSX_AVAILABLE_STARTING(__MAC_10_7,__IPHONE_4_0) __TVOS_PROHIBITED __WATCHOS_PROHIBITED;

/*
 *  startMonitoringForRegion:
 *
 *  Discussion:
 *      Start monitoring the specified region.
 *
 *      If a region of the same type with the same identifier is already being monitored for this application,
 *      it will be removed from monitoring. For circular regions, the region monitoring service will prioritize
 *      regions by their size, favoring smaller regions over larger regions.
 *
 *      This is done asynchronously and may not be immediately reflected in monitoredRegions.
 */
- (void)startMonitoringForRegion:(nullable CLRegion *)region __OSX_AVAILABLE_STARTING(__MAC_TBD,__IPHONE_5_0) __TVOS_PROHIBITED __WATCHOS_PROHIBITED;

/*
 *  startRangingBeaconsInRegion:
 *
 *  Discussion:
 *      Start calculating ranges for beacons in the specified region.
 */
- (void)startRangingBeaconsInRegion:(nullable CLBeaconRegion *)region __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_7_0) __TVOS_PROHIBITED __WATCHOS_PROHIBITED;

/*
 *  stopRangingBeaconsInRegion:
 *
 *  Discussion:
 *      Stop calculating ranges for the specified region.
 */
- (void)stopRangingBeaconsInRegion:(nullable CLBeaconRegion *)region __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_7_0) __TVOS_PROHIBITED __WATCHOS_PROHIBITED;

/*
 *  startUpdatingHeading
 *
 *  Discussion:
 *      Start updating heading.
 */
- (void)startUpdatingHeading __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0) __TVOS_PROHIBITED __WATCHOS_PROHIBITED;

/*
 *  stopUpdatingHeading
 *
 *  Discussion:
 *      Stop updating heading.
 */
- (void)stopUpdatingHeading __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0) __TVOS_PROHIBITED __WATCHOS_PROHIBITED;

/*
 *  startUpdatingLocation
 *
 *  Discussion:
 *      Start updating locations.
 */
- (void)startUpdatingLocation __TVOS_PROHIBITED __WATCHOS_PROHIBITED;

/*
 *  stopUpdatingLocation
 *
 *  Discussion:
 *      Stop updating locations.
 */
- (void)stopUpdatingLocation;

/*
 *  startMonitoringSignificantLocationChanges
 *
 *  Discussion:
 *      Start monitoring significant location changes.  The behavior of this service is not affected by the desiredAccuracy
 *      or distanceFilter properties.  Locations will be delivered through the same delegate callback as the standard
 *      location service.
 *
 */
- (void)startMonitoringSignificantLocationChanges;
/*
 *  stopMonitoringSignificantLocationChanges
 *
 *  Discussion:
 *      Stop monitoring significant location changes.
 *
 */
- (void)stopMonitoringSignificantLocationChanges;
@end
