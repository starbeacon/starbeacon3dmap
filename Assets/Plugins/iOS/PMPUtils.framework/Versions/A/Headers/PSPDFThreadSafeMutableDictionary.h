//
//  PSPDFThreadSafeMutableDictionary.h
//  BeaconDetector
//
//  Created by KY Wong on 24/6/15.
//  Copyright (c) 2015 Cherrypicks. All rights reserved.
//

#import <Foundation/Foundation.h>


// Dictionary-Subclasss whose primitive operations are thread safe.
@interface PSPDFThreadSafeMutableDictionary : NSMutableDictionary
@end
