﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClonePrefab : MonoBehaviour {

    public Transform parentTransform;
    public Transform prefab;
    public Text cloneCountText;
    public int initalCount;

    private int count = 0;

	// Use this for initialization
    void Start()
    {
        count += initalCount;
        cloneCountText.text = count.ToString();
    }

    public void PlaceClone()
    {
        Transform clone = Instantiate(prefab);
        clone.parent = parentTransform;
        clone.position = new Vector3(0, count * 0.28f, 0);
    
        count++;
        cloneCountText.text = count.ToString();
    }
}
