﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using AOT;
using System;

public class PMPNativeBridgeForIOS : MonoBehaviour {
	#if UNITY_IOS 
	#region iOS framework bridge
	[DllImport("__Internal")]
	private static extern void pmp_location_manager_init(int projectId);

	[DllImport("__Internal")]
	private static extern void set_on_did_location_updated_callback (NativeOnDidLocationUpdatedCallback callback);

	[DllImport("__Internal")]
	private static extern void set_download_json_result_callback (NativeDownloadJSONResultCallback callback);

	[DllImport("__Internal")]
	private static extern void set_download_tag_data_result_callback (NativeDownloadJSONResultCallback callback);

	private delegate void NativeOnDidLocationUpdatedCallback(int beaconType, int locationX, int locationY, int locationZ, string name);
	private delegate void NativeDownloadJSONResultCallback (bool success, string response);

	#endregion

	#region location events
	public delegate void OnDidLocationUpdatedCallback (Location location);
	public delegate void DownloadJSONResultCallback (bool success,string response);
	public event OnDidLocationUpdatedCallback onDidLocationUpdatedHandler;
	public event DownloadJSONResultCallback downloadMapResultHandler;
	public event DownloadJSONResultCallback downloadTagResultHandler; 
	#endregion

	private Node[] nodeArray;
	private Path[] pathArray;

	public Location lastLocation {
		get;
		protected set;
	}

	public static PMPNativeBridgeForIOS sharedInstance {
		get;
		protected set;
	}

	void Awake(){
		if (sharedInstance == null) {
			sharedInstance = this;
		} else if(sharedInstance != this) {
			Destroy (gameObject);
		}
		DontDestroyOnLoad (this);
		Debug.Log ("PMPLocationManagerIOSBridge created");
	}

	public void Init(int projectId){
		Debug.Log ("PMPLocationManagerIOSBridge init");
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			setDelegates ();
			pmp_location_manager_init (projectId);
		}
	}

	#region utility
	private static T[] GetNativeArray<T>(IntPtr array, int length) {
		T[] result = new T[length];
		int size = Marshal.SizeOf (typeof(T));

		if (IntPtr.Size == 4) {
			// 32-bit system
			for (int i = 0; i < result.Length; i++) {
				result [i] = (T)Marshal.PtrToStructure (array, typeof(T));
				array = new IntPtr (array.ToInt32 () + size);
			}
		} else {
			// probably 64-bit system
			for (int i = 0; i < result.Length; i++) {
				result [i] = (T)Marshal.PtrToStructure (array, typeof(T));
				array = new IntPtr (array.ToInt64 () + size);
			}
		}
		return result;
	}
	#endregion

	#region PMPLocationManager callback
	void setDelegates(){
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			set_on_did_location_updated_callback(onDidLocationUpdated);
			set_download_json_result_callback (onDownloadJSONFinished);
			set_download_tag_data_result_callback (onDownloadTagDataFinished);
		}
	}

	[MonoPInvokeCallback(typeof(NativeOnDidLocationUpdatedCallback))] 
	private static void onDidLocationUpdated(int beaconType, int locationX, int locationY, int locationZ, string name){
		Debug.Log ("onDidLocationUpdated " + locationX + ", " + locationY + ", " + name);
		sharedInstance.lastLocation = new Location ();
		sharedInstance.lastLocation.x = locationX;
		sharedInstance.lastLocation.y = locationY;
		sharedInstance.lastLocation.z = locationZ;
		sharedInstance.lastLocation.name = name;

		if (sharedInstance.onDidLocationUpdatedHandler != null) {
			sharedInstance.onDidLocationUpdatedHandler (sharedInstance.lastLocation);
		}
	}

	[MonoPInvokeCallback(typeof(NativeDownloadJSONResultCallback))] 
	private static void onDownloadJSONFinished (bool success, string response){
		if (sharedInstance.downloadMapResultHandler != null) {
			sharedInstance.downloadMapResultHandler (success, response);
		}
	}

	[MonoPInvokeCallback(typeof(NativeDownloadJSONResultCallback))] 
	private static void onDownloadTagDataFinished(bool success, string response){
		if (sharedInstance.downloadTagResultHandler != null) {
			sharedInstance.downloadTagResultHandler (success, response);
		}
	}


	#endregion
	#endif
}
