﻿using UnityEngine;

public class UnityBridgeNotifier : AndroidJavaProxy {
	public delegate void DownloadJSONResultCallback(bool success, string response);
	public event DownloadJSONResultCallback downloadMapResultHandler;
	public event DownloadJSONResultCallback downloadTagResultHandler; 

	public UnityBridgeNotifier(): base("com.pmp.mapsdk.cms.PMPUnityBridgeNotifier"){

	}
	void didFinishedDownloadMapJSON(bool success, string responseString){
		if (downloadMapResultHandler != null) {
			downloadMapResultHandler (success,responseString);
		}
	}
	void didFinishedDownloadTagJSON(bool success, string responseString){
		if (downloadTagResultHandler != null) {
			downloadTagResultHandler (success,responseString);
		}
	}
}
