﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PMPNativeBridgeForAndroid : MonoBehaviour {
	public static PMPNativeBridgeForAndroid  sharedInstance;
	private AndroidJavaObject currentActivity;

	#region location events
	public delegate void OnDidLocationUpdatedCallback (Location location);
	public delegate void DownloadJSONResultCallback (bool success,string response);
	public event OnDidLocationUpdatedCallback onDidLocationUpdatedHandler;
	public event DownloadJSONResultCallback downloadMapResultHandler;
	public event DownloadJSONResultCallback downloadTagResultHandler;
	#endregion

	private void Awake() {
		if (sharedInstance == null) {
			sharedInstance = this;
		} else if (sharedInstance != this) {
			Destroy(gameObject);
		}
		DontDestroyOnLoad(gameObject);
	}
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Init(int projectId){
		#if UNITY_ANDROID
		if (Application.platform == RuntimePlatform.Android) {
			Debug.Log("PMPNativeBridgeForAndroid init");
			AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

			var pmpBridgeClass = new AndroidJavaClass("com.pmp.mapsdk.location.PMPUnityBridge");
			AndroidJavaObject pmpBridge = pmpBridgeClass.CallStatic<AndroidJavaObject>("getShared", currentActivity);
//			ServerManagerNotifier serverManagerNotifier = new ServerManagerNotifier(currentActivity);
//			serverManagerNotifier.OnDownloadJSONFinished += onDownloadJSONFinished;
			UnityBridgeNotifier unityBridgeNotifier = new UnityBridgeNotifier();
			unityBridgeNotifier.downloadMapResultHandler += onDownloadMapJSONFinished;
			unityBridgeNotifier.downloadTagResultHandler += onDownloadTagJSONFinished;
			LocationManager locationManager = LocationManager.Instance;
			locationManager.OnIndoorLocationUpdated += onDidLocationUpdated;
			pmpBridge.Call("init", projectId, unityBridgeNotifier, locationManager);
			Debug.Log("PMPNativeBridgeForAndroid init finished");
		}
		#endif
	}

	private void onDownloadMapJSONFinished (bool success, string response){
		Debug.Log ("PMPNativeBridgeForAndroid onDownloadMapJSONFinished");
		if (downloadMapResultHandler != null) {
			downloadMapResultHandler (success, response);
		}
	}

	private void onDownloadTagJSONFinished (bool success, string response){
		Debug.Log ("PMPNativeBridgeForAndroid onDownloadTagJSONFinished");
		if (downloadTagResultHandler != null) {
			downloadTagResultHandler (success, response);
		}
	}

	private void onDidLocationUpdated(Location location){
		Debug.Log ("PMPNativeBridgeForAndroid onDidLocationUpdated: " + location.x + ", " + location.y + ", " + location.name);
		if (onDidLocationUpdatedHandler != null) {
			onDidLocationUpdatedHandler (location);
		}
	}
}
