﻿using UnityEngine;
using TouchScript.Gestures.TransformGestures;
using DimBoxes;

public class TouchCameraController : MonoBehaviour {

    public ScreenTransformGesture dragToPanGesture;
    public ScreenTransformGesture zoomAndRotateGesture;
    public ScreenTransformGesture tiltGesture;
    public float maxPanSpeed = 0.01f;
    public float zoomSpeed = 1f;
    public float rotationSpeed = 0.01f;
    public float minTilt = 0f;
    public float maxTilt = 90f;
    public float tiltSpeed = 200f;
    public Transform pivot;
    public Transform cam;
    public BoundBox mapBoundBox;

    private float closestZoomDistance;
    private float farthestZoomDistance;
    private bool isTilting = false;
    private bool isRotating = false;
    private bool isZooming = false;
    private Vector3 rotationPivot = Vector3.zero;
    private Vector3 zoomDirection = Vector3.down;

    // Use this for initialization
    void Start() {
        closestZoomDistance = mapBoundBox.height;
        farthestZoomDistance = mapBoundBox.depth / 2 / Mathf.Tan(cam.GetComponent<Camera>().fieldOfView / 2 * Mathf.Deg2Rad) + mapBoundBox.height; //use depth for portrait-oriented map, width for landscape
        CameraAngleHelper.closestZoomDistance = this.closestZoomDistance;
        CameraAngleHelper.farthestZoomDistance = this.farthestZoomDistance;
        //SetCameraZenithAngle(); 
    }

    // Update is called once per frame
    void Update() {
    }

    private void OnEnable() {
        dragToPanGesture.Transformed += DragToPanGestureHandler;
        zoomAndRotateGesture.Transformed += ZoomAndRotateGestureHandler;
        zoomAndRotateGesture.TransformCompleted += ZoomAndRotateGestureTransformCompleted;
        tiltGesture.Transformed += TiltGestureHandler;
        tiltGesture.TransformCompleted += TiltGestureTransformCompleted;
    }

    private void OnDisable() {
        dragToPanGesture.Transformed -= DragToPanGestureHandler;
        zoomAndRotateGesture.Transformed -= ZoomAndRotateGestureHandler;
        zoomAndRotateGesture.TransformCompleted -= ZoomAndRotateGestureTransformCompleted;
        tiltGesture.Transformed -= TiltGestureHandler;
        tiltGesture.TransformCompleted -= TiltGestureTransformCompleted;
    }

    private void DragToPanGestureHandler(object sender, System.EventArgs e) {
        float panSpeed = maxPanSpeed * cam.localPosition.y / farthestZoomDistance;
        Vector3 moveAmount = new Vector3() {
            x = dragToPanGesture.DeltaPosition.x * panSpeed * -1,
            y = 0f, //no change in up-down position
            z = dragToPanGesture.DeltaPosition.y * panSpeed * -1 //y of delta is z of the cam, tricky isn't it?
        };
        pivot.transform.localPosition += moveAmount;
    }

    private void ZoomAndRotateGestureHandler(object sender, System.EventArgs e) {
        // find out the pivot
        CalculatePivot();

        //rotate the map
        float rotationAngle = zoomAndRotateGesture.DeltaRotation * rotationSpeed * -1;
        isRotating = true;
        mapBoundBox.transform.RotateAround(rotationPivot, Vector3.up, rotationAngle);

        //zoom by changing pos of cam
        float twoFingerZoomScale = (zoomAndRotateGesture.DeltaScale - 1.0f) * zoomSpeed;
        isZooming = true;
        if (cam.transform.localPosition.y >= closestZoomDistance) {  // && cam.transform.localPosition.y <= farthestZoomDistance) {
            cam.transform.localPosition += zoomDirection * twoFingerZoomScale;
            cam.transform.localPosition = new Vector3(
                cam.transform.localPosition.x,
                Mathf.Clamp(cam.transform.localPosition.y, closestZoomDistance, farthestZoomDistance),
                cam.transform.localPosition.z);
        }

    }

    private void TiltGestureHandler(object sender, System.EventArgs e) {
        if (!isTilting && cam.position.y > 0.1f) {
            CalculatePivot();
        }
        float twoFingerDeltaY = tiltGesture.DeltaPosition.y / Screen.height * tiltSpeed;
        isTilting = true;
        Quaternion tiltAmount = Quaternion.Euler(twoFingerDeltaY * -1, 0, 0);
        Quaternion currentTilt = cam.localRotation;
        Quaternion destTilt = currentTilt * tiltAmount;
        destTilt = Quaternion.Euler(ClampAngle(destTilt.eulerAngles.x, minTilt, maxTilt), 0, 0);
        Quaternion tiltBy = destTilt * Quaternion.Inverse(currentTilt);
        cam.RotateAround(rotationPivot, Vector3.right, tiltBy.eulerAngles.x);
    }

    private void CalculatePivot() {
        Plane xzPlane = new Plane(Vector3.up, Vector3.zero);
        Camera camera = cam.GetComponent<Camera>();
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
        zoomDirection = ray.direction;
        float distance;
        xzPlane.Raycast(ray, out distance);
        rotationPivot = ray.GetPoint(distance);
    }

    private void ZoomAndRotateGestureTransformCompleted(object sender, System.EventArgs e) {
        isRotating = false;
        isZooming = false;
    }

    private void TiltGestureTransformCompleted(object sender, System.EventArgs e) {
        isTilting = false;
    }

    private float ClampAngle(float angle, float min, float max) {
        if (angle < 90 || angle > 270) {       // if angle in the critic region...
            if (angle > 180) angle -= 360;  // convert all angles to -180..+180
            if (max > 180) max -= 360;
            if (min > 180) min -= 360;
        }
        angle = Mathf.Clamp(angle, min, max);
        if (angle < 0) angle += 360;  // if angle negative, convert to 0..360
        return angle;
    }
}
