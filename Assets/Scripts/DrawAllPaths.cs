﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawAllPaths : MonoBehaviour {
    public LocationService locationService;
    //public float nodeSize = 0.05f;
    public Transform map;
    public Transform nodePrefab;
    public Material pathMaterial;


    // Use this for initialization
    void Start () {
        if (locationService.locationMode == LocationMode.Both) {
            List<Node> nodesOnThisFloor = ServerManager.Instance.nodesOnThisFloor;
            foreach (Node node in nodesOnThisFloor) {
                Transform nodeGO = Instantiate(nodePrefab);
                nodeGO.parent = map;
                nodeGO.localPosition = locationService.ConvertFromCMSCoordinates(node.location, true);
                nodeGO.rotation = Quaternion.identity;
                nodeGO.localScale = Vector3.one * 7.5f;
            }

            List<Path> pathsOnThisFloor = ServerManager.Instance.pathsOnThisFloor;
            foreach (Path path in pathsOnThisFloor) {
                Vector3 startNodePosition = locationService.ConvertFromCMSCoordinates(path.startNode.location);
                Vector3 endNodePosition = locationService.ConvertFromCMSCoordinates(path.endNode.location);
                GameObject pathGO = new GameObject();
                pathGO.transform.parent = map;
                //pathGO.transform.localPosition = (startNodePosition + endNodePosition) / 2;
                LineRenderer lineRenderer = pathGO.AddComponent<LineRenderer>();
                lineRenderer.material = pathMaterial;
                lineRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                lineRenderer.useWorldSpace = false;
                lineRenderer.startWidth = 0.02f;
                lineRenderer.endWidth = 0.02f;
                lineRenderer.positionCount = 2;
                lineRenderer.SetPosition(0, startNodePosition);
                lineRenderer.SetPosition(1, endNodePosition);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    //private void OnPostRender() {
    //    if (locationService.locationMode == LocationMode.Both) {
    //        List<Node> nodesOnThisFloor = ServerManager.Instance.nodesOnThisFloor;
    //        foreach (Node node in nodesOnThisFloor) {
    //            Vector3 nodePosition = locationService.ConvertFromCMSCoordinates(node.location);
    //            nodePosition = RotatePointAroundPivot(nodePosition, touchCameraController.rotationPivot, -1 * touchCameraController.rotationAngle * Vector3.up);
    //            GL.Begin(GL.QUADS);
    //            GL.Color(Color.yellow);
    //            GL.Vertex(nodePosition - Vector3.back * nodeSize);
    //            GL.Vertex(nodePosition - Vector3.right * nodeSize);
    //            GL.Vertex(nodePosition - Vector3.forward * nodeSize);
    //            GL.Vertex(nodePosition - Vector3.left * nodeSize);
    //            GL.End();
    //        }
    //        List<Path> pathsOnThisFloor = ServerManager.Instance.pathsOnThisFloor;
    //        foreach (Path path in pathsOnThisFloor) {
    //            Vector3 startNodePosition = locationService.ConvertFromCMSCoordinates(path.startNode.location);
    //            startNodePosition = RotatePointAroundPivot(startNodePosition, touchCameraController.rotationPivot, -1 * touchCameraController.rotationAngle * Vector3.up);
    //            Vector3 endNodePosition = locationService.ConvertFromCMSCoordinates(path.endNode.location);
    //            endNodePosition = RotatePointAroundPivot(endNodePosition, touchCameraController.rotationPivot, -1 * touchCameraController.rotationAngle * Vector3.up);
    //            GL.Begin(GL.LINES);
    //            GL.Color(Color.green);
    //            GL.Vertex(startNodePosition);
    //            GL.Vertex(endNodePosition);
    //            GL.End();
    //        }
    //    }
    //}

    //private Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles) {
    //    Vector3 dir = point - pivot; // get point direction relative to pivot
    //    dir = Quaternion.Euler(angles) * dir; // rotate it
    //    point = dir + pivot; // calculate rotated point
    //    return point; // return it
    //}
}
