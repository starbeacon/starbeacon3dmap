﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HouseToggler : MonoBehaviour {

    public Transform housePrefab;
    public Vector3 housePosition;
    public Transform parentTransform;
    public Button toggleHouseButton;
    public Text houseTriCount;

    private Transform house;
    private bool houseExists;

	// Use this for initialization
	void Start () {
        houseExists = false;
        toggleHouseButton.GetComponentInChildren<Text>().text = "Build A House";
        houseTriCount.gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ToggleHouse()
    {
        if (houseExists)
        {
            Destroy(house.gameObject);
            houseExists = false;
            toggleHouseButton.GetComponentInChildren<Text>().text = "Build A House";
            houseTriCount.gameObject.SetActive(false);
        }
        else
        {
            house = Instantiate(housePrefab);
            house.parent = parentTransform;
            house.position = housePosition; // new Vector3(0, 0, -3.0f);
            houseExists = true;
            toggleHouseButton.GetComponentInChildren<Text>().text = "Demolish The House";

            int triCount = 0;
            MeshFilter[] meshFilters = house.GetComponentsInChildren<MeshFilter>();
            foreach (MeshFilter meshFilter in meshFilters)
            {
                triCount += meshFilter.mesh.triangles.Length;
                houseTriCount.gameObject.SetActive(true);
                houseTriCount.text = triCount.ToString();
            }
        }
    }
}
