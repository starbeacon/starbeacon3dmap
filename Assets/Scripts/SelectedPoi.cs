﻿using TouchScript.Gestures;
using UnityEngine;

public class SelectedPoi : MonoBehaviour {
    public Material normalMaterial;
    public Material selectedMaterial;
    public Camera cam;
    public float focusedCamYOffset;
    public float focusedZenithAngle;
    public float focusedCamZOffset;
    [HideInInspector]
    public FloorManager floorManager;
    [HideInInspector]
	public Poi poiData;
	public delegate void SelectedPoiCallback (Poi poi);
	public static event SelectedPoiCallback selectedPoiHandler;

    private TapGesture tapGesture;
    private TapGesture doubleTapGesture;

    private void OnEnable() {
        tapGesture = GetComponents<TapGesture>()[0];
        tapGesture.Tapped += SelectedPOiOnTappedHandler;
        doubleTapGesture = GetComponents<TapGesture>()[1];
        doubleTapGesture.Tapped += SelectedPoiOnDoubleTappedHandler;
    }

    private void OnDisable() {
        tapGesture.Tapped -= SelectedPOiOnTappedHandler;
        doubleTapGesture.Tapped -= SelectedPoiOnDoubleTappedHandler;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void SelectedPOiOnTappedHandler(object sender, System.EventArgs e) {
        if (this.floorManager.selectedPoi == this) {  //deselect current POI
            this.ToggleSelection(false);
            this.floorManager.selectedPoi = null;
			if (selectedPoiHandler != null) {
				selectedPoiHandler (null);
			}
        } else {
            SelectThisPoi();
        }
    }

    private void SelectedPoiOnDoubleTappedHandler(object sender, System.EventArgs e) {
        SelectThisPoi();
        MoveAndRotateCam();
    }

    private void SelectThisPoi() {
        if (this.floorManager.selectedPoi != null) {
            this.floorManager.selectedPoi.ToggleSelection(false);
        }
        this.ToggleSelection(true);
        this.floorManager.selectedPoi = this;
        if (selectedPoiHandler != null) {
            selectedPoiHandler(poiData);
        }
    }

    private void ToggleSelection(bool selected) {
        this.gameObject.GetComponent<MeshRenderer>().material = selected ? selectedMaterial : normalMaterial;
        //TODO: show/hide the POI detail panel based on the "selected" parameter
    }

    private void MoveAndRotateCam() {
        Vector3 poiCenter = this.GetComponent<MeshRenderer>().bounds.center;
        Vector3 cameraPos = poiCenter + new Vector3(0f, focusedCamYOffset, 0f);
        cameraPos = RotatePointAroundPivot(cameraPos, poiCenter, new Vector3(-1 * (90 - focusedZenithAngle), 0f, 0f));
        cameraPos += Vector3.forward * focusedCamZOffset;
        iTween.MoveTo(cam.gameObject, iTween.Hash("position", cameraPos, "time", 1f, "easetype", iTween.EaseType.easeOutSine));

        Vector3 cameraRotationEuler = new Vector3(focusedZenithAngle, 0f, 0f);
        iTween.RotateTo(cam.gameObject, iTween.Hash("rotation", cameraRotationEuler, "time", 1f, "easetype", iTween.EaseType.easeOutSine));

        //Vector3 cameraRotationEuler = CameraAngleHelper.CameraZenithAngle(cameraPos.y);
        //iTween.RotateTo(cam.gameObject, iTween.Hash("rotation", cameraRotationEuler, "time", 1f, "easetype", iTween.EaseType.easeOutSine));
    }

    private Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles) {
        Vector3 dir = point - pivot;
        dir = Quaternion.Euler(angles) * dir;
        point = dir + pivot;
        return point;
    }
}
