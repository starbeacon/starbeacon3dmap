﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CameraAngleHelper {
    public static float farthestZoomDistance;
    public static float closestZoomDistance;

    public static Vector3 CameraZenithAngle(float currentCameraPosY) {
        float heightRatio = currentCameraPosY / (farthestZoomDistance - closestZoomDistance);
        float camRotateDegree = EasingFunction.EaseOutCubic(0f, 90f, heightRatio);
        return new Vector3(camRotateDegree, 0f, 0f);
    }
}
