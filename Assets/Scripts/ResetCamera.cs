﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetCamera : MonoBehaviour {

    public Transform pivot;
    public Camera mainCamera;

    private Vector3 orgPivotPos;
    private Quaternion orgPivotRotation;
    private Vector3 orgCamPos;
    private Quaternion orgCamRotation;

	void Start () {
        orgPivotPos = pivot.localPosition;
        orgPivotRotation = pivot.localRotation;
        orgCamPos = mainCamera.transform.localPosition;
        orgCamRotation = mainCamera.transform.localRotation;
	}

    public void Reset()
    {
        pivot.localPosition = orgPivotPos;
        pivot.localRotation = orgPivotRotation;
        mainCamera.transform.localPosition = orgCamPos;
        mainCamera.transform.localRotation = orgCamRotation;
    }
}
