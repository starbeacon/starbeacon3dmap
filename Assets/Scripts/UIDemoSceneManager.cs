﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using SimpleJSON;

public class UIDemoSceneManager : MonoBehaviour {
	public Text infoText;
	[HideInInspector]
	public static UIDemoSceneManager Instance;
	public MapMenuController mapMenuController;

	public int projectId = 44; //44 is HKIA
	#if UNITY_EDITOR
	public const string baseUrl = "https://sit-cdn.starbeacon.io/api/";
	#endif
	[HideInInspector]
	public List<Poi> pois;
	[HideInInspector]
	public List<Tags> tagsList;
	[HideInInspector]
	public List<PoiCategory> poiCategoriesList;
	[HideInInspector]
	public List<Brand> brandList;
	[HideInInspector]
	public List<PoiCategory> flattenPoiCategoriesList;
	[HideInInspector]
	public List<Area> areaList;

	private AndroidJavaObject currentActivity;

	private void Awake() {
		if (Instance == null) {
			Instance = this;
		} else if (Instance != this) {
			Destroy(gameObject);
		}
		DontDestroyOnLoad(gameObject);
	}

	// Use this for initialization
	void Start () {
		#if UNITY_EDITOR
		string mapUrl = baseUrl + (projectId == 0? "list_maps":(projectId + "/list_maps"));
		StartCoroutine(downloadJSON(mapUrl, OnDownloadMapJSON));

		string tagsUrl = baseUrl + (projectId == 0? "tags":(projectId + "/tags"));
		StartCoroutine(downloadJSON(tagsUrl, OnDownloadTagJSON));


		#elif UNITY_IOS
		PMPNativeBridgeForIOS.sharedInstance.downloadMapResultHandler += OnDownloadMapJSON;
		PMPNativeBridgeForIOS.sharedInstance.downloadTagResultHandler += OnDownloadTagJSON;
		PMPNativeBridgeForIOS.sharedInstance.Init(projectId);
		#elif UNITY_ANDROID
		Debug.Log ("ServerManager start");
		PMPNativeBridgeForAndroid.sharedInstance.downloadMapResultHandler += OnDownloadMapJSON;
		PMPNativeBridgeForAndroid.sharedInstance.downloadTagResultHandler += OnDownloadTagJSON;
		PMPNativeBridgeForAndroid.sharedInstance.Init(projectId);
		#endif
	}

	private void Update() {

	}

	private void OnDestroy() {
		#if UNITY_IOS
		PMPNativeBridgeForIOS.sharedInstance.downloadMapResultHandler -= OnDownloadMapJSON;
		PMPNativeBridgeForIOS.sharedInstance.downloadTagResultHandler -= OnDownloadTagJSON;
		#endif
	}
		
	private void OnDownloadMapJSON (bool success,string response){
		if (success) {
			JSONNode responseJSON = JSON.Parse (response);

			JSONArray poiJSONArray = responseJSON ["pois"].AsArray;
			pois = new List<Poi> ();

			for(int i = 0; i < poiJSONArray.Count; i++){
				JSONNode jn = poiJSONArray [i];
				Poi p = new Poi (jn);
				pois.Add (p);
			}
			Debug.Log ("OnDownloadMapJSON, number of pois:" + pois.Count );

			JSONArray poiCategoriesJSONArray = responseJSON ["poi_categories"].AsArray;
			poiCategoriesList = new List<PoiCategory> ();
		
			for(int i = 0 ; i< poiCategoriesJSONArray.Count; i++){
				JSONNode jn = poiCategoriesJSONArray [i];
				PoiCategory pc = new PoiCategory (jn);
				poiCategoriesList.Add (pc);
			}

			flattenPoiCategoriesList = new List<PoiCategory> ();
			for(int i = 0; i < poiCategoriesList.Count; i++){
				flattenPoiCategoriesList.AddRange (PoiCategory.GetAllSubCategories(poiCategoriesList[i]));
			}
			Debug.Log ("OnDownloadMapJSON, number of poiCategories:" + flattenPoiCategoriesList.Count );


			JSONArray brandJSONArray = responseJSON ["brands"].AsArray;
			brandList = new List<Brand> ();

			for(int i = 0 ; i< brandJSONArray.Count; i++){
				JSONNode jn = brandJSONArray [i];
				Brand b = new Brand (jn);
				brandList.Add (b);
			}
			Debug.Log ("OnDownloadMapJSON, number of brands:" + brandList.Count );


			JSONArray areaJSONArray = responseJSON ["areas"].AsArray;

			areaList = new List<Area> ();
			for(int i = 0; i < areaJSONArray.Count; i++){
				JSONNode jn = areaJSONArray [i];
				Area a = new Area (jn);
				areaList.Add (a);
			}
			Debug.Log ("OnDownloadMapJSON, number of areas:" + areaJSONArray.Count );

			mapMenuController.SetUpAreaDropDown (areaList);



			infoText.text = "Download JSON success";
		}else{
			infoText.text = "Download JSON failed";
			#if UNITY_IOS
			//retry
			PMPNativeBridgeForIOS.sharedInstance.Init(projectId);
			#endif
		}
	}

	private void OnDownloadTagJSON(bool success, string response){
		Debug.Log ("OnDownloadTagJSON" + (success ? "success":"failed"));
		JSONArray responseJSON = JSON.Parse (response).AsArray;

		tagsList = new List<Tags> ();

		for(int i = 0; i < responseJSON.Count; i++){
			Tags tags = new Tags (responseJSON[i]);
			tagsList.Add (tags);
		}
			
		Debug.Log ("OnDownloadTagJSON, number of tags:" + tagsList.Count );
	}

	#region editor utilites
	delegate void DownloadJSONResult(bool success, string result);

	private IEnumerator downloadJSON(string url, DownloadJSONResult downloadJSONResult){
		WWW www = new WWW (url);
		yield return www;
		if (www.error == null || www.error.Length < 1) {

			downloadJSONResult (true, www.text);
		} else {
			downloadJSONResult (false,"");
		}
	}

	#endregion
}
