﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class InspectWorldPos : MonoBehaviour {

    public Vector3 worldPos;

	// Use this for initialization
	void Start () {
        worldPos = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
