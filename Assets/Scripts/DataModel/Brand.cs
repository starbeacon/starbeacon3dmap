﻿using System;
using SimpleJSON;

public class Brand {
	public double brandId;
	public double[] poiCategoryIds;
	public LocalizedText[] name;

	public Brand(JSONNode jn){
		brandId = jn ["id"].AsDouble;

		JSONArray nameJSONArray = jn ["name"].AsArray;
		name = new LocalizedText [nameJSONArray.Count];
		for(int i = 0; i < name.Length; i++){
			name [i] = new LocalizedText (nameJSONArray[i]);
		}

		JSONArray poiCategoryIdsJSONArray = jn ["poi_category_ids"].AsArray;
		if (poiCategoryIdsJSONArray != null) {
			poiCategoryIds = new double[poiCategoryIdsJSONArray.Count];
			for (int i = 0; i < poiCategoryIdsJSONArray.Count; i++) {
				poiCategoryIds [i] = poiCategoryIdsJSONArray [i].AsDouble;
			}
		} else {
			poiCategoryIds = new double[0];
		}
	}
}
