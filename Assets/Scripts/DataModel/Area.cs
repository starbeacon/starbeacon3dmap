﻿using System;
using SimpleJSON;
using System.Collections.Generic;

public class Area  {
	public double areaIdentifier;
	public double mapId;
	public LocalizedText[] name;
	public List<Area> subAreas;

	public Area(JSONNode jn){
		areaIdentifier = jn ["id"].AsDouble;
		mapId = jn ["map_id"].AsDouble;

		JSONArray nameJSONArray = jn ["name"].AsArray;
		name = new LocalizedText [nameJSONArray.Count];
		for(int i = 0; i < name.Length; i++){
			name [i] = new LocalizedText (nameJSONArray[i]);
		}

		JSONArray subAreaJSONArray = jn ["sub_areas"].AsArray;
		subAreas = new List<Area> ();

		if (subAreaJSONArray != null && subAreaJSONArray.Count > 0) {
			for (int i = 0; i < subAreaJSONArray.Count; i++) {
				Area subArea = new Area (subAreaJSONArray [i]);
				subAreas.Add (subArea);
			}
		}
	}
}
