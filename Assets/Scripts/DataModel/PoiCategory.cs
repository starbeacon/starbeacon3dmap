﻿using System;
using SimpleJSON;
using System.Collections.Generic;

public class PoiCategory {

	public double poiCategoryId;
	public LocalizedText[] name;
	public List<PoiCategory> subCategories;
	public bool unsearchable;

	public PoiCategory(JSONNode jn){
		poiCategoryId = jn ["id"].AsDouble;
		JSONArray nameJSONArray = jn ["name"].AsArray;
		name = new LocalizedText [nameJSONArray.Count];
		for(int i = 0; i < name.Length; i++){
			name [i] = new LocalizedText (nameJSONArray[i]);
		}

		subCategories = new List<PoiCategory> ();
		JSONArray subCategoriesJSONArray = jn ["sub_categories"].AsArray;
		if (subCategoriesJSONArray != null && subCategoriesJSONArray.Count > 0) {
			for(int i = 0; i < subCategoriesJSONArray.Count ; i++){
				PoiCategory pc = new PoiCategory (subCategoriesJSONArray[i]);
				subCategories.Add (pc);
			}
		}

		unsearchable = jn ["unsearchable"].AsBool;
	}

	#region utilities
	public static List<PoiCategory> GetAllSubCategories(PoiCategory pc){
		List<PoiCategory> result = new List<PoiCategory> ();
		result.Add (pc);
		for(int i = 0; i < pc.subCategories.Count; i++){
			result.AddRange (GetAllSubCategories(pc.subCategories[i]));
		}
		return result;
	}
	#endregion
}
