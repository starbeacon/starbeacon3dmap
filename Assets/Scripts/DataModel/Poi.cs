﻿using System;
using SimpleJSON;

public class Poi  {
	public double poiIdentifer;
	public LocalizedText[] name;
	public LocalizedText[] address;
	public double x;
	public double y;
	public double z;
	public double brandId;
	public double [] poiCategoryIds;
	public double mapId;

	public Poi(){
	
	}

	public Poi(JSONNode jn){
		poiIdentifer = jn ["id"].AsDouble;
		JSONArray nameJSONArray = jn ["name"].AsArray;
		name = new LocalizedText [nameJSONArray.Count];
		for(int i = 0; i < name.Length; i++){
			name [i] = new LocalizedText (nameJSONArray[i]);
		}

		JSONArray locationJSONArray = jn ["location"].AsArray;
		address = new LocalizedText [locationJSONArray.Count];
		for(int i = 0; i < name.Length; i++){
			address [i] = new LocalizedText (locationJSONArray[i]);
		}

		x = jn ["x"].AsDouble;
		y = jn ["y"].AsDouble;
		z = jn ["z"].AsDouble;
		brandId = jn ["brand_id"].AsDouble;
		mapId = jn ["map_id"].AsDouble;

		JSONArray poiCategoryIdsJSONArray = jn ["poi_category_ids"].AsArray;
		if (poiCategoryIdsJSONArray != null) {
			poiCategoryIds = new double[poiCategoryIdsJSONArray.Count];
			for (int i = 0; i < poiCategoryIdsJSONArray.Count; i++) {
				poiCategoryIds [i] = poiCategoryIdsJSONArray [i].AsDouble;
			}
		} else {
			poiCategoryIds = new double[0];
		}


	}
}
