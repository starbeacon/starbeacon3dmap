﻿using System;
using SimpleJSON;

public class LocalizedText  {
	public double languageId;
	public string content;
	public static int currentLanguageId = 1;
	public LocalizedText(JSONNode jn){
		languageId = jn ["language_id"].AsDouble;
		content = jn["content"];
	}

	#region utilities
	public static string GetLocalizedText(LocalizedText[] text){

		for (int i = 0; i < text.Length; i++) {
			if (text [i].languageId == currentLanguageId) {
				return text [i].content;
			}
		}
		return text [0].content;
	}
	#endregion
}
