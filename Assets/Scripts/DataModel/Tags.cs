﻿using System;
using System.Collections.Generic;
using SimpleJSON;

public class Tags {
	public string content;
	public List<Tag> types;
	public Tags(JSONNode jn){
		content = jn ["content"];

		types = new List<Tag> ();
		JSONArray typesArray = jn ["types"].AsArray;

		for(int j = 0; j < typesArray.Count; j++){
			Tag tag = new Tag (typesArray[j]);
			types.Add (tag);
		}
	}
}
