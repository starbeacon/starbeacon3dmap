﻿using System;
using SimpleJSON;

public class Tag  {
	public string name;
	public double [] ids;

	public Tag(JSONNode jn){
		name = jn ["name"];
		JSONArray idArray =  jn ["ids"].AsArray;
		ids = new double[idArray.Count];
		for(int k = 0 ; k < idArray.Count; k++){
			ids [k] = idArray [k].AsDouble;
		}
	}
}
