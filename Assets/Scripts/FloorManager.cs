﻿using System.Collections;
using System.Collections.Generic;
using TouchScript.Gestures;
using UnityEngine;

public class FloorManager : MonoBehaviour {
    public LocationService locationService;
    public Transform map;
    public bool showPoiCoordinates = false;
    public Transform poiPrefab;
    public Material poiMaterial;
    public Material selectedMaterial;
    public float doubleTapThreshold = 0.3f;
    public Camera cam;
    public float focusedPoiCamYOffset = 6f;
    public float focusedPoiCamZenithAngle = 45f;
    public float focusedPoiCamZOffset = -3f;

    [HideInInspector]
    public SelectedPoi selectedPoi;

	// Use this for initialization
	void Start () {
        //Scan the list of POIs and make the related gameobject clickable
        List<Poi> poisOnThisFloor = ServerManager.Instance.poisOnThisFloor;
        foreach (Poi poi in poisOnThisFloor) {
            Location location = new Location() {
                x = poi.x,
                y = poi.y,
                z = poi.z,
                name = poi.mapId.ToString()
            };
            Vector3 pos = locationService.ConvertFromCMSCoordinates(location, true);
            pos.y = 0.65f;
            EnablePoi(pos, poi);
        }

        //Mock data for Editor only
        //Vector3[] mockPositions = new Vector3[] {
        //    new Vector3(-3.119189f, 0.65f, -0.4357407f),
        //    new Vector3(-2.785478f, 0.65f, -0.07241744f),
        //    new Vector3(-2.733302f, 0.65f, 0.9928555f),
        //    new Vector3(-1.284216f, 0.65f, 2.999566f),
        //    new Vector3(-1.601049f, 0.65f, -12.07802f),
        //    new Vector3(-2.646877f, 0.65f, -12.07802f)
        //};
        //foreach (Vector3 mockPos in mockPositions) {
        //    EnablePoi(mockPos);
        //}
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void EnablePoi(Vector3 pos, Poi poi = null) {
        if (showPoiCoordinates) {
            Transform poiTransform = Instantiate(poiPrefab);
            poiTransform.parent = this.map;
            poiTransform.localPosition = pos;
            poiTransform.localScale = Vector3.one * 5f;
        }
        Vector3 rayWorldPos = this.map.TransformPoint(pos);
        Ray ray = new Ray(rayWorldPos, Vector3.down);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit)) {
            if (hit.collider != null) {
                GameObject poiGameObject = hit.collider.gameObject;
                MeshRenderer meshRenderer = poiGameObject.GetComponent<MeshRenderer>();
                meshRenderer.material = poiMaterial;
                //assign POI behavior such as tapping with fingers
                TapGesture tapGesture = poiGameObject.AddComponent<TapGesture>();
                TapGesture doubleTapGesture = poiGameObject.AddComponent<TapGesture>();
                doubleTapGesture.NumberOfTapsRequired = 2;
                doubleTapGesture.TimeLimit = doubleTapThreshold;
                tapGesture.RequireGestureToFail = doubleTapGesture;

                SelectedPoi selectedPoi = poiGameObject.AddComponent<SelectedPoi>();
                selectedPoi.floorManager = this;
                selectedPoi.normalMaterial = this.poiMaterial;
                selectedPoi.selectedMaterial = this.selectedMaterial;
                selectedPoi.cam = this.cam;
                selectedPoi.focusedCamYOffset = this.focusedPoiCamYOffset;
                selectedPoi.focusedZenithAngle = this.focusedPoiCamZenithAngle;
                selectedPoi.focusedCamZOffset = this.focusedPoiCamZOffset;

                if (poi != null) {
                    selectedPoi.poiData = poi;
                }
            }
        }
    }
}
