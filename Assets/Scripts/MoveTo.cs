﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveTo : MonoBehaviour {

    public Transform arrowPrefab;
    public Vector3 arrowStartPoint;
    public Transform goal;
    public float nextArrowDelay;

    private List<Transform> arrows;
    private NavMeshPath path;
    private Quaternion orgArrowRotation;
    private LineRenderer lineRenderer;
    private bool isSpawningArrows = false;

    // Use this for initialization
    void Start () {
        arrows = new List<Transform>();
        path = new NavMeshPath();
        lineRenderer = this.GetComponent<LineRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartMoving() {
        if (path.status == NavMeshPathStatus.PathInvalid) {
            NavMesh.CalculatePath(arrowStartPoint, goal.position, NavMesh.AllAreas, path);
            lineRenderer.positionCount = path.corners.Length;
            lineRenderer.SetPositions(path.corners);
            isSpawningArrows = true;
        }
        StartCoroutine(StartMovingCoroutine());
    }

    private IEnumerator StartMovingCoroutine() {
        do {
            var arrow = Instantiate(arrowPrefab, this.transform);
            NavMeshAgent agent = arrow.GetComponent<NavMeshAgent>();
            agent.Warp(arrowStartPoint);

            RespawningArrow respawningArrow = arrow.GetComponent<RespawningArrow>();
            respawningArrow.arrowStartPoint = arrowStartPoint;

            orgArrowRotation = arrow.localRotation;
            arrows.Add(arrow.transform);

            float delay = 0.0f;
            if (arrows.Count > 1)
                delay = nextArrowDelay;
            yield return new WaitForSeconds(delay);
            //respawningArrow.path = this.path;
            respawningArrow.hasStartedMoving = true;
            agent.SetPath(this.path);
        } while (!IsAgentCompletedPath(arrows[0].GetComponent<NavMeshAgent>()) && isSpawningArrows);

        isSpawningArrows = false;
        for (int i = 0; i < arrows.Count; i++) {
            arrows[i].GetComponent<RespawningArrow>().path = this.path;
        }
    }

    public void ResetArrowPos() {
        //NavMeshAgent agent = arrow.GetComponent<NavMeshAgent>();
        //agent.Warp(arrowStartPoint);
        //arrow.localRotation = orgArrowRotation;

        //lineRenderer.positionCount = 2;
        //lineRenderer.SetPosition(0, Vector3.zero);
        //lineRenderer.SetPosition(1, Vector3.zero);
    }

    private bool IsAgentCompletedPath(NavMeshAgent agent) {
        if (!agent.pathPending) {
            if (agent.remainingDistance <= agent.stoppingDistance) {
                if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f) {
                    return true;
                }
            }
        }
        return false;
    }
}
