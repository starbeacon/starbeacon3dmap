﻿using UnityEngine;

public class ServerManagerNotifier : AndroidJavaProxy {
    private AndroidJavaObject currentActivity;

//    public delegate void OnSuccessHandler(AndroidJavaObject currentActivity);
//    public event OnSuccessHandler OnSuccess;
//
//    public delegate void OnFailedHandler();
//    public event OnFailedHandler OnFailed;


	public delegate void OnDownloadJSONFinishedHandler(bool success, string response);
	public event OnDownloadJSONFinishedHandler OnDownloadJSONFinished;

    public ServerManagerNotifier(AndroidJavaObject currentActivity) : base("com.pmp.mapsdk.cms.PMPServerManagerNotifier") {
        this.currentActivity = currentActivity;
    }

	void didSuccess(string response) {
		if (this.OnDownloadJSONFinished != null) OnDownloadJSONFinished(true,response);
    }

    void didFailure() {
		if (this.OnDownloadJSONFinished != null) OnDownloadJSONFinished(true,"");
    }
}