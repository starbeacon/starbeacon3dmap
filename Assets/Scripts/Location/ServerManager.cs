﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using SimpleJSON;

public class ServerManager : MonoBehaviour {
    public static ServerManager Instance;
    
    public int projectId = 44; //44 is HKIA
    public double mapId = -1; //30 is HKIA L5
	#if UNITY_EDITOR
	public const string baseUrl = "https://sit-cdn.starbeacon.io/api/";
	#endif
    public Text infoText;

    [HideInInspector]
    public Hashtable nodes;
    [HideInInspector]
    public Path[] paths;
    [HideInInspector]
    public List<Node> nodesOnThisFloor;
    [HideInInspector]
    public List<Path> pathsOnThisFloor;
	[HideInInspector]
	public List<Poi> pois;
    [HideInInspector]
    public List<Poi> poisOnThisFloor;

    private bool isDownloadedMap = false;
    private bool isLocatedFloor = false;
    private AndroidJavaObject currentActivity;

    private void Awake() {
        if (Instance == null) {
            Instance = this;
        } else if (Instance != this) {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start () {
		#if UNITY_EDITOR
		string mapUrl = baseUrl + (projectId == 0? "list_maps":(projectId + "/list_maps"));
		StartCoroutine(downloadJSON(mapUrl, OnDownloadMapJSON));
		#elif UNITY_IOS
		PMPNativeBridgeForIOS.sharedInstance.downloadMapResultHandler += OnDownloadMapJSON;
		PMPNativeBridgeForIOS.sharedInstance.Init(projectId);
		#elif UNITY_ANDROID
		Debug.Log ("ServerManager start");
		PMPNativeBridgeForAndroid.sharedInstance.downloadMapResultHandler += OnDownloadMapJSON;
		PMPNativeBridgeForAndroid.sharedInstance.Init(projectId);
		#endif
    }

    private void Update() {
        if (infoText != null) {
            if (isDownloadedMap) {
                infoText.text = "finished downloading map";
            }
            if (isLocatedFloor) {
                infoText.text = "floor located";
            }
        }
    }

    private void OnDestroy() {
#if UNITY_IOS
		PMPNativeBridgeForIOS.sharedInstance.downloadMapResultHandler -= OnDownloadMapJSON;
		PMPNativeBridgeForIOS.sharedInstance.onDidLocationUpdatedHandler -= OnIndoorLocationUpdated;
#endif
    }
		
	private void OnIndoorLocationUpdated(Location location){
		Debug.Log ("OnIndoorLocationUpdated start: " + location.name);
		if (Double.TryParse(location.name, out this.mapId)) {
			nodesOnThisFloor = new List<Node>();
			foreach (Node node in this.nodes.Values) {
				if (node.mapId == this.mapId) {
					nodesOnThisFloor.Add(node);
				}
			}
			pathsOnThisFloor = new List<Path>();
			foreach (Path path in this.paths) {
				if (path.startNode != null && path.startNode.mapId == this.mapId && path.endNode != null && path.endNode.mapId == this.mapId) {
					pathsOnThisFloor.Add(path);
				}
			}
            poisOnThisFloor = new List<Poi>();
            foreach (Poi poi in this.pois) {
                if (poi.mapId == this.mapId) {
                    poisOnThisFloor.Add(poi);
                }
            }

            Debug.Log ("OnIndoorLocationUpdated about to switch scene");

			isLocatedFloor = true;
			#if UNITY_IOS
			PMPNativeBridgeForIOS.sharedInstance.onDidLocationUpdatedHandler -= OnIndoorLocationUpdated;
			#elif UNITY_ANDROID
			PMPNativeBridgeForAndroid.sharedInstance.onDidLocationUpdatedHandler -= OnIndoorLocationUpdated;
			#endif
			SceneManager.LoadScene("hkia", LoadSceneMode.Single);
		}
	}

	private void OnDownloadMapJSON (bool success,string response){
		if (success) {
			infoText.text = "Download JSON success";

			JSONNode responseJSON = JSON.Parse (response);

			nodes = new Hashtable();
			JSONArray nodesJSONArray = responseJSON ["nodes"].AsArray;
			for(int i = 0; i < nodesJSONArray.Count; i++){
				JSONNode jn = nodesJSONArray [i];
				Node n = new Node (jn);
				nodes.Add (n.id, n);
			}
			Debug.Log ("OnDownloadMapJSON, number of nodes:" + nodes.Count );

			JSONArray pathsJSONArray = responseJSON ["paths"].AsArray;
			paths = new Path[pathsJSONArray.Count];

			for(int i = 0;i < paths.Length; i++){
				JSONNode jn = pathsJSONArray [i]; 
				Path p = new Path (jn, nodes);

				this.paths [i] = p;
			}

			JSONArray poiJSONArray = responseJSON ["pois"].AsArray;
			pois = new List<Poi> ();

			for(int i = 0; i < poiJSONArray.Count; i++){
				JSONNode jn = poiJSONArray [i];
				Poi p = new Poi (jn);

				pois.Add (p);
			}
			Debug.Log ("OnDownloadMapJSON, number of pois:" + pois.Count );


			isDownloadedMap = true;

			Debug.Log ("OnDownloadMapJSON, number of paths:" + paths.Length );
			#if UNITY_IOS
			PMPNativeBridgeForIOS.sharedInstance.onDidLocationUpdatedHandler += OnIndoorLocationUpdated;
			#elif UNITY_ANDROID
			PMPNativeBridgeForAndroid.sharedInstance.onDidLocationUpdatedHandler += OnIndoorLocationUpdated;
			#endif

		}else{
			infoText.text = "Download JSON failed";
			#if UNITY_IOS
			//retry
			PMPNativeBridgeForIOS.sharedInstance.Init(projectId);
			#endif
		}
	}

	#region editor utilites
	delegate void DownloadJSONResult(bool success, string result);

	private IEnumerator downloadJSON(string url, DownloadJSONResult downloadJSONResult){
		WWW www = new WWW (url);
		yield return www;
		if (www.error == null || www.error.Length < 1) {

			downloadJSONResult (true, www.text);
		} else {
			downloadJSONResult (false,"");
		}
	}

	#endregion
}
