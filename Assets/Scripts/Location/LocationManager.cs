﻿using UnityEngine;
#if UNITY_ANDROID
public class LocationManager : AndroidJavaProxy {
    private static LocationManager instance;

    private Location location;

    private LocationManager() : base("com.pmp.mapsdk.location.PMPLocationManagerNotifier") {
        location = new Location();
    }

    public static LocationManager Instance {
        get {
            if (instance == null) {
                instance = new LocationManager();
            }
            return instance;
        }
    }

    public delegate void OnIndoorLocationUpdatedHandler(Location location);
    public event OnIndoorLocationUpdatedHandler OnIndoorLocationUpdated;

    public delegate void OnIndoorTransmissionsUpdatedHandler(int beaconCount);
    public event OnIndoorTransmissionsUpdatedHandler OnIndoorTransmissionsUpdated;

    public delegate void OnIndoorExitRegionHandler();
    public event OnIndoorExitRegionHandler OnIndoorExitRegion;

    public delegate void OnCompassUpdatedHandler(double direction);
    public event OnCompassUpdatedHandler OnCompassUpdated;

    public void didIndoorLocationUpdated(AndroidJavaObject beaconType, AndroidJavaObject androidLocationObj) {
		Debug.Log ("LocationManager onDidLocationUpdated");
        if (this.OnIndoorLocationUpdated != null) {
            location.x = androidLocationObj.Call<double>("getX");
            location.y = androidLocationObj.Call<double>("getY");
            location.z = androidLocationObj.Call<double>("getZ");
            location.name = androidLocationObj.Call<string>("getName");

            this.OnIndoorLocationUpdated(location);
        }
    }

    public void didIndoorTransmissionsUpdated(AndroidJavaObject transmissions) {
        if (this.OnIndoorTransmissionsUpdated != null) {
            this.OnIndoorTransmissionsUpdated(transmissions.Call<int>("size"));
        }
    }

    public void didIndoorExitRegion() {
        if (this.OnIndoorExitRegion != null) {
            this.OnIndoorExitRegion();
        }
    }

    public void didOutdoorLocationsUpdated() { }

    public void didCompassUpdated(double direction) {
        //TODO: this method in Java obj is depricated. Look for other way of implementation
        //if (this.OnCompassUpdated != null) {
        //    Debug.Log("!!! Direction changed");
        //    OnCompassUpdated(direction);
        //}
    }

    public void didProximityEnterRegion(int region) { }
    public void didProximityExitRegion(int region) { }

    public void didError(AndroidJavaObject error) {
    }
}
#endif