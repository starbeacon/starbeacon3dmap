﻿using System;

[Serializable]
public class Location {
    public double x;
    public double y;
    public double z;   //This is not used
    public string name; //This is used as the floor
}
