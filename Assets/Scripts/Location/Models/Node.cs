﻿using System;
using SimpleJSON;

[Serializable]
public class Node {
    public double id;
    public double mapId;
    public Location location;

	public Node(JSONNode jn){
		id = jn ["id"].AsDouble;
		mapId = jn ["map_id"].AsDouble;
		Location l = new Location ();
		l.x = jn ["x"].AsDouble;
		l.y = jn ["y"].AsDouble;
		l.z = jn ["z"].AsDouble;
		l.name = jn ["map_id"].AsDouble + "";
		location = l;
	}
}