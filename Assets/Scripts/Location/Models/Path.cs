﻿using System;
using System.Collections;
using UnityEngine;
using SimpleJSON;

[Serializable]
public class Path {
    public double id;
    public Node startNode;
    public Node endNode;

	public Path(JSONNode jn, Hashtable nodes){
		double startNodeId = jn ["start_node_id"].AsDouble;
		double endNodeId = jn ["end_node_id"].AsDouble;
		if (nodes.ContainsKey (startNodeId)) {
			startNode = nodes [startNodeId] as Node;
		}
		if (nodes.ContainsKey (endNodeId)) {
			endNode = nodes [endNodeId] as Node;
		}
	}

}

