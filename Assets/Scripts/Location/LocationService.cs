﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LocationMode {
    RedDot,     //raw, unprocessed location
    BlueDot,    //snapped to path
    Both        //show both red and blue dots
}

public class LocationService : MonoBehaviour {
    public TextMesh textMesh;
    public Transform redDot;
    public Transform blueDot;
    public float targetWorldHoverHeight = 0f;
    public float targetLocalHoverHeight = 0.3154882f;
    public LocationMode locationMode = LocationMode.BlueDot;
    public float targetSpeed = 2f;
    public int detectedBeaconCount;

    //The following values were calculated or measured, don't mess them up
    public int cmsMapHeight = 21504;
    public float cmsZoomScale = 100f; //How big is the scale of the map in CMS comparing to in Unity, this was calculated
    public float cmsWorldXOffset = -53.993862f;
    public float cmsWorldYOffset = -63.57513f;
    public float cmsLocalXOffset = 54.217619f;
    public float cmsLocalYOffset = 57.935369f;

    private string infoText = "";
    private bool shouldMoveTarget = false;
    private Location newLocation;

    private void Awake() {
#if UNITY_ANDROID
        LocationManager locationManager = LocationManager.Instance;
        locationManager.OnIndoorLocationUpdated += LocationManager_OnIndoorLocationUpdated;
        locationManager.OnIndoorTransmissionsUpdated += LocationManager_OnIndoorTransmissionsUpdated;
        locationManager.OnIndoorExitRegion += LocationManager_OnIndoorExitRegion;
#endif
#if UNITY_IOS
		PMPNativeBridgeForIOS locationManager = PMPNativeBridgeForIOS.sharedInstance;
        locationManager.onDidLocationUpdatedHandler += LocationManager_OnIndoorLocationUpdated;
#endif
    }

    private void Start() {
        if (locationMode == LocationMode.RedDot) {
            blueDot.gameObject.SetActive(false);
        } else if (locationMode == LocationMode.BlueDot) {
            redDot.gameObject.SetActive(false);
        }
    }

    private void Update() {
        //textMesh.text = infoText;
        if (this.shouldMoveTarget) {
            //Move target
            if (this.locationMode == LocationMode.BlueDot || this.locationMode == LocationMode.Both) {
                Path nearestPath = this.FindTheNearestPath();
                Location projectedLocation = this.ProjectedLocation(nearestPath);
                Vector3 targetWorldPos = ConvertFromCMSCoordinates(projectedLocation, true);
                if (this.blueDot != null) {
                    float duration = this.CalculateTargetMovementDuration(targetWorldPos);
                    iTween.MoveTo(this.blueDot.gameObject, iTween.Hash("position", targetWorldPos, "isLocal", true, "time", duration, "easetype", iTween.EaseType.easeOutSine));
                }               
            }
            if (this.locationMode == LocationMode.RedDot || this.locationMode == LocationMode.Both) {
                Vector3 targetWorldPos = ConvertFromCMSCoordinates(newLocation, true);
                if (this.redDot != null) {
                    float duration = this.CalculateTargetMovementDuration(targetWorldPos);
                    iTween.MoveTo(this.redDot.gameObject, iTween.Hash("position", targetWorldPos, "isLocal", true, "time", duration, "easetype", iTween.EaseType.easeOutSine));
                }
            }
            shouldMoveTarget = false;
        }
    }

    private void OnDestroy() {
#if UNITY_ANDROID
        LocationManager locationManager = LocationManager.Instance;
        locationManager.OnIndoorLocationUpdated -= LocationManager_OnIndoorLocationUpdated;
        locationManager.OnIndoorTransmissionsUpdated -= LocationManager_OnIndoorTransmissionsUpdated;
        locationManager.OnIndoorExitRegion -= LocationManager_OnIndoorExitRegion;
#endif
#if UNITY_IOS
		PMPNativeBridgeForIOS locationManager = PMPNativeBridgeForIOS.sharedInstance;
        locationManager.onDidLocationUpdatedHandler -= LocationManager_OnIndoorLocationUpdated;
#endif
    }

    private void LocationManager_OnIndoorExitRegion() {
        //do nothing yet
    }

    private void LocationManager_OnIndoorTransmissionsUpdated(int beaconCount) {
        this.detectedBeaconCount = beaconCount;
    }

    private void LocationManager_OnIndoorLocationUpdated(Location location) {
        //Don't access any GameObject or Monobehavior here, it will crash!!!
        //this.infoText = string.Format("Location: ({0}, {1}, {2}) {3}", location.x, location.y, location.z, location.name);
        this.newLocation = location;
        shouldMoveTarget = true;
    }

    private float CalculateTargetMovementDuration(Vector3 destination) {
        float distance = Vector3.Distance(redDot.position, destination);
        float duration = distance / targetSpeed;
        return Mathf.Clamp(duration, 0.2f, 1.0f);
    }

    private float DistanceFromAPointToPath(Location location, Path path) {
        Vector2 p = new Vector2((float)location.x, (float)location.y);
        Vector2 p0 = new Vector2((float)path.startNode.location.x, (float)path.startNode.location.y);
        Vector2 p1 = new Vector2((float)path.endNode.location.x, (float)path.endNode.location.y);
        Vector2 v = p1 - p0;
        Vector2 w = p - p0;
        float c1 = Vector2.Dot(w, v);
        if (c1 <= 0) {
            return Vector2.Distance(p, p0);
        }
        float c2 = Vector2.Dot(v, v);
        if (c2 <= c1) {
            return Vector2.Distance(p, p1);
        }
        float b = c1 / c2;
        Vector2 pb = p0 + v * b;
        return Vector2.Distance(p, pb);
    }

    private Path FindTheNearestPath() {
        List<Path> pathsOnThisFloor = ServerManager.Instance.pathsOnThisFloor;
        float distance = float.MaxValue;
        Path nearestPath = null;
        foreach(Path path in pathsOnThisFloor) {
            float newDistance = this.DistanceFromAPointToPath(newLocation, path);
            if (newDistance < distance) {
                distance = newDistance;
                nearestPath = path;
            }
        }
        return nearestPath;
    }

    private Location ProjectedLocation(Path path) {
        Vector2 v1 = new Vector2((float)path.startNode.location.x, (float)path.startNode.location.y);
        Vector2 v2 = new Vector2((float)path.endNode.location.x, (float)path.endNode.location.y);
        Vector2 p = new Vector2((float)newLocation.x, (float)newLocation.y);
        Vector2 e1 = v2 - v1;
        Vector2 e2 = p - v1;
        float val = Vector2.Dot(e1, e2);
        float len2 = e1.SqrMagnitude();
        Vector2 p0 = v1 + e1 * val / len2;

        Location projectedLocation = new Location() {
            x = p0.x,
            y = p0.y,
            z = 0d,
            name = path.startNode.location.name
        };
        return projectedLocation;
    }

    public Vector3 ConvertFromCMSCoordinates(Location location, bool isLocal = false) {
        Vector3 pos;
        if (isLocal) {
            pos.x = (float)location.x * -1 / this.cmsZoomScale + cmsLocalXOffset;
            pos.y = this.targetLocalHoverHeight; //TODO: may need some logic regarding floor
            pos.z = (this.cmsMapHeight - (float)location.y) * -1 / this.cmsZoomScale + cmsLocalYOffset;
        } else {
            pos.x = (float)location.x / this.cmsZoomScale + cmsWorldXOffset;
            pos.y = this.targetWorldHoverHeight; //TODO: may need some logic regarding floor
            pos.z = (this.cmsMapHeight - (float)location.y) / this.cmsZoomScale + cmsWorldYOffset;
        }
        return pos;
    }
}
