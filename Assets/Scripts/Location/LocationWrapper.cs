﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LocationWrapper : MonoBehaviour {
    public TextMesh textMesh;
    public int projectId = 44; //44 is HKIA
    public int mapId = 30; //this is HKIA L5
    public bool showNodeAndPath = false;
    public Transform nodePrefab;
    public Material pathLineMaterial;
    public Transform target;
    public float targetHoverHeight = 0f;
    public LocationMode locationMode = LocationMode.RedDot;
    public float targetSpeed = 2f;

    //The following values were calculated or measured, don't mess them up
    public int cmsMapHeight = 21504;
    public float cmsZoomScale = 100f; //How big is the scale of the map in CMS comparing to in Unity, this was calculated
    public float cmsXOffset = -53.993862f; 
    public float cmsYOffset = -63.57513f;

    private AndroidJavaObject currentActivity;
    private int detectedBeaconCount = 0;
    private Location location;
    private Vector3 targetWorldPos;
    private Vector3 NodePositions;

    // Use this for initialization
    void Start() {
#if UNITY_ANDROID
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        var serverManagerPlugin = new AndroidJavaClass("com.pmp.mapsdk.cms.PMPServerManager");
        AndroidJavaObject serverManager = serverManagerPlugin.CallStatic<AndroidJavaObject>("getShared", currentActivity);
        ServerManagerNotifier serverManagerNotifier = new ServerManagerNotifier(currentActivity);
//        serverManagerNotifier.OnSuccess += OnDownloadMapSuccessAndroid;
//        serverManagerNotifier.OnFailed += OnDownloadMapFailedAndroid;
        serverManager.Call("downloadMap", projectId, serverManagerNotifier);
#endif
    }

    // Update is called once per frame
    void Update() {
        if (location != null) {
            textMesh.text = "Location: (" + location.x + ", " + location.y + ", " + location.z + ") on " + location.name;
            //if (target != null) {
            //    target.position = targetWorldPos; //TODO: use Lerp
            //}
        } else {
            textMesh.text = "unknown location, " + detectedBeaconCount + " beacons detected";
        }
    }

#if UNITY_ANDROID
    private void OnDownloadMapSuccessAndroid(AndroidJavaObject responseData) {
        AndroidJavaObject devices = responseData.Call<AndroidJavaObject>("getDevices");
        int devicesCount = devices.Call<int>("size");
        var beaconList = new AndroidJavaObject("java.util.ArrayList");
        for (int i = 0; i < devicesCount; i++) {
            var device = devices.Call<AndroidJavaObject>("get", i);
            string bleWayFindingUuid = device.Call<string>("getBleWayFindingUuid");
            if (bleWayFindingUuid != null) {
                var beacon = new AndroidJavaObject("com.pmp.mapsdk.location.PMPBeacon", device, bleWayFindingUuid, responseData.Call<double>("getMapScale"));
                beaconList.Call<bool>("add", beacon);
            }
        }

        LocationManager locationManager = LocationManager.Instance;
        locationManager.OnIndoorLocationUpdated += LocationManager_OnIndoorLocationUpdated;
        locationManager.OnIndoorTransmissionsUpdated += LocationManager_OnIndoorTransmissionsUpdated;
        locationManager.OnIndoorExitRegion += LocationManager_OnIndoorExitRegion;
        //locationManager.OnCompassUpdated += LocationManager_OnCompassUpdated;

        var locationManagerPlugin = new AndroidJavaClass("com.pmp.mapsdk.location.PMPLocationManager");
        AndroidJavaObject mgr = locationManagerPlugin.CallStatic<AndroidJavaObject>("getShared", currentActivity);
        mgr.Call("setEnableLog", true);
        mgr.Call("setEnableBeaconAccuracyFilter", false);
        mgr.Call("setEnableLowPassFilter", false);
        mgr.Call("setEnableMaxNumOfBeaconsDetection", false);
        mgr.Call("setEnableKalmanFilter", true);
        mgr.Call("addDelegate", locationManager);
        mgr.Call("setProximityUUIDS", beaconList);
        mgr.Call("start");

        //AndroidJavaObject androidNodes = responseData.Call<AndroidJavaObject>("getNodes");
        //int nodeCount = androidNodes.Call<int>("size");
        //Node[] nodes = new Node[nodeCount];
        //for (int i = 0; i < nodeCount; i++) {
        //    AndroidJavaObject androidNode = androidNodes.Call<AndroidJavaObject>("get", i);
        //    Node node = new Node {
        //        id = androidNode.Call<double>("getId"),
        //        mapId = this.mapId,
        //        location = null
        //    };
        //    nodes[i] = node;

        //    if (nodes[i].mapId == (double)this.mapId) {
        //        Location nodeLocation = new Location {
        //            x = androidNode.Call<double>("getX"),
        //            y = androidNode.Call<double>("getY"),
        //            z = androidNode.Call<double>("getZ"),
        //            name = androidNode.Call<double>("getMapId").ToString()
        //        };
        //        nodes[i].location = nodeLocation;
        //        Vector3 nodePosition = this.ConvertFromCMSCoordinates(nodeLocation);
        //        var nodeGameObject = Instantiate(nodePrefab, nodePosition, Quaternion.identity);
        //        nodeGameObject.rotation = Quaternion.AngleAxis(90, Vector3.left);
        //    }
        //}

        //AndroidJavaObject paths = responseData.Call<AndroidJavaObject>("getPaths");
        //int pathCount = paths.Call<int>("size");
        //for (int i = 0; i < pathCount; i++) {
        //    AndroidJavaObject path = paths.Call<AndroidJavaObject>("get", i);
        //    Node startNode = Array.Find(nodes, node => node.id == path.Call<double>("getStartNodeId"));
        //    Node endNode = Array.Find(nodes, node => node.id == path.Call<double>("getEndNodeId"));
        //    if (startNode.location != null && endNode.location != null) {
        //        //GameObject pathGameObject = new GameObject();
        //        //LineRenderer pathLineRenderer = pathGameObject.AddComponent<LineRenderer>();
        //        //pathLineRenderer.material = pathLineMaterial;
        //        //pathLineRenderer.startWidth = 0.05f;
        //        //pathLineRenderer.endWidth = 0.05f;
        //        //pathLineRenderer.positionCount = 2;
        //        //pathLineRenderer.SetPosition(0, this.ConvertFromCMSCoordinates(startNode.location));
        //        //pathLineRenderer.SetPosition(1, this.ConvertFromCMSCoordinates(endNode.location));
        //    }
        //}
    }

    private void OnDownloadMapFailedAndroid() {
        Debug.Log("Failed downloading map");
    }
#endif

    private void LocationManager_OnIndoorLocationUpdated(Location location) {
        this.location = location;
        if (this.locationMode == LocationMode.BlueDot) {
            //TODO: snap to path algorithm goes here
        } else if (this.locationMode == LocationMode.RedDot) {
            targetWorldPos = ConvertFromCMSCoordinates(location);

            if (target != null) {
                float duration = this.CalculateTargetMovementDuration(targetWorldPos);
                iTween.MoveTo(target.gameObject, iTween.Hash("position", targetWorldPos, "time", duration, "easetype", iTween.EaseType.easeOutSine));
            }
        }
    }

    //private void LocationManager_OnCompassUpdated(double direction) {
    //    this.direction = direction;
    //}

    private void LocationManager_OnIndoorTransmissionsUpdated(int beaconCount) {
        this.detectedBeaconCount = beaconCount;
        if (beaconCount < 3) {
            this.location = null;
        }
    }

    private void LocationManager_OnIndoorExitRegion() {
        this.location = null;
    }

    private Vector3 ConvertFromCMSCoordinates(Location location) {
        Vector3 pos;
        pos.x = (float)location.x / this.cmsZoomScale + cmsXOffset;
        pos.y = this.targetHoverHeight; //TODO: may need some logic regarding floor
        pos.z = (this.cmsMapHeight - (float)location.y) / this.cmsZoomScale + cmsYOffset;

        return pos;
    }

    private float CalculateTargetMovementDuration(Vector3 destination) {
        float distance = Vector3.Distance(target.position, destination);
        float duration = distance / targetSpeed;
        return Mathf.Clamp(duration, 0.2f, 1.0f);
    }
}

