﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class TopHolderController : MonoBehaviour {
	public Text searchButtonText;
	public GameObject searchUI;
	public GameObject compass;
	private bool isShown;
	private const float compassRotationSpeed = 360;
	float compassTargetRotationZ = -1;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (compassTargetRotationZ == -1) {
			return;
		}

		if (compass.transform.localRotation.eulerAngles.z % 360 != compassTargetRotationZ % 360) {
			float angleDelta = Mathf.Abs (compassTargetRotationZ - compass.transform.localRotation.eulerAngles.z) % 360;
			if (angleDelta > 180 ) {
				angleDelta = 360 - angleDelta;
				if(compassTargetRotationZ - compass.transform.localRotation.eulerAngles.z < 0){
					angleDelta = -angleDelta;
				}
			}else if(compassTargetRotationZ - compass.transform.localRotation.eulerAngles.z > 0){
				angleDelta = -angleDelta;
			}


			Debug.Log ("angleDelta: " + angleDelta);
			if (Mathf.Abs (angleDelta) > compassRotationSpeed * Time.deltaTime) {
				compass.transform.Rotate ((angleDelta > 0 ? Vector3.back : Vector3.forward) * compassRotationSpeed * Time.deltaTime);

			} else {
				compass.transform.localRotation = Quaternion.Euler( new Vector3 (0,0,compassTargetRotationZ));
			}
		} else {
			compassTargetRotationZ = -1;
		}

	}

	#region button event
	public void OnSearchButtonClicked(){
		isShown = !isShown;
		searchUI.gameObject.SetActive (isShown);
		searchButtonText.text = isShown ? "X" : "Search";
	}

	public void OnCompassClicked(){
		compassTargetRotationZ = 0;
	
	}
	#endregion
}
