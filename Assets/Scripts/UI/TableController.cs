﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TableController : MonoBehaviour {
	public GameObject tableRowPrefab;
	public GameObject tableScrollViewContent;
	public TableControllerDelegate tableControllerDelegate;
	float searchResultScrollContentWidth;
	// Use this for initialization
	void Start () {
		searchResultScrollContentWidth = Screen.width;

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	protected int GetNumberOfRow(){
		if (tableControllerDelegate != null) {
			return tableControllerDelegate.GetNumberOfRowForTable ();
		}
		return 0;
	}

	protected int GetRowHeight(){
		if (tableControllerDelegate != null) {
			return tableControllerDelegate.GetRowHeightForTable ();
		}
		return 0;
	}

	public void RefreshTable(){
		foreach (Transform child in tableScrollViewContent.transform) {
			GameObject.Destroy(child.gameObject);
		}

		Vector2 newCellSize = new Vector2 (searchResultScrollContentWidth, (float)GetRowHeight());
		tableScrollViewContent.GetComponent<GridLayoutGroup> ().cellSize = newCellSize;

		tableScrollViewContent.GetComponent<RectTransform> ().sizeDelta = new Vector2(0, GetRowHeight() * GetNumberOfRow());
		if (tableRowPrefab == null || GetNumberOfRow() <= 0)
			return;
		
		for(int i = 0; i < GetNumberOfRow(); i++){
			GameObject rowObj = Instantiate(tableRowPrefab, Vector3.zero, Quaternion.identity);
			RectTransform rt = rowObj.GetComponent<RectTransform> ();
			rt.SetParent (tableScrollViewContent.transform);

			if (tableControllerDelegate != null) {
				tableControllerDelegate.OnInstantiateTableRow (i, rowObj);
			}
		}
	}
}

public interface TableControllerDelegate{
	int GetNumberOfRowForTable();
	int GetRowHeightForTable();
	void OnInstantiateTableRow(int rowIndex, GameObject rowObj);
}
