﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TouchScript.Gestures;
using TouchScript.Gestures.TransformGestures;



public class MapMenu : MonoBehaviour {
	public ScreenTransformGesture scrollGesture;
	public ScreenTransformGesture zoomGesture;
	public GameObject mapObjectsParent;
	GameObject[] miniMaps;
	public float mapVerticalDistance = 3;
	public Camera menuCamera;

	const float maxCameraOrthographicSizeFor9x16Screen = 12;
	const float minCameraOrthographicSizeFor9x16Screen = 6;
	const float screenRatioOf9x16 = 0.5625f;
	float currentCameraOrthographicSizeFor9x16Screen;
	const float zoomSpeedModifier = 5;

	Vector3 targetPositionForMenu;
	const float autoScrollSpeed = 24;
	const float scrollSpeedModifier = 0.015f;

	const float rotationSpeedModifier = 0.25f;
	const float minRotationDelta = 10;

	public int selectedFloorIndex {
		get;
		protected set;
	}
	private int focusedFloorIndex = -1;

	public delegate void OnSelectedFloorCallback(int selectedFloorIndex);
	public event OnSelectedFloorCallback OnSelectedFloorHandler;

	void Awake() {
	}

	void Start(){
		UpdateCameraOrthographicSize (maxCameraOrthographicSizeFor9x16Screen);
//		float orthographicCameraSize = orthographicCameraSizeFor9x16Screen / (((float)Screen.width / (float)Screen.height) / screenRatioOf9x16);
//		menuCamera.orthographicSize = GetOrthographicCameraSizeForCurrentScreen(maxOrthographicCameraSizeFor9x16Screen);
	}

	void Update(){
		if (targetPositionForMenu != Vector3.zero) {
			if (mapObjectsParent.transform.localPosition.y < targetPositionForMenu.y) {
				float deltaY = Mathf.Min ((targetPositionForMenu - mapObjectsParent.transform.localPosition).y, autoScrollSpeed * Time.deltaTime);
				mapObjectsParent.transform.localPosition += new Vector3 (0, deltaY, 0);
				UpdateMapMenuAppearance (0);
			} else if (mapObjectsParent.transform.localPosition.y > targetPositionForMenu.y) {
				float deltaY = Mathf.Min ((mapObjectsParent.transform.localPosition - targetPositionForMenu).y, autoScrollSpeed * Time.deltaTime);
				mapObjectsParent.transform.localPosition -= new Vector3 (0, deltaY, 0);
				UpdateMapMenuAppearance (0);
			} else {
				targetPositionForMenu = Vector3.zero;
			}
		}
	}

	void UpdateCameraOrthographicSize(float cameraOrthographicSizeFor9x16Screen){
		currentCameraOrthographicSizeFor9x16Screen = cameraOrthographicSizeFor9x16Screen;
		menuCamera.orthographicSize = GetCameraOrthographicSizeForCurrentScreen(currentCameraOrthographicSizeFor9x16Screen);
		
	}

	//Calculate orthographic size for the camera to fit the screen
	float GetCameraOrthographicSizeForCurrentScreen(float cameraOrthographicSize){
		return cameraOrthographicSize  / (((float)Screen.width / (float)Screen.height) / screenRatioOf9x16);
	}

	public void SetupMapMenu(GameObject [] maps, int index){
		if (miniMaps != null) {
			for (int i = 0; i < miniMaps.Length; i++) {
				MiniFloorMap map = miniMaps [i].GetComponent<MiniFloorMap> ();
				map.floorMapSelectedHandler -= OnFloorMapSelected;
				Destroy (miniMaps[i]);
			}
		}

		miniMaps = maps;
		for (int i = 0; i < miniMaps.Length; i++) {
			MiniFloorMap map = miniMaps [i].GetComponent<MiniFloorMap> ();
			map.floorMapSelectedHandler += OnFloorMapSelected;
			miniMaps [i].transform.SetParent (mapObjectsParent.transform);
			miniMaps [i].transform.localPosition = new Vector3 (0,i * mapVerticalDistance,0);
		}

		mapObjectsParent.transform.localPosition = new Vector3 (mapObjectsParent.transform.localPosition.x, 0, mapObjectsParent.transform.localPosition.z);
		if (index < 0 || index >= maps.Length) {
			index = 0;
		}
		selectedFloorIndex = index;
		targetPositionForMenu = Vector3.zero;
		UpdateMapMenuAppearance (0);
	}
		
	private void OnEnable() {
		scrollGesture.Transformed += ScrollGestureHandler;
		zoomGesture.Transformed += ZoomAndRotateGestureHandler;
	}

	private void OnDisable() {
		scrollGesture.Transformed -= ScrollGestureHandler;
		zoomGesture.Transformed -= ZoomAndRotateGestureHandler;
	}

	private void ZoomAndRotateGestureHandler(object sender, System.EventArgs e) {
//		float twoFingerZoomScale = (zoomGesture.DeltaScale - 1.0f) * zoomSpeed;

		currentCameraOrthographicSizeFor9x16Screen -= (zoomGesture.DeltaScale - 1.0f) * zoomSpeedModifier;
		currentCameraOrthographicSizeFor9x16Screen = Mathf.Min(Mathf.Max (minCameraOrthographicSizeFor9x16Screen,currentCameraOrthographicSizeFor9x16Screen), maxCameraOrthographicSizeFor9x16Screen);
		UpdateCameraOrthographicSize (currentCameraOrthographicSizeFor9x16Screen);
	}

	private void ScrollGestureHandler(object sender, System.EventArgs e) {
		if (!mapObjectsParent.activeSelf || miniMaps == null || EventSystemUtilities.IsPointerOverUIObject()){
			return;
		}
	
		targetPositionForMenu = Vector3.zero;
		Vector3 moveAmount = new Vector3() {
			x = 0f,
			y = scrollGesture.DeltaPosition.y * scrollSpeedModifier,
			z = 0f 
		};

		Vector3 positionAfterGesture = mapObjectsParent.transform.localPosition + moveAmount;

		if (positionAfterGesture.y > 0) {
			mapObjectsParent.transform.localPosition = new Vector3 (mapObjectsParent.transform.localPosition.x, 0, mapObjectsParent.transform.localPosition.z);
		} else if (positionAfterGesture.y < -mapVerticalDistance * (miniMaps.Length)) {
			mapObjectsParent.transform.localPosition = new Vector3 (mapObjectsParent.transform.localPosition.x, -mapVerticalDistance * (miniMaps.Length), mapObjectsParent.transform.localPosition.z);
		} else {
			mapObjectsParent.transform.localPosition += moveAmount;
		}

		if (Mathf.Abs (scrollGesture.DeltaPosition.x) > minRotationDelta) {
			UpdateMapMenuAppearance (scrollGesture.DeltaPosition.x * -rotationSpeedModifier);
		} else {
			UpdateMapMenuAppearance (0);

		}
	}

	private void UpdateMapMenuAppearance(float rotationDelta){
		int focusedFloor = Mathf.Min((int)(mapObjectsParent.transform.localPosition.y / mapVerticalDistance) * -1, miniMaps.Length - 1);
//		if (focusedFloor == focusedFloorIndex) {
//			return;
//		}
		focusedFloorIndex = focusedFloor;
		for(int i = 0 ; i < miniMaps.Length; i++){
			if (i == selectedFloorIndex) {
				miniMaps [i].GetComponent<MiniFloorMap> ().OnMenuUpdated (MiniFloorMap.MiniMapState.Selected, rotationDelta);
			} else if (Mathf.Abs (i - focusedFloor) <= 1) {
				miniMaps [i].GetComponent<MiniFloorMap> ().OnMenuUpdated (MiniFloorMap.MiniMapState.Normal, rotationDelta);
			} else {
				miniMaps [i].GetComponent<MiniFloorMap> ().OnMenuUpdated (MiniFloorMap.MiniMapState.Dimmed, rotationDelta);
			}
		}
	}
		
	void OnFloorMapSelected (MiniFloorMap miniFloorMap){
		for(int i = 0 ; i < miniMaps.Length; i++){
			if (miniMaps[i].GetComponent<MiniFloorMap>() == miniFloorMap) {
				selectedFloorIndex = i;
				break;
			}
		}

		targetPositionForMenu = new Vector3 (mapObjectsParent.transform.localPosition.x, -(mapVerticalDistance * selectedFloorIndex + mapVerticalDistance/2), mapObjectsParent.transform.localPosition.z);
		focusedFloorIndex = -1;
		UpdateMapMenuAppearance (0);
		if (OnSelectedFloorHandler != null) {
			OnSelectedFloorHandler (selectedFloorIndex);
		}
	}
}


