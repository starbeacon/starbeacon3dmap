﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapMenuController : MonoBehaviour {
	public Dropdown areaDropDown;
	public MapMenu mapMenu;
	public GameObject miniMapPrefab;
	public Camera mainCamera;
	public Camera mapMenuCamera;

	List<string>areaDropDownOptions;
	private bool isShown;

	// Use this for initialization
	void Start () {
		areaDropDown.onValueChanged.AddListener (
			delegate {
				AreaDropdownValueChangedHandler(areaDropDown);	
			});
		mapMenu.OnSelectedFloorHandler += OnSelectedFloor;
		SetUpMapMenu ();
		ShowMapMenu (false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void Destroy() {
		areaDropDown.onValueChanged.RemoveAllListeners();
		mapMenu.OnSelectedFloorHandler -= OnSelectedFloor;
	}

	public void SetUpAreaDropDown(List<Area> areaList){
		areaDropDownOptions = new List<string> ();
		areaDropDown.ClearOptions ();
		for(int i = 0 ; i < areaList.Count; i++){
			if (areaList [i].subAreas.Count < 1) {
				continue;
			}
			areaDropDownOptions.Add (LocalizedText.GetLocalizedText(areaList[i].name));
		}
		areaDropDown.AddOptions (areaDropDownOptions);
		areaDropDown.RefreshShownValue ();
	}

	public void ToggleMapMenu(){
		ShowMapMenu (!isShown);
	}

	public void ShowMapMenu(bool show){
		isShown = show;
		areaDropDown.gameObject.SetActive(show);
		mapMenu.mapObjectsParent.SetActive(show);

		mainCamera.gameObject.SetActive(!show);
		mapMenuCamera.gameObject.SetActive(show);
	}

	#region map menu
	void SetUpMapMenu(){
		//hardcoded logic for demo
		int numberOfMiniMaps = 5;
		GameObject[] miniMaps = new GameObject[numberOfMiniMaps];
		for(int i = 0; i < numberOfMiniMaps; i++){
			GameObject map = Instantiate (miniMapPrefab, Vector3.zero, Quaternion.identity);
			map.GetComponent<MiniFloorMap> ().floorText.text = "L" + (i + 1);
			miniMaps [i] = map;
		}

		mapMenu.SetupMapMenu (miniMaps, 0);
	}

	void OnSelectedFloor(int selectedFloorIndex){
		
	}

	#endregion

	#region areaDropDown
	private void AreaDropdownValueChangedHandler(Dropdown target) {
//		SetUpMapMenu ();
	}

	public void SetDropdownIndex(int index) {
		areaDropDown.value = index;
	}
	#endregion
}
