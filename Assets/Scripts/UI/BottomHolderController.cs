﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BottomHolderController : MonoBehaviour {

	private RectTransform rectTransform;
	[HideInInspector]
	public bool expanded {
		get;
		protected set;
	}
	public Text PoiNameText;
	public Text PoiAddressText;
	public Button navigationPreviewButton;

	private Poi selectedPoi;

	// Use this for initialization
	void Awake(){
		SelectedPoi.selectedPoiHandler += OnSelectedPoi;
	}

	void Start () {
		expanded = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void Destroy(){
		SelectedPoi.selectedPoiHandler -= OnSelectedPoi;

	}

	#region button event
	public void ToggleExpand(){
		SetBottomHolderViewExpand (!expanded);
	}

	public void OnRecenterButtonClicked(){
		
	}

	public void OnNavigationPreviewButtonClicked(){
	
	}

	#endregion

	#region animator & animation
	public void SetBottomHolderViewExpand(bool expand){
		expanded = expand;
		Animator animator = GetComponent<Animator> ();
		animator.Play (expand ? "BottomHolderViewExpand" : "BottomHolderViewCollapse");
	}

	//This method is added to bottom hold view's animation via Animation panel
	public void OnExpandAnimationEnd(){
		navigationPreviewButton.gameObject.SetActive (expanded);
	}
	#endregion

	void OnSelectedPoi(Poi poi){
		selectedPoi = poi;
		if (poi != null) {
			PoiNameText.text = LocalizedText.GetLocalizedText (poi.name);
			PoiAddressText.text = LocalizedText.GetLocalizedText (poi.address);
			SetBottomHolderViewExpand (true);
		} else {
			SetBottomHolderViewExpand (false);
		}
	}
}
