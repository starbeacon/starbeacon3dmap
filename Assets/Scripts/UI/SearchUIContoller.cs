﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class SearchResult{
	public Tags tags;
	public PoiCategory poiCategory;
	public Brand brand;
}

public class SearchUIContoller : MonoBehaviour,TableControllerDelegate {
	public TableController searchReultTableController;
	private List<SearchResult> searchResultList;

	public delegate void OnSearchResultPoiListUpdated(List<Poi>resultPoiList);
	public event OnSearchResultPoiListUpdated searchResultPoiListUpdatedHandler;

	// Use this for initialization
	void Start () {
		searchReultTableController.tableControllerDelegate = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	#region Input field event
	public void OnSearchFieldValueChanged(string value){
		List<Tags> tagsList = UIDemoSceneManager.Instance.tagsList;
		List<PoiCategory> flattenPoiCategoriesList = UIDemoSceneManager.Instance.flattenPoiCategoriesList;
		List<Brand> brandList = UIDemoSceneManager.Instance.brandList;

		searchResultList = new List<SearchResult> ();
		searchReultTableController.gameObject.SetActive (true);

		if (value.Length > 0) {

			if(tagsList != null){
				for(int i = 0; i < tagsList.Count; i++){
					if(tagsList[i].content.ToLower().IndexOf(value.ToLower()) >= 0){
						SearchResult r = new SearchResult();
						r.tags = tagsList [i];
						searchResultList.Add (r);
					}
				}
			}

			if (flattenPoiCategoriesList != null) {
				for(int i = 0; i < flattenPoiCategoriesList.Count; i++){
					if (flattenPoiCategoriesList [i].unsearchable)
						continue;
					if(LocalizedText.GetLocalizedText(flattenPoiCategoriesList[i].name).ToLower().IndexOf(value.ToLower()) >= 0){
						SearchResult r = new SearchResult();
						r.poiCategory = flattenPoiCategoriesList [i];
						searchResultList.Add (r);
					}
				}
			}

			if (brandList != null) {
				for(int i = 0; i < brandList.Count; i++){
					if(LocalizedText.GetLocalizedText(brandList[i].name).ToLower().IndexOf(value.ToLower()) >= 0){
						SearchResult r = new SearchResult();
						r.brand = brandList [i];
						searchResultList.Add (r);
					}
				}
			}
		}

		if (searchReultTableController != null) {
			searchReultTableController.RefreshTable ();
		}
	}

	public void OnEndEdit(string s){
		searchReultTableController.gameObject.SetActive (false);
	}

	#endregion

	#region TableControllerDelegate
	public int GetNumberOfRowForTable(){
		if (searchResultList != null) {
			return searchResultList.Count;
		}
		return 0;
	}

	public int GetRowHeightForTable(){
		return 80;
	}

	public void OnInstantiateTableRow(int rowIndex, GameObject rowObj){
		if (searchResultList != null && searchResultList.Count > rowIndex) {
			Text searchResultTitle = rowObj.transform.Find ("SearchResultTitle").gameObject.GetComponent<Text> ();

			SearchResult result = searchResultList [rowIndex];
			if (result.tags != null) {
				searchResultTitle.text = result.tags.content;
			}else if(result.poiCategory != null){
				searchResultTitle.text = LocalizedText.GetLocalizedText(result.poiCategory.name);
			}else if(result.brand != null){
				searchResultTitle.text = LocalizedText.GetLocalizedText(result.brand.name);
			}
				
			Button rowObjBtn = rowObj.GetComponent<Button> ();
			if (rowObjBtn != null) {
				rowObjBtn.onClick.AddListener (delegate {
					OnSelectedSearchResult(rowIndex);
				});
			}
		}
	}
	#endregion

	void OnSelectedSearchResult(int index){
		if (searchResultList != null && searchResultList.Count > index) {
			List<Poi> resultPoiList = new List<Poi> ();
			List<Poi> poiList = UIDemoSceneManager.Instance.pois;

			SearchResult result = searchResultList[index];

			if (result.tags != null) {
				Tags tags = result.tags;
		
				for(int i = 0; i < poiList.Count; i++){
					Poi p = poiList [i];
					bool nextPoi = false;

					for(int j = 0; j < tags.types.Count; j++){
						Tag t = tags.types [j];
						if (t.name.Equals ("Poi")) {
							for(int k = 0; k < t.ids.Length; k++){
								if (t.ids [k] == p.poiIdentifer) {
									resultPoiList.Add (p);
									nextPoi = true;
									break;
								}
							}

						}else if(t.name.Equals("PoiCategory")){
							for(int k = 0; k < t.ids.Length; k++){
								for (int l = 0; l < p.poiCategoryIds.Length; l++) {
									if (t.ids [k] == p.poiCategoryIds[l]) {
										resultPoiList.Add (p);
										nextPoi = true;
										break;
									}
								}
								if (nextPoi)
									break;
							}

						}else if(t.name.Equals("Brand")){
							for(int k = 0; k < t.ids.Length; k++){
								if (t.ids [k] == p.brandId) {
									resultPoiList.Add (p);
									nextPoi = true;
									break;
								}
							}
						}
						if (nextPoi)
							break;
					}
				}
			}else if(result.poiCategory != null){
				PoiCategory pc = result.poiCategory;

				for (int i = 0; i < poiList.Count; i++) {
					Poi p = poiList [i];
					for (int j = 0; j < p.poiCategoryIds.Length; j++) {
						if (p.poiCategoryIds [j] == pc.poiCategoryId) {
							resultPoiList.Add (p);
							break;
						}
					}
				}

			}else if(result.brand != null){
				Brand b = result.brand;

				for (int i = 0; i < poiList.Count; i++) {
					Poi p = poiList [i];
					if (p.brandId == b.brandId) {
						resultPoiList.Add (p);
					}
				}
			}

			Debug.Log ("OnSelectedSearchResult, Poi count:" + resultPoiList.Count);

			if (searchResultPoiListUpdatedHandler != null) {
				searchResultPoiListUpdatedHandler (resultPoiList);
			}

		}
	}
}
