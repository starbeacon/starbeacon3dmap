﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TouchScript.Gestures;

public class MiniFloorMap : MonoBehaviour {
	public GameObject miniFloorMapObject;
	private TapGesture tapGesture;
	private static Color selectedColor;
	private static Color normalColor;
	private static Color dimColor;

	public enum MiniMapState
	{
		Normal = 0,
		Selected,
		Dimmed
	
	}
	public Text floorText;
	public delegate void OnFloorMapSelectedCallback(MiniFloorMap miniFloorMap);
	public event OnFloorMapSelectedCallback floorMapSelectedHandler;

	// Use this for initialization
	void Awake () {
		selectedColor  = new Color(Color.green.r,Color.green.g, Color.green.b,1f );
		normalColor = new Color(Color.white.r,Color.white.g, Color.white.b,1f );
		dimColor = new Color(Color.grey.r,Color.grey.g, Color.grey.b,0.5f );
	}
	
	// Update is called once per frame
	void Update () {

	}

	private void OnEnable() {
		tapGesture = miniFloorMapObject.GetComponent<TapGesture>();
		tapGesture.Tapped += MiniFloorMapSelectedHandler;
	}

	private void OnDisable() {
		tapGesture.Tapped -= MiniFloorMapSelectedHandler;
	}

	protected void MiniFloorMapSelectedHandler (object sender, System.EventArgs e) {
		if (EventSystemUtilities.IsPointerOverUIObject()) {
			return;
		}

		if (floorMapSelectedHandler != null) {
			floorMapSelectedHandler (this);
		}

	}

	public void OnMenuUpdated(MiniMapState state, float rotationDelta){
		miniFloorMapObject.transform.Rotate (Vector3.up * rotationDelta);

		if (state == MiniMapState.Selected) {
			miniFloorMapObject.GetComponent<MeshRenderer> ().material.color = selectedColor;
			floorText.enabled = true;
		} else if (state == MiniMapState.Normal) {
			miniFloorMapObject.GetComponent<MeshRenderer> ().material.color = normalColor;
			floorText.enabled = true;
		} else {
			miniFloorMapObject.GetComponent<MeshRenderer> ().material.color = dimColor;
			floorText.enabled = false;
		}
	}

}
