﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WayfindingCamera : MonoBehaviour {
    public Transform arrowPrefab;
    public Vector3 arrowStartPoint;
    public Transform goal;
    public Camera camera;
    public float speed = 1.0f;
    public enum Status {
        Ready,
        Zooming,
        OnPath,
        Arrived
    }

    private Vector3 orgCamPos;
    private Vector3 targetCamPos;
    private float startTime;
    private float journeyLength;
    private float rotationDiff;
    private Status status = Status.Ready;
    private Transform arrow;
    
	// Use this for initialization
	void Start () {
        startTime = Time.time;
        targetCamPos = this.transform.position + Vector3.up * 0.0918f ;
        journeyLength = Vector3.Distance(camera.transform.position, targetCamPos);
        rotationDiff = Quaternion.Angle(camera.transform.rotation, this.transform.rotation);
	}
	
	// Update is called once per frame
	void Update () {
        if (status == Status.Zooming) {
            float distCovered = (Time.time - startTime) * speed;
            float fracJourney = distCovered / journeyLength;
            float rotationCovered = (Time.time - startTime) * speed;
            float fracRotation = rotationCovered / rotationDiff;
            camera.transform.position = Vector3.Lerp(camera.transform.position, targetCamPos, fracJourney);
            camera.transform.rotation = Quaternion.Lerp(camera.transform.rotation, this.transform.localRotation, fracRotation);
        }
        if (camera.transform.position == targetCamPos) {
            status = Status.OnPath;
            camera.transform.position = arrow.position + Vector3.up * 0.0918f;
            camera.transform.rotation = arrow.rotation;
            arrow.GetComponent<NavMeshAgent>().destination = goal.position;
        }
        if (status == Status.OnPath) {
            camera.transform.position = arrow.position + Vector3.up * 0.0918f;
            camera.transform.rotation = arrow.rotation;
        }
        if (camera.transform.position == goal.position + Vector3.up * 0.09525f) {
            status = Status.Arrived;
        }
        if (status == Status.Arrived) {
            arrow.gameObject.SetActive(false);
            status = Status.Ready;
        }
    }

    public void StartMoving() {
        if (status == Status.Ready) {
            orgCamPos = camera.transform.position;
            status = Status.Zooming;
            if (arrow == null) {
                arrow = Instantiate(arrowPrefab, this.transform);
            } else {
                arrow.rotation = Quaternion.identity;
                arrow.gameObject.SetActive(true);
            }
            NavMeshAgent agent = arrow.GetComponent<NavMeshAgent>();
            agent.Warp(arrowStartPoint);
        }
    }
}
