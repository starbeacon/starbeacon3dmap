﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RespawningArrow : MonoBehaviour {
    [HideInInspector]
    public bool hasStartedMoving = false;
    [HideInInspector]
    public Vector3 arrowStartPoint;
    
    public NavMeshPath path;

    private NavMeshAgent agent;

    private void Awake() {
        agent = this.GetComponent<NavMeshAgent>();
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (this.hasStartedMoving && this.IsAgentCompletedPath(agent)) {
            agent.Warp(arrowStartPoint);
            this.hasStartedMoving = false;
            if (path != null)
                agent.SetPath(this.path);
            this.hasStartedMoving = true;
        }
    }

    private bool IsAgentCompletedPath(NavMeshAgent agent) {
        if (!agent.pathPending) {
            if (agent.remainingDistance <= agent.stoppingDistance) {
                if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f) {
                    return true;
                }
            }
        }
        return false;
    }
}
