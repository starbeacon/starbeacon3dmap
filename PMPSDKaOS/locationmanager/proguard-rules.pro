-keepattributes *Annotation*
-keep public class com.google.vending.licensing.ILicensingService
-keep public class com.android.vending.licensing.ILicensingService

# For native methods, see http://proguard.sourceforge.net/manual/examples.html#native
-keepclasseswithmembernames class * {
    native <methods>;
}

# keep setters in Views so that animations can still work.
# see http://proguard.sourceforge.net/manual/examples.html#beans
-keepclassmembers public class * extends android.view.View {
	public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
   void set*(***);
   *** get*();
}

# We want to keep methods in Activity that could be used in the XML attribute onClick
-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}

# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

# -keep class com.cherrypicks.pmpmap.app.** { *; }
# -keep class com.cherrypicks.pmpmap.** { *; }

-keep class org.cocos2dx.cpp.** { *; }
-keep class com.cherrypicks.pmpmap.PMPMapConfig { *; }
-keep class com.cherrypicks.pmpmap.utils.** { *; }
-keep class com.cherrypicks.pmpmap.datamodel.** { *; }
-keep class com.pmp.mapsdk.location.PMPBeacon { *; }
-keep class com.pmp.mapsdk.location.PMPAppLifeCycle { *; }
-keep class com.pmp.mapsdk.location.PMPApplication { *; }
-keep class com.pmp.mapsdk.location.PMPBackgroundDetectionNotifier { *; }
-keep class com.pmp.mapsdk.location.PMPBackgroundStatus { *; }
-keep class com.pmp.mapsdk.location.PMPBackgroundStatus { *; }
-keep class com.pmp.mapsdk.location.PMPBeaconType { *; }
-keep class com.pmp.mapsdk.location.PMPLocationManagerError { *; }
-keep class com.pmp.mapsdk.location.PMPLocationManagerNotifier { *; }
-keep class com.pmp.mapsdk.location.PMPLocation { *; }
-keep class com.pmp.mapsdk.location.GeoCoordinate { *; }
-keep class com.pmp.mapsdk.location.PMPDeviceOptions { *; }
-keep class com.pmp.mapsdk.location.PMPProximityDevices { *; }

-keep class com.pmp.mapsdk.beacon.BaseNodes { *; }
-keep class com.pmp.mapsdk.beacon.BaseDevices { *; }
-keep class com.pmp.mapsdk.beacon.BaseDeviceOptions { *; }

-keep class com.pmp.mapsdk.beacon.library.service.BeaconService { *; }
-keep class com.pmp.mapsdk.beacon.library.BeaconIntentService { *; }

-keep class org.altbeacon.beacon.BeaconManager { *; }
-keep class org.altbeacon.beacon.service.BeaconService { *; }
-keep class org.altbeacon.beacon.BeaconIntentProcessor { *; }

-keep class com.pmp.mapsdk.location.PMPLocationManager {
    public <methods>;
}

-keep class com.pmp.mapsdk.logging.** { *; }
-keep class com.pmp.mapsdk.PMPDataManager { *; }
-keep class com.pmp.mapsdk.DownloadDataCallback { *; }
-keep class com.pmp.mapsdk.utils.PMPBackgroundService { *; }



-keep class com.pmp.mapsdk.location.Trilateration.** { *; }

-keep class com.cherrypicks.pmpmap.external.** { *; }
-keep class com.pmp.mapsdk.external.PMPMapSDK { *; }
-keep public interface com.pmp.mapsdk.external.PMPMapSDK$* { *; }

-keep class com.cherrypicks.pmpmap.analytics.** { *; }

-keep class com.pmp.mapsdk.app.PMPMapActivity {
    public final static <fields>;
    public *;
    protected *;
}

-keep class com.nostra13.universalimageloader.** { *; }

-keep public class **.R {
  public *;
}
-keep public class **.R$* {
  public *;
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers class **.R$* {
    public static <fields>;
}

# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version.  We know about them, and they are safe.
-dontwarn android.support.**

-keepclassmembers public class org.springframework {
    public *;
}

-keep class com.facebook.** {
   *;
}

-keep public class com.groupon.inject.JsonConverter
-keep class com.flurry.** { *; }
-dontwarn com.flurry.**
-keepattributes Signature
-dontwarn org.apache.http.**
-dontwarn org.springframework.**
-dontwarn org.apache.log4j.**
-dontwarn org.apache.commons.logging.**
-dontwarn org.apache.commons.codec.binary.**
-dontwarn android.net.http.**
-keep class android.net.http.SslError
-dontwarn com.google.android.maps.**
-dontwarn javax.activation.**
-dontwarn java.beans.**
-dontwarn javax.xml.**
-dontwarn javax.ws.**
-dontwarn org.apache.commons.**
-dontwarn org.joda.time.**
-dontwarn org.springframework.**
-dontwarn org.w3c.dom.**
-dontskipnonpubliclibraryclasses
-dontskipnonpubliclibraryclassmembers
-keep public class * extends android.content.ContentProvider