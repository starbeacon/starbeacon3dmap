#include <jni.h>
#include <string>
#include <opencv2/opencv.hpp>
using namespace std;

#ifdef __cplusplus
extern "C" {
#endif

#include <jni.h>

using namespace cv;

cv::KalmanFilter Tracker(4,2,0);
cv::Mat_<float> Measurement(2,1);
float measure_noise = 10.f;
float process_noise = 1e-5f;
float error_cov = 0.1f;
struct Location
{
    float x;
    float y;
    float z;
    float scale;
};

JNIEXPORT jobject JNICALL Java_com_pmp_mapsdk_location_Trilateration_KF_init
        (JNIEnv *env, jobject obj, jobject loc) {
    jclass java_location = env->FindClass("com/pmp/mapsdk/location/Trilateration/Location");
    jmethodID java_get_x_methodId = env->GetMethodID(java_location, "getX", "()F");
    float x = env->CallFloatMethod(loc, java_get_x_methodId);
    jmethodID java_get_y_methodId = env->GetMethodID(java_location, "getY", "()F");
    float y = env->CallFloatMethod(loc, java_get_y_methodId);

    setIdentity(Tracker.measurementMatrix);
    cv::Mat_<float> transitionMatrix(4,4);
    cv::Mat_<float> processNoiseCov(4,4);
    transitionMatrix <<
                     1,0,1,0,
            0,1,0,1,
            0,0,1,0,
            0,0,0,1;
    processNoiseCov <<
                    process_noise,0,0,0,
            0,process_noise,0,0,
            0,0,1e-2f,0,
            0,0,0,1e-2f;
    Tracker.transitionMatrix = transitionMatrix;
    Tracker.processNoiseCov = processNoiseCov;
    setIdentity(Tracker.measurementNoiseCov, Scalar::all(measure_noise*measure_noise));
    setIdentity(Tracker.errorCovPost, Scalar::all(error_cov));
    Tracker.statePost.at<float>(0) = x;
    Tracker.statePost.at<float>(1) = y;
    Tracker.statePost.at<float>(2) = 0;
    Tracker.statePost.at<float>(3) = 0;
}

JNIEXPORT jobject JNICALL Java_com_pmp_mapsdk_location_Trilateration_KF_tracking
        (JNIEnv *env, jobject obj, jobject loc) {
    jclass java_location = env->FindClass("com/pmp/mapsdk/location/Trilateration/Location");
    jmethodID java_get_x_methodId = env->GetMethodID(java_location, "getX", "()F");
    float x = env->CallFloatMethod(loc, java_get_x_methodId);
    jmethodID java_get_y_methodId = env->GetMethodID(java_location, "getY", "()F");
    float y = env->CallFloatMethod(loc, java_get_y_methodId);

    Mat prediction4 = Tracker.predict();
    Measurement(0) = x; // Measurement
    Measurement(1) = y;

    Mat estimate = Tracker.correct(Measurement);
    jmethodID methodId = env->GetMethodID(java_location, "<init>", "(FFFF)V");
    jobject loc_obj = env->NewObject(java_location, methodId, estimate.at<float>(0), estimate.at<float>(1), 0, 0);
    return loc_obj;
}

#ifdef __cplusplus
}
#endif
