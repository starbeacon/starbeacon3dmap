package com.pmp.mapsdk.location;

import android.content.Context;
import android.os.Debug;
import android.util.Log;

import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.cms.PMPServerManagerNotifier;
import com.pmp.mapsdk.cms.PMPUnityBridgeNotifier;
import com.pmp.mapsdk.cms.model.Devices;
import com.pmp.mapsdk.cms.model.ResponseData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gordonwong on 19/1/2018.
 */

public class PMPUnityBridge implements PMPLocationManagerNotifier  {
    private static final String TAG = PMPUnityBridge.class.getSimpleName();
    private volatile static PMPUnityBridge shared;
    private final Context context;
    private PMPLocationManagerNotifier locationManagerNotifier;

    private PMPUnityBridge (Context context){
        this.context = context.getApplicationContext();
    }

    public static PMPUnityBridge getShared(Context context) {
        if(shared == null) {
            synchronized (PMPCore.class) {
                if(shared == null) {
                    shared = new PMPUnityBridge(context);
                    PMPLocationManager.getShared(context);
                }
            }
        }
        return shared;
    }

    public void init(int projectId, final PMPUnityBridgeNotifier unityBridgeNotifier, final PMPLocationManagerNotifier locationManagerNotifier){
        this.locationManagerNotifier = locationManagerNotifier;
        PMPServerManager.getShared(context).downloadMap(projectId, new PMPServerManagerNotifier() {
            @Override
            public void didSuccess(String responseString) {
                ResponseData responseData = PMPServerManager.getShared(context).getServerResponse();
                List<PMPBeacon> list = new ArrayList<PMPBeacon>();
                for (Devices device : responseData.getDevices()) {
                    if(device.getBleWayFindingUuid() != null){

                        PMPBeacon beacon = new PMPBeacon(device, device.getBleWayFindingUuid(), responseData.getMapScale());
                        list.add(beacon);
                    }
                }

                PMPLocationManager mgr = PMPLocationManager.getShared(context);
                mgr.stop();

                mgr.setEnableLog(true);
                mgr.setEnableBeaconAccuracyFilter(false);
                mgr.setEnableLowPassFilter(false);
                mgr.setEnableMaxNumOfBeaconsDetection(false);
                mgr.setEnableKalmanFilter(true);
                mgr.addDelegate(PMPUnityBridge.getShared(context));
                mgr.setProximityUUIDS(list);
                mgr.start();

                unityBridgeNotifier.didFinishedDownloadMapJSON(true,responseString);
            }

            @Override
            public void didFailure() {
                unityBridgeNotifier.didFinishedDownloadMapJSON(false,"");
            }
        });

        PMPServerManager.getShared(context).downloadTags(projectId, new PMPServerManagerNotifier() {
            @Override
            public void didSuccess(String responseString) {
                unityBridgeNotifier.didFinishedDownloadTagJSON(true, responseString);
            }

            @Override
            public void didFailure() {
                unityBridgeNotifier.didFinishedDownloadTagJSON(false, "");
            }
        });
    }

    public void didIndoorLocationUpdated(PMPBeaconType beaconType, PMPLocation location){
        Log.d(TAG,"PMPNativeBridge didIndoorLocationUpdated");

        if(locationManagerNotifier != null){
            Log.d(TAG,"PMPNativeBridge locationManagerNotifier didIndoorLocationUpdated");
            locationManagerNotifier.didIndoorLocationUpdated(beaconType,location);
        }
    }

    public void didIndoorTransmissionsUpdated(List<PMPBeacon> transmissions){

    }
    public void didIndoorExitRegion(){

    }
    public void didOutdoorLocationsUpdated(){

    }
    public void didCompassUpdated(double direction){

    }

    public void didProximityEnterRegion(int region, int minor, double rssi, double distance){

    }

    public void didProximityExitRegion(int region){

    }
    public void didError(PMPLocationManagerError error){

    }
}
