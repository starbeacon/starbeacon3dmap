package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by andrewman on 6/3/2017.
 */
/*
{
  "threshold" : 0.3,
  "zoom_level" : 0,
  "width" : 10752,
  "height" : 21504,
  "version" : "20170225024437"
}
 */
public class MapImage implements Serializable {
    @SerializedName("threshold")
    private float threshold;
    @SerializedName("zoom_level")
    private int zoomLevel;
    @SerializedName("width")
    private int width;
    @SerializedName("height")
    private int height;
    @SerializedName("version")
    private String version;

    public float getThreshold() {
        return threshold;
    }

    public int getZoomLevel() {
        return zoomLevel;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getVersion() {
        return version;
    }
}
