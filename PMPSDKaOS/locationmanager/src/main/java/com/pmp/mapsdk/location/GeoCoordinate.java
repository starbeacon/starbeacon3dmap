package com.pmp.mapsdk.location;

import android.graphics.PointF;

/**
 * Created by Yu on 10/8/16.
 */
public class GeoCoordinate {
    public static boolean isInsidePolygon(PointF currentLocation, PointF[] vertices) {
        int i = 0, j = 0;
        boolean c = false;
        PointF vi, vj;
        for (i = 0, j = vertices.length - 1; i < vertices.length; j = i++) {
            vi = vertices[i];
            vj = vertices[j];
            if (((vi.y > currentLocation.y) != (vj.y > currentLocation.y)) &&
                    (currentLocation.x < (vj.x - vi.x) * (currentLocation.y - vi.y) / (vj.y - vi.y) + vi.x)) {
                c = !c;
            }
        }
        return c;
    }
}
