package com.pmp.mapsdk.cms.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import org.json.*;


public class Name implements Parcelable {

    @SerializedName("language_id")
    private double languageId;
    @SerializedName("content")
    private String content;
    
    
	public Name () {
		
	}	
        
    public Name (JSONObject json) {
    
        this.languageId = json.optDouble("language_id");
        this.content = json.isNull("content")?"" : json.optString("content");

    }
    
    public double getLanguageId() {
        return this.languageId;
    }

    public void setLanguageId(double languageId) {
        this.languageId = languageId;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.languageId);
        dest.writeString(this.content);
    }

    protected Name(Parcel in) {
        this.languageId = in.readDouble();
        this.content = in.readString();
    }

    public static final Parcelable.Creator<Name> CREATOR = new Parcelable.Creator<Name>() {
        @Override
        public Name createFromParcel(Parcel source) {
            return new Name(source);
        }

        @Override
        public Name[] newArray(int size) {
            return new Name[size];
        }
    };
}
