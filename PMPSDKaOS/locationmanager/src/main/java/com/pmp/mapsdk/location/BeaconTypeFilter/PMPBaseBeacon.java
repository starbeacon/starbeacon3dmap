package com.pmp.mapsdk.location.BeaconTypeFilter;

import org.altbeacon.beacon.Beacon;

/**
 * Created by Yu on 12/7/16.
 */
public class PMPBaseBeacon {
    Beacon aosbeacon;

    public String proximityUUID() { return aosbeacon.getId1().toString(); }
    public Integer getMajor() { return aosbeacon.getId2().toInt(); }
    public Integer getMinor() { return aosbeacon.getId3().toInt(); }
    public double getAccuracy() { return aosbeacon.getDistance(); }
    public Integer getRssi() { return aosbeacon.getRssi(); }
}
