package com.pmp.mapsdk.location;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.cms.PMPServerManagerNotifier;
import com.pmp.mapsdk.cms.model.Devices;
import com.pmp.mapsdk.cms.model.ResponseData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wongkinyu on 1/4/18.
 */

public class PMPCore implements PMPLocationManagerNotifier {
    private static final String TAG = PMPCore.class.getSimpleName();
    private volatile static PMPCore shared;
    private final Context context;
    private PMPBeaconType _beaconType;
    private PMPLocation _location;

    private PMPCore (Context context){
        this.context = context.getApplicationContext();
    }

    public static PMPCore getShared(Context context) {
        if(shared == null) {
            synchronized (PMPCore.class) {
                if(shared == null) {
                    shared = new PMPCore(context);
                    PMPLocationManager.getShared(context);
                }
            }
        }
        return shared;
    }



    public void start(int projectId) {
        PMPServerManager.getShared(context).downloadMap(projectId, new PMPServerManagerNotifier() {
            @Override
            public void didSuccess(String responseString) {
                ResponseData responseData = PMPServerManager.getShared(context).getServerResponse();
                List<PMPBeacon> list = new ArrayList<PMPBeacon>();
                for (Devices device : responseData.getDevices()) {
                    if(device.getBleWayFindingUuid() != null){

                        PMPBeacon beacon = new PMPBeacon(device, device.getBleWayFindingUuid(), responseData.getMapScale());
                        list.add(beacon);
                    }
                }

                PMPLocationManager mgr = PMPLocationManager.getShared(context);
                mgr.setEnableLog(true);
                mgr.setEnableBeaconAccuracyFilter(false);
                mgr.setEnableLowPassFilter(false);
                mgr.setEnableMaxNumOfBeaconsDetection(false);
                mgr.setEnableKalmanFilter(true);
                mgr.addDelegate(PMPCore.this);
                mgr.setProximityUUIDS(list);
                mgr.start();
            }

            @Override
            public void didFailure() {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("WARNING");
                builder.setMessage("Download failed");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
    }

    public  void stop() {
        PMPLocationManager mgr = PMPLocationManager.getShared(context);
        mgr.stop();
    }

    @Override
    public void didIndoorLocationUpdated(PMPBeaconType beaconType, PMPLocation location) {
        Log.d(TAG, "didIndoorLocationUpdated: " + beaconType + "," + String.format("(%f,%f)", location.getX(), location.getY()));
        _beaconType = beaconType;
        _location = location;
    }

    @Override
    public void didIndoorTransmissionsUpdated(List<PMPBeacon> transmissions) {

    }

    @Override
    public void didIndoorExitRegion() {

    }

    @Override
    public void didOutdoorLocationsUpdated() {

    }

    @Override
    public void didCompassUpdated(double direction) {

    }

    @Override
    public void didProximityEnterRegion(int region, int minor, double rssi, double distance) {

    }

    @Override
    public void didProximityExitRegion(int region) {

    }

    @Override
    public void didError(PMPLocationManagerError error) {

    }

    public PMPBeaconType getBeaconType() {
        return _beaconType;
    }

    public PMPLocation getLocation() {
        return _location;
    }
}
