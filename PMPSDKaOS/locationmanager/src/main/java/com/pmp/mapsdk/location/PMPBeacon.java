package com.pmp.mapsdk.location;

import android.graphics.PointF;
import android.os.Parcel;

import com.android.internal.util.Predicate;
import com.pmp.mapsdk.beacon.BaseDevices;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.Identifier;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

//import org.altbeacon.beacon.Beacon;

/**
 * Created by Yu on 17/2/16.
 */
public class PMPBeacon extends Beacon {
    /*
     *  scale accuracy
     *
     *  Discussion:
     *    Represents an one sigma horizontal accuracy in meters where the measuring device's location is
     *    referenced at the beaconing device. This value is heavily subject to variations in an RF environment.
     *    A negative accuracy value indicates the proximity is unknown.
     *
     */
    public double getScaleDistance() {
        return getDistance() * mapScaleRatio;
    }

    public int getBeaconType() {
        return beaconType;
    }

    public void setBeaconType(int beaconType) {
        this.beaconType = beaconType;
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    public double getMapScaleRatio() {
        return mapScaleRatio;
    }

    public void setMapScaleRatio(double mapScaleRatio) {
        this.mapScaleRatio = mapScaleRatio;
    }

    public double getOutdoorPathRatio() {
        return outdoorPathRatio;
    }

    public void setOutdoorPathRatio(double outdoorPathRatio) {
        this.outdoorPathRatio = outdoorPathRatio;
    }

    public List<PointF> getOutdoorPath() {
        return outdoorPath;
    }

    public void setOutdoorPath(List<PointF> outdoorPath) {
        this.outdoorPath = outdoorPath;
    }

    public PMPLocation getLocation() {
        return location;
    }

    public void setLocation(PMPLocation location) {
        this.location = location;
    }

    /*
         *  beacon type
         *
         *  Discussion:
         *    Beacon type.
         *
         */
    private int beaconType;

    /*
     *  threshold
     *
     *  Discussion:
     *    Beacon threshold.
     *
     */
    private double threshold;

    /*
     *  mapScaleRatio
     *
     *  Discussion:
     *    Map scale ratio.
     *
     */
    private double mapScaleRatio;

    /*
 *  outdoor path ratio
 *
 *  Discussion:
 *    Beacon outdoor path ratio.
 *
 */
    private double outdoorPathRatio;

    /*
     *  outdoor path
     *
     *  Discussion:
     *    Beacon outdoor path.
     *
     */
    private List<PointF> outdoorPath;

    /*
     *  Location
     *
     *  Discussion:
     *    Location of the beacon place.
     *
     */
    private PMPLocation location;

    private boolean usingForTrilateration = false;
    private int batteryLevel = 0;

    public int getMajorIntValue() {
        return getId2().toInt();
    }

    public int getMinorIntValue() {
        return getId3().toInt();
    }

    public int getRssiIntValue() {
        return getRssi();
    }

    public boolean getUsingForTrilateration() { return usingForTrilateration; }

    public void setUsingForTrilateration(boolean usingForTrilateration) { this.usingForTrilateration = usingForTrilateration; }

    private native PMPBeacon decryptBeacon(Beacon beacon);

    static {

    }


    public static final Creator<Beacon> CREATOR = new Creator() {
        public PMPBeacon createFromParcel(Parcel in) {
            return new PMPBeacon(in);
        }

        public PMPBeacon[] newArray(int size) {
            return new PMPBeacon[size];
        }
    };

    protected PMPBeacon(Parcel in) {
        super(in);
        this.location = in.readParcelable(PMPLocation.class.getClassLoader());
        this.beaconType = in.readInt();
        this.threshold = in.readInt();
        this.mapScaleRatio = in.readDouble();
        this.outdoorPath = in.readArrayList(PointF.class.getClassLoader());
    }

    public PMPBeacon() {
        super();
    }

    public PMPBeacon(Beacon beacon) {
        super(beacon);
        int major = beacon.getId2().toInt();
        int minor = beacon.getId3().toInt();
        int encoded = 0xffff & major;
        int keyType = 0xf & (minor >> 12);
        batteryLevel = 0xf & (minor >> 8);
        int crc = 0xff & minor;
        if (keyType != 0) {
            if (crc8Bit(0xFFFFFF & (keyType << 20  | (batteryLevel << 16) | encoded), 24) == crc) {
                mIdentifiers.set(1, Identifier.fromInt(encoded ^ keyStore[keyType]));
            } else {
                mIdentifiers.set(1, Identifier.fromInt(0));
            }
        } else {
            mIdentifiers.set(1, Identifier.fromInt(major));
        }
    }

    public PMPBeacon(PMPBeacon beacon) {
        super(beacon);
        this.combineWithLocation(beacon);
    }

    public PMPBeacon(BaseDevices devices, String uuid, int beaconType, double mapScaleRatio) {
        super();
        mIdentifiers.add(Identifier.parse(uuid));
        mIdentifiers.add(Identifier.fromInt((int)devices.getMajorNo()));
        mIdentifiers.add(Identifier.fromInt(0));
        mRssi = 0;
        this.location = new PMPLocation(devices.getX(), devices.getY(), devices.getZ(), String.format("%d", (int)devices.getMapId()));
        this.beaconType = beaconType;
        this.threshold = devices.getThreshold();
        if (devices.getDeviceOptions() != null) {
            if (devices.getDeviceOptions().getOutdoorPathRatio() != null) {
                try {
                    this.outdoorPathRatio = Double.valueOf(devices.getDeviceOptions().getOutdoorPathRatio());
                } catch (NumberFormatException e) {

                }
            }
            this.outdoorPath = devices.getDeviceOptions().getNearbyOutdoorNodes();
        }
        this.mapScaleRatio = mapScaleRatio;
    }

    public PMPBeacon(BaseDevices devices, String uuid, double mapScaleRatio) {
        super();
        mIdentifiers.add(Identifier.parse(uuid));
        mIdentifiers.add(Identifier.fromInt((int)devices.getMajorNo()));
        mIdentifiers.add(Identifier.fromInt(0));
        mRssi = 0;
        this.location = new PMPLocation(devices.getX(), devices.getY(), devices.getZ(), String.format("%d", (int)devices.getMapId()));
        this.beaconType = (int)devices.getMapDeviceTypeId();
        this.threshold = devices.getThreshold();
        if (devices.getDeviceOptions() != null) {
            if (devices.getDeviceOptions().getOutdoorPathRatio() != null) {
                try {
                    this.outdoorPathRatio = Double.valueOf(devices.getDeviceOptions().getOutdoorPathRatio());
                } catch (NumberFormatException e) {

                }
            }
            this.outdoorPath = devices.getDeviceOptions().getNearbyOutdoorNodes();
        }
        this.mapScaleRatio = mapScaleRatio;
    }

    public static PMPBeacon transmissionWithCLBeacon(PMPBeacon CLBeacon) {
        if (CLBeacon.getRssi() == 0) {
            return null;
        }
        else {
            return findBeaconByCLBeacon(CLBeacon);
        }
    }


    public Predicate<PMPBeacon> uuidEqualsTo() {
        return new Predicate<PMPBeacon>() {

            public boolean apply(PMPBeacon dataPoint) {
                return dataPoint.getId1().toString().equals(PMPBeacon.this.getId1().toString());
            }
        };
    }

    public Predicate<PMPBeacon> majorEqualsTo() {
        return new Predicate<PMPBeacon>() {

            public boolean apply(PMPBeacon dataPoint) {
                return dataPoint.getMajorIntValue() == PMPBeacon.this.getMajorIntValue();
            }
        };
    }

    static PMPBeacon findBeaconByCLBeacon(PMPBeacon beacon) {
        final PMPBeacon currentBeacon = beacon;
        PMPBeacon beaconFound = CollectionUtils.find(PMPLocationManager.getShared().proximityUUIDS, new org.apache.commons.collections4.Predicate<PMPBeacon>() {
            @Override
            public boolean evaluate(PMPBeacon object) {
                return object.getMajorIntValue()== currentBeacon.getMajorIntValue();
            }
        });
        if (beaconFound != null) {
            return beacon.combineWithLocation(beaconFound);
        }
        return null;
    }

    public PMPBeacon combineWithLocation(PMPBeacon beacon) {
        this.location = beacon.getLocation();
        this.beaconType = beacon.getBeaconType();
        this.threshold = beacon.getThreshold();
        this.mapScaleRatio = beacon.getMapScaleRatio();
        this.outdoorPathRatio = beacon.getOutdoorPathRatio();
        this.outdoorPath = beacon.getOutdoorPath();
        return this;
    }

    public String toString() {
        return "{" + this.getId1() + " :, major=" + this.getId2() + ", minor=" + this.getId3() + ", RSSI " + this.getRssi() + ", accuracy " + this.getDistance() + "}";
    }


    /* Beacon Encryption */
    private int[] crcTable = {
        0x0, 0xd5, 0x7f, 0xaa, 0xfe, 0x2b, 0x81, 0x54, 0x29, 0xfc, 0x56, 0x83, 0xd7, 0x2, 0xa8, 0x7d,
                0x52, 0x87, 0x2d, 0xf8, 0xac, 0x79, 0xd3, 0x6, 0x7b, 0xae, 0x4, 0xd1, 0x85, 0x50, 0xfa, 0x2f,
                0xa4, 0x71, 0xdb, 0xe, 0x5a, 0x8f, 0x25, 0xf0, 0x8d, 0x58, 0xf2, 0x27, 0x73, 0xa6, 0xc, 0xd9,
                0xf6, 0x23, 0x89, 0x5c, 0x8, 0xdd, 0x77, 0xa2, 0xdf, 0xa, 0xa0, 0x75, 0x21, 0xf4, 0x5e, 0x8b,
                0x9d, 0x48, 0xe2, 0x37, 0x63, 0xb6, 0x1c, 0xc9, 0xb4, 0x61, 0xcb, 0x1e, 0x4a, 0x9f, 0x35, 0xe0,
                0xcf, 0x1a, 0xb0, 0x65, 0x31, 0xe4, 0x4e, 0x9b, 0xe6, 0x33, 0x99, 0x4c, 0x18, 0xcd, 0x67, 0xb2,
                0x39, 0xec, 0x46, 0x93, 0xc7, 0x12, 0xb8, 0x6d, 0x10, 0xc5, 0x6f, 0xba, 0xee, 0x3b, 0x91, 0x44,
                0x6b, 0xbe, 0x14, 0xc1, 0x95, 0x40, 0xea, 0x3f, 0x42, 0x97, 0x3d, 0xe8, 0xbc, 0x69, 0xc3, 0x16,
                0xef, 0x3a, 0x90, 0x45, 0x11, 0xc4, 0x6e, 0xbb, 0xc6, 0x13, 0xb9, 0x6c, 0x38, 0xed, 0x47, 0x92,
                0xbd, 0x68, 0xc2, 0x17, 0x43, 0x96, 0x3c, 0xe9, 0x94, 0x41, 0xeb, 0x3e, 0x6a, 0xbf, 0x15, 0xc0,
                0x4b, 0x9e, 0x34, 0xe1, 0xb5, 0x60, 0xca, 0x1f, 0x62, 0xb7, 0x1d, 0xc8, 0x9c, 0x49, 0xe3, 0x36,
                0x19, 0xcc, 0x66, 0xb3, 0xe7, 0x32, 0x98, 0x4d, 0x30, 0xe5, 0x4f, 0x9a, 0xce, 0x1b, 0xb1, 0x64,
                0x72, 0xa7, 0xd, 0xd8, 0x8c, 0x59, 0xf3, 0x26, 0x5b, 0x8e, 0x24, 0xf1, 0xa5, 0x70, 0xda, 0xf,
                0x20, 0xf5, 0x5f, 0x8a, 0xde, 0xb, 0xa1, 0x74, 0x9, 0xdc, 0x76, 0xa3, 0xf7, 0x22, 0x88, 0x5d,
                0xd6, 0x3, 0xa9, 0x7c, 0x28, 0xfd, 0x57, 0x82, 0xff, 0x2a, 0x80, 0x55, 0x1, 0xd4, 0x7e, 0xab,
                0x84, 0x51, 0xfb, 0x2e, 0x7a, 0xaf, 0x5, 0xd0, 0xad, 0x78, 0xd2, 0x7, 0x53, 0x86, 0x2c, 0xf9,
    };

    private int[] keyStore = {
        0x0, /* Disable */
                0x41a8,
                0x4bc8,
                0xd92,
                0x46e0,
                0xfbb4,
                0xf6cf,
                0x94de,
                0x60e4,
                0x8637,
                0x7bf2,
                0xc9af,
                0x7ed2,
                0x7cea,
                0xe04f,
                0xdb8b
    };

    int crc8_arr(int[] bytes, int length) {
        int crc = 0;
        for(int i = 0; i < length; i++)
        {
            int b = bytes[i];
            crc = crcTable[crc ^ b];
        }
        return crc;
    }

    int crc8Bit(int input, int lengthInBit) {
        int length = lengthInBit / 8;
        int[] inputs = new int[length];

        for (int index = 0; index < length; index++) {
            inputs[index] = (input >> (8 * (length - index - 1))) & 0xFF;
        }

        int result = crc8_arr(inputs, length);
        return result;
    }

    public int getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(int batteryLevel) {
        this.batteryLevel = batteryLevel;
    }
}
