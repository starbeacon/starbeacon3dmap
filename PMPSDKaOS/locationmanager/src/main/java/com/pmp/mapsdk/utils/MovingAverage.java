package com.pmp.mapsdk.utils;

import java.util.ArrayList;

public class MovingAverage {
	private ArrayList<Double> rssiSamples = null;
	private ArrayList<Double> distanceSamples = null;
	private int sampleCount = 0;
	private int averageSize = 0;
	private int value = 0;

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public MovingAverage(int size) {
		rssiSamples = new ArrayList<Double>(size);
		for (int index = 0; index < size; index++) {
			rssiSamples.add(Double.valueOf(0));
		}

		distanceSamples = new ArrayList<Double>(size);
		for (int index = 0; index < size; index++) {
			distanceSamples.add(Double.valueOf(0));
		}


		sampleCount = 0;
		averageSize = size;
	}

	public void addSample(double rssi, double distance) {
		// int pos = (int) Math.IEEEremainder(sampleCount++, (double)
		// averageSize);
		int position = sampleCount++ % averageSize;
		rssiSamples.set(position, rssi);
		distanceSamples.set(position, distance);
	}

	public double rssiAvg() {
		double sum = 0;
		for (double a : rssiSamples) {
			sum += a;
		}
		return sum
				/ ((sampleCount > averageSize - 1) ? averageSize : sampleCount);
	}

	public double distanceAvg() {
		double sum = 0;
		for (double a : distanceSamples) {
			sum += a;
		}
		return sum
				/ ((sampleCount > averageSize - 1) ? averageSize : sampleCount);
	}
}
