package com.pmp.mapsdk.cms;

/**
 * Created by Yu on 16/2/16.
 */
public enum PMPServerType {
    Production,
    Development
}
