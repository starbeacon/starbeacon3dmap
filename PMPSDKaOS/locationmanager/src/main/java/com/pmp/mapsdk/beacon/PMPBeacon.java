package com.pmp.mapsdk.beacon;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Yu on 17/2/16.
 */

public class PMPBeacon {
    private Timer timer;
    private String uuid;
    private int major;
    private int minor;
    private double rssi;
    private double distance;
    private long timeout;
    private PMPBeaconTimerNotifier beaconTimerNotifier;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getMajor() {
        return major;
    }

    public void setMajor(int major) {
        this.major = major;
    }

    public int getMinor() {
        return minor;
    }

    public void setMinor(int rssi) {
        this.minor = minor;
    }

    public double getRssi() { return rssi; }

    public void setRssi(double rssi) {
        this.rssi = rssi;
    }

    public double getDistance(){ return  distance; }

    public void setDistance(double distance){this.distance = distance; };

    public PMPBeacon(String uuid, int major, int minor, double rssi, double distance, long timeout,
                     PMPBeaconTimerNotifier beaconTimerNotifier) {
        this.uuid = uuid;
        this.major = major;
        this.minor = minor;
        this.rssi = rssi;
        this.distance = distance;
        this.timeout = timeout;
        this.beaconTimerNotifier = beaconTimerNotifier;
        if (timeout > 0) {
            timer = new Timer();
            timer.schedule(new TimerTask() {

                @Override
                public void run() {
                    if (PMPBeacon.this.beaconTimerNotifier != null) {
                        PMPBeacon.this.beaconTimerNotifier.didTimeout(PMPBeacon.this);
                    }
                }
            }, this.timeout);
        }
    }

    public void resetTimeout() {
        removeTimer();
        if (timeout > 0) {
            timer = new Timer();
            timer.schedule(new TimerTask() {

                @Override
                public void run() {
                    if (PMPBeacon.this.beaconTimerNotifier != null) {
                        PMPBeacon.this.beaconTimerNotifier.didTimeout(PMPBeacon.this);
                    }
                }
            }, this.timeout);
        }
    }

    public void removeTimer() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
            timer = null;
        }
    }

    public boolean compare(PMPBeacon beacon) {
        return beacon.getMinor() == this.getMinor()
                && beacon.getMajor() == this.getMajor()
                && beacon.getUuid() != null
                && beacon.getUuid().equals(this.uuid);
    }
}
