package com.pmp.mapsdk.cms.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

import org.json.*;


public class EshopUrl implements Parcelable {

    @SerializedName("language_id")
    private double languageId;
    @SerializedName("content")
    private String content;
    
    
	public EshopUrl () {
		
	}	
        
    public EshopUrl (JSONObject json) {
        this.languageId = json.optDouble("language_id");
        this.content = json.isNull("content")?"" : json.optString("content");

    }
    
    public double getLanguageId() {
        return this.languageId;
    }

    public void setLanguageId(double languageId) {
        this.languageId = languageId;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.languageId);
        dest.writeString(this.content);
    }

    protected EshopUrl(Parcel in) {
        this.languageId = in.readDouble();
        this.content = in.readString();
    }

    public static final Parcelable.Creator<EshopUrl> CREATOR = new Parcelable.Creator<EshopUrl>() {
        @Override
        public EshopUrl createFromParcel(Parcel source) {
            return new EshopUrl(source);
        }

        @Override
        public EshopUrl[] newArray(int size) {
            return new EshopUrl[size];
        }
    };
}
