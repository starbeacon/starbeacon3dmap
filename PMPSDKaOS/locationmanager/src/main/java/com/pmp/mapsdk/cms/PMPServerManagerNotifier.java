package com.pmp.mapsdk.cms;

import com.pmp.mapsdk.cms.model.ResponseData;

/**
 * Created by Yu on 18/2/16.
 */
public interface PMPServerManagerNotifier {
    void didSuccess( String responseString);
    void didFailure();
}
