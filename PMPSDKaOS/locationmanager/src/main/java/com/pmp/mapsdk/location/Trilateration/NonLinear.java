package com.pmp.mapsdk.location.Trilateration;

/**
 * Created by Yu on 5/4/16.
 */
public class NonLinear {
    static {
        System.loadLibrary("eigenNative");
    }
    public static native Location determine(Location[] locations);
}
