package com.pmp.mapsdk.location;

/**
 * Created by Yu on 18/2/16.
 */
public class PMPPoint3D {
    public static final PMPPoint3D ZERO = new PMPPoint3D(0, 0, 0);
    double x, y, z;

    public PMPPoint3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    double distanceFrom(PMPPoint3D point) {
        return Math.sqrt(Math.pow(point.x - this.x, 2) + Math.pow(point.y - this.y, 2) + Math.pow(point.z - this.z, 2));
    }

    static PMPPoint3D calculatePointOnPath(PMPPoint3D point1, PMPPoint3D point2, double distance) {
        PMPPoint3D result = PMPPoint3D.ZERO;

        double de = Math.pow(point2.x - point1.x, 2) + Math.pow(point2.y - point1.y, 2) + Math.pow(point2.z - point1.z, 2);

        double t1 = Math.sqrt(Math.pow(distance, 2) / de);
//    double t2 = -sqrt(pow(distance, 2)/de);

        return new PMPPoint3D(point1.x + (point2.x - point1.x) * t1, point1.y + (point2.y - point1.y) * t1, point1.z + (point2.z - point1.z) * t1);
    }

}
