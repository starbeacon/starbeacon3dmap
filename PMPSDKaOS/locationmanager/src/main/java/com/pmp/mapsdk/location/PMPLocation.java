package com.pmp.mapsdk.location;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Yu on 17/2/16.
 */
public class PMPLocation implements Parcelable {
    protected PMPLocation(Parcel in) {
        x = in.readDouble();
        y = in.readDouble();
        z = in.readDouble();
        name = in.readString();
    }

    public static final Creator<PMPLocation> CREATOR = new Creator<PMPLocation>() {
        @Override
        public PMPLocation createFromParcel(Parcel in) {
            return new PMPLocation(in);
        }

        @Override
        public PMPLocation[] newArray(int size) {
            return new PMPLocation[size];
        }
    };

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public String getName() {
        return name;
    }

    private double x, y, z;
    private String name;
    public PMPLocation(double x, double y, double z, String name) {
        super();
        this.x = x;
        this.y = y;
        this.z = z;
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(x);
        dest.writeDouble(y);
        dest.writeDouble(z);
        dest.writeString(name);
    }

    @Override
    public String toString() {
        return String.format("PMPLocation (%f,%f,%f) : %s", x, y, z, name);
    }
}
