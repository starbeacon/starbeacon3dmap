package com.pmp.mapsdk.utils;

import java.util.Collection;

/**
 * Created by Yu on 16/1/2017.
 */

public class MathUtil {
    public static double findDeviation(Collection<Double> nums)
    {

        double mean = findMean(nums);

        double squareSum = 0;

        for (double value : nums) {
            squareSum += Math.pow(value - mean, 2);
        }

        return Math.sqrt((squareSum) / (nums.size() - 1));

    } // End of double findDeviation(double[])
    public static double findDeviation(double[] nums)
    {

        double mean = findMean(nums);

        double squareSum = 0;

        for (int i = 0; i < nums.length; i++)
        {

            squareSum += Math.pow(nums[i] - mean, 2);

        }

        return Math.sqrt((squareSum) / (nums.length - 1));

    } // End of double findDeviation(double[])

/* Method for computing deviation of int values */


// Beginning of double findDeviation(int[])

    public static double findDeviation(int[] nums)
    {

        double mean = findMean(nums);

        double squareSum = 0;

        for (int i = 0; i < nums.length; i++)
        {

            squareSum += Math.pow(nums[i] - mean, 2);

        }

        return Math.sqrt((squareSum) / (nums.length - 1));

    } // End of double findDeviation(int[])


    /** Method for computing mean of an array of double values */

// Beginning of double findMean(double[])

    public static double findMean(Collection<Double> nums)
    {

        double sum = 0;

        for (Double value : nums) {
            sum += value;
        }

        return sum / nums.size();

    } // End of double getMean(double[])

    public static double findMean(double[] nums)
    {

        double sum = 0;

        for (int i = 0; i < nums.length; i++)
        {

            sum += nums[i];

        }

        return sum / nums.length;

    } // End of double getMean(double[])


    /** Method for computing mean of an array of int values */

// Beginning of double findMean(int[])

    public static double findMean(int[] nums)
    {
        double sum = 0;

        for (int i = 0; i < nums.length; i++)
        {
            sum += nums[i];

        }

        return sum / nums.length;

    } // End of double getMean(int[])
}
