package com.pmp.mapsdk.location;

/**
 * Created by Yu on 22/2/16.
 */
public interface PMPAppLifeCycle {
    abstract void onCreate();

    abstract void onPause();

    abstract void onResume();
}