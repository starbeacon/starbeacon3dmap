package com.pmp.mapsdk.location.Trilateration;

import android.graphics.PointF;

/**
 * Created by Yu on 6/7/2017.
 */

public class KF {
    static {
        System.loadLibrary("KF");
    }

    public static native void init(Location location);

    public static native Location tracking(Location location);
}
