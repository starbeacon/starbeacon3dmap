package com.pmp.mapsdk.beacon;

import org.altbeacon.beacon.Beacon;

import java.util.List;

/**
 * Created by Yu on 16/2/16.
 */
public interface PMPBeaconManagerNotifier {
    /**
     * Called once per second to give an estimate of the mDistance to visible beacons
     * @param beacons a collection of <code>Beacon<code> objects that have been seen in the past second
     */
    public void didRangeBeacons(List<Beacon> beacons);

    /**
     * Called when at least one beacon in a <code>Region</code> is visible.
     * @param region a Region that defines the criteria of beacons to look for
     */
    public void didProximityEnterRegion(int region, int minor, double rssi, double distance);

    /**
     * Called when no beacons in a <code>Region</code> are visible.
     */
    public void didProximityExitRegion(int region);

    /**
     * Called when error happen.
     * @param error Error code.
     */
    public void didError(PMPBeaconManagerError error);
}
