//package com.pmp.mapsdk.cms;
//
//import android.os.Parcel;
//import android.os.Parcelable;
//
//import com.pmp.mapsdk.cms.model.Nodes;
//import com.pmp.mapsdk.cms.model.Pois;
//import com.pmp.mapsdk.cms.model.ResponseData;
//import com.pmp.mapsdk.cms.model.SaveUserFlightResponse;
//import com.pmp.mapsdk.utils.PMPUtil;
//
//import org.apache.commons.collections4.CollectionUtils;
//import org.apache.commons.collections4.Predicate;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
///**
// * Created by joeyyc on 2/12/2016.
// */
//
//public class PMPPOIByDuration implements Parcelable {
//
//    public Pois poi;
//    public float duration;
//
//    public PMPPOIByDuration(Pois p, float d) {
//        this.poi = p;
//        this.duration = d;
//    }
//
//    private static List<PMPPOIByDuration> filterByVertex(int vertId, float duration) {
//        List<PMPPOIByDuration> result = new ArrayList<PMPPOIByDuration>();
//        VertexByDuration[] vertexByDurations = CoreEngine.getInstance().filterVertexByDuration(vertId,duration);
//        Map<Integer, Pois> poisMap = PMPServerManager.getShared().getServerResponse().getPoisMap();
//        ArrayList<Integer> cachedPois = new ArrayList<>();
//        for (VertexByDuration vertexByDuration : vertexByDurations) {
//            Nodes node = vertexByDuration.getNode();
//            float d = vertexByDuration.getDuration();
//            if (node != null && node.getPoiIds() != null) {
//                for (int poiId : node.getPoiIds()) {
//                    if (poisMap.containsKey(poiId) && !cachedPois.contains(poiId)) {
//                        Pois p = poisMap.get(poiId);
//                        PMPPOIByDuration poiByDuration = new PMPPOIByDuration(p, d);
//                        result.add(poiByDuration);
//                        cachedPois.add(poiId);
//                    }
//                }
//            }
//        }
//        return result;
//    }
//
//    public static List<PMPPOIByDuration> filterAroundMeByDuration(float duration) {
//        return filterByVertex(CoreEngine.CurrentLocationVertexID, duration);
//    }
//
//    public static List<PMPPOIByDuration> filterAroundPOIByDuration(Pois poi, float duration) {
//        ArrayList<PMPPOIByDuration> result = new ArrayList<PMPPOIByDuration>();
//        for (int nodeId : poi.getNodeIds()) {
//            List<PMPPOIByDuration> tmp = filterByVertex(nodeId, duration);
//            addAroundPOIToResult(result, tmp);
//        }
//        return result;
//    }
//
//    public static List<PMPPOIByDuration> getAll() {
//        return filterByVertex(CoreEngine.CurrentLocationVertexID, -1);
//    }
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeParcelable(this.poi, flags);
//        dest.writeFloat(this.duration);
//    }
//
//    protected PMPPOIByDuration(Parcel in) {
//        this.poi = in.readParcelable(Pois.class.getClassLoader());
//        this.duration = in.readFloat();
//    }
//
//    public static final Parcelable.Creator<PMPPOIByDuration> CREATOR = new Parcelable.Creator<PMPPOIByDuration>() {
//        @Override
//        public PMPPOIByDuration createFromParcel(Parcel source) {
//            return new PMPPOIByDuration(source);
//        }
//
//        @Override
//        public PMPPOIByDuration[] newArray(int size) {
//            return new PMPPOIByDuration[size];
//        }
//    };
//
//    public static Pois getSavedFlightGate() {
//        SaveUserFlightResponse response = PMPProximityServerManager.getShared().getSaveUserFlightResponse();
//        ResponseData responseData = PMPDataManager.getSharedPMPManager(null).getResponseData();
//        if (response != null &&
//                response.getResult() != null &&
//                response.getResult().getGateCode() != null &&
//                response.getResult().getGateCode().length() > 0 &&
//                responseData != null) {
//            final String externalIDForGate = response.getResult().getGateCode();
//            final Pois foundPoi = CollectionUtils.find(responseData.getPois(), new Predicate<Pois>() {
//                @Override
//                public boolean evaluate(Pois object) {
//                    return object.getExternalId().equals(externalIDForGate);
//                }
//            });
//            return foundPoi;
//        }
//        return null; //temporary disable
//    }
//
//    public static void addAroundPOIToResult(ArrayList<PMPPOIByDuration> result, List<PMPPOIByDuration> tmp) {
//        if (result!= null && tmp != null) {
//            for (PMPPOIByDuration new_poi : tmp) {
//                boolean found = false;
//                for (int i=0; i<result.size(); i++) {
//                    PMPPOIByDuration poi = result.get(i);
//                    if (new_poi.poi.getId() == poi.poi.getId()) {
//                        found = true;
//                        if (Math.ceil(new_poi.duration/60.0) < Math.ceil(poi.duration/60.0)) {
//                            result.set(i, new_poi);
//                        }
//                        break;
//                    }
//                }
//                if (!found) {
//                    result.add(new_poi);
//                }
//            }
//        }
//    }
//
//    public static void addDurationPOI(PMPPOIByDuration poi, List<PMPPOIByDuration> poiList) {
//        if (poi != null && poiList != null) {
//            for (int i = 0; i < poiList.size(); i++) {
//                PMPPOIByDuration tempPOI = poiList.get(i);
//                if (poi.duration >= 0 && poi.duration < Float.MAX_VALUE) {
//                    if (Math.ceil(tempPOI.duration / 60.0) > Math.ceil(poi.duration / 60.0) || (tempPOI.duration < 0 || tempPOI.duration == Float.MAX_VALUE)) {
//                        poiList.add(i, poi);
//                        return;
//                    } else if (Math.ceil(tempPOI.duration / 60.0) == Math.ceil(poi.duration / 60.0)) {
//                        int compareResult = comparePOIName(PMPUtil.getLocalizedString(tempPOI.poi.getName()), (PMPUtil.getLocalizedString(poi.poi.getName())));
//                        if (compareResult > 0) {
//                            poiList.add(i, poi);
//                            return;
//                        }
//                    }
//                } else if (tempPOI.duration < 0 || tempPOI.duration == Float.MAX_VALUE) {
//                    int compareResult = comparePOIName(PMPUtil.getLocalizedString(tempPOI.poi.getName()), (PMPUtil.getLocalizedString(poi.poi.getName())));
//                    if (compareResult > 0) {
//                        poiList.add(i, poi);
//                        return;
//                    }
//                }
//            }
//            poiList.add(poi);
//        }
//    }
//
//    public static int comparePOIName(String a, String b) {
//        String commonPrefix = greatestCommonPrefix(a, b);
//
//        if (commonPrefix.length()>0) {
//            String aNumber = "";
//            String bNumber = "";
//
//            for (int i = commonPrefix.length(); i<a.length(); i++) {
//                if ("0123456789".contains(a.substring(i, i+1))) {
//                    aNumber += a.substring(i, i+1);
//                } else {
//                    break;
//                }
//            }
//            for (int i = commonPrefix.length(); i<b.length(); i++) {
//                if ("0123456789".contains(b.substring(i, i+1))) {
//                    bNumber += b.substring(i, i+1);
//                } else {
//                    break;
//                }
//            }
//
//            if (aNumber.length()>0 && bNumber.length()>0) {
//                int aInt = 0;
//                int bInt = 0;
//
//                try {
//                    aInt = Integer.parseInt(aNumber);
//                } catch (Exception e) {}
//
//                try {
//                    bInt = Integer.parseInt(bNumber);
//                } catch (Exception e) {}
//
//                if (aInt > 0 && bInt > 0) {
//                    if (aInt < bInt) return -1;
//                    if (aInt == bInt) return 0;
//                    return 1;
//                }
//            }
//        }
//
//        return a.compareToIgnoreCase(b);
//    }
//
//    public static String greatestCommonPrefix(String a, String b) {
//        int minLength = Math.min(a.length(), b.length());
//        for (int i = 0; i < minLength; i++) {
//            if (a.charAt(i) != b.charAt(i) || "0123456789".contains(a.substring(i, i+1))) {
//                return a.substring(0, i);
//            }
//        }
//        return a.substring(0, minLength);
//    }
//}
