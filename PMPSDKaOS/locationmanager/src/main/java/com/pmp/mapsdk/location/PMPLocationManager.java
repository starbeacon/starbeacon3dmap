package com.pmp.mapsdk.location;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.os.SystemClock;
import android.util.Log;

import com.cherrypicks.stepcounter.CPStepCounter;
import com.pmp.mapsdk.beacon.PMPBeaconManager;
import com.pmp.mapsdk.beacon.PMPBeaconManagerError;
import com.pmp.mapsdk.beacon.PMPBeaconManagerNotifier;
import com.pmp.mapsdk.location.Trilateration.KF;
import com.pmp.mapsdk.location.Trilateration.Location;
import com.pmp.mapsdk.location.Trilateration.NonLinear;
import com.pmp.mapsdk.logging.LogManager;

import org.altbeacon.beacon.Beacon;
import org.apache.commons.collections4.Closure;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import map.enterprise.keewee.com.locationmanager.R;

/**
 * Created by Yu on 17/2/16.
 */
public class PMPLocationManager implements PMPBeaconManagerNotifier {
    public static final String TAG = PMPLocationManager.class.getSimpleName();
    public static final String VERSION = "1.0.1";
    public static final int NOT_FOUND = -1;
    public static final int PMP_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;
    public static final float LOW_PASS_FILTER_FACTOR = 0.3f;
    public static final int MIN_OUTDOOR_PATH_POINT = 2;
    public static final int MIN_NUM_OF_BEACON_TO_INDOOR = 3;
    public static final String ENTRY_INDOOR = "entryIndoor";
    public static final String ENTRY_OUTDOOR = "entryOutdoor";
    public static final String ALL_BEACONS = "allBeacons";
    public boolean enableKalmanFilter;

    private static final String regexStr = "^[0-9]*$";
    private volatile static PMPLocationManager shared;
    private SharedPreferences _userDefaults;
    private Context context;
    private ArrayList<PMPBeacon> beaconRegions;
    private Map<String, PMPBeacon> allbeacons;
    private ArrayList<Integer> floorSwitchingFilters = new ArrayList<>();
    private boolean enableFloorSwitchingFilters;
    private boolean enableBeaconAccuracyFilter;
    private int beaconAccuracyFilter;
    private String currentFloorDetected;
    private String lastFloorDetected;
    private PMPLocation lastLocation;
    private PMPBeacon entryPathBeaconDetected;
    private PMPBeaconManager _locationManager;
    private PMPBeaconManager _proximityManager;
    private boolean blePowerSaveMode = false;
    private int numberOfValidEntryIndoorDetected;
    private int numberOfValidPositioningBeaconDetected;
    private int indoorLocationTimeOut;

    private boolean enableWalkingDetection;
    private int numOfStationaryToResume = 5;
    private AtomicInteger stationaryCount = new AtomicInteger(0);
    private PMPLocation walkingDetectionFirstLocation = null;
    private PMPLocation walkingDetectionLastLocation = null;
    private boolean isRegisterStepCounter = false;
    private AtomicBoolean isWalking = new AtomicBoolean(false);
    private CPStepCounter stepCounter;
    private Object syncLock = new Object();

    private AtomicBoolean recording = new AtomicBoolean(false);
    private StringBuilder recordingBuilder = null;
    private long startMs = 0;
    private AtomicBoolean playback = new AtomicBoolean(false);
    private PlaybackTask playbackTask = null;

    private long kalmanLastUpdated = 0;

    private Handler handler = null;
    private Runnable indoorOutZoneCallback = new Runnable() {
        @Override
        public void run() {
            long currentMs = System.currentTimeMillis();
            for (PMPLocationManagerNotifier delegete : delegates) {
                if (recording.get()) {
                    recordingBuilder.append(String.format("%d,%d\n", 2, currentMs - startMs));
                }
                delegete.didIndoorExitRegion();
            }
            startMs = currentMs;
            lastLocation = null;
            handler.removeCallbacks(indoorOutZoneCallback);
        }
    };

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)
                        == BluetoothAdapter.STATE_ON) {
                    ble_start();
                } else {
                    ble_stop();
                }
            }
        }
    };

    private PMPLocationManager (Context context){
        this.context = context.getApplicationContext();
        handler = new Handler();
        loadDefaultSetting();
    }

    public static PMPLocationManager getShared(Context context) {
        if(shared == null) {
            synchronized (PMPLocationManager.class) {
                if(shared == null) {
                    shared = new PMPLocationManager(context);
                }
            }
        }
        return shared;
    }

    public static PMPLocationManager getShared() {
        return shared;
    }

    void loadDefaultSetting() {
        _userDefaults = context.getSharedPreferences("PREF_PMP_LOCATION_SETTINGS", 0);
        setEnableMaxNumOfBeaconsDetection(_userDefaults.getBoolean(context.getString(R.string.Preference_EnableMaxNumOfBeaconsDetection), false));
        setMaxNumOfBeaconsDetection(_userDefaults.getInt(context.getString(R.string.Preference_MaxNumOfBeaconsDetection), 3));

        setEnableMovingAverage(_userDefaults.getBoolean(context.getString(R.string.Preference_EnableMovingAverage), false));

//        setEnableWalkingDetection(_userDefaults.getBoolean(context.getString(R.string.Preference_EnableWalkingDetection), false));
        enableWalkingDetection = _userDefaults.getBoolean(context.getString(R.string.Preference_EnableWalkingDetection), false);
        setNumOfStationaryToResume(_userDefaults.getInt(context.getString(R.string.Preference_NumOfStationaryToResume), 5));
        setEnableLowPassFilter(_userDefaults.getBoolean(context.getString(R.string.Preference_EnableLowPassFilter), false));
        setLowPassFilterFactor(_userDefaults.getFloat(context.getString(R.string.Preference_LowPassFilterFactor), LOW_PASS_FILTER_FACTOR));

        setNumOfMovingAverageSample(_userDefaults.getInt(context.getString(R.string.Preference_NumOfMovingAverageSample), 3));

        setEnableBeaconAccuracyFilter(_userDefaults.getBoolean(context.getString(R.string.Preference_EnableBeaconAccuracyFilter), false));
        setBeaconAccuracyFilter(_userDefaults.getInt(context.getString(R.string.Preference_BeaconAccuracyFilter), 10));

        setIndoorLocationTimeOut(_userDefaults.getInt(context.getString(R.string.Preference_IndoorLocationOutZoneTimer), 20));

        setEnableFloorSwitchingFilters(_userDefaults.getBoolean(context.getString(R.string.Preference_EnableFloorSwitchingFilters), false));
        Set<String> floorSwitchStringSet = _userDefaults.getStringSet(context.getString(R.string.Preference_FloorSwitchingFilters), null);
        if (floorSwitchStringSet != null && !floorSwitchStringSet.isEmpty()) {
            floorSwitchingFilters = new ArrayList<>();
            for (String str : floorSwitchStringSet) {
                floorSwitchingFilters.add(Integer.valueOf(str));
            }
        }
        setNumOfFloorBeaconDetection(_userDefaults.getInt(context.getString(R.string.Preference_NumOfFloorBeaconDetection), 3));
        setEnableLog(_userDefaults.getBoolean(context.getString(R.string.Preference_EnableLog), false));

        currentBeaconType = PMPBeaconType.Unknown;
        entryReceivedTime = 0;
        managerStarted = false;
        numOfRepeatedFloor = 0;
        delegates = new ArrayList<>();
        beaconRegions = new ArrayList<PMPBeacon>();
        allbeacons = Collections.synchronizedMap(new HashMap<String, PMPBeacon>());

        stepCounter = CPStepCounter.getInstance(context);
    }

    public List<PMPBeacon> proximityUUIDS;

    public boolean enableMaxNumOfBeaconsDetection;
    public int maxNumOfBeaconsDetection;

    public boolean enableMovingAverage;
    public int numOfMovingAverageSample;

    /* force to enable now. Note you cannot stop it */
    public int numOfFloorBeaconDetection;

    public boolean enableLowPassFilter;
    public float lowPassFilterFactor; // Default value is 0.3

    public boolean isEnableLog() {
        return enableLog;
    }

    public void setEnableLog(boolean enableLog) {
        this.enableLog = enableLog;
        LogManager.setVerboseLoggingEnabled(enableLog);
    }

    private boolean enableLog;

    public String version;

    private PMPBeaconType currentBeaconType;
    private long entryReceivedTime;
    private boolean managerStarted;
    private boolean bleStarted = false;
    private int numOfRepeatedFloor;
    private List<PMPLocationManagerNotifier> delegates;

    private BroadcastReceiver stepCounterReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            //Log.i(TAG, "Step Counter Update");
            synchronized (syncLock) {
                isWalking.set(stepCounter.isWalking());
                LogManager.d(TAG, "isWalking %d", isWalking);
                if (isWalking.get()) {
                    stationaryCount.set(0);
                    walkingDetectionFirstLocation = null;
                    walkingDetectionLastLocation = null;
                }
            }
        }
    };

    public void start() {
//        if (!this.requireLocationPermission()) {
//            return;
//        }
        if (isPlayback() || managerStarted || proximityUUIDS == null ||proximityUUIDS.size() == 0) {
            return;
        }
        Log.e(TAG, "start");
        managerStarted = true;
        context.registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
        /* Start iBeacon monitoring */
        ble_start();
    }

    public void stop() {
        if (managerStarted) {
            Log.e(TAG, "stop");
            managerStarted = false;
            context.unregisterReceiver(mReceiver);
            ble_stop();
        }
    }

    public Boolean isRecording() {
        return recording.get();
    }

    public void startRecording() {
        if (!recording.get()) {
            recording.set(true);
            recordingBuilder = new StringBuilder();
            startMs = System.currentTimeMillis();
        }
    }

    public String stopRecording() {
        if (recording.get()) {
            recording.set(false);
            if (recordingBuilder != null) {
                return recordingBuilder.toString();
            }
        }
        return "";
    }

    public Boolean isPlayback() {
        return playback.get();
    }

    public void startPlayBack(String records) {
        if (!playback.get()) {
            playback.set(true);
            stop();
            String[] lines = records.split("\n");
            playbackTask = new PlaybackTask(lines);
            playbackTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    public void stopPlayBack() {
        if (playback.get()) {
            playbackTask.cancel(true);
            playbackTask = null;
            start();
            playback.set(false);
        }
    }

    static class PlaybackTask extends AsyncTask {
        String[] records;
        public  PlaybackTask(String[] records) {
            super();
            this.records = records;
        }

        @Override
        protected Object doInBackground(Object[] params) {
            while (!isCancelled()) {
                try {
                    for (String record :
                            records) {
                        Log.e(TAG, "playback record "+record);
                        String[] cmd = record.split(",");
                        switch (Integer.valueOf(cmd[0])) {
                            case 0:
                                Thread.sleep(Long.valueOf(cmd[1]));
                                for (PMPLocationManagerNotifier delegete : getShared().delegates) {
                                    delegete.didIndoorLocationUpdated(PMPBeaconType.values()[Integer.valueOf(cmd[5])], new PMPLocation(Float.valueOf(cmd[2]), Float.valueOf(cmd[3]), 0, cmd[4]));
                                    delegete.didIndoorTransmissionsUpdated(Arrays.asList(new PMPBeacon()));
                                }
                                break;
                            case 1:
                                break;
                            case 2:
                                Thread.sleep(Long.valueOf(cmd[1]));
                                for (PMPLocationManagerNotifier delegete : getShared().delegates) {
                                    delegete.didIndoorExitRegion();
                                }
                                break;
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }

    private void ble_start() {
        if (!bleStarted) {
            bleStarted = true;
            if (enableWalkingDetection) {
                isRegisterStepCounter = true;
                LocalBroadcastManager.getInstance(context).registerReceiver(stepCounterReceiver, new IntentFilter(CPStepCounter.ACTION_STEP_COUNT_UPDATE));
                LocalBroadcastManager.getInstance(context).registerReceiver(stepCounterReceiver, new IntentFilter(CPStepCounter.ACTION_IS_WALKING_UPDATE));
                stepCounter.start();
            }
            _locationManager = new PMPBeaconManager(context);
            _locationManager.setNotifier(this);
            _proximityManager = new PMPBeaconManager(context);
            _proximityManager.setNotifier(this);
            List<String> pushBeacon = new ArrayList<>();
            List<String> positionBeacon = new ArrayList<>();
            this.beaconRegions.clear();
            for (PMPBeacon beacon : this.proximityUUIDS) {
                final PMPBeacon currentBeacon = beacon;
                PMPBeacon found = CollectionUtils.find(this.beaconRegions, new Predicate<PMPBeacon>() {
                    public boolean evaluate(PMPBeacon p) {
                        return p.getId1().equals(currentBeacon.getId1());
                    }
                });
                if (found == null) {
                    LogManager.d(TAG, "beacon.uuid " + beacon.getId1());
                    if (beacon.getBeaconType() == PMPBeaconType.Push.getValue()) {
                        pushBeacon.add(beacon.getId1().toString());
                    } else {
                        positionBeacon.add(beacon.getId1().toString());
                    }
                    beaconRegions.add(beacon);
                }
            }
            _locationManager.start(new ArrayList<String>(), positionBeacon, this);
            _proximityManager.start(pushBeacon, new ArrayList<String>(), this);
        }
    }

    private void ble_stop() {
        synchronized (syncLock) {
            if (bleStarted) {
                bleStarted = false;
                walkingDetectionFirstLocation = null;
                walkingDetectionLastLocation = null;
                stationaryCount.set(0);
                if (isRegisterStepCounter) {
                    isRegisterStepCounter = false;
                    LocalBroadcastManager.getInstance(context).unregisterReceiver(stepCounterReceiver);
                    stepCounter.stop();
                }
                _locationManager.stop();
                _proximityManager.stop();
            }
        }
    }

    public void setPowerSave(Boolean enable) {

    }

    public void addDelegate(final PMPLocationManagerNotifier notifier)
    {
        if (notifier != null) {
            PMPLocationManagerNotifier exist = CollectionUtils.find(delegates, new Predicate<PMPLocationManagerNotifier>() {
                @Override
                public boolean evaluate(PMPLocationManagerNotifier object) {
                    return notifier == object;
                }
            });
            if (exist == null) {
                delegates.add(notifier);
            }
        }
    }

    public void removeDelegate(final PMPLocationManagerNotifier notifier)
    {
        if (notifier != null) {
            PMPLocationManagerNotifier exist = CollectionUtils.find(delegates, new Predicate<PMPLocationManagerNotifier>() {
                @Override
                public boolean evaluate(PMPLocationManagerNotifier object) {
                    return notifier == object;
                }
            });
            if (exist != null) {
                delegates.remove(exist);
            }
        }
    }

    @Override
    public void didRangeBeacons(List<Beacon> beacons) {
        if (!enableMovingAverage) {
            allbeacons.clear();
        }
        Log.e(TAG, "didRangeBeacons");
//    KWLog(@"%@", beacons);
        ArrayList<PMPBeacon> unsortValidBeacons = new ArrayList<PMPBeacon>();
        for (Beacon beacon : beacons) {
            if (beacon.getRssi() < 0) {
                PMPBeacon pmpBeacon = new PMPBeacon(beacon);
                allbeacons.put(pmpBeacon.getId2().toString(), pmpBeacon);
                unsortValidBeacons.add(pmpBeacon);
            }
        }


        /**************************/
        /* Beacon Type Filter     */
        /**************************/
        Map<String, ArrayList<PMPBeacon>> floorBeacons = applyBeaconTypeFilter(unsortValidBeacons);

        /*************************/
        /* Outdoor filter        */
        /*************************/
        if (applyOutdoorFilter(floorBeacons)) {
            // Outdoor path detected, return HERE
            return;
        }

        /**************************/
        /* Floor switching filter */
        /**************************/
        applyFloorSwitchingFilter(floorBeacons);
        Log.e(TAG, "didRangeBeacons end");
    }

    void performExecution(List<PMPBeacon> transmissions)
    {
        if (!transmissions.isEmpty()) {
            determine(transmissions);
        }
    }

    void determine(List<PMPBeacon> transmissions)
    {
        int count = transmissions.                  size();
        Location[] locations = new Location[count];
        int index = 0;
        for (PMPBeacon beacon : transmissions) {
            locations[index] = new Location(beacon.getLocation().getX(), beacon.getLocation().getY(), 0, beacon.getScaleDistance());
            index++;
        }
        Location result = NonLinear.determine(locations);
        if (result != null) {
            if(enableKalmanFilter) {
                if (SystemClock.uptimeMillis() - kalmanLastUpdated > 5000) {
                    KF.init(result);
                }
                kalmanLastUpdated = SystemClock.uptimeMillis();
                result = KF.tracking(result);
            }
            onLocationReport(new PMPLocation(result.getX(), result.getY(), 0, transmissions.get(0).getLocation().getName()));
        }
    }

    void onDidTransmissionsUpdated(List<PMPBeacon> transmissions) {
        if (!enableLog) {
            return;
        }
        for (PMPLocationManagerNotifier delegete : delegates) {
            delegete.didIndoorTransmissionsUpdated(transmissions);
        }
    }

    void onDidLocationUpdated(PMPLocation location) {
        onDidLocationUpdated(location, false);
    }

    void onDidLocationUpdated(PMPLocation location, boolean skipFilter) {
        onDidLocationUpdated(location, skipFilter, PMPBeaconType.Positioning);
    }

    void onDidLocationUpdated(PMPLocation location, boolean skipFilter, PMPBeaconType beaconType) {
        if (location == null) {
            // Null check
            return;
        }
        if (enableLowPassFilter && !skipFilter) {
            if (this.lastLocation != null) {
                location = filteredLocation(location, this.lastLocation);
            }
        }
        this.lastLocation = location;
        Log.d(TAG, "location update non-moving : " + location);

        long currentMS = System.currentTimeMillis();
        for (PMPLocationManagerNotifier delegete : delegates) {
            if (recording.get()) {
                recordingBuilder.append(String.format("%d,%d,%f,%f,%s,%d\n", 0, currentMS - startMs, location.getX(), location.getY(), location.getName(), beaconType.getValue()));
            }
            delegete.didIndoorLocationUpdated(beaconType, location);
        }
        startMs = currentMS;
        //Reset timer by re-use this function
        onMapActivityResume();
    }

    public void setProximityUUIDS(List<PMPBeacon> proximityUUIDS) {
        this.proximityUUIDS = proximityUUIDS;
    }

    PMPLocation filteredLocation(PMPLocation newLocation, PMPLocation previousLocation) {
        double x = previousLocation.getX() * (1 - lowPassFilterFactor) + newLocation.getX() * lowPassFilterFactor;
        double y = previousLocation.getY() * (1 - lowPassFilterFactor) + newLocation.getY() * lowPassFilterFactor;
        return new PMPLocation(x, y, 0, newLocation.getName());
    }

    @Override
    public void didProximityEnterRegion(int region, int minor, double rssi, double distance) {
        for (PMPLocationManagerNotifier notifier : delegates) {
            notifier.didProximityEnterRegion(region, minor, rssi, distance);
        }
    }

    @Override
    public void didProximityExitRegion(int region) {
        for (PMPLocationManagerNotifier notifier : delegates) {
            notifier.didProximityExitRegion(region);
        }
    }

    @Override
    public void didError(PMPBeaconManagerError error) {
        for (PMPLocationManagerNotifier notifier : delegates) {
            notifier.didError(PMPLocationManagerError.fromInt(error.getValue()));
        }
    }

//    public boolean requireLocationPermission(Activity activity) {
//        if (ContextCompat.checkSelfPermission(activity,
//                Manifest.permission.ACCESS_COARSE_LOCATION)
//                != PackageManager.PERMISSION_GRANTED) {
//
//            // Should we show an explanation?
//            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
//                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
//
//                // Show an expanation to the user *asynchronously* -- don't block
//                // this thread waiting for the user's response! After the user
//                // sees the explanation, try again to request the permission.
//
//            } else {
//
//                // No explanation needed, we can request the permission.
//
//                ActivityCompat.requestPermissions(activity,
//                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
//                        PMP_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);
//
//                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
//                // app-defined int constant. The callback method gets the
//                // result of the request.
//            }
//        } else {
//            return true;
//        }
//        return false;
//    }

    public boolean isBlePowerSaveMode() {
        return blePowerSaveMode;
    }

    public void setBlePowerSaveMode(boolean blePowerSaveMode) {
        this.blePowerSaveMode = blePowerSaveMode;
        if (_locationManager != null) {
            _locationManager.setBlePowerSaveMode(blePowerSaveMode);
        }
        if (_proximityManager != null) {
            _proximityManager.setBlePowerSaveMode(blePowerSaveMode);
        }
    }

    public void setEnableLowPassFilter(boolean enableLowPassFilter) {
        this.enableLowPassFilter = enableLowPassFilter;
        _userDefaults.edit().putBoolean(context.getString(R.string.Preference_EnableLowPassFilter), enableLowPassFilter).commit();
    }

    public void setEnableMaxNumOfBeaconsDetection(boolean enableMaxNumOfBeaconsDetection) {
        this.enableMaxNumOfBeaconsDetection = enableMaxNumOfBeaconsDetection;
        _userDefaults.edit().putBoolean(context.getString(R.string.Preference_EnableMaxNumOfBeaconsDetection), enableMaxNumOfBeaconsDetection).commit();
    }

    public void setEnableMovingAverage(boolean enableMovingAverage) {
        this.enableMovingAverage = enableMovingAverage;
        _userDefaults.edit().putBoolean(context.getString(R.string.Preference_EnableMovingAverage), enableMovingAverage).commit();

    }

    public void setEnableBeaconAccuracyFilter(boolean enableBeaconAccuracyFilter) {
        this.enableBeaconAccuracyFilter = enableBeaconAccuracyFilter;
        _userDefaults.edit().putBoolean(context.getString(R.string.Preference_EnableBeaconAccuracyFilter), enableBeaconAccuracyFilter).commit();
    }

    public void setMaxNumOfBeaconsDetection(int maxNumOfBeaconsDetection) {
        this.maxNumOfBeaconsDetection = maxNumOfBeaconsDetection;
        _userDefaults.edit().putInt(context.getString(R.string.Preference_MaxNumOfBeaconsDetection), maxNumOfBeaconsDetection).commit();

    }

    public void setNumOfMovingAverageSample(int numOfMovingAverageSample) {
        this.numOfMovingAverageSample = numOfMovingAverageSample;
        _userDefaults.edit().putInt(context.getString(R.string.Preference_NumOfMovingAverageSample), numOfMovingAverageSample).commit();

    }

    public void setNumOfFloorBeaconDetection(int numOfFloorBeaconDetection) {
        this.numOfFloorBeaconDetection = numOfFloorBeaconDetection;
        _userDefaults.edit().putInt(context.getString(R.string.Preference_NumOfFloorBeaconDetection), numOfFloorBeaconDetection).commit();

    }

    public void setLowPassFilterFactor(float lowPassFilterFactor) {
        this.lowPassFilterFactor = lowPassFilterFactor;
        _userDefaults.edit().putFloat(context.getString(R.string.Preference_LowPassFilterFactor), lowPassFilterFactor).commit();
    }

    public void setEnableFloorSwitchingFilters(boolean enableFloorSwitchingFilters) {
        this.enableFloorSwitchingFilters = enableFloorSwitchingFilters;
        _userDefaults.edit().putBoolean(context.getString(R.string.Preference_EnableFloorSwitchingFilters), enableFloorSwitchingFilters).commit();
    }

    public void setEnableKalmanFilter(boolean enableKalmanFilter){
        this.enableKalmanFilter = enableKalmanFilter;
        _userDefaults.edit().putBoolean(context.getString(R.string.Preference_EnableKalmanFilter), enableKalmanFilter).commit();
    }

    public void setFloorSwitchingFilters(ArrayList<Integer> floorSwitchingFilters) {
        this.floorSwitchingFilters = floorSwitchingFilters;
        saveFloorSwitchingFilters();
    }

    private void saveFloorSwitchingFilters() {
        if (floorSwitchingFilters != null) {
            ArrayList<String> strList = new ArrayList<>();
            for (Integer value : floorSwitchingFilters) {
                strList.add(value.toString());
            }
            Set<String> alphaSet = new HashSet<String>(strList);
            _userDefaults.edit().putStringSet(context.getString(R.string.Preference_FloorSwitchingFilters), alphaSet);
        }
    }

    public void addFloorSwitchingFilter(final Integer filterId) {
        if (filterId != null) {
            Integer exits = CollectionUtils.find(this.floorSwitchingFilters, new Predicate<Integer>() {
                @Override
                public boolean evaluate(Integer object) {
                    return object.intValue() == filterId.intValue();
                }
            });
            if (exits == null) {
                this.floorSwitchingFilters.add(filterId);
                saveFloorSwitchingFilters();
            }
        }
    }

    public void removeFloorSwitchingFilter(final Integer filterId) {
        if (filterId != null) {
            Integer exits = CollectionUtils.find(this.floorSwitchingFilters, new Predicate<Integer>() {
                @Override
                public boolean evaluate(Integer object) {
                    return object.intValue() == filterId.intValue();
                }
            });
            if (exits != null) {
                this.floorSwitchingFilters.remove(exits);
                saveFloorSwitchingFilters();
            }
        }
    }

    public void removeAllFloorSwitchingFilter() {
        if (!this.floorSwitchingFilters.isEmpty()) {
            this.floorSwitchingFilters.clear();
            saveFloorSwitchingFilters();
        }
    }

    public void setIndoorLocationTimeOut(int indoorLocationTimeOut) {
        this.indoorLocationTimeOut = indoorLocationTimeOut;
        _userDefaults.edit().putInt(context.getString(R.string.Preference_IndoorLocationOutZoneTimer), indoorLocationTimeOut).commit();
    }

    public void setBeaconAccuracyFilter(int beaconAccuracyFilter) {
        this.beaconAccuracyFilter = beaconAccuracyFilter;
        _userDefaults.edit().putInt(context.getString(R.string.Preference_BeaconAccuracyFilter), beaconAccuracyFilter).commit();
    }

    public boolean isEnableLowPassFilter() {
        return enableLowPassFilter;
    }

    public boolean isEnableMaxNumOfBeaconsDetection() {
        return enableMaxNumOfBeaconsDetection;
    }

    public boolean isEnableMovingAverage() {
        return enableMovingAverage;
    }

    public boolean isEnableBeaconAccuracyFilter() {return enableBeaconAccuracyFilter; }

    public int getNumOfMovingAverageSample() {
        return numOfMovingAverageSample;
    }

    public int getNumOfFloorBeaconDetection() {
        return numOfFloorBeaconDetection;
    }

    public double getLowPassFilterFactor() {
        return lowPassFilterFactor;
    }

    public boolean isEnableFloorSwitchingFilters() {
        return enableFloorSwitchingFilters;
    }

    public List<Integer> getFloorSwitchingFilters() {
        return floorSwitchingFilters;
    }

    public int getBeaconAccuracyFilter() {return beaconAccuracyFilter;}

    public boolean isEnableWalkingDetection() {
        return enableWalkingDetection;
    }

    public void setEnableWalkingDetection(boolean enableWalkingDetection) {
        this.enableWalkingDetection = enableWalkingDetection;
        _userDefaults.edit().putBoolean(context.getString(R.string.Preference_EnableWalkingDetection), enableWalkingDetection).commit();
    }

    public int getNumOfStationaryToResume() { return numOfStationaryToResume; }

    public void setNumOfStationaryToResume(int numOfStationaryToResume) {
        this.numOfStationaryToResume = numOfStationaryToResume;
        _userDefaults.edit().putInt(context.getString(R.string.Preference_NumOfStationaryToResume), numOfStationaryToResume).commit();
    }

    public void onMapActivityResume() {
        if(lastLocation != null){
            handler.removeCallbacks(indoorOutZoneCallback);
            handler.postDelayed(indoorOutZoneCallback, indoorLocationTimeOut * 1000);
        }
    }

    public void onMapActivityPause() {
        handler.removeCallbacks(indoorOutZoneCallback);
    }

    private Map<String, ArrayList<PMPBeacon>> applyBeaconTypeFilter(ArrayList<PMPBeacon> currentValidBeaconsDetected) {
        // Reset entry path
        Map<String, ArrayList<PMPBeacon>> floorBeacons =  new HashMap<>();
        numberOfValidEntryIndoorDetected = 0;
        numberOfValidPositioningBeaconDetected = 0;
        entryPathBeaconDetected = null;
        ArrayList<PMPBeacon> transmissions = new ArrayList<PMPBeacon>();
        for (PMPBeacon value : allbeacons.values()) {
            PMPBeacon beacon = new PMPBeacon(value);

            PMPBeacon transmission = PMPBeacon.transmissionWithCLBeacon(beacon);
            if (transmission != null) {
                transmissions.add(transmission);
            }
        }
        floorBeacons.put(ALL_BEACONS, transmissions);
        boolean showResult = false;
        for (PMPBeacon transmission : transmissions) {
            if ((transmission.getBeaconType() == PMPBeaconType.LiftInside.getValue() ||
                    transmission.getBeaconType() == PMPBeaconType.ShuttleBusInside.getValue())
                    && transmission.getDistance() < transmission.getThreshold()
                    && currentValidBeaconsDetected.size() == 1
                    && transmission.getId1().equals(currentValidBeaconsDetected.get(0).getId1())) {
                transmission.setUsingForTrilateration(true);
                onDidLocationUpdated(transmission.getLocation(), true, PMPBeaconType.values()[transmission.getBeaconType()]);
                LogManager.d(TAG, "Lift beacon detected");
                showResult = true;
            } else if (transmission.getBeaconType() == PMPBeaconType.Entry.getValue() && transmission.getOutdoorPath().size() >= MIN_OUTDOOR_PATH_POINT/* && validBeacons.count <= 4*/) {
                entryPathBeaconDetected = transmission;
                ArrayList entryBeacons = floorBeacons.get(ENTRY_OUTDOOR);
                if (entryBeacons != null) {
                    entryBeacons.add(transmission);
                } else {
                    floorBeacons.put(ENTRY_OUTDOOR, new ArrayList(Arrays.asList(transmission)));
                }
            } else if (transmission.getBeaconType() == PMPBeaconType.EntryIndoor.getValue() && transmission.getDistance() < transmission.getThreshold()) {
                ArrayList entryIndoorBeacons = floorBeacons.get(ENTRY_INDOOR);
                if (entryIndoorBeacons != null) {
                    entryIndoorBeacons.add(transmission);
                } else {
                    floorBeacons.put(ENTRY_INDOOR, new ArrayList(Arrays.asList(transmission)));
                }
                numberOfValidEntryIndoorDetected++;
            } else if(transmission.getBeaconType() == PMPBeaconType.POI.getValue()) {
                if (transmission.getThreshold() != 0 && transmission.getDistance() > transmission.getThreshold()) {
                    continue;
                }
                ArrayList<PMPBeacon> filterArray = new ArrayList<PMPBeacon>(transmissions);
                CollectionUtils.filter(filterArray, new Predicate<PMPBeacon>() {
                    @Override
                    public boolean evaluate(PMPBeacon beacon) {
                        return beacon.getBeaconType() == PMPBeaconType.POI.getValue();
                    }
                });
                Collections.sort(filterArray, new Comparator<Beacon>() {
                    @Override
                    public int compare(Beacon lhs, Beacon rhs) {
                        if (lhs.getRssi() > rhs.getRssi()) {
                            return -1;
                        } else if (lhs.getRssi() == rhs.getRssi()) {
                            return 0;
                        } else {
                            return 1;
                        }
                    }
                });
                final PMPBeacon beacon = filterArray.get(0);
                CollectionUtils.forAllDo(transmissions, new Closure<PMPBeacon>() {
                    @Override
                    public void execute(PMPBeacon input) {
                        if(input.getMajorIntValue() == beacon.getMajorIntValue()) {
                            input.setUsingForTrilateration(true);
                        }
                    }
                });
                LogManager.d(TAG, "poi : rssi %f", beacon.getDistance());
                beacon.setUsingForTrilateration(true);
                onDidLocationUpdated(beacon.getLocation());
                LogManager.d(TAG, "POI beacon detected");
                showResult = true;
            } else if (transmission.getBeaconType() != PMPBeaconType.LiftInside.getValue() &&
                    transmission.getBeaconType() != PMPBeaconType.ShuttleBusInside.getValue() &&
                    transmission.getBeaconType() != PMPBeaconType.Entry.getValue() &&
                    transmission.getBeaconType() != PMPBeaconType.EntryIndoor.getValue() &&
                    transmission.getBeaconType() != PMPBeaconType.Push.getValue()) {
                if (transmission.getThreshold() != 0 && transmission.getDistance() > transmission.getThreshold()) {
                    continue;
                }
                ArrayList positioningBeacons = floorBeacons.get(transmission.getLocation().getName());
                if (positioningBeacons != null) {
                    positioningBeacons.add(transmission);
                } else {
                    floorBeacons.put(transmission.getLocation().getName(), new ArrayList(Arrays.asList(transmission)));
                }
                numberOfValidPositioningBeaconDetected++;
            }
        }

        if (showResult) {
            onDidTransmissionsUpdated(transmissions);
            return null;
        }else {

        }

        return floorBeacons;
    }

    private boolean applyOutdoorFilter(Map<String, ArrayList<PMPBeacon>> floorBeacons) {
        if (entryPathBeaconDetected != null
                && (numberOfValidPositioningBeaconDetected < MIN_NUM_OF_BEACON_TO_INDOOR
                && numberOfValidEntryIndoorDetected < MIN_NUM_OF_BEACON_TO_INDOOR) && floorBeacons != null && !floorBeacons.isEmpty()) {
            PMPPoint3D entry_path_start = PMPPoint3D.ZERO, entry_path_end = PMPPoint3D.ZERO;
            double estimateDistance = entryPathBeaconDetected.getDistance() * entryPathBeaconDetected.getOutdoorPathRatio();
            for (int index = 0; index < entryPathBeaconDetected.getOutdoorPath().size() - 1; index++) {
                entry_path_start = new PMPPoint3D(entryPathBeaconDetected.getOutdoorPath().get(index).x, entryPathBeaconDetected.getOutdoorPath().get(index).y, 0);
                entry_path_end = new PMPPoint3D(entryPathBeaconDetected.getOutdoorPath().get(index+1).x, entryPathBeaconDetected.getOutdoorPath().get(index+1).y, 0);
                double currentPathDistance = entry_path_start.distanceFrom(entry_path_end) / entryPathBeaconDetected.getMapScaleRatio();
                LogManager.d(TAG, "currentPathDistance " + currentPathDistance);
                if (estimateDistance <= currentPathDistance) {
                    break;
                } else {
                    estimateDistance-=currentPathDistance;
                }
                if (index + 1 < entryPathBeaconDetected.getOutdoorPath().size()) {
                    estimateDistance = currentPathDistance;
                }
            }

            PMPPoint3D result = PMPPoint3D.calculatePointOnPath(entry_path_start, entry_path_end, estimateDistance * entryPathBeaconDetected.getMapScaleRatio());
//            Log.d(TAG, "<<%@ acc %f", self.entryPathBeaconDetected.major, self.entryPathBeaconDetected.accuracy*OUTDOOR_MULTI);
//        result = CGPoint3DMake(4183.36, 2191.65, 0);
            PMPLocation location =  new PMPLocation(result.getX(), result.getY(), 0, entryPathBeaconDetected.getLocation().getName());
            entryPathBeaconDetected.setUsingForTrilateration(true);
            onDidTransmissionsUpdated(floorBeacons.get(ALL_BEACONS));
            onLocationReport(location);

//            entryReceivedTime = [[NSDate date] timeIntervalSince1970] * 1000;
            return true;
        }
        return false;
    }

    private void applyFloorSwitchingFilter(Map<String, ArrayList<PMPBeacon>> floorBeacons) {
//    KWLog(@"FloorSwitching : %@", floorBeacons);
        if (floorBeacons == null || floorBeacons.isEmpty()) {
            return;
        }
        List<Map.Entry<String, Double>> sortedFloorKeyArray = null;
        ArrayList<PMPBeacon> sortedBeacons = new ArrayList<PMPBeacon>();
        if (allbeacons.values().size() > 0) {
            for (ArrayList<PMPBeacon> beacons : floorBeacons.values()) {
                sortedBeacons.addAll(beacons);
            }
            Collections.sort(sortedBeacons, new Comparator<Beacon>() {
                @Override
                public int compare(Beacon lhs, Beacon rhs) {
                    if (lhs.getRssi() > rhs.getRssi()) {
                        return -1;
                    } else if (lhs.getRssi() == rhs.getRssi()) {
                        return 0;
                    } else {
                        return 1;
                    }
                }
            });
        }

    /* Force floor detection */
        final HashMap<String, Double> floorWeighting = new HashMap<String, Double>();
        for (String key : floorBeacons.keySet()) {
            if (key.matches(regexStr)) {
                floorWeighting.put(key, 0.0);
            }
        }
        if (sortedBeacons.size() > 1) {
            double initialRatio = 1;
            double multiplier = 0.9;
            int numOfBeaconsToDetected = 10;

            for (final PMPBeacon beacon : sortedBeacons) {
                if (enableFloorSwitchingFilters && floorSwitchingFilters.size() > 0) {
                    Integer found = CollectionUtils.find(floorSwitchingFilters, new Predicate<Integer>() {
                        @Override
                        public boolean evaluate(Integer major) {
                            return major.intValue() == beacon.getMajorIntValue();
                        }
                    });
                    if (found != null) {
                        continue;
                    }
                }
                if (floorWeighting.containsKey(beacon.getLocation().getName())) {
                    double weighting = floorWeighting.get(beacon.getLocation().getName()) + initialRatio;
                    floorWeighting.put(beacon.getLocation().getName(), weighting);
                    initialRatio *= multiplier;
                    numOfBeaconsToDetected--;
                    if (numOfBeaconsToDetected <= 0) {
                        break;
                    }
                }
            }

            Set<Map.Entry<String, Double>> set = floorWeighting.entrySet();
            sortedFloorKeyArray = new ArrayList<Map.Entry<String, Double>>(
                    set);
            Collections.sort(sortedFloorKeyArray, new Comparator<Map.Entry<String, Double>>() {
                public int compare(Map.Entry<String, Double> o1,
                                   Map.Entry<String, Double> o2) {
                    return o2.getValue().compareTo(o1.getValue());
                }
            });
//            LogManager.e(TAG, "sortedFloorArray " + sortedFloorKeyArray);
//        KWLog(@"sortedFloorKeyArray %@", sortedFloorKeyArray);
//        [self onDidTransmissionsUpdated:sortedBeacons];
            if (enableLog && false) {
                Intent intent = new Intent("floorWeighting");
                intent.putExtra("floorWeighting", floorWeighting);
                context.sendBroadcast(intent);
            }
        }

        ArrayList<PMPBeacon> transmissions = new ArrayList<>();
        if (sortedFloorKeyArray != null && sortedFloorKeyArray.size() > 0) {
            if (lastFloorDetected != null) {
                Double value = floorWeighting.get(lastFloorDetected);
                if (sortedFloorKeyArray.get(0).getKey().equals(lastFloorDetected) &&
                        value != null && value > 0) {
                    numOfRepeatedFloor++;
                } else {
                    numOfRepeatedFloor = 1;
                    lastFloorDetected = sortedFloorKeyArray.get(0).getKey();
                    if (currentBeaconType == PMPBeaconType.EntryIndoor) {
                        currentBeaconType = PMPBeaconType.Positioning;
                        numOfRepeatedFloor = numOfFloorBeaconDetection;
                    }
                }
            } else {
                numOfRepeatedFloor = numOfFloorBeaconDetection;
                lastFloorDetected = currentFloorDetected = sortedFloorKeyArray.get(0).getKey();
            }

            if (numOfRepeatedFloor >= numOfFloorBeaconDetection) {
                lastFloorDetected = currentFloorDetected = sortedFloorKeyArray.get(0).getKey();
            }
            ArrayList<PMPBeacon> sortedFloorBeacons = floorBeacons.get(currentFloorDetected);
            if(sortedFloorBeacons != null){
                Collections.sort(sortedFloorBeacons, new Comparator<Beacon>() {
                    @Override
                    public int compare(Beacon lhs, Beacon rhs) {
                        if (lhs.getRssi() > rhs.getRssi()) {
                            return -1;
                        } else if (lhs.getRssi() == rhs.getRssi()) {
                            return 0;
                        } else {
                            return 1;
                        }
                    }
                });

                int index = 0;
                if (enableMaxNumOfBeaconsDetection) {
                    for (PMPBeacon tran : sortedFloorBeacons) {
                        if (enableBeaconAccuracyFilter) {
                            if (tran.getDistance() < beaconAccuracyFilter) {
                                transmissions.add(tran);
                                index++;
                            }
                        } else {
                            transmissions.add(tran);
                            index++;
                        }
                        if (index >= maxNumOfBeaconsDetection) {
                            break;
                        }
                    }
                } else {
                    if (enableBeaconAccuracyFilter) {
                        CollectionUtils.filter(sortedFloorBeacons, new Predicate<PMPBeacon>() {
                            @Override
                            public boolean evaluate(PMPBeacon beacon) {
                                return beacon.getDistance() < beaconAccuracyFilter;
                            }
                        });
                    } else {
                        transmissions = sortedFloorBeacons;
                    }
                }
            }

            if (transmissions != null && transmissions.size() > 2) {
//            KWLog(@"transmissions => %@", transmissions);
//            KWLog(@"all transmissions => %@", [floorBeacons objectForKey:ALL_BEACONS]);
                for (PMPBeacon beacon : transmissions) {
                    beacon.setUsingForTrilateration(true);
                }
                performExecution(transmissions);
            }
            onDidTransmissionsUpdated(floorBeacons.get(ALL_BEACONS));
        }
    }

    private void onLocationReport(PMPLocation location) {
        if (!bleStarted)
            return;
        synchronized (syncLock) {
            if (!isWalking.get() && enableWalkingDetection) {
                if (walkingDetectionLastLocation != null) {
                    walkingDetectionLastLocation = filteredLocation(location, walkingDetectionLastLocation);
                } else if (walkingDetectionFirstLocation != null) {
                    walkingDetectionLastLocation = filteredLocation(location, walkingDetectionFirstLocation);
                } else {
//            NSLog(@"first and last location are nil %d", self.stationaryCount);
                    walkingDetectionFirstLocation = location;
                }
                PMPLocation tmplocation = walkingDetectionFirstLocation;
                stationaryCount.incrementAndGet();
                if (stationaryCount.get() > numOfStationaryToResume) {
                    tmplocation = walkingDetectionLastLocation;
                    walkingDetectionFirstLocation = tmplocation;
                    stationaryCount.set(0);
                }
                onDidLocationUpdated(tmplocation, true);
            } else {
                onDidLocationUpdated(location);
            }
        }
    }
    //    @Override
//    public void onRequestPermissionsResult(int requestCode,
//                                           String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case PMP_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//                    // permission was granted, yay! Do the
//                    // contacts-related task you need to do.
//                    PMPLocationManager.this.start();
//                } else {
//
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                    for (PMPLocationManagerNotifier delegete : delegates) {
//                        delegete.didError(PMPLocationManagerError.LocationPermissionDenied);
//                    }
//                }
//                return;
//            }
//
//            // other 'case' lines to check for other
//            // permissions this app might request
//        }
//    }
}
