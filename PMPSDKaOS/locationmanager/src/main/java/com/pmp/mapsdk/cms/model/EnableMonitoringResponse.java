package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;


public class EnableMonitoringResponse {
    @SerializedName("result")
    private String result;
    @SerializedName("errorMessage")
    private String errorMessage;
    @SerializedName("errorCode")
    private String errorCode;


	public EnableMonitoringResponse() {

	}

    public EnableMonitoringResponse(JSONObject json) {
    
//        this.result = new Result(json.optJSONObject("result"));
        this.errorMessage = json.optString("errorMessage");
        this.errorCode = json.optString("errorCode");

    }
    
    public String getResult() {
        return this.result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }


    
}
