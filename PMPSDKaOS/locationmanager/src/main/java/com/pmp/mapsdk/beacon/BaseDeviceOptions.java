package com.pmp.mapsdk.beacon;

import android.graphics.PointF;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public abstract class BaseDeviceOptions implements Parcelable {



    public abstract String getOutdoorPathRatio();

    public abstract void setOutdoorPathRatio(String outdoorPathRatio);

    public abstract ArrayList<PointF> getNearbyOutdoorNodes();

    public abstract void setNearbyOutdoorNodes(ArrayList<PointF> nearbyOutdoorNodes);

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }


    public abstract ArrayList<? extends BaseNodes> getNodes();

    public abstract void setNodes(ArrayList<? extends BaseNodes> nodes);
}
