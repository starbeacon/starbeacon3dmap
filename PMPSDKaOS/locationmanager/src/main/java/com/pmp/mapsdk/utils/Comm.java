package com.pmp.mapsdk.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class Comm {
	public static boolean hasSystemFeature(Context context, String feature) {
		if (context != null)
			new NullPointerException("Context can't be null");
		PackageManager packageManager = context.getPackageManager();
		return packageManager.hasSystemFeature(feature);
	}

	public static boolean hasPermission(Context context, String permission) {
		if (context != null)
		new NullPointerException("Context can't be null");
		/* Fix Samsung permission crash */
		try {
			boolean granted = ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
			Log.e("", "granted " + granted);
			return granted;
		} catch (Exception e) {
			e.printStackTrace();
			Method checkSelfPermissionMethod = getDeclaredMethod(context,
					"checkSelfPermission", String.class);
			if (checkSelfPermissionMethod != null) {
				try {
					boolean granted = (Integer) checkSelfPermissionMethod.invoke(context,
							permission) == PackageManager.PERMISSION_GRANTED;
					Log.e("", "granted " + granted);
					return granted;
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}

		return true;
	}

	public static String arrayListToString(ArrayList arrayList){
		StringBuilder sb = new StringBuilder();
		for (Object s : arrayList)
		{
			sb.append(s);
			sb.append(",");
		}

		sb.deleteCharAt(sb.length() - 1);
		return sb.toString();
	}

	private static Method getDeclaredMethod(Object obj, String name,
											Class<?>... parameterTypes) {
		try {
			return obj.getClass().getMethod(name, parameterTypes);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		return null;
	}
}
