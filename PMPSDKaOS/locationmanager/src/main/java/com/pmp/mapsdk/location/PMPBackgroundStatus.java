package com.pmp.mapsdk.location;

/**
 * Created by Yu on 22/2/16.
 */
public enum PMPBackgroundStatus {
    /**
     * if KWAppCycleAdapter is not set in application activity, this value will
     * return
     */
    Undetermined,
    /**
     * Application enter background
     */
    Background,
    /**
     * Application enter foreground
     *
     */
    Foreground
}