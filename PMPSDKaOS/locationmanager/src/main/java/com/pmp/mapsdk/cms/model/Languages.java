package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;

import org.json.*;


public class Languages {

    @SerializedName("id")
    private double id;
    @SerializedName("code")
    private String code;
    
    
	public Languages () {
		
	}	
        
    public Languages (JSONObject json) {
    
        this.id = json.optDouble("id");
        this.code = json.optString("code");

    }
    
    public double getId() {
        return this.id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    
}
