package com.pmp.mapsdk.beacon;

/**
 * iBeacon Error Code
 * 
 * @author Yu
 *
 */
public enum PMPBeaconManagerError {
	/**
	 * Unknown error
	 */
	UNKNOWN(-1),
	/**
	 * Device not support BLE
	 */
	BLE_NOT_SUPPORTED(0),
	/**
	 * BLE not found on current area
	 */
	BLE_NOT_FOUND(1),
	/**
	 * BLE uuid must be filled
	 */
	BLE_UUID_IS_EMPTY(2),
	/**
	 * Device Bluetooth power off, please power on to try again
	 */
	BLUETOOTH_POWER_OFF(3),
	/**
	 * Device Bluetooth power on
	 */
	BLUETOOTH_POWER_ON(4),
	/**
	 * Ranging cannot be started due to error
	 */
	RANGING_NOT_START(5);
	private final int value;
	private PMPBeaconManagerError(int value) {
		this.value = value;
	}
	public int getValue() {
		return value;
	}
}
