package com.pmp.mapsdk.beacon;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.RemoteException;
import android.support.v4.content.LocalBroadcastManager;

import com.pmp.mapsdk.logging.LogManager;
import com.pmp.mapsdk.logging.Loggers;
import com.pmp.mapsdk.utils.MovingAverage;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.BleNotAvailableException;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.service.RangedBeacon;
import org.altbeacon.beacon.service.RunningAverageRssiFilter;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

@SuppressLint({ "DefaultLocale", "NewApi" })
public class PMPBeaconManager implements RangeNotifier {
	protected static final String TAG = PMPBeaconManager.class.getName();
	private static PMPBeaconManager detector;
	private BeaconManager beaconManager;
	private Context context;
	private boolean binding = false;
	private BeaconConsumer beaconConsumer;
	private PMPBeaconManagerNotifier notifier;
	private IntentFilter boardcastFilter;

	private Timer noBLEDetectedTimer;
	private long timeout = 5 * 1000; // follow Apple iBeacon timeout value 30
	// sec

	private boolean highestRssiFilterEnabled = true;

	private int movingAverageSize = 5;
	private int alterIDGuardTime = 5000;
	private long lastAlterIDTime = 0;
	private PMPBeacon lastDidShowBeacon = null;
	private boolean enableMinorSecurity = false;
	private boolean enableLog = false;
	private int nextPValue = -1;
	private String lastRecKey = "";
	private ConcurrentHashMap<String, MovingAverage> movingAvgDict = new ConcurrentHashMap<String, MovingAverage>();
	private ConcurrentHashMap<String, PMPBeacon> beaconEntryStates = new ConcurrentHashMap<String, PMPBeacon>();
	private List<String> pushBeacons = new ArrayList<>();
	private List<String> positioningBeacons = new ArrayList<>();
	private boolean blePowerSaveMode = false;

	private BroadcastReceiver receiver = new BroadcastReceiver() {

		public void onReceive(Context context, Intent intent) {
			if (PMPBeaconManager.this.notifier == null) {
				return;
			}
			if (BluetoothAdapter.ACTION_STATE_CHANGED
					.equals(intent.getAction())) {
				if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) == BluetoothAdapter.STATE_OFF) {
					// Bluetooth was disconnected
					PMPBeaconManager.this.notifier
							.didError(PMPBeaconManagerError.BLUETOOTH_POWER_OFF);
					PMPBeaconManager.this.unbindService();
				} else if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) == BluetoothAdapter.STATE_ON) {
					// Bluetooth was connected
					try {
						PMPBeaconManager.this.bindService();
						PMPBeaconManager.this.notifier
								.didError(PMPBeaconManagerError.BLUETOOTH_POWER_ON);
					} catch (BleNotAvailableException e) {

					}

				}
			}
		}
	};

	public static PMPBeaconManager getInstance(Context context) {
		if (detector == null) {
			detector = new PMPBeaconManager(context);
		}
		detector.context = context;
		return detector;
	}

	public PMPBeaconManager(Context context) {
		this.context = context;
		BeaconManager.setRssiFilterImplClass(RunningAverageRssiFilter.class);
		RangedBeacon.setSampleExpirationMilliseconds(3000);
//		BeaconManager.setUseTrackingCache(false);
//		RangedBeacon.setMaxTrackinAge(3000);
		this.beaconManager = BeaconManager.getInstanceForApplication(context);
//		this.beaconManager.setBackgroundMode(true);
		this.beaconManager.setDebug(false);
		this.beaconManager.setForegroundScanPeriod(1000);
		this.beaconManager.setForegroundBetweenScanPeriod(0);
//		this.beaconManager.setAndroidLScanningDisabled(true);
//		this.beaconManager.setForegroundBetweenScanPeriod(0);
		this.beaconManager
				.getBeaconParsers()
				.add(new BeaconParser()
						.setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"/* "m:0-3=4c000215,i:4-19,i:20-21,i:22-23,p:24-24" */));
		this.beaconConsumer = new InternalBeaconConsumer();
		boardcastFilter = new IntentFilter(
				BluetoothAdapter.ACTION_STATE_CHANGED);
	}

	public boolean start(List<String> pushBeacons, List<String> positioningBeacons, PMPBeaconManagerNotifier notifier) {
		try {
			this.notifier = notifier;
			this.pushBeacons = pushBeacons;
			this.positioningBeacons = positioningBeacons;
			if(context != null) {
				LocalBroadcastManager.getInstance(context).registerReceiver(receiver, boardcastFilter);
			}
			if (checkBluetoothPowerIsOn()) {
				bindService();
				return true;
			} else {
				showError(PMPBeaconManagerError.BLUETOOTH_POWER_OFF);
			}
		} catch (BleNotAvailableException e) {
			LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver);
			unbindService();
			showError(PMPBeaconManagerError.BLE_NOT_SUPPORTED);
		}
		return false;
	}

	public void stop() {
		if (this.binding) {
			this.notifier = null;
			LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver);
			unbindService();
		}
	}

	private void bindService() {
		if (!checkBLEAvailability()) {
			throw new BleNotAvailableException("");
		}
		if (!this.binding) {
			this.binding = true;
			this.beaconManager.bind(this.beaconConsumer);
			startTimer();
		}
	}

	private void unbindService() {
		if (this.binding) {
			this.binding = false;
			this.beaconManager.unbind(this.beaconConsumer);
			stopTimer();
		}
	}

	private void startTimer() {
		stopTimer();
		noBLEDetectedTimer = new Timer();
		noBLEDetectedTimer.schedule(new TimerTask() {

			@Override
			public void run() {
				if (PMPBeaconManager.this.notifier != null) {
					PMPBeaconManager.this.notifier
							.didError(PMPBeaconManagerError.BLE_NOT_FOUND);
				}

			}
		}, this.timeout);
	}

	private void stopTimer() {
		if (noBLEDetectedTimer != null) {
			noBLEDetectedTimer.cancel();
			noBLEDetectedTimer.purge();
			noBLEDetectedTimer = null;
		}
	}

	/**
	 * Check if Bluetooth LE is supported by this Android device, and if so,
	 * make sure it is enabled. Throws a BleNotAvailableException if Bluetooth
	 * LE is not supported. (Note: The Android emulator will do this)
	 *
	 * @return false if it is supported and not enabled
	 */
	private boolean checkBLEAvailability() {
		return context.getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_BLUETOOTH_LE);
	}

	private boolean checkBluetoothPowerIsOn() {
		BluetoothAdapter btAdapter = ((Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR1) ? ((BluetoothManager) context
				.getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter()
				: (BluetoothAdapter.getDefaultAdapter()));

		if (btAdapter == null
				|| !(btAdapter.getState() == BluetoothAdapter.STATE_ON || btAdapter
				.getState() == BluetoothAdapter.STATE_TURNING_ON)) {
			return false;
		}
		return true;
	}

	private void showError(PMPBeaconManagerError code) {
		if (this.notifier != null)
			this.notifier.didError(code);
	}

	private void broadcast(Beacon beacon) {
		if (this.notifier != null) {
			this.notifier.didProximityEnterRegion(beacon.getId2().toInt(),beacon.getId3().toInt(),beacon.getRssi(),beacon.getDistance());
		}
	}

	private void broadcastEnter(PMPBeacon beacon) {
		LogManager.d(TAG, "broadcastEnter");
		if (this.notifier != null) {
			this.notifier.didProximityEnterRegion(
					beacon.getMajor(), beacon.getMinor(), beacon.getRssi(), beacon.getDistance());
		}
	}

	private void broadcastExit(PMPBeacon beacon, boolean allExit) {
		LogManager.d(TAG, "broadcastExit");
		if (this.notifier != null) {
			this.notifier.didProximityExitRegion(beacon.getMajor());
		}
	}

//	void setBackgroundMode(boolean backgroundMode) {
//		beaconManager.setBackgroundMode(backgroundMode);
//	}
//
//	void setBackgroundScanPeriod(long p) {
//		beaconManager.setBackgroundScanPeriod(p);
//	}
//
//	void setBackgroundBetweenScanPeriod(long p) {
//		beaconManager.setBackgroundBetweenScanPeriod(p);
//	}
//
//	void setForegroundBetweenScanPeriod(long p) {
//		beaconManager.setForegroundBetweenScanPeriod(p);
//	}
//
//	void setForegroundScanPeriod(long p) {
//		beaconManager.setForegroundScanPeriod(p);
//	}

	public boolean isHighestRssiFilterEnabled() {
		return highestRssiFilterEnabled;
	}

	public long getTimeout() {
		return timeout;
	}

	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	public int getMovingAverageSize() {
		return movingAverageSize;
	}

	public void setMovingAverageSize(int movingAverageSize) {
		this.movingAverageSize = movingAverageSize;
	}

	public int getAlterIDGuardTime() {
		return alterIDGuardTime;
	}

	public void setAlterIDGuardTime(int alterIDGuardTime) {
		this.alterIDGuardTime = alterIDGuardTime;
	}

	public void setHighestRssiFilterEnabled(boolean highestRssiFilterEnabled) {
		this.highestRssiFilterEnabled = highestRssiFilterEnabled;
	}

	public boolean isEnableMinorSecurity() {
		return enableMinorSecurity;
	}

	public void setEnableMinorSecurity(boolean enableMinorSecurity) {
		this.enableMinorSecurity = enableMinorSecurity;
	}

	public PMPBeaconManagerNotifier getNotifier() {
		return notifier;
	}

	public void setNotifier(PMPBeaconManagerNotifier notifier) {
		this.notifier = notifier;
	}

	public boolean isEnableLog() {
		return enableLog;
	}

	public void setEnableLog(boolean enableLog) {
		this.enableLog = enableLog;
		LogManager.setLogger(Loggers.verboseLogger());
//		 if (this.beaconManager != null)
//		 this.beaconManager.setDebug(enableLog);
	}

	private PMPBeacon findNearestBeacon(Collection<Beacon> beacons) {
		// String nearestKey = (String)
		// this.movingAvgDict.keySet().toArray()[0];
		// MovingAverage nearest = this.movingAvgDict.get(nearestKey);
		for (Beacon beacon : beacons) {
//			String key = String.format("%s:%d", beacon.getId1(), beacon
//					.getId2().toInt());
//			this.movingAvg(key, beacon.getRssi(), beacon.getId3().toInt());
			String key = String.format("%s:%d", beacon.getId1().toString(), beacon
					.getId2().toInt());
			this.movingAvg(key, beacon.getRssi(), beacon.getId3().toInt(),beacon.getDistance());

		}

		if(movingAvgDict.size() == 0){
			return null;
		}

		String keyOfMaxValue = Collections.max(this.movingAvgDict.entrySet(),
				new Comparator<Map.Entry<String, MovingAverage>>() {
					@Override
					public int compare(Map.Entry<String, MovingAverage> o1,
									   Map.Entry<String, MovingAverage> o2) {
						return o1.getValue().rssiAvg() > o2.getValue()
								.rssiAvg() ? 1 : -1;
					}
				}).getKey();
		MovingAverage nearest = this.movingAvgDict.get(keyOfMaxValue);
		if (nearest == null) {
			return null;
		}
		LogManager.d(TAG, String.format("MAL : %s", keyOfMaxValue));

		String[] keyComponents = keyOfMaxValue.split(":");
		return new PMPBeacon(keyComponents[0],
				Integer.valueOf(keyComponents[1]), nearest.getValue(), nearest.rssiAvg(), nearest.distanceAvg(), 0, null);
	}

	private double movingAvg(String key, Integer rssi, Integer minor, double distance) {
		MovingAverage ma = this.movingAvgDict.get(key);
		if (ma == null) {
			ma = new MovingAverage(this.movingAverageSize);
			this.movingAvgDict.put(key, ma);
		}
		ma.setValue(minor);
		ma.addSample(rssi,distance);
		return ma.rssiAvg();
	}

	private PMPBeacon beaconByKey(String key) {
		return this.beaconEntryStates.get(key);
	}

	private boolean validMinor(int minor) {
		if (!this.enableMinorSecurity) {
			return true;
		}
		int header = 0xF & minor;
		int decode32Bit = minor & 0xFFF0;
		int decodeL16Bit = ((decode32Bit << header) & 0xFFFF0000) >> 16;
		int decodeR16Bit = ((decode32Bit << header) & 0xFFFF) >> 4;
		int decodeValue = decodeR16Bit | decodeL16Bit;
		LogManager.d(TAG, String.format("decodeValue 0x%X", decodeValue));
		int nextValue = (decodeValue & 0xF00) >> 8;
		int currentValue = (decodeValue & 0xF0) >> 4;

		LogManager.d(TAG,
				String.format(
						"minor[0x%X]header[0x%X] data[0x%X] nextValue[0x%X] currentValue[0x%X] nextPValue[0x%X]",
						minor, header, decode32Bit, nextValue, currentValue,
						nextPValue));
		if (nextPValue != -1 && nextPValue == currentValue) {
			nextPValue = nextValue;
			return true;
		}
		nextPValue = nextValue;
		return false;
	}

	public boolean isBlePowerSaveMode() {
		return blePowerSaveMode;
	}

	public void setBlePowerSaveMode(boolean blePowerSaveMode) {
		this.blePowerSaveMode = blePowerSaveMode;
		if (blePowerSaveMode) {
			this.beaconManager.setForegroundScanPeriod(10000L);
			this.beaconManager.setForegroundBetweenScanPeriod(300000L);
		} else {
			this.beaconManager.setForegroundScanPeriod(1100L);
			this.beaconManager.setForegroundBetweenScanPeriod(0L);
		}
	}

	@Override
	public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
		LogManager.d(TAG, String.format("Beacon size %d : \n%s", beacons.size(), beacons.toString()));

		if (beacons.size() > 0 && this.binding && region != null) {
			List<Beacon> _pushBeacons = new ArrayList<>();
			List<Beacon> _positioningBeacons = new ArrayList<>();
			if (this.pushBeacons != null && !this.pushBeacons.isEmpty()) {

				for (Beacon beacon : beacons) {
					final Beacon currentPushBeacon = beacon;
					String beaconFound = CollectionUtils.find(this.pushBeacons, new org.apache.commons.collections4.Predicate<String>() {
						@Override
						public boolean evaluate(String object) {
							return object.toUpperCase().equals(currentPushBeacon.getId1().toString().toUpperCase());
						}
					});
					if (beaconFound != null) {
						_pushBeacons.add(beacon);
					}
				}
			}
			if (this.positioningBeacons != null && !this.positioningBeacons.isEmpty()) {
				for (Beacon beacon : beacons) {
					final Beacon currentPushBeacon = new BeaconInternal(beacon);
					String beaconFound = CollectionUtils.find(this.positioningBeacons, new org.apache.commons.collections4.Predicate<String>() {
						@Override
						public boolean evaluate(String object) {
							return object.toUpperCase().equals(currentPushBeacon.getId1().toString().toUpperCase());
						}
					});
					if (beaconFound != null) {
						_positioningBeacons.add(beacon);
					}
				}
			}
			if (!_positioningBeacons.isEmpty()) {
				if (this.notifier != null) {
					this.notifier.didRangeBeacons(_positioningBeacons);
				}
			}
			if (!_pushBeacons.isEmpty()) {
				if (isHighestRssiFilterEnabled()) {
					PMPBeacon beacon = findNearestBeacon(_pushBeacons);
					if (beacon != null) {
						final String key = String.format("%s:%d", beacon.getUuid(),
								beacon.getMajor());
						if (key.equalsIgnoreCase(lastRecKey)) {
							LogManager.e(TAG, "Duplicate Beacon ID");
							return;
						}
						if (!validMinor(beacon.getMinor())) {
							LogManager.d(TAG, "Invalid Minor");
							return;
						}
						PMPBeacon kwBeacon = beaconByKey(key);

						// Reset non-cached beacon timer
						for (Beacon nonCachedBeacon : _pushBeacons) {
							PMPBeacon b = beaconByKey(String.format("%s:%d",
									nonCachedBeacon.getId1().toString(), nonCachedBeacon.getId2().toInt()));
							if (b != null)
								b.resetTimeout();
						}
						boolean compareResult = false;//(kwBeacon != null && beacon != null) ? kwBeacon.compare(beacon) : false;
						long currentGuardTime = System.currentTimeMillis()
								- PMPBeaconManager.this.lastAlterIDTime;
						LogManager.d(TAG, "currentGuardTime " + currentGuardTime);
						if (!compareResult) {
							if (kwBeacon != null) {
								kwBeacon.removeTimer();
								kwBeacon = null;
							}
							kwBeacon = new PMPBeacon(beacon.getUuid(),
									beacon.getMajor(), beacon.getMinor(),
									beacon.getRssi(),
									beacon.getDistance(),
									PMPBeaconManager.this.timeout, new PMPBeaconTimerNotifier() {

								@Override
								public void didTimeout(PMPBeacon beacon) {
									PMPBeaconManager.this.beaconEntryStates
											.remove(key);
									PMPBeaconManager.this.movingAvgDict
											.remove(key);
									broadcastExit(beacon, PMPBeaconManager.this.beaconEntryStates
											.size() == 0);
								}
							});
							PMPBeaconManager.this.beaconEntryStates.put(key, kwBeacon);
							PMPBeaconManager.this.lastDidShowBeacon = kwBeacon;
							PMPBeaconManager.this.lastAlterIDTime = System
									.currentTimeMillis();
							broadcastEnter(kwBeacon);
						} else if (kwBeacon != null
								&& !kwBeacon
								.compare(PMPBeaconManager.this.lastDidShowBeacon)
								&& currentGuardTime > PMPBeaconManager.this.alterIDGuardTime) {
							PMPBeaconManager.this.lastDidShowBeacon = kwBeacon;
							PMPBeaconManager.this.lastAlterIDTime = System
									.currentTimeMillis();
							broadcastEnter(kwBeacon);
						}
					}

				} else {
					for (Beacon beacon : _pushBeacons) {
						// Log.d(TAG, String.format("[%s] [%d]",
						// beacon.getMajor(),
						// beacon.getRssi()));
						broadcast(beacon);
					}
				}
			}
		}
	}

	private class InternalBeaconConsumer implements BeaconConsumer {
		private InternalBeaconConsumer() {
		}

		@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
		public void onBeaconServiceConnect() {
			LogManager.d("AppStarter", "Activating background region monitoring");
//			PMPBeaconManager.this.beaconManager
//					.setRangeNotifier(PMPBeaconManager.this);
			PMPBeaconManager.this.beaconManager.addRangeNotifier(PMPBeaconManager.this);
			try {
				if (pushBeacons != null && pushBeacons.size() > 0) {
					for (String uuid : pushBeacons)
//						PMPBeaconManager.this.beaconManager.startRangingBeaconsInRegion(new Region(uuid, uuid, new Integer(-1), new Integer(-1)));
						PMPBeaconManager.this.beaconManager
								.startRangingBeaconsInRegion(new Region(uuid,
										Identifier.parse(uuid), null, null));
				}
				if (positioningBeacons != null && positioningBeacons.size() > 0) {
					for (String uuid : positioningBeacons)
//						PMPBeaconManager.this.beaconManager.startRangingBeaconsInRegion(new Region(uuid, uuid, new Integer(-1), new Integer(-1)));
						PMPBeaconManager.this.beaconManager
								.startRangingBeaconsInRegion(new Region(uuid,
										Identifier.parse(uuid), null, null));
				}
			} catch (RemoteException e) {
				PMPBeaconManager.this.notifier
						.didError(PMPBeaconManagerError.RANGING_NOT_START);
			}

		}

		public boolean bindService(Intent intent, ServiceConnection conn,
								   int arg2) {
			return PMPBeaconManager.this.context.getApplicationContext()
					.bindService(intent, conn, arg2);
		}

		public Context getApplicationContext() {
			return PMPBeaconManager.this.context.getApplicationContext();
		}

		public void unbindService(ServiceConnection conn) {
			PMPBeaconManager.this.context.getApplicationContext().unbindService(
					conn);
		}
	}
}
