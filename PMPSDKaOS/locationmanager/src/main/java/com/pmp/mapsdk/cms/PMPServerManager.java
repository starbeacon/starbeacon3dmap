package com.pmp.mapsdk.cms;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.pmp.mapsdk.cms.model.ResponseData;
import com.pmp.mapsdk.cms.model.Tags;
import com.pmp.mapsdk.utils.PMPUtil;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Yu on 16/2/16.
 */
public class PMPServerManager {
    public static final String TAG = PMPServerManager.class.getSimpleName();
    public static String BASE_URL = "https://sit-cdn.starbeacon.io/api/";

    private static PMPServerManager shared = null;
    private ResponseData serverResponse;
    private String version;
    private RequestQueue requestQueue = null;
    private boolean requesting = false;
    private int previousStatusCode = 0;
    private static final boolean isLocal = false;
    private ArrayList<Tags> tags;
    private Handler handler = new Handler();

    public void setContext(Context context) {
        this.context = context;
        this.requestQueue = Volley.newRequestQueue(context);
    }

    private Context context;

    public static PMPServerManager getShared(Context context) {
        if (shared == null) {
            shared = new PMPServerManager();
        }
        if (context != null) {
            shared.setContext(context);
        }
        return  shared;
    }

    public static PMPServerManager getShared() {
        return shared;
    }


    public void fetchCacheJson(int projectID, final PMPServerManagerNotifier notifier) {
        String url = BASE_URL + "list_maps";

        downloadData(url, true, "maps.json", new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                ResponseData responseData = null;
                try{
                    responseData = new Gson().fromJson(response, ResponseData.class);
                }catch (JsonSyntaxException e){
                    Log.e(TAG, "Invalid JSON!");
                }
                if (responseData != null && responseData.getErrorCode() == 0) {
                    serverResponse = responseData;
                    if (tags != null) {
                        serverResponse.setTags(tags);
                    }
                    serverResponse.build();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
//                                PMPDataManager.getSharedPMPManager(context).nativeParseJson(response,true);
                                Log.d(TAG, "nativeParseJson complete");
                                notifier.didSuccess(response);
                            }catch (final Exception exception) {
                                Log.d(TAG, "Exception ≈:" + exception.getMessage());
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        //show alert message
                                        Toast.makeText(context, exception.getMessage(), Toast.LENGTH_LONG).show();
//
//                                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                                        builder.setTitle("Error").setMessage(exception.getMessage());
//                                        builder.show();
                                    }
                                });

                            }
                        }
                    }).start();
                }else {
                    notifier.didFailure();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                notifier.didFailure();
            }
        });

        String tagUrl = BASE_URL + "tags";

        downloadData(tagUrl, true, "tags.json", new Response.Listener<String>(){
            @Override
            public void onResponse(String response) {
                try {
//                    Log.d(TAG, "parsing tags");
                    Tags[] tmp = new Gson().fromJson(response, Tags[].class);
                    tags = new ArrayList<Tags>();
                    for (Tags tag :
                            tmp) {
                        tags.add(tag);
                    }
                    if (serverResponse != null) {
                        serverResponse.setTags(tags);
                    }
//                    Log.d(TAG, "tags complete");
                }catch (Exception exception) {
//                    Log.e(TAG, "tag error:" + exception.getMessage());
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
    }

    public void downloadMap(int projectID, final PMPServerManagerNotifier notifier) {
        String url = BASE_URL + "list_maps";
        if (projectID != 0) {
            url = BASE_URL + projectID + "/list_maps";
        }
        downloadData(url, false, "maps.json", new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                ResponseData responseData = null;
                try{
                    responseData = new Gson().fromJson(response, ResponseData.class);
                }catch (JsonSyntaxException e){
                    Log.e(TAG, "Invalid JSON!");
                }
                if (responseData != null && responseData.getErrorCode() == 0) {
                    serverResponse = responseData;
                    if (tags != null) {
                        serverResponse.setTags(tags);
                    }
                    serverResponse.build();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
//                                PMPDataManager.getSharedPMPManager(context).nativeParseJson(response,false);
//                                Log.d(TAG, "nativeParseJson complete");
                                notifier.didSuccess(response);
                            }catch (final Exception exception) {
                                Log.d(TAG, "Exception ≈:" + exception.getMessage());
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        //show alert message
                                        Toast.makeText(context, exception.getMessage(), Toast.LENGTH_LONG).show();
//
//                                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                                        builder.setTitle("Error").setMessage(exception.getMessage());
//                                        builder.show();
                                    }
                                });

                            }
                        }
                    }).start();
                }else {
                    notifier.didFailure();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                notifier.didFailure();
            }
        });

        getTags();
            /*
            requesting = true;

            StringRequest jsObjRequest = new StringRequest
                    (Request.Method.GET, String.format(JSON_URL, projectID), new Response.Listener<String>() {
                        @Override
                        public void onResponse(final String response) {
                            Log.d(TAG, "onResponse!");
                            requesting = false;
                            try {
                                serverResponse = new Gson().fromJson(response, ResponseData.class);
                                if (tags != null) {
                                    serverResponse.setTags(tags);
                                }

                                serverResponse.build();
                                int errorCode = (int)serverResponse.getErrorCode();//response.getInt("errorCode");
                                if (errorCode == 0) {
                                    File dir = context.getFilesDir();
                                    try {
                                        File file = new File(dir, "maps.json");
                                        FileOutputStream fos = new FileOutputStream(file, false);
                                        fos.write(response.toString().getBytes());
                                        fos.close();
                                    } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

//                                PMPUtil.getSharedPreferences(context).edit().putString(JSON_CACHE, response.toString()).commit();
                                    //serverResponse = new ResponseData(response);
                                    if (notifier != null) {
                                        notifier.didSuccess(serverResponse);
                                    }

                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                PMPDataManager.getSharedPMPManager(context).nativeParseJson(response);
                                                Log.d(TAG, "nativeParseJson complete");
                                            }catch (Exception exception) {
                                                Log.d(TAG, "Exception nativeParseJson:" + exception.getMessage());
                                            }
                                        }
                                    }).start();
                                } else {
                                    handleError(notifier);
                                }
                            } catch (Exception e) {
                                Log.e(TAG, e.getMessage());
                                handleError(notifier);
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d(TAG, "onErrorResponse");
                            requesting = false;
                            handleError(notifier);
                        }
                    }){

                private final String DATE_KEY = "Date";


                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    int mStatusCode = response.statusCode;
                    String date = response.headers.get("Last-Modified");
                    if(date != null)
                        PMPUtil.getSharedPreferences(context).edit().putString(DATE_KEY ,date).commit();

                    return super.parseNetworkResponse(response);
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return super.getParams();
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headerMap = super.getHeaders();

                    HashMap<String, String> customHeader = new HashMap<String, String>(headerMap);
                    String lastModify = PMPUtil.getSharedPreferences(context).getString(DATE_KEY, null);
                    if(lastModify != null)
                        customHeader.put("If-Modified-Since", lastModify);

//                    Fri, 03 Feb 2017 10:39:39 GMT


                    return customHeader;
                }
            };

//            Cache.Entry cacheEntry = this.requestQueue.getCache().get(url);
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            jsObjRequest.setShouldCache(false);
//            jsObjRequest.setCacheEntry(cacheEntry);
            this.requestQueue.add(jsObjRequest);
            */

//        }

    }

/*
    public void handleError(final PMPServerManagerNotifier notifier) {
        if(notifier == null){
            return;
        }
        File dir = context.getFilesDir();
        File file = new File(dir, "maps.json");
        String cacheJson = null;
        if (file.exists()) {
            try {
                FileInputStream fis = new FileInputStream(file);
                cacheJson = IOUtils.toString(fis);
                fis.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        final String finalCacheJson = cacheJson;
        new Thread(new Runnable() {
            @Override
            public void run() {

                if(finalCacheJson != null){
                    //Load cache...
                    try {
                        Log.d(TAG, "Loading cached json");
                        serverResponse = new Gson().fromJson(finalCacheJson, ResponseData.class);
                        if (tags != null) {
                            serverResponse.setTags(tags);
                        }

                        serverResponse.build();
                        notifier.didSuccess(serverResponse);
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    PMPDataManager.getSharedPMPManager(context).nativeParseJson(finalCacheJson);
                                    Log.d(TAG, "nativeParseJson complete");
                                }catch (Exception exception) {
                                    Log.d(TAG, "Exception nativeParseJson:" + exception.getMessage());
                                }
                            }
                        }).start();
                    } catch (Exception e) {
                        e.printStackTrace();
                        notifier.didFailure();
                    }

                }else{
                    notifier.didFailure();
                }
            }
        }).start();

    }
*/


    private <T> void  downloadData(String url, final boolean fetchLocalCacheJson, final String fileName, final Response.Listener<String> listener, final Response.ErrorListener errorListener) {
        requesting = true;
        final Response.ErrorListener myErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                requesting = false;

                File dir = context.getFilesDir();
                File file = new File(dir, fileName);
                String cacheJson = null;
                if (file.exists()) {

                    try {
                        FileInputStream fis = new FileInputStream(file);
                        cacheJson = IOUtils.toString(fis, "UTF-8");
                        fis.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else{

                }

                if (cacheJson == null || cacheJson.length() <= 0) {

                    errorListener.onErrorResponse(error);
                }else {
                    char firstChar = cacheJson.charAt(0);
                    char lastChar = cacheJson.charAt(cacheJson.length() - 1);
                    char lastSecondChar = cacheJson.charAt(cacheJson.length() - 2);

                    boolean validJson = false;
                    if (firstChar == '[' && lastChar == ']') {
                        validJson = true;
                    }else if ((firstChar == '{' && lastChar == '}') || (firstChar == '{' && lastSecondChar == '}')) {
                        validJson = true;
                    }

                    if (validJson) {
                        listener.onResponse(cacheJson);
                    }else {
                        file.delete();
                        errorListener.onErrorResponse(null);
                    }
                }
            }
        };
        StringRequest jsObjRequest = new StringRequest
                (Request.Method.GET, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        requesting = false;

                        boolean validJson = false;

                        if(response.length() > 0){
                            String s = response.trim();
                            char firstChar = response.charAt(0);
                            char lastChar = response.charAt(response.length() - 1);
                            if (firstChar == '[' && lastChar == ']') {
                                validJson = true;
                            }else if (firstChar == '{' && lastChar == '}') {
                                validJson = true;
                            }
                        }


                        if (validJson) {
                            File dir = context.getFilesDir();
                            try {
                                File file = new File(dir, fileName);
                                FileOutputStream fos = new FileOutputStream(file, false);
                                fos.write(response.getBytes("UTF-8"));
                                fos.close();
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            listener.onResponse(response);
                        }else {
                            myErrorListener.onErrorResponse(null);
                        }

                    }
                }, myErrorListener){

            private final String DATE_KEY = "Date_" + fileName;


            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                previousStatusCode = response.statusCode;
                String date = response.headers.get("Last-Modified");
                if(date != null)
                    PMPUtil.getSharedPreferences(context).edit().putString(DATE_KEY ,date).commit();

                Response<String> r = super.parseNetworkResponse(response);
                return r;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return super.getParams();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerMap = super.getHeaders();

                HashMap<String, String> customHeader = new HashMap<String, String>(headerMap);
                String lastModify = PMPUtil.getSharedPreferences(context).getString(DATE_KEY, null);
                if(lastModify != null)
                    customHeader.put("If-Modified-Since", lastModify);
                return customHeader;
            }
        };

        if(fetchLocalCacheJson){
            File dir = context.getFilesDir();
            File file = new File(dir, fileName);
            String cacheJson = null;
            if (file.exists()) {
                try {
                    FileInputStream fis = new FileInputStream(file);
                    cacheJson = IOUtils.toString(fis, "UTF-8");
                    fis.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (cacheJson == null || cacheJson.length() <= 0) {
                errorListener.onErrorResponse(null);
            }else {
                char firstChar = cacheJson.charAt(0);
                char lastChar = cacheJson.charAt(cacheJson.length() - 1);
                char lastSecondChar = cacheJson.charAt(cacheJson.length() - 2);

                boolean validJson = false;
                if (firstChar == '[' && lastChar == ']') {
                    validJson = true;
                }else if ((firstChar == '{' && lastChar == '}') || (firstChar == '{' && lastSecondChar == '}')) {
                    validJson = true;
                }

                if (validJson) {
                    listener.onResponse(cacheJson);
                }else {
                    file.delete();
                    errorListener.onErrorResponse(null);
                }
            }
        }else {
            Cache.Entry entry = new Cache.Entry();
            entry.data = new byte[0];

            jsObjRequest.setCacheEntry(entry);
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            jsObjRequest.setShouldCache(false);
            this.requestQueue.add(jsObjRequest);
        }
    }

    public void downloadTags(int projectID, final PMPServerManagerNotifier notifier) {
        String url = BASE_URL + "tags";
        if (projectID != 0) {
            url = BASE_URL + projectID + "/tags";
        }

        downloadData(url, false, "tags.json", new Response.Listener<String>(){
            @Override
            public void onResponse(String response) {
                try {
                    Tags[] tmp = new Gson().fromJson(response, Tags[].class);
                    tags = new ArrayList<Tags>();
                    for (Tags tag :
                            tmp) {
                        tags.add(tag);
                    }
                    if (serverResponse != null) {
                        serverResponse.setTags(tags);
                    }
                    notifier.didSuccess(response);
                }catch (Exception exception) {
                    notifier.didFailure();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                notifier.didFailure();
            }
        });
    }

        public void getTags() {
        String url = BASE_URL + "tags";

        downloadData(url, false, "tags.json", new Response.Listener<String>(){
            @Override
            public void onResponse(String response) {
                try {
//                    Log.d(TAG, "parsing tags");
                    Tags[] tmp = new Gson().fromJson(response, Tags[].class);
                    tags = new ArrayList<Tags>();
                    for (Tags tag :
                            tmp) {
                        tags.add(tag);
                    }
                    if (serverResponse != null) {
                        serverResponse.setTags(tags);
                    }
//                    Log.d(TAG, "tags complete");
                }catch (Exception exception) {
//                    Log.e(TAG, "tag error:" + exception.getMessage());
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
    }

//    public void getBrandDataAsync(String lang, final Response.Listener<String> listener) {
//
//        String url = String.format(BuildConfig.BaseURL + "%s/brands", lang);;
//
//        downloadData(url, false, "brands_" + lang + ".json", new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                listener.onResponse(response);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                listener.onResponse(null);
//            }
//        });
//    }
//
//    public void getArtAndCultureDataAsync(String lang, final Response.Listener<String> listener) {
//
//        String url = String.format( BuildConfig.BaseURL + "%s/art_and_culture", lang);
//
//        downloadData(url, false, "art_and_culture_" + lang + ".json", new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                listener.onResponse(response);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                listener.onResponse(null);
//            }
//        });
//    }

    public ResponseData getServerResponse() {
        return serverResponse;
    }

    public boolean isRequesting() {
        return requesting;
    }

//    public com.pmp.mapsdk.cms.model.proximity.Response getProximityZoneMappingResponse() {
//        AssetManager assetManager = this.context.getAssets();
//        try {
//            InputStream inputStream = assetManager.open("zone_data.json");
//            StringWriter writer = new StringWriter();
//            IOUtils.copy(inputStream, writer, Charset.defaultCharset());
//            String theString = writer.toString();
//            return new Gson().fromJson(theString, com.pmp.mapsdk.cms.model.proximity.Response.class);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
}
