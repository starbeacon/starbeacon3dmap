//package com.pmp.mapsdk.cms;
//
//import android.content.Context;
//import android.content.SharedPreferences;
//import android.text.TextUtils;
//import android.util.Log;
//
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.RequestFuture;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
//import com.cherrypicks.pmpmapsdk.BuildConfig;
//import com.google.gson.Gson;
//import com.pmp.mapsdk.cms.model.EnableMonitoringResponse;
//import com.pmp.mapsdk.cms.model.PMPProximityServerManagerNotifier;
//import com.pmp.mapsdk.cms.model.SaveUserFlightResponse;
//import com.pmp.mapsdk.cms.model.SaveUserLocationResponse;
//import com.pmp.mapsdk.cms.model.SaveUserTokenResult;
//import com.pmp.mapsdk.external.PMPMapSDK;
//import com.pmp.mapsdk.utils.PMPUtil;
//
//import java.io.UnsupportedEncodingException;
//import java.net.URLEncoder;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.TimeZone;
//import java.util.concurrent.ExecutionException;
//import java.util.concurrent.TimeUnit;
//import java.util.concurrent.TimeoutException;
//
///**
// * Created by Alan on 16/2/16.
// */
//public class PMPProximityServerManager {
//    public static final String TAG = PMPProximityServerManager.class.getSimpleName();
//    private static String JSON_URL = BuildConfig.ProximityServerURL;
//
//    private static PMPProximityServerManager shared = null;
//    private RequestQueue requestQueue = null;
//    private SaveUserLocationResponse saveUserLocationResponse = null;
//    private SaveUserFlightResponse saveUserFlightResponse = null;
//
//    public void setContext(Context context) {
//        this.context = context;
//    }
//
//    private Context context;
//
//    public static PMPProximityServerManager getShared(Context context) {
//        if (shared == null) {
//            shared = new PMPProximityServerManager();
//            shared.requestQueue = Volley.newRequestQueue(context);
//        }
//        if (context != null) {
//            shared.setContext(context);
//        }
//        return  shared;
//    }
//
//    public static PMPProximityServerManager getShared() {
//        return shared;
//    }
//
//    public void saveUserLocation(String AAZoneId, final PMPProximityServerManagerNotifier notifier) {
//        String staticParams = "&platform=android&clientTimestamp=" + System.currentTimeMillis();
//        String mtelUserId = PMPUtil.getSharedPreferences(context).getString("MtelUserId", null);
//        String pushToken = PMPUtil.getSharedPreferences(context).getString("MtelPushToken", null);
//
//        if (!TextUtils.isEmpty(pushToken)) {
//            String url = JSON_URL + "saveUserCurrentLocation?mtelUserId=" + mtelUserId + "&pushToken=" + pushToken + "&AAZoneId=" + AAZoneId + staticParams;
//            StringRequest jsObjRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//                @Override
//                public void onResponse(final String response) {
//
//                    try {
//                        SaveUserLocationResponse serverResponse = new Gson().fromJson(response, SaveUserLocationResponse.class);
//                        int errorCode = Integer.parseInt("" + serverResponse.getErrorCode());
//                        if (errorCode == 0) {
//                            saveUserLocationResponse = serverResponse;
//                            if (notifier != null) notifier.didSuccess(serverResponse);
//                        } else {
//                            if (notifier != null) notifier.didFailure();
//                        }
//                    } catch (Exception e) {
//                        Log.e(TAG, e.getMessage());
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    Log.d(TAG, "onErrorResponse");
//                }
//            });
//
////            Cache.Entry cacheEntry = this.requestQueue.getCache().get(url);
//            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//            jsObjRequest.setShouldCache(false);
////            jsObjRequest.setCacheEntry(cacheEntry);
//            this.requestQueue.add(jsObjRequest);
//        }
//    }
//
//    public void saveUserToken(String mtelUserId, String pushToken , int language, final PMPProximityServerManagerNotifier notifier) {
//        if (TextUtils.isEmpty(pushToken)) {
//            notifier.didFailure();
//            return;
//        }
//
//        String url = getSaveUserTokenUrl(mtelUserId, pushToken, language);
//        StringRequest jsObjRequest = new StringRequest(Request.Method.POST, url , new Response.Listener<String>() {
//            @Override
//            public void onResponse(final String response) {
////                Log.i("test", "test response = " + response);
//
//                try {
//                    SaveUserTokenResult serverResponse = new Gson().fromJson(response, SaveUserTokenResult.class);
//
//
//                    int errorCode = Integer.parseInt("" + serverResponse.getErrorCode());
//                    if (errorCode == 0) {
//                        if(notifier != null)notifier.didSuccess(serverResponse);
//                    } else {
//                        if(notifier != null)notifier.didFailure();
//                    }
//                } catch (Exception e) {
//                    Log.e(TAG, e.getMessage());
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d(TAG, "onErrorResponse");
//            }
//        });
//
////            Cache.Entry cacheEntry = this.requestQueue.getCache().get(url);
//        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        jsObjRequest.setShouldCache(false);
////            jsObjRequest.setCacheEntry(cacheEntry);
//        this.requestQueue.add(jsObjRequest);
//    }
//
//    public boolean saveUserTokenSync(String mtelUserId, String pushToken , int language) {
//
//        if (TextUtils.isEmpty(pushToken)) {
//            return false;
//        }
//        String url = getSaveUserTokenUrl(mtelUserId, pushToken, language);
//
//        RequestFuture<String> future = RequestFuture.newFuture();
//
//        StringRequest jsObjRequest = new StringRequest(Request.Method.POST, url , future, future);
//
////            Cache.Entry cacheEntry = this.requestQueue.getCache().get(url);
//        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        jsObjRequest.setShouldCache(false);
////            jsObjRequest.setCacheEntry(cacheEntry);
//        this.requestQueue.add(jsObjRequest);
//
//        try {
//            String response = future.get(30, TimeUnit.SECONDS);
//            try {
//                SaveUserTokenResult serverResponse = new Gson().fromJson(response, SaveUserTokenResult.class);
//
//
//                int errorCode = Integer.parseInt("" + serverResponse.getErrorCode());
//                if (errorCode == 0) {
//                    return true;
//                } else {
//                    return false;
//                }
//            } catch (Exception e) {
//                Log.e(TAG, e.getMessage());
//            }
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        } catch (TimeoutException e) {
//            e.printStackTrace();
//        }
//        return false;
//    }
//
//    private String getSaveUserTokenUrl(String mtelUserId, String pushToken , int language){
//        String staticParams = "&platform=android&clientTimestamp=" + System.currentTimeMillis();
//
//        String url = JSON_URL + "saveUserToken?mtelUserId=" + mtelUserId + "&pushToken=" + pushToken + "&language=" + language + staticParams;
//        return url;
//    }
//
//    public boolean saveUserFlight(String mtelUserId, String pushToken , String mtelRecordId, String flightNo, String flightPreferredId, String flightDate, String flightTime, String isArrival,
//                               int enable, String gid) {
//        //save flight if previous save not success
//        String savedMtelUserId = PMPUtil.getSharedPreferences(context).getString("MtelUserId", null);
//        String savedPushToken = PMPUtil.getSharedPreferences(context).getString("MtelPushToken", null);
//        if(savedMtelUserId == null || savedPushToken == null){
//            boolean success = saveUserTokenSync(mtelUserId, pushToken, PMPMapSDK.getLangID());
//            if(!success){
//                return false;
//            }else{
//                SharedPreferences.Editor editor = PMPUtil.getSharedPreferences(context).edit();
//                editor.putString("MtelUserId", mtelUserId);
//                editor.putString("MtelPushToken", pushToken);
//                editor.commit();
//            }
//        }
//
//        String dateTime = flightDate + " " + flightTime;
//        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//        fmt.setTimeZone(TimeZone.getTimeZone("Hongkong"));
//        long time = 0;
//        try {
//            Date date = fmt.parse(dateTime);
//            time = date.getTime();
//        } catch (ParseException e) {
//            e.printStackTrace();
//            return false;
//        }
//        String json = "";
//        try {
//            json = "{\"gid\":\"" + gid + "\",\"recordId\":\"" + mtelRecordId + "\",\"flightNo\":\"" + flightPreferredId + "\",\"isArrival\":\"" + isArrival + "\"}";
//            json = URLEncoder.encode(json, "UTF-8");
//            flightNo = URLEncoder.encode(flightNo, "UTF-8");
//            flightPreferredId = URLEncoder.encode(flightPreferredId, "UTF-8");
//
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//        String staticParams = "&platform=android&clientTimestamp=" + System.currentTimeMillis();
//
//        String url = JSON_URL + "saveUserFlight?mtelUserId=" + mtelUserId + "&pushToken=" + pushToken + "&mtelRecordId=" + mtelRecordId
//                + "&flightNo=" + flightNo + "&flightDate=" + time + "&isArrival=" + isArrival + "&enable=" + enable +
//                "&extra=" + json;
//        url += staticParams;
//
//        RequestFuture<String> future = RequestFuture.newFuture();
//
//        StringRequest jsObjRequest = new StringRequest(Request.Method.POST, url, future, future);
//
//        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        jsObjRequest.setShouldCache(false);
////            jsObjRequest.setCacheEntry(cacheEntry);
//        this.requestQueue.add(jsObjRequest);
//        try {
//            String response = future.get(30, TimeUnit.SECONDS);
//            try {
//                EnableMonitoringResponse serverResponse = new Gson().fromJson(response, EnableMonitoringResponse.class);
//
//
//                int errorCode = Integer.parseInt("" + serverResponse.getErrorCode());
//                if (errorCode == 0) {
//                    return true;
//                } else {
//                    return false;
//                }
//            } catch (Exception e) {
//                Log.e(TAG, e.getMessage());
//            }
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        } catch (TimeoutException e) {
//            e.printStackTrace();
//        }
//        return false;
//
//    }
//
//    public void getUserSavedFlight(final PMPProximityServerManagerNotifier notifier) {
//        String mtelUserId = PMPUtil.getSharedPreferences(context).getString("MtelUserId", null);
//        String pushToken = PMPUtil.getSharedPreferences(context).getString("MtelPushToken", null);
//        if (!TextUtils.isEmpty(pushToken)) {
//            String staticParams = "&platform=android&clientTimestamp=" + System.currentTimeMillis();
//            String url = JSON_URL + "getUserSavedFlight?mtelUserId=" + mtelUserId + "&pushToken=" + pushToken;
//            url += staticParams;
////            Log.i("test", "test url = " + url);
//            StringRequest jsObjRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//                @Override
//                public void onResponse(final String response) {
////                    Log.i("test", "test response = " + response);
//                    try {
//                        SaveUserFlightResponse serverResponse = new Gson().fromJson(response, SaveUserFlightResponse.class);
//                        int errorCode = Integer.parseInt("" + serverResponse.getErrorCode());
//                        if (errorCode == 0) {
//                            saveUserFlightResponse = serverResponse;
//                            if (notifier != null) notifier.didSuccess(serverResponse);
//                        } else {
//                            saveUserFlightResponse = null;
//                            if (notifier != null) notifier.didFailure();
//                        }
//                    } catch (Exception e) {
//                        Log.e(TAG, e.getMessage());
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    Log.d(TAG, "onErrorResponse");
//                }
//            });
//
////            Cache.Entry cacheEntry = this.requestQueue.getCache().get(url);
//            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//            jsObjRequest.setShouldCache(false);
////            jsObjRequest.setCacheEntry(cacheEntry);
//            this.requestQueue.add(jsObjRequest);
//        }
//    }
//
//    public SaveUserLocationResponse getSaveUserLocationResponse() {
//        return saveUserLocationResponse;
//    }
//
//    public SaveUserFlightResponse getSaveUserFlightResponse() {
//        return saveUserFlightResponse;
//    }
//
//    public String getPushToken (){
//        return PMPUtil.getSharedPreferences(context).getString("MtelPushToken", null);
//    }
//}
