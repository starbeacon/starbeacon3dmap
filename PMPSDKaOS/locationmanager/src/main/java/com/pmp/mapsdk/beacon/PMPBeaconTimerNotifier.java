package com.pmp.mapsdk.beacon;

/**
 * Created by Yu on 17/2/16.
 */
public interface PMPBeaconTimerNotifier {
    void didTimeout(PMPBeacon beacon);
}
