package com.pmp.mapsdk.location.Trilateration;

/**
 * Created by Yu on 5/4/16.
 */
public class Location {
    public Location(float x, float y, float z, float scale) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.scale = scale;
    }
    public Location(double x, double y, double z, double scale) {
        this.x = (float)x;
        this.y = (float)y;
        this.z = (float)z;
        this.scale = (float)scale;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    float x, y, z, scale;

}
