package com.pmp.mapsdk.utils;

/**
 * Created by Yu on 22/2/16.
 */
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class PMPBootCompletedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent)
    {
        // Create Intent
        Intent serviceIntent = new Intent(context, PMPBackgroundService.class);
        // Start service
        context.startService(serviceIntent);

    }
}
