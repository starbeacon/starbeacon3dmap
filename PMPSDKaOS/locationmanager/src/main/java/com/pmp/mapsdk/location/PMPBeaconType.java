package com.pmp.mapsdk.location;

/**
 * Created by Yu on 17/2/16.
 */
public enum PMPBeaconType {
    Unknown(-1),
    Positioning(1),
    Push(2),
    Entry(3),
    LiftInside(4),
    EntryIndoor(5),
    RSSIFilter(6),
    ZonalPositioning(7),
    POI(8),
    ShuttleBusInside(9);
    private final int value;
    private PMPBeaconType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
