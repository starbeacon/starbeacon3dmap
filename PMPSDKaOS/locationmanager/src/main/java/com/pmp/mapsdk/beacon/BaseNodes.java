package com.pmp.mapsdk.beacon;

import java.util.ArrayList;

public abstract class BaseNodes {
    
    public abstract double getStatus() ;

    public abstract void setStatus(double status);

    public abstract double getMapId();

    public abstract void setMapId(double mapId);

    public abstract double getX();

    public abstract void setX(double x);

    public abstract double getId();

    public abstract void setId(double id);

    public abstract double getY();

    public abstract void setY(double y);

    public abstract double getZ();

    public abstract void setZ(double z);

    public abstract ArrayList<Integer> getPoiIds();

    public abstract void setPoiIds(ArrayList<Integer> poiIds);
    
}
