package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;
import com.pmp.mapsdk.beacon.BaseNodes;

import org.json.*;
import java.util.ArrayList;

public class Nodes extends BaseNodes{

    @SerializedName("status")
    private double status;
    @SerializedName("map_id")
    private double mapId;
    @SerializedName("x")
    private double x;
    @SerializedName("id")
    private double id;
    @SerializedName("y")
    private double y;
    @SerializedName("z")
    private double z;
    @SerializedName("poi_ids")
    private ArrayList<Integer> poiIds;
    
    
	public Nodes () {
		
	}	
        
    public Nodes (JSONObject json) {
    
        this.status = json.optDouble("status");
        this.mapId = json.optDouble("map_id");
        this.x = json.optDouble("x");
        this.id = json.optDouble("id");
        this.y = json.optDouble("y");
        this.z = json.optDouble("z");

        this.poiIds = new ArrayList<Integer>();
        JSONArray arrayPoiIds = json.optJSONArray("poi_ids");
        if (null != arrayPoiIds) {
            int poiIdsLength = arrayPoiIds.length();
            for (int i = 0; i < poiIdsLength; i++) {
                int item = arrayPoiIds.optInt(i);
                this.poiIds.add(item);

            }
        }
        else {
            int item = json.optInt("poi_ids");
            this.poiIds.add(item);

        }


    }
    
    public double getStatus() {
        return this.status;
    }

    public void setStatus(double status) {
        this.status = status;
    }

    public double getMapId() {
        return this.mapId;
    }

    public void setMapId(double mapId) {
        this.mapId = mapId;
    }

    public double getX() {
        return this.x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getId() {
        return this.id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public double getY() {
        return this.y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return this.z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public ArrayList<Integer> getPoiIds() {
        return this.poiIds;
    }

    public void setPoiIds(ArrayList<Integer> poiIds) {
        this.poiIds = poiIds;
    }


    
}
