package com.pmp.mapsdk.cms.model;

import org.json.*;
import com.google.gson.annotations.SerializedName;


public class AaZones {
    @SerializedName("id")
    private double id;
    @SerializedName("name")
    private String name;
    
    
	public AaZones () {
		
	}	
        
    public AaZones (JSONObject json) {
    
        this.id = json.optDouble("id");
        this.name = json.optString("name");

    }
    
    public double getId() {
        return this.id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }


    
}
