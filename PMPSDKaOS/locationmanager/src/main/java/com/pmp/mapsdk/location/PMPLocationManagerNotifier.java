package com.pmp.mapsdk.location;

import java.util.List;

/**
 * Created by Yu on 17/2/16.
 */
public interface PMPLocationManagerNotifier {
    void didIndoorLocationUpdated(PMPBeaconType beaconType, PMPLocation location);
    void didIndoorTransmissionsUpdated(List<PMPBeacon> transmissions);
    void didIndoorExitRegion();
    void didOutdoorLocationsUpdated();
    void didCompassUpdated(double direction);
    /**
     * Called when at least one beacon in a <code>Region</code> is visible.
     * @param region a Region that defines the criteria of beacons to look for
     */
    public void didProximityEnterRegion(int region, int minor, double rssi, double distance);

    /**
     * Called when no beacons in a <code>Region</code> are visible.
     */
    public void didProximityExitRegion(int region);
    void didError(PMPLocationManagerError error);
}
