package com.pmp.mapsdk.cms.model;

import com.google.gson.JsonArray;
import com.google.gson.annotations.SerializedName;

import org.json.*;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;

public class Tags {

    @SerializedName("content")
    private String content;
    @SerializedName("types")
    private  ArrayList<Tag> tagArray;

	public Tags () {
		
	}	
        
    public Tags (JSONObject json) {
        this.content = json.optString("content");
        this.tagArray = new ArrayList<Tag>();
        JSONArray tagJsonArray = json.optJSONArray("types");
        if(tagJsonArray != null){
            for(int i = 0; i < tagJsonArray.length(); i++){
                this.tagArray.add(new Tag(tagJsonArray.optJSONObject(i)));
            }
        }
    }
    
    public String getContent() {
        return this.content;
    }


    public ArrayList<Tag> getTagArray(){
        return this.tagArray;
    }

}
