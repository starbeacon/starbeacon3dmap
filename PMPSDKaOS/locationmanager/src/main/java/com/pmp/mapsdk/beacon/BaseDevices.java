package com.pmp.mapsdk.beacon;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public abstract class BaseDevices implements Parcelable {


    public abstract double getId();

    public abstract void setId(double id);

    public abstract double getX();

    public abstract void setX(double x);

    public abstract BaseDeviceOptions getDeviceOptions();

    public abstract double getY();

    public abstract void setY(double y);

    public abstract double getZ();

    public abstract void setZ(double z);

    public abstract double getBleZonalPushUuidId();

    public abstract void setBleZonalPushUuidId(double bleZonalPushUuidId);

    public abstract double getMapDeviceTypeId();

    public abstract void setMapDeviceTypeId(double mapDeviceTypeId);

    public abstract String getDeviceName();

    public abstract void setDeviceName(String deviceName);

    public abstract double getDeviceGroupId();

    public abstract void setDeviceGroupId(double deviceGroupId);

    public abstract double getMajorNo();

    public abstract void setMajorNo(double majorNo);

    public abstract double getMapId() ;

    public abstract void setMapId(double mapId);

    public abstract double getBleWayFindingUuidId();

    public abstract void setBleWayFindingUuidId(double bleWayFindingUuidId);

    public abstract double getStatus();

    public abstract void setStatus(double status);

    public abstract String getBleZonalPushUuid();

    public abstract String getBleWayFindingUuid();

    public abstract double getThreshold();

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    public abstract void setNodes(ArrayList<? extends BaseNodes> nodes);

    public abstract void build();
}
