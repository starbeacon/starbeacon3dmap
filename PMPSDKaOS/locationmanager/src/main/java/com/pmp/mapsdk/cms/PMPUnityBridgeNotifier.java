package com.pmp.mapsdk.cms;

/**
 * Created by gordonwong on 23/1/2018.
 */

public interface PMPUnityBridgeNotifier {
    void didFinishedDownloadMapJSON(boolean success, String responseString);
    void didFinishedDownloadTagJSON(boolean success, String responseString);
}
