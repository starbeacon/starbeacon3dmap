package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;


public class SaveUserTokenResult {

    @SerializedName("errorMessage")
    private String errorMessage;
    @SerializedName("errorCode")
    private String errorCode;
    @SerializedName("result")
    private String result;


    public SaveUserTokenResult() {

    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getResult() {
        return result;
    }
}
