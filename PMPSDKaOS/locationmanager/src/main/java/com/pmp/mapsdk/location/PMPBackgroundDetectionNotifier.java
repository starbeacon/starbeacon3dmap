package com.pmp.mapsdk.location;

/**
 * Created by Yu on 22/2/16.
 */
public interface PMPBackgroundDetectionNotifier {
    abstract void onDidEnterBackground();

    abstract void onDidEnterForeground();
}