package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Response {

    @SerializedName("map_scale")
    private double mapScale;
    @SerializedName("map_num_recent_search")
    private double mapNumRecentSearch;
    @SerializedName("map_device_types")
    private ArrayList<MapDeviceTypes> mapDeviceTypes;
    @SerializedName("maps")
    private ArrayList<Maps> maps;
    @SerializedName("path_types")
    private ArrayList<PathTypes> pathTypes;
    @SerializedName("uuids")
    private ArrayList<Uuids> uuids;
    @SerializedName("pois")
    private ArrayList<Pois> pois;
    @SerializedName("notifications")
    private ArrayList<String> notifications;
    @SerializedName("step_messages")
    private ArrayList<StepMessages> stepMessages;
    @SerializedName("map_num_search_paths")
    private double mapNumSearchPaths;
    @SerializedName("devices")
    private ArrayList<Devices> devices;
    @SerializedName("nodes")
    private ArrayList<Nodes> nodes;
    @SerializedName("promotions")
    private ArrayList<Promotions> promotions;
    @SerializedName("errorCode")
    private double errorCode;
    @SerializedName("map_direction")
    private double mapDirection;
    @SerializedName("markers")
    private ArrayList<Markers> markers;
    @SerializedName("paths")
    private ArrayList<Paths> paths;
    @SerializedName("poi_categories")
    private ArrayList<PoiCategories> poiCategories;
    @SerializedName("tile_url_prefix")
    private String tileUrlPrefix;
    @SerializedName("map_lng")
    private double mapLng;
    @SerializedName("custom_step_messages")
    private ArrayList<CustomStepMessages> customStepMessages;
    @SerializedName("map_lat")
    private double mapLat;
    @SerializedName("aos_change_floor_filters")
    private ArrayList<Integer> aosChangeFloorFilters;
    @SerializedName("ios_change_floor_filters")
    private ArrayList<Integer> iosChangeFloorFilters;
    @SerializedName("areas")
    private ArrayList<Areas> areas;
    @SerializedName("brands")
    private ArrayList<Brands> brands;
    @SerializedName("languages")
    private ArrayList<Languages> languages;
    @SerializedName("last_step_message")
    private ArrayList<LastStepMessage> lastStepMessages;
    @SerializedName("gate_ids")
    private ArrayList<Integer> gateIds;
    @SerializedName("tags")
    private ArrayList<Tags> tags;
    @SerializedName("device_groups")
    private ArrayList<DeviceGroups> deviceGroups;
    @SerializedName("push_message_timeout")
    private int pushMessageTimeout;
    @SerializedName("zone_logging_timeout")
    private int zoneLoggingTimeout;
    @SerializedName("zone_did_change_logging_timeout")
    private int zoneDidChangeLoggingTimeout;
    @SerializedName("zones")
    private ArrayList<Zones> zones;
    @SerializedName("aa_zones")
    private ArrayList<AaZones> aaZones;
    @SerializedName("map_default_zoom")
    private double mapDefaultZoom;

	public Response () {
		
	}	
        
//    public Response (JSONObject json) {
//
//        this.mapScale = json.optDouble("map_scale");
//        this.mapNumRecentSearch = json.optDouble("map_num_recent_search");
//
//        HashMap<Double, Double> mapDeviceTypesMap = new HashMap<>();
//        this.mapDeviceTypes = new ArrayList<MapDeviceTypes>();
//        JSONArray arrayMapDeviceTypes = json.optJSONArray("map_device_types");
//        if (null != arrayMapDeviceTypes) {
//            int mapDeviceTypesLength = arrayMapDeviceTypes.length();
//            for (int i = 0; i < mapDeviceTypesLength; i++) {
//                JSONObject item = arrayMapDeviceTypes.optJSONObject(i);
//                if (null != item) {
//                    mapDeviceTypesMap.put(item.optDouble("id"), item.optDouble("threshold"));
//                    this.mapDeviceTypes.add(new MapDeviceTypes(item));
//                }
//            }
//        }
//        else {
//            JSONObject item = json.optJSONObject("map_device_types");
//            if (null != item) {
//                this.mapDeviceTypes.add(new MapDeviceTypes(item));
//            }
//        }
//
//
//        this.maps = new ArrayList<Maps>();
//        JSONArray arrayMaps = json.optJSONArray("maps");
//        if (null != arrayMaps) {
//            int mapsLength = arrayMaps.length();
//            for (int i = 0; i < mapsLength; i++) {
//                JSONObject item = arrayMaps.optJSONObject(i);
//                if (null != item) {
//                    this.maps.add(new Maps(item));
//                }
//            }
//        }
//        else {
//            JSONObject item = json.optJSONObject("maps");
//            if (null != item) {
//                this.maps.add(new Maps(item));
//            }
//        }
//
//
//        this.pathTypes = new ArrayList<PathTypes>();
//        JSONArray arrayPathTypes = json.optJSONArray("path_types");
//        if (null != arrayPathTypes) {
//            int pathTypesLength = arrayPathTypes.length();
//            for (int i = 0; i < pathTypesLength; i++) {
//                JSONObject item = arrayPathTypes.optJSONObject(i);
//                if (null != item) {
//                    this.pathTypes.add(new PathTypes(item));
//                }
//            }
//        }
//        else {
//            JSONObject item = json.optJSONObject("path_types");
//            if (null != item) {
//                this.pathTypes.add(new PathTypes(item));
//            }
//        }
//
//        HashMap<Double, String> uuidsMap = new HashMap<>();
//        this.uuids = new ArrayList<Uuids>();
//        JSONArray arrayUuids = json.optJSONArray("uuids");
//        if (null != arrayUuids) {
//            int uuidsLength = arrayUuids.length();
//            for (int i = 0; i < uuidsLength; i++) {
//                JSONObject item = arrayUuids.optJSONObject(i);
//                if (null != item) {
//                    uuidsMap.put(item.optDouble("id"), item.optString("content"));
//                    this.uuids.add(new Uuids(item));
//                }
//            }
//        }
//        else {
//            JSONObject item = json.optJSONObject("uuids");
//            if (null != item) {
//                this.uuids.add(new Uuids(item));
//            }
//        }
//
//        this.aosChangeFloorFilters = new ArrayList<Integer>();
//        JSONArray arrayAosChangeFloorFilters = json.optJSONArray("aos_change_floor_filters");
//        if (null != arrayAosChangeFloorFilters) {
//            int aosChangeFloorFiltersLength = arrayAosChangeFloorFilters.length();
//            for (int i = 0; i < aosChangeFloorFiltersLength; i++) {
//                int item = arrayAosChangeFloorFilters.optInt(i);
//                this.aosChangeFloorFilters.add(item);
//
//            }
//        }
//        else {
//            int item = json.optInt("aos_change_floor_filters");
//            this.aosChangeFloorFilters.add(item);
//
//        }
//
//
//        this.iosChangeFloorFilters = new ArrayList<Integer>();
//        JSONArray arrayIosChangeFloorFilters = json.optJSONArray("ios_change_floor_filters");
//        if (null != arrayIosChangeFloorFilters) {
//            int iosChangeFloorFiltersLength = arrayIosChangeFloorFilters.length();
//            for (int i = 0; i < iosChangeFloorFiltersLength; i++) {
//                int item = arrayIosChangeFloorFilters.optInt(i);
//                this.iosChangeFloorFilters.add(item);
//            }
//        }
//        else {
//            int item = json.optInt("ios_change_floor_filters");
//            this.iosChangeFloorFilters.add(item);
//        }
//
//        this.pois = new ArrayList<Pois>();
//        JSONArray arrayPois = json.optJSONArray("pois");
//        if (null != arrayPois) {
//            int poisLength = arrayPois.length();
//            for (int i = 0; i < poisLength; i++) {
//                JSONObject item = arrayPois.optJSONObject(i);
//                if (null != item) {
//                    this.pois.add(new Pois(item));
//                }
//            }
//        }
//        else {
//            JSONObject item = json.optJSONObject("pois");
//            if (null != item) {
//                this.pois.add(new Pois(item));
//            }
//        }
//
//
//        this.notifications = new ArrayList<String>();
//        JSONArray arrayNotifications = json.optJSONArray("notifications");
//        if (null != arrayNotifications) {
//            int notificationsLength = arrayNotifications.length();
//            for (int i = 0; i < notificationsLength; i++) {
//                String item = arrayNotifications.optString(i);
//                if (null != item) {
//                    this.notifications.add(item);
//                }
//            }
//        }
//        else {
//            String item = json.optString("notifications");
//            if (null != item) {
//                this.notifications.add(item);
//            }
//        }
//
//
//        this.stepMessages = new ArrayList<StepMessages>();
//        JSONArray arrayStepMessages = json.optJSONArray("step_messages");
//        if (null != arrayStepMessages) {
//            int stepMessagesLength = arrayStepMessages.length();
//            for (int i = 0; i < stepMessagesLength; i++) {
//                JSONObject item = arrayStepMessages.optJSONObject(i);
//                if (null != item) {
//                    this.stepMessages.add(new StepMessages(item));
//                }
//            }
//        }
//        else {
//            JSONObject item = json.optJSONObject("step_messages");
//            if (null != item) {
//                this.stepMessages.add(new StepMessages(item));
//            }
//        }
//
//        this.mapNumSearchPaths = json.optDouble("map_num_search_paths");
//
//        this.nodes = new ArrayList<Nodes>();
//        JSONArray arrayNodes = json.optJSONArray("nodes");
//        if (null != arrayNodes) {
//            int nodesLength = arrayNodes.length();
//            for (int i = 0; i < nodesLength; i++) {
//                JSONObject item = arrayNodes.optJSONObject(i);
//                if (null != item) {
//                    this.nodes.add(new Nodes(item));
//                }
//            }
//        }
//        else {
//            JSONObject item = json.optJSONObject("nodes");
//            if (null != item) {
//                this.nodes.add(new Nodes(item));
//            }
//        }
//
//        this.devices = new ArrayList<Devices>();
//        JSONArray arrayDevices = json.optJSONArray("devices");
//        if (null != arrayDevices) {
//            int devicesLength = arrayDevices.length();
//            for (int i = 0; i < devicesLength; i++) {
//                JSONObject item = arrayDevices.optJSONObject(i);
//                if (null != item) {
//                    this.devices.add(new Devices(item, uuidsMap, mapDeviceTypesMap, this.nodes));
//                }
//            }
//        }
//        else {
//            JSONObject item = json.optJSONObject("devices");
//            if (null != item) {
//                this.devices.add(new Devices(item, uuidsMap, mapDeviceTypesMap, this.nodes));
//            }
//        }
//
//        this.promotions = new ArrayList<Promotions>();
//        JSONArray arrayPromotions = json.optJSONArray("promotions");
//        if (null != arrayPromotions) {
//            int promotionsLength = arrayPromotions.length();
//            for (int i = 0; i < promotionsLength; i++) {
//                JSONObject item = arrayPromotions.optJSONObject(i);
//                if (null != item) {
//                    this.promotions.add(new Promotions(item));
//                }
//            }
//        }
//        else {
//            JSONObject item = json.optJSONObject("promotions");
//            if (null != item) {
//                this.promotions.add(new Promotions(item));
//            }
//        }
//
//        this.errorCode = json.optDouble("errorCode");
//        this.mapDirection = json.optDouble("map_direction");
//
//        this.markers = new ArrayList<Markers>();
//        JSONArray arrayMarkers = json.optJSONArray("markers");
//        if (null != arrayMarkers) {
//            int markersLength = arrayMarkers.length();
//            for (int i = 0; i < markersLength; i++) {
//                JSONObject item = arrayMarkers.optJSONObject(i);
//                if (null != item) {
//                    this.markers.add(new Markers(item));
//                }
//            }
//        }
//        else {
//            JSONObject item = json.optJSONObject("markers");
//            if (null != item) {
//                this.markers.add(new Markers(item));
//            }
//        }
//
//
//        this.paths = new ArrayList<Paths>();
//        JSONArray arrayPaths = json.optJSONArray("paths");
//        if (null != arrayPaths) {
//            int pathsLength = arrayPaths.length();
//            for (int i = 0; i < pathsLength; i++) {
//                JSONObject item = arrayPaths.optJSONObject(i);
//                if (null != item) {
//                    this.paths.add(new Paths(item));
//                }
//            }
//        }
//        else {
//            JSONObject item = json.optJSONObject("paths");
//            if (null != item) {
//                this.paths.add(new Paths(item));
//            }
//        }
//
//
//        this.poiCategories = new ArrayList<PoiCategories>();
//        JSONArray arrayPoiCategories = json.optJSONArray("poi_categories");
//        if (null != arrayPoiCategories) {
//            int poiCategoriesLength = arrayPoiCategories.length();
//            for (int i = 0; i < poiCategoriesLength; i++) {
//                JSONObject item = arrayPoiCategories.optJSONObject(i);
//                if (null != item) {
//                    this.poiCategories.add(new PoiCategories(item));
//                }
//            }
//        }
//        else {
//            JSONObject item = json.optJSONObject("poi_categories");
//            if (null != item) {
//                this.poiCategories.add(new PoiCategories(item));
//            }
//        }
//
//        //Areas
//        this.areas = new ArrayList<Areas>();
//        JSONArray arrayAreas = json.optJSONArray("areas");
//        if (null != arrayAreas) {
//            int arrayAreasLength = arrayAreas.length();
//            for (int i = 0; i < arrayAreasLength; i++) {
//                JSONObject item = arrayAreas.optJSONObject(i);
//                if (null != item) {
//                    this.areas.add(new Areas(item));
//                }
//            }
//        }
//        else {
//            JSONObject item = json.optJSONObject("areas");
//            if (null != item) {
//                this.areas.add(new Areas(item));
//            }
//        }
//
//
//        //Brands
//        this.brands = new ArrayList<Brands>();
//        JSONArray arrayBrands = json.optJSONArray("brands");
//        if (null != arrayBrands) {
//            int brandsLength = arrayBrands.length();
//            for (int i = 0; i < brandsLength; i++) {
//                JSONObject item = arrayBrands.optJSONObject(i);
//                if (null != item) {
//                    this.brands.add(new Brands(item));
//                }
//            }
//        }
//        else {
//            JSONObject item = json.optJSONObject("brands");
//            if (null != item) {
//                this.brands.add(new Brands(item));
//            }
//        }
//
//        //Tags
//        this.tags = new ArrayList<Tags>();
//        JSONArray arrayTags = json.optJSONArray("tags");
//        if (null != arrayTags) {
//            int tagsLength = arrayTags.length();
//            for (int i = 0; i < tagsLength; i++) {
//                JSONObject item = arrayTags.optJSONObject(i);
//                this.tags.add(new Tags(item));
//            }
//        }
//        else {
//            JSONObject item = json.optJSONObject("tags");
//            if (null != item) {
//                this.tags.add(new Tags(item));
//            }
//        }
//
//        //GateIds
//        this.gateIds = new ArrayList<Integer>();
//        JSONArray arrayGateIds = json.optJSONArray("gate_ids");
//        if (null != arrayGateIds) {
//            int gateIdsLength = arrayGateIds.length();
//            for (int i = 0; i < gateIdsLength; i++) {
//                int item = arrayGateIds.optInt(i);
//                this.gateIds.add(item);
//            }
//        }
//        else {
//            int item = json.optInt("gate_ids");
//            this.gateIds.add(item);
//        }
//
//        //Languages
//        this.languages = new ArrayList<Languages>();
//        JSONArray arrayLanguages = json.optJSONArray("languages");
//        if (null != arrayLanguages) {
//            int languagesLength = arrayLanguages.length();
//            for (int i = 0; i < languagesLength; i++) {
//                JSONObject item = arrayLanguages.optJSONObject(i);
//                if (null != item) {
//                    this.languages.add(new Languages(item));
//                }
//            }
//        }
//        else {
//            JSONObject item = json.optJSONObject("languages");
//            if (null != item) {
//                this.languages.add(new Languages(item));
//            }
//        }
//
//        //LastStepMessages
//        this.lastStepMessages = new ArrayList<LastStepMessage>();
//        JSONArray arrayLastStepMessages = json.optJSONArray("last_step_message");
//        if (null != arrayLastStepMessages) {
//            int lastStepMessagesLength = arrayLastStepMessages.length();
//            for (int i = 0; i < lastStepMessagesLength; i++) {
//                JSONObject item = arrayLastStepMessages.optJSONObject(i);
//                if (null != item) {
//                    this.lastStepMessages.add(new LastStepMessage(item));
//                }
//            }
//        }
//        else {
//            JSONObject item = json.optJSONObject("last_step_message");
//            if (null != item) {
//                this.lastStepMessages.add(new LastStepMessage(item));
//            }
//        }
//
//        this.tileUrlPrefix = json.optString("tile_url_prefix");
//        this.mapLng = json.optDouble("map_lng");
//
//        this.customStepMessages = new ArrayList<CustomStepMessages>();
//        JSONArray arrayCustomStepMessages = json.optJSONArray("custom_step_messages");
//        if (null != arrayCustomStepMessages) {
//            int customStepMessagesLength = arrayCustomStepMessages.length();
//            for (int i = 0; i < customStepMessagesLength; i++) {
//                JSONObject item = arrayCustomStepMessages.optJSONObject(i);
//                if (null != item) {
//                    this.customStepMessages.add(new CustomStepMessages(item));
//                }
//            }
//        }
//        else {
//            JSONObject item = json.optJSONObject("custom_step_messages");
//            if (null != item) {
//                this.customStepMessages.add(new CustomStepMessages(item));
//            }
//        }
//
//        this.mapLat = json.optDouble("map_lat");
//
//        this.deviceGroups = new ArrayList<DeviceGroups>();
//        JSONArray arrayDeviceGroups = json.optJSONArray("device_groups");
//        if (null != arrayDeviceGroups) {
//            int deviceGroupsLength = arrayDeviceGroups.length();
//            for (int i = 0; i < deviceGroupsLength; i++) {
//                JSONObject item = arrayDeviceGroups.optJSONObject(i);
//                if (null != item) {
//                    this.deviceGroups.add(new DeviceGroups(item));
//                }
//            }
//        }
//        else {
//            JSONObject item = json.optJSONObject("device_groups");
//            if (null != item) {
//                this.deviceGroups.add(new DeviceGroups(item));
//            }
//        }
//
//    }
    
    public double getMapScale() {
        return this.mapScale;
    }

    public void setMapScale(double mapScale) {
        this.mapScale = mapScale;
    }

    public double getMapNumRecentSearch() {
        return this.mapNumRecentSearch;
    }

    public void setMapNumRecentSearch(double mapNumRecentSearch) {
        this.mapNumRecentSearch = mapNumRecentSearch;
    }

    public ArrayList<MapDeviceTypes> getMapDeviceTypes() {
        return this.mapDeviceTypes;
    }

    public void setMapDeviceTypes(ArrayList<MapDeviceTypes> mapDeviceTypes) {
        this.mapDeviceTypes = mapDeviceTypes;
    }

    public ArrayList<Maps> getMaps() {
        return this.maps;
    }

    public void setMaps(ArrayList<Maps> maps) {
        this.maps = maps;
    }

    public ArrayList<PathTypes> getPathTypes() {
        return this.pathTypes;
    }

    public void setPathTypes(ArrayList<PathTypes> pathTypes) {
        this.pathTypes = pathTypes;
    }

    public ArrayList<Uuids> getUuids() {
        return this.uuids;
    }

    public void setUuids(ArrayList<Uuids> uuids) {
        this.uuids = uuids;
    }

    public ArrayList<Pois> getPois() {
        return this.pois;
    }

    public void setPois(ArrayList<Pois> pois) {
        this.pois = pois;
    }

    public ArrayList<String> getNotifications() {
        return this.notifications;
    }

    public void setNotifications(ArrayList<String> notifications) {
        this.notifications = notifications;
    }

    public ArrayList<StepMessages> getStepMessages() {
        return this.stepMessages;
    }

    public void setStepMessages(ArrayList<StepMessages> stepMessages) {
        this.stepMessages = stepMessages;
    }

    public double getMapNumSearchPaths() {
        return this.mapNumSearchPaths;
    }

    public void setMapNumSearchPaths(double mapNumSearchPaths) {
        this.mapNumSearchPaths = mapNumSearchPaths;
    }

    public ArrayList<Devices> getDevices() {
        return this.devices;
    }

    public void setDevices(ArrayList<Devices> devices) {
        this.devices = devices;
    }

    public ArrayList<Nodes> getNodes() {
        return this.nodes;
    }

    public void setNodes(ArrayList<Nodes> nodes) {
        this.nodes = nodes;
    }

    public ArrayList<Promotions> getPromotions() {
        return this.promotions;
    }

    public void setPromotions(ArrayList<Promotions> promotions) {
        this.promotions = promotions;
    }

    public double getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(double errorCode) {
        this.errorCode = errorCode;
    }

    public double getMapDirection() {
        return this.mapDirection;
    }

    public void setMapDirection(double mapDirection) {
        this.mapDirection = mapDirection;
    }

    public ArrayList<Markers> getMarkers() {
        return this.markers;
    }

    public void setMarkers(ArrayList<Markers> markers) {
        this.markers = markers;
    }

    public ArrayList<Paths> getPaths() {
        return this.paths;
    }

    public void setPaths(ArrayList<Paths> paths) {
        this.paths = paths;
    }

    public ArrayList<PoiCategories> getPoiCategories() {
        return this.poiCategories;
    }

    public void setPoiCategories(ArrayList<PoiCategories> poiCategories) {
        this.poiCategories = poiCategories;
    }

    public ArrayList<Areas> getAreas() {
        return this.areas;
    }

    public void setAreas(ArrayList<Areas> areas) {
        this.areas = areas;
    }

    public ArrayList<Brands> getBrands() {
        return this.brands;
    }

    public void setBrands(ArrayList<Brands> brands) {
        this.brands = brands;
    }

    public ArrayList<Tags> getTags() {
        return this.tags;
    }

    public void setTags(ArrayList<Tags> tags) {
        this.tags = tags;
    }

    public ArrayList<Integer> getGateIds() {
        return this.gateIds;
    }

    public void setGateIds(ArrayList<Integer> gateIds) {
        this.gateIds = gateIds;
    }

    public ArrayList<Languages> getLanguages() {
        return this.languages;
    }

    public void setLanguages(ArrayList<Languages> languages) {
        this.languages = languages;
    }

    public ArrayList<LastStepMessage> getLastStepMessages() {
        return this.lastStepMessages;
    }

    public void setLastStepMessages(ArrayList<LastStepMessage> lastStepMessages) {
        this.lastStepMessages = lastStepMessages;
    }

    public String getTileUrlPrefix() {
        return this.tileUrlPrefix;
    }

    public void setTileUrlPrefix(String tileUrlPrefix) {
        this.tileUrlPrefix = tileUrlPrefix;
    }

    public double getMapLng() {
        return this.mapLng;
    }

    public void setMapLng(double mapLng) {
        this.mapLng = mapLng;
    }

    public ArrayList<CustomStepMessages> getCustomStepMessages() {
        return this.customStepMessages;
    }

    public void setCustomStepMessages(ArrayList<CustomStepMessages> customStepMessages) {
        this.customStepMessages = customStepMessages;
    }

    public double getMapLat() {
        return this.mapLat;
    }

    public void setMapLat(double mapLat) {
        this.mapLat = mapLat;
    }

    public ArrayList<Integer> getAosChangeFloorFilters() {
        return aosChangeFloorFilters;
    }

    public ArrayList<DeviceGroups> getDeviceGroups() {
        return deviceGroups;
    }

    public int getPushMessageTimeout() {
        return pushMessageTimeout;
    }

    public int getZoneLoggingTimeout() {
        return zoneLoggingTimeout;
    }

    public int getZoneDidChangeLoggingTimeout() {
        return zoneDidChangeLoggingTimeout;
    }

    public ArrayList<Zones> getZones() {
        return zones;
    }

    public ArrayList<AaZones> getAaZones() {
        return aaZones;
    }

    public double getMapDefaultZoom() {
        return mapDefaultZoom;
    }
}
