package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;

import org.json.*;
import java.util.ArrayList;

public class Markers {

    @SerializedName("status")
    private boolean status;
    @SerializedName("map_id")
    private double mapId;
    @SerializedName("x")
    private double x;
    @SerializedName("id")
    private double id;
    @SerializedName("y")
    private double y;
    @SerializedName("content")
    private ArrayList<Content> content;
    @SerializedName("z")
    private double z;
    @SerializedName("image")
    private String image;
    @SerializedName("map_zoom_max")
    private double mapZoomMax;
    @SerializedName("map_zoom_min")
    private double mapZoomMin;
    
    
	public Markers () {
		
	}	
        
    public Markers (JSONObject json) {
    
        this.status = json.optBoolean("status");
        this.mapId = json.optDouble("map_id");
        this.x = json.optDouble("x");
        this.id = json.optDouble("id");
        this.y = json.optDouble("y");

        this.content = new ArrayList<Content>();
        JSONArray arrayContent = json.optJSONArray("content");
        if (null != arrayContent) {
            int contentLength = arrayContent.length();
            for (int i = 0; i < contentLength; i++) {
                JSONObject item = arrayContent.optJSONObject(i);
                if (null != item) {
                    this.content.add(new Content(item));
                }
            }
        }
        else {
            JSONObject item = json.optJSONObject("content");
            if (null != item) {
                this.content.add(new Content(item));
            }
        }

        this.z = json.optDouble("z");
        this.image = json.optString("image");
        this.mapZoomMax = json.optDouble("map_zoom_max");
        this.mapZoomMin = json.optDouble("map_zoom_min");

    }
    
    public boolean getStatus() {
        return this.status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public double getMapId() {
        return this.mapId;
    }

    public void setMapId(double mapId) {
        this.mapId = mapId;
    }

    public double getX() {
        return this.x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getId() {
        return this.id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public double getY() {
        return this.y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public ArrayList<Content> getContent() {
        return this.content;
    }

    public void setContent(ArrayList<Content> content) {
        this.content = content;
    }

    public double getZ() {
        return this.z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getMapZoomMax() {
        return this.mapZoomMax;
    }

    public void setMapZoomMax(double mapZoomMax) {
        this.mapZoomMax = mapZoomMax;
    }

    public double getMapZoomMin() {
        return this.mapZoomMin;
    }

    public void setMapZoomMin(double mapZoomMin) {
        this.mapZoomMin = mapZoomMin;
    }


    
}
