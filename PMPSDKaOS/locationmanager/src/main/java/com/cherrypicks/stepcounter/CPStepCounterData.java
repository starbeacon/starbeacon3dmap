package com.cherrypicks.stepcounter;

class CPStepCounterData {

	float value;
	long timeInMillis;
	
	public CPStepCounterData() {
		// TODO Auto-generated constructor stub
		this.value = 0.0f;
		this.timeInMillis = 0;
	}
	
	public CPStepCounterData(float value, long timeInMillis) {
		this.value = value;
		this.timeInMillis = timeInMillis;
	}
}
