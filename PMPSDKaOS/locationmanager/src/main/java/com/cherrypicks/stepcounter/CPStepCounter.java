package com.cherrypicks.stepcounter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

/**
 *  A step counter object.
 *
 *  @author Chiu
 */
public final class CPStepCounter {

    // Broadcast name
    /**
     * Broadcast Action: Broadcast when the step count is changed.
     * <p>Constant Value: "com.cherrypicks.stepcounter.stepCountUpdate"
     */
    public static final String ACTION_STEP_COUNT_UPDATE = "com.cherrypicks.stepcounter.stepCountUpdate";
    /**
     * Broadcast Action: Broadcast when user start or stop walking.
     * <p>Constant Value: "com.cherrypicks.stepcounter.stepCountUpdate"
     */
    public static final String ACTION_IS_WALKING_UPDATE = "com.cherrypicks.stepcounter.isWalkingUpdate";

    // Public method
    /**
     *	Get the library version.
     *
     * 	@return the version number string.
     */
    public static String getVersion() {
        return version;
    }

    /**
     *  Get the CPStepCounter singleton.
     *
     *	@param context the application context
     *
     *  @return the CPStepCounter singleton.
     */
    public synchronized static CPStepCounter getInstance(Context context) {
        if(sharedInstance == null){
            sharedInstance = new CPStepCounter(context);
        }

        return sharedInstance;
    }

    /**
     *  Returns a boolean indicate that the step counter is started or not.
     *
     *	@return true if step counter is started, false if is stopped.
     */
    public boolean isOn() {
        return sensor.isOn();
    }

    /**
     *  Return a boolean indicate that the user is walking or not.
     *
     *	@return true if user is walking, false if is not walking.
     */
    public boolean isWalking() {
        return sensor.isWalking();
    }

    /**
     *  Get step count.
     *
     *  @return the number of step count.
     */
    public int getStepCount() {
        return sensor.getStepCount();
    }

    // Private variables
    private static final String version = "1.1d";
    private static CPStepCounter sharedInstance;
    private CPStepSensor sensor;
    private Context context;
    private StepCountUpdateReceiver stepCountUpdateReceiver = new StepCountUpdateReceiver();
    private IsWalkingUpdateReceiver isWalkingUpdateReceiver = new IsWalkingUpdateReceiver();
    private boolean stepperStarted = false;
    // Constructor
    private CPStepCounter(Context context) {
        this.context = context;

        sensor = new CPStepSensor(context);
    }

    // Start method
    /**
     *  Start the step counter.
     */
    public void start() {
        if (!stepperStarted) {
            stepperStarted = true;
            sensor.start();
            LocalBroadcastManager.getInstance(context).registerReceiver(stepCountUpdateReceiver, new IntentFilter(CPStepSensor.ACTION_STEP_SENSOR_STEP_COUNT_UPDATE));
            LocalBroadcastManager.getInstance(context).registerReceiver(isWalkingUpdateReceiver, new IntentFilter(CPStepSensor.ACTION_STEP_SENSOR_IS_WALKING_UPDATE));
        }
    }

    // Stop method
    /**
     * Stop the step counter.
     */
    public void stop() {
        if (stepperStarted) {
            stepperStarted = false;
            sensor.stop();
            LocalBroadcastManager.getInstance(context).unregisterReceiver(stepCountUpdateReceiver);
            LocalBroadcastManager.getInstance(context).unregisterReceiver(isWalkingUpdateReceiver);
        }
    }

    // Reset method
    /**
     *  Reset the step count to 0.
     */
    public void reset() {
        sensor.reset();
    }

    private class StepCountUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!intent.getAction().equals(CPStepSensor.ACTION_STEP_SENSOR_STEP_COUNT_UPDATE)) {
                return;
            }

            Intent broadcastIntent = new Intent(ACTION_STEP_COUNT_UPDATE);
            context.sendBroadcast(broadcastIntent);
        }
    }

    private class IsWalkingUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!intent.getAction().equals(CPStepSensor.ACTION_STEP_SENSOR_IS_WALKING_UPDATE)) {
                return;
            }

            Intent broadcastIntent = new Intent(ACTION_IS_WALKING_UPDATE);
            context.sendBroadcast(broadcastIntent);
        }
    }
}