package com.cherrypicks.stepcounter;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.util.Log;

import java.util.ArrayList;

class CPStepSensor implements SensorEventListener {

	// Broadcast name
	public static final String ACTION_STEP_SENSOR_STEP_COUNT_UPDATE = "stepsensor.stepCountUpdate";
	public static final String ACTION_STEP_SENSOR_IS_WALKING_UPDATE = "stepsensor.isWalkingUpdate";
	
	// Threshold
	final private static float wdWindowSize = 0.8f;
	final private static float wdThreshold = 0.6f;
	final private static float scMovingWindowSize = 0.31f;
	final private static float scPeakWindowSize = 0.59f;
	final private static float scPeakThreshold = 0.0f;
	final private static float fastestWalkingFrequency = 3.0f;
	final private static long deleteTempStepDelay = 800;
	final private static int tempStepThreshold = 5;
	
	// Private variables
	private boolean isOn = false;
	private boolean isWalking = false;
	private int tempStepCount = 0;
	private int realStepCount = 0;
	private SensorManager sensorManager;
	private Context context;
	private float maxSD = 0.0f;
	private boolean stepCountEnable = true;
	private CPStepCounterData lastStep = new CPStepCounterData(0.0f, 0);
	private CPStepCounterData tentativeStep = null;
	private ArrayList<CPStepCounterData> rawAccListForWD, rawAccListForSC, movingAvgList;
	private boolean pendingDelete = false;
	private Handler deleteTempStepHandler = null;
	private Runnable deleteTempStepRunnable = new Runnable() {
		@Override
		public void run() {
			// TODO Auto-generated method stub
			deleteTempStep();
			pendingDelete = false;
		}
	};
	
	// Constructor
	public CPStepSensor(Context context) {
		this.context = context;
		
		deleteTempStepHandler = new Handler();
		rawAccListForWD = new ArrayList<CPStepCounterData>();
		rawAccListForSC = new ArrayList<CPStepCounterData>();
		movingAvgList = new ArrayList<CPStepCounterData>();
		
		sensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
	}
	
	// Public method
	// Get isOn
	public boolean isOn() {
		return isOn;
	}
	
	// Get isWalking
	public boolean isWalking() {
		return isWalking;
	}

	// Get step count
	public int getStepCount() {
		int stepCount = realStepCount;
		if (tempStepCount >= (tempStepThreshold + Math.round(maxSD))) {
			stepCount += tempStepCount;
		}
		
		return stepCount;
	}
	
	// Start method
	public void start() {		
		if (!isOn) {
			// Setup value
			isOn = true;
			
			// Setup sensor manager
			registerListener();
		}
	}

	// Stop method
	public void stop() {		
		isOn = false;
		// Unregister sensor manager
		unregisterListener();
	}
	
	// Reset method
	public void reset() {		
		// Reset counter
		stop();
		isWalking = false;
		tempStepCount = 0;
		realStepCount = 0;
		maxSD = 0.0f;
		stepCountEnable = true;
		pendingDelete = false;
		lastStep = new CPStepCounterData(0.0f, 0);
		
		// Clear array
		rawAccListForWD.clear();
		rawAccListForSC.clear();
		movingAvgList.clear();
		
		start();
		// Broadcast
		Intent stepCountIntent = new Intent(ACTION_STEP_SENSOR_STEP_COUNT_UPDATE);
		context.sendBroadcast(stepCountIntent);
		Intent isWalkingIntent = new Intent(ACTION_STEP_SENSOR_IS_WALKING_UPDATE);
		context.sendBroadcast(isWalkingIntent);
	}
	
	// Register listener
    private void registerListener() {
		Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    // Unregister listener
    private void unregisterListener() {
        sensorManager.unregisterListener(this);
    }

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() != Sensor.TYPE_ACCELEROMETER) {
			return;
		}
		// Get acceleration
		float x = event.values[0];
		float y = event.values[1];
		float z = event.values[2];
    
        float accelationSquareRoot =  (float) Math.sqrt(x * x + y * y + z * z);
        long currentTime = System.currentTimeMillis();
        
        processNewAccelerationData(accelationSquareRoot, currentTime);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		
	}

	// Process acceleration data
	private void processNewAccelerationData(float acceleration, long timeInMillis) {
		// Save raw acceleration
		CPStepCounterData rawAcc = new CPStepCounterData(acceleration, timeInMillis);
		
		// Calculate SD
		rawAccListForWD.add(rawAcc);
		removeExpiredElement(rawAccListForWD, wdWindowSize);
		float sd = standardDeviationFromArray(rawAccListForWD, wdWindowSize);
		
		if (sd > maxSD) {
			maxSD = sd;
		}
		
		// Calculate moving average acceleration
		rawAccListForSC.add(rawAcc);
		removeExpiredElement(rawAccListForSC, scMovingWindowSize);
		CPStepCounterData movingAvg = new CPStepCounterData(meanFromArray(rawAccListForWD, scMovingWindowSize), timeInMillis);
		
		// Save moving average
		movingAvgList.add(movingAvg);
		removeExpiredElement(movingAvgList, scPeakWindowSize);
		
		// Walking mode change
		if (isWalking != (sd >= wdThreshold)) {
			isWalking = (sd >= wdThreshold);
			
//			// Broadcast
			Intent intent = new Intent(ACTION_STEP_SENSOR_IS_WALKING_UPDATE);
			context.sendBroadcast(intent);
		}
		
		// Count step
		if (isWalking) {
			
			pendingDelete = false;
			deleteTempStepHandler.removeCallbacks(deleteTempStepRunnable);
			
			countStep();
		} else if (tempStepCount > 0) {
			if (!pendingDelete) {
				pendingDelete = true;
				deleteTempStepHandler.postDelayed(deleteTempStepRunnable, deleteTempStepDelay);
			}
		}
	}
	
	// Count step
	private void countStep() {
		float threshold = meanFromArray(movingAvgList, scPeakWindowSize)+scPeakThreshold;
		CPStepCounterData lastMovingAvg = movingAvgList.get(movingAvgList.size()-1);

		if (stepCountEnable) {
			if (lastMovingAvg.value >= threshold) {
				tentativeStep = lastMovingAvg;
				stepCountEnable = false;
			}
		} else {
			if (lastMovingAvg.value >= threshold) {
				if (lastMovingAvg.value >= tentativeStep.value) {
					tentativeStep = lastMovingAvg;
				}
			} else {
				if (tentativeStep.timeInMillis - lastStep.timeInMillis >= 1000.0 * 1.0/fastestWalkingFrequency) {
					tempStepCount++;
					lastStep = tentativeStep;	
					
					// Broadcast
					Intent intent = new Intent(ACTION_STEP_SENSOR_STEP_COUNT_UPDATE);
					context.sendBroadcast(intent);
				}
				stepCountEnable = true;
			}
		}
	}

	// Delete temp step
	private void deleteTempStep() {		
		if (tempStepCount >= (tempStepThreshold + Math.round(maxSD))) {
			realStepCount += tempStepCount;
		}
		
		tempStepCount = 0;
		maxSD = 0.0f;
		
		// Broadcast
		Intent intent = new Intent(ACTION_STEP_SENSOR_STEP_COUNT_UPDATE);
		context.sendBroadcast(intent);
	}
	
	// Remove expired elements
	private void removeExpiredElement(ArrayList<CPStepCounterData> array, float windowSize) {
		boolean finishDelete = false;
		while (!finishDelete && (array.size() > 0)) {
			if (array.get(array.size()-1).timeInMillis - array.get(0).timeInMillis > windowSize * 1000) {
				array.remove(0);
			} else {
				finishDelete = true;
			}
		}
	}
	
	// Mean from array
	private float meanFromArray(ArrayList<CPStepCounterData> array, float windowSize) {
		
		float sum = 0;
		int count = 0;
		
		if (array.size() > 0) {
			CPStepCounterData lastData = array.get(array.size()-1);
			for (int i = 0; i < array.size(); i++) {
				if (i < array.size()) {
					CPStepCounterData currentData = array.get(i);
					if (lastData.timeInMillis - currentData.timeInMillis < windowSize * 1000) {
						sum += currentData.value;
						count++;
					}	
				} else {
					Log.i("SENSOR", "A MEAN return G");

					return SensorManager.GRAVITY_EARTH;
				}
			}
			
			return sum/count;
		} else {
			Log.i("SENSOR", "B MEAN return G");

			return SensorManager.GRAVITY_EARTH;
		}

	}
	
	// SD from array
	private float standardDeviationFromArray(ArrayList<CPStepCounterData> array, float windowSize) {
		
		float mean = meanFromArray(array, windowSize);
		
		float sum = 0;
		int count = 0;
		
		if (array.size() > 0) {
			CPStepCounterData lastData = array.get(array.size()-1);
			for (int i = 0; i < array.size(); i++) {
				if (i < array.size()) {
					CPStepCounterData currentData = array.get(i);
					if (lastData.timeInMillis - currentData.timeInMillis < windowSize * 1000) {
						sum += Math.pow((currentData.value - mean), 2);
						count++;
					}
				} else {
					
					Log.i("SENSOR", "B SD return 0");
					return 0;
				}
			}

			return (float) Math.sqrt(sum/(count-1));
		} else {
			Log.i("SENSOR", "B SD return 0");

			return 0;
		}
	}
}
