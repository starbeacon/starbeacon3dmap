package com.example.arcameratestapp;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import com.cherrypicks.arsignagecamerasdk.ARSignageTranslationFragment;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        ARSignageTranslationFragment fragment = new ARSignageTranslationFragment();
        fragment.setDebugEnable(true);
        fragment.setLangID(ARSignageTranslationFragment.Language_TraditionalChinese);
        FragmentManager childFragMgr = getSupportFragmentManager();

        FragmentTransaction transaction = childFragMgr.beginTransaction();
        transaction.addToBackStack(null)
                .replace(R.id.fragment_container, fragment)
                .commit();
    }
}
