package com.cherrypicks.arsignagecamerasdk.analytics;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Objects;

/**
 * Created by gordonwong on 10/7/2017.
 */

public class AnalyticsHelper {
    public static void logEvent(String eventName, HashMap<String, String> params, Context context){
        Intent intent = new Intent();
        intent.setAction("com.cherrypicks.arsignagecamerasdk.analytics.LOGEVENT");
        intent.putExtra("event_name",eventName);
        Bundle bundle = new Bundle();
        for(HashMap.Entry<String,String> entry: params.entrySet()){
            bundle.putString(entry.getKey(),entry.getValue());
        }
        intent.putExtra("params",bundle);

        context.sendBroadcast(intent);
    }
}
