package com.cherrypicks.arsignagecamerasdk;

import android.Manifest;
import android.animation.LayoutTransition;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.YuvImage;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cherrypicks.arsignagecamerasdk.customui.CustomAnimationDrawable;
import com.cherrypicks.arsignagecamerasdk.servermanager.ARServerManager;
import com.cherrypicks.arsignagecamerasdk.servermanager.ARServerManagerNotifier;
import com.cherrypicks.arsignagecamerasdk.servermanager.datamodel.ARImageResponse;
import com.cherrypicks.arsignagecamerasdk.utils.CommonUtils;
import com.cherrypicks.arsignagecamerasdk.utils.LanguageUtils;
import com.exif.ExifInterface;
import com.exif.ExifTag;
import com.flurgle.camerakit.CameraKit;
import com.flurgle.camerakit.CameraListener;
import com.flurgle.camerakit.CameraView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


public class ARSignageTranslationFragment extends BaseFragment implements Toolbar.OnMenuItemClickListener {
    public static final int Language_English = LanguageUtils.Language_English;
    public static final int Language_TraditionalChinese = LanguageUtils.Language_TraditionalChinese;
    public static final int Language_SimplifiedChinese = LanguageUtils.Language_SimplifiedChinese;
    public static final int Language_Japanese = LanguageUtils.Language_Janpanese;
    public static final int Language_Korean = LanguageUtils.Language_Korean;

    private static final String TAG = "PMPARSignage";
    private static final int PERMISSION_REQUEST_CODE = 200;
    private final int CELL_HEIGHT = 44;
    private final int LANGUAGE_ARROW_HEIGHT = 14;

    private ArrayList<Triple<Integer, String, String>> supportedLanguage = null;

    private Toolbar toolbar;
    private FrameLayout container, fl_blink;
    private LinearLayout ll_error_view;
    private CameraView camera;
    private ImageButton ibtn_take;
    private RelativeLayout rl_loading, rl_lang_selector, rl_loading_circle, rl_error_view;
    private ImageView iv_loading;
    private boolean isPhotoTaking = false;
    private CustomAnimationDrawable loadingAnimation;
    private int currentSelectID;
    private RelativeLayout rl_open_lang_selector;
    private TextView tv_open_lang_selector, tv_error_title, tv_error_msg;
    private ListView lv_lang;
    private ImageView iv_error_image, iv_result;
    private Button btn_error_ok;

    //Animation related...
    private int currentRotationDegree = 0;
    private int langSelectorOriginalY = -1;
    private int langSelectorOriginalWidth = -1;
    private int langSelectorOriginalHeight = -1;

    //Permission control
    private boolean isPermissionHasAsked = false; //make sure we have one asked for one time...

    //Image handling
    private int minimumPhotoEdgeLength = 1800;

    //debug...
    private boolean isDebugEnable = true;
    private Bitmap lastImage;
    private String lastErrorMsg = "";

    private class Triple<A, B, C> {
        A a;
        B b;
        C c;

        public Triple(A a, B b, C c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public A getFirst() {
            return a;
        }

        public B getSecond() {
            return b;
        }

        public C getLast() {
            return c;
        }
    }

    public void setDebugEnable(boolean debugEnable) {
        isDebugEnable = debugEnable;
    }

    public void setLangID(int langID) {
        LanguageUtils.setLangID(langID);
        if(langID == 4){
            currentSelectID = ARServerManager.TargetLanguage.JANPANESE;
        }else if(langID == 5){
            currentSelectID = ARServerManager.TargetLanguage.KOREAN;
        }else{
            currentSelectID = ARServerManager.TargetLanguage.SIMPLIFIED_CHINESE;
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.loadLibrary("arservermanager");
        setHasOptionsMenu(true);

        supportedLanguage = new ArrayList<Triple<Integer, String, String>>() {{
            add(new Triple<>(ARServerManager.TargetLanguage.SIMPLIFIED_CHINESE, getString(R.string.LANG_CHINESE_LONG), getString(R.string.LANG_CHINESE_SHORT)));
            add(new Triple<>(ARServerManager.TargetLanguage.JANPANESE, getString(R.string.LANG_JAPANESE_LONG), getString(R.string.LANG_JAPANESE_SHORT)));
            add(new Triple<>(ARServerManager.TargetLanguage.KOREAN, getString(R.string.LANG_KOREAN_LONG), getString(R.string.LANG_KOREAN_SHORT)));
        }};

        //default value
//        currentSelectID = ARServerManager.TargetLanguage.SIMPLIFIED_CHINESE;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.pmp_ar_signage_camera_fragment, container, false);
        toolbar = (Toolbar) view.findViewById(R.id.toolbar);

        updateToolbar();
        setUpCamera(view);
        setUpUI(view);
        setUpTutorial(view);

        return view;
    }

    private void setUpUI(View view) {
        LayoutTransition layoutTransition = ((ViewGroup) view).getLayoutTransition();
//        layoutTransition.enableTransitionType(LayoutTransition.CHANGING);

        fl_blink = (FrameLayout) view.findViewById(R.id.fl_blink);
        container = (FrameLayout) view.findViewById(R.id.container);
        ibtn_take = (ImageButton) view.findViewById(R.id.ibtn_take);
        ibtn_take.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPhotoTaking) {
                    return;
                }
                isPhotoTaking = true;
                camera.captureImage();
                showBlink();
                showLoading();
            }
        });

        rl_loading = (RelativeLayout) view.findViewById(R.id.rl_loading);
        TextView tv_blocker = (TextView) view.findViewById(R.id.tv_blocker);
        tv_blocker.setText(LanguageUtils.getLocalizedString(getContext(),R.string.IMAGE_PROCESSING));
        rl_loading.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //prevent touch
                return true;
            }
        });

        getChildFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getChildFragmentManager().getBackStackEntryCount() == 0) {
                    //If no child... it means user nor back from a full screen child
                    onResume();
                }
            }
        });

        rl_loading_circle = (RelativeLayout) view.findViewById(R.id.rl_loading_circle);
        rl_lang_selector = (RelativeLayout) view.findViewById(R.id.rl_lang_selector);
        ViewGroup.LayoutParams params = rl_lang_selector.getLayoutParams();
        params.height = (int) (((supportedLanguage.size() * CELL_HEIGHT) + LANGUAGE_ARROW_HEIGHT) * CommonUtils.getDeviceDensity(getContext()));
        rl_lang_selector.setLayoutParams(params);

        lv_lang = (ListView) view.findViewById(R.id.lv_lang);
        lv_lang.setAdapter(new LanguageAdapter());
        lv_lang.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                currentSelectID = supportedLanguage.get(position).getFirst();
                BaseAdapter adapter = (BaseAdapter) lv_lang.getAdapter();
                adapter.notifyDataSetChanged();
                tv_open_lang_selector.setText(supportedLanguage.get(position).getLast());
                rl_open_lang_selector.performClick();
//                rl_lang_selector.setVisibility(View.GONE);
            }
        });

        rl_open_lang_selector = (RelativeLayout) view.findViewById(R.id.rl_open_lang_selector);
        tv_open_lang_selector = (TextView) view.findViewById(R.id.tv_open_lang_selector);
        tv_open_lang_selector.setText(supportedLanguage.get(currentSelectID).getLast());
        rl_open_lang_selector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rl_lang_selector.getVisibility() == View.VISIBLE) {
                    rl_lang_selector.setVisibility(View.GONE);
                    rl_open_lang_selector.setBackgroundResource(R.drawable.btn_lang_off);
                } else {
                    rl_lang_selector.setVisibility(View.VISIBLE);
                    rl_open_lang_selector.setBackgroundResource(R.drawable.btn_lang_on);
                }
            }
        });
        rl_open_lang_selector.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
//                onUserRotated(currentRotationDegree);
                if (rl_lang_selector.getY() > 0) {
                    if (langSelectorOriginalY <= 0) {
                        langSelectorOriginalY = (int) rl_lang_selector.getY();
                        langSelectorOriginalWidth = rl_lang_selector.getWidth();
                        langSelectorOriginalHeight = rl_lang_selector.getHeight();
                        rl_lang_selector.setVisibility(View.GONE);
                    }
                }
            }
        });

        //error view
        tv_error_title = (TextView) view.findViewById(R.id.tv_error_title);
        tv_error_msg = (TextView) view.findViewById(R.id.tv_error_msg);
        iv_error_image = (ImageView) view.findViewById(R.id.iv_error_image);
        rl_error_view = (RelativeLayout) view.findViewById(R.id.rl_error_view);
        ll_error_view = (LinearLayout) view.findViewById(R.id.ll_error_view);
        btn_error_ok = (Button) view.findViewById(R.id.btn_error_ok);
        btn_error_ok.setText(LanguageUtils.getLocalizedString(getContext(), R.string.TUTORIAL_CONFIRM));
        btn_error_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_error_view.setVisibility(View.GONE);
            }
        });
        rl_error_view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //prevent touch
                return true;
            }
        });

        iv_loading = (ImageView) view.findViewById(R.id.iv_loading);

        Button btn_save = (Button) view.findViewById(R.id.btn_save);
        if (isDebugEnable) {
            btn_save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bitmap mutableBitmap = CommonUtils.addDebugInfoToBitmap(lastImage, lastErrorMsg);

                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
                        if (checkPermission()) {
                            addImageToGallery(mutableBitmap);

                        } else {
                            requestPermission();
                        }
                    } else {
                        addImageToGallery(mutableBitmap);
                    }

                    mutableBitmap.recycle();
                }
            });
        } else {
            btn_save.setVisibility(View.GONE);
        }


        iv_result = (ImageView) view.findViewById(R.id.iv_result);

        if (isDebugEnable) {
            TextView tv_version = (TextView) view.findViewById(R.id.tv_version);
            tv_version.setText("Version:" + BuildConfig.VERSION_NAME + "\n" + BuildConfig.BaseURL);
        }
    }

    private void updateToolbar() {
        toolbar.setTitle(LanguageUtils.getLocalizedString(getContext(), R.string.TITLE));
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.icon_back);
        toolbar.setOnMenuItemClickListener(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getFragmentManager().popBackStack();

            }
        });
        toolbar.inflateMenu(R.menu.ar_main_menu);
    }

    private void setUpCamera(View view) {
        camera = (CameraView) view.findViewById(R.id.camera);
        camera.setFocus(CameraKit.Constants.FOCUS_TAP);
        camera.setPermissions(CameraKit.Constants.PERMISSIONS_PICTURE);
        camera.setMethod(CameraKit.Constants.METHOD_STANDARD);
//        camera.setMethod(CameraKit.Constants.METHOD_STILL);


        camera.setZoom(CameraKit.Constants.ZOOM_PINCH);
//        camera.setCropOutput(true);

        camera.setCameraListener(new CameraListener() {
            @Override
            public void onCameraOpened() {
                super.onCameraOpened();
            }

            @Override
            public void onCameraClosed() {
                super.onCameraClosed();
            }

            public Bitmap getResizedBitmap(Bitmap bm, ExifTag tag, float angle) {
                /**
                 * Constants for {@link TAG_ORIENTATION}. They can be interpreted as
                 * follows:
                 * <ul>
                 * <li>TOP_LEFT is the normal orientation.</li>
                 * <li>TOP_RIGHT is a left-right mirror.</li>
                 * <li>BOTTOM_LEFT is a 180 degree rotation.</li>
                 * <li>BOTTOM_RIGHT is a top-bottom mirror.</li>
                 * <li>LEFT_TOP is mirrored about the top-left<->bottom-right axis.</li>
                 * <li>RIGHT_TOP is a 90 degree clockwise rotation.</li>
                 * <li>LEFT_BOTTOM is mirrored about the top-right<->bottom-left axis.</li>
                 * <li>RIGHT_BOTTOM is a 270 degree clockwise rotation.</li>
                 * </ul>
                 */
                int orientation = ExifInterface.Orientation.TOP_LEFT;
                if (tag != null)
                    orientation = tag.getValueAsInt(ExifInterface.Orientation.TOP_LEFT);
                Matrix matrix = new Matrix();
                switch (orientation) {
                    case ExifInterface.Orientation.TOP_LEFT:
//                        return bitmap;
                        break;
                    case ExifInterface.Orientation.TOP_RIGHT:
                        matrix.setScale(-1, 1);
                        break;
                    case ExifInterface.Orientation.BOTTOM_LEFT:
                        matrix.setRotate(180);
                        break;
                    case ExifInterface.Orientation.BOTTOM_RIGHT:
                        matrix.setRotate(180);
                        matrix.postScale(-1, 1);
                        break;
                    case ExifInterface.Orientation.LEFT_TOP:
                        matrix.setRotate(90);
                        matrix.postScale(-1, 1);
                        break;
                    case ExifInterface.Orientation.RIGHT_TOP:
                        matrix.setRotate(90);
                        break;
                    case ExifInterface.Orientation.LEFT_BOTTOM:
                        matrix.setRotate(-90);
                        matrix.postScale(-1, 1);
                        break;
                    case ExifInterface.Orientation.RIGHT_BOTTOM:
                        matrix.setRotate(-90);
                        break;
                    default:
                }

                matrix.postRotate(angle);

                int width = bm.getWidth();
                int height = bm.getHeight();

                int targetPixel = 1800;
                int maxLength = Math.max(width, height);
                if (maxLength < targetPixel) {
                    return bm;
                }

                float ratio = (float) targetPixel / (float) maxLength;

                float scaleWidth = ratio;
                float scaleHeight = ratio;
                // CREATE A MATRIX FOR THE MANIPULATION
                // RESIZE THE BIT MAP
                matrix.postScale(scaleWidth, scaleHeight);

                // "RECREATE" THE NEW BITMAP
                Bitmap resizedBitmap = Bitmap.createBitmap(
                        bm, 0, 0, width, height, matrix, false);
                bm.recycle();
                return resizedBitmap;
            }

            public Bitmap rotateImage(Bitmap source, float angle) {
                Matrix matrix = new Matrix();

                int targetPixel = 1800;

                if (source.getWidth() > targetPixel || source.getHeight() > targetPixel) {
                    int sourceLenght = Math.max(source.getWidth(), source.getHeight());
                    double ratio = (double) sourceLenght / (double) targetPixel;
                    matrix.setRectToRect(new RectF(0, 0, source.getWidth(), source.getHeight()), new RectF(0, 0, (float) (source.getWidth() / ratio), (float) (source.getHeight() / ratio)), Matrix.ScaleToFit.CENTER);

                }
                matrix.postRotate(angle);
//                Exif

                Bitmap newBitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                        matrix, true);
                return newBitmap;
            }

            private void sendImageToServer(byte[] jpeg, int currentRotationDegree) {
                final long startTime = System.currentTimeMillis();

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeByteArray(jpeg, 0, jpeg.length, options);

                int sampleSize = 1;
                int height = options.outHeight;
                int width = options.outWidth;

                while (height / 2 >= minimumPhotoEdgeLength || width /  2 >= minimumPhotoEdgeLength){
                    sampleSize *= 2;
                    height /= 2;
                    width /= 2;
                }

                options.inSampleSize = sampleSize;
                options.inJustDecodeBounds = false;
                Bitmap bitmap;
                try {
                    bitmap = BitmapFactory.decodeByteArray(jpeg, 0, jpeg.length, options);
                    if (iv_result.getDrawable() != null && iv_result.getDrawable() instanceof BitmapDrawable) {
                        if (((BitmapDrawable) iv_result.getDrawable()).getBitmap() != null) {
                            ((BitmapDrawable) iv_result.getDrawable()).getBitmap().recycle();
                        }
                    }
                }catch (OutOfMemoryError e){
                    CommonUtils.showDialog(getActivity(), "Error", "Failed to process the image!");
                    return;
                }
//                iv_result.setImageBitmap(bitmap);

//                ByteArrayInputStream bs = new ByteArrayInputStream(jpeg);
                ExifInterface exif = new ExifInterface();
                try {
                    exif.readExif(jpeg);
                } catch (IOException e) {
                    e.printStackTrace();
                }
//                ExifInterface exif = new ExifInterface(bs);
//                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
//                int orientation = exif.getTagIntValue(ExifInterface.TAG_ORIENTATION, ExifInterface.Orientation.LEFT_TOP);
                ExifTag tag = exif.getTag(ExifInterface.TAG_ORIENTATION);

//                Bitmap resultBitmap = rotateImage(bitmap, -currentRotationDegree);
                Bitmap resultBitmap = getResizedBitmap(bitmap, tag, -currentRotationDegree);
//                iv_result.setImageBitmap(resultBitmap);

//                Bitmap resultBitmap = bitmap;

                bitmap = null;
                Log.i("test", "test bitmap Width = " + resultBitmap.getWidth() + " Height=" + resultBitmap.getHeight());

                final ByteArrayOutputStream stream = new ByteArrayOutputStream();
                resultBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                if (lastImage != null) {
                    lastImage.recycle();
                }
                lastImage = resultBitmap;

//                ByteArrayOutputStream bos = new ByteArrayOutputStream();
//                bmp.compress(CompressFormat.JPEG, 70, bos);

                ARServerManager.getShared(getContext()).sendImage(currentSelectID, byteArray, new ARServerManagerNotifier() {
                    @Override
                    public void didSuccess(final ARImageResponse responseData) {
                        if (isDetached()) {
                            return;
                        }

                        lastErrorMsg = responseData.getErrorMessage();
                        long endTime = System.currentTimeMillis();
                        final long diff = (endTime - startTime);
                        lastErrorMsg = lastErrorMsg + "\n" + CommonUtils.getDebugMessage(diff, responseData.getTime())
                                +", score: "+ responseData.getScore() + ", signageId: " + responseData.getSignageId();

                        if (responseData.getErrorCode() == 0) {
                            showLoadingFinish(new CustomAnimationDrawable.IAnimationFinishListener() {
                                @Override
                                public void onAnimationFinished() {
                                    stopLoading();

                                    byte[] imageData = Base64.decode(responseData.getResult(), Base64.DEFAULT);
                                    PMPARSignageResultFragment fragment = new PMPARSignageResultFragment();
                                    fragment.setImageData(imageData);
                                    fragment.setDebugEnable(isDebugEnable);
                                    fragment.setDebugOriImage(lastImage);
                                    fragment.setErrorMessage(lastErrorMsg);
                                    fragment.setServerProcessTime(responseData.getTime());
                                    fragment.setTotalProcessTime(diff);
                                    fragment.setScore(responseData.getScore());
                                    fragment.setSignageId(responseData.getSignageId());
                                    FragmentManager childFragMgr = getChildFragmentManager();

                                    FragmentTransaction transaction = childFragMgr.beginTransaction();
                                    transaction.addToBackStack(null)
                                            .replace(R.id.container, fragment)
                                            .commit();
                                }
                            });

                        } else {
                            stopLoading();

                            showErrorView(R.string.ERROR_VIEW_TITLE, R.string.ERROR_VIEW_MSG);
                            if (isDebugEnable) {
                                CommonUtils.showDialog(getActivity(), "Error", "" + responseData.getErrorMessage());
                            }

                            openCamera();
                        }

                        isPhotoTaking = false;

                    }

                    @Override
                    public void didFailure() {
                        if (this == null || isDetached()) {
                            return;
                        }
                        showErrorView(R.string.ERROR_VIEW_TITLE, R.string.ERROR_VIEW_MSG);

                        if (isDebugEnable) {
                            CommonUtils.showDialog(getActivity(), "", LanguageUtils.getLocalizedString(getContext(),R.string.ERROR_MSG_INTERNET));
                        }

                        openCamera();
                        isPhotoTaking = false;
                        stopLoading();

                    }
                });

            }

            @Override
            public void onPictureTaken(final byte[] jpeg) {
                super.onPictureTaken(jpeg);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        camera.stop();
                        sendImageToServer(jpeg, currentRotationDegree);
                    }
                });
            }

            @Override
            public void onPictureTaken(YuvImage yuv) {
                super.onPictureTaken(yuv);
            }

            @Override
            public void onVideoTaken(File video) {
                super.onVideoTaken(video);
            }
        });

    }

    private void showBlink() {
        Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.blink);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                fl_blink.setVisibility(View.VISIBLE);
                fl_blink.setBackgroundColor(getResources().getColor(R.color.white));
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                fl_blink.setVisibility(View.GONE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        fl_blink.startAnimation(anim);
    }

    private void showLoading() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadingAnimation = new CustomAnimationDrawable((AnimationDrawable) getResources().getDrawable(R.drawable.loading_image_1));
                loadingAnimation.setAnimationFinishListener(new CustomAnimationDrawable.IAnimationFinishListener() {
                    @Override
                    public void onAnimationFinished() {
                        loadingAnimation.stop();
                        loadingAnimation = new CustomAnimationDrawable((AnimationDrawable) getResources().getDrawable(R.drawable.loading_image_2));
                        iv_loading.setImageDrawable(loadingAnimation);
                        loadingAnimation.start();
                    }
                });

                iv_loading.setImageDrawable(loadingAnimation);

                rl_loading.setVisibility(View.VISIBLE);

                loadingAnimation.start();

            }
        });

    }

    private void stopLoading() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                rl_loading.setVisibility(View.GONE);
                loadingAnimation.stop();
            }
        });
    }

    private void showLoadingFinish(final CustomAnimationDrawable.IAnimationFinishListener listener) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadingAnimation = new CustomAnimationDrawable((AnimationDrawable) getResources().getDrawable(R.drawable.loading_image_3));
                loadingAnimation.setAnimationFinishListener(listener);

                iv_loading.setImageDrawable(loadingAnimation);

                rl_loading.setVisibility(View.VISIBLE);
                loadingAnimation.start();

            }
        });
    }

    private void showErrorView(int titleResID, int msgResID) {
        String msg = LanguageUtils.getLocalizedString(getContext(), msgResID);
        String title = LanguageUtils.getLocalizedString(getContext(), titleResID);
        tv_error_msg.setText(msg);
        tv_error_title.setText(title);
        rl_error_view.setVisibility(View.VISIBLE);
    }

    private void hideErrorView() {
        rl_error_view.setVisibility(View.GONE);
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

//                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (cameraAccepted)
                        addImageToGallery(lastImage);
//                        Toast.makeText(getApplicationContext(), "Permission Granted, Now you can access location data and camera", Toast.LENGTH_LONG).show();
                    else {
//                        Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access location data and camera", Toast.LENGTH_LONG).show();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                            if (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
//
//                                return;
//                            }
                        }

                    }
                }

                break;
        }
    }

    private void openCamera() {
        iv_result.setImageBitmap(null);
        camera.start();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.ar_main_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == R.id.tutorial_item) {
            showTutorial();
//            showErrorView(getString(R.string.TUTORIAL_VIEW_TITLE), getString(R.string.TUTORIAL_VIEW_MSG));
            return true;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        int cameraCheck = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA);

        if(cameraCheck == PackageManager.PERMISSION_GRANTED){
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            if(preferences.getInt("firstTimeLaunch",0) == 0) {
                showTutorial();
                preferences.edit().putInt("firstTimeLaunch",1).commit();
            }
        }

        if (cameraCheck == PackageManager.PERMISSION_GRANTED || (cameraCheck != PackageManager.PERMISSION_GRANTED && !isPermissionHasAsked)) {
            isPermissionHasAsked = true;
            openCamera();
        }else{
            CommonUtils.showDialog(getActivity(), LanguageUtils.getLocalizedString(getContext(),R.string.ERROR_CANT_OPEN_CAMERA_TITLE), LanguageUtils.getLocalizedString(getContext(),R.string.ERROR_CANT_OPEN_CAMERA_MSG));
            getFragmentManager().popBackStack();
        }

//        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
//        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
//            if (checkPermission()) {
//                openCamera();
//            } else {
//                requestPermission();
//            }
//        }else{
//            openCamera();
//        }

    }

    protected void onUserRotated(int rotationDegree) {
        super.onUserRotated(rotationDegree);
        currentRotationDegree = rotationDegree;

        int duration = 100;

        rl_open_lang_selector.animate().rotation(rotationDegree).setDuration(duration).start();
        rl_loading_circle.animate().rotation(rotationDegree).setDuration(duration).start();
        ll_error_view.animate().rotation(rotationDegree).setDuration(duration).start();

        //make sure rl_lang_selector is init
        if (rl_lang_selector.getY() > 0) {
//            if(langSelectorOriginalY <= 0){
//                langSelectorOriginalY = (int) rl_lang_selector.getY();
//                langSelectorOriginalWidth = rl_lang_selector.getWidth();
//                langSelectorOriginalHeight = rl_lang_selector.getHeight();
//            }

            lv_lang.animate().rotation(rotationDegree).setDuration(duration).start();

            int diffY = (int) (30 * CommonUtils.getDeviceDensity(getContext()));
            switch (currentOrientation) {
                case Surface.ROTATION_0:
                    ViewGroup.LayoutParams params = rl_lang_selector.getLayoutParams();
                    params.width = langSelectorOriginalWidth;
                    params.height = langSelectorOriginalHeight;
                    rl_lang_selector.setLayoutParams(params);
//                    rl_lang_selector.setY(langSelectorOriginalY);
                    rl_lang_selector.animate().translationY(0);

                    break;
                case Surface.ROTATION_90:
                case Surface.ROTATION_180:
                case Surface.ROTATION_270:
//                    rl_lang_selector.setY(langSelectorOriginalY - (langSelectorOriginalWidth - langSelectorOriginalHeight) - 30 * CommonUtils.getDeviceDensity(getContext()));
                    ViewGroup.LayoutParams params1 = rl_lang_selector.getLayoutParams();
                    params1.width = langSelectorOriginalHeight;
                    params1.height = langSelectorOriginalWidth;
                    rl_lang_selector.setLayoutParams(params1);
                    rl_lang_selector.animate().translationY(-diffY);

                    break;
            }
        }

        //update errorView
        switch (currentOrientation) {
            case Surface.ROTATION_0:
                iv_error_image.setImageResource(R.drawable.ar_camera_tryagain_portrait_image);
                break;
            case Surface.ROTATION_90:
            case Surface.ROTATION_180:
            case Surface.ROTATION_270:
                iv_error_image.setImageResource(R.drawable.ar_camera_tryagain_landscape_image);
                break;
        }
    }

    @Override
    public void onPause() {
        if (camera != null) {
            camera.stop();
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    private void refreshGallery(File file) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(Uri.fromFile(file));
        getContext().sendBroadcast(mediaScanIntent);
    }


    public void fadeIn(final ImageView v, final int begin_alpha, final int end_alpha, int time, final boolean toggleVisibility) {

        if (Integer.valueOf(android.os.Build.VERSION.SDK_INT) >= android.os.Build.VERSION_CODES.JELLY_BEAN)
            v.setImageAlpha(begin_alpha);
        else
            v.setAlpha(begin_alpha);

        if (toggleVisibility) {
            if (v.getVisibility() == View.GONE)
                v.setVisibility(View.VISIBLE);
            else
                v.setVisibility(View.GONE);
        }

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime,
                                               Transformation t) {
                if (interpolatedTime == 1) {
                    if (Integer.valueOf(android.os.Build.VERSION.SDK_INT) >= android.os.Build.VERSION_CODES.JELLY_BEAN)
                        v.setImageAlpha(end_alpha);
                    else
                        v.setAlpha(end_alpha);

                    if (toggleVisibility) {
                        if (v.getVisibility() == View.GONE)
                            v.setVisibility(View.VISIBLE);
                        else
                            v.setVisibility(View.GONE);
                    }
                } else {
                    int new_alpha = (int) (begin_alpha + (interpolatedTime * (end_alpha - begin_alpha)));
                    if (Integer.valueOf(android.os.Build.VERSION.SDK_INT) >= android.os.Build.VERSION_CODES.JELLY_BEAN)
                        v.setImageAlpha(new_alpha);
                    else
                        v.setAlpha(new_alpha);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration(time);
        v.startAnimation(a);
    }

    private boolean addImageToGallery(Bitmap image) {

        ContentValues values = new ContentValues();
        if (lastErrorMsg == null || lastErrorMsg.length() == 0) {
            lastErrorMsg = "UnknownError";
        }

        String url = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), image, lastErrorMsg, lastErrorMsg);

        boolean success = url != null;
        if (success) {
            CommonUtils.showDialog(getContext(), "", LanguageUtils.getLocalizedString(getContext(),R.string.SAVE_PHOTO_SUCCESS_MSG));
        }

        return success;
    }

    private class LanguageAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return supportedLanguage.size();
        }

        @Override
        public Triple<Integer, String, String> getItem(int position) {
            return supportedLanguage.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.cell_language, null);
            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (CELL_HEIGHT * CommonUtils.getDeviceDensity(getContext())));
            convertView.setLayoutParams(params);

            TextView tv_language_name = (TextView) convertView.findViewById(R.id.tv_language_name);
            ImageView iv_lang_on = (ImageView) convertView.findViewById(R.id.iv_lang_on);

            if (currentSelectID == getItem(position).getFirst()) {
                iv_lang_on.setVisibility(View.VISIBLE);
            } else {
                iv_lang_on.setVisibility(View.GONE);
            }

            tv_language_name.setText(getItem(position).getSecond());
            return convertView;
        }
    }

}
