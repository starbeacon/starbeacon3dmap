package com.cherrypicks.arsignagecamerasdk.servermanager;


import com.cherrypicks.arsignagecamerasdk.servermanager.datamodel.ARImageResponse;

public interface ARServerManagerNotifier {
    void didSuccess(ARImageResponse responseData);
    void didFailure();
}
