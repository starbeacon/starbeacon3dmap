package com.cherrypicks.arsignagecamerasdk.customui;

import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;


public class CustomAnimationDrawable extends AnimationDrawable {
//public abstract class CustomAnimationDrawableNew extends AnimationDrawable {
public interface IAnimationFinishListener

{
    void onAnimationFinished();
}
    private IAnimationFinishListener animationFinishListener;

    public IAnimationFinishListener getAnimationFinishListener()
    {
        return animationFinishListener;
    }

    public void setAnimationFinishListener(IAnimationFinishListener animationFinishListener)
    {
        this.animationFinishListener = animationFinishListener;
    }

    /** Handles the animation callback. */
    Handler mAnimationHandler;
    private boolean finished = false;

    public CustomAnimationDrawable(AnimationDrawable aniDrawable) {
        /* Add each frame to our animation drawable */
        for (int i = 0; i < aniDrawable.getNumberOfFrames(); i++) {
            this.addFrame(aniDrawable.getFrame(i), aniDrawable.getDuration(i));
        }
    }

    @Override
    public void start() {
        super.start();
        /*
         * Call super.start() to call the base class start animation method.
         * Then add a handler to call onAnimationFinish() when the total
         * duration for the animation has passed
         */
//        mAnimationHandler = new Handler();
//        mAnimationHandler.postDelayed(new Runnable() {
//
//            public void run() {
//                if(animationFinishListener != null)animationFinishListener.onAnimationFinished();
//            }
//        }, getTotalDuration());

    }

    @Override
    public boolean selectDrawable(int idx)
    {
        boolean ret = super.selectDrawable(idx);

        if ((idx != 0) && (idx == getNumberOfFrames() - 1))
        {
            if (!finished)
            {
                finished = true;
                if (animationFinishListener != null) animationFinishListener.onAnimationFinished();
            }
        }

        return ret;
    }

    /**
     * Gets the total duration of all frames.
     *
     * @return The total duration.
     */
    public int getTotalDuration() {

        int iDuration = 0;

        for (int i = 0; i < this.getNumberOfFrames(); i++) {
            iDuration += this.getDuration(i);
        }

        return iDuration;
    }

}
