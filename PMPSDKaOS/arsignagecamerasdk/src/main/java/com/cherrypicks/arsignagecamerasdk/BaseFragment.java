package com.cherrypicks.arsignagecamerasdk;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.cherrypicks.arsignagecamerasdk.utils.LanguageUtils;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;


public class BaseFragment extends Fragment {
    protected int currentOrientation = Surface.ROTATION_0;
    long lastOrientationUpdateTime = 0;
    private OrientationEventListener orientationEventListener;
    protected View v_tutorial;
    protected TextView tv_title;
    protected ViewPager vp_tutorial;

    private int tutorialWidth = -1;
    private int tutorialHeight = -1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        orientationEventListener = new OrientationEventListener(getContext()) {
            @Override
            public void onOrientationChanged(int orientation) {
                if (orientation == ORIENTATION_UNKNOWN) return;

                int phi = Math.abs(0 - orientation) % 360;       // This is either the distance or 360 - distance
                int distance = phi > 180 ? 360 - phi : phi;

                int newOrientation;
                if (distance < 50) {
                    newOrientation = Surface.ROTATION_0;
                } else if (orientation < 180) {
                    newOrientation = Surface.ROTATION_90;
                } else {
                    newOrientation = Surface.ROTATION_270;
                }

                long currentTime = System.currentTimeMillis();
                long diff = currentTime - lastOrientationUpdateTime;

                if (currentOrientation != newOrientation && diff > 1000) {
                    lastOrientationUpdateTime = currentTime;
                    currentOrientation = newOrientation;

                    int rotationDegree = 0;
                    switch (newOrientation) {
                        case Surface.ROTATION_0:
                            rotationDegree = 0;
                            break;
                        case Surface.ROTATION_90:
                            rotationDegree = -90;
                            break;
                        case Surface.ROTATION_270:
                            rotationDegree = 90;
                            break;
                    }

                    onUserRotated(rotationDegree);
                }
            }
        };
        orientationEventListener.enable();
    }

    protected void setUpTutorial(View view) {
        tv_title = (TextView) view.findViewById(R.id.tv_title);
        tv_title.setText(LanguageUtils.getLocalizedString(getContext(), R.string.TUTORIAL_TITLE_STEP_1));
        v_tutorial = view.findViewById(R.id.v_tutorial);
        v_tutorial.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if(tutorialHeight == -1 && v_tutorial.getHeight() > 0){
                    tutorialHeight = v_tutorial.getHeight();
                    tutorialWidth = v_tutorial.getWidth();
                }
            }
        });
        ImageButton btn_close = (ImageButton) v_tutorial.findViewById(R.id.btn_close);
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideTutorial();
            }
        });
        vp_tutorial = (ViewPager) view.findViewById(R.id.vp_tutorial);
        vp_tutorial.setAdapter(new TutorialAdapter());
        vp_tutorial.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 0){
                    tv_title.setText(LanguageUtils.getLocalizedString(getContext(), R.string.TUTORIAL_TITLE_STEP_1));
                }else{
                    tv_title.setText(LanguageUtils.getLocalizedString(getContext(), R.string.TUTORIAL_TITLE_STEP_2));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        CircleIndicator indicator = (CircleIndicator) view.findViewById(R.id.indicator);
        indicator.setViewPager(vp_tutorial);
    }

    protected void showTutorial() {
        vp_tutorial.setCurrentItem(0);
        v_tutorial.setVisibility(View.VISIBLE);
    }

    protected void hideTutorial() {
        v_tutorial.setVisibility(View.GONE);
    }

    protected class TutorialAdapter extends PagerAdapter {
        private List<Integer> viewIDs;

        public TutorialAdapter() {
            this.viewIDs = new ArrayList<>();
            if(currentOrientation == Surface.ROTATION_0){
                viewIDs.add(R.layout.pmp_ar_signage_tutorial_page1);
                viewIDs.add(R.layout.pmp_ar_signage_tutorial_page2);
            }else{
                viewIDs.add(R.layout.pmp_ar_signage_tutorial_page1_langscape);
                viewIDs.add(R.layout.pmp_ar_signage_tutorial_page2_langscape);
            }
        }

        @Override
        public int getCount() {
            return viewIDs.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            ViewGroup layout = (ViewGroup) inflater.inflate(viewIDs.get(position), container, false);
            container.addView(layout);

            updateImageByLanuage(layout);

            return layout;
        }

        private void updateImageByLanuage(ViewGroup layout) {
            ImageView iv_language = (ImageView) layout.findViewById(R.id.iv_language);
            ImageView iv_pinch = (ImageView) layout.findViewById(R.id.iv_pinch);
            ImageView iv_retake = (ImageView) layout.findViewById(R.id.iv_retake);
            ImageView iv_save = (ImageView) layout.findViewById(R.id.iv_save);
            ImageView iv_take_photo = (ImageView) layout.findViewById(R.id.iv_take_photo);
            ImageView iv_take_photo_area = (ImageView) layout.findViewById(R.id.iv_take_photo_area);

            if(currentOrientation == Surface.ROTATION_0) {
                switch (LanguageUtils.getLandID()){
                    case LanguageUtils.Language_English:
                        if(iv_language != null)
                            iv_language.setImageResource(R.drawable.arrow_lang_verticle);
                        if(iv_pinch != null)
                            iv_pinch.setImageResource(R.drawable.icon_text_pinch);
                        if(iv_retake != null)
                            iv_retake.setImageResource(R.drawable.arrow_retake_verticle);
                        if(iv_save != null)
                            iv_save.setImageResource(R.drawable.arrow_save_verticle);
                        if(iv_take_photo != null)
                            iv_take_photo.setImageResource(R.drawable.arrow_photo_verticle);
                        if(iv_take_photo_area != null)
                            iv_take_photo_area.setImageResource(R.drawable.text_take);
                        break;
                    case LanguageUtils.Language_SimplifiedChinese:
                        if(iv_language != null)
                            iv_language.setImageResource(R.drawable.arrow_lang_verticle_sc);
                        if(iv_pinch != null)
                            iv_pinch.setImageResource(R.drawable.icon_text_pinch_sc);
                        if(iv_retake != null)
                            iv_retake.setImageResource(R.drawable.arrow_retake_verticle_sc);
                        if(iv_save != null)
                            iv_save.setImageResource(R.drawable.arrow_save_verticle_sc);
                        if(iv_take_photo != null)
                            iv_take_photo.setImageResource(R.drawable.arrow_photo_verticle_sc);
                        if(iv_take_photo_area != null)
                            iv_take_photo_area.setImageResource(R.drawable.text_take_sc);
                        break;
                    case LanguageUtils.Language_TraditionalChinese:
                        if(iv_language != null)
                            iv_language.setImageResource(R.drawable.arrow_lang_verticle_tc);
                        if(iv_pinch != null)
                            iv_pinch.setImageResource(R.drawable.icon_text_pinch_tc);
                        if(iv_retake != null)
                            iv_retake.setImageResource(R.drawable.arrow_retake_verticle_tc);
                        if(iv_save != null)
                            iv_save.setImageResource(R.drawable.arrow_save_verticle_tc);
                        if(iv_take_photo != null)
                            iv_take_photo.setImageResource(R.drawable.arrow_photo_verticle_tc);
                        if(iv_take_photo_area != null)
                            iv_take_photo_area.setImageResource(R.drawable.text_take_tc);
                        break;
                    case LanguageUtils.Language_Janpanese:
                        if(iv_language != null)
                            iv_language.setImageResource(R.drawable.arrow_lang_verticle_jp);
                        if(iv_pinch != null)
                            iv_pinch.setImageResource(R.drawable.icon_text_pinch_jp);
                        if(iv_retake != null)
                            iv_retake.setImageResource(R.drawable.arrow_retake_verticle_jp);
                        if(iv_save != null)
                            iv_save.setImageResource(R.drawable.arrow_save_verticle_jp);
                        if(iv_take_photo != null)
                            iv_take_photo.setImageResource(R.drawable.arrow_photo_verticle_jp);
                        if(iv_take_photo_area != null)
                            iv_take_photo_area.setImageResource(R.drawable.text_take_jp);
                        break;
                    case LanguageUtils.Language_Korean:
                        if(iv_language != null)
                            iv_language.setImageResource(R.drawable.arrow_lang_verticle_kr);
                        if(iv_pinch != null)
                            iv_pinch.setImageResource(R.drawable.icon_text_pinch_kr);
                        if(iv_retake != null)
                            iv_retake.setImageResource(R.drawable.arrow_retake_verticle_kr);
                        if(iv_save != null)
                            iv_save.setImageResource(R.drawable.arrow_save_verticle_kr);
                        if(iv_take_photo != null)
                            iv_take_photo.setImageResource(R.drawable.arrow_photo_verticle_kr);
                        if(iv_take_photo_area != null)
                            iv_take_photo_area.setImageResource(R.drawable.text_take_kr);
                        break;
                }
            }else{
                switch (LanguageUtils.getLandID()){
                    case LanguageUtils.Language_English:
                        if(iv_language != null)
                            iv_language.setImageResource(R.drawable.arrow_lang_horizontal);
                        if(iv_pinch != null)
                            iv_pinch.setImageResource(R.drawable.icon_text_pinch);
                        if(iv_retake != null)
                            iv_retake.setImageResource(R.drawable.arrow_retake_horizontal);
                        if(iv_save != null)
                            iv_save.setImageResource(R.drawable.arrow_save_horizontal);
                        if(iv_take_photo != null)
                            iv_take_photo.setImageResource(R.drawable.arrow_photo_horizontal);
                        if(iv_take_photo_area != null)
                            iv_take_photo_area.setImageResource(R.drawable.text_take);
                        break;
                    case LanguageUtils.Language_SimplifiedChinese:
                        if(iv_language != null)
                            iv_language.setImageResource(R.drawable.arrow_lang_horizontal_sc);
                        if(iv_pinch != null)
                            iv_pinch.setImageResource(R.drawable.icon_text_pinch_sc);
                        if(iv_retake != null)
                            iv_retake.setImageResource(R.drawable.arrow_retake_horizontal_sc);
                        if(iv_save != null)
                            iv_save.setImageResource(R.drawable.arrow_save_horizontal_sc);
                        if(iv_take_photo != null)
                            iv_take_photo.setImageResource(R.drawable.arrow_photo_horizontal_sc);
                        if(iv_take_photo_area != null)
                            iv_take_photo_area.setImageResource(R.drawable.text_take_sc);
                        break;
                    case LanguageUtils.Language_TraditionalChinese:
                        if(iv_language != null)
                            iv_language.setImageResource(R.drawable.arrow_lang_horizontal_tc);
                        if(iv_pinch != null)
                            iv_pinch.setImageResource(R.drawable.icon_text_pinch_tc);
                        if(iv_retake != null)
                            iv_retake.setImageResource(R.drawable.arrow_retake_horizontal_tc);
                        if(iv_save != null)
                            iv_save.setImageResource(R.drawable.arrow_save_horizontal_tc);
                        if(iv_take_photo != null)
                            iv_take_photo.setImageResource(R.drawable.arrow_photo_horizontal_tc);
                        if(iv_take_photo_area != null)
                            iv_take_photo_area.setImageResource(R.drawable.text_take_tc);
                        break;
                    case LanguageUtils.Language_Janpanese:
                        if(iv_language != null)
                            iv_language.setImageResource(R.drawable.arrow_lang_horizontal_jp);
                        if(iv_pinch != null)
                            iv_pinch.setImageResource(R.drawable.icon_text_pinch_jp);
                        if(iv_retake != null)
                            iv_retake.setImageResource(R.drawable.arrow_retake_horizontal_jp);
                        if(iv_save != null)
                            iv_save.setImageResource(R.drawable.arrow_save_horizontal_jp);
                        if(iv_take_photo != null)
                            iv_take_photo.setImageResource(R.drawable.arrow_photo_horizontal_jp);
                        if(iv_take_photo_area != null)
                            iv_take_photo_area.setImageResource(R.drawable.text_take_jp);
                        break;
                    case LanguageUtils.Language_Korean:
                        if(iv_language != null)
                            iv_language.setImageResource(R.drawable.arrow_lang_horizontal_kr);
                        if(iv_pinch != null)
                            iv_pinch.setImageResource(R.drawable.icon_text_pinch_kr);
                        if(iv_retake != null)
                            iv_retake.setImageResource(R.drawable.arrow_retake_horizontal_kr);
                        if(iv_save != null)
                            iv_save.setImageResource(R.drawable.arrow_save_horizontal_kr);
                        if(iv_take_photo != null)
                            iv_take_photo.setImageResource(R.drawable.arrow_photo_horizontal_kr);
                        if(iv_take_photo_area != null)
                            iv_take_photo_area.setImageResource(R.drawable.text_take_kr);
                        break;
                }
            }

        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    protected void onUserRotated(int rotationDegree) {
        Log.i("BaseFragment", "onUserRotated" + rotationDegree);
        if(v_tutorial != null){
            v_tutorial.setRotation(rotationDegree);

            ViewGroup.LayoutParams params = v_tutorial.getLayoutParams();
            switch (currentOrientation){
                case Surface.ROTATION_0:
                    params.width = tutorialWidth;
                    params.height = tutorialHeight;
                    v_tutorial.setX(0);
                    v_tutorial.setY(0);
                    break;
                default:
                    params.width = tutorialHeight;
                    params.height = tutorialWidth;

                    int diff = (tutorialHeight - tutorialWidth) / 2;
                    v_tutorial.setX(-diff);
                    v_tutorial.setY(diff);

                    break;
            }
            v_tutorial.setLayoutParams(params);

            int currentPage = vp_tutorial.getCurrentItem();
            vp_tutorial.setAdapter(new TutorialAdapter());
            vp_tutorial.setCurrentItem(currentPage);
        }
    }

    @Override
    public void onDestroy() {
        orientationEventListener.disable();
        super.onDestroy();
    }
}
