package com.cherrypicks.arsignagecamerasdk.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.support.v7.app.AlertDialog;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.DisplayMetrics;

import com.cherrypicks.arsignagecamerasdk.R;

/**
 * Created by Alan on 10/4/17.
 */
public class CommonUtils {
    public static void showDialog(Context context, String title, String message) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.TUTORIAL_CONFIRM, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
//                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        // do nothing
//                    }
//                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public static float getDeviceDensity(Context context){
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return metrics.density;
    }

    public static Bitmap addDebugInfoToBitmap(Bitmap sourceImage, String message) {
        Bitmap mutableBitmap = sourceImage.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(mutableBitmap);

        //draw bg
        Paint myPaint = new Paint();
        myPaint.setColor(Color.BLACK);
        myPaint.setStrokeWidth(10);
        canvas.drawRect(0,0, 800, 100, myPaint);


        //draw text
        TextPaint paint = new TextPaint();
        paint.setColor(Color.WHITE); // Text Color
        paint.setStrokeWidth(20); // Text Size
        paint.setTextSize(30);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)); // Text Overlapping Pattern
        paint.setShadowLayer(3f, 0f, 3f, Color.DKGRAY);
        StaticLayout mTextLayout = new StaticLayout(message, paint, canvas.getWidth(), Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        canvas.translate(30, 30);
        mTextLayout.draw(canvas);

        return mutableBitmap;
    }

    public static String getDebugMessage(long totalProcessTime, long serverProcessTime){
        return "Total time:" + totalProcessTime + "ms, server time:"+ serverProcessTime + "ms, network time:" + (totalProcessTime - serverProcessTime);
    }
}
