package com.cherrypicks.arsignagecamerasdk.servermanager;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.cherrypicks.arsignagecamerasdk.BuildConfig;
import com.cherrypicks.arsignagecamerasdk.servermanager.datamodel.ARImageResponse;
import com.google.gson.Gson;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


/**
 * Created by Alan on 10/4/17.
 */
public class ARServerManager {
    public static class TargetLanguage{
        public static final int SIMPLIFIED_CHINESE = 0;
        public static final int JANPANESE = 1;
        public static final int KOREAN = 2;
    }

    public static final String TAG = ARServerManager.class.getSimpleName();
    public static String BASE_URL = BuildConfig.BaseURL;

    private static ARServerManager shared = null;
    private RequestQueue requestQueue = null;
    private boolean requesting = false;
    private SecureRandom random = new SecureRandom();

    public void setContext(Context context) {
        this.context = context;
        this.requestQueue = Volley.newRequestQueue(context);
    }

    private Context context;

    public static ARServerManager getShared(Context context) {
        if (shared == null) {
            shared = new ARServerManager();
        }
        if (context != null) {
            shared.setContext(context);
        }
        return  shared;
    }

    public static ARServerManager getShared() {
        return shared;
    }

    public void sendImage(int langID, byte[] image, final ARServerManagerNotifier notifier) {
        //        zh-CN: Simplified Chinese
        //        ja-JP: Japanese
        //        ko-KR: Korean
        String landCode = "chi";
        switch (langID){
            case TargetLanguage.SIMPLIFIED_CHINESE:
                landCode = "chi";
                break;
            case TargetLanguage.JANPANESE:
                landCode = "jap";
                break;
            case TargetLanguage.KOREAN:
                landCode = "kor";
                break;
        }

        String url = BASE_URL + "arconversion.do";
        HashMap<String, String> params = new HashMap<>();
        params.put("language", landCode);

        char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }

        String key = sb.toString();
        params.put("key", key);

        params.put("token", getKey(key));
//        Log.i("test","getKey(key)" + getKey(key));

        apiCall(url, params, image,  new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                ARImageResponse responseData = new Gson().fromJson(response, ARImageResponse.class);
                notifier.didSuccess(responseData);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                notifier.didFailure();
            }
        });


    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


    private <T> void  apiCall(String url, final HashMap<String, String> params, final byte[] image, final Response.Listener<String> listener, final Response.ErrorListener errorListener) {
        requesting = true;

        final Response.ErrorListener myErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse");
                requesting = false;
                errorListener.onErrorResponse(error);
            }
        };
        VolleyMultipartRequest jsObjRequest = new VolleyMultipartRequest
                (Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(final NetworkResponse response) {
                        Log.d(TAG, "onResponse!");
                        requesting = false;
                        String resultResponse = new String(response.data);
                        listener.onResponse(resultResponse);

                    }
                }, myErrorListener){


            @Override
            protected Map<String, DataPart> getByteData() throws AuthFailureError {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                params.put("fileName", new DataPart("file_avatar.jpg", image, "image/jpeg"));

                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> customHeader = new HashMap<String, String>();
                customHeader.put("Device", "" + android.os.Build.MODEL);
                customHeader.put("OSVersion", "" + android.os.Build.VERSION.RELEASE);
                return customHeader;
            }
        };

        Cache.Entry entry = new Cache.Entry();
        entry.data = new byte[0];

        jsObjRequest.setCacheEntry(entry);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(360000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsObjRequest.setShouldCache(false);
        this.requestQueue.add(jsObjRequest);
    }


    public boolean isRequesting() {
        return requesting;
    }

    public native String getKey(String random);

}
