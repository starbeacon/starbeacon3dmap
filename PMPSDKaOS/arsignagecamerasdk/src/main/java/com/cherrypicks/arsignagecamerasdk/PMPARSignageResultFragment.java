package com.cherrypicks.arsignagecamerasdk;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.cherrypicks.arsignagecamerasdk.utils.CommonUtils;
import com.cherrypicks.arsignagecamerasdk.utils.LanguageUtils;


public class PMPARSignageResultFragment extends BaseFragment implements Toolbar.OnMenuItemClickListener{
    private static final int PERMISSION_REQUEST_CODE = 200;

    private ImageButton ibtn_retake;
    private ImageButton ibtn_dl;
    private ImageView resultImageView;
    byte[] imageData;

    int resultImageViewHeight = -1;
    int resultImageViewWidth = -1;

    //debug message
    private boolean isDebugEnable = false;
    private String errorMessage = "";
    private Bitmap oriImage;
    private long totalProcessTime = 0;
    private long serverProcessTime = 0;
    private int score;
    private int signageId;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.pmp_ar_signage_result_fragment, container, false);
        setUpUI(view);

        return view;
    }

    private void setUpUI(View view) {
        ibtn_retake = (ImageButton) view.findViewById(R.id.ibtn_retake);
        ibtn_dl = (ImageButton) view.findViewById(R.id.ibtn_dl);
        ibtn_retake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        });

        ibtn_dl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
                    if (checkPermission()) {
                        saveImage();
                    } else {
                        requestPermission();
                    }
                }else{
                    saveImage();
                }


            }
        });

        resultImageView = (ImageView) view.findViewById(R.id.iv_result);
        Bitmap bitmap = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
        resultImageView.setImageBitmap(bitmap);

        if(isDebugEnable){
            TextView tv_debug_time = (TextView) view.findViewById(R.id.tv_debug_time);
            tv_debug_time.setText(CommonUtils.getDebugMessage(totalProcessTime, serverProcessTime) + ", score: " + this.score + ", signage id: " + this.signageId);
        }
    }

    public void setImageData(byte[] imageData) {
        this.imageData = imageData;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public void setDebugEnable(boolean debugEnable) {
        isDebugEnable = debugEnable;
    }

    public void setDebugOriImage(Bitmap oriImage) {
        this.oriImage = oriImage;
    }

    public void setTotalProcessTime(long totalProcessTime) {
        this.totalProcessTime = totalProcessTime;
    }

    public void setServerProcessTime(int serverProcessTime) {
        this.serverProcessTime = serverProcessTime;
    }

    public void setScore(int score){
        this.score = score;
    }

    public void setSignageId(int signageId){
        this.signageId = signageId;
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

//                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean saveImageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (saveImageAccepted)
                        saveImage();
//                        Toast.makeText(getApplicationContext(), "Permission Granted, Now you can access location data and camera", Toast.LENGTH_LONG).show();
                    else {
                        CommonUtils.showDialog(getActivity(), LanguageUtils.getLocalizedString(getContext(),R.string.ERROR_CANT_SAVE_PHOTO_TITLE), LanguageUtils.getLocalizedString(getContext(),R.string.ERROR_CANT_SAVE_PHOTO_MSG));
//                        Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access location data and camera", Toast.LENGTH_LONG).show();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                            if (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
//
//                                return;
//                            }
                        }

                    }
                }

                break;
        }
    }

    private void saveImage(){
        Bitmap bitmap = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
        if(addImageToGallery(bitmap)){
            CommonUtils.showDialog(getContext(), "", LanguageUtils.getLocalizedString(getContext(),R.string.SAVE_PHOTO_SUCCESS_MSG));
        }else{
            CommonUtils.showDialog(getContext(), "", LanguageUtils.getLocalizedString(getContext(),R.string.ERROR_CANT_SAVE_PHOTO_TITLE));
        }
    }

    private boolean addImageToGallery(Bitmap image) {
        String fileName = ""+System.currentTimeMillis();

        if(isDebugEnable){
            if(errorMessage == null || errorMessage.length() == 0){
                errorMessage = "UnknownError";
            }

            if(oriImage != null){
                Bitmap mutableBitmap = CommonUtils.addDebugInfoToBitmap(oriImage, errorMessage);

                MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), mutableBitmap, fileName, fileName);

                mutableBitmap.recycle();
            }
            fileName = fileName+"_result";

            Bitmap debugBitmap = CommonUtils.addDebugInfoToBitmap(image, errorMessage);
            image.recycle();
            image = debugBitmap;
        }

        String url = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), image, fileName, fileName);

        return url != null;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
    }
    
    @Override
    public void onDestroyView() {

        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onUserRotated(int rotationDegree) {
        super.onUserRotated(rotationDegree);
        int duration = 100;

        ViewGroup.LayoutParams params = resultImageView.getLayoutParams();
        if(resultImageView.getHeight() > 0 && resultImageViewHeight < 0){
            resultImageViewHeight = resultImageView.getHeight();
            resultImageViewWidth = resultImageView.getWidth();
        }

        if(resultImageViewWidth > 0){
            switch (currentOrientation){
                case Surface.ROTATION_0:
                    params.width = resultImageViewWidth;
                    params.height = resultImageViewHeight;
                    resultImageView.setX(0);
                    resultImageView.setY(0);
                    break;
                default:
                    params.width = resultImageViewHeight;
                    params.height = resultImageViewWidth;

                    int diff = (resultImageViewHeight - resultImageViewWidth) / 2;
                    resultImageView.setX(-diff);
                    resultImageView.setY(diff);

                    break;
            }
            resultImageView.setLayoutParams(params);
        }

        ibtn_retake.animate().rotation(rotationDegree).setDuration(duration).start();
        ibtn_dl.animate().rotation(rotationDegree).setDuration(duration).start();
        resultImageView.animate().rotation(rotationDegree).setDuration(duration).start();
    }
}
