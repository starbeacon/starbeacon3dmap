package com.cherrypicks.arsignagecamerasdk.servermanager.datamodel;

import com.google.gson.annotations.SerializedName;


public class ARImageResponse {
    @SerializedName("errorCode")
    private int errorCode;
    @SerializedName("errorMessage")
    private String errorMessage;
    @SerializedName("result")
    private String result;
    @SerializedName("time")
    private int time;
    @SerializedName("matchingScore")
    private int score;
    @SerializedName("signageId")
    private int signageId;

    public ARImageResponse() {

    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getResult() {
        return result;
    }

    public int getTime() {
        return time;
    }

    public int getScore() {return score;}

    public int getSignageId() {return signageId;}
}
