package com.cherrypicks.arsignagecamerasdk.utils;

import android.content.Context;
import android.content.res.Configuration;

import java.util.Locale;

/**
 * Created by Alan on 10/4/17.
 */
public class LanguageUtils {
    public static final int Language_English = 1;
    public static final int Language_TraditionalChinese = 2;
    public static final int Language_SimplifiedChinese = 3;
    public static final int Language_Janpanese = 4;
    public static final int Language_Korean = 5;

    private static int landID = Language_English;

    public static void setLangID(int newLangID) {
        landID = newLangID;
    }

    public static int getLandID() {
        return landID;
    }

    public static String getLocalizedString(Context mContext , int resID){
        Configuration conf = mContext.getResources().getConfiguration();
        conf = new Configuration(conf);
        switch (landID) {
            case Language_English:
                conf.setLocale(Locale.ENGLISH);
                break;
            case Language_TraditionalChinese:
                conf.setLocale(Locale.TRADITIONAL_CHINESE);
                break;
            case Language_SimplifiedChinese:
                conf.setLocale(Locale.SIMPLIFIED_CHINESE);
                break;
            case Language_Janpanese:
                conf.setLocale(Locale.JAPANESE);
                break;
            case Language_Korean:
                conf.setLocale(Locale.KOREAN);
                break;
            default:
                conf.setLocale(Locale.ENGLISH);
                break;
        }

        Context localizedContext = mContext.createConfigurationContext(conf);
        return localizedContext.getResources().getString(resID);
    }
}
