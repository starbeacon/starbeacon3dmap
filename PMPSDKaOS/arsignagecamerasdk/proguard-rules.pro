-keepattributes *Annotation*
-keepattributes EnclosingMethod
-keepattributes InnerClasses

-keep public class com.google.vending.licensing.ILicensingService
-keep public class com.android.vending.licensing.ILicensingService

# For native methods, see http://proguard.sourceforge.net/manual/examples.html#native
-keepclasseswithmembernames class * {
    native <methods>;
}

# keep setters in Views so that animations can still work.
# see http://proguard.sourceforge.net/manual/examples.html#beans
-keepclassmembers public class * extends android.view.View {
	public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
   void set*(***);
   *** get*();
}

# We want to keep methods in Activity that could be used in the XML attribute onClick
-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}

# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

# -keep class com.pmp.mapsdk.app.** { *; }
# -keep class com.cherrypicks.pmpmap.** { *; }

-keep class com.cherrypicks.arsignagecamerasdk.servermanager.datamodel.** { *; }
-keep class com.cherrypicks.arsignagecamerasdk.ARSignageTranslationFragment {
    public void setDebugEnable(boolean);
    public void setLangID(int);
}

-keep public class **.R {
  public *;
}
-keep public class **.R$* {
  public *;
}

-keep public class com.groupon.inject.JsonConverter
-keep class com.flurry.** { *; }
-dontwarn com.flurry.**
-keepattributes Signature
-dontwarn org.apache.http.**
-dontwarn org.springframework.**
-dontwarn org.apache.log4j.**
-dontwarn org.apache.commons.logging.**
-dontwarn org.apache.commons.codec.binary.**
-dontwarn android.net.http.**
-keep class android.net.http.SslError
-dontwarn com.google.android.maps.**
-dontwarn javax.activation.**
-dontwarn java.beans.**
-dontwarn javax.xml.**
-dontwarn javax.ws.**
-dontwarn org.apache.commons.**
-dontwarn org.joda.time.**
-dontwarn org.springframework.**
-dontwarn org.w3c.dom.**
-dontskipnonpubliclibraryclasses
-dontskipnonpubliclibraryclassmembers
-keep public class * extends android.content.ContentProvider



# These options are the minimal options for a functioning application
# using Proguard and the AWS SDK 2.1.5 for Android

-keep class org.apache.commons.logging.**               { *; }
-keep class com.amazonaws.org.apache.commons.logging.** { *; }
-keep class com.amazonaws.services.sqs.QueueUrlHandler  { *; }
-keep class com.amazonaws.javax.xml.transform.sax.*     { public *; }
-keep class com.amazonaws.javax.xml.stream.**           { *; }
-keep class com.amazonaws.services.**.model.*Exception* { *; }
-keep class com.amazonaws.internal.**                   { *; }
-keep class org.codehaus.**                             { *; }
-keep class org.joda.time.tz.Provider                   { *; }
-keep class org.joda.time.tz.NameProvider               { *; }
-keepattributes Signature,*Annotation*,EnclosingMethod
-keepnames class com.fasterxml.jackson.** { *; }
-keepnames class com.amazonaws.** { *; }

-dontwarn com.fasterxml.jackson.databind.**
-dontwarn javax.xml.stream.events.**
-dontwarn org.codehaus.jackson.**
-dontwarn org.apache.commons.logging.impl.**
-dontwarn org.apache.http.conn.scheme.**
-dontwarn org.apache.http.annotation.**
-dontwarn org.ietf.jgss.**
-dontwarn org.joda.convert.**
-dontwarn com.amazonaws.org.joda.convert.**
-dontwarn org.w3c.dom.bootstrap.**

#SDK split into multiple jars so certain classes may be referenced but not used
-dontwarn com.amazonaws.services.s3.**
-dontwarn com.amazonaws.services.sqs.**

-dontnote com.amazonaws.services.sqs.QueueUrlHandler

-dontwarn org.apache.http.annotation.**
-dontwarn com.amazonaws.**