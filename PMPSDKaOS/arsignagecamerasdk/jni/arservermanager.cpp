#include <jni.h>
#include <string>
#include "md5.h"

using namespace std;

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jstring JNICALL
Java_com_cherrypicks_arsignagecamerasdk_servermanager_ARServerManager_getKey(JNIEnv *env,
                                                                             jobject instance,
                                                                             jstring random_) {
    const char *random = (*env).GetStringUTFChars(random_, 0);
    std::string key = "CUJSRQPP40"; // DEV: ""Y8UWEB4YS7";", UAT: "CUJSRQPP40";

    char *cstr = strdup((key + random).c_str());
    for (int j = 2; j > 0 ; j--){
        MD5_CTX context = {0};
        MD5Init(&context);
        MD5Update(&context, (unsigned char *) cstr, strlen(cstr));
        unsigned char dest[16] = {0};
        MD5Final(dest, &context);

        int i;
        char destination[32] = {0};
        for (i = 0; i < 16; i++) {
            sprintf(destination, "%s%02x", destination, dest[i]);
        }

        cstr = destination;
    }

    return env->NewStringUTF(cstr);

}

#ifdef __cplusplus
}
#endif
