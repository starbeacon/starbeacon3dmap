package pmp.com.proximitytool;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.support.v4.app.NotificationCompat;

import com.cherrypicks.pmpmap.datamodel.MapState;
import com.google.gson.Gson;
import com.pmp.mapsdk.cms.model.AaZones;
import com.pmp.mapsdk.cms.model.Devices;
import com.pmp.mapsdk.cms.model.Response;
import com.pmp.mapsdk.cms.model.Zones;
import com.pmp.mapsdk.external.PMPMapSDK;
import com.pmp.mapsdk.external.ProximityCallback;
import com.pmp.mapsdk.external.ProximityMessage;
import com.pmp.mapsdk.location.PMPApplication;
import com.pmp.mapsdk.utils.PMPUtil;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;

/**
 * Created by Yu on 23/2/2017.
 */

public class PXApplication extends Application {
    Response presponse;
    @Override
    public void onCreate() {
        super.onCreate();

        PMPUtil.getSharedPreferences(this).edit().putString("MtelUserId", "test1");
        PMPUtil.getSharedPreferences(this).edit().putString("MtelPushToken", "test1");

        AssetManager assetManager = this.getAssets();
        try {
            InputStream inputStream = assetManager.open("zone_data.json");
            StringWriter writer = new StringWriter();
            IOUtils.copy(inputStream, writer, Charset.defaultCharset());
            String theString = writer.toString();
            presponse = new Gson().fromJson(theString, Response.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        PMPMapSDK.setProximityCallback(new ProximityCallback() {
            @Override
            public void onProximityEnterRegion(final int proximityId, String promotionId, boolean isBackground, ProximityMessage proximityMessage) {
                if (isBackground) {
                    final Devices device = CollectionUtils.find(presponse.getDevices(), new Predicate<Devices>() {

                        @Override
                        public boolean evaluate(Devices device) {
                            return device.getMajorNo() == proximityId;
                        }
                    });
                    if (device != null) {
                        Zones zone = CollectionUtils.find(presponse.getZones(), new Predicate<Zones>() {
                            @Override
                            public boolean evaluate(Zones zone) {
                                return zone.getId() == device.getZoneId();
                            }
                        });
                        AaZones aaZone = CollectionUtils.find(presponse.getAaZones(), new Predicate<AaZones>() {
                            @Override
                            public boolean evaluate(AaZones zone) {
                                return zone.getId() == device.getAaZoneId();
                            }
                        });
                        StringBuilder sb = new StringBuilder();
                        if (zone != null) {
                            sb.append(String.format("(%d)%s,", (int)device.getZoneId(), zone.getName()));
                        }
                        if (aaZone != null) {
                            sb.append(String.format("(%d)%s,", (int)device.getAaZoneId(), aaZone.getName()));
                        }
                        sb.append(String.format("(%d),", proximityId));

                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                        NotificationCompat.Builder b = new NotificationCompat.Builder(getApplicationContext());

                        b.setAutoCancel(true)
                                .setDefaults(Notification.DEFAULT_ALL)
                                .setWhen(System.currentTimeMillis())
                                .setSmallIcon(android.R.drawable.ic_dialog_alert)
                                .setContentTitle("Proximity")
                                .setContentText(sb)
                                .setDefaults(Notification.DEFAULT_LIGHTS| Notification.DEFAULT_SOUND)
                                .setContentIntent(contentIntent)
                                .setContentInfo("Info");


                        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(1, b.build());
                    }

                }

            }

            @Override
            public void onProximityExitRegion(int proximityId) {

            }

            @Override
            public void onProximityClicked(int proximityId, String promotionId, MapState mapState) {

            }
        });
        PMPMapSDK.onApplicationCreate(this);
        PMPMapSDK.setMainActivityClass(MainActivity.class);
        PMPMapSDK.setEnableNotification(true);
        PMPApplication.getPmpApplication().setEnableProximityLogging(false);
    }
}
