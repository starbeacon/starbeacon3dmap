package pmp.com.proximitytool;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.cherrypicks.pmpmap.PMPIndoorLocationManager;
import com.google.gson.Gson;
import com.pmp.mapsdk.cms.model.AaZones;
import com.pmp.mapsdk.cms.model.Devices;
import com.pmp.mapsdk.cms.model.Response;
import com.pmp.mapsdk.cms.model.Zones;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    private BaseAdapter adapter = null;
    private TextView deviceIDLabel;
    private TextView zoneLabel;
    private TextView aaZoneLabel;
    private ListView tableView;
    private ArrayList dataSource = new ArrayList();
    private Response presponse;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            if (intent.getAction().equals(PMPIndoorLocationManager.BROADCAST_PROXIMITY_ENTER_ZONE)) {
//                final int proximityId = intent.getIntExtra(PMPIndoorLocationManager.BROADCAST_ZONE_ID, -1);
//                boolean isBackground = intent.getBooleanExtra(PMPIndoorLocationManager.BROADCAST_IS_BACKGROUND, false);
//
//                if (isBackground) {
//                    return;
//                }
//
//                final Devices device = CollectionUtils.find(presponse.getDevices(), new Predicate<Devices>() {
//
//                    @Override
//                    public boolean evaluate(Devices device) {
//                        return device.getMajorNo() == proximityId;
//                    }
//                });
//                if (device != null) {
//                    Zones zone = CollectionUtils.find(presponse.getZones(), new Predicate<Zones>() {
//                        @Override
//                        public boolean evaluate(Zones zone) {
//                            return zone.getId() == device.getZoneId();
//                        }
//                    });
//                    AaZones aaZone = CollectionUtils.find(presponse.getAaZones(), new Predicate<AaZones>() {
//                        @Override
//                        public boolean evaluate(AaZones zone) {
//                            return zone.getId() == device.getAaZoneId();
//                        }
//                    });
//                    StringBuilder sb = new StringBuilder();
//                    if (aaZone != null) {
//                        aaZoneLabel.setText(String.format("(%d)%s", (int) device.getAaZoneId(), aaZone.getName()));
//                    }
//                    if (zone != null) {
//                        zoneLabel.setText(String.format("(%d)%s", (int) device.getZoneId(), zone.getName()));
//                    }
//                    deviceIDLabel.setText(String.format("(%d)", proximityId));
//                    if (dataSource.size() > 100) {
//                        dataSource.remove(dataSource.size() - 1);
//                    }
//                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
//                    String timestamp = sdf.format(new Date());
//                    dataSource.add(0, new DatasourceStruct(proximityId, timestamp));
//                    adapter.notifyDataSetChanged();
//                }
//            }
        }
    };
    private LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            this.setTitle(String.format("Proximity Debug Tool V%s", version));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        AssetManager assetManager = this.getAssets();
        try {
            InputStream inputStream = assetManager.open("zone_data.json");
            StringWriter writer = new StringWriter();
            IOUtils.copy(inputStream, writer, Charset.defaultCharset());
            String theString = writer.toString();
            presponse = new Gson().fromJson(theString, Response.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        zoneLabel = (TextView) findViewById(R.id.zone);
        aaZoneLabel = (TextView) findViewById(R.id.aazone);
        deviceIDLabel = (TextView) findViewById(R.id.device_id);

        tableView = (ListView) findViewById(R.id.listview);

        tableView.setAdapter(adapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return dataSource.size();
            }

            @Override
            public Object getItem(int i) {
                return null;
            }

            @Override
            public long getItemId(int i) {
                return 0;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                ViewHolder viewHolder;
                if (view == null) {
                    view = inflater.inflate(R.layout.proximity_cell, null);
                    viewHolder = new ViewHolder(view);
                    view.setTag(viewHolder);
                } else {
                    viewHolder = (ViewHolder) view.getTag();
                }
                final DatasourceStruct data = (DatasourceStruct) dataSource.get(i);

                final Devices device = CollectionUtils.find(presponse.getDevices(), new Predicate<Devices>() {

                    @Override
                    public boolean evaluate(Devices device) {
                        return device.getMajorNo() == data.deviceId;
                    }
                });
                if (device != null) {
                    Zones zone = CollectionUtils.find(presponse.getZones(), new Predicate<Zones>() {
                        @Override
                        public boolean evaluate(Zones zone) {
                            return zone.getId() == device.getZoneId();
                        }
                    });
                    AaZones aaZone = CollectionUtils.find(presponse.getAaZones(), new Predicate<AaZones>() {
                        @Override
                        public boolean evaluate(AaZones zone) {
                            return zone.getId() == device.getAaZoneId();
                        }
                    });
                    StringBuilder sb = new StringBuilder();
                    if (aaZone != null) {
                        viewHolder.aaZoneLabel.setText(String.format("(%d)%s", (int) device.getAaZoneId(), aaZone.getName()));
                    }
                    if (zone != null) {
                        viewHolder.zoneLabel.setText(String.format("(%d)%s", (int) device.getZoneId(), zone.getName()));
                    }
                    viewHolder.deviceIDLabel.setText(String.format("(%d)", data.deviceId));
                    viewHolder.timestamp.setText(data.timestamp);
                }
                return view;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        registerNotification();
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterNotification();
    }

    public void registerNotification() {
//        IntentFilter enterIntent = new IntentFilter(PMPIndoorLocationManager.BROADCAST_PROXIMITY_ENTER_ZONE);
//        this.registerReceiver(receiver, enterIntent);
    }

    public void unregisterNotification() {
        this.unregisterReceiver(receiver);
    }

    public static class ViewHolder {
        public ViewHolder(View v) {
            deviceIDLabel = (TextView) v.findViewById(R.id.device_id);
            zoneLabel = (TextView) v.findViewById(R.id.zone);
            aaZoneLabel = (TextView) v.findViewById(R.id.aazone);
            timestamp = (TextView) v.findViewById(R.id.timestamp);
        }
        public TextView deviceIDLabel;
        public TextView zoneLabel;
        public TextView aaZoneLabel;
        public TextView timestamp;
    }

    public static class DatasourceStruct {
        public DatasourceStruct(int deviceId, String timestamp) {
            this.deviceId = deviceId;
            this.timestamp = timestamp;
        }
        public String timestamp;
        public int deviceId;
    }
}
