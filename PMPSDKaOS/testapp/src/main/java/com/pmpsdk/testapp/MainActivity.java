package com.pmpsdk.testapp;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = "Main";

    TextView proximityTextView;
    private BroadcastReceiver proximityInzoneReceiver, proximityOutzoneReceiver;
    private final static int MY_PERMISSIONS_REQUEST_LOCATION = 1000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        proximityTextView = (TextView) findViewById(R.id.tv_proximity);
// Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
//
//        PMPMapSDK.setLaunchDetailCallback(new LaunchDetailCallback() {
//            @Override
//            public void onLaunchDetailClicked(String poiID, MapState mapState) {
//                Intent intent = new Intent(MainActivity.this, POIDetailActivity.class);
//                intent.putExtra(POIDetailActivity.POI_ID, poiID);
//                intent.putExtra(POIDetailActivity.MAP_STATE, mapState);
//                startActivity(intent);
//
//
//            }
//        });
//
//
//        PMPMapSDK.setProximityCallback(new ProximityCallback() {
//
//            @Override
//            public void onProximityEnterRegion(int proximityId, double rssi, double distance, boolean isBackground) {
//                final int final_proximityId = proximityId;
//                if (isBackground) {
//                    Notification notification = new NotificationCompat.Builder(MainActivity.this)
//                            .setSmallIcon(R.drawable.swire_pin)
//                            .setContentText("Proximity:" + proximityId)
//                            .setOnlyAlertOnce(true)
//                            .build();
//                    NotificationManager mNotifyMgr =
//                            (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//                    mNotifyMgr.notify(1, notification);
//                }
//
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        proximityTextView.setVisibility(View.VISIBLE);
//                        proximityTextView.setText("Proximity:" + final_proximityId);
//                    }
//                });
//            }
//
//            @Override
//            public void onProximityExitRegion(int proximityId) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        proximityTextView.setVisibility(View.GONE);
//                    }
//                });
//            }
//
//        });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

//    public void openMap(View view) {
//        /*
//        Intent goMap = new Intent(MainActivity.this, PMPMapActivity.class);
////        goMap.putExtra(PMPMapActivity.DEBUG_ENABLE, true);
//        startActivity(goMap);
//        */
//        Class<? extends Activity> cls = MapActivity.class;
//        cls = PMPMapActivityV2.class;
//        Intent intent = new Intent(this, cls);
//        startActivity(intent);
//    }
//
//    public void navToPOI(View view) {
//        Intent intent = new Intent(this, POIListActivity.class);
//        startActivity(intent);
//    }
//
//    /*
//    public void openMapEng(View view) {
//        PMPMapSDK.setLangID(PMPMapSDK.Language_English);
//        openMap(view);
//    }
//
//    public void openMapChi(View view) {
//        PMPMapSDK.setLangID(PMPMapSDK.Language_TraditionalChinese);
//        openMap(view);
//    }
//
//    public void openMapSChi(View view) {
//        PMPMapSDK.setLangID(PMPMapSDK.Language_SimplifiedChinese);
//        openMap(view);
//    }*/
//
//
//    public void openFloorSwitch(View view) {
//        Intent intent = new Intent(MainActivity.this, FloorSwitchFilterActivity.class);
//        startActivity(intent);
//    }
}
