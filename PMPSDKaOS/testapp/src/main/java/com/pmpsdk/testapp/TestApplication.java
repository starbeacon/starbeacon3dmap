package com.pmpsdk.testapp;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.pmp.mapsdk.location.PMPCore;

import io.fabric.sdk.android.Fabric;


public class TestApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        PMPCore.getShared(this).start(44);
        PMPCore.getShared(this).getBeaconType();
        PMPCore.getShared(this).getLocation();
//        PMPMapSDK.onApplicationCreate(this);
//        PMPMapSDK.setMainActivityClass(com.hkia.myflight.MainActivity.class);
//        SharedPreferences.Editor editor = PMPUtil.getSharedPreferences(this).edit();
//        editor.putString("MtelUserId", "0");
//        editor.putString("MtelPushToken", "3789191793068265068");
//        editor.commit();
    }

}
