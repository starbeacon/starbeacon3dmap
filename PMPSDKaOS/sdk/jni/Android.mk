LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
include ../native/jni/OpenCV.mk

LOCAL_MODULE := KF

LOCAL_LDFLAGS := -Wl,--allow-multiple-definition

LOCAL_SRC_FILES := kf.cpp

LOCAL_C_INCLUDES += $(LOCAL_PATH)

include $(BUILD_SHARED_LIBRARY)
