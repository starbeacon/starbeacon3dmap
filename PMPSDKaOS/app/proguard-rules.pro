-keepattributes *Annotation*
-keepattributes EnclosingMethod
-keepattributes InnerClasses

-keep public class com.google.vending.licensing.ILicensingService
-keep public class com.android.vending.licensing.ILicensingService

# For native methods, see http://proguard.sourceforge.net/manual/examples.html#native
-keepclasseswithmembernames class * {
    native <methods>;
}

# keep setters in Views so that animations can still work.
# see http://proguard.sourceforge.net/manual/examples.html#beans
-keepclassmembers public class * extends android.view.View {
	public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
   void set*(***);
   *** get*();
}

# We want to keep methods in Activity that could be used in the XML attribute onClick
-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}

# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

# -keep class com.pmp.mapsdk.app.** { *; }
# -keep class com.cherrypicks.pmpmap.** { *; }

-keep class org.cocos2dx.lib.** { *; }
-keep class org.cocos2dx.cpp.** { *; }
-keep class com.cherrypicks.pmpmap.core.CoreEngine { *; }
-keep class com.cherrypicks.pmpmap.sensor.MotionManager { *; }
-keep class com.cherrypicks.pmpmap.core.RouteStepIndicator { *; }
-keep class com.cherrypicks.pmpmap.PMPMapController { *; }
-keep class com.cherrypicks.pmpmap.PMPMapConfig { *; }
-keep class com.cherrypicks.pmpmap.datamodel.** { *; }
-keep class com.pmp.mapsdk.cms.model.** { *; }
-keep class com.pmp.mapsdk.cms.PMPPOIByDuration { *; }
-keep class com.pmp.mapsdk.location.PMPBeacon { *; }
-keep class com.pmp.mapsdk.location.Trilateration.** { *; }

-keep class com.pmp.mapsdk.external.** { *; }
-keep class com.pmp.mapsdk.external.PMPMapSDK { *; }
-keep class com.cherrypicks.pmpmap.Coord { *; }
-keep public interface com.pmp.mapsdk.external.PMPMapSDK$* { *; }
-keep public interface com.cherrypicks.pmpmap.PMPMapControllerCallback { *; }
-keep public interface com.cherrypicks.pmpmap.PMPAugmentedRealityCallback { *; }
-keep public interface com.cherrypicks.pmpmap.tts.AndroidTTSManagerBridge { *; }
-keep public interface com.cherrypicks.pmpmap.core.CoreEngineListener { *; }
-keep class com.cherrypicks.pmpmap.core.VertexByDuration { *; }

-keep class com.cherrypicks.pmpmap.analytics.** { *; }

-keep class com.pmp.mapsdk.app.PMPMapActivity {
    public final static <fields>;
    public *;
    protected *;
}

-keep class com.pmp.mapsdk.app.PMPMapFragment {
    public final static <fields>;
    public *;
    protected *;
}

-keep class com.pmp.mapsdk.app.PMPPOIDetailFragment {
    public final static <fields>;
    public *;
    protected *;
}

-keep class com.pmp.mapsdk.app.PMPBrowserFragment {
    public final static <fields>;
    public *;
    protected *;
}

-keep class com.nostra13.universalimageloader.** { *; }

-keep public class **.R {
  public *;
}
-keep public class **.R$* {
  public *;
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers class **.R$* {
    public static <fields>;
}

# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version.  We know about them, and they are safe.
-dontwarn android.support.**

-keepclassmembers public class org.springframework {
    public *;
}

-keep class com.facebook.** {
   *;
}

-keep public class com.groupon.inject.JsonConverter
-keep class com.flurry.** { *; }
-dontwarn com.flurry.**
-keepattributes Signature
-dontwarn org.apache.http.**
-dontwarn org.springframework.**
-dontwarn org.apache.log4j.**
-dontwarn org.apache.commons.logging.**
-dontwarn org.apache.commons.codec.binary.**
-dontwarn android.net.http.**
-keep class android.net.http.SslError
-dontwarn com.google.android.maps.**
-dontwarn javax.activation.**
-dontwarn java.beans.**
-dontwarn javax.xml.**
-dontwarn javax.ws.**
-dontwarn org.apache.commons.**
-dontwarn org.joda.time.**
-dontwarn org.springframework.**
-dontwarn org.w3c.dom.**
-dontskipnonpubliclibraryclasses
-dontskipnonpubliclibraryclassmembers
-keep public class * extends android.content.ContentProvider



# These options are the minimal options for a functioning application
# using Proguard and the AWS SDK 2.1.5 for Android

-keep class org.apache.commons.logging.**               { *; }
-keep class com.amazonaws.org.apache.commons.logging.** { *; }
-keep class com.amazonaws.services.sqs.QueueUrlHandler  { *; }
-keep class com.amazonaws.javax.xml.transform.sax.*     { public *; }
-keep class com.amazonaws.javax.xml.stream.**           { *; }
-keep class com.amazonaws.services.**.model.*Exception* { *; }
-keep class com.amazonaws.internal.**                   { *; }
-keep class org.codehaus.**                             { *; }
-keep class org.joda.time.tz.Provider                   { *; }
-keep class org.joda.time.tz.NameProvider               { *; }
-keepattributes Signature,*Annotation*,EnclosingMethod
-keepnames class com.fasterxml.jackson.** { *; }
-keepnames class com.amazonaws.** { *; }

-dontwarn com.fasterxml.jackson.databind.**
-dontwarn javax.xml.stream.events.**
-dontwarn org.codehaus.jackson.**
-dontwarn org.apache.commons.logging.impl.**
-dontwarn org.apache.http.conn.scheme.**
-dontwarn org.apache.http.annotation.**
-dontwarn org.ietf.jgss.**
-dontwarn org.joda.convert.**
-dontwarn com.amazonaws.org.joda.convert.**
-dontwarn org.w3c.dom.bootstrap.**

#SDK split into multiple jars so certain classes may be referenced but not used
-dontwarn com.amazonaws.services.s3.**
-dontwarn com.amazonaws.services.sqs.**

-dontnote com.amazonaws.services.sqs.QueueUrlHandler

-dontwarn org.apache.http.annotation.**
-dontwarn com.amazonaws.**
