package com.cherrypicks.pmpmap;

import com.pmp.mapsdk.app.PMPMapFragment;

/**
 * Created by dickywong on 6/13/17.
 */

public interface PMPMapControllerWithFragment {
        public void onPOISelect(int poiId) ;
        public void onDeselectAllPOI() ;
        public void onMapCoordUpdate(Coord coord) ;
        public void onMapSceneInitated(PMPMapFragment mapFragment);
}
