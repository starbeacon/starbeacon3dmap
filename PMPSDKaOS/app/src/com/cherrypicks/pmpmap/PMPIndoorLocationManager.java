package com.cherrypicks.pmpmap;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.cherrypicks.pmpmapsdk.BuildConfig;
import com.pmp.mapsdk.app.PMPPermissionListener;
import com.pmp.mapsdk.cms.model.Devices;
import com.pmp.mapsdk.cms.model.ResponseData;
import com.pmp.mapsdk.external.PMPMapSDK;
import com.pmp.mapsdk.location.PMPApplication;
import com.pmp.mapsdk.location.PMPBackgroundStatus;
import com.pmp.mapsdk.location.PMPBeacon;
import com.pmp.mapsdk.location.PMPBeaconType;
import com.pmp.mapsdk.location.PMPLocation;
import com.pmp.mapsdk.location.PMPLocationManager;
import com.pmp.mapsdk.location.PMPLocationManagerError;
import com.pmp.mapsdk.location.PMPLocationManagerNotifier;

import org.altbeacon.beacon.BeaconManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alan on 16/2/16.
 */
public class PMPIndoorLocationManager implements PMPLocationManagerNotifier {
    private static final String TAG = "PMPManager";
    public static final String BROADCAST_INVALID_SESSION = "BROADCAST_INVALID_SESSION";
//    public static final String BROADCAST_PROXIMITY_ENTER_ZONE = "BROADCAST_PROXIMITY_ENTER_ZONE";
//    public static final String BROADCAST_PROXIMITY_OUT_ZONE = "BROADCAST_PROXIMITY_OUT_ZONE";
    public static final String BROADCAST_ZONE_ID = "BROADCAST_ZONE_ID";
    public static final String BROADCAST_IS_BACKGROUND = "BROADCAST_IS_BACKGROUND";

    public interface IndoorLocationListener{
        public void didIndoorLocationUpdated(PMPBeaconType beaconType, PMPLocation location);

        public void didIndoorTransmissionsUpdated(List<PMPBeacon> transmissions);

        public void didOutdoorLocationsUpdated();

        public void didIndoorExitRegion();

        public void didCompassUpdated(double direction);

        public void didError(PMPLocationManagerError error);
    }

    private static PMPIndoorLocationManager instance;
    private PMPApplication pmpApp;
    private Context mContext;
    private IndoorLocationListener locationListener;
    private PMPLocationManager mgr;
    private Handler handler;


    public static PMPIndoorLocationManager getSharedPMPManager(Context mContext){
        if(instance == null){
            instance = new PMPIndoorLocationManager();
            instance.pmpApp = new PMPApplication();
            instance.mContext = mContext.getApplicationContext();
            instance.mgr = PMPLocationManager.getShared(mContext);
            instance.handler = new Handler(mContext.getMainLooper());
        }

        return instance;
    }

    public void setLocationListener(IndoorLocationListener locationListener) {
        this.locationListener = locationListener;
    }
    private int myActivityCount = 0;
    public void onCreate(Application application) {
        initBeacon();
        pmpApp.onCreate(application);
        application.registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                pmpApp.onActivityCreated(activity, savedInstanceState);
//                myActivityCount++;
//                if( myActivityCount == 1){
//                    SharedPreferences sharedPreferences = PMPMapSDK.getApplication().getSharedPreferences("connection_changed" , MODE_PRIVATE);
//                    if(sharedPreferences.getBoolean("connection_changed_key" , false)) {
//                        initBeacon();
//                    }
//                    sharedPreferences.edit().putBoolean("connection_changed_key", false).apply();
//                }
            }

            @Override
            public void onActivityStarted(Activity activity) {
                pmpApp.onActivityStarted(activity);
            }

            @Override
            public void onActivityResumed(Activity activity) {
                pmpApp.onActivityResumed(activity);

            }

            @Override
            public void onActivityPaused(Activity activity) {
                pmpApp.onActivityPaused(activity);

            }

            @Override
            public void onActivityStopped(Activity activity) {
                pmpApp.onActivityStopped(activity);

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
                pmpApp.onActivitySaveInstanceState(activity, outState);

            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                pmpApp.onActivityDestroyed(activity);
                myActivityCount--;
            }
        });


    }

    private List<PMPBeacon> getHardcodeBeacons(){
        List<PMPBeacon> list = new ArrayList<PMPBeacon>();

//        Beacons demoBeacon = new Beacons();
//        demoBeacon.setMap_id(0);
//        demoBeacon.setuuid(0);
//        demoBeacon.setUuid("8eb1c3f4-176b-433c-ada8-d4371c8e7b41");
//        demoBeacon.setMajor_no(520);
//        demoBeacon.setMap_device_type_id(PMPBeaconType.PMPBeaconTypePush.getValue());
//
//        PMPBeacon beacon = new PMPBeacon(demoBeacon);

//        list.add(beacon);

        return list;
    }

    public void initBeacon(){
        List<PMPBeacon> list = new ArrayList<PMPBeacon>();

        list.addAll(getHardcodeBeacons());

        PMPLocationManager mgr = PMPLocationManager.getShared(mContext);
        mgr.setEnableLog(false);
        mgr.addDelegate(this);
        BeaconManager.distanceModelUpdateUrl = BuildConfig.BaseURL + "android-distance.json";//"http://data.altbeacon.org/android-distance.json";

        mgr.setProximityUUIDS(list);
        startDetection();

        downloadBeaconDataAndInit();
   }

    private void downloadBeaconDataAndInit(){
        PMPLocationManager mgr = PMPLocationManager.getShared(mContext);
        mgr.stop();
        PMPDataManager.getSharedPMPManager(mContext).downloadData(new PMPDataManager.DownloadDataCallback() {
            @Override
            public void didSuccess(ResponseData responseData, boolean isCached) {

                List<PMPBeacon> list = new ArrayList<PMPBeacon>();
                for (Devices device : responseData.getDevices()) {
                    if(device.getBleWayFindingUuid() != null){

                        PMPBeacon beacon = new PMPBeacon(device, device.getBleWayFindingUuid(), responseData.getMapScale());
                        list.add(beacon);
                    }

                    if(device.getBleZonalPushUuid() != null){
                        PMPBeacon beacon = new PMPBeacon(device, device.getBleZonalPushUuid(), 2, responseData.getMapScale());
                        list.add(beacon);
                    }
                }

                list.addAll(getHardcodeBeacons());

                PMPLocationManager mgr = PMPLocationManager.getShared(mContext);
                mgr.setEnableLog(true);
                mgr.addDelegate(PMPIndoorLocationManager.this);

                mgr.stop();

                mgr.setProximityUUIDS(list);

                setFloorSwitchingFilters(responseData.getAosChangeFloorFilters());

                startDetection();
            }

            @Override
            public void didFailure() {
                Log.i(TAG, "Data Download fail...");
            }
        });
    }

//    public BaseDevices convertDevicetoProximityDevice(Devices device){
//        ArrayList<BaseNodes> baseNodes = new ArrayList<BaseNodes>();
//        for (Nodes node : device.getNodes()){
////                            double id, double mapId, ArrayList<Integer> poiIds, double status, double x, double y, double z
//            BaseNodes pmpNode = new BaseNodes(node.getId(), node.getMapId(), node.getPoiIds(), node.getStatus(), node.getX(), node.getY(), node.getZ());
//            baseNodes.add(pmpNode);
//        }
////                        ArrayList<PointF> nearbyOutdoorNodes, String nearbyOutdoorNodesString, ArrayList<BaseNodes> nodes, String outdoorPathRatio
//        BaseDeviceOptions baseDeviceOptions = null;
//        if(device.getDeviceOptions() != null){
//            baseDeviceOptions = new BaseDeviceOptions(device.getDeviceOptions().getNearbyOutdoorNodes(), device.getDeviceOptions().getNearbyOutdoorNodesString(), baseNodes, device.getDeviceOptions().getOutdoorPathRatio());
//        }
//
//        BaseDevices baseDevices = new BaseDevices(device.getDeviceGroupId(), device.getAaZoneId(), device.getBleWayFindingUuid(), device.getBleWayFindingUuidId(), device.getBleZonalPushUuid(), device.getBleZonalPushUuidId(), device.getDeviceName(), baseDeviceOptions,
//                device.getDeviceTypes(), device.getId(), device.getMajorNo(), device.getMapDeviceTypeId(), device.getMapId(), baseNodes, device.getStatus(), device.getThreshold(), device.getUuidMap(), device.getX(), device.getY(), device.getZ(), device.getZoneId());
//
//        return baseDevices;
//    }

    public PMPApplication getPmpApplication() {
        return pmpApp;
    }

    /**
     * Typically you dont need to manually call startDetection unless you
     * have stop the detection by yourself
     */
    public void startDetection() {
        PMPLocationManager mgr = PMPLocationManager.getShared(mContext);
        GPSLocationManager gpsMgr = GPSLocationManager.getSharedManager(mContext);
        if(gpsMgr.checkIfLocationPermissionAllowed(mContext, new PMPPermissionListener() {
            @Override
            public void permissionResult(boolean granted) {
                if (granted) {
                    PMPLocationManager mgr = PMPLocationManager.getShared(mContext);
                    mgr.stop();
                    mgr.start();
                }
            }
        })) {
            mgr.stop();
            mgr.start();
        }
    }

//    public void requireLocationPermissionIfNeeded(Activity activity) {
//        GPSLocationManager gpsMgr = GPSLocationManager.getSharedManager(mContext);
//
//        gpsMgr.requireLocationPermissionIfNeeded(activity);
//    }


    public void setEnableLog(boolean enableLog){
        mgr.setEnableLog(enableLog);
    }

    public void setEnableLowPassFilter(boolean enableLowPassFilter) {
        mgr.setEnableLowPassFilter(enableLowPassFilter);
    }

    public void setEnableMaxNumOfBeaconsDetection(boolean enableMaxNumOfBeaconsDetection) {
        mgr.setEnableMaxNumOfBeaconsDetection(enableMaxNumOfBeaconsDetection);
    }

    public void setEnableMovingAverage(boolean enableMovingAverage) {
        mgr.setEnableMovingAverage(enableMovingAverage);
    }

    public void setEnableBeaconAccuracyFilter(boolean enableBeaconAccuracyFilter) {
        mgr.setEnableBeaconAccuracyFilter(enableBeaconAccuracyFilter);
    }

    public void setBeaconAccuracyFilter(int beaconAccuracyFilter){
        mgr.setBeaconAccuracyFilter(beaconAccuracyFilter);
    }

    public void setMaxNumOfBeaconsDetection(int maxNumOfBeaconsDetection) {
        mgr.setMaxNumOfBeaconsDetection(maxNumOfBeaconsDetection);
    }

    public void setNumOfMovingAverageSample(int numOfMovingAverageSample) {
        mgr.setNumOfMovingAverageSample(numOfMovingAverageSample);
    }

    public void setNumOfFloorBeaconDetection(int numOfFloorBeaconDetection) {
        mgr.setNumOfFloorBeaconDetection(numOfFloorBeaconDetection);
    }

    public void setLowPassFilterFactor(float lowPassFilterFactor) {
        mgr.setLowPassFilterFactor(lowPassFilterFactor);
    }

    public void setEnableFloorSwitchingFilters(boolean floorSwitchPredict) {
        mgr.setEnableFloorSwitchingFilters(floorSwitchPredict);
    }

    public void setFloorSwitchingFilters(ArrayList<Integer> filterList) {
        mgr.setFloorSwitchingFilters(filterList);

        String data = "";
        for(int filter : filterList){
            data += filter + ",";
        }
        SharedPreferences preferences = mContext.getSharedPreferences("PREF_PMP_LOCATION_SETTINGS", Context.MODE_PRIVATE);
        preferences.edit().putString(mContext.getString(map.enterprise.keewee.com.locationmanager.R.string.Preference_Floor_Predict), data).commit();
    }

    public void setEnableWalkingDetection(boolean enableWalkingDetection) {
        mgr.setEnableWalkingDetection(enableWalkingDetection);
    }

    public void setEnableKalmanFilter(boolean enableKalmanFilter){
        mgr.setEnableKalmanFilter(enableKalmanFilter);
    }

    /*
     *     Location callback
     */
    @Override
    public void didIndoorLocationUpdated(final PMPBeaconType beaconType, final PMPLocation location) {
//        HashMap<String, Object> param = new HashMap<>();
//        param.put("computedX", location.getX());
//        param.put("computedY", location.getY());
//        AnalyticsLogger.getInstance().logEvent("ENGINE_LOCATION_UPDATE", param);
        if(locationListener !=null){
            if(location.getX() != 0 && location.getY() != 0){
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        locationListener.didIndoorLocationUpdated(beaconType, location);
                    }
                });
            }
        }
    }

    @Override
    public void didIndoorTransmissionsUpdated(List<PMPBeacon> transmissions) {
//        StringBuffer sb = new StringBuffer();
//        for (int i = 0 ; i < transmissions.size() ; i++) {
//            PMPBeacon beacon = transmissions.get(i);
//            sb.append(String.format("%d-%d", beacon.getRssi(), beacon.getMajorIntValue()));
//            if (i != transmissions.size() - 1) {
//                sb.append(',');
//            }
//        }
//        HashMap<String, Object> param = new HashMap<>();
//        param.put("beacons", sb.toString());
//        AnalyticsLogger.getInstance().logEvent("ENGINE_BEACON_UPDATE");

        if(locationListener !=null)locationListener.didIndoorTransmissionsUpdated(transmissions);
    }

    @Override
    public void didIndoorExitRegion() {
        if(locationListener !=null)locationListener.didIndoorExitRegion();
    }

    @Override
    public void didOutdoorLocationsUpdated() {
        if(locationListener !=null)locationListener.didOutdoorLocationsUpdated();
    }

    @Override
    public void didCompassUpdated(double direction) {
        if(locationListener !=null)locationListener.didCompassUpdated(direction);
    }

    @Override
    public void didError(PMPLocationManagerError error) {
        if(locationListener !=null)locationListener.didError(error);
    }

    public void didProximityEnterRegion(int region, int minor, double rssi, double distance) {
        PMPBackgroundStatus bgStatus = getPmpApplication().getBgStatus();
//        if(bgStatus == PMPBackgroundStatus.Background){
//            Intent notificationIntent = new Intent(mContext, PMPMapActivity.class);
//            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//
//            TaskStackBuilder stackBuilder = TaskStackBuilder.create(mContext);
//            stackBuilder.addParentStack(PMPMapActivity.class);
//            stackBuilder.addNextIntent(notificationIntent);
//
////            PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//            PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//            NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext);
//
//            Notification notification = builder.setContentTitle("Demo App Notification")
//                    .setContentText("New Notification From Demo App..")
//                    .setTicker("New Message Alert!")
//                    .setSmallIcon(R.mipmap.ic_launcher)
//                    .setContentIntent(pendingIntent).build();
//
//            NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(0, notification);
//        }

        pmpApp.notifyProximityEnterRegionCallback(region, minor, rssi, distance, bgStatus == PMPBackgroundStatus.Background);

//        Intent i = new Intent(PMPIndoorLocationManager.BROADCAST_PROXIMITY_ENTER_ZONE);
//        i.putExtra(PMPIndoorLocationManager.BROADCAST_ZONE_ID, region);
//        i.putExtra(PMPIndoorLocationManager.BROADCAST_IS_BACKGROUND, bgStatus == PMPBackgroundStatus.Background);
//        mContext.sendBroadcast(i);
    }

    public void didProximityExitRegion(int region) {
//        Intent i = new Intent(PMPIndoorLocationManager.BROADCAST_PROXIMITY_OUT_ZONE);
//        i.putExtra(PMPIndoorLocationManager.BROADCAST_ZONE_ID, region);
//        mContext.sendBroadcast(i);

        if(PMPMapSDK.getProximityCallback() != null){
            PMPMapSDK.getProximityCallback().onProximityExitRegion(region);
        }
    }
}
