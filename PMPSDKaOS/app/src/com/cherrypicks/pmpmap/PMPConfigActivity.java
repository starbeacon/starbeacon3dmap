package com.cherrypicks.pmpmap;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.SwitchPreference;
import android.util.Log;

import com.cherrypicks.pmpmap.core.CoreEngine;
import com.cherrypicks.pmpmapsdk.BuildConfig;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.location.PMPLocationManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Alan on 16/2/16.
 */
public class PMPConfigActivity extends PreferenceActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPreferenceManager().setSharedPreferencesName("PREF_PMP_LOCATION_SETTINGS");
        addPreferencesFromResource(R.xml.options);

        SharedPreferences sp = getPreferenceScreen().getSharedPreferences();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);

        Preference pref1 = findPreference( "version" );
        /*
        try {
            pref1.setSummary(BuildConfig.VERSION_NAME);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }*/
        pref1.setSummary(BuildConfig.VERSION_NAME + "(" + BuildConfig.VERSION_CODE+ ")\n" + BuildConfig.BaseURL);
        SwitchPreference p = (SwitchPreference)getPreferenceManager().findPreference("Record");
        p.setChecked(PMPLocationManager.getShared().isRecording());
        p = (SwitchPreference)getPreferenceManager().findPreference("Playback");
        p.setChecked(PMPLocationManager.getShared().isPlayback());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }
    }

    public String appVersion() throws PackageManager.NameNotFoundException {
        PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        String version = pInfo.versionName;
        return version;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        PMPIndoorLocationManager mgr = PMPIndoorLocationManager.getSharedPMPManager(this);
        PMPMapConfig mapConfig = PMPMapConfig.getSharedConfig();
        if(key.equals(getString(R.string.Preference_EnableLog))){
            mgr.setEnableLog(sharedPreferences.getBoolean(key, false));
        }else if(key.equals(getString(R.string.Preference_EnableLowPassFilter))){
            mgr.setEnableLowPassFilter(sharedPreferences.getBoolean(key, false));
        }else if(key.equals(getString(R.string.Preference_EnableMaxNumOfBeaconsDetection))){
            mgr.setEnableMaxNumOfBeaconsDetection(sharedPreferences.getBoolean(key, false));
        }else if(key.equals(getString(R.string.Preference_EnableMovingAverage))){
            mgr.setEnableMovingAverage(sharedPreferences.getBoolean(key, false));
        }else if(key.equals(getString(R.string.Preference_EnableBeaconAccuracyFilter))){
            mgr.setEnableBeaconAccuracyFilter(sharedPreferences.getBoolean(key, false));
        }else if(key.equals(getString(R.string.Preference_EnableKalmanFilter))){
            mgr.setEnableKalmanFilter(sharedPreferences.getBoolean(key,false));
        }else if (key.equals(getString(R.string.Preference_Record))) {
            if (sharedPreferences.getBoolean(key, false)) {
                if (sharedPreferences.getBoolean(getString(R.string.Preference_Playback), false)) {
                    PMPLocationManager.getShared().stopPlayBack();
                }
                PMPLocationManager.getShared().startRecording();
                finish();
            } else {
                String record = PMPLocationManager.getShared().stopRecording();
                if (record != null && record.length() > 0) {
                    writeFile(record);
                    // TODO: 5/5/2017 add prompt to input name
//                    LayoutInflater li = LayoutInflater.from(this);
//                    View promptsView = li.inflate(R.layout.pmp_input_alert, null);
//
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
//                            this);
//
//                    // set prompts.xml to alertdialog builder
//                    alertDialogBuilder.setView(promptsView);
//
//                    final EditText userInput = (EditText) promptsView
//                            .findViewById(R.id.editTextDialogUserInput);
//
//                    // set dialog message
//                    alertDialogBuilder
//                            .setCancelable(false)
//                            .setPositiveButton("OK",
//                                    new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog,int id) {
//                                            // get user input and set it to result
//                                            // edit text
//                                            String name = "Default Name";
//                                            try {
//                                                name = userInput.getText().toString();
//                                            } catch (Exception e) {
//
//                                            }
//                                            SharedPreferences userDefaults = getSharedPreferences("PREF_PMP_LOCATION_SETTINGS", 0);
//                                        }
//                                    })
//                            .setNegativeButton("Cancel",
//                                    new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog,int id) {
//                                            dialog.cancel();
//                                        }
//                                    });
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();
                }
            }
            SwitchPreference p = (SwitchPreference)getPreferenceManager().findPreference("Record");
            p.setChecked(PMPLocationManager.getShared().isRecording());
            p = (SwitchPreference)getPreferenceManager().findPreference("Playback");
            p.setChecked(PMPLocationManager.getShared().isPlayback());
        } else if (key.equals(getString(R.string.Preference_Playback))) {
            if (sharedPreferences.getBoolean(key, false)) {
                if (sharedPreferences.getBoolean(getString(R.string.Preference_Record), false)) {
                    String record = PMPLocationManager.getShared().stopRecording();
                    if (record != null && record.length() > 0) {
                        Log.e("PMPConfigActivity", "Write record " + record);
                        writeFile(record);
                    }
                }
                String record = readFileAsString();
                if (record != null && record.length() > 0) {
                    PMPLocationManager.getShared().startPlayBack(record);
                    this.finish();
                }
            } else {
                PMPLocationManager.getShared().stopPlayBack();
            }
            SwitchPreference p = (SwitchPreference)getPreferenceManager().findPreference("Record");
            p.setChecked(PMPLocationManager.getShared().isRecording());
            p = (SwitchPreference)getPreferenceManager().findPreference("Playback");
            p.setChecked(PMPLocationManager.getShared().isPlayback());
        }

        if(key.equals(getString(R.string.Preference_MaxNumOfBeaconsDetection))){
            mgr.setMaxNumOfBeaconsDetection(sharedPreferences.getInt(key, 0));
        }else if(key.equals(getString(R.string.Preference_NumOfMovingAverageSample))){
            mgr.setNumOfMovingAverageSample(sharedPreferences.getInt(key, 0));
        }else if(key.equals(getString(R.string.Preference_NumOfFloorBeaconDetection))){
            mgr.setNumOfFloorBeaconDetection(sharedPreferences.getInt(key, 3));
        } if(key.equals(getString(R.string.Preference_LowPassFilterFactor))){
            mgr.setLowPassFilterFactor(sharedPreferences.getFloat(key, 0));
        }

        if(key.equals(getString(R.string.Preference_EnableFloorSwitchingFilters))){
            mgr.setEnableFloorSwitchingFilters(sharedPreferences.getBoolean(key, false));
        }else if(key.equals(getString(R.string.Preference_Floor_Predict))){
//            String data = sharedPreferences.getString(key, "");
//            String[] filters = data.split(",");
//            ArrayList<Integer> marjorFilter = new ArrayList<>();
//
//            for(String filter : filters){
//                marjorFilter.add(Integer.parseInt(filter.trim()));
//            }
//
//            mgr.setFloorSwitchingFilters(marjorFilter);

        }else if(key.equals(getString(R.string.Preference_BeaconAccuracyFilter))){
            mgr.setBeaconAccuracyFilter(sharedPreferences.getInt(key, 0));
        }else if (key.equals(getString(R.string.Preference_WalkingDistanceBuffer))) {
            float val = (float)sharedPreferences.getFloat(key, 0) / 100.0f;
            mapConfig.setWalkingDistanceBuffer(val);
        }else if (key.equals(getString(R.string.Preference_Debug))) {
            CoreEngine.getInstance().setDebug(sharedPreferences.getBoolean(key, false));
        }else if (key.equals(getString(R.string.Preference_DestinationThreshold))) {
            float val = (float)sharedPreferences.getFloat(key, 0);
            CoreEngine.getInstance().setDestinationThreshold(val);
        }else if (key.equals(getString(R.string.Preference_PassDestinataionThreshold))) {
            float val = (float)sharedPreferences.getFloat(key, 0);
            CoreEngine.getInstance().setBypassDestinationThreshold(val);
        }else if (key.equals(getString(R.string.Preference_RerouteThreshold))) {
            float val = (float)sharedPreferences.getFloat(key, 0);
            CoreEngine.getInstance().setRerouteThreshold(val);
        }else if(key.equals(getString(R.string.Preference_PathForeSeeingDistance))){
//            pathForeSeeingDistance
            int val = sharedPreferences.getInt(key, 0);
            CoreEngine.getInstance().setPathForeSeeingDistance(val);
        }else if (key.equals(getString(R.string.Preference_DirectionPredictionMode))) {
            int val = sharedPreferences.getInt(key, 0);
            CoreEngine.getInstance().setDirectionPredictionMode(val);
        }

        /*
                   pmpSetEnableDebug(preferences.getBoolean(fragment.getActivity().getString(R.string.Preference_Debug), false));
            pmpSetDestinationThreshold(preferences.getFloat(fragment.getActivity().getString(R.string.Preference_DestinationThreshold), nativeMapConfig.getDestinationThreshold()));
            pmpSetPassDestinataionThreshold(preferences.getFloat(fragment.getActivity().getString(R.string.Preference_PassDestinataionThreshold), nativeMapConfig.getPassDestinationAlertThreadhols()));
            pmpSetRerouteThreshold(preferences.getFloat(fragment.getActivity().getString(R.string.Preference_RerouteThreshold), nativeMapConfig.getRerouteThreshold()));
         */
    }

    public static boolean canWriteOnExternalStorage() {
        // get the state of your external storage
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // if storage is mounted return true
            return true;
        }
        return false;
    }

    public String readFileAsString() {
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        BufferedReader in = null;
        try {
            File sdcard = Environment.getExternalStorageDirectory();
            // to this path add a new directory path
            File dir = new File(sdcard.getAbsolutePath() + "/pmp-log/");
            // create the file in which we will write the contents
            File file = new File(dir, "log.csv");
            in = new BufferedReader(new FileReader(file));
            char[] inputBuffer = new char[2048];
            int l;
            // FILL BUFFER WITH DATA
            while ((l = in.read(inputBuffer)) != -1) {
                stringBuilder.append(inputBuffer, 0, l);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return stringBuilder.toString();
    }

    public void writeFile(String record) {
        // get the path to sdcard
        File sdcard = Environment.getExternalStorageDirectory();
        // to this path add a new directory path
        File dir = new File(sdcard.getAbsolutePath() + "/pmp-log/");
        // create this directory if not already created
        dir.mkdir();
        // create the file in which we will write the contents
        File file = new File(dir, "log.csv");
        FileOutputStream outStream = null;
        try {
            outStream = new FileOutputStream(file);
            outStream.write(record.getBytes());
            outStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
