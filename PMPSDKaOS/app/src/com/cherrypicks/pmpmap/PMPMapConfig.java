package com.cherrypicks.pmpmap;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.cherrypicks.pmpmap.datamodel.NativeMapConfig;
import com.cherrypicks.pmpmap.datamodel.PathResult;
import com.cherrypicks.pmpmap.utils.JavaHelper;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.app.PMPMapActivity;
import com.pmp.mapsdk.app.PMPMapFragment;
import com.pmp.mapsdk.app.PMPMapFragmentDeprecated;
import com.pmp.mapsdk.location.PMPBeacon;
import com.pmp.mapsdk.location.PMPLocation;
import com.pmp.mapsdk.utils.PMPUtil;

import org.cocos2dx.lib.Cocos2dxGLSurfaceView;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Alan on 16/2/16.
 */
public class PMPMapConfig {
    private static final String TAG = "PMPMapConfig";
    public static final int PMPMapModeBrowsing = 0;
//    public static final int PMPMapModeBrowsingWithCompass = 1;
    public static final int PMPMapModeNavigating = 1;
//    public static final int PMPMapModeNavigatingWithCompass = 3;
    public static final int PMPMapModeLocateMe = 2;
//    public static final int PMPMapModeLocateMeWithCompass = 5;

    public static final int NavigationStep_NotNavigating = 0;
    public static final int NavigationStep_OnTheWay = 1;
    public static final int NavigationStep_Arrived = 2;
    public static final int NavigationStep_ByPassed = 3;

    private static String SettingEnableARNavigation = "EnableARNavigation";
    private static String SettingShowARReminder = "ShowARReminder";

    public interface MapEngineInitialFinishCallback{
         void onMapEngineInitialFinish();
    }

    public interface MapModeChangeCallback{
        void onMapModeChanged(int mode);
    }

    public interface ArrivedDestinationCallback{
        void onArrivedDestination();

        void onBypassedDestination();
    }

    public abstract static class POICallback{
        public abstract void onSelectPOI(int poi_id);

        public abstract void onDeselectPOI();

        public abstract void onSelectPOIPin(int poi_id);
    }

    public interface StepMessageUpdateCallback{
        void onStepMessageUpdate(int mode, String message, String poiMessage, String iconImageName, float totalDutation, float totalDistance);
    }

    private static PMPMapConfig instance;

    private MapEngineInitialFinishCallback mapEngineInitialFinishCallback;
    private MapModeChangeCallback mapModeChangeCallback;
    private ArrivedDestinationCallback arrivedDestinationCallback;
    private StepMessageUpdateCallback stepMessageUpdateCallback;
    private POICallback poiCallback;
    private PMPMapFragmentDeprecated fragment;
    private boolean isInitFinish;
    private SharedPreferences preferences;

    public static PMPMapConfig getSharedConfig(PMPMapFragmentDeprecated fragment){
        if(instance == null){
            instance = new PMPMapConfig();
        }
        instance.fragment = fragment;
        if (fragment != null) {
            instance.preferences = fragment.getActivity().getSharedPreferences("PREF_PMP_LOCATION_SETTINGS", 0);
        }
        return instance;
    }

    public static PMPMapConfig getSharedConfig() {
        if(instance == null){
            instance = new PMPMapConfig();
        }
        return instance;
    }

    public static PMPMapConfig getSharedConfig(Context context) {
        if(instance == null){
            instance = new PMPMapConfig();
        }
        if (instance.preferences == null ) {
            instance.preferences = context.getSharedPreferences("PREF_PMP_LOCATION_SETTINGS", 0);
        }
        return instance;
    }

    public boolean isInitFinish() {
        return isInitFinish;
    }

    public void setStepMessageUpdateCallback(StepMessageUpdateCallback stepMessageUpdateCallback) {
        this.stepMessageUpdateCallback = stepMessageUpdateCallback;
    }

    public void setMapEngineInitialFinishCallback(MapEngineInitialFinishCallback mapEngineInitialFinishCallback) {
        this.mapEngineInitialFinishCallback = mapEngineInitialFinishCallback;
    }

    public void setMapModeChangeCallback(MapModeChangeCallback mapModeChangeCallback) {
        this.mapModeChangeCallback = mapModeChangeCallback;
    }

    public void setArrivedDestinationCallback(ArrivedDestinationCallback arrivedDestinationCallback) {
        this.arrivedDestinationCallback = arrivedDestinationCallback;
    }

    public void setPoiCallback(POICallback poiCallback) {
        this.poiCallback = poiCallback;
    }


    public void applyNewConfig(){
        if(fragment != null){
            NativeMapConfig nativeMapConfig = pmpGetNativeMapConfig();

            pmpSetEnableDebug(preferences.getBoolean(fragment.getActivity().getString(R.string.Preference_Debug), false));
            pmpSetDestinationThreshold(preferences.getFloat(fragment.getActivity().getString(R.string.Preference_DestinationThreshold), nativeMapConfig.getDestinationThreshold()));
            pmpSetPassDestinataionThreshold(preferences.getFloat(fragment.getActivity().getString(R.string.Preference_PassDestinataionThreshold), nativeMapConfig.getPassDestinationAlertThreadhols()));
            pmpSetRerouteThreshold(preferences.getFloat(fragment.getActivity().getString(R.string.Preference_RerouteThreshold), nativeMapConfig.getRerouteThreshold()));

        }
    }

    public void loadDefaultConfigIfNeeded(){
        if(fragment != null){
            SharedPreferences.Editor editor = preferences.edit();
            NativeMapConfig nativeMapConfig = pmpGetNativeMapConfig();

            if(!preferences.contains(fragment.getActivity().getString(R.string.Preference_Debug))){
                editor.putBoolean(fragment.getActivity().getString(R.string.Preference_Debug), false);

                //This can also mean first time open app...
                PMPIndoorLocationManager locationManager = PMPIndoorLocationManager.getSharedPMPManager(fragment.getActivity());
                locationManager.setEnableLowPassFilter(true);
                locationManager.setEnableMaxNumOfBeaconsDetection(true);
                locationManager.setEnableBeaconAccuracyFilter(true);
                locationManager.setEnableFloorSwitchingFilters(true);
                locationManager.setLowPassFilterFactor(0.5f);
                locationManager.setMaxNumOfBeaconsDetection(10);
                locationManager.setBeaconAccuracyFilter(30);
                locationManager.setEnableWalkingDetection(true);
                locationManager.setEnableFloorSwitchingFilters(true);
            }

            if (nativeMapConfig != null) {
                if (!preferences.contains(fragment.getActivity().getString(R.string.Preference_DestinationThreshold))) {
                    editor.putFloat(fragment.getActivity().getString(R.string.Preference_DestinationThreshold), nativeMapConfig.getDestinationThreshold());
                }

                if (!preferences.contains(fragment.getActivity().getString(R.string.Preference_PassDestinataionThreshold))) {
                    editor.putFloat(fragment.getActivity().getString(R.string.Preference_PassDestinataionThreshold), nativeMapConfig.getPassDestinationAlertThreadhols());
                }

                if (!preferences.contains(fragment.getActivity().getString(R.string.Preference_RerouteThreshold))) {
                    editor.putFloat(fragment.getActivity().getString(R.string.Preference_RerouteThreshold), nativeMapConfig.getRerouteThreshold());
                }
            }

            editor.commit();
        }
    }

    private void runVoidOnGlThread(final Runnable runnable){
        if(!isInitFinish){
            return;
        }

        if(fragment != null && fragment.isVisible() && !fragment.isDetached()){
//            final CountDownLatch latch = new CountDownLatch(1);

            fragment.runOnGLThread(new Runnable() {
                @Override
                public void run() {
                    runnable.run();
//                    latch.countDown();
                }
            });

//            try {
//                latch.await();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
        }
    }

    private Object runOnGlThread(String methodName, final Object... param){
        if(!isInitFinish){
            return null;
        }
        if(fragment != null && fragment.isVisible() && !fragment.isDetached()){
            final CountDownLatch latch = new CountDownLatch(1);

            java.lang.reflect.Method method = null;
            try {
                if(param != null) {
                    Class[] temp = new Class[param.length];
                    for(int i = 0; i < temp.length; i++){

                        if(JavaHelper.isWrapperType(param[i].getClass())){
                            temp[i] = JavaHelper.toPrimitiveType(param[i].getClass());
                        }else{
                            temp[i] = param[i].getClass();
                        }
                    }
                    method = this.getClass().getDeclaredMethod(methodName, temp);
                }
                else
                    method = this.getClass().getDeclaredMethod(methodName);

            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }

            final java.lang.reflect.Method finalMethod = method;
            final Object[] result = new Object[1];

            fragment.runOnGLThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if(finalMethod != null){
                            finalMethod.setAccessible(true);
                            result[0] = finalMethod.invoke(instance, param);
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                    latch.countDown();

                }
            });

            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return result[0];
        }

        return null;
    }

    /**
     * Map Setting start here...
     */
    public void pmpInit(){
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (fragment == null || !(fragment instanceof PMPMapFragmentDeprecated)) {
                    Log.d(TAG, "Please call pmpInit with Cocos2dxActivity reference!!!");
                    return;
                }
                Cocos2dxGLSurfaceView surfaceView = fragment.getGLSurfaceView();
                if (!surfaceView.isInLayout()) {
                    isInitFinish = true;
                    runVoidOnGlThread(new Runnable() {
                        @Override
                        public void run() {
                            init();
                        }
                    });
                    loadDefaultConfigIfNeeded();

                } else {
                    pmpInit();
                }
            }
        }, 100);

    }

    public void pmpStartLoadingScene(){
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                startLoadingScene();
            }
        });
    }

    public void pmpResetEngine(){
//        runVoidOnGlThread(new Runnable() {
//            @Override
//            public void run() {
//                resetEngine();
//            }
//        });
        resetEngine();
        isInitFinish = false;
    }

    public void pmpNativeParseJson(final String json){
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                nativeParseJson(json);
            }
        });
    }

    public void pmpChangeMapByIndex(final int index){
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                changeMapByIndex(index);
            }
        });
    }

    public void pmpSetMapControlEnabled(final boolean shouldEnable) {
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                setMapControlEnabled(shouldEnable);
            }
        });
    }

    public void pmpShowOverViewMode(final float centerX, final float centerY, final float windowWidth, final float windowHeight, final float destX, final float destY, final boolean switchToUserFloor) {
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                showOverViewMode(centerX, centerY, windowWidth, windowHeight, destX, destY, switchToUserFloor);
            }
        });
    }

    public void pmpSetDestinationThreshold(final float destinationThreshold) {
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                setDestinationThreshold(destinationThreshold);
            }
        });

        preferences.edit().putFloat(fragment.getActivity().getString(R.string.Preference_DestinationThreshold), destinationThreshold).commit();
    }

    public void pmpSetPassDestinataionThreshold(final float passDestinataionThreshold) {
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                setPassDestinataionThreshold(passDestinataionThreshold);
            }
        });

        preferences.edit().putFloat(fragment.getActivity().getString(R.string.Preference_PassDestinataionThreshold), passDestinataionThreshold).commit();
    }

    public void pmpSetRerouteThreshold(final float rerouteThreshold) {
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                setRerouteThreshold(rerouteThreshold);
            }
        });

        preferences.edit().putFloat(fragment.getActivity().getString(R.string.Preference_RerouteThreshold), rerouteThreshold).commit();
    }

    public void pmpSetEnableDebug(final boolean debug) {
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                setEnableDebug(debug);

            }
        });

        preferences.edit().putBoolean(fragment.getActivity().getString(R.string.Preference_Debug), debug).commit();
    }

    public void pmpClearSearhResult() {
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                clearSearhResult();
            }
        });
    }

    public void pmpSetMapMode(final int mapMode) {
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                setMapMode(mapMode);
            }
        });
    }

    public void     pmpOnIndoorLocationUpdate(final int beaconType, final PMPLocation loc) {
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                onIndoorLocationUpdate(beaconType, loc.getX(), loc.getY(), loc.getZ(), Integer.parseInt(loc.getName()));
            }
        });
    }

    public void pmpOnExitIndoor() {
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                onExitIndoor();
            }
        });
    }

    public void pmpSetCompassRightOffsetPx(final int px) {
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                setCompassRightOffsetPx(px);
            }
        });
    }

    public void pmpSetCompassTopOffsetPx(final int px) {
        if(isInitFinish)
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                setCompassTopOffsetPx(px);
            }
        });
    }

    public void pmpOnCompassUpdate(final float degree) {
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                onCompassUpdate(degree);
            }
        });
    }

    public void pmpSetBeacons(final List<PMPBeacon> beacons) {
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                setBeacons(beacons);
            }
        });
    }

    public void pmpSetCurrentPathResult(final int index){
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                setCurrentPathResult(index);
            }
        });
    }

    public void pmpSetLocateMeButtonContentInset(final float right, final float bottom) {
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                setLocateMeButtonContentInset(right, bottom);
            }
        });
    }

    public void pmpHideOverViewMode() {
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                hideOverViewMode();
            }
        });

    }

    public void pmpSetFloorControlContentInset(final float top, final float bottom, final float left, final float right) {
        if(isInitFinish)
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                setFloorControlContentInset(top, bottom, left, right);
            }
        });
    }

    public void pmpForceSelectPOI(final float windowCenterX, final float windowCenterY, final int poiID, final float poiX, final float poiY, final int poiMapID) {
        runVoidOnGlThread(new Runnable() {
            @Override
            public void run() {
                forceSelectPOI(windowCenterX, windowCenterY, poiID, poiX, poiY, poiMapID);
            }
        });
    }

    public int pmpGetMapMode() {
        Object result = runOnGlThread("getMapMode");
        if(result != null)
            return (int)result;
        return 0;
    }

    public float pmpGetMapScale() {
        Object result = runOnGlThread("getMapScale");
        if(result != null)
            return (float)result;
        return 0;
    }

    public ArrayList<PathResult> pmpSearchPossiblePath(final int fromID, final int toID, final boolean showPath){
        Object result = runOnGlThread("searchPossiblePath", fromID, toID, showPath);
        if(result != null)
            return (ArrayList<PathResult>)result;
        return null;
    }

    public ArrayList<PathResult> pmpSearchPossiblePathByPOI(final int fromID, final int poiID, final boolean showPath){
        Object result = runOnGlThread("searchPathByPOI", fromID, poiID, showPath);
        if(result != null)
            return (ArrayList<PathResult>)result;
        return null;
    }

    public float pmpGetLastSearchRemainDistanceInMeter() {
        Object result = runOnGlThread("getLastSearchRemainDistanceInMeter");
        if(result != null)
            return (float)result;
        return -1;
    }

    public float pmpGetLastSearchTotalDistance() {
        Object result = runOnGlThread("getLastSearchTotalDistance");
        if(result != null)
            return (float)result;
        return -1;
    }

    public float pmpGetLastSearchRemainTimeInSec() {
        Object result = runOnGlThread("getLastSearchRemainTimeInSec");
        if(result != null)
            return (float)result;
        return -1;
    }

    public boolean pmpIsEnableOverViewMode() {
        Object result = runOnGlThread("isEnableOverViewMode");
        if(result != null)
            return (boolean)result;
        return false;
    }

    public float pmpGetLastSearchTotalTimeInSec() {
        Object result = runOnGlThread("getLastSearchTotalTimeInSec");
        if(result != null)
            return (float)result;
        return -1;
    }

    public float pmpGetCurrentFloor() {
        Object result = runOnGlThread("getCurrentFloor");
        if(result != null)
            return (float)result;
        return -1;
    }

    public int pmpGetLangId() {
        Object result = runOnGlThread("getLangId");
        if(result != null)
            return (int)result;
        return -1;
    }

    public NativeMapConfig pmpGetNativeMapConfig() {
        Object result = runOnGlThread("getMapConfig");
        return (NativeMapConfig)result;
    }

    public static float getHorizontalViewAngle(){
        if (instance != null){
            if (instance.fragment instanceof PMPMapFragmentDeprecated){
                PMPMapFragmentDeprecated fgment = (PMPMapFragmentDeprecated) instance.fragment;
                return PMPCameraManager.getSharedManager(fgment.getContext()).getHorizontalViewAngle();
            }
        }

        return 0.0f;
    }

    public boolean getEnableARNavigation(Context context) {
        boolean value = true;

        value = PMPUtil.getSharedPreferences(context).getBoolean(SettingEnableARNavigation, value);

        return value;
    }

    public void setEnableARNavigation(Context context, boolean enable) {
        SharedPreferences.Editor editor = PMPUtil.getSharedPreferences(context).edit();
        editor.putBoolean(SettingEnableARNavigation, enable);
        editor.commit();
    }

    public boolean getShowARReminder(Context context) {
        boolean value = true;

        value = PMPUtil.getSharedPreferences(context).getBoolean(SettingShowARReminder, value);

        return value;
    }

    public void setShowARReminder(Context context, boolean shouldShow) {
        SharedPreferences.Editor editor = PMPUtil.getSharedPreferences(context).edit();
        editor.putBoolean(SettingShowARReminder, shouldShow);
        editor.commit();
    }

    /**
     * Native function
     */
    private native void init();
    private native void nativeParseJson(String json);
    private native void startLoadingScene();
    private native void resetEngine();
    private native ArrayList<PathResult> searchPossiblePath(int fromID, int toID, boolean showPath);
    private native ArrayList<PathResult> searchPathByPOI(int fromID, int poiID, boolean showPath);
    private native void setMapControlEnabled(boolean shouldEnable);
    private native void changeMapByIndex(int index);
    private native void clearSearhResult();
    private native void setMapMode(int mapMode);
    private native void forceSelectPOI(float windowCenterX, float windowCenterY, int poiID, float poiX, float poiY, int poiMapID);
    private native void showOverViewMode(float centerX, float centerY, float windowWidth, float windowHeight, float destX, float destY, boolean switchToUserFloor);
    private native int  getMapMode();
    private native float getMapScale();
    private native boolean isEnableOverViewMode();
    private native void onIndoorLocationUpdate(int beaconType, double x, double y, double z, int mapID);
    private native void onExitIndoor();
    private native void onCompassUpdate(float degree);
    private native int  getCurrentFloor();
    private native float getLastSearchTotalDistance();
    private native float getLastSearchRemainDistanceInMeter();
    private native float getLastSearchTotalTimeInSec();
    private native float getLastSearchRemainTimeInSec();
    private native int  getLangId();
    private native void setCompassTopOffsetPx(int px);
    private native void setCompassRightOffsetPx(int px);
    private native void setDestinationThreshold(float destinationThreshold);
    private native void setPassDestinataionThreshold(float passDestinataionThreshold);
    private native void setRerouteThreshold(float rerouteThreshold);
    private native void setEnableDebug(boolean enableDebug);
    private native NativeMapConfig getMapConfig();
    public native void setBeacons(List<PMPBeacon> beacons);
    private native void hideOverViewMode();
    private native void setCurrentPathResult(int index);
    private native void setLocateMeButtonContentInset(float right, float bottom);
    private native void setFloorControlContentInset(float top, float bottom, float left, float right);
    public native int pmpGetCurrentNavitaionStep();
    public native void setWalkingDistanceBuffer(float buffer);
    /**
     * Native Callback...
     */

    public static void onMapEngineInitialFinish() {
        Log.i(TAG, "onMapEngineInitialFinish:");
        if(instance != null && instance.mapEngineInitialFinishCallback != null){
            instance.mapEngineInitialFinishCallback.onMapEngineInitialFinish();
        }
    }

    public static void onMapModeChanged(int mode) {
        Log.i(TAG, "onMapModeChanged:" + mode);
        if(instance != null && instance.mapModeChangeCallback != null)instance.mapModeChangeCallback.onMapModeChanged(mode);
    }

    public static void onArrivalDestination() {
        Log.i(TAG, "onArrivalDestination:");
        if(instance != null && instance.arrivedDestinationCallback != null && instance != null){
            instance.fragment.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    instance.arrivedDestinationCallback.onArrivedDestination();
                }
            });
        }
    }

    public static void onByPassDestination() {
        Log.i(TAG, "onByPassDestination:");
        if (instance != null && instance.arrivedDestinationCallback != null && instance != null) {
            instance.fragment.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    instance.arrivedDestinationCallback.onBypassedDestination();
                }
            });
        }
    }

    public static void onSelectPOI(int poi_id) {
        Log.i(TAG, "onSelectPOI:" + poi_id);
        if(instance != null && instance.poiCallback != null)instance.poiCallback.onSelectPOI(poi_id);
    }

    public static void OnDiselectPOI() {
        Log.i(TAG, "OnDiselectPOI:");
        if(instance != null && instance.poiCallback != null)instance.poiCallback.onDeselectPOI();
    }

    public static void onSelectPOIPin(int poi_id) {
        Log.i(TAG, "onSelectPOIPin:");
        if(instance != null && instance.poiCallback != null)instance.poiCallback.onSelectPOIPin(poi_id);
    }


    public static void OnProximityDismiss() {
        Log.i(TAG, "OnProximityDismiss:");
    }

    public static void OnReroute() {
        Log.i(TAG, "OnReroute:");
    }

    public static void onFloorSwitch(int floorIndex) {
        Log.i(TAG, "onFloorSwitch:");
    }

    public static void onStepMessageUpdate(int mode, String message, String poiMessage, String iconImageName, float totalDuration, float totalDistance) {
        Log.i(TAG, "onStepMessageUpdate:");
        if(instance != null && instance.stepMessageUpdateCallback != null)instance.stepMessageUpdateCallback.onStepMessageUpdate(mode, message, poiMessage, iconImageName, totalDuration, totalDistance);
    }

    /**
     * Java Callback...
     */
}
