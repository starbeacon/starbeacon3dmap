package com.cherrypicks.pmpmap.core;

import com.cherrypicks.pmpmap.PMPIndoorLocationManager;
import com.pmp.mapsdk.cms.model.Areas;
import com.pmp.mapsdk.cms.model.Promotions;

/**
 * Created by andrew on 29/11/2016.
 */

public interface CoreEngineListener extends PMPIndoorLocationManager.IndoorLocationListener {
    public void onEngineInitialDone();
    public void onEntryLiftDetected(double x, double y);
    public void onEntryShuttleBusDetected(double x, double y);
    public void onArrivalDestination();
    public void onBypassDestination();
    public void onResetBypassDest();
    public void onReroute();
    public void onClearSearchResult();
    public void onStepStepMessageUpdate(int beaconType, String message, String poiMessage, String imageName, float totalDuration, float totalDistance);
    public void onMapModeChanged(int newMapMode);
    public void onAreaChanged(Areas area);
    public void onPathTypeChecked(int pathType, String message, String imageName);
    public void onShopNearByPromoMessage(int promoId,int poiId);
    public void onInZone(Promotions promotion, boolean isBackground);
    public void onCameraFacedDown();
    public void onCameraFacedFront();
}
