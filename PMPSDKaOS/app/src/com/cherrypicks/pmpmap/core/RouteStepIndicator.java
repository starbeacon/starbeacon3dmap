package com.cherrypicks.pmpmap.core;

/**
 * Created by andrew on 5/12/2016.
 */

public class RouteStepIndicator {
    private String floorName;
    private int pathType;
    private float x;
    private float y;
    private float z;
    private int mapId;
    private int nextMapId;
    private int pathId;

    public RouteStepIndicator(String floorName, int pathType, float x, float y, float z, int mapId, int nextMapId, int pathId) {
        this.floorName = floorName;
        this.pathType = pathType;
        this.x = x;
        this.y = y;
        this.z = z;
        this.mapId = mapId;
        this.nextMapId = nextMapId;
        this.pathId = pathId;
    }

    public static RouteStepIndicator create(String floorName, int pathType, float x, float y, float z, int mapId, int nextMapId, int pathId) {
        return new RouteStepIndicator(floorName, pathType, x, y, z, mapId, nextMapId, pathId);
    }

    public String getFloorName() {
        return floorName;
    }

    public int getPathType() {
        return pathType;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    public int getMapId() {
        return mapId;
    }

    public int getNextMapId() {
        return nextMapId;
    }
}
