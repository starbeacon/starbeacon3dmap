package com.cherrypicks.pmpmap.core;

import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.cms.model.Nodes;
import com.pmp.mapsdk.cms.model.Pois;

import java.util.Map;

/**
 * Created by andrew on 6/12/2016.
 */

public class VertexByDuration {
    private int vertexId;
    private float duration;

    public VertexByDuration(int vertexId, float duration) {
        this.vertexId = vertexId;
        this.duration = duration;
    }

    public static VertexByDuration create(int vertexId, float duration) {
        return new VertexByDuration(vertexId, duration);
    }

    public int getVertexId() {
        return vertexId;
    }

    public float getDuration() {
        return duration;
    }

    public Nodes getNode() {
        Nodes node = null;
        if (PMPServerManager.getShared(null).getServerResponse() != null) {
            Map<Integer, Nodes> nodesMap = PMPServerManager.getShared().getServerResponse().getNodesMap();
            if (nodesMap != null && nodesMap.containsKey(vertexId)) {
                node = nodesMap.get(vertexId);
            }
        }
        return node;
    }

}
