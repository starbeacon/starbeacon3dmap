package com.cherrypicks.pmpmap.core;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.cherrypicks.pmpmap.CocosFragment;
import com.cherrypicks.pmpmap.GPSLocationManager;
import com.cherrypicks.pmpmap.PMPDataManager;
import com.cherrypicks.pmpmap.PMPIndoorLocationManager;
import com.cherrypicks.pmpmap.datamodel.PathResult;
import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.cms.model.Areas;
import com.pmp.mapsdk.cms.model.Maps;
import com.pmp.mapsdk.cms.model.Promotions;
import com.pmp.mapsdk.cms.model.ResponseData;
import com.pmp.mapsdk.external.PMPMapSDK;
import com.pmp.mapsdk.location.PMPApplication;
import com.pmp.mapsdk.location.PMPBeacon;
import com.pmp.mapsdk.location.PMPBeaconType;
import com.pmp.mapsdk.location.PMPLocation;
import com.pmp.mapsdk.location.PMPLocationManagerError;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by andrew on 29/11/2016.
 */

public class CoreEngine implements CoreEngineListener, PMPIndoorLocationManager.IndoorLocationListener {

    private final static String TAG = "CoreEngine";
    public final static int DeviceFacingUnknown = 0;
    public final static int DeviceFacingFront = 1;
    public final static int DeviceFacingDown = 2;


    public final static int MapModeBrowsing = 0;
    public final static int MapModeLocateMe = 2;
    public final static int MapModeNavigating = 1;
    public final static int MapModeUnknown = 3;

    public final static int NavigationStepNotNavigating = 0;
    public final static int NavigationStepOnTheWay = 1;
    public final static int NavigationStepArrived = 2;
    public final static int NavigationStepBypassed = 3;

    public final static int PathTypeNormal = 0;
    public final static int PathTypeEscalatorUp = 2;
    public final static int PathTypeEscalatorDown = 3;
    public final static int PathTypeStaircaseUp = 4;
    public final static int PathTypeStaircaseDown = 5;
    public final static int PathTypeLiftUp = 6;
    public final static int PathTypeLiftDown = 7;
    public final static int PathTypePathEntry = 8;
    public final static int PathTypeImmigrationCheck = 9;
    public final static int PathTypeAPM = 10;
    public final static int PathTypeShuttleBus = 11;
    public final static int PathTypeMovingWalkway = 12;
    public final static int PathTypeSecurityCheck = 13;
    public final static int PathTypeCustomCheck = 14;
    public final static int PathTypeArrivalImmigrationCheck = 15;


    public final static int CurrentLocationVertexID = -999;

    //analytics
    public String majorOfBeaconsForTrilateration;
//    public String majorOfNeighbourBeacons;

    private List<CoreEngineListener> listeners = new ArrayList<>();
    private static CoreEngine instance = null;
    private Handler handler;
    private long nativeListener;
    private PMPLocation indoorLocation;
    private Areas area;

    //compass
    private float[] mGData = new float[3];
    private float[] mMData = new float[3];
    private float[] mR = new float[16];
    private float[] mI = new float[16];
    private float[] mOrientation = new float[3];
    private int mCount;

    //GPS positioning...
    private boolean forceToUseGpsSignal = false;
    private boolean shouldUseGpsSignalAsPositioning = true;
    private Timer gpsSignalFlagTimer = new Timer();
    private ResetGpsFlagTask gpsSignalFlagTask;
    private ResetForceGpsFlagTask forceGpsFlagTask;

    private float bearing;
    private SensorEventListener sensorEventListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
/*
            bearing = -event.values[0];
            Log.d(TAG, "bearing:" + bearing);

            for (CoreEngineListener l :
                    listeners) {
                l.didCompassUpdated(bearing);
            }
            nativeOnCompassUpdate(bearing);
*/
/*
            int type = event.sensor.getType();
            float[] data;
            if (type == Sensor.TYPE_ACCELEROMETER) {
                data = mGData;
            } else if (type == Sensor.TYPE_MAGNETIC_FIELD) {
                data = mMData;
            } else {
                // we should not be here.
                return;
            }
            for (int i=0 ; i<3 ; i++)
                data[i] = event.values[i];
            SensorManager.getRotationMatrix(mR, mI, mGData, mMData);
// some test code which will be used/cleaned up before we ship this.
//        SensorManager.remapCoordinateSystem(mR,
//                SensorManager.AXIS_X, SensorManager.AXIS_Z, mR);
//        SensorManager.remapCoordinateSystem(mR,
//                SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, mR);
            SensorManager.getOrientation(mR, mOrientation);
            float incl = SensorManager.getInclination(mI);
            if (mCount++ > 50) {
                final float rad2deg = (float)(180.0f/Math.PI);
                mCount = 0;
                Log.d("Compass", "yaw: " + (int)(mOrientation[0]*rad2deg) +
                        "  pitch: " + (int)(mOrientation[1]*rad2deg) +
                        "  roll: " + (int)(mOrientation[2]*rad2deg) +
                        "  incl: " + (int)(incl*rad2deg)
                );
            }
            final float alpha = 0.97f;
            final float rad2deg = (float)(180.0f/Math.PI);
            float  newBearing = (mOrientation[0]*rad2deg);

            bearing = alpha * bearing + (1 - alpha) * newBearing;

            for (CoreEngineListener l :
                    listeners) {
                l.didCompassUpdated(bearing);
            }
            nativeOnCompassUpdate(bearing);
*/
//            final float alpha = 0.97f;
//            synchronized (this) {
//                if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
//                    mGData[0] = alpha * mGData[0] + (1 - alpha)
//                            * event.values[0];
//                    mGData[1] = alpha * mGData[1] + (1 - alpha)
//                            * event.values[1];
//                    mGData[2] = alpha * mGData[2] + (1 - alpha)
//                            * event.values[2];
//
//                }
//                if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
//                    mMData[0] = alpha * mMData[0] + (1 - alpha)
//                            * event.values[0];
//                    mMData[1] = alpha * mMData[1] + (1 - alpha)
//                            * event.values[1];
//                    mMData[2] = alpha * mMData[2] + (1 - alpha)
//                            * event.values[2];
//                }
//
//                float R[] = new float[9];
//                float I[] = new float[9];
//                boolean success = SensorManager.getRotationMatrix(R, I, mGData,
//                        mMData);
//                if (success) {
//                    float orientation[] = new float[3];
//                    SensorManager.getOrientation(R, orientation);
//                    bearing = (float) Math.toDegrees(orientation[0]); // orientation
//                    bearing = (bearing + 360) % 360;
//                    Log.d(TAG, "bearing:" + bearing);
//                    for (CoreEngineListener l :
//                            listeners) {
//                        l.didCompassUpdated(bearing);
//                        nativeOnCompassUpdate(bearing);
//                    }
//                }
//            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };
    private SensorManager sensorManager;// = (SensorManager)getActivity().getSystemService(Activity.SENSOR_SERVICE);
    private Sensor accelerometer;
    private Sensor magnetometer;

    //DEBUG
//    private PMPLocation[] fakeLocations = {
//            new PMPLocation(5512, 11256, 0, "29"),
//            /*
//            new PMPLocation(5516, 10800, 0, "29"),
//            new PMPLocation(5516, 10380, 0, "29"),
//            new PMPLocation(5516, 10380, 0, "29"),
//            new PMPLocation(5516, 10380, 0, "29"),
//            new PMPLocation(5516, 10380, 0, "29"),
//
//            new PMPLocation(5516, 10300, 0, "29"),
//            new PMPLocation(5516, 10300, 0, "29"),
//            new PMPLocation(5516, 10300, 0, "29"),
//
//            new PMPLocation(5516, 10260, 0, "29"),
//            new PMPLocation(5516, 10260, 0, "29"),
//
//            new PMPLocation(5514, 10284, 0, "29"),
//
//            new PMPLocation(5397, 10252, 0, "29"),
//            new PMPLocation(5451, 10226, 0, "29"),
//            new PMPLocation(5404, 10211, 0, "29"),
//            */
//    };
    private PMPLocation[] fakeLocations = {
            new PMPLocation(5074, 15579, 0, "30"),
            new PMPLocation(5074, 15579, 0, "30"),
            new PMPLocation(5074, 15579, 0, "30"),
            new PMPLocation(5074, 15579, 0, "30"),
            new PMPLocation(5178, 15588, 0, "30"),
            new PMPLocation(5178, 15588, 0, "30"),
            new PMPLocation(5178, 15588, 0, "30"),
            new PMPLocation(5329, 15596, 0, "30"),
            new PMPLocation(5329, 15596, 0, "30"),
            new PMPLocation(5329, 15596, 0, "30"),
            new PMPLocation(5329, 15596, 0, "30"),

    };
    private int fakeLocationIndex = 0;
    private android.os.Handler debugHandler = new android.os.Handler();
    private Runnable debugLocationRunnable = new Runnable() {
        @Override
        public void run() {
            setInsideRegion(true);
            fakeLocationIndex++;
            if (fakeLocationIndex >= fakeLocations.length) {
                fakeLocationIndex = 0;
            }
            didIndoorLocationUpdated(PMPBeaconType.Positioning, fakeLocations[fakeLocationIndex]);
            debugHandler.postDelayed(this, 3000);
        }
    };
    private boolean fakeLocationEnable = false;

    private Runnable signalTimeoutRunnable = new Runnable() {
        @Override
        public void run() {
            setInsideRegion(false);
            didIndoorExitRegion();
        }
    };


    public static CoreEngine getInstance() {
        if (instance == null) {
            instance = new CoreEngine();
        }
        return instance;
    }

    private CoreEngine() {
        handler = new Handler(Looper.getMainLooper());
        PMPIndoorLocationManager.getSharedPMPManager(null).setLocationListener(this);
        sensorManager = (SensorManager) PMPApplication.getApplication().getSystemService(Activity.SENSOR_SERVICE);
        setupSensorManager(sensorManager);
        nativeInitial();
    }

    private void setupSensorManager(SensorManager sensorManager) {
        this.sensorManager = sensorManager;

//        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
//        magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
//        sensorManager.registerListener(sensorEventListener, accelerometer, SensorManager.SENSOR_DELAY_GAME);
//        sensorManager.registerListener(sensorEventListener, magnetometer, SensorManager.SENSOR_DELAY_GAME);
//
//        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
//        sensorManager.registerListener(sensorEventListener,
//                sensor,
//                SensorManager.SENSOR_DELAY_GAME);
    }

    public void addListener(final CoreEngineListener listener) {
        if (listener != null) {
            CoreEngineListener listenerFound = CollectionUtils.find(listeners, new Predicate<CoreEngineListener>() {
                @Override
                public boolean evaluate(CoreEngineListener object) {
                    return listener.equals(object);
                }
            });
            if (listenerFound == null) {
                listeners.add(listener);
            }
        }
    }

    public void removeListener(final CoreEngineListener listener) {
        if (listener != null) {
            CoreEngineListener listenerFound = CollectionUtils.find(listeners, new Predicate<CoreEngineListener>() {
                @Override
                public boolean evaluate(CoreEngineListener object) {
                    return listener.equals(object);
                }
            });
            if (listenerFound != null) {
                listeners.remove(listenerFound);
            }
        }
    }

    @Override
    public void onEngineInitialDone() {
        Log.d(TAG, "onEngineInitialDone");
        handler.post(new Runnable() {
            @Override
            public void run() {
                PMPIndoorLocationManager.getSharedPMPManager(null).setLocationListener(CoreEngine.this);
                synchronized (CoreEngine.this) {
                    for (CoreEngineListener l :
                            listeners) {
                        l.onEngineInitialDone();
                    }
                    if (fakeLocationEnable) {
                        debugHandler.removeCallbacks(debugLocationRunnable);
                        debugLocationRunnable.run();
                    }
                }

            }
        });

    }

    @Override
    public void onEntryLiftDetected(final double x, final double y) {

        handler.post(new Runnable() {
            @Override
            public void run() {
                for (CoreEngineListener l : listeners) {
                    l.onEntryLiftDetected(x, y);
                }
            }
        });
    }

    @Override
    public void onEntryShuttleBusDetected(final double x, final double y) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (CoreEngineListener l : listeners) {
                    l.onEntryShuttleBusDetected(x, y);
                }
            }
        });
    }

    @Override
    public void onArrivalDestination() {

        handler.post(new Runnable() {
            @Override
            public void run() {
                for (CoreEngineListener l :
                        listeners) {
                    l.onArrivalDestination();
                }
            }
        });
    }

    @Override
    public void onBypassDestination() {

        handler.post(new Runnable() {
            @Override
            public void run() {
                for (CoreEngineListener l :
                        listeners) {
                    l.onBypassDestination();
                }
            }
        });
    }

    @Override
    public void onResetBypassDest() {

        handler.post(new Runnable() {
            @Override
            public void run() {
                for (CoreEngineListener l :
                        listeners) {
                    l.onResetBypassDest();
                }
            }
        });
    }

    @Override
    public void onReroute() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (CoreEngineListener l :
                        listeners) {
                    l.onReroute();
                }
            }
        });

    }

    @Override
    public void onClearSearchResult() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (CoreEngineListener l :
                        listeners) {
                    l.onClearSearchResult();
                }
            }
        });
    }

    @Override
    public void onStepStepMessageUpdate(final int type, final String message, final String poiMessage, final String imageName, final float totalDuration, final float totalDistance) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (CoreEngineListener l :
                        listeners) {
                    l.onStepStepMessageUpdate(type, message, poiMessage, imageName, totalDuration, totalDistance);
                }
            }
        });
    }

    @Override
    public void onMapModeChanged(final int newMapMode) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (CoreEngineListener l :
                        listeners) {
                    l.onMapModeChanged(newMapMode);
                }
            }
        });
    }

    @Override
    public void onAreaChanged(Areas area) {

    }

    @Override
    public void onPathTypeChecked(final int pathType, final String message, final String imageName) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (CoreEngineListener l :
                        listeners) {
                    l.onPathTypeChecked(pathType, message, imageName);
                }

            }
        });
    }

    @Override
    public void onShopNearByPromoMessage(final int promoId, final int poiId) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (CoreEngineListener l :
                        listeners) {
                    l.onShopNearByPromoMessage(promoId,poiId);
                }

            }
        });
    }

    @Override
    public void onInZone(final Promotions promotion, final boolean isBackground) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (CoreEngineListener l :
                        listeners) {
                    l.onInZone(promotion, isBackground);
                }

            }
        });
    }

    @Override
    public void onCameraFacedDown() {
        Log.d(TAG, "onCameraFacedDown");
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (CoreEngineListener l :
                        listeners) {
                    l.onCameraFacedDown();
                }

            }
        });
    }

    @Override
    public void onCameraFacedFront() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (CoreEngineListener l :
                        listeners) {
                    l.onCameraFacedFront();
                }

            }
        });
    }

    public int getMapMode() {
        return nativeGetMapMode();
    }

    public void setMapMode(final int mode) {
        nativeSetMapMode(mode);
    }

    public boolean searchPathFromPOIToPOI(final int fromPoiId, final int toPoiId) {
        return nativeSearchPathFromPOIToPOI(fromPoiId, toPoiId);
    }
    public boolean searchPathByPOI(final int poiId) {
        return searchPathByPOI(poiId, true);
    }
    public boolean searchPathByPOI(final int poiId, boolean updateRouteStepIndicator) {
        return nativeSearchPathByPOI(poiId,updateRouteStepIndicator);
    }

    public PMPMapSDK.PathCalculationResult calculateDistanceFromNodeToNode(int fromNodeId, int toNodeId) {
        return nativeCalculateDistanceFromNodeToNode(fromNodeId, toNodeId);
    }

    public void clearSearchResult() {
        nativeClearSearchResult();
    }

    public void setCompassEnable(boolean enable) {
        nativeSetCompassEnable(enable);
    }

    public boolean isCompassEnable() {
        return nativeIsCompassEnable();
    }

    public void setRecenterModeEnable(boolean enable) {
        nativeSetRecenterModeEnable(enable);
    }

    public boolean isRecenterModeEnable() {
        return nativeIsRecenterModeEnable();
    }

    public float getNavigationTotalTime() {
        return nativeGetNavigationTotalTime();
    }

    public float getNavigationTotalDistance() {
        return nativeGetNavigationTotalDistance();
    }

    public int getRouteStepIndictorsSize() {
        return nativeGetRouteStepIndictorsSize();
    }

    public float getNavigationTimeRemaining() {
        return nativeGetNavigationTimeRemaining();
    }

    public float getNavigationDistanceRemaining() {
        return nativeGetNavigationDistanceRemaining();
    }

    public void onIndoorLocationUpdate(final int type, final PMPLocation loc) {
        //Dirty fix!!! need to review when free
        if(CocosFragment.getInstance() != null){
            CocosFragment.getInstance().runOnGLThread(new Runnable() {
                @Override
                public void run() {
                    nativeOnIndoorLocationUpdate(type, loc.getX(), loc.getY(), loc.getZ(), Integer.parseInt(loc.getName()));
                }
            });
        }else{
            nativeOnIndoorLocationUpdate(type, loc.getX(), loc.getY(), loc.getZ(), Integer.parseInt(loc.getName()));
        }
    }

    public PMPLocation getIndoorLocation() {
        return indoorLocation;
    }

    public VertexByDuration[] filterVertexByDuration(int vertexId, float duration) {
        return nativeFilterVertexByDuration(vertexId, duration);
    }

    public void setDebug(boolean enable) {
        nativeSetDebug(enable);
    }

    public boolean isDebug() {
        return nativeIsDebug();
    }

    public void setDestinationThreshold(float threshold) {
        nativeSetDestinationThreshold(threshold);
    }

    public float getDestinationThreshold() {
        return nativeGetDestinationThreshold();
    }

    public void setBypassDestinationThreshold(float threshold) {
        nativeSetBypassDestinationThreshold(threshold);
    }

    public float getBypassDestinationThreshold() {
        return nativeGetBypassDestinationThreshold();
    }

    public void setRerouteThreshold(float threshold) {
        nativeSetRerouteThreshold(threshold);
    }

    public float getRerouteThreshold() {
        return nativeGetRerouteThreshold();
    }

    public PathResult getPathResult() {
        return nativeGetPathResult();
    }

    public Areas getArea() {
        return area;
    }

    public RouteStepIndicator[] getRouteStepIndicators() {
        return nativeGetRouteStepIndicators();
    }

    public boolean isDisability() {
        return nativeIsDisability();
    }

    public void setIsDisability(boolean val) {
        nativeSetIsDisability(val);
    }

    public String parseScheduleByPOIID(int poiID, int[] isOpening) {
        return nativeParseScheduleByPOIID(poiID, isOpening);
    }

    public String parseScheduleByPromotionID(int promotionID, int[] isOpening) {
        return nativeParseScheduleByPromotionID(promotionID, isOpening);
    }

    public boolean checkResultPathType(int pathTypeID) {
        return nativeCheckResultPathType(pathTypeID);
    }

    public void setInsideRegion(boolean b) {
        nativeSetInsideRegion(b);
    }

    public boolean isInsideRegion() {
        return nativeIsInsideRegion();
    }

    public int getAreaIDByCoordinate(int x, int y) {
        return nativeGetAreaIDByCoordinate(x, y);
    }

    public void setPathForeSeeingDistance(int value){
        nativeSetPathForeSeeingDistance(value);
    }

    public int getCurrentNavigationStep() {
        return nativeGetCurrentNavigationStep();
    }

    public void setDirectionPredictionMode(int mode) {
        nativeSetDirectionPredictionMode(mode);
    }

    public boolean isInitialized() {
        return nativeIsInitialized();
    }
    public String getCurrentAreaName() {
        return nativeGetCurrentAreaName();
    }

    public String getCurrentMapName() {
        return nativeGetCurrentMapName();
    }

    public void startCalibration() {
        nativeStartCalibration();
    }

    public float getBearing() {
        return bearing;
    }

    public int getDeviceFacing() {
        return nativeGetDeviceFacing();
    }

    public void setBlueDotMode(int mode){
        nativeSetBlueDotMode(mode);
    }

    private native void nativeInitial();

    private native int nativeGetMapMode();

    private native void nativeSetMapMode(int mode);

    private native boolean nativeSearchPathFromPOIToPOI(int fromPoiId, int toPoiId);

    private native boolean nativeSearchPathByPOI(int poiId, boolean updateRouteStepIndicator);

    private native PMPMapSDK.PathCalculationResult nativeCalculateDistanceFromNodeToNode(int fromNodeId, int toNodeId);

    private native void nativeClearSearchResult();

    private native void nativeSetCompassEnable(boolean enable);

    private native boolean nativeIsCompassEnable();

    private native boolean nativeIsRecenterModeEnable();

    private native boolean nativeSetRecenterModeEnable(boolean enable);

    private native float nativeGetNavigationTotalTime();

    private native float nativeGetNavigationTotalDistance();

    private native int nativeGetRouteStepIndictorsSize();

    private native float nativeGetNavigationTimeRemaining();

    private native float nativeGetNavigationDistanceRemaining();

    private native void nativeOnIndoorLocationUpdate(int type, double x, double y, double z, int mapId);

    private native void nativeOnCompassUpdate(float compass);

    private native VertexByDuration[] nativeFilterVertexByDuration(int vertexId, float duration);

    private native void nativeSetDebug(boolean enable);

    private native boolean nativeIsDebug();

    private native void nativeSetDestinationThreshold(float threshold);

    private native float nativeGetDestinationThreshold();

    private native void nativeSetBypassDestinationThreshold(float threshold);

    private native float nativeGetBypassDestinationThreshold();

    private native void nativeSetRerouteThreshold(float threshold);

    private native float nativeGetRerouteThreshold();

    private native PathResult nativeGetPathResult();

    private native RouteStepIndicator[] nativeGetRouteStepIndicators();

    private native boolean nativeIsDisability();

    private native void nativeSetIsDisability(boolean val);

    private native String nativeParseScheduleByPOIID(int poiID, int[] isOpening);
    private native String nativeParseScheduleByPromotionID(int promotionID, int[] isOpening);

    private native boolean nativeCheckResultPathType(int pathTypeID);

    private native void nativeSetInsideRegion(boolean insideRegion);
    private native boolean nativeIsInsideRegion();

    private native int nativeGetAreaIDByCoordinate(int x, int y);
    private native void nativeSetPathForeSeeingDistance(float value);

    private native int nativeGetCurrentNavigationStep();
    private native void nativeSetDirectionPredictionMode(int mode);

    private native boolean nativeIsInitialized();
    private native String nativeGetCurrentAreaName();
    private native String nativeGetCurrentMapName();

    private native void nativeStartCalibration();
    private native int nativeGetDeviceFacing();

    private native void nativeSetBlueDotMode(int mode);

    @Override
    public void didIndoorLocationUpdated(final PMPBeaconType beaconType, final PMPLocation location) {
        if(!forceToUseGpsSignal){
            //gps position related
            shouldUseGpsSignalAsPositioning = false;
            cancelGpsSignalFlagTimerTask();
            gpsSignalFlagTimer.schedule(gpsSignalFlagTask = new ResetGpsFlagTask(), PMPMapSDK.getConfiguration().getBleToGpsTimeoutInSec() * 1000);

            didLocationUpdated(beaconType, location);
        }
    }


    public void onGPSIndoorLocationUpdate(double x, double y, float accuracy){
        if(accuracy < PMPMapSDK.getConfiguration().getGpsAccuracyThreshold()){
            forceToUseGpsSignal = true;
            cancelForceGpsSignalFlagTimerTask();
            gpsSignalFlagTimer.schedule(forceGpsFlagTask = new ResetForceGpsFlagTask(), 5000);
        }

        if(forceToUseGpsSignal || shouldUseGpsSignalAsPositioning){
            ArrayList<Maps> map = PMPServerManager.getShared().getServerResponse().getMaps();
            if(map != null && map.size() > 0){
                setInsideRegion(true);
                PMPLocation location = new PMPLocation(x, y, 0, (indoorLocation != null)? indoorLocation.getName() : ((int)map.get(0).getId()) + "");
                didLocationUpdated(PMPBeaconType.Positioning, location);
            }
        }
    }

    private void didLocationUpdated(final PMPBeaconType beaconType, final PMPLocation location){
        CoreEngine.getInstance().onIndoorLocationUpdate(beaconType.getValue(), location);
        this.indoorLocation = location;
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (CoreEngineListener l :
                        listeners) {
                    l.didIndoorLocationUpdated(beaconType, location);
                }
            }
        });

        int areaId = getAreaIDByCoordinate((int)location.getX(), (int)location.getY());
        ResponseData response = PMPDataManager.getSharedPMPManager(null).getResponseData();

        if(response != null && response.getAreas() != null && (area == null || areaId != (int)area.getId())){
            for(Areas tempArea : response.getAreas()){
                if(tempArea.getId() == areaId){
                    area = tempArea;
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            for (CoreEngineListener l :
                                    listeners) {
                                l.onAreaChanged(area);
                            }
                        }
                    });
                }
            }
        }

        if(PMPMapSDK.getIndoorLocationUpdateCallback() != null){
            PMPMapSDK.getIndoorLocationUpdateCallback().onIndoorLocationUpdate((int)location.getX(), (int)location.getY(), Integer.parseInt(location.getName()), (int)getNavigationTimeRemaining(), (int)getNavigationDistanceRemaining());
        }
    }

    private void cancelGpsSignalFlagTimerTask(){
        if(gpsSignalFlagTask != null){
            gpsSignalFlagTask.cancel();
            gpsSignalFlagTask = null;
        }
    }

    private void cancelForceGpsSignalFlagTimerTask(){
        if(forceGpsFlagTask != null){
            forceGpsFlagTask.cancel();
            forceGpsFlagTask = null;
        }
    }

    public class ResetGpsFlagTask extends TimerTask
    {
        public void run()
        {
            shouldUseGpsSignalAsPositioning = true;
        }
    }

    public class ResetForceGpsFlagTask extends TimerTask
    {
        public void run()
        {
            forceToUseGpsSignal = false;
        }
    }

    @Override
    public void didIndoorTransmissionsUpdated(final List<PMPBeacon> transmissions) {
        Log.d(TAG, "didIndoorTransmissionsUpdated:" + transmissions.size());
        boolean locationServiceOn = GPSLocationManager.checkIfLocationPermissionAllowed(PMPApplication.getApplication());

        if (locationServiceOn) {
            if (transmissions.size() == 0) {
                CoreEngine.getInstance().setInsideRegion(false);
            }else {
                CoreEngine.getInstance().setInsideRegion(true);
            }
        }else {
            CoreEngine.getInstance().setInsideRegion(false);
        }

//        Map<Areas, Integer> areaCount = new HashMap<Areas, Integer>();
//        int sumOfArea = 0;
//        for (PMPBeacon beacon :
//                transmissions) {
////            if (!response.getDeviceMapByMajor().containsKey(beacon.getMajor())) {
////                continue;
////            }
//            Devices device = response.getDeviceMapByMajor().get(beacon.getMajorIntValue());
////            if (!response.getDeviceGroupsMap().containsKey(device.getDeviceGroupId())) {
////                continue;
////            }
//            DeviceGroups deviceGroup = response.getDeviceGroupsMap().get((int) device.getDeviceGroupId());
//            Areas area = response.getAreasMap().get(deviceGroup.getAreaId());
//            if (area != null) {
//                int count = 1;
//                if (areaCount.containsKey(area)) {
//                    count = areaCount.get(area) + 1;
//                }
//                areaCount.put(area, count);
//                sumOfArea++;
//            }
//        }
//        for (Map.Entry<Areas, Integer> entry :
//                areaCount.entrySet()) {
//            int count = entry.getValue();
//            if (count >= sumOfArea * 0.8f) {
//                final Areas newArea = entry.getKey();
//                if (area == null || area.getId() != newArea.getId()) {
//                    area = newArea;
//                    handler.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            for (CoreEngineListener l :
//                                    listeners) {
//                                l.onAreaChanged(newArea);
//                            }
//                        }
//                    });
//                    break;
//                }
//            }
//        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (CoreEngineListener l :
                        listeners) {
                    l.didIndoorTransmissionsUpdated(transmissions);
                }
            }
        });
        handler.removeCallbacks(signalTimeoutRunnable);
        handler.postDelayed(signalTimeoutRunnable, 30 * 1000);

        String tmp = "";
//        majorOfNeighbourBeacons = "";
//        int neigbhourBeaconsCount = 0;
        for(PMPBeacon b : transmissions){
            if(b.getUsingForTrilateration()) {
                if(tmp.length() > 0){
                    tmp += ",";
                }
                tmp += b.getMajorIntValue();
            }
//            else if(neigbhourBeaconsCount < 20){
//                neigbhourBeaconsCount++;
//                if(majorOfNeighbourBeacons.length() > 0){
//                    majorOfNeighbourBeacons += ",";
//                }
//                majorOfNeighbourBeacons += b.getMajorIntValue();
//            }
        }
        majorOfBeaconsForTrilateration = tmp;

    }

    @Override
    public void didIndoorExitRegion() {
//        boolean locationServiceOn = GPSLocationManager.checkIfLocationPermissionAllowed(PMPApplication.getApplication());
//        if (!locationServiceOn || !CoreEngine.getInstance().isInsideRegion()) {
        if(!PMPMapSDK.getConfiguration().isShouldEnableGPSPositioning()) {
            this.indoorLocation = null;
            PMPLocation location = new PMPLocation(0,0,0, "0");
            CoreEngine.getInstance().onIndoorLocationUpdate(PMPBeaconType.Positioning.getValue(), location);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    for (CoreEngineListener l :
                            listeners) {
                        l.didIndoorExitRegion();
                    }
                }
            });
        }
//        }
    }

    @Override
    public void didOutdoorLocationsUpdated() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (CoreEngineListener l :
                        listeners) {
                    l.didOutdoorLocationsUpdated();
                }
            }
        });
    }

    @Override
    public void didCompassUpdated(final double direction) {
        nativeOnCompassUpdate((float) direction);
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (CoreEngineListener l :
                        listeners) {
                    l.didCompassUpdated(direction);
                }
            }
        });
    }

    @Override
    public void didError(final PMPLocationManagerError error) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (CoreEngineListener l :
                        listeners) {
                    l.didError(error);
                }
            }
        });
    }

    public void setBearing(float bearing) {
        this.bearing = bearing;
//        Log.d(TAG, "bearing:" + bearing);

        for (CoreEngineListener l :
                listeners) {
            l.didCompassUpdated(bearing);
        }
        nativeOnCompassUpdate(bearing);
    }
}
