package com.cherrypicks.pmpmap.analytics;

import android.util.Log;

import com.amazonaws.mobileconnectors.pinpoint.PinpointManager;
import com.amazonaws.mobileconnectors.pinpoint.analytics.AnalyticsClient;
import com.amazonaws.mobileconnectors.pinpoint.analytics.AnalyticsEvent;
import com.cherrypicks.pmpmap.PMPMapController;
import com.cherrypicks.pmpmap.analytics.aws.AWSMobileClient;
import com.cherrypicks.pmpmap.core.CoreEngine;
import com.cherrypicks.pmpmap.datamodel.MapState;
import com.cherrypicks.pmpmapsdk.BuildConfig;
import com.pmp.mapsdk.cms.PMPProximityServerManager;
import com.pmp.mapsdk.external.PMPMapSDK;
import com.pmp.mapsdk.location.PMPApplication;
import com.pmp.mapsdk.location.PMPLocation;
import com.pmp.mapsdk.utils.PMPUtil;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by andrewman on 7/26/16.
 */
public class AnalyticsLogger {
    private final static String TAG = "AnalyticsLogger";

    private static AnalyticsLogger instance = new AnalyticsLogger();
    private AnalyticsEventCallback eventCallback;
    private MapState mapState = new MapState();
    private PinpointManager pinpointManager;
    public static AnalyticsLogger getInstance() {
        return instance;
    }

    public void logEvent(String eventName) {
        logEvent(eventName, null);
    }
    public void logEvent(String eventName, Map<String, Object> param) {
        nativeUpdateMapState();

        HashMap<String, Object> finalParam = null;
        if (param != null) {
            finalParam = new HashMap<>(param);
        }else {
            finalParam = new HashMap<>();
        }

        addCommonParams(finalParam);

        awsLogEvent(eventName, finalParam);
    }

    public void logBackgroundEvent(String eventName, Map<String, Object> param) {
        nativeUpdateMapState();

        HashMap<String, Object> finalParam = null;
        if (param != null) {
            finalParam = new HashMap<>(param);
        }else {
            finalParam = new HashMap<>();
        }

        String deviceID = PMPUtil.getSharedPreferences(PMPApplication.getApplication()).getString("MtelDeviceId", null);
        if(deviceID != null){
            finalParam.put("mtelDeviceId", deviceID);
        }

        awsLogEvent(eventName, finalParam);
    }

    private void awsLogEvent(String eventName, Map<String, Object> finalParam) {
        Log.d(TAG, "logEvent:" + eventName + " param:" + finalParam);

        if (eventCallback != null) {
            eventCallback.onReceivedEvent(eventName, finalParam);
        }
        if(AWSMobileClient.defaultMobileClient() != null && AWSMobileClient.defaultMobileClient().getPinpointManager() != null && AWSMobileClient.defaultMobileClient().getPinpointManager().getAnalyticsClient() != null){

            AWSMobileClient.defaultMobileClient().getPinpointManager().getSessionClient().startSession();
            AnalyticsClient analyticsClient = AWSMobileClient.defaultMobileClient().getPinpointManager().getAnalyticsClient();

            AnalyticsEvent event = analyticsClient.createEvent(eventName);
            Iterator<Map.Entry<String, Object>> it = finalParam.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
//            System.out.println(pair.getKey() + " = " + pair.getValue());
                event.addAttribute((String)pair.getKey(), ""+ pair.getValue());
                it.remove(); // avoids a ConcurrentModificationException
            }
            analyticsClient.recordEvent(event);
            AWSMobileClient.defaultMobileClient().getPinpointManager().getSessionClient().stopSession();
            analyticsClient.submitEvents();

        }
    }

    private void addCommonParams(HashMap<String, Object> finalParam){
        PMPLocation location = CoreEngine.getInstance().getIndoorLocation();
        if (location != null) {
            finalParam.put("floorId", ""+ location.getName());
            finalParam.put("x", ""+(int)location.getX());
            finalParam.put("y", ""+(int)location.getY());
        }

        finalParam.put("client_timestamp", "" + System.currentTimeMillis());
        finalParam.put("major_id", ""+PMPApplication.getPmpApplication().getCurrentProximityID());
        finalParam.put("minor_id", ""+ PMPApplication.getPmpApplication().getCurrentProximityMinor());
        finalParam.put("map_center_x", ""+ PMPMapController.getInstance().getScreenCenterX());
        finalParam.put("map_center_y", ""+ PMPMapController.getInstance().getScreenCenterY());
        finalParam.put("language", ""+ PMPMapSDK.getLangID());
        finalParam.put("sdk_version", BuildConfig.VERSION_NAME);
        String pushToken = PMPProximityServerManager.getShared().getPushToken();
        if(pushToken != null){
            finalParam.put("pushToken", pushToken);
        }

        String deviceID = PMPUtil.getSharedPreferences(PMPApplication.getApplication()).getString("MtelDeviceId", null);
        if(deviceID != null){
            finalParam.put("mtelDeviceId", deviceID);
        }

    }

//    public void logBackgroundEvent(String eventName, Map<String, Object> param) {
//        String host = "https://mobileanalytics.us-east-1.amazonaws.com";
//        String path = "/2014-06-05/events";
//        String timestamp = getISO8601DateTime();
//
//        String appId = getPinpointManager().getPinpointContext().getTargetingClient().currentEndpoint().getApplicationId();
//        String endpointId = getPinpointManager().getPinpointContext().getTargetingClient().currentEndpoint().getEndpointId();
//        String sessionId = "";
//        try {
//            sessionId = getSesseionID();
//        } catch (Exception e) {
//            return;
//        }
//
//        AWSCredentialsProvider credentialsProvider = getPinpointManager().getPinpointContext().getPinpointConfiguration().getCredentialsProvider();
//
//        String versionNumber = "1";
//        String buildNumber = "2";
//        String prodName = "3";
//        String packageName = "3";
//
//        try {
//            JSONObject clientContext = new JSONObject();
//
//            JSONObject client = new JSONObject();
//            client.put("client_id", endpointId);
//            client.put("app_package_name", packageName);
//            client.put("installation_id", "038448DD-E25E-44FE-8128-046E7C682A9F");
//            client.put("app_version_name", buildNumber);
//            client.put("app_title", prodName);
//            client.put("app_version_code", versionNumber);
//
//            JSONObject env = new JSONObject();
//            env.put("locale", "en_US");
//            env.put("model", "Android");
//            env.put("model_version", "x86_64");
//            env.put("platform_version", "10.2");
//            env.put("make", "Android");
//            env.put("platform", "Android");
//
//            JSONObject services = new JSONObject();
//            JSONObject mobile_analytics = new JSONObject();
//            mobile_analytics.put("app_id", appId);
//            services.put("mobile_analytics", mobile_analytics);
//
//            JSONObject version = new JSONObject();
//            version.put("version", "1.0");
//
//            clientContext.put("client", client);
//            clientContext.put("env", env);
//            clientContext.put("services", services);
//            clientContext.put("version", version);
//
//
//            JSONObject eventsDict = new JSONObject();
//            String eventType = eventName;
//            JSONObject attributes = new JSONObject();
//
//            Iterator<Map.Entry<String, Object>> it = param.entrySet().iterator();
//            while (it.hasNext()) {
//                Map.Entry pair = (Map.Entry)it.next();
//                attributes.put((String)pair.getKey(), ""+ pair.getValue());
//                it.remove(); // avoids a ConcurrentModificationException
//            }
//
//            JSONObject session = new JSONObject();
//            session.put("id", sessionId);
//            session.put("startTimestamp", timestamp);
//
//            JSONObject event = new JSONObject();
//            event.put("timestamp", timestamp);
//            event.put("session", session);
//            event.put("eventType", eventType);
//            event.put("attributes", attributes);
//
//            JSONArray events = new JSONArray();
//            events.put(event);
//            eventsDict.put("events", events);
//
//
//            byte[] data = clientContext.toString().getBytes("UTF-8");
//            final String jsonString = Base64.encodeToString(data, Base64.DEFAULT);
//
//            data = eventsDict.toString().getBytes("UTF-8");
//            String paramsString = Base64.encodeToString(data, Base64.DEFAULT);
//
//            String url = host + path;
//
//
//            AWS4Signer signer = new AWS4Signer();
//            signer.setRegionName(Regions.AP_NORTHEAST_1.getName());
//            signer.setServiceName("MobileAnalytics");
//
//
//            Request request = new Request() {
//                @Override
//                public void addHeader(String s, String s1) {
//
//                }
//
//                @Override
//                public Map<String, String> getHeaders() {
//                    HashMap<String, String> map = new HashMap<>();
//                    map.put("x-amz-Client-Context", jsonString);
//                    return map;
//                }
//
//                @Override
//                public void setHeaders(Map map) {
//
//                }
//
//                @Override
//                public void setResourcePath(String s) {
//
//                }
//
//                @Override
//                public String getResourcePath() {
//                    return null;
//                }
//
//                @Override
//                public void addParameter(String s, String s1) {
//
//                }
//
//                @Override
//                public Request withParameter(String s, String s1) {
//                    return null;
//                }
//
//                @Override
//                public Map<String, String> getParameters() {
//                   return null;
//                }
//
//                @Override
//                public void setParameters(Map map) {
//
//                }
//
//                @Override
//                public URI getEndpoint() {
//                    return null;
//                }
//
//                @Override
//                public void setEndpoint(URI uri) {
//
//                }
//
//                @Override
//                public HttpMethodName getHttpMethod() {
//                    return null;
//                }
//
//                @Override
//                public void setHttpMethod(HttpMethodName httpMethodName) {
//
//                }
//
//                @Override
//                public InputStream getContent() {
//                    return null;
//                }
//
//                @Override
//                public void setContent(InputStream inputStream) {
//
//                }
//
//                @Override
//                public String getServiceName() {
//                    return null;
//                }
//
//                @Override
//                public AmazonWebServiceRequest getOriginalRequest() {
//                    return null;
//                }
//
//                @Override
//                public int getTimeOffset() {
//                    return 0;
//                }
//
//                @Override
//                public void setTimeOffset(int i) {
//
//                }
//
//                @Override
//                public Request withTimeOffset(int i) {
//                    return null;
//                }
//
//                @Override
//                public AWSRequestMetrics getAWSRequestMetrics() {
//                    return null;
//                }
//
//                @Override
//                public void setAWSRequestMetrics(AWSRequestMetrics awsRequestMetrics) {
//
//                }
//
//                @Override
//                public boolean isStreaming() {
//                    return false;
//                }
//
//                @Override
//                public void setStreaming(boolean b) {
//
//                }
//            };
//            signer.sign(request, credentialsProvider.getCredentials());
//
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//    }

//    public String getSesseionID() throws Exception {
//        SessionClient client = getPinpointManager().getSessionClient();
//        Class<?> clazz = client.getClass();
//        Method retrieveItems = clazz.getDeclaredMethod("getSession", String.class);
//        retrieveItems.setAccessible(true);
//        Session session = (Session)retrieveItems.invoke(client);
//        return session.getSessionID();
//    }
//
//    private String getISO8601DateTime() {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.UK);
//        String formattedDate = sdf.format(new Date());
//        return formattedDate;
//    }

//    private PinpointManager getPinpointManager(){
//        if(pinpointManager == null){
//            CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
//                    PMPApplication.getApplication().getApplicationContext(),    /* get the context for the application */
//                    "ap-northeast-1:3d3c64ea-678e-4a92-93de-8fc68fc34838",    /* Identity Pool ID */
//                    Regions.AP_NORTHEAST_1           /* Region for your identity pool--US_EAST_1 or EU_WEST_1*/
//            );
////        AmazonPinpointAnalyticsClient client = new AmazonPinpointAnalyticsClient(credentialsProvider);
//            PinpointConfiguration configuration = new PinpointConfiguration(PMPApplication.getApplication().getApplicationContext(), "e7f51ab12a3e47b0bc69b3c4b3957b99", Regions.US_EAST_1, credentialsProvider);
//            pinpointManager = new PinpointManager(configuration);
//        }
//
//        return pinpointManager;
//    }

    public AnalyticsEventCallback getEventCallback() {
        return eventCallback;
    }

    public void setEventCallback(AnalyticsEventCallback eventCallback) {
        this.eventCallback = eventCallback;
    }

    public native void nativeUpdateMapState();
}
