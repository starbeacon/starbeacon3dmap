package com.cherrypicks.pmpmap.analytics;

import java.util.Map;

/**
 * Created by andrewman on 8/11/16.
 */
public interface AnalyticsEventCallback {
    public void onReceivedEvent(String eventName, Map<String, Object> param);
}