package com.cherrypicks.pmpmap;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.pmp.mapsdk.app.PMPCheckPermissionActivity;
import com.pmp.mapsdk.app.PMPPermissionListener;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static com.pmp.mapsdk.app.PMPCheckPermissionActivity.PMP_PERMISSION_DID_CHANGED;

/**
 * Created by Alan on 16/2/16.
 */
public class GPSLocationManager implements LocationListener {
    public static final int PMP_PERMISSIONS_REQUEST_LOCATION = 31;
    private final int PMP_GEOCODER_INTERVAL = 30;

    private static GPSLocationManager instance;
    private LocationManager locationManager;
    private Context context;

    private LocationListener gpsLocationListener;
    private Location latestLocation;
    private static boolean isPermissionRequested = false;
    public boolean isInHongKongOrChina = false;
    private long lastTimeRequestedGeocoder = 0;
    private boolean isRequestdGPSLocationUpdates = false;
    private static PMPPermissionListener permissionListener = null;
    private BroadcastReceiver permissionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(PMP_PERMISSION_DID_CHANGED)) {
                boolean granted = intent.getBooleanExtra(PMP_PERMISSION_DID_CHANGED, false);
                if (granted && isRequestdGPSLocationUpdates) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, GPSLocationManager.this);
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, GPSLocationManager.this);
                }
                if (permissionListener != null) {
                    permissionListener.permissionResult(granted);
                }
            }
        }
    };

    public static GPSLocationManager getSharedManager(Context context) {
        if (instance == null) {
            instance = new GPSLocationManager(context);
        }

        return instance;
    }

    public GPSLocationManager(Context context) {
        this.context = context.getApplicationContext();
        locationManager = (LocationManager) this.context.getSystemService(Context.LOCATION_SERVICE);
        LocalBroadcastManager.getInstance(this.context).registerReceiver(permissionReceiver, new IntentFilter(PMP_PERMISSION_DID_CHANGED));
    }

    public void setGpsLocationListener(LocationListener gpsLocationListener) {
        this.gpsLocationListener = gpsLocationListener;
    }

    public static boolean checkIfLocationPermissionAllowed(Context context) {



        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else if(isLocationEnabled(context)){
            return true;
        }else{
            return false;
        }
    }

    private static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public static boolean checkIfLocationPermissionAllowed(Context context, PMPPermissionListener listener) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionListener = listener;
            if (!PMPCheckPermissionActivity.isRequestingPermission) {
                PMPCheckPermissionActivity.isRequestingPermission = true;
                Intent intent = new Intent(context, PMPCheckPermissionActivity.class);
                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
            return false;
        } else {
            return true;
        }
    }

//    public static boolean requireLocationPermissionIfNeeded(Activity activity) {
//        if (isPermissionRequested) {
//            //we should only prompt permission request once for each app session...
//            return true;
//        }
//        if (!checkIfLocationPermissionAllowed(activity)) {
//            isPermissionRequested = true;
//            // Should we show an explanation?
//            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
//                    Manifest.permission.ACCESS_FINE_LOCATION)) {
//
//                // Show an expanation to the user *asynchronously* -- don't block
//                // this thread waiting for the user's response! After the user
//                // sees the explanation, try again to request the permission.
//                ActivityCompat.requestPermissions(activity,
//                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                        PMP_PERMISSIONS_REQUEST_LOCATION);
//            } else {
//
//                // No explanation needed, we can request the permission.
//
//                ActivityCompat.requestPermissions(activity,
//                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                        PMP_PERMISSIONS_REQUEST_LOCATION);
//
//                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
//                // app-defined int constant. The callback method gets the
//                // result of the request.
//            }
//        }
//        return false;
//    }

    public void requestLocationUpdates(Activity context) {
        if (context != null) {
            isRequestdGPSLocationUpdates = true;
            if (checkIfLocationPermissionAllowed(context)) {
                //allowed
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            } else {
                //not allowed
                if (!PMPCheckPermissionActivity.isRequestingPermission) {
                    Intent intent = new Intent(context, PMPCheckPermissionActivity.class);
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            }
        }
    }

    public Location getLatestLocation(Activity context) {
        if (latestLocation != null) {
            return latestLocation;
        } else {
            if (checkIfLocationPermissionAllowed(context)) {
                Location lastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (lastLocation == null)
                    lastLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                return lastLocation;
            }
        }
        return null;
    }

    public void cancelLocationUpdate() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i("LocationManager","location.getAccuracy() = " +location.getAccuracy());
        this.latestLocation = location;
        if(gpsLocationListener != null)gpsLocationListener.onLocationChanged(location);

        long tsLong = System.currentTimeMillis()/1000;
        if (tsLong - lastTimeRequestedGeocoder > PMP_GEOCODER_INTERVAL) {
            lastTimeRequestedGeocoder = tsLong;
            double lat = location.getLatitude();
            double lng = location.getLongitude();
            Geocoder geoCoder = new Geocoder(this.context, Locale.ENGLISH);
            StringBuilder builder = new StringBuilder();
            try {
                List<Address> address = geoCoder.getFromLocation(lat, lng, 1);
                if (address.size() == 0) {
                    isInHongKongOrChina = false;
                }else {
                    int maxLines = address.get(0).getMaxAddressLineIndex();
                    for (int i = 0; i < maxLines; i++) {
                        String addressStr = address.get(0).getAddressLine(i);
                        builder.append(addressStr);
                        builder.append(" ");
                    }

                    builder.append(address.get(0).getLocality());
                    builder.append(" ");
                    builder.append(address.get(0).getCountryName());
                    builder.append(" ");

                    String finalAddress = builder.toString(); //This is the complete address.
                    if (finalAddress.toLowerCase().contains("hong kong") ||
                            finalAddress.toLowerCase().contains("china")) {
                        isInHongKongOrChina = true;
                    } else {
                        isInHongKongOrChina = false;
                    }
                }

            } catch (IOException e) {
                // Handle IOException
            } catch (NullPointerException e) {
                // Handle NullPointerException
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        if(gpsLocationListener != null)gpsLocationListener.onStatusChanged(provider, status, extras);

    }

    @Override
    public void onProviderEnabled(String provider) {
        if(gpsLocationListener != null)gpsLocationListener.onProviderEnabled(provider);

    }

    @Override
    public void onProviderDisabled(String provider) {
        if(gpsLocationListener != null)gpsLocationListener.onProviderDisabled(provider);

    }

}
