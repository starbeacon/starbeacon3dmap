package com.cherrypicks.pmpmap;

import android.graphics.Point;

/**
 * Created by andrew on 2/12/2016.
 */

public class Coord {
    public Point    pivot;
    public Point   position;
    public float   rotation;
    public float   scale;
    public float   tilt;//pitch
    public float   roll;
    public float   yaw;

    /*
    Support Rotation Only Yet
     */
    public Coord() {

    }

    public Coord(float rotation) {
        this.rotation = rotation;
    }

    public Coord(float scale, float rotation) {
        this.scale = scale;
        this.rotation = rotation;
    }

    public float getRotation() {
        return rotation;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }
}
