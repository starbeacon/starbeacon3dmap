package com.cherrypicks.pmpmap;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.FrameLayout;

import com.cherrypicks.pmpmap.ui.CameraPreview;

/**
 * Created by cpuser on 10/2/2017.
 */

public class PMPCameraManager {

    public static final int PMP_PERMISSIONS_REQUEST_CAMERA = 32;

    private static PMPCameraManager instance;
    private Context context;

    private Camera mCamera;
    private CameraPreview mPreview;

    private static boolean isCameraPermissionRequested = false;

    public static PMPCameraManager getSharedManager(Context context) {
        if (instance == null) {
            instance = new PMPCameraManager(context);
        }

        return instance;
    }

    public PMPCameraManager(Context context) {
        this.context = context.getApplicationContext();
    }

    public static boolean checkIfCameraHardwareAvailable(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    public static boolean checkIfCameraPermissionAllowed(Context context) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }

    public static void requestCameraPermissionIfNeeded(Fragment fragment) {
//        we want to request permission every time we need it
//        if (isCameraPermissionRequested) {
//            //we should only prompt permission request once for each app session...
//            return;
//        }
        if (!checkIfCameraPermissionAllowed(fragment.getContext())) {
            isCameraPermissionRequested = true;

            fragment.requestPermissions(new String[]{Manifest.permission.CAMERA}, PMP_PERMISSIONS_REQUEST_CAMERA);
        }
    }


    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
            e.printStackTrace();
        }
        return c; // returns null if camera is unavailable
    }

    public void addCameraView(Context context, FrameLayout preview){
        if (mCamera != null){
            mCamera.release();
            mCamera = null;
        }
        mCamera = getCameraInstance();
        mPreview = new CameraPreview(context, mCamera);
        preview.addView(mPreview);
    }

    public float getHorizontalViewAngle(){
        if (mCamera != null){
            Camera.Parameters parameters = mCamera.getParameters();
            return parameters.getHorizontalViewAngle();
        }

        return 0.0f;
    }
}
