package com.cherrypicks.pmpmap;

import com.pmp.mapsdk.app.PMPMapFragment;

/**
 * Created by andrew on 2/12/2016.
 */

public interface PMPMapControllerCallback {
    public void onPOISelect(int poiId) ;
    public void onDeselectAllPOI() ;
    public void onMapCoordUpdate(Coord coord) ;
    public void onMapSceneInitated();
}

