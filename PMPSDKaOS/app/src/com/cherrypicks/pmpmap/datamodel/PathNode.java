package com.cherrypicks.pmpmap.datamodel;

/**
 * Created by Alan on 17/2/16.
 */
public class PathNode {
    Vertex previousVertex;
    Vertex currentVertex;
    Vertex nextVertex;
    float previousDuration;
    int previousPathType;
    float nextDuration;
    int nextPathType;

    public PathNode(Vertex previousVertex, Vertex nextVertex, Vertex currentVertex) {
        this.previousVertex = previousVertex;
        this.nextVertex = nextVertex;
        this.currentVertex = currentVertex;
    }

    public Vertex getCurrentVertex() {
        return currentVertex;
    }

    public Vertex getNextVertex() {
        return nextVertex;
    }

    public Vertex getPreviousVertex() {
        return previousVertex;
    }

}
