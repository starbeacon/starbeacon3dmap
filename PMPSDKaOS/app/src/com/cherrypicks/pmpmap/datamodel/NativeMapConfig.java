package com.cherrypicks.pmpmap.datamodel;

/**
 * Created by Alan on 17/2/16.
 */
public class NativeMapConfig {
    float destinationThreshold;
    float rerouteThreshold;
    float passDestinationAlertThreadhols;
    boolean debugEnable;
    float navigationRecenterDelta;
    float navigationRecenterWaitingTime;

    public NativeMapConfig() {

    }

    public float getDestinationThreshold() {
        return destinationThreshold;
    }

    public void setDestinationThreshold(float destinationThreshold) {
        this.destinationThreshold = destinationThreshold;
    }

    public float getNavigationRecenterDelta() {
        return navigationRecenterDelta;
    }

    public void setNavigationRecenterDelta(float navigationRecenterDelta) {
        this.navigationRecenterDelta = navigationRecenterDelta;
    }

    public float getNavigationRecenterWaitingTime() {
        return navigationRecenterWaitingTime;
    }

    public void setNavigationRecenterWaitingTime(float navigationRecenterWaitingTime) {
        this.navigationRecenterWaitingTime = navigationRecenterWaitingTime;
    }

    public float getPassDestinationAlertThreadhols() {
        return passDestinationAlertThreadhols;
    }

    public void setPassDestinationAlertThreadhols(float passDestinationAlertThreadhols) {
        this.passDestinationAlertThreadhols = passDestinationAlertThreadhols;
    }

    public float getRerouteThreshold() {
        return rerouteThreshold;
    }

    public void setRerouteThreshold(float rerouteThreshold) {
        this.rerouteThreshold = rerouteThreshold;
    }

    public boolean isDebugEnable() {
        return debugEnable;
    }

    public void setDebugEnable(boolean setDebugEnable) {
        this.debugEnable = setDebugEnable;
    }
}
