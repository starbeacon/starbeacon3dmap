package com.cherrypicks.pmpmap.datamodel;

/**
 * Created by Alan on 17/2/16.
 */
public class Edge {
    public static class Condition{
        public static int FOOT       =  1 << 0;
        public static int WHEELCHAIR =  1 << 1;
    }

    public static class PathType{
        public static int Node              =  1;
        public static int Escalator_Up      =  2;
        public static int Escalator_Down    =  3;
        public static int Staircase_Up      =  4;
        public static int Staircase_Down    =  5;
        public static int Lift_Down         =  6;
        public static int Lift_Up           =  7;
        public static int Path_Entry        =  8;
        public static int Immigration_Check =  9;
        public static int Automated_People_Mover = 10;
        public static int Shuttle_Bus       = 11;
        public static int Moving_Walkway    = 12;
        public static int Security_Check    = 13;
        public static int Custom_Check      = 14;
        public static int Arrival_Immigration_Check = 15;
    }

    Vertex toVertex;
    Vertex fromVertex;
    double weight;
    double distance;
    int condition;
    int pathType;

    public Edge(Vertex fv, Vertex tv, double weight, double distance){
        super();
        this.condition  = Condition.FOOT;
        this.pathType   = PathType.Node;
        this.weight     = weight;
        this.fromVertex = fv;
        this.toVertex   = tv;
        this.distance   = distance;
    }

    public Edge(Vertex fv, Vertex tv, double weight, double distance, int condition, int pathType){
        super();
        this.fromVertex = fv;
        this.toVertex   = tv;
        this.weight     = weight;
        this.condition  = condition;
        this.pathType   = pathType;
        this.distance   = distance;
    }

    public int getCondition() {
        return condition;
    }

    public Vertex getFromVertex() {
        return fromVertex;
    }

    public int getPathType() {
        return pathType;
    }

    public Vertex getToVertex() {
        return toVertex;
    }

    public double getWeight() {
        return weight;
    }

    public double getDistance() {
        return distance;
    }
}
