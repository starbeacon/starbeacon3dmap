package com.cherrypicks.pmpmap.datamodel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by andrewman on 7/28/16.
 */
public class MapState implements Serializable {
    private final static String SELECTED_POI_ID_KEY = "selectedPOIId";
    private final static String MAP_COORD_PIVOT_X_KEY = "mapCoordPivotX";
    private final static String MAP_COORD_PIVOT_Y_KEY = "mapCoordPrivotY";
    private final static String MAP_COORD_POSITION_X_KEY = "mapCoordPositionX";
    private final static String MAP_COORD_POSITION_Y_KEY = "mapCoordPositionY";
    private final static String MAP_COORD_SCALE_KEY = "mapCoordScale";
    private final static String MAP_COORD_TILT_KEY = "mapCoordTilt";
    private final static String MAP_COORD_ROTATION_KEY = "mapCoordRotation";
    private final static String MAP_ID_KEY = "mapId";
    private final static String INDOOR_KEY = "indoor";
    private final static String MAP_MODE_KEY = "mapMode";

    private int selectedPOIId = -1;
    private float mapCoordPivotX = 0;
    private float mapCoordPivotY = 0;
    private float mapCoordPositionX = 0;
    private float mapCoordPositionY = 0;
    private float mapCoordScale = 1;
    private float mapCoordTilt = 0;
    private float mapCoordRotation = 0;
    private int mapId;
    private boolean indoor = false;
    private int mapMode;

    public MapState() {

    }

    public MapState(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has(SELECTED_POI_ID_KEY)) {
            selectedPOIId = jsonObject.getInt(SELECTED_POI_ID_KEY);
        }
        if (jsonObject.has(MAP_COORD_PIVOT_X_KEY)) {
            mapCoordPivotX = (float) jsonObject.getDouble(MAP_COORD_PIVOT_X_KEY);
        }
        if (jsonObject.has(MAP_COORD_PIVOT_Y_KEY)) {
            mapCoordPivotY = (float)jsonObject.getDouble(MAP_COORD_PIVOT_Y_KEY);
        }
        if (jsonObject.has(MAP_COORD_POSITION_X_KEY)) {
            mapCoordPositionX = (float)jsonObject.getDouble(MAP_COORD_POSITION_X_KEY);
        }
        if (jsonObject.has(MAP_COORD_POSITION_Y_KEY)) {
            mapCoordPositionY = (float)jsonObject.getDouble(MAP_COORD_POSITION_Y_KEY);
        }
        if (jsonObject.has(MAP_COORD_SCALE_KEY)) {
            mapCoordScale = (float)jsonObject.getDouble(MAP_COORD_SCALE_KEY);
        }
        if (jsonObject.has(MAP_COORD_TILT_KEY)) {
            mapCoordTilt = (float)jsonObject.getDouble(MAP_COORD_TILT_KEY);
        }
        if (jsonObject.has(MAP_ID_KEY)) {
            mapId = jsonObject.getInt(MAP_ID_KEY);
        }
        if (jsonObject.has(INDOOR_KEY)) {
            indoor = jsonObject.getBoolean(INDOOR_KEY);
        }
        if (jsonObject.has(MAP_MODE_KEY)) {
            mapMode = jsonObject.getInt(MAP_MODE_KEY);
        }
        if (jsonObject.has(MAP_COORD_ROTATION_KEY)) {
            mapCoordRotation = (float)jsonObject.getDouble(MAP_COORD_ROTATION_KEY);
        }
    }

    public int getSelectedPOIId() {
        return selectedPOIId;
    }

    public void setSelectedPOIId(int selectedPOIId) {
        this.selectedPOIId = selectedPOIId;
    }

    public float getMapCoordPivotX() {
        return mapCoordPivotX;
    }

    public void setMapCoordPivotX(float mapCoordPivotX) {
        this.mapCoordPivotX = mapCoordPivotX;
    }

    public float getMapCoordPivotY() {
        return mapCoordPivotY;
    }

    public void setMapCoordPivotY(float mapCoordPivotY) {
        this.mapCoordPivotY = mapCoordPivotY;
    }

    public float getMapCoordPositionX() {
        return mapCoordPositionX;
    }

    public void setMapCoordPositionX(float mapCoordPositionX) {
        this.mapCoordPositionX = mapCoordPositionX;
    }

    public float getMapCoordPositionY() {
        return mapCoordPositionY;
    }

    public void setMapCoordPositionY(float mapCoordPositionY) {
        this.mapCoordPositionY = mapCoordPositionY;
    }

    public float getMapCoordScale() {
        return mapCoordScale;
    }

    public void setMapCoordScale(float mapCoordScale) {
        this.mapCoordScale = mapCoordScale;
    }

    public float getMapCoordTilt() {
        return mapCoordTilt;
    }

    public void setMapCoordTilt(float mapCoordTilt) {
        this.mapCoordTilt = mapCoordTilt;
    }

    public int getMapId() {
        return mapId;
    }

    public void setMapId(int mapId) {
        this.mapId = mapId;
    }

    public boolean isIndoor() {
        return indoor;
    }

    public void setIndoor(boolean indoor) {
        this.indoor = indoor;
    }

    public int getMapMode() {
        return mapMode;
    }

    public void setMapMode(int mapMode) {
        this.mapMode = mapMode;
    }

    public float getMapCoordRotation() {
        return mapCoordRotation;
    }

    public void setMapCoordRotation(float mapCoordRotation) {
        this.mapCoordRotation = mapCoordRotation;
    }

    public JSONObject createJSONObject() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(SELECTED_POI_ID_KEY, selectedPOIId);
        jsonObject.put(MAP_COORD_PIVOT_X_KEY, mapCoordPivotX);
        jsonObject.put(MAP_COORD_PIVOT_Y_KEY, mapCoordPivotY);
        jsonObject.put(MAP_COORD_POSITION_X_KEY, mapCoordPositionX);
        jsonObject.put(MAP_COORD_POSITION_Y_KEY, mapCoordPositionY);
        jsonObject.put(MAP_COORD_SCALE_KEY, mapCoordScale);
        jsonObject.put(MAP_COORD_TILT_KEY, mapCoordTilt);
        jsonObject.put(MAP_ID_KEY, mapId);
        jsonObject.put(INDOOR_KEY, indoor);
        jsonObject.put(MAP_MODE_KEY, mapMode);
        jsonObject.put(MAP_COORD_ROTATION_KEY, mapCoordRotation);
        return jsonObject;
    }
}
