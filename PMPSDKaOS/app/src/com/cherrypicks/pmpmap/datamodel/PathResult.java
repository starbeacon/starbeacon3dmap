package com.cherrypicks.pmpmap.datamodel;

import java.util.ArrayList;

/**
 * Created by Alan on 17/2/16.
 */
public class PathResult {
    ArrayList<PathNode> pathNodes;
    float totalDistance;
    Vertex fromVertex;
    Vertex toVertex;

    public PathResult(Vertex fromVertex, Vertex toVertex, ArrayList<PathNode> pathNodes, float totalDistance) {
        this.fromVertex = fromVertex;
        this.pathNodes = pathNodes;
        this.totalDistance = totalDistance;
        this.toVertex = toVertex;
    }

    public Vertex getFromVertex() {
        return fromVertex;
    }

    public ArrayList<PathNode> getPathNodes() {
        return pathNodes;
    }

    public float getTotalDistance() {
        return totalDistance;
    }

    public Vertex getToVertex() {
        return toVertex;
    }

    public boolean isIntersectPathType(int nextPathType) {
        boolean ret = false;
        for (PathNode node :
                pathNodes) {
            if (node.nextPathType == nextPathType) {
                ret = true;
                break;
            }
        }
        return ret;
    }
}
