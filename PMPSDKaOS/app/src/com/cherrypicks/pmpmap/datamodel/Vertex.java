package com.cherrypicks.pmpmap.datamodel;

import java.util.ArrayList;

/**
 * Created by Alan on 17/2/16.
 */
public class Vertex {
    int node_id;
    ArrayList<Edge> toEdges;
    ArrayList<Edge> fromEdges;
    float x, y, z;
    int mapId;//0 - G/F, 1- 1/F

    public Vertex(int node_id) {
        this.node_id = node_id;
    }

    public void setFromEdges(ArrayList<Edge> fromEdges) {
        this.fromEdges = fromEdges;
    }

    public void setNode_id(int node_id) {
        this.node_id = node_id;
    }

    public void setToEdges(ArrayList<Edge> toEdges) {
        this.toEdges = toEdges;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public ArrayList<Edge> getFromEdges() {
        return fromEdges;
    }

    public int getNode_id() {
        return node_id;
    }

    public ArrayList<Edge> getToEdges() {
        return toEdges;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    public int getMapId() {
        return mapId;
    }

    public void setMapId(int mapId) {
        this.mapId = mapId;
    }
}
