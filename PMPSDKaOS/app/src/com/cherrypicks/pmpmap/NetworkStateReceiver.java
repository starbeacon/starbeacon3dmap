package com.cherrypicks.pmpmap;

/**
 * Created by dickywong on 7/12/17.
 */


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.pmp.mapsdk.cms.model.Tag;
import com.pmp.mapsdk.external.PMPMapSDK;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static android.content.Context.MODE_PRIVATE;

public class NetworkStateReceiver extends BroadcastReceiver {

    private static List<ConnectionListener> listeners = new CopyOnWriteArrayList<ConnectionListener>();

    /**
     * Add a listener to be notified about any network changes.
     * This method is thread safe.
     * <p>
     * IMPORTANT: Not removing it again will result in major leaks.
     *
     * @param listener the listener.
     */
    public static void addListener(ConnectionListener listener) {
        listeners.add(listener);
    }

    /**
     * Removes a network listener.
     * This method is thread safe.
     *
     * @param listener the listener.
     */
    public static synchronized void removeListener(ConnectionListener listener) {
        listeners.remove(listener);
    }

    /**
     * Attempt to detect if a device is online and can transmit or receive data.
     * This method is thread safe.
     * <p>
     * An emulator is always considered online, as `getActiveNetworkInfo()` does not report the correct value.
     *
     * @param context an Android context.
     * @return {@code true} if device is online, otherwise {@code false}.
     */
    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnectedOrConnecting());
    }


    @Override
    public void onReceive(Context context, Intent intent) {

        boolean connected = isOnline(context);
        SharedPreferences sharedPreferences = context.getSharedPreferences("connection_changed" , MODE_PRIVATE);
        boolean savedConnectionState = sharedPreferences.getBoolean("connection_state_key" , false);
//        Log.v("dicky","dicky network saveed state is "+savedConnectionState);
//        Log.v("dicky","dicky network state is "+connected);
        if(connected == savedConnectionState){

        }else{
            sharedPreferences.edit().putBoolean("connection_changed_key", true).apply();
        }
        sharedPreferences.edit().putBoolean("connection_state_key", connected).apply();


        for (ConnectionListener listener : listeners) {
            listener.onChange(connected);
        }
    }

    public interface ConnectionListener {
        void onChange(boolean connectionAvailable);
    }
}
