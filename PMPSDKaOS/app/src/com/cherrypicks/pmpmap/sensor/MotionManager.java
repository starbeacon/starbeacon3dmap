package com.cherrypicks.pmpmap.sensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.opengl.Matrix;
import android.util.Log;

import com.cherrypicks.pmpmap.PMPDataManager;
import com.cherrypicks.pmpmap.core.CoreEngine;

/**
 * Created by andrewman on 26/6/2017.
 */

public class MotionManager implements SensorEventListener {
    private final static String TAG = "MotionManager";
    private static MotionManager instance;
    private Context context;
    private SensorManager sensorManager;
    private Sensor rotationVectorSensor;
    private Sensor gameRotationVectgorSensor;
    private Sensor accelerometerSensor;
    private Sensor magneticSensor;
    private float rotationMartix[] = new float[16];
    private float quaternion[] = new float[4];
    private float rotationVector[] = new float[3];
    private float orientation[] = new float[3];

    private float[] mGData = new float[3];
    private float[] mMData = new float[3];
    private float[] mR = new float[16];
    private float[] mI = new float[16];
    private float[] mOrientation = new float[3];
    private static final float MIN_TIME_STEP = (1f / 40f);
    private long mLastTime = System.currentTimeMillis();
    private final float alpha = 0.98f;

    public static MotionManager getInstance(Context ctx) {
        if (instance == null && ctx != null) {
            instance = new MotionManager(ctx);
        }
        return instance;
    }

    public static MotionManager getInstance() {
        return instance;
    }

    private MotionManager(Context ctx) {
        this.context = ctx;
        sensorManager = (SensorManager) ctx.getSystemService(Context.SENSOR_SERVICE);
        rotationVectorSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        gameRotationVectgorSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);

        if (rotationVectorSensor != null) {
            sensorManager.registerListener(this,rotationVectorSensor, 10000);
        }else {
            accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            magneticSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            sensorManager.registerListener(this,accelerometerSensor, 10000);
            sensorManager.registerListener(this,magneticSensor, 10000);
        }

        if (gameRotationVectgorSensor != null) {
            sensorManager.registerListener(this,gameRotationVectgorSensor, 10000);
        }

        /*
        rotationMartix[ 0] = 1;
        rotationMartix[ 4] = 1;
        rotationMartix[ 8] = 1;
        rotationMartix[12] = 1;
        */
        rotationMartix[0] = 1;
        rotationMartix[5] = 1;
        rotationMartix[10] = 1;
        rotationMartix[15] = 1;
    }

    public float[] getRotationMartix() {
        return rotationMartix;
    }

    public float[] getQuaternion() {
        return quaternion;
    }

    public float[] getRotationVector() {
        return rotationVector;
    }

    public float[] getOrientation() {
        return orientation;
    }


    /*
    private void reset() {
        for (int i = 0; i < rotationMartix.length; i++) {
            rotationMartix[i] = 0;
        }

        for (int i = 0; i < quaternion.length; i++) {
            quaternion[i] = 0;
        }
    }*/

    private void updateCompass() {
        SensorManager.getRotationMatrix(mR, mI, mGData, mMData);
// some test code which will be used/cleaned up before we ship this.
//        SensorManager.remapCoordinateSystem(mR,
//                SensorManager.AXIS_X, SensorManager.AXIS_Z, mR);
//        SensorManager.remapCoordinateSystem(mR,
//                SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, mR);
        SensorManager.getOrientation(mR, mOrientation);
        float incl = SensorManager.getInclination(mI);
        final float rad2deg = (float)(180.0f/Math.PI);
//        Log.d("Compass", "yaw: " + (int)(mOrientation[0]*rad2deg) +
//                "  pitch: " + (int)(mOrientation[1]*rad2deg) +
//                "  roll: " + (int)(mOrientation[2]*rad2deg) +
//                "  incl: " + (int)(incl*rad2deg)
//        );

        CoreEngine.getInstance().setBearing(Math.round(mOrientation[0]*rad2deg));
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
//        reset();
        switch (event.sensor.getType()) {
            case Sensor.TYPE_ORIENTATION:
            {

            }
                break;
            case Sensor.TYPE_GAME_ROTATION_VECTOR:
            {
                rotationVector[0] = event.values[0];
                rotationVector[1] = event.values[1];
                rotationVector[2] = event.values[2];
//                Log.d(TAG, "x:" + event.values[0]);
//                Log.d(TAG, "y:" + event.values[1]);
//                Log.d(TAG, "z:" + event.values[2]);
                float mat[] = new float[16];

                SensorManager.getRotationMatrixFromVector(mat, event.values);
                float newRotationMartix[] = new float[16];
//                SensorManager.remapCoordinateSystem(mat, SensorManager.AXIS_X, SensorManager.AXIS_Z, newRotationMartix);
                SensorManager.remapCoordinateSystem(mat, SensorManager.AXIS_MINUS_X, SensorManager.AXIS_MINUS_Z, newRotationMartix);
                if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR)
                    Matrix.rotateM(newRotationMartix, 0, -CoreEngine.getInstance().getBearing(), 0,0,1);
//                StringBuffer sb = new StringBuffer();
//                for (int i = 0; i < 4; i++) {
//                    for (int n = 0; n < 4; n++) {
//                        sb.append(rotationMartix[(i + 1) * n]);
//                        sb.append(",");
//                    }
//                    sb.append('\n');
//                }
//                Log.d(TAG, "RotMartix:" + sb.toString());

                SensorManager.getQuaternionFromVector(quaternion, rotationVector);
                /*
                float kFilteringFactor = 0.5f;
                //low pass filter
                for (int i = 0; i< rotationMartix.length;i++) {
                    float f = newRotationMartix[i] * kFilteringFactor + rotationMartix[i] * (1.0f - kFilteringFactor);
                    rotationMartix[i] = f;
                }*/
                rotationMartix = newRotationMartix;

                float quaternion[] = new float[4];
                SensorManager.getQuaternionFromVector(quaternion, event.values);
                float qx = quaternion[1],qy = quaternion[2],qz = quaternion[3],qw = quaternion[0];
                double yaw = (float) Math.atan2(2.0*(qy*qz + qw*qx), qw*qw - qx*qx - qy*qy + qz*qz);
                double pitch = Math.asin(-2.0*(qx*qz - qw*qy));
                double roll = Math.atan2(2.0*(qx*qy + qw*qz), qw*qw + qx*qx - qy*qy - qz*qz);
//                Log.d(TAG, "!@Yaw:" + Math.toDegrees(yaw));
//                Log.d(TAG, "!@pitch:" + Math.toDegrees(pitch));
//                Log.d(TAG, "!@roll:" + Math.toDegrees(roll));
                orientation[0] = (float) -roll;
//                orientation[1] = (float) -yaw;
//                orientation[2] = (float) -pitch;
                float orientation[] = new float[3];
                SensorManager.getOrientation(mat, orientation);
                this.orientation[1] = -orientation[1];//pitch
                this.orientation[2] = -orientation[2];//roll

            }
                break;
            case Sensor.TYPE_ROTATION_VECTOR:
            {
//                Log.d(TAG, "x:" + event.values[0]);
//                Log.d(TAG, "y:" + event.values[1]);
//                Log.d(TAG, "z:" + event.values[2]);
                float mat[] = new float[16];

                SensorManager.getRotationMatrixFromVector(mat, event.values);
                float newRotationMartix[] = new float[16];
                SensorManager.remapCoordinateSystem(mat, SensorManager.AXIS_X, SensorManager.AXIS_Z, newRotationMartix);
//                SensorManager.remapCoordinateSystem(mat, SensorManager.AXIS_MINUS_X, SensorManager.AXIS_MINUS_Z, newRotationMartix);
                if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR)
                    Matrix.rotateM(newRotationMartix, 0, -CoreEngine.getInstance().getBearing(), 0,0,1);
//                StringBuffer sb = new StringBuffer();
//                for (int i = 0; i < 4; i++) {
//                    for (int n = 0; n < 4; n++) {
//                        sb.append(rotationMartix[(i + 1) * n]);
//                        sb.append(",");
//                    }
//                    sb.append('\n');
//                }
//                Log.d(TAG, "RotMartix:" + sb.toString());


                if (gameRotationVectgorSensor == null) {
                    rotationVector[0] = event.values[0];
                    rotationVector[1] = event.values[1];
                    rotationVector[2] = event.values[2];
                    rotationMartix = newRotationMartix;
                    if(PMPDataManager.getSharedPMPManager(null).getResponseData() != null)
                        Matrix.rotateM(rotationMartix, 0, (float) -PMPDataManager.getSharedPMPManager(null).getResponseData().getMapDirection(), 0,0,1);
                }

                /*
                float quaternion[] = new float[4];
                SensorManager.getQuaternionFromVector(quaternion, event.values);
                float qx = quaternion[1],qy = quaternion[2],qz = quaternion[3],qw = quaternion[0];
                double yaw = (float) Math.atan2(2.0*(qy*qz + qw*qx), qw*qw - qx*qx - qy*qy + qz*qz);
                double pitch = Math.asin(-2.0*(qx*qz - qw*qy));
                double roll = Math.atan2(2.0*(qx*qy + qw*qz), qw*qw + qx*qx - qy*qy - qz*qz);
                Log.d(TAG, "@Yaw:" + Math.toDegrees(yaw));
                Log.d(TAG, "@pitch:" + Math.toDegrees(pitch));
                Log.d(TAG, "@roll:" + Math.toDegrees(roll));
                if (gameRotationVectgorSensor == null)
                    orientation[0] = (float) roll;
                CoreEngine.getInstance().setBearing((float) Math.toDegrees(roll));
                */

                float orientation[] = new float[3];
                SensorManager.getOrientation(mat, orientation);
                double bearing = (float)(Math.toDegrees(orientation[0])+360.0) % 360.0;
                CoreEngine.getInstance().setBearing((float) bearing);
//                Log.d(TAG, "@bearing:" + bearing);
//                Log.d(TAG, "@orientation[0]:" +Math.toDegrees(orientation[0]) );
//                Log.d(TAG, "@orientation[1]:" +Math.toDegrees(orientation[1]) );
//                Log.d(TAG, "@orientation[2]:" +Math.toDegrees(orientation[2]) );
                if (gameRotationVectgorSensor == null) {
                    this.orientation[0] = -orientation[0];//yaw
                    this.orientation[1] = -orientation[1];//pitch
                    this.orientation[2] = -orientation[2];//roll
                }
            }
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
            {
                for (int i=0 ; i<3 ; i++)
                    mMData[i] = alpha * mMData[0] + (1.0f - alpha) * event.values[i];
//                    mMData[i] = event.values[i];
                updateCompass();
            }
                break;
            case Sensor.TYPE_ACCELEROMETER:
            {
                for (int i=0 ; i<3 ; i++)
                    mGData[i] = alpha * mGData[0] + (1.0f - alpha)  * event.values[i];
//                    mGData[i] = event.values[i];
                updateCompass();
            }
                break;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        Log.d(TAG, "onAccuracyChanged");
    }

    public Sensor getRotationVectorSensor() {
        return rotationVectorSensor;
    }

    public Sensor getGameRotationVectgorSensor() {
        return gameRotationVectgorSensor;
    }

    public float getYaw() {
        return orientation[0];
    }

    public float getPitch() {
        return orientation[1];
    }

    public float getRoll() {
        return orientation[2];
    }
}
