package com.cherrypicks.pmpmap;

import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andrew on 29/11/2016.
 */

public class PMPMapController implements PMPMapControllerCallback, PMPAugmentedRealityCallback{
    private final static String TAG = "PMPMapController";
    private static PMPMapController instance;
    private CocosFragment cocosFragment;
    private List<PMPMapControllerCallback> callbacks = new ArrayList<PMPMapControllerCallback>();
    private List<PMPAugmentedRealityCallback> arCallbacks = new ArrayList<PMPAugmentedRealityCallback>();
    private Handler handler = new Handler(Looper.getMainLooper());
    public static PMPMapController getInstance() {
        if (instance == null) {
            instance = new PMPMapController();
        }
        return instance;
    }

    private PMPMapController() {
        nativeInitial();
    }

    public CocosFragment getCocosFragment() {
        return cocosFragment;
    }

    public void setCocosFragment(CocosFragment cocosFragment) {
        this.cocosFragment = cocosFragment;
    }

    public void addCallback(final PMPMapControllerCallback cb) {
        if (cb != null) {
            PMPMapControllerCallback found = CollectionUtils.find(callbacks, new Predicate<PMPMapControllerCallback>() {
                @Override
                public boolean evaluate(PMPMapControllerCallback object) {
                    return cb.equals(object);
                }
            });
            if (found == null) {
                callbacks.add(cb);
            }
        }
    }

    public void removeCallback(final PMPMapControllerCallback cb) {
        if (cb != null) {
            PMPMapControllerCallback found = CollectionUtils.find(callbacks, new Predicate<PMPMapControllerCallback>() {
                @Override
                public boolean evaluate(PMPMapControllerCallback object) {
                    return cb.equals(object);
                }
            });
            if (found != null) {
                callbacks.remove(cb);
            }
        }
    }

    public void removeCallback(final PMPMapControllerWithFragment cb) {
        if (cb != null) {
            PMPMapControllerCallback found = CollectionUtils.find(callbacks, new Predicate<PMPMapControllerCallback>() {
                @Override
                public boolean evaluate(PMPMapControllerCallback object) {
                    return cb.equals(object);
                }
            });
            if (found != null) {
                callbacks.remove(cb);
            }
        }
    }


    public void addARCallback(final PMPAugmentedRealityCallback cb) {
        if (cb != null) {
            PMPAugmentedRealityCallback found = CollectionUtils.find(arCallbacks, new Predicate<PMPAugmentedRealityCallback>() {
                @Override
                public boolean evaluate(PMPAugmentedRealityCallback object) {
                    return cb.equals(object);
                }
            });
            if (found == null) {
                arCallbacks.add(cb);
            }
        }
    }

    public void removeARCallback(final PMPAugmentedRealityCallback cb) {
        if (cb != null) {
            PMPAugmentedRealityCallback found = CollectionUtils.find(arCallbacks, new Predicate<PMPAugmentedRealityCallback>() {
                @Override
                public boolean evaluate(PMPAugmentedRealityCallback object) {
                    return cb.equals(object);
                }
            });
            if (found != null) {
                arCallbacks.remove(cb);
            }
        }
    }

    public void setCurrnetFloorById(final int floorId) {
        if (cocosFragment != null) {
            cocosFragment.runOnGLThread(new Runnable() {
                @Override
                public void run() {
                    nativeSetCurrnetFloorById(floorId);
                }
            });
        }
    }

    public void setSelectedPOIId(final int poiId) {
        if (cocosFragment != null) {
            cocosFragment.runOnGLThread(new Runnable() {
                @Override
                public void run() {
                    nativeSetSelectedPOIId(poiId);
                }
            });
        }
    }

    public void deselectAllPOI() {
        if (cocosFragment != null) {
            cocosFragment.runOnGLThread(new Runnable() {
                @Override
                public void run() {
                    nativeDeselectAllPOI();
                }
            });
        }
    }

    public void jumpToPosition(final float x, final float y, final float screenX, final float screenY) {
        if (cocosFragment != null) {
            cocosFragment.runOnGLThread(new Runnable() {
                @Override
                public void run() {
                    nativeJumpToPosition(x, y, screenX, screenY);
                }
            });
        }
    }

    public int getCurrentFloorId(){
        return nativeGetCurrentFloorId();

    }

    public void setFocusPOIIds(final List<Integer> ids) {
        if (cocosFragment != null) {
            cocosFragment.runOnGLThread(new Runnable() {
                @Override
                public void run() {
                    nativeSetFocusPOIIds(ids);
                }
            });
        }
    }

    public void setMapCoord(Coord coord) {
        nativeSetMapCoord(coord);
    }

    public Coord getMapCoord() {
        return nativeGetMapCoord();
    }

    public void fitBounds(final RectF bounds, final RectF padding, final boolean animated) {
        if (cocosFragment != null) {
            cocosFragment.runOnGLThread(new Runnable() {
                @Override
                public void run() {
                    nativeFitBounds(bounds, padding, animated);
                }
            });
        }
    }

    public void setFocusPOICategoryId(int id) {
        nativeSetFocusPOICategoryId(id);
    }

    public int getFocusPOICategoryId() {
        return nativeGetFocusPOICategoryId();
    }

    public void setOverviewing(boolean b) {
        nativeSetOverviewing(b);
    }

    public boolean isOverviewing() {
        return nativeIsOverviewing();
    }

    public boolean isMapSceneInitated() {
        return nativeIsMapSceneInitated();
    }

    public void redrawMapScene() {
        if (cocosFragment != null) {
            cocosFragment.runOnGLThread(new Runnable() {
                @Override
                public void run() {
                    nativeRedrawMapScene();
                }
            });
        }
    }

    public void setEnablePathOverview(boolean b) {
        nativeSetEnablePathOverview(b);
    }

    public boolean isEnablePathOverview() {
        return nativeIsEnablePathOverview();
    }

    public float scaleForBounds(final RectF bounds, final RectF padding) {
        return nativeScaleForBounds(bounds, padding);
    }

    public void setZoomLevel(final float scale) {
        if (cocosFragment != null) {
            cocosFragment.runOnGLThread(new Runnable() {
                @Override
                public void run() {
                    nativeSetZoomLevel(scale);
                }
            });
        }
    }

    public void showAR(final float headerHeight,
                       final float footerHeight,
                       final float angle,
                       final boolean isScrBlocked,
                       final boolean isArrived,
                       final boolean isBypassed,
                       final String mtelRecordId,
                       final String flightNo,
                       final String gateCode,
                       final String flightStatus,
                       final String departureTime) {
        if (cocosFragment != null) {
            cocosFragment.runOnGLThread(new Runnable() {
                @Override
                public void run() {
                    nativeShowAR(headerHeight, footerHeight, angle, isScrBlocked, isArrived, isBypassed, mtelRecordId, flightNo, gateCode, flightStatus, departureTime);
                }
            });
        }
    }

    public void updateUserFlightForARView(final String mtelRecordId,
                                          final String flightNo,
                                          final String gateCode,
                                          final String flightStatus,
                                          final String departureTime) {
        nativeUpdateUserFlightForARView(mtelRecordId, flightNo, gateCode, flightStatus, departureTime);
    }

    public void exitAR() {

    }

    public void replaceSceneWithMapScene() {
        if (cocosFragment != null) {
            cocosFragment.runOnGLThread(new Runnable() {
                @Override
                public void run() {
                    nativeReplaceSceneWithMapScene();
                }
            });
        }
    }

    public boolean isARViewShowing() {
        return nativeIsARViewShowing();
    }

    public boolean isARMasking() {
        return nativeIsARMasking();
    }

    public int getScreenCenterX(){
        return nativeGetScreenCenterX();
    }

    public int getScreenCenterY(){
        return nativeGetScreenCenterY();
    }

    private native void nativeInitial();
    private native void nativeSetCurrnetFloorById(int floorId);
    private native int nativeGetCurrentFloorId();
    private native void nativeSetSelectedPOIId(int poiId);
    private native void nativeDeselectAllPOI();
    private native void nativeJumpToPosition(float x, float y, float screenX, float screenY);
    private native void nativeSetFocusPOIIds(List<Integer> ids);
    private native void nativeSetMapCoord(Coord coord);
    private native Coord nativeGetMapCoord();
    private native void nativeFitBounds(RectF bounds, RectF padding, boolean animated);
    private native void nativeSetFocusPOICategoryId(int id);
    private native int nativeGetFocusPOICategoryId();
    private native boolean nativeIsOverviewing();
    private native void nativeSetOverviewing(boolean b);
    private native boolean nativeIsMapSceneInitated();
    private native void nativeRedrawMapScene();
    private native void nativeSetEnablePathOverview(boolean b);
    private native boolean nativeIsEnablePathOverview();
    private native float nativeScaleForBounds(final RectF bounds, final RectF padding);
    private native void nativeSetZoomLevel(float scale);
    private native void nativeShowAR(float headerHeight, float footerHeight, float angle, boolean isScrBlocked, boolean isArrived, boolean isBypassed, String mtelRecordId, String flightNo, String gateCode, String flightStatus, String departureTime);
    private native void nativeUpdateUserFlightForARView(String mtelRecordId, String flightNo, String gateCode, String flightStatus, String departureTime);
    private native void nativeReplaceSceneWithMapScene();
    /*
    private native void nativeSetARViewArrived(boolean b);
    private native void nativeSetARViewBypassed(boolean b);
    private native void nativeSetARScreenBlocked(boolean b);
    private native void nativeResetBypassed();
    */
    private native boolean nativeIsARViewShowing();
    private native boolean nativeIsARMasking();

    private native int nativeGetScreenCenterX();
    private native int nativeGetScreenCenterY();


    @Override
    public void onPOISelect(final int poiId) {
        //Log.d(TAG, "onPOISelect:" + poiId);
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (PMPMapControllerCallback cb:
                        callbacks) {
                    cb.onPOISelect(poiId);
                }
            }
        });
    }

    @Override
    public void onDeselectAllPOI() {
        //Log.d(TAG, "onDeselectAllPOI");
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (PMPMapControllerCallback cb:
                        callbacks) {
                    cb.onDeselectAllPOI();
                }
            }
        });
    }

    @Override
    public void onMapCoordUpdate(final Coord coord) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (PMPMapControllerCallback cb:
                        callbacks) {
                    cb.onMapCoordUpdate(coord);
                }
            }
        });
    }

    @Override
    public void onMapSceneInitated() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (PMPMapControllerCallback cb:
                callbacks) {
                    cb.onMapSceneInitated();
                }
            }
        });
    }

    //PMPAugmentedRealityCallback methods
    @Override
    public void onAwayFromRoute() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (PMPAugmentedRealityCallback cb:
                        arCallbacks) {
                    cb.onAwayFromRoute();
                }
            }
        });
    }

    @Override
    public void onReturnToRoute() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (PMPAugmentedRealityCallback cb:
                        arCallbacks) {
                    cb.onReturnToRoute();
                }
            }
        });
    }

    @Override
    public void onRequestShowFlightDetails(final String recordId, final String preferredIdentifier) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (PMPAugmentedRealityCallback cb:
                        arCallbacks) {
                    cb.onRequestShowFlightDetails(recordId, preferredIdentifier);
                }
            }
        });
    }
}
