package com.cherrypicks.pmpmap;

/**
 * Created by joeyyc on 11/4/2017.
 */

public interface PMPAugmentedRealityCallback {
    public void onAwayFromRoute();
    public void onReturnToRoute();
    public void onRequestShowFlightDetails(String recordId, String preferredIdentifier);
}
