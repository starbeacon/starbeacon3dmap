package com.cherrypicks.pmpmap.utils;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Alan on 16/2/16.
 */
public class JavaHelper {



    private static final Set<Class<?>> WRAPPER_TYPES = getWrapperTypes();



    public static boolean isWrapperType(Class<?> clazz)
    {
        return WRAPPER_TYPES.contains(clazz);
    }

    public static Class toPrimitiveType(Class<?> clazz){
        if(clazz == Boolean.class){
            return Boolean.TYPE;
        }else if(clazz == Character.class){
            return Character.TYPE;
        }else if(clazz == Byte.class){
            return Byte.TYPE;
        }else if(clazz == Short.class){
            return Short.TYPE;
        }else if(clazz == Integer.class){
            return Integer.TYPE;
        }else if(clazz == Long.class){
            return Long.TYPE;
        }else if(clazz == Float.class){
            return Float.TYPE;
        }else if(clazz == Double.class){
            return Double.TYPE;
        }else if(clazz == Void.class){
            return Void.TYPE;
        }
        return clazz;
    }

    private static Set<Class<?>> getWrapperTypes()
    {
        Set<Class<?>> ret = new HashSet<Class<?>>();
        ret.add(Boolean.class);
        ret.add(Character.class);
        ret.add(Byte.class);
        ret.add(Short.class);
        ret.add(Integer.class);
        ret.add(Long.class);
        ret.add(Float.class);
        ret.add(Double.class);
        ret.add(Void.class);
        return ret;
    }

}
