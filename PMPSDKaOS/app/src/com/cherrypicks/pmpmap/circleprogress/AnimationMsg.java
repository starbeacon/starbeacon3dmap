package com.cherrypicks.pmpmap.circleprogress;

enum AnimationMsg {

   START_SPINNING,
   STOP_SPINNING,
   SET_VALUE,
   SET_VALUE_ANIMATED,
   TICK

}
