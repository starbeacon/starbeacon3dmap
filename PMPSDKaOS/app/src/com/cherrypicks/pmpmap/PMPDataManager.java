package com.cherrypicks.pmpmap;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.cms.PMPServerManagerNotifier;
import com.pmp.mapsdk.cms.model.Languages;
import com.pmp.mapsdk.cms.model.ResponseData;
import com.pmp.mapsdk.utils.PMPUtil;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Alan on 16/2/16.
 */
public class PMPDataManager {
    private static final String TAG = "PMPDataManager";

    private static PMPDataManager instance;
    private DownloadDataCallback callback;

    private Context context;

    private ResponseData responseData;
    private boolean isDataDownloadFinish = false;

    public interface DownloadDataCallback {
        void didSuccess(ResponseData responseData, boolean isCached);
        void didFailure();
    }

    public static PMPDataManager getSharedPMPManager(Context mContext){
        if(instance == null){
            instance = new PMPDataManager();
            instance.context = mContext;
        }

        return instance;
    }

    public ResponseData getResponseData() {
        return responseData;
    }

    public void setCallback(DownloadDataCallback callback) {
        this.callback = callback;
    }

    public boolean isDataDownloadFinish() {
        return isDataDownloadFinish;
    }

    public void downloadData(DownloadDataCallback downloadDataCallback){
        setCallback(downloadDataCallback);

//        SharedPreferences sharedPreferences = context.getSharedPreferences("CACHE_DATA" , MODE_PRIVATE);
//        boolean isCached = sharedPreferences.getBoolean("IS_CACHED",false);
//
//        if(!isCached){
//            Log.d(TAG, "downloadData: dicky !isCached");
//            PMPServerManager.getShared(context).fetchCacheJson(40, new PMPServerManagerNotifier() {
//                @Override
//                public void didSuccess(ResponseData response) {
//                    isDataDownloadFinish = true;
//                    responseData = response;
//                    if (callback != null) {
//
//                        SharedPreferences sharedPreferences = context.getSharedPreferences("CACHE_DATA" , MODE_PRIVATE);
//                        sharedPreferences.edit().putBoolean("IS_CACHED",true).apply();
//
//                        //call this after try download map online
////                        callback.didSuccess(responseData, false);
//
//                        PMPServerManager.getShared(context).downloadMap(40, new PMPServerManagerNotifier() {
//                            @Override
//                            public void didSuccess(ResponseData response) {
//                                isDataDownloadFinish = true;
//                                responseData = response;
//                                if (callback != null) {
//                                    callback.didSuccess(responseData, false);
//                                }
//                            }
//
//                            @Override
//                            public void didFailure() {
////                                callback.didFailure();
//                                //download fail , just use cache data
//                                callback.didSuccess(responseData, false);
//                            }
//                        });
//                    }
//                }
//
//                @Override
//                public void didFailure() {
//                    callback.didFailure();
//                }
//            });
//
//        }else {
            PMPServerManager.getShared(context).downloadMap(56, new PMPServerManagerNotifier() {
                @Override
                public void didSuccess(ResponseData response) {
                    Log.i(TAG, "Data Download finish...");
                    isDataDownloadFinish = true;
                    responseData = response;
                    if (callback != null) {
                        callback.didSuccess(responseData, false);
                    }
                }

                @Override
                public void didFailure() {

                    Log.i(TAG, "Data Download failed...");
                    callback.didFailure();
                }
            });
        }
//   }

    public native void nativeParseJson(String jsonStr, boolean isCache);
    public native void nativeFindNearVertexId(int mapId, float x, float y);

    public static int getLanguageIDByCode(String code) {
        int id = 0;
        if (instance == null) return id;

        if (instance.responseData != null) {
            ArrayList<Languages> langs = instance.responseData.getLanguages();
            if (langs != null) {
                for (Languages lang : langs) {
                    if (lang.getCode().equalsIgnoreCase(code)) {
                        id = (int)lang.getId();
                        break;
                    }
                }
            }
        }
        return id;
    }
}
