/****************************************************************************
Copyright (c) 2015 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package com.cherrypicks.pmpmap.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.external.PMPMapSDK;

public class SearchOverView extends FrameLayout {
    public static int OVERVIEW_BOTTOM_HOLDER_HEIGHT = 135;
    public static int OVERVIEW_TOP_HOLDER_HEIGHT = 145;
    private View rootView;

//    private LinearLayout ll_nav_top_bar;
//    private RelativeLayout rl_arrived_msg, rl_proximity_msg;
//    private ImageView iv_nav_icon;
    private TextView tv_dst_location;
    private Button btn_start, btn_left, btn_right;
    private NavigationListener mNavigationListener;
    private View v_top_holder, v_bottom_holder;
    private TextView tv_route_name, tv_time, tv_route_distance;
    private View rl_route_selection_holder;
    private ErrorView errorView;

    private int totalNumberOfResult;
    private int currentPathIndex = 0;
    private ViewStateEvent viewStatEvent;

//    private ProximityClickListener proximityClickListener;

    public interface NavigationListener {
        void onCloseNavBtnClicked();
        void onStartNavBtnClicked();
        void onPathOptionIndexChanged(int index, SearchOverView view);
    }

    public interface CloseNavigationListener {
        void onProximityBannerClicked();
        void onProximityBannerCancel();
    }

    public SearchOverView(Context context) {
        super(context);
        initUI();
    }

    public SearchOverView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUI();
    }

    public SearchOverView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initUI();
    }

    public ViewStateEvent getViewStatEvent() {
        return viewStatEvent;
    }

    public void setViewStatEvent(ViewStateEvent viewStatEvent) {
        this.viewStatEvent = viewStatEvent;
    }

    public void setTotalNumberOfResult(int totalNumberOfResult) {
        this.totalNumberOfResult = totalNumberOfResult;
        controlLeftRightButton();
    }

    public void setRouteName(String name){
        tv_route_name.setText(name);
    }

    public void setRouteDistance(String distance){
        tv_route_distance.setText(distance);
    }

    public void setTime(String time){
        tv_time.setText(time);
    }

    public void setDestName(String name) {
        tv_dst_location.setText(name.toUpperCase());
    }

    public void setCurrentPathIndex(int currentPathIndex) {
        this.currentPathIndex = currentPathIndex;
    }

    public int getCurrentPathIndex() {
        return currentPathIndex;
    }

    private void initUI(){
        rootView = LayoutInflater.from(getContext()).inflate(R.layout.pmp_search_overview, this);


        v_top_holder = rootView.findViewById(R.id.rl_top_holder);
        v_top_holder.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());

        v_bottom_holder = rootView.findViewById(R.id.rl_bottom_holder);
        tv_dst_location = (TextView) rootView.findViewById(R.id.tv_dst_location);
        rl_route_selection_holder = rootView.findViewById(R.id.rl_route_selection_holder);
        errorView = (ErrorView)rootView.findViewById(R.id.error_view);

        //prevent touch...
        v_top_holder.setOnTouchListener(new OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        v_bottom_holder.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        Button btn_start = (Button)rootView.findViewById(R.id.btn_start);
        btn_start.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mNavigationListener != null){
                    mNavigationListener.onStartNavBtnClicked();
                }
            }
        });

        Button btn_close = (Button)rootView.findViewById(R.id.btn_close);
        btn_close.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mNavigationListener != null){
                    mNavigationListener.onCloseNavBtnClicked();
                }
            }
        });

        btn_left = (Button)rootView.findViewById(R.id.btn_left);
        btn_left.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentPathIndex > 0){
                    currentPathIndex--;
                }

                controlLeftRightButton();

                if(mNavigationListener != null){
                    mNavigationListener.onPathOptionIndexChanged(currentPathIndex, SearchOverView.this);
                }
            }
        });

        btn_right = (Button)rootView.findViewById(R.id.btn_right);
        btn_right.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentPathIndex < totalNumberOfResult){
                    currentPathIndex++;
                }

                controlLeftRightButton();

                if(mNavigationListener != null){
                    mNavigationListener.onPathOptionIndexChanged(currentPathIndex, SearchOverView.this);
                }
            }
        });

        tv_route_name = (TextView)rootView.findViewById(R.id.tv_route_name);

        tv_time = (TextView)rootView.findViewById(R.id.tv_time);

        tv_route_distance = (TextView)rootView.findViewById(R.id.tv_route_distance);


    }

    private void controlLeftRightButton() {
        btn_right.setVisibility(View.INVISIBLE);
        btn_left.setVisibility(View.INVISIBLE);

        if(currentPathIndex < totalNumberOfResult - 1){
            btn_right.setVisibility(View.VISIBLE);
        }

        if(currentPathIndex > 0){
            btn_left.setVisibility(View.VISIBLE);
        }

        setRouteName(String.format(getResources().getString(R.string.PMPMAP_ROUTE_N), "" + currentPathIndex + 1));
    }

    public void forecPerformCloseNavigation() {
        if(this.mNavigationListener != null){
            this.mNavigationListener.onCloseNavBtnClicked();
        }
    }

    public int getTopHolderHeight() {
        return v_top_holder.getHeight();
    }

    public int getBottomHolderHeight() {
        return v_bottom_holder.getHeight();
    }

    public void setNavigationListener(NavigationListener navigationListener) {
        this.mNavigationListener = navigationListener;
    }

    public void setShowRouteSelection(boolean show) {
        if (show) {
            rl_route_selection_holder.setVisibility(VISIBLE);
        }else {
            rl_route_selection_holder.setVisibility(GONE);
        }
    }

    public void setErrorType(int errorType, ErrorView.RetryCallback retryCallback) {
        errorView.setVisibility(VISIBLE);
        errorView.setErrorType(errorType);
        errorView.setRetryCallback(retryCallback);
        v_bottom_holder.setVisibility(GONE);
    }

    public void hideError() {
        errorView.setVisibility(GONE);
        v_bottom_holder.setVisibility(VISIBLE);
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (viewStatEvent != null) {
            viewStatEvent.onVisibilityChanged(visibility);
        }
    }
}
