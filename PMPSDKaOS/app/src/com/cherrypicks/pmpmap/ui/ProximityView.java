/****************************************************************************
Copyright (c) 2015 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package com.cherrypicks.pmpmap.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmap.PMPDataManager;
import com.cherrypicks.pmpmap.core.CoreEngine;
import com.cherrypicks.pmpmap.ui.gesture.OnSwipeTouchListener;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.cms.model.Pois;
import com.pmp.mapsdk.cms.model.PromotionMessage;
import com.pmp.mapsdk.cms.model.Promotions;
import com.pmp.mapsdk.external.PMPNotification;
import com.pmp.mapsdk.utils.PMPUtil;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.pmp.mapsdk.cms.model.Promotions.PMPPromotionMessageType_AELnotOperating;
import static com.pmp.mapsdk.cms.model.Promotions.PMPPromotionMessageType_LastAELNotification;
import static com.pmp.mapsdk.cms.model.Promotions.PMPPromotionMessageType_WelcomeMesaage;
import static com.pmp.mapsdk.cms.model.Promotions.PMPPromotionMessageType_WelcomeMesaageSeaToAir;
import static com.pmp.mapsdk.cms.model.Promotions.PMPPromotionMessageType_WelcomeMessageArrival;
import static com.pmp.mapsdk.cms.model.Promotions.PMPPromotionMessageType_WelcomeMessageTransfer;

public class ProximityView extends FrameLayout{
    private final static String TAG = "ProximityView";
    private View rootView;

    private ImageView iv_image;
    private TextView tv_msg, tv_poi_name;
    private Object promotion;
    private int majorID;
    private List promotions = Collections.synchronizedList(new ArrayList());
    private Timer loopTimer;
    private boolean showExploreAround = false;

    private ProximityListener listener;
    private LinearLayout content;

    public boolean isShowExploreAround() {
        return showExploreAround;
    }

    public interface ProximityListener {
        void onProximityBannerClicked();
        void onProximityBannerCancel();
        void onExploreAroundClicked();
    }

    public ProximityView(Context context) {
        super(context);
        initUI();
    }

    public ProximityView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUI();
    }

    public ProximityView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initUI();
    }

    private void initUI(){
        rootView = LayoutInflater.from(getContext()).inflate(R.layout.pmp_proximity_view, this);

        content = (LinearLayout) findViewById(R.id.content);
        iv_image = (ImageView)rootView.findViewById(R.id.iv_image);
        tv_msg = (TextView)rootView.findViewById(R.id.tv_msg);
        tv_poi_name = (TextView)rootView.findViewById(R.id.tv_poi_name);

        Button btn_close = (Button)rootView.findViewById(R.id.btn_close);
        btn_close.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.onProximityBannerCancel();
                }
            }
        });

        rootView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    if (showExploreAround) {
                        listener.onExploreAroundClicked();
                        showExploreAround = false;
                        setVisibility(GONE);
                    } else {
                        listener.onProximityBannerClicked();
                        setVisibility(GONE);
                    }
                }
            }
        });
        rootView.setOnTouchListener(new OnSwipeTouchListener(getContext()) {
            @Override
            public void onSwipeTop() {
                super.onSwipeTop();
                onSwipe();
                Log.d(TAG, "onSwipeTop");

            }
        });
        this.setVisibility(GONE);
    }

    public void setMessage(String msg, boolean animated){
//        if (animated) {
//            tv_msg.animateText(msg);
//        } else {
            tv_msg.setText(msg);
//        }
    }

    public void setTitle(String msg, boolean animated){
//        if (animated) {
//            tv_poi_name.animateText(msg);
//        } else {
            tv_poi_name.setText(msg);
//        }
    }


    public void setProximityListener(ProximityListener listener) {
        this.listener = listener;
    }

    public int getMajorID() {
        return majorID;
    }

    public void setMajorID(int majorID) {
        this.majorID = majorID;
    }

    public void addPromotion(Object promotion) {
        int pushMessageTimeout = PMPDataManager.getSharedPMPManager(null).getResponseData().getPushMessageTimeout();
        int currentPromotionCount = promotions.size();
        final Promotion convertPromotion = Promotion.create(promotion);
        final Promotion existPromotion = (Promotion) CollectionUtils.find(promotions, new Predicate<Promotion>() {

            @Override
            public boolean evaluate(Promotion promo) {
                if (promo.type == convertPromotion.type) {
                    if (promo.type == PromotionType.Shop || promo.type == PromotionType.Message) {
                        Promotions p1 = (Promotions) promo.content;
                        Promotions p2 = (Promotions) convertPromotion.content;
                        if (p1.getId() == p2.getId()) {
                            return true;
                        }
                    } else if (promo.type == PromotionType.External) {
                        PMPNotification notif1 = (PMPNotification) promo.content;
                        PMPNotification notif2 = (PMPNotification) convertPromotion.content;
                        if ((notif1.getTitle() == null &&
                                notif1.getMessage() == notif2.getMessage() ||
                                notif1.getTitle().equals(notif2.getTitle())) &&
                                notif1.getMessage().equals(notif2.getMessage())) {
                            return true;
                        }
                    } else {
                        return true;
                    }
                }
                return false;
            }
        });

        if (existPromotion == null/* && !convertPromotion.isExpired()*/) {
            if (this.getVisibility() != VISIBLE) {
                updatePromotion(convertPromotion, false);
                this.setVisibility(VISIBLE);
            }

            promotions.add(convertPromotion);

            if (loopTimer == null && pushMessageTimeout != -1 ) {
                loopTimer = new Timer();
                loopTimer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        post(new Runnable() {
                            @Override
                            public void run() {
                                onSwipe();
                            }
                        });

                    }
                }, 0, pushMessageTimeout * 1000);
            }

        }
    }

    public void removePromotion(PromotionType type) {
        Iterator<Promotion> iterator = promotions.iterator();
        ArrayList removeList = new ArrayList();
        while (iterator.hasNext()) {
            Promotion promo = iterator.next();
            if (promo.type == type) {
                removeList.add(promo);
            }
        }
        promotions.removeAll(removeList);
    }

    public void removePromotionAndDismissIfNeeded(PromotionType type) {
        removePromotion(type);
        if (promotions.size() == 0) {
            this.setVisibility(GONE);
        }
    }

    public void showPromotionIfNeeded() {
        this.setVisibility(promotions.size() == 0 ? GONE : VISIBLE);
    }

    private void updatePromotion(Promotion promotion, boolean animated) {
        setTitle(getResources().getString(R.string.PMPMAP_ANNOUNCEMENT), animated);

        if (promotion.type == PromotionType.Shop) {
            this.promotion = promotion.content;
            showExploreAround = false;
            tv_poi_name.setVisibility(VISIBLE);
            tv_msg.setMaxLines(2);
            content.setGravity(Gravity.CENTER_VERTICAL);
            Promotions promotions = (Promotions) this.promotion;

//            for(PMPPois *poi in [PMPServerManager sharedInstance].serverResponse.pois){
//                if(poi.poisIdentifier == _currentPromotion.referenceId){
//                    if (animated) {
//                        [self addPushAnimation:self.titleLabel];
//                    }
//                    self.titleLabel.text = [poi.name localizedString];
//                    break;
//                }
//            }
            if(PMPServerManager.getShared(getContext()).getServerResponse().getPois() != null){
                for (Pois poi : PMPServerManager.getShared(getContext()).getServerResponse().getPois()){
                    if((int)poi.getId() == (int)promotions.getReferenceId()){
                        setTitle(PMPUtil.getLocalizedString(poi.getName()), animated);
                    }
                }
            }

            if(promotions.getMessage() != null && promotions.getMessage().size() > 0){
                setMessage(PMPUtil.getLocalizedString(promotions.getMessage()), animated);
            }

            if (animated) {
                addFadeAnimation(iv_image, R.drawable.icon_push_annoucement);
            } else {
                iv_image.setImageDrawable(ContextCompat.getDrawable(this.getContext(), R.drawable.icon_push_annoucement));
            }
        } else if (promotion.type == PromotionType.External) {
            this.promotion = promotion.content;
            showExploreAround = false;
            tv_poi_name.setVisibility(VISIBLE);
            tv_msg.setMaxLines(2);
            content.setGravity(Gravity.CENTER_VERTICAL);
            PMPNotification notification = (PMPNotification) promotion.content;
            setTitle(notification.getTitle(), true);
            setMessage(notification.getMessage(), true);
            if (animated) {
                addFadeAnimation(iv_image, R.drawable.icon_push_annoucement);
            } else {
                iv_image.setImageDrawable(ContextCompat.getDrawable(this.getContext(), R.drawable.icon_push_annoucement));
            }
        } else if (promotion.type == PromotionType.Message) {
            this.promotion = promotion.content;
            showExploreAround = false;
            tv_poi_name.setVisibility(VISIBLE);
            tv_msg.setMaxLines(2);
            content.setGravity(Gravity.CENTER_VERTICAL);
            Promotions promotions = (Promotions) this.promotion;
            if(promotions.getMessage() != null && promotions.getMessage().size() > 0){
                setMessage(PMPUtil.getLocalizedString(promotions.getMessage()), animated);
            }

            if(promotions.getMessageType() == PMPPromotionMessageType_WelcomeMesaage ||
                    promotions.getMessageType() == PMPPromotionMessageType_WelcomeMesaageSeaToAir ||
                    promotions.getMessageType() == PMPPromotionMessageType_WelcomeMessageTransfer ||
                    promotions.getMessageType() == PMPPromotionMessageType_WelcomeMessageArrival){
                setTitle(getResources().getString(R.string.PMPMAP_WELCOME), animated);
            }else if(promotions.getMessageType() == PMPPromotionMessageType_AELnotOperating ||
                    promotions.getMessageType() == PMPPromotionMessageType_LastAELNotification){
                setTitle(getResources().getString(R.string.PMPMAP_TRANSPORT_ALERT), animated);
            }

            if (animated) {
                addFadeAnimation(iv_image, R.drawable.icon_push_annoucement);
            } else {
                iv_image.setImageDrawable(ContextCompat.getDrawable(this.getContext(), R.drawable.icon_push_annoucement));
            }
        } else {
            this.promotion = null;
            showExploreAround = true;
            tv_poi_name.setVisibility(GONE);
            tv_msg.setMaxLines(3);
            content.setGravity(Gravity.CENTER_VERTICAL);
            setMessage((String) promotion.content, animated);

            if (animated) {
                addFadeAnimation(iv_image, R.drawable.icon_push_explore);
            } else {
                iv_image.setImageDrawable(ContextCompat.getDrawable(this.getContext(), R.drawable.icon_push_explore));
            }
        }
    }

    public void addFadeAnimation(final ImageView v, final int new_image) {
        final Context c = this.getContext();
        final Animation anim_out = AnimationUtils.loadAnimation(c, android.R.anim.fade_out);
        final Animation anim_in  = AnimationUtils.loadAnimation(c, android.R.anim.fade_in);
        anim_out.setAnimationListener(new Animation.AnimationListener()
        {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation)
            {
                Drawable drawable = null;
                if (new_image != 0) {
                    drawable = ContextCompat.getDrawable(c, new_image);
                }
                v.setImageDrawable(drawable);
                anim_in.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {}
                });
                v.startAnimation(anim_in);
            }
        });
        v.startAnimation(anim_out);
    }

    public Object getPromotion() {
        return promotion;
    }

    private void onSwipe() {
        if(promotions.size() > 0){
            Promotion promotion = (Promotion) promotions.get(0);
            updatePromotion(promotion, true);
            if(promotion.type != PromotionType.Gate || promotions.size() > 1){
                promotions.remove(0);
            }
            setVisibility(VISIBLE);
        } else {
            ProximityView.this.setVisibility(GONE);
            if (loopTimer != null) {
                loopTimer.cancel();
                loopTimer.purge();
                loopTimer = null;
            }
        }
    }

    static class Promotion extends Object {
        public Object content;
        public PromotionType type;
        static Promotion create(Object content) {
            Promotion message = new Promotion();
            message.content = content;
            message.type = getPromotionType(content);
            return message;
        }

        public boolean isExpired() {
            if (getPromotionType(content) == PromotionType.Shop) {
                Promotions promotions = (Promotions) content;
                int[] isOpeningFlag = new int[1];
                CoreEngine.getInstance().parseScheduleByPromotionID((int) promotions.getId(), isOpeningFlag);

                return isOpeningFlag[0] != 1;
            }
            return false;
        }
    }

    static PromotionType getPromotionType(Object content) {
        if (content instanceof Promotions) {
            Promotions promotion = (Promotions) content;
            if(promotion.getPromotionMessages()!= null && promotion.getPromotionMessages().size() > 0){
                PromotionMessage promotionMessage = promotion.getPromotionMessages().get(0);
                if(promotionMessage.getActionUrls() != null && promotionMessage.getActionUrls().size() > 0) {
                    if(PMPUtil.getLocalizedString(promotionMessage.getActionUrls()) != null &&
                            PMPUtil.getLocalizedString(promotionMessage.getActionUrls()).contains("hkgmyflight")) {
                        return PromotionType.Message;
                    }
                }
            }

            return PromotionType.Shop;
        }else if (content instanceof PMPNotification) {
            return PromotionType.External;
        }
        return PromotionType.Gate;
    }

}
