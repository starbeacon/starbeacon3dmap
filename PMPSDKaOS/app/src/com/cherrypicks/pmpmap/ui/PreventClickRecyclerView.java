package com.cherrypicks.pmpmap.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Alan on 29/12/16.
 */

public class PreventClickRecyclerView extends RecyclerView {
    public PreventClickRecyclerView(Context context) {
        super(context);
    }

    public PreventClickRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PreventClickRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
//        if (event.getAction() == MotionEvent.ACTION_DOWN && this.getScrollState() == RecyclerView.SCROLL_STATE_SETTLING) {
//            this.stopScroll();
//        }
        return false;
    }
}