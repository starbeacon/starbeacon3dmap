/****************************************************************************
Copyright (c) 2015 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package com.cherrypicks.pmpmap.ui;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmap.PMPMapConfig;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.cms.model.Maps;
import com.pmp.mapsdk.cms.model.PoiCategories;
import com.pmp.mapsdk.cms.model.Pois;
import com.pmp.mapsdk.utils.PMPUtil;

public class BottomHolderView extends FrameLayout{
    public static final int BottomHolderModeHidden = 0;
    public static final int BottomHolderModeHiddenWithOverView = 1;
    public static final int BottomHolderModeShowNavigation = 2;
    public static final int BottomHolderModeShowByPass = 3;
    public static final int BottomHolderModeShowPOI = 4;
    public static final int BottomHolderModeCount = 5;

    private final int DefaultBottomHolderHeightConstraint = 80;

    private final int ANIMATION_DURATION = 300;

    //UI reference from parent
    private Button btn_indoor, btn_locate_me;
    private boolean isOverViewModeEnable = false;
    private boolean indoorLocationDetected = false;

    //UI
    private View rootView;
    private RelativeLayout rl_poi_holder, rl_nav_holder, rl_bypass;
    private TextView tv_poi_name, tv_nav_time, tv_nav_distance, tv_dest_name, tv_location;
    private Button btn_start_nav;
    private ImageView iv_icon;

    private BottomHolderListener listener;

    //UI stage
    private int bottomHolderStage;

    //Data
    PMPMapConfig mConfig;
    private Pois poi;
    private float indoorBtnPreviousAnimationPos = 0;

    public boolean isOverViewModeEnable() {
        return isOverViewModeEnable;
    }

    public void setOverViewModeEnable(boolean overViewModeEnable) {
        isOverViewModeEnable = overViewModeEnable;
    }

    public boolean isIndoorLocationDetected() {
        return indoorLocationDetected;
    }

    public void setIndoorLocationDetected(boolean indoorLocationDetected) {
        this.indoorLocationDetected = indoorLocationDetected;
    }

    public interface BottomHolderListener {
        void onModeChanged(int mode);
        void onStartNavClicked(Pois poi);
        void onPOIDetailClicked(Pois poi);
    }

    public BottomHolderView(Context context) {
        super(context);
        initUI();
    }

    public BottomHolderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUI();
    }

    public BottomHolderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initUI();
    }

    private void initUI(){
        rootView = LayoutInflater.from(getContext()).inflate(R.layout.pmp_bottom_holder_view, this);
        //POI holder
        rl_poi_holder = (RelativeLayout) rootView.findViewById(R.id.rl_poi_holder);
        btn_start_nav = (Button) rootView.findViewById(R.id.btn_start_nav);
        tv_poi_name = (TextView)rootView.findViewById(R.id.tv_name);
        tv_location = (TextView)rootView.findViewById(R.id.tv_location);

        btn_start_nav.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.onStartNavClicked(poi);
                }
            }
        });

        rl_poi_holder.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.onPOIDetailClicked(poi);
                }
            }
        });

        //Nav holder
        rl_nav_holder = (RelativeLayout) rootView.findViewById(R.id.rl_nav_holder);
        tv_nav_time = (TextView) rootView.findViewById(R.id.tv_nav_time);
        tv_nav_distance = (TextView)rootView.findViewById(R.id.tv_nav_distance);
        tv_dest_name = (TextView)rootView.findViewById(R.id.tv_dest_name);

        //Bypass
        rl_bypass = (RelativeLayout) rootView.findViewById(R.id.rl_bypass);

        iv_icon = (ImageView) rootView.findViewById(R.id.iv_icon);
    }

    private void setIndoorBtnAnimation(float value){
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        value = value * metrics.density;

        ObjectAnimator moveY = ObjectAnimator.ofFloat(btn_indoor, "translationY", indoorBtnPreviousAnimationPos, value);
        moveY.setDuration(ANIMATION_DURATION);
        moveY.setRepeatCount(0);
//        am.setFillEnabled(true);
//        am.setFillAfter(true);
        moveY.setInterpolator(new LinearInterpolator());
        moveY.start();

        moveY = ObjectAnimator.ofFloat(btn_locate_me, "translationY", indoorBtnPreviousAnimationPos, value);
        moveY.setDuration(ANIMATION_DURATION);
        moveY.setRepeatCount(0);
//        am.setFillEnabled(true);
//        am.setFillAfter(true);
        moveY.setInterpolator(new LinearInterpolator());
        moveY.start();

        indoorBtnPreviousAnimationPos = value;
    }

    public void setIndoorButtonReference(Button button){
        btn_indoor = button;
    }

    public void setGoogleMapLocateMeButtonReference(Button button){
        btn_locate_me = button;
    }

    public void setPOI(Pois poi){
        this.poi = poi;
        PoiCategories cat = PMPUtil.getPOICategoryByPoi(getContext(), poi);
        if(cat != null)
            PMPUtil.setImageWithURL(iv_icon, cat.getImage());
        tv_poi_name.setText(PMPUtil.getLocalizedString(poi.getName()));

        Maps floor = PMPUtil.findFloorByPoi(getContext(), poi);
        if(floor != null){
            tv_location.setText(floor.getName());
        }

    }

    public void setDestinationName(String name){
        tv_dest_name.setText(name);
    }

    public void setNavTime(String time){
        tv_nav_time.setText(time);
    }

    public void setNavDistance(String distance){
        tv_nav_distance.setText(distance);
    }

    public void setDestinationIcon(String url){
        PMPUtil.setImageWithURL(iv_icon, url);
    }

    public void setPMPMapConfigReference(PMPMapConfig config){
        mConfig = config;
    }


    public void setBottomHolderListener(BottomHolderListener listener) {
        this.listener = listener;
    }

    public int getBottomHolderStage() {
        return bottomHolderStage;
    }

    public void showBottomHolderByMode(int mode){
        bottomHolderStage = mode;
        if(listener != null){
            listener.onModeChanged(bottomHolderStage);
        }

        if(mode == BottomHolderModeHidden){
            if(rootView.getVisibility() == View.GONE){
                mConfig.pmpSetFloorControlContentInset(0,0,0,0);
                mConfig.pmpSetLocateMeButtonContentInset(0,0);
                setIndoorBtnAnimation(0);
                return;
            }

            mConfig.pmpSetFloorControlContentInset(0,0,0,0);
            mConfig.pmpSetLocateMeButtonContentInset(0,0);
            setIndoorBtnAnimation(0);

            Animation am = new TranslateAnimation(
                    Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, 1.0f);
            am.setDuration(ANIMATION_DURATION);
            am.setRepeatCount(0);
            am.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    rootView.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            rootView.startAnimation(am);

            return;
        }else{
            //Play animation if needed
            int delay = 0;
            if(rootView.getVisibility() != View.GONE && mode != BottomHolderModeShowByPass){
                delay = 300;
                Animation am = new TranslateAnimation(
                        Animation.RELATIVE_TO_SELF, 0.0f,
                        Animation.RELATIVE_TO_SELF, 0.0f,
                        Animation.RELATIVE_TO_SELF, 0.0f,
                        Animation.RELATIVE_TO_SELF, 1.0f);
                am.setDuration(ANIMATION_DURATION);
                am.setFillAfter(true);
                am.setFillEnabled(true);
                am.setRepeatCount(0);
                rootView.startAnimation(am);
            }

            rl_poi_holder.setVisibility(View.GONE);
            rl_nav_holder.setVisibility(View.GONE);
            rl_bypass.setVisibility(View.GONE);


            switch (mode){
                case BottomHolderModeShowPOI:{
                    rl_poi_holder.setVisibility(View.VISIBLE);
                }
                break;
                case BottomHolderModeShowByPass:{
                    rl_nav_holder.setVisibility(View.VISIBLE);
                }
                break;
                case BottomHolderModeShowNavigation:{
                    rl_nav_holder.setVisibility(View.VISIBLE);
                }
                break;
                case BottomHolderModeHiddenWithOverView: {

                    break;
                }

                default:
                    break;
            }

            if(mode == BottomHolderModeHiddenWithOverView){
                mConfig.pmpSetFloorControlContentInset(0, isIndoorLocationDetected() ? SearchOverView.OVERVIEW_BOTTOM_HOLDER_HEIGHT + 10 : DefaultBottomHolderHeightConstraint, 0, 0);
                mConfig.pmpSetLocateMeButtonContentInset(0, isIndoorLocationDetected() ? SearchOverView.OVERVIEW_BOTTOM_HOLDER_HEIGHT + 10 : DefaultBottomHolderHeightConstraint);
                setIndoorBtnAnimation(isIndoorLocationDetected() ? -SearchOverView.OVERVIEW_BOTTOM_HOLDER_HEIGHT - 20 : -DefaultBottomHolderHeightConstraint);

            }else{
                mConfig.pmpSetFloorControlContentInset(0, DefaultBottomHolderHeightConstraint , 0, 0);
                mConfig.pmpSetLocateMeButtonContentInset(0, DefaultBottomHolderHeightConstraint);
                setIndoorBtnAnimation(-DefaultBottomHolderHeightConstraint);
            }

            new android.os.Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    rootView.setVisibility(View.VISIBLE);

                    Animation am = new TranslateAnimation(
                            Animation.RELATIVE_TO_SELF, 0.0f,
                            Animation.RELATIVE_TO_SELF, 0.0f,
                            Animation.RELATIVE_TO_SELF, 1.0f,
                            Animation.RELATIVE_TO_SELF, 0.0f);

                    am.setDuration(ANIMATION_DURATION);
                    am.setFillAfter(true);
                    am.setFillEnabled(true);
                    am.setRepeatCount(0);
                    rootView.startAnimation(am);
                }
            }, delay);
        }
    }
}
