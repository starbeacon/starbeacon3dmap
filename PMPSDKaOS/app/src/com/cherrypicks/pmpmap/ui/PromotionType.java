package com.cherrypicks.pmpmap.ui;

/**
 * Created by Yu on 24/2/2017.
 */

public enum PromotionType {
    Gate,
    Shop,
    External,
    Message
}
