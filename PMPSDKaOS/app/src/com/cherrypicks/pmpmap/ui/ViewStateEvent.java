package com.cherrypicks.pmpmap.ui;

/**
 * Created by Yu on 11/8/16.
 */
public interface ViewStateEvent {
    abstract void onVisibilityChanged(int visibility);
}
