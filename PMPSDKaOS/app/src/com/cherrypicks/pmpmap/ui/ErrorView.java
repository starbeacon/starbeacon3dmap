package com.cherrypicks.pmpmap.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmapsdk.R;

/**
 * Created by andrewman on 7/26/16.
 */
public class ErrorView extends RelativeLayout {
    public final static int ErrorType_NetworkConnection = 1;
    public final static int ErrorType_LocationServiceDisabled = 2;
    public final static int ErrorType_PathNotFound = 3;
    public final static int ErrorType_NoGPSSignal = 4;
    public final static int ErrorType_BluetoothDisabled = 5;

    public interface RetryCallback {
        public void onRetry();
    }

    private ImageView iv_icon;
    private TextView tv_title;
    private TextView tv_desc;
    private Button btn_retry;
    private int errorType = 0;
    private RetryCallback retryCallback;

    public ErrorView(Context context) {
        super(context);
        _init();
    }

    public ErrorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        _init();
    }

    public ErrorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        _init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ErrorView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        _init();
    }

    private void _init() {
        inflate(getContext(),R.layout.pmp_error_view, this);
        iv_icon = (ImageView)findViewById(R.id.iv_icon);
        tv_title = (TextView)findViewById(R.id.tv_title);
        tv_desc = (TextView)findViewById(R.id.tv_desc);
        btn_retry = (Button)findViewById(R.id.btn_retry);
        btn_retry.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (retryCallback != null) {
                    retryCallback.onRetry();
                }
            }
        });
    }

    public int getErrorType() {
        return errorType;
    }

    public void setErrorType(int errorType) {
        this.errorType = errorType;
        int iconRes = -1;
        int titleRes = -1;
        int descRes = -1;
        switch (errorType) {
            case ErrorType_NetworkConnection:
                iconRes = R.drawable.icon_no_signal;
                titleRes = R.string.PMPMAP_ERROR_NETWORK_CONNECTION_TITLE;
                descRes = R.string.PMPMAP_ERROR_NETWORK_CONNECTION_DESC;
                break;
            case ErrorType_LocationServiceDisabled:
                iconRes = R.drawable.icon_no_signal;
                titleRes = R.string.PMPMAP_ERROR_LOCATION_SERVICE_DISABLED_TITLE;
                descRes = R.string.PMPMAP_ERROR_LOCATION_SERVICE_DISABLED_DESC;
                break;
            case ErrorType_PathNotFound:
                iconRes = R.drawable.icon_no_signal;
                titleRes = R.string.PMPMAP_ERROR_PATH_NOT_FOUND_TITLE;
                descRes = R.string.PMPMAP_ERROR_PATH_NOT_FOUND_DESC;
                break;
            case ErrorType_NoGPSSignal:
                iconRes = R.drawable.icon_no_signal;
                titleRes = R.string.PMPMAP_ERROR_NO_GPS_SIGNAL_TITLE;
                descRes = R.string.PMPMAP_ERROR_NO_GPS_SIGNAL_DESC;
                break;
            case ErrorType_BluetoothDisabled:
                iconRes = R.drawable.icon_no_signal;
                titleRes = R.string.PMPMAP_ERROR_NETWORK_CONNECTION_TITLE;
                descRes = R.string.PMPMAP_NAV_PREVIEW_BLT_MSG;
                break;
            default:
                break;
        }

        iv_icon.setImageResource(iconRes);
        tv_title.setText(titleRes);
        tv_desc.setText(descRes);
    }

    public RetryCallback getRetryCallback() {
        return retryCallback;
    }

    public void setRetryCallback(RetryCallback retryCallback) {
        this.retryCallback = retryCallback;
    }
}
