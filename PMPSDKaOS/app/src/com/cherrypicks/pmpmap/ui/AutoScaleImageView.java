package com.cherrypicks.pmpmap.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;

public class AutoScaleImageView extends android.widget.ImageView {
	private static final boolean ISDEBUG = true;
	private static final String TAG = "ImageView";
	private static final String FIT_BACKGROUND = "fitBackground";
	boolean bnAdjustViewSize = true;
	boolean alignToBackground;

	public AutoScaleImageView(Context context) {
		this(context, null);
	}

	public AutoScaleImageView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public AutoScaleImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		alignToBackground = FIT_BACKGROUND.equals(String.valueOf(getTag()));
	}
	
	public boolean getAdjestViewSize() {
		return bnAdjustViewSize;
	}
	
	public void setAdjectViewSize(boolean bnAdjustViewSize) {
		this.bnAdjustViewSize = bnAdjustViewSize;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		if (!bnAdjustViewSize || getDrawable() == null) {
			//if (ISDEBUG && getDrawable() == null) Log.d(TAG, "ImageDrawable is null!");
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		} else {
			
			int widthSpecMode = MeasureSpec.getMode(widthMeasureSpec);
			int heightSpecMode = MeasureSpec.getMode(heightMeasureSpec);
			int widthSpecSize =  MeasureSpec.getSize(widthMeasureSpec);
			int heightSpecSize = MeasureSpec.getSize(heightMeasureSpec);
	
			boolean resizeWidth = widthSpecMode != MeasureSpec.EXACTLY;
			boolean resizeHeight = heightSpecMode != MeasureSpec.EXACTLY;
			
			int w, h;
			Drawable bg = getBackground();
			Drawable dr = getDrawable();
			
			if(bg==null || !alignToBackground) {
				w = dr.getMinimumWidth();
				h = dr.getMinimumHeight();
			}else {
				w = bg.getMinimumWidth();
				h = bg.getMinimumHeight();
			}
			
			if (w <= 0) w = 1;
			if (h <= 0) h = 1;

			float desiredAspect = (float) w / (float) h;
			
			boolean bnValidAdjust = false;

			if (desiredAspect != 0.0f) {
				if (resizeWidth && resizeHeight) {
					int intUseWH = 0; //0: W, 1: H
					int newWidth = heightSpecSize * h / w;
					int newHeight = widthSpecSize * h / w;
					
					if (newWidth > widthSpecSize && newHeight != 0) {
						intUseWH = 1;
					} else if (newHeight > heightSpecSize && newWidth != 0) {
						intUseWH = 0;
					}
					if (intUseWH == 0) {
						setMeasuredDimension(newWidth, heightSpecSize);
						bnValidAdjust = true;
					} else {
						setMeasuredDimension(widthSpecSize, newHeight);
						bnValidAdjust = true;
					}
				} else if (resizeHeight) {
					int newHeight = widthSpecSize * h / w;
					//if (_AbstractResourceTaker.ISDEBUG) Log.d(getClass().getName(), "Resizable Y Axis to " + widthSpecSize + "x" +  newHeight);
					setMeasuredDimension(widthSpecSize, newHeight);
					bnValidAdjust = true;
				} else if (resizeWidth) {
					int newWidth = heightSpecSize * h / w;
					//if (_AbstractResourceTaker.ISDEBUG) Log.d(getClass().getName(), "Resizable X Axis to " + newWidth + "x" +  heightSpecSize);
					setMeasuredDimension(newWidth, heightSpecSize);
					bnValidAdjust = true;
				}
			}
			if (!bnValidAdjust) {
				super.onMeasure(widthMeasureSpec, heightMeasureSpec);
			}
			
		}
	}
	
	protected void onDraw (Canvas canvas) {
		try {
			super.onDraw(canvas);
		} catch (Exception e) {
			if (ISDEBUG) Log.d(TAG, "onDraw error", e);
		}
	}
}
