/****************************************************************************
Copyright (c) 2015 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package com.cherrypicks.pmpmap.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmap.PMPMapConfig;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.cms.model.Pois;
import com.pmp.mapsdk.external.PMPMapSDK;

import java.io.InputStream;

public class NavTopView extends FrameLayout{
    private View rootView;

    private LinearLayout ll_nav_top_bar;
    private RelativeLayout rl_proximity_msg, rl_nav_holder;
    private ImageView iv_nav_icon;
    private TextView tv_nav_distance, tv_instruction, tv_dest_name, tv_distance;
    private Button btn_off_nav;
    private CloseNavigationListener closeNavigationListener;
    private ProximityClickListener proximityClickListener;
    private Pois poi;

    public interface CloseNavigationListener {
        void onCloseNavBtnClicked();
    }

    public interface ProximityClickListener {
        void onProximityBannerClicked();
        void onProximityBannerCancel();
    }

    public NavTopView(Context context) {
        super(context);
        initUI();
    }

    public NavTopView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUI();
    }

    public NavTopView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initUI();
    }



    private void initUI(){
        rootView = LayoutInflater.from(getContext()).inflate(R.layout.pmp_nav_top_view, this);

        rl_nav_holder = (RelativeLayout)rootView.findViewById(R.id.rl_nav_holder);
        rl_nav_holder.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());

        rl_proximity_msg = (RelativeLayout)rootView.findViewById(R.id.rl_proximity_msg);
        ll_nav_top_bar = (LinearLayout)rootView.findViewById(R.id.rl_nav_top_bar);
        iv_nav_icon = (ImageView)rootView.findViewById(R.id.iv_nav_icon);
        tv_nav_distance = (TextView)rootView.findViewById(R.id.tv_nav_distance);
        tv_instruction = (TextView)rootView.findViewById(R.id.tv_instruction);
        tv_dest_name = (TextView)rootView.findViewById(R.id.tv_dest_name);
        btn_off_nav = (Button)rootView.findViewById(R.id.btn_off_nav);
        tv_distance = (TextView)rootView.findViewById(R.id.tv_distance);
        Button btn_close = (Button)rootView.findViewById(R.id.btn_close);
        btn_close.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(proximityClickListener != null){
                    proximityClickListener.onProximityBannerCancel();
                }
            }
        });

        btn_off_nav.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(closeNavigationListener != null){
                    closeNavigationListener.onCloseNavBtnClicked();
                }
            }
        });

        rl_proximity_msg.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(proximityClickListener != null){
                    proximityClickListener.onProximityBannerClicked();
                }
            }
        });


        //prevent touch...
        rootView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    public void setPoi(Pois poi) {
        this.poi = poi;
    }

    public void setProximityClickListener(ProximityClickListener proximityClickListener) {
        this.proximityClickListener = proximityClickListener;
    }

    public void forcePerformCloseNavigation(){
        if(this.closeNavigationListener != null){
            this.closeNavigationListener.onCloseNavBtnClicked();
        }
    }

    public void setCloseNavigationListener(CloseNavigationListener listener) {
        this.closeNavigationListener = listener;
    }

    public void setProximityBannerVisibility(int visibility){
        rl_proximity_msg.setVisibility(visibility);
    }

    public int getProximityBannerVisibility(){
        return rl_proximity_msg.getVisibility();
    }

    public void resetStatus(){
        rl_nav_holder.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
    }

    public void setArrivedNotCoveredDst(){
        tv_instruction.setText("");
        rl_nav_holder.setBackgroundColor(getContext().getResources().getColor(R.color.nav_arrived_notcoverd_view_color));
    }

    public void setArrivedDst(){
        rl_nav_holder.setBackgroundColor(getContext().getResources().getColor(R.color.nav_arrived_view_color));
    }

    public void setBypassedDst(){
        rl_nav_holder.setBackgroundColor(getContext().getResources().getColor(R.color.nav_bypass_view_color));
    }

    public void setIconImage(Bitmap image){
        iv_nav_icon.setImageBitmap(image);
    }

    public void setStepMessageUpdate(final int type, final String message, final String poiMessage, final String iconImageName, float totalDuration, final float totalDistance) {
        post(new Runnable() {
            @Override
            public void run() {
                if (totalDistance == 0) {
                    tv_distance.setVisibility(GONE);
                }else {
                    tv_distance.setVisibility(VISIBLE);
                }
                tv_distance.setText(String.format(getResources().getString(R.string.PMPMAP_N_METER), String.format("%.0f", totalDistance)));
                if(poiMessage == null || poiMessage.length() == 0){
                    tv_instruction.setVisibility(View.GONE);
                    tv_dest_name.setGravity(Gravity.CENTER_VERTICAL);
                    tv_instruction.setText("");
                    tv_dest_name.setText(message);
                }else{
                    tv_instruction.setVisibility(View.VISIBLE);
                    tv_dest_name.setGravity(Gravity.TOP);
                    tv_instruction.setText(message);
                    tv_dest_name.setText(poiMessage);
                }
                iv_nav_icon.setImageResource(R.drawable.icon_nav_forward);
                try
                {
                    InputStream ims = getContext().getAssets().open(iconImageName + ".png");
                    Drawable d = Drawable.createFromStream(ims, null);
                    iv_nav_icon.setImageDrawable(d);
                }
                catch(Exception ex)
                {
//                        iv_nav_icon.setImageDrawable(d);
                }

                if(type == PMPMapConfig.NavigationStep_Arrived && poi != null){
                    tv_instruction.setTextColor(Color.BLACK);
                    tv_dest_name.setTextColor(Color.BLACK);

                }else{
                    tv_instruction.setTextColor(Color.WHITE);
                    tv_dest_name.setTextColor(Color.WHITE);
                }
            }
        });

    }

    public void setDistance(String distance){
        tv_nav_distance.setText("" + distance);
    }

}
