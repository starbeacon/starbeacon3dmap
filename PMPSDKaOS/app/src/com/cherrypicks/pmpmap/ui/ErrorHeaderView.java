package com.cherrypicks.pmpmap.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmapsdk.R;

/**
 * Created by andrewman on 8/4/16.
 */
public class ErrorHeaderView extends LinearLayout {
    public TextView errorLabel;
    public ErrorHeaderView(Context context) {
        super(context);
    }

    public ErrorHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ErrorHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ErrorHeaderView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        errorLabel = (TextView) findViewById(R.id.errorLabel);
    }
}
