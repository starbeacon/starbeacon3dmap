package com.cherrypicks.pmpmap.tts;

public interface AndroidTTSManagerBridge {
    public void onTTSMessageReceived (String msg, boolean disruptCurrentUtterance, boolean isStepMessage);
    public String getLanguage ();
}