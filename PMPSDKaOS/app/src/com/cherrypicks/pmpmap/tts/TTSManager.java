package com.cherrypicks.pmpmap.tts;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.accessibility.AccessibilityManager;

import com.cherrypicks.pmpmap.PMPMapController;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

/**
 * Created by gordonwong on 7/4/2017.
 */
public class TTSManager implements AndroidTTSManagerBridge {
    public enum TTSLanguage{
        TTSEnglish ,
        TTSCantonese,
        TTSMandarin,
        TTSJapanese,
        TTSKorean
    }
    public  boolean mute;
    public static final String googleTTSEnginePackageName = "com.google.android.tts";

    protected TextToSpeech ttsObj;
    final protected  Locale[] localeArray = { Locale.ENGLISH, new Locale("yue","HK"),Locale.TRADITIONAL_CHINESE,Locale.JAPANESE, Locale.KOREAN };
    final protected String[] languageNameArray = new String[]{"English","廣東話","普通話","日本語","한국어"};

    protected ArrayList<TTSLanguage> availableTTSLanguageList;
    protected  String lastSpokenStepMessage;
    protected  String lastSpokenPathMessage;
    protected TTSLanguage language;
    protected Context ttsContext;

    public boolean setLanguage(TTSLanguage language){
        this.language = language;
        return this.setLanguage(localeArray[language.ordinal()]);
    }

    protected boolean setLanguage(Locale locale){
        if(ttsObj == null){
            return false;
        }

        int result = ttsObj.setLanguage(locale);
        if(result != TextToSpeech.SUCCESS){
            return false;
        }
        return true;
    }

    public TTSLanguage getTTSLanguage(){
        return language;
    }

    public ArrayList<TTSLanguage> getAvailableTTSLanguageList(){
        return availableTTSLanguageList;
    }

    public void clearLastSpokenMessageRecord(){
        lastSpokenPathMessage = "";
        lastSpokenStepMessage = "";
    }

    public boolean speak(String text, boolean disruptCurrentUtterance, boolean checkLastSpokenString, boolean isStepMessage){
        return false;
//        if(ttsObj == null){
//            return false;
//        }
//        AccessibilityManager am = (AccessibilityManager) ttsContext.getSystemService(Context.ACCESSIBILITY_SERVICE);
//        if(am.isTouchExplorationEnabled()){
//            return false;
//        }
//
//
//        if(checkLastSpokenString){
//            if(isStepMessage &&  lastSpokenStepMessage.equals(text)){
//                return false;
//            }else if(!isStepMessage && lastSpokenPathMessage.equals(text)){
//                return false;
//            }
//
//            if(isStepMessage){
//                lastSpokenStepMessage = text;
//                lastSpokenPathMessage = "";
//            }else{
//                lastSpokenPathMessage = text;
//            }
//        }
//        int queueMode = TextToSpeech.QUEUE_ADD;
//        if(disruptCurrentUtterance){
//            queueMode = TextToSpeech.QUEUE_FLUSH;
//        };
//        Log.i("TTS", "speak: " + text + " disruptCurrentUtterance: " + (disruptCurrentUtterance?"true":"false"));
//        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
//            return ttsObj.speak(text,queueMode,null,null) == TextToSpeech.SUCCESS;
//        }else{
//            return ttsObj.speak(text,queueMode,null)  == TextToSpeech.SUCCESS;
//        }
    }

    public void shutDownTTSObject(){
        if(ttsObj != null) {
            ttsObj.stop();
            ttsObj.shutdown();
            ttsObj = null;
        }
    }

    public TTSManager(Activity activity, TextToSpeech.OnInitListener listener){
        ttsObj = new TextToSpeech(activity,listener,googleTTSEnginePackageName);
        ttsContext =  activity;
        lastSpokenStepMessage = "";
        lastSpokenPathMessage = "";
        availableTTSLanguageList = new ArrayList<TTSLanguage>();
        nativeInitial();
    }

    public void initiateAvailableTTSLanguageList(){
        availableTTSLanguageList = new ArrayList<TTSLanguage>();
        for(int i = 0; i < localeArray.length ; i++){
            int result = ttsObj.setLanguage(localeArray[i]);
            if(result == TextToSpeech.SUCCESS || result == TextToSpeech.LANG_COUNTRY_AVAILABLE){
               availableTTSLanguageList.add(TTSLanguage.values()[i]);
            }
        }

    }

    @Override
    public void onTTSMessageReceived(String msg, boolean disruptCurrentUtterance, boolean isStepMessage) {
        if(mute || PMPMapController.getInstance().isARViewShowing()){
            return;
        }
        this.speak(msg,disruptCurrentUtterance,true,isStepMessage);
    }

    public String getLanguage (){
        switch(language){
            case TTSEnglish:
                return "en";
            case TTSCantonese:
                return "zh-Hant";
            case TTSMandarin:
                return "zh-Hans";
            case TTSJapanese:
                return "ja";
            case TTSKorean:
                return "ko";
            default:
                return "en";
        }
    }

    public String getTTSLanguageName(TTSLanguage language){
        return languageNameArray[language.ordinal()];
    }

    private native void nativeInitial();

}
