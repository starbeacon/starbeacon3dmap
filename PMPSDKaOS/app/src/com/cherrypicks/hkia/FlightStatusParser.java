package com.cherrypicks.hkia;

import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;

import org.apache.commons.io.input.ReaderInputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by andrew on 19/1/2017.
 */

public class FlightStatusParser {
    private final static String TAG = "FlightStatusParser";
    // We don't use namespaces
    private static final String ns = null;
    private static final String FLIGHT_TAG_NAME = "WsfidFlight";
    private static final String ID_TAG_NAME  = "GlobalObjectIdentifier";
    private static final String GATE_TAG_NAME = "Gate";
    private static final String SCHEDULED_DATE_TIME_TAG_NAME = "ScheduledDateTime";

    public static List<FlightStatus> parse(String json) throws XmlPullParserException, IOException {
        Log.d(TAG, "JSON : " + json);
        List<FlightStatus> resutls = new ArrayList<>();
        XmlPullParser parser = Xml.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        StringReader stringReader = new StringReader(json);
        parser.setInput(stringReader);
        parser.nextTag();
        parser.nextTag();
        while (parser.nextTag() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            Log.d(TAG, "Name: " + name);
            // Starts by looking for the entry tag
            if (name.equals(FLIGHT_TAG_NAME)) {
                resutls.add(readWsfidFlight(parser));
            } else {
                skip(parser);
            }
        }
        Collections.sort(resutls);
        return resutls;

    }

    private static FlightStatus readWsfidFlight(XmlPullParser parser) throws XmlPullParserException, IOException {
        String id = null, gate = null, scheduledDateTimeStr = null;
        parser.require(XmlPullParser.START_TAG, ns,  FLIGHT_TAG_NAME);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals(ID_TAG_NAME)) {
                id = readId(parser);
            } else if (name.equals(GATE_TAG_NAME)) {
                gate = readGate(parser);
            } else if (name.equals(SCHEDULED_DATE_TIME_TAG_NAME)) {
                scheduledDateTimeStr = readScheduledDateTime(parser);
            } else {
                skip(parser);
            }
        }
        Date date = null;
        if (!TextUtils.isEmpty(scheduledDateTimeStr)) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            try {
                date = format.parse(scheduledDateTimeStr);
                System.out.println(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        Log.d(TAG, String.format("id: %s gate: %s scheduledDateTimeStr: %s", id, gate, scheduledDateTimeStr));
        return new FlightStatus(id, gate, date);
    }

    private static String readId(XmlPullParser parser) throws XmlPullParserException, IOException{
        parser.require(XmlPullParser.START_TAG, ns, ID_TAG_NAME);
        String id = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, ID_TAG_NAME);
        return id;
    }

    private static String readGate(XmlPullParser parser) throws XmlPullParserException, IOException{
        parser.require(XmlPullParser.START_TAG, ns, GATE_TAG_NAME);
        String id = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, GATE_TAG_NAME);
        return id;
    }

    private static String readScheduledDateTime(XmlPullParser parser) throws XmlPullParserException, IOException{
        parser.require(XmlPullParser.START_TAG, ns, SCHEDULED_DATE_TIME_TAG_NAME);
        String id = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, SCHEDULED_DATE_TIME_TAG_NAME);
        return id;
    }


    // For the tags title and summary, extracts their text values.
    private static String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private static void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}
