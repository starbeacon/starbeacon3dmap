package com.cherrypicks.hkia;

import java.util.Date;

/**
 * Created by andrew on 19/1/2017.
 */

public class FlightStatus implements Comparable<FlightStatus> {
    private String id;
    private String gate;
    private Date scheduledDate;

    public FlightStatus(String id, String gate, Date scheduledDate) {
        this.id = id;
        this.gate = gate;
        this.scheduledDate = scheduledDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGate() {
        return gate;
    }

    public void setGate(String gate) {
        this.gate = gate;
    }

    public Date getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(Date scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    @Override
    public int compareTo(FlightStatus another) {
        return this.scheduledDate.compareTo(another.scheduledDate);
    }
}
