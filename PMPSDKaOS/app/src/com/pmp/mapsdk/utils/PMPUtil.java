package com.pmp.mapsdk.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.cherrypicks.pmpmap.PMPDataManager;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.app.PMPMapFragment;
import com.pmp.mapsdk.cms.HttpsTrustManager;
import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.cms.model.Areas;
import com.pmp.mapsdk.cms.model.Devices;
import com.pmp.mapsdk.cms.model.Maps;
import com.pmp.mapsdk.cms.model.PoiCategories;
import com.pmp.mapsdk.cms.model.Pois;
import com.pmp.mapsdk.cms.model.Promotions;
import com.pmp.mapsdk.cms.model.ResponseData;
import com.pmp.mapsdk.external.PMPMapSDK;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.TimeZone;

public class PMPUtil {
    private static RequestQueue queue;
    private static HashMap<Integer, Promotions> promotionMap = new HashMap<>();
    private static SharedPreferences preferences;

    public static SharedPreferences getSharedPreferences(Context context) {
        if(preferences == null){
            preferences = context.getSharedPreferences("PMP", Context.MODE_PRIVATE);
        }

        return preferences;
    }

    public static String getLocalizedStringByLangID(ArrayList<?> contents, int langId) {
        if (contents == null)
            return "";
        int langID = 0;
        String defaultText = "";
        String langCode = "en";
        switch (langId) {
            case PMPMapSDK.Language_English:
                langCode = "en";
                break;
            case PMPMapSDK.Language_TraditionalChinese:
                langCode = "zh-hk";
                break;
            case PMPMapSDK.Language_SimplifiedChinese:
                langCode = "zh-cn";
                break;
            case PMPMapSDK.Language_Japanese:
                langCode = "jp";
                break;
            case PMPMapSDK.Language_Korean:
                langCode = "ko";
                break;
            default:
                break;
        }
        langID = PMPDataManager.getLanguageIDByCode(langCode);
        Boolean settedDefaultString = false;
        for(Object content : contents){
            Class cls = content.getClass();
            try {
                Method getLanguageIdMethod = cls.getMethod("getLanguageId");
                Method getContentMethod = cls.getMethod("getContent");
                Double id = (Double) getLanguageIdMethod.invoke(content);
                String contentStr = (String) getContentMethod.invoke(content);

                if (langID == id && contentStr != null && contentStr.length() > 0) {
                    defaultText = contentStr;
                    return contentStr;
                }else{
                    if(!settedDefaultString && contentStr != null) {
                        settedDefaultString = true;
                        defaultText = contentStr;
                    }
                }
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return defaultText;
    }

    public static String getLocalizedString(ArrayList<?> contents) {
        return getLocalizedStringByLangID(contents, PMPMapSDK.getLangID());
    }

    public static String getLocalizedAddress(Context context, Pois poi) {
        String areaStr = "";
        if ((int)poi.getAreaId() != 0) {
            for (Areas a : PMPServerManager.getShared().getServerResponse().getAreas()) {
                if ((int)poi.getAreaId() == (int)a.getId()) {
                    areaStr = getLocalizedString(a.getName());
                    break;
                }
            }
        }
        String restrictedAreaStr;
        if (poi.getRestricted()) {
//            restrictedAreaStr = context.getResources().getString(R.string.PMPMAP_SEARCH_OPTION_RESTRICTED_AREA);
            restrictedAreaStr = PMPMapFragment.getLocalizedStringWithSpecifiedLanguage("PMPMAP_SEARCH_OPTION_RESTRICTED_AREA",PMPMapSDK.getLangID());
        } else {
//            restrictedAreaStr = context.getResources().getString(R.string.PMPMAP_SEARCH_OPTION_NON_RESTRICTED_AREA);
            restrictedAreaStr = PMPMapFragment.getLocalizedStringWithSpecifiedLanguage("PMPMAP_SEARCH_OPTION_NON_RESTRICTED_AREA",PMPMapSDK.getLangID());
        }

        String locationString = "";
        if(poi.getLocation().size() > 0){
            String temp = getLocalizedString(poi.getLocation());
            if(temp != null && temp.length() > 0){
                locationString = temp;
            }
        }

        String addressFormat = context.getResources().getString(R.string.PMPMAP_POI_DETAIL_ADDRESS_FORMAT);
        return String.format(addressFormat, locationString, areaStr, restrictedAreaStr);//locationString + restrictedAreaStr + areaStr;
    }

    public static String getLocalizedTime(Context context, int second) {
        int minute = (int)Math.ceil(second/60.0);
        String str = "";

        if (second >= 0) {
            if (minute < 2) {
                str = String.format(context.getResources().getString(R.string.PMPMAP_N_MIN), "1");
            } else if (minute < 9999) {
                str = String.format(context.getResources().getString(R.string.PMPMAP_N_MINS), "" + minute);
            }
        }

        return str;
    }

    public static Maps findFloorByPoi(Context context, Pois poi){
        if(poi != null){
            for(Maps map : PMPDataManager.getSharedPMPManager(context).getResponseData().getMaps()){
                if(map.getId() == poi.getMapId()){
                    return map;
                }
            }
        }
        return null;
    }

    public static Pois findPOIByPoiID(Context context, int poiID){
        for(Pois poi : PMPDataManager.getSharedPMPManager(context).getResponseData().getPois()){
            if(poi.getId() == poiID){
                return poi;
            }
        }
        return null;
    }

    public static Promotions findPromotionByMajorID(Context context, int majorID){
        //build cache if needed
        if(promotionMap.size() == 0){
            ResponseData responseData = PMPDataManager.getSharedPMPManager(context).getResponseData();
            if (responseData.getDevices() != null && responseData.getPromotions() != null) {
                for (Devices beacons : responseData.getDevices()) {
                    double groupID = beacons.getDeviceGroupId();
                    for (Promotions promotion : responseData.getPromotions()) {
                        if (promotion.getDeviceGroupId() == groupID) {
                            promotionMap.put(majorID, promotion);
                            break;
                        }
                    }
                }
            }
        }

        //load cache...
        return promotionMap.get(majorID);
    }

    public static Promotions findPromotionByPOIID(Context context, int poiId){
        ResponseData responseData = PMPDataManager.getSharedPMPManager(context).getResponseData();
        if (responseData != null && responseData.getPromotions() != null) {
            for (Promotions promotion : responseData.getPromotions()) {
                for( Integer refId : promotion.getReferenceIds()){
                    if(refId.intValue() == poiId){
                        return promotion;
                    }
                }
            }
        }
        return null;
    }

    public static PoiCategories getPOICategoryByPoi(Context context, Pois poi){
        if(poi != null){
            ArrayList<PoiCategories> categories = PMPDataManager.getSharedPMPManager(context).getResponseData().getPoiCategories();
            for(PoiCategories cat : categories){
                for(Integer poiCatID : poi.getPoiCategoryIds()){
                    if((int)poiCatID == (int)cat.getId()){
                        return cat;
                    }
                }
            }
        }

        return null;
    }

    public static void setImageWithURL(final ImageView targetImage, String url) {
        setImageWithURL(targetImage, url, false);
    }

    public static void setImageWithURL(final ImageView targetImage, String url, final boolean animated) {
        if(targetImage == null || TextUtils.isEmpty(url) || Uri.parse(url).getHost() == null){
            return;
        }
        if (targetImage.getTag() instanceof ImageRequest) {
            queue.cancelAll(((ImageRequest)targetImage.getTag()).getTag());
        }
        Response.Listener<Bitmap> listener = new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap bitmap) {
                // use your bitmap
                if(targetImage.getContext() != null && targetImage != null){
                    if (animated) {
                        addFadeAnimation(targetImage, bitmap);
                    }
                    targetImage.setImageBitmap(bitmap);
                }
            }
        };
        HttpsTrustManager.allowAllSSL();
        ImageRequest request = new ImageRequest(
                url,
                listener,
                512,
                512,
                Bitmap.Config.ARGB_8888,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ImageError", "Error:" + error);
                    }
                });
        request.setTag(url);
        if(queue == null){
            queue = Volley.newRequestQueue(targetImage.getContext());
        }
        queue.add(request);
        targetImage.setTag(request);

    }

    public static void setImageWithURLForListView(final ImageView targetImage, String url){
        if(targetImage == null || TextUtils.isEmpty(url) || Uri.parse(url).getHost() == null){
            return;
        }
        if (targetImage.getTag() instanceof ImageRequest) {
            queue.cancelAll(""+targetImage);
        }
        Response.Listener<Bitmap> listener = new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap bitmap) {
                // use your bitmap
                if(targetImage.getContext() != null && targetImage != null){
                    targetImage.setImageBitmap(bitmap);
                }
            }
        };
        HttpsTrustManager.allowAllSSL();
        ImageRequest request = new ImageRequest(
                url,
                listener,
                512,
                512,
                Bitmap.Config.ARGB_8888,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ImageError", "Error:" + error);
                    }
                });
        request.setTag(""+targetImage);
        if(queue == null){
            queue = Volley.newRequestQueue(targetImage.getContext());
        }
        queue.add(request);
        targetImage.setTag(request);

    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static void addFadeAnimation(final ImageView v, final Bitmap new_image) {
        if (v != null && new_image != null) {
            final Context c = v.getContext();
            if (c != null) {
                final Animation anim_out = AnimationUtils.loadAnimation(c, android.R.anim.fade_out);
                final Animation anim_in = AnimationUtils.loadAnimation(c, android.R.anim.fade_in);
                anim_out.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        v.setImageBitmap(new_image);
                        anim_in.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                            }
                        });
                        v.startAnimation(anim_in);
                    }
                });
                v.startAnimation(anim_out);
            }
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * Return date in specified format.
     * @param milliSeconds Date in milliseconds
     * @param dateFormat Date format
     * @return String representing date in specified format
     */
    public static String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        TimeZone hkTimeZone = TimeZone.getTimeZone("Asia/Hong_Kong");
        Calendar calendar = Calendar.getInstance(hkTimeZone);
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }
}
