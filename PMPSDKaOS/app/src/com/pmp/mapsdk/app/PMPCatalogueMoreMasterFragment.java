package com.pmp.mapsdk.app;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.cherrypicks.pmpmap.PMPDataManager;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.cms.model.Areas;
import com.pmp.mapsdk.cms.model.Brands;
import com.pmp.mapsdk.cms.model.PoiCategories;
import com.pmp.mapsdk.cms.model.Pois;
import com.pmp.mapsdk.external.PMPMapSDK;
import com.pmp.mapsdk.utils.PMPUtil;

import java.util.ArrayList;

/**
 * Created by joeyyc on 1/12/2016.
 */

public class PMPCatalogueMoreMasterFragment extends Fragment {
    public static final String ARGS_BRANDCATEGORY = "ARGS_BRANDCATEGORY";
    public static final String ARGS_IS_LOADING_AREA = "ARGS_IS_LOADING_AREA";
    public static final String ARGS_SUBCATEGORIES = "ARGS_SUBCATEGORIES";
    public static final String ARGS_SELECTED_POI_CATEGORY = "ARGS_SELECTED_POI_CATEGORY";

    private ImageButton btnBack;
    private ImageButton btnSearch;
    private ImageButton btnBookmark;
    private TextView tvTitle;
    private ViewPager pager;
    private TabLayout tabLayout;

    private PoiCategories poiCategory;
    private ArrayList<PoiCategories> subCategories;
    private Boolean isLoadingArea;
    private ArrayList<ArrayList<Pois>> poiArray;
    private ArrayList<Areas> areas;
    private PoiCategories selectedPoiCategory;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = (View) inflater.inflate(R.layout.pmp_search_view_pager, container, false);
        View header = view.findViewById(R.id.header);
        header.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
        btnBack = (ImageButton) view.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        btnSearch = (ImageButton) view.findViewById(R.id.btnMidRight);
        btnSearch.setImageResource(R.drawable.icon_search);
        btnBookmark = (ImageButton) view.findViewById(R.id.btnMostRight);
        tvTitle = (TextView) view.findViewById(R.id.lblHeadTitle);

        pager = (ViewPager) view.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) view.findViewById(R.id.sliding_tabs);
        tabLayout.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());

        Bundle bundle = getArguments();
        if (bundle != null) {
            isLoadingArea = bundle.getBoolean(ARGS_IS_LOADING_AREA, true);
            poiCategory = (PoiCategories) bundle.getSerializable(ARGS_SUBCATEGORIES);
            if (poiCategory != null) {
                tvTitle.setText(PMPUtil.getLocalizedString(poiCategory.getName()));
            }
            subCategories = poiCategory.getSub_categories();
            if (subCategories == null) {
                subCategories = new ArrayList<>();
            }
            selectedPoiCategory = (PoiCategories) bundle.getSerializable(ARGS_SELECTED_POI_CATEGORY);

            generatePOIList();

            if (!isLoadingArea) {
                pager.setAdapter(new CatalogueFragmentPagerAdapter(getChildFragmentManager()));
            } else {
                pager.setAdapter(new LocationFragmentPagerAdapter(getChildFragmentManager()));
            }
            tabLayout.setupWithViewPager(pager);
            if (selectedPoiCategory != null) {
                int idx = subCategories.indexOf(selectedPoiCategory);
                pager.setCurrentItem(idx);
            }
        }

        return view;
    }

    private void generatePOIList() {
        final PMPServerManager serverManager = PMPServerManager.getShared(getActivity());

        if (serverManager.getServerResponse() == null) {
            return;
        }

        areas = serverManager.getServerResponse().getAreas();
        ArrayList<Pois> pois = serverManager.getServerResponse().getPois();
        ArrayList<Brands> brands = serverManager.getServerResponse().getBrands();
        ArrayList<PoiCategories> brandCategories = serverManager.getServerResponse().getPoiCategories();
        poiArray = new ArrayList<ArrayList<Pois>>();

        if (isLoadingArea) {
            for (Areas a : areas) {
                ArrayList<Pois> poiList = new ArrayList<Pois>();
                for (Pois p : pois) {
                    /*
                    loops:
                    if (p.getAreaId() == a.getId()) {
                        for (Brands b : brands) {
                            if (b.getId() == p.getBrandId()) {
                                for (int bCatId : b.getBrandCategoryIds()) {
                                    for (PoiCategories bCat : brandCategories) {
                                        if (bCatId == (int)bCat.getId()) {
                                            poiList.add(p);
                                            break loops;
                                        }
                                    }
                                }
                            }
                        }
                    }*/
                }
                poiArray.add(poiList);
            }
        } else {
            for (PoiCategories subCat : subCategories) {
                ArrayList<Pois> poiList = new ArrayList<Pois>();
                for (Pois p : pois) {
                    loop:
                    for (int catId : p.getPoiCategoryIds()) {
                        PoiCategories poiCat = PMPDataManager.getSharedPMPManager(null).getResponseData().getPoiCategoryMap().get(catId);
                        if (catId == subCat.getId()) {
                            poiList.add(p);
                            break loop;
                        }else {
                            while (poiCat.getParentPoiCategory() != null) {
                                poiCat = poiCat.getParentPoiCategory();
                                if (poiCat.getId() == subCat.getId()) {
                                    poiList.add(p);
                                    break loop;
                                }
                            }
                        }
                    }
                }
                poiArray.add(poiList);
            }
        }
    }

    public class LocationFragmentPagerAdapter extends FragmentStatePagerAdapter {

        public LocationFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return poiArray.size();
        }

        @Override
        public Fragment getItem(int position) {
            ArrayList<Pois> arr = poiArray.get(position);
            PMPCatalogueMoreSlaveFragment fragment = new PMPCatalogueMoreSlaveFragment();

            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(PMPCatalogueMoreSlaveFragment.ARGS_POI_ARRAY, arr);
            bundle.putBoolean(PMPCatalogueMoreSlaveFragment.ARGS_BY_AREA, true);
            fragment.setArguments(bundle);

            return fragment;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            Areas area = areas.get(position);

            return PMPUtil.getLocalizedString(area.getName());
        }
    }

    public class CatalogueFragmentPagerAdapter extends FragmentStatePagerAdapter {

        public CatalogueFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return subCategories.size();
        }

        @Override
        public Fragment getItem(int position) {
            ArrayList<Pois> arr = poiArray.get(position);
            PMPCatalogueMoreSlaveFragment fragment = new PMPCatalogueMoreSlaveFragment();

            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(PMPCatalogueMoreSlaveFragment.ARGS_POI_ARRAY, arr);
            bundle.putBoolean(PMPCatalogueMoreSlaveFragment.ARGS_BY_AREA, false);
            fragment.setArguments(bundle);

            return fragment;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            PoiCategories subCat = subCategories.get(position);

            return PMPUtil.getLocalizedString(subCat.getName());
        }
    }
}
