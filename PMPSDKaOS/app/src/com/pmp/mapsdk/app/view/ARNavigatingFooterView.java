package com.pmp.mapsdk.app.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmapsdk.R;

/**
 * Created by joeyyc on 25/5/2017.
 */

public class ARNavigatingFooterView extends RelativeLayout {
    private TextView destTitleTextView;
    private TextView currentLocTitleTextView;
    private TextView currentLocTextView;
    private TextView poiNameTextView;
    private TextView durationTextView;

    public ARNavigatingFooterView(Context context) {
        super(context);
    }

    public ARNavigatingFooterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public ARNavigatingFooterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ARNavigatingFooterView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        poiNameTextView = (TextView) findViewById(R.id.tv_poi_name);
        durationTextView = (TextView) findViewById(R.id.tv_duration);
        destTitleTextView = (TextView) findViewById(R.id.tv_dest_title);
        currentLocTitleTextView = (TextView) findViewById(R.id.tv_current_location_title);
        currentLocTextView = (TextView) findViewById(R.id.tv_current_location);
    }

    public void setPOIName(String poiName) {
        poiNameTextView.setText(poiName);
    }

    public void setDurationText(String text) {
        durationTextView.setText(text);
    }

    public void setDestTitle(String destTitle) {
        destTitleTextView.setText(destTitle);
    }

    public void setCurrentLocTitle(String currentLocTitle) {
        currentLocTitleTextView.setText(currentLocTitle);
    }

    public void setCurrentLoc(String currentLoc) {
        currentLocTextView.setText(currentLoc);
    }
}
