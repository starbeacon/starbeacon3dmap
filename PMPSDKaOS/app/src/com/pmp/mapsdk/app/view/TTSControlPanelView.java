package com.pmp.mapsdk.app.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmap.tts.TTSManager;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.external.PMPMapSDK;

import java.util.ArrayList;

/**
 * Created by gordonwong on 20/4/2017.
 */

public class TTSControlPanelView extends RelativeLayout{
    private  ImageButton enableTTSButton;
    private  ImageButton disableTTSButton;
    private ListView ttsLanguageListView;
    private TTSManager ttsManagerRef;

    public TTSControlPanelView(Context context) {
        super(context);
    }

    public TTSControlPanelView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TTSControlPanelView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TTSControlPanelView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        enableTTSButton = (ImageButton)findViewById(R.id.btn_tts_enable);
        disableTTSButton = (ImageButton)findViewById(R.id.btn_tts_disable);
        ttsLanguageListView = (ListView)findViewById(R.id.lv_tts_language);

        enableTTSButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ttsManagerRef != null) {
                    ttsManagerRef.mute = false;
                    enableTTSButton.setImageResource(R.drawable.icon_voice_enable_on);
                    disableTTSButton.setImageResource(R.drawable.icon_voice_disable_off);
                }
            }
        });

        disableTTSButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ttsManagerRef != null) {
                    ttsManagerRef.mute = true;
                    enableTTSButton.setImageResource(R.drawable.icon_voice_enable_off);
                    disableTTSButton.setImageResource(PMPMapSDK.getMapUISetting().getTtsButtonOnImageResource());
                }
            }
        });
    }

    public void initControlPanel(TTSManager m){
        ttsManagerRef = m;
        ttsManagerRef.initiateAvailableTTSLanguageList();

        switch (PMPMapSDK.getLangID()){
            case PMPMapSDK.Language_English:
                if(ttsManagerRef.getAvailableTTSLanguageList().contains(TTSManager.TTSLanguage.TTSEnglish)) {
                    ttsManagerRef.setLanguage(TTSManager.TTSLanguage.TTSEnglish);
                }else{
                    if(ttsManagerRef.getAvailableTTSLanguageList().size() > 0) {
                        ttsManagerRef.setLanguage(ttsManagerRef.getAvailableTTSLanguageList().get(0));
                    }
                }
                break;
            case PMPMapSDK.Language_TraditionalChinese:
                if(ttsManagerRef.getAvailableTTSLanguageList().contains(TTSManager.TTSLanguage.TTSCantonese)) {
                    ttsManagerRef.setLanguage(TTSManager.TTSLanguage.TTSCantonese);
                }else{
                    if(ttsManagerRef.getAvailableTTSLanguageList().size() > 0) {
                        ttsManagerRef.setLanguage(ttsManagerRef.getAvailableTTSLanguageList().get(0));
                    }
                }
                break;
            case PMPMapSDK.Language_SimplifiedChinese:
                if(ttsManagerRef.getAvailableTTSLanguageList().contains(TTSManager.TTSLanguage.TTSMandarin)) {
                    ttsManagerRef.setLanguage(TTSManager.TTSLanguage.TTSMandarin);
                }else{
                    if(ttsManagerRef.getAvailableTTSLanguageList().size() > 0) {
                        ttsManagerRef.setLanguage(ttsManagerRef.getAvailableTTSLanguageList().get(0));
                    }
                }
                break;
            case PMPMapSDK.Language_Japanese:
                if(ttsManagerRef.getAvailableTTSLanguageList().contains(TTSManager.TTSLanguage.TTSJapanese)) {
                    ttsManagerRef.setLanguage(TTSManager.TTSLanguage.TTSJapanese);
                }else{
                    if(ttsManagerRef.getAvailableTTSLanguageList().size() > 0) {
                        ttsManagerRef.setLanguage(ttsManagerRef.getAvailableTTSLanguageList().get(0));
                    }
                }
                break;
            case PMPMapSDK.Language_Korean:
                if(ttsManagerRef.getAvailableTTSLanguageList().contains(TTSManager.TTSLanguage.TTSKorean)) {
                    ttsManagerRef.setLanguage(TTSManager.TTSLanguage.TTSKorean);
                }else{
                    if(ttsManagerRef.getAvailableTTSLanguageList().size() > 0) {
                        ttsManagerRef.setLanguage(ttsManagerRef.getAvailableTTSLanguageList().get(0));
                    }
                }
                break;
            default:
                if(ttsManagerRef.getAvailableTTSLanguageList().size() > 0) {
                    ttsManagerRef.setLanguage(ttsManagerRef.getAvailableTTSLanguageList().get(0));
                }
                break;
        }
        TTSLangCellAdapter langDataAdapter =  new TTSLangCellAdapter(getContext(),ttsManagerRef.getAvailableTTSLanguageList()); //new ArrayAdapter<String>(getContext(), R.layout.pmp_cell_tts_language, R.id.pmp_cell_text_view_tts_language, languageList);
        ttsLanguageListView.setAdapter(langDataAdapter);

        ttsLanguageListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                if(ttsManagerRef != null) {
                    ttsManagerRef.clearLastSpokenMessageRecord();
                    ttsManagerRef.setLanguage(ttsManagerRef.getAvailableTTSLanguageList().get(position));
                    ttsManagerRef.mute = false;
                }
                enableTTSButton.setImageResource(R.drawable.icon_voice_enable_on);
                disableTTSButton.setImageResource(R.drawable.icon_voice_disable_off);
                ttsLanguageListView.invalidateViews();
                ttsLanguageListView.refreshDrawableState();
            }
        });
//                View listItem = ttsLanguageListView.getChildAt(0);
//            ((TextView)listItem.findViewById(R.id.pmp_cell_text_view_tts_language)).setTextColor(Color.parseColor("#0000ff"));
//            ((ImageView)listItem.findViewById(R.id.pmp_cell_image_view_selection_status)).setVisibility(VISIBLE);
    }

    // TextToSpeech.OnInitListener
//    @Override
//    public void onInit(int status) {
//        if (status == TextToSpeech.SUCCESS) {

//            switch (PMPMapSDK.getLangID()){
//                case PMPMapSDK.Language_English:
//                    ttsManagerRef.setLanguage(TTSManager.TTSLanguage.TTSEnglish);
//                    break;
//                case PMPMapSDK.Language_TraditionalChinese:
//                    ttsManagerRef.setLanguage(TTSManager.TTSLanguage.TTSCantonese);
//                    break;
//                case PMPMapSDK.Language_SimplifiedChinese:
//                    ttsManagerRef.setLanguage(TTSManager.TTSLanguage.TTSMandarin);
//                    break;
//                default:
//                    ttsManagerRef.setLanguage(TTSManager.TTSLanguage.TTSEnglish);
//                    break;
//            }
//            TTSLangCellAdapter langDataAdapter =  new TTSLangCellAdapter(getContext(),ttsManagerRef.getAvailableTTSLanguageList()); //new ArrayAdapter<String>(getContext(), R.layout.pmp_cell_tts_language, R.id.pmp_cell_text_view_tts_language, languageList);
//            ttsLanguageListView.setAdapter(langDataAdapter);
//
//            ttsLanguageListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
//
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view,
//                                        int position, long id) {
//
//                    if(ttsManagerRef != null) {
//                        ttsManagerRef.clearLastSpokenMessageRecord();
//                        ttsManagerRef.setLanguage(ttsManagerRef.getAvailableTTSLanguageList().get(position));
//                        ttsManagerRef.mute = false;
//                    }
//                    enableTTSButton.setImageResource(R.drawable.icon_voice_enable_on);
//                    disableTTSButton.setImageResource(R.drawable.icon_voice_disable_off);
//                    ttsLanguageListView.invalidateViews();
//                    ttsLanguageListView.refreshDrawableState();
//                }
//            });
//        } else {
//            Log.e("TTSManager", "Failed to init TTS manager:" + status);
//            ttsManagerRef.shutDownTTSObject();
//        }
//    }
    private class TTSLangCellAdapter extends BaseAdapter {
        private Context mContext;
        private LayoutInflater mInflater;
        private ArrayList<TTSManager.TTSLanguage> mDataSource;

        public TTSLangCellAdapter(Context context, ArrayList<TTSManager.TTSLanguage> items) {
            mContext = context;
            mDataSource = items;
            mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        //1
        @Override
        public int getCount() {
            return mDataSource.size();
        }

        //2
        @Override
        public Object getItem(int position) {
            return mDataSource.get(position);
        }

        //3
        @Override
        public long getItemId(int position) {
            return position;
        }

        //4
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get view for row item
            int selectedRowPosition = -1;
            for(int i = 0; i <mDataSource.size(); i++){
                if(mDataSource.get(i) == ttsManagerRef.getTTSLanguage()){
                    selectedRowPosition = i;
                    break;
                }
            }

            View rowView = mInflater.inflate(R.layout.pmp_cell_tts_language, parent, false);
            TextView title =  (TextView)rowView.findViewById(R.id.pmp_cell_tv_tts_language);
            title.setText(ttsManagerRef.getTTSLanguageName(mDataSource.get(position)));
            title.setTextColor(Color.parseColor( position == selectedRowPosition ? "#3d76c0" : "#a9a9a9"));
            ((ImageView)rowView.findViewById(R.id.pmp_cell_iv_tick)).setVisibility( position == selectedRowPosition? VISIBLE : INVISIBLE);
            return rowView;
        }
    }
}


