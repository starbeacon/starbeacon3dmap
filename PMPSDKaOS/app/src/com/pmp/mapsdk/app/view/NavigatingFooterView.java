package com.pmp.mapsdk.app.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.cherrypicks.pmpmapsdk.R;

import org.w3c.dom.Text;

/**
 * Created by andrew on 30/11/2016.
 */

public class NavigatingFooterView extends LinearLayout{
    private TextView poiNameTextView;
    private TextView durationTextView;

    public RelativeLayout layoutAROptions;
    public Button btnShowAROption;
    public Button btnEnableARNavigation;
    public TextView tvEnableARNav;
    public Switch switchARNav;

    public NavigatingFooterView(Context context) {
        super(context);
    }

    public NavigatingFooterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public NavigatingFooterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public NavigatingFooterView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        poiNameTextView = (TextView) findViewById(R.id.tv_poi_name);
        durationTextView = (TextView) findViewById(R.id.tv_duration);
        layoutAROptions = (RelativeLayout) findViewById(R.id.layout_AROptions);
        btnShowAROption = (Button) findViewById(R.id.btnShowAROption);
        btnEnableARNavigation = (Button) findViewById(R.id.btnEnableARNavigation);
        tvEnableARNav = (TextView) findViewById(R.id.tvEnableARNav);
        switchARNav = (Switch) findViewById(R.id.switchARNav);
    }

    public void setPOIName(String poiName) {
        poiNameTextView.setText(poiName);
    }

    public void setDurationText(String text) {
        durationTextView.setText(text);
    }
}
