package com.pmp.mapsdk.app.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmapsdk.R;

import java.io.InputStream;

/**
 * Created by andrew on 30/11/2016.
 */

public class NavigatingHeaderView extends RelativeLayout implements View.OnClickListener{
    private TextView distanceTextView;
    private ImageView iconImageView;
    private TextView instructionTextView;
    private ImageButton closeButton;
    private Runnable onCloseClickedListener;

    public NavigatingHeaderView(Context context) {
        super(context);
    }

    public NavigatingHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NavigatingHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public NavigatingHeaderView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        distanceTextView = (TextView) findViewById(R.id.tv_distance);
        iconImageView = (ImageView) findViewById(R.id.iv_icon);
        instructionTextView = (TextView) findViewById(R.id.tv_instruction);
        closeButton = (ImageButton) findViewById(R.id.btn_close);
        closeButton.setOnClickListener(this);
    }

    public void setDistance(float distance) {
        //Suggested by AA: currently estimated remaining distance is very confusing (42m under the direction arrow in screenshot) suggest to be remove it
        if (distance == 0 || true) {
            distanceTextView.setVisibility(GONE);
        }else {
            distanceTextView.setVisibility(VISIBLE);
            String str = String.format(getResources().getString(R.string.PMPMAP_N_METER), String.format("%.0f", distance));
            distanceTextView.setText(str);
        }

    }

    public void setIconImage(String iconImage) {
        try
        {
            InputStream ims = getContext().getAssets().open(iconImage + ".png");
            Drawable d = Drawable.createFromStream(ims, null);
            if (d instanceof BitmapDrawable) {
                BitmapDrawable bitmapDrawable = (BitmapDrawable) d;
                bitmapDrawable.setTargetDensity(getResources().getDisplayMetrics());
            }
            iconImageView.setImageDrawable(d);
        }
        catch(Exception ex)
        {

        }
    }

    public void setIconImage(int iconImage) {
        iconImageView.setImageResource(iconImage);
    }

    public void setInstruction(String instruction) {
        if (!instructionTextView.getText().equals(instruction)) {
            instructionTextView.setText(instruction);
        }
    }

    public void setInstruction(int id) {
        instructionTextView.setText(id);
    }

    public Runnable getOnCloseClickedListener() {
        return onCloseClickedListener;
    }

    public void setOnCloseClickedListener(Runnable onCloseClickedListener) {
        this.onCloseClickedListener = onCloseClickedListener;
    }

    public void clickCloseBtn(){
        this.closeButton.performClick();
    }
    @Override
    public void onClick(View v) {
        if (onCloseClickedListener != null) {
            onCloseClickedListener.run();
        }
    }
}
