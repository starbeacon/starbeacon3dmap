package com.pmp.mapsdk.app.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.external.PMPMapSDK;

/**
 * Created by andrew on 29/11/2016.
 */

public class FloorItemView extends RelativeLayout {
    public final static int Layout_ID = R.layout.pmp_cell_floor;
    private TextView titleTextView;
    private TextView indictorTextView;
    private boolean selectedStatus;

    public FloorItemView(Context context) {
        super(context);
    }

    public FloorItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FloorItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public FloorItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        titleTextView = (TextView)findViewById(R.id.tv_floor);
        indictorTextView = (TextView)findViewById(R.id.tv_indicator);
    }

    public void setTitle(String title) {
        titleTextView.setText(title);
    }

    public void setIndictorText(String indictorText) {
        if (TextUtils.isEmpty(indictorText)) {
            indictorTextView.setVisibility(GONE);
        }else {
            indictorTextView.setVisibility(VISIBLE);
            indictorTextView.setText(indictorText);
        }
    }

    public boolean isSelectedStatus() {
        return selectedStatus;
    }

    public void setSelectedStatus(boolean selected) {
        this.selectedStatus = selected;
        if (selectedStatus) {
            titleTextView.setTextColor(PMPMapSDK.getMapUISetting().getFloorNameTextColorOn());
            titleTextView.setBackgroundResource(PMPMapSDK.getMapUISetting().getFloorCellOnResource());
        }else {
            titleTextView.setTextColor(PMPMapSDK.getMapUISetting().getFloorNameTextColorOff());
            titleTextView.setBackgroundResource(PMPMapSDK.getMapUISetting().getFloorCellOffResource());
        }
    }
}
