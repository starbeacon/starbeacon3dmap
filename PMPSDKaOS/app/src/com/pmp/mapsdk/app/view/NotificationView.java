package com.pmp.mapsdk.app.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmapsdk.R;

/**
 * Created by andrew on 4/1/2017.
 */

public class NotificationView extends LinearLayout {
    public final static int NotificationTypeNone = 1;
    public final static int NotificationTypeImmigration = 2;
    public final static int NotificationTypePOIClose = 3;
    public final static int NotificationTypeSecurity = 4;
    private TextView warningView = null;
    private int notificationType;
    public NotificationView(Context context) {
        super(context);
    }

    public NotificationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NotificationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public NotificationView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        warningView = (TextView) findViewById(R.id.warning);
    }

    public int getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(int notificationType) {
        int visibility = View.GONE;
        this.notificationType = notificationType;
        warningView.setVisibility(GONE);
        switch (notificationType) {
            case NotificationTypeNone:
                break;
            case NotificationTypeImmigration:
                visibility = VISIBLE;
                warningView.setVisibility(VISIBLE);
                warningView.setText(getContext().getString(R.string.PMPMAP_NAV_PREVIEW_SECURIT_CHECK_MSG));
                break;
            case NotificationTypeSecurity:
                visibility = VISIBLE;
                warningView.setVisibility(VISIBLE);
                warningView.setText(getContext().getString(R.string.PMPMAP_NAV_PREVIEW_SECURITY_CHECK_MSG));
                break;
            case NotificationTypePOIClose:
                visibility = VISIBLE;
                warningView.setText(getContext().getString(R.string.PMPMAP_NAV_PREVIEW_POI_CLOSED_MSG));
                warningView.setVisibility(VISIBLE);
            default:
                break;
        }
        setVisibility(visibility);
    }
}
