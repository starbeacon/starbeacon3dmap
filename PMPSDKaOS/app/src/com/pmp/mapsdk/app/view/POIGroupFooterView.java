package com.pmp.mapsdk.app.view;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.cherrypicks.pmpmap.CocosFragment;
import com.cherrypicks.pmpmap.ui.RecyclerViewHeader;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.app.adapter.POIGroupAdapter;
import com.pmp.mapsdk.external.PMPMapSDK;

import static android.support.v7.widget.RecyclerView.SCROLL_STATE_IDLE;
import static android.support.v7.widget.RecyclerView.SCROLL_STATE_SETTLING;

/**
 * Created by andrew on 28/11/2016.
 */

public class POIGroupFooterView extends RelativeLayout {
    private final static String TAG = "POIGroupFooterView";
    private final static int LayoutId = R.layout.pmp_view_footer_poi_group;
    private final int scrollBuffer = 40;
    private RecyclerView recyclerView;
    private RecyclerViewHeader header;
    //private ListView listView;
    private Button expandButton;
    private boolean isExpaned = false;
    private POIGroupAdapter groupAdapter;
    private Runnable onExpandButtonClickedListener;
    private Runnable onCollapseEventListener;
    private RecyclerView.OnScrollListener onScrollListener;

    private CocosFragment cocosFragmentReference;
    private float defaultHeight;
    private boolean firstStart = false;
    private boolean Initialized = false;
    private int defaultListHeight = 0;

    public POIGroupFooterView(Context context) {
        super(context);
    }

    public POIGroupFooterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public POIGroupFooterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public POIGroupFooterView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setCocosFragmentReference(CocosFragment cocosFragmentReference) {
        this.cocosFragmentReference = cocosFragmentReference;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (recyclerView.getChildAt(0) != null) {
            if (firstStart && !Initialized && recyclerView.getChildAt(0).getTop() == header.getHeight() && header.getHeight()>0) {
                if (recyclerView.getChildAt(0).getTop() != header.getHeight() - defaultHeight) {
                    recyclerView.scrollBy(0, (int)defaultHeight);
                }
            }
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        setBackgroundColor(Color.TRANSPARENT);
        recyclerView = (RecyclerView) findViewById(R.id.rv_poi);

        expandButton = (Button) findViewById(R.id.btn_expand);
        recyclerView.setVisibility(VISIBLE);
        expandButton.setText(getLocalizedString("PMPMAP_POI_GROUP_SHOW_LIST"));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        expandButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onExpandButtonClickedListener != null) {
                    onExpandButtonClickedListener.run();
                }
            }
        });

        header = (RecyclerViewHeader)findViewById(R.id.header);
        header.setVisibility(VISIBLE);
        header.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Initialized = true;
                firstStart = false;

                MotionEvent.PointerProperties[] pointerProperties = new MotionEvent.PointerProperties[event.getPointerCount()];
                MotionEvent.PointerCoords[] pointerCoords = new MotionEvent.PointerCoords[event.getPointerCount()];

                for (int i=0; i<event.getPointerCount(); i++) {
                    pointerProperties[i] = new MotionEvent.PointerProperties();
                    pointerCoords[i] = new MotionEvent.PointerCoords();
                    event.getPointerProperties(i, pointerProperties[i]);
                    event.getPointerCoords(i, pointerCoords[i]);
                    pointerCoords[i].y = pointerCoords[i].y+POIGroupFooterView.this.getY()-recyclerView.getScrollY();

                }

                MotionEvent optimizedEvent = MotionEvent.obtain(event.getDownTime(),
                        event.getEventTime(),
                        event.getAction(),
                        event.getPointerCount(),
                        pointerProperties,
                        pointerCoords,
                        event.getMetaState(),
                        event.getButtonState(),
                        event.getXPrecision(),
                        event.getYPrecision(),
                        event.getDeviceId(),
                        event.getEdgeFlags(),
                        event.getSource(),
                        event.getFlags());

                cocosFragmentReference.getGLSurfaceView().onTouchEvent(optimizedEvent);
                return true;
            }
        });

        recyclerView.setOnSystemUiVisibilityChangeListener(new OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if (recyclerView.getChildAt(0) != null) {
                    if (firstStart && !Initialized && recyclerView.getChildAt(0).getTop() == header.getHeight() && header.getHeight()>0) {
                        if (recyclerView.getChildAt(0).getTop() != header.getHeight() - defaultHeight) {
                            recyclerView.scrollBy(0, (int)defaultHeight);
                        }

                    }
                }
            }
        });
        header.attachTo(recyclerView);

        recyclerView.setOnDragListener(new OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                Initialized = true;
                firstStart = false;
                return false;
            }
        });


        onScrollListener =

        new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == SCROLL_STATE_IDLE) {
                    if (recyclerView.getChildAt(0) != null) {
                        if (recyclerView.getChildAt(0).getTop() > 0) {
                            DisplayMetrics metrics = getResources().getDisplayMetrics();
                            if (recyclerView.getChildAt(0).getTop() < (header.getHeight() - defaultHeight) / 2) {
                                recyclerView.scrollBy(0, recyclerView.getChildAt(0).getTop());
                            } else if (recyclerView.getChildAt(0).getTop() >= (header.getHeight() - defaultHeight) / 2 &&
                                    recyclerView.getChildAt(0).getTop() < header.getHeight() - defaultHeight + 50 * metrics.density) {
                                recyclerView.scrollBy(0, recyclerView.getChildAt(0).getTop() - (header.getHeight() - (int) defaultHeight));
                            } else {
                                if (onCollapseEventListener != null) {
                                    onCollapseEventListener.run();
                                }
                            }
                        }
                    }
                }
            }
        };

        recyclerView.addOnScrollListener(onScrollListener);

        invalidate();
    }


    public boolean isExpaned() {
        return isExpaned;
    }

    public void setExpaned(boolean expaned) {
        boolean statusChanged = (isExpaned != expaned);
        isExpaned = expaned;
        if (isExpaned) {
            if (statusChanged) {
                header.setVisibility(VISIBLE);
                recyclerView.setVisibility(VISIBLE);
                expandButton.setAlpha(0);
                expandButton.setEnabled(false);
            }
            setClickable(true);
            if (Initialized && recyclerView.getChildAt(0)!=null)
                recyclerView.scrollBy(0, recyclerView.getChildAt(0).getTop() - (header.getHeight()-(int)(defaultHeight)));
        }else {
            Initialized = true;
            firstStart = false;
            setClickable(false);
            if (statusChanged) {
                header.setVisibility(GONE);
                recyclerView.setVisibility(GONE);
                expandButton.setAlpha(1);
                expandButton.setEnabled(true);
            }
        }
    }

    public POIGroupAdapter getGroupAdapter() {
        return groupAdapter;
    }

    public void setGroupAdapter(POIGroupAdapter groupAdapter, int listHeight) {
        firstStart = true;
        Initialized = false;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        defaultListHeight = (int)(groupAdapter.getPOISize() * 104*metrics.density);
        if (defaultListHeight < listHeight) {
            groupAdapter.footerHeight = listHeight - defaultListHeight;
        } else {
            groupAdapter.footerHeight = 0;
        }
        this.groupAdapter = groupAdapter;
        recyclerView.setAdapter(groupAdapter);

        if(listHeight > defaultListHeight){
            if (listHeight * 2 / 3 > defaultListHeight) {
                defaultHeight = defaultListHeight;
            } else {
                defaultHeight = listHeight * 2 / 3;
            }
        } else {
            defaultListHeight = listHeight;
            defaultHeight = defaultListHeight * 2 / 3;
        }
    }

    public Runnable getOnExpandButtonClickedListener() {
        return onExpandButtonClickedListener;
    }

    public void setOnExpandButtonClickedListener(Runnable onExpandButtonClickedListener) {
        this.onExpandButtonClickedListener = onExpandButtonClickedListener;
    }

    public Runnable getOnCollapseEventListener() {
        return onCollapseEventListener;
    }

    public void setOnCollapseEventListener(Runnable onCollapseEventListener) {
        this.onCollapseEventListener = onCollapseEventListener;
    }
    public static String getLocalizedString(String name){
        if(PMPMapSDK.getApplication() != null){
            int id = PMPMapSDK.getApplication().getResources().getIdentifier(name, "string", PMPMapSDK.getApplication().getPackageName());
            return PMPMapSDK.getApplication().getString(id);
        }
        return "";
    }

}
