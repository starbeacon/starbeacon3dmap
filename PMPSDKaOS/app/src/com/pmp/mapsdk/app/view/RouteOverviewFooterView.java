package com.pmp.mapsdk.app.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmapsdk.R;

/**
 * Created by andrew on 30/11/2016.
 */

public class RouteOverviewFooterView extends RelativeLayout{
    private TextView titleTextView;
    private TextView durationTextView;
    private TextView reminderView;
    private View stepHolderView;
    private ImageButton previousButton;
    private ImageButton nextButton;
    private Runnable onPreviousClickListener;
    private Runnable onNextClickListner;

    public RouteOverviewFooterView(Context context) {
        super(context);
    }

    public RouteOverviewFooterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RouteOverviewFooterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public RouteOverviewFooterView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        titleTextView = (TextView) findViewById(R.id.tv_title);
        durationTextView = (TextView) findViewById(R.id.tv_duration);
        reminderView = (TextView) findViewById(R.id.tv_reminder);
        stepHolderView = findViewById(R.id.ll_step_holder);
        previousButton = (ImageButton)findViewById(R.id.btn_previous);
        nextButton = (ImageButton)findViewById(R.id.btn_next);

        previousButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onPreviousClickListener != null) {
                    onPreviousClickListener.run();
                }
            }
        });

        nextButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onNextClickListner != null) {
                    onNextClickListner.run();
                }
            }
        });
        setShowStep(false);
    }

    public void setDuration(float duration) {
        if (duration < 0) {
            durationTextView.setText("");
            return;
        }

        float mins = (float)Math.ceil(duration / 60.0f);
        String str = String.format(getResources().getString(R.string.PMPMAP_N_MINS), String.format("%.0f", mins));

        if (mins <= 1) {
            mins = 1;
        }

        if(mins == 1){
            str = String.format(getResources().getString(R.string.PMPMAP_N_MIN), String.format("%.0f", mins));
        }

        if (reminderView.getText() != null && reminderView.getText().length()>0) {
            str = "*" + str;
        }
        durationTextView.setText(str);
    }

    public void setReminder(String reminder) {
        String str = reminder;
        if (str == null) {
            str = "";
        }

        reminderView.setText(str);
        if (str.length() > 0 &&
                (durationTextView.getText()!= null &&
                durationTextView.getText().length()>0 &&
                durationTextView.getText().charAt(0) != '*')) {
            durationTextView.setText("*" + durationTextView.getText());
        } else if (str.length()==0 &&
                        (durationTextView.getText()!= null &&
                        durationTextView.getText().length()>0 &&
                        durationTextView.getText().charAt(0) == '*')) {
            durationTextView.setText(durationTextView.getText().subSequence(1, durationTextView.getText().length()));
        }
    }

    public void setEnablePreviousButton( boolean enablePreviousButton) {
        previousButton.setEnabled(enablePreviousButton);
        if (enablePreviousButton) {
            previousButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.btn_step_left_on));
        } else {
            previousButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.btn_step_left_off));
        }
    }

    public void setEnableNextButton( boolean enableNextButton) {
        nextButton.setEnabled(enableNextButton);
        if (enableNextButton) {
            nextButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.btn_step_right_on));
        } else {
            nextButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.btn_step_right_off));
        }
    }

    public void setShowStep(boolean showStep) {
        if (showStep) {
            stepHolderView.setVisibility(VISIBLE);
        }else {
            stepHolderView.setVisibility(GONE);
        }
    }

    public Runnable getOnPreviousClickListener() {
        return onPreviousClickListener;
    }

    public void setOnPreviousClickListener(Runnable onPreviousClickListener) {
        this.onPreviousClickListener = onPreviousClickListener;
    }

    public Runnable getOnNextClickListner() {
        return onNextClickListner;
    }

    public void setOnNextClickListner(Runnable onNextClickListner) {
        this.onNextClickListner = onNextClickListner;
    }
}
