package com.pmp.mapsdk.app.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmapsdk.R;

/**
 * Created by andrew on 30/11/2016.
 */

public class POISelectionFooterView extends LinearLayout{
    private TextView poiNameTextView;
    private TextView addressTextView;

    public POISelectionFooterView(Context context) {
        super(context);
    }

    public POISelectionFooterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public POISelectionFooterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public POISelectionFooterView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        poiNameTextView = (TextView) findViewById(R.id.tv_poi_name);
        addressTextView = (TextView) findViewById(R.id.tv_address);
    }

    public void setPOIName(String poiName) {
        poiNameTextView.setText(poiName);
    }

    public void setAddress(String address) {
        addressTextView.setText(address);
    }
}
