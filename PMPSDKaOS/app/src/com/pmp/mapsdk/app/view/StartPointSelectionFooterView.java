package com.pmp.mapsdk.app.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmapsdk.R;

/**
 * Created by joeyyc on 2/2/2017.
 */

public class StartPointSelectionFooterView extends RelativeLayout {
    private TextView poiNameTextView;
    private TextView addressTextView;

    public StartPointSelectionFooterView(Context context) {
        super(context);
    }

    public StartPointSelectionFooterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StartPointSelectionFooterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public StartPointSelectionFooterView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        poiNameTextView = (TextView) findViewById(R.id.tv_title);
        addressTextView = (TextView) findViewById(R.id.tv_subtitle);
    }

    public void setPOIName(String poiName) {
        poiNameTextView.setText(poiName);
    }

    public void setAddress(String address) {
        addressTextView.setText(address);
    }
}
