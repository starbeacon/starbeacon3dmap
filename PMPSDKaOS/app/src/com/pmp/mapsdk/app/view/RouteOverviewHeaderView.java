package com.pmp.mapsdk.app.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SwitchCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmap.core.CoreEngine;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.external.PMPMapSDK;
import com.pmp.mapsdk.location.PMPApplication;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andrew on 30/11/2016.
 */

public class RouteOverviewHeaderView extends LinearLayout {
    private TextView titleTextView;
    private TextView fromTextView;
    private Button fromButton;
    private TextView toTextView;
    private Button toButton;
    private ImageButton walkModeButton;
    private ImageButton closeButton;
    private SwitchCompat disabilityModeSwitch;
    private Runnable onCloseClickedListener;
    private Runnable onChangeStartLocationListener;
    private Runnable onChangeDestinationListener;
    private Runnable onChangeDisabilityModeListener;
    private List<View> pathModeButtons = new ArrayList<View>();
    private OnClickListener pathModeClickListner = new OnClickListener() {
        @Override
        public void onClick(View v) {
            for (View btn : pathModeButtons) {
                btn.setSelected(false);
            }
            v.setSelected(true);
        }
    };

    public RouteOverviewHeaderView(Context context) {
        super(context);
    }

    public RouteOverviewHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RouteOverviewHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public RouteOverviewHeaderView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        titleTextView = (TextView) findViewById(R.id.tv_title);
        fromTextView = (TextView) findViewById(R.id.tv_from);
        fromButton = (Button) findViewById(R.id.btn_from);
        toTextView = (TextView) findViewById(R.id.tv_to);
        toButton = (Button) findViewById(R.id.btn_to);
        walkModeButton = (ImageButton) findViewById(R.id.btn_walk_mode);

        StateListDrawable states = new StateListDrawable();
        states.addState(new int[] {android.R.attr.state_pressed}, getResources().getDrawable(R.drawable.pmp_path_mode_background));
        states.addState(new int[] {android.R.attr.state_selected}, getResources().getDrawable(R.drawable.pmp_path_mode_background));
        states.addState(new int[] { }, new ColorDrawable(PMPMapSDK.getMapUISetting().getThemeColor()));
        walkModeButton.setBackground(states);
//        disabilityModeButton = (ImageButton) findViewById(R.id.btn_disablility_mode);
        closeButton = (ImageButton) findViewById(R.id.btn_close);
        disabilityModeSwitch = (SwitchCompat) findViewById(R.id.switch_disability_mode);
        if (PMPApplication.getApplication() != null){
            disabilityModeSwitch.setChecked(CoreEngine.getInstance().isDisability());
        }
        fromButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onChangeStartLocationListener != null) {
                    onChangeStartLocationListener.run();
                }
            }
        });
        toButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onChangeDestinationListener != null) {
                    onChangeDestinationListener.run();
                }
            }
        });
        closeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onCloseClickedListener != null) {
                    onCloseClickedListener.run();
                }
            }
        });
        disabilityModeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(onChangeDisabilityModeListener != null) {
                    onChangeDisabilityModeListener.run();
                }
            }
        });;

        pathModeButtons.add(walkModeButton);
//        pathModeButtons.add(disabilityModeButton);
        walkModeButton.setOnClickListener(pathModeClickListner);
//        disabilityModeButton.setOnClickListener(pathModeClickListner);
//        disabilityModeButton.setBackground(states);
        walkModeButton.setSelected(true);
        setToButtonEnabled(false);
    }

    public void setFromButtonEnabled(boolean enabled) {
        if (enabled) {
            this.fromButton.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.pmp_text_underline));
        } else {
            this.fromButton.setBackground(null);
        }
        this.fromButton.setEnabled(enabled);
    }

    public void setToButtonEnabled(boolean enabled) {
        if (enabled) {
            this.toButton.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.pmp_text_underline));
        } else {
            this.toButton.setBackground(null);
        }
        this.toButton.setEnabled(enabled);
    }

    public void setToLocationAlpha(float alpha) {
        toButton.setAlpha(alpha);
        setToButtonEnabled((alpha==1)?false:true);
    }

    public void setToLocation(String locName) {
        toButton.setText(locName);
    }

    public void setFromLocationAlpha(float alpha) {
        if (alpha == 1) {
            fromButton.setAlpha(1);
            fromButton.setTextColor(Color.parseColor("#FFFFFF"));
        } else if (alpha > 0) {
            fromButton.setAlpha(1);
            fromButton.setTextColor(Color.parseColor("#8aFFFFFF"));
        } else {
            fromButton.setAlpha(0);
        }
        setFromButtonEnabled((alpha==1)?false:true);
    }

    public void setFromLocation(String locName) {
        fromButton.setText(locName);
    }

    public Runnable getOnCloseClickedListener() {
        return onCloseClickedListener;
    }

    public void setOnCloseClickedListener(Runnable onCloseClickedListener) {
        this.onCloseClickedListener = onCloseClickedListener;
    }

    public Runnable getOnChangeStartLocationListener() {
        return onChangeStartLocationListener;
    }

    public void setOnChangeStartLocationListener(Runnable onChangeStartLocationListener) {
        this.onChangeStartLocationListener = onChangeStartLocationListener;
    }

    public Runnable getOnChangeDestinationListener() {
        return onChangeDestinationListener;
    }

    public void setOnChangeDestinationListener(Runnable onChangeDisabilityModeListener) {
        this.onChangeDestinationListener = onChangeDestinationListener;
    }

    public Runnable getOnChangeDisabilityModeListenerListener() {
        return onChangeDisabilityModeListener;
    }

    public void setOnChangeDisabilityModeListener(Runnable onChangeDisabilityModeListener) {
        this.onChangeDisabilityModeListener = onChangeDisabilityModeListener;
    }

    public void setDisabilityModeSwitchState(boolean checked) {
        this.disabilityModeSwitch.setChecked(checked);
    }
    public boolean getDisabilityModeSwitchState(){
        return  this.disabilityModeSwitch.isChecked();
    }
}
