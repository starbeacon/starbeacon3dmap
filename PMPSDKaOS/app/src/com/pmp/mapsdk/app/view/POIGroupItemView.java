package com.pmp.mapsdk.app.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.utils.PMPUtil;

/**
 * Created by andre on 11/30/2016.
 */

public class POIGroupItemView extends RelativeLayout {
    private ImageView thumbImageView;
    private TextView poiNameTextView;
    private TextView addressTextView;
    private TextView durationTextView;


    public POIGroupItemView(Context context) {
        super(context);
    }

    public POIGroupItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public POIGroupItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public POIGroupItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        thumbImageView = (ImageView) findViewById(R.id.iv_thumb);
        thumbImageView.setBackgroundColor(Color.WHITE);
        poiNameTextView = (TextView) findViewById(R.id.tv_poi_name);
        addressTextView = (TextView) findViewById(R.id.tv_address);
        durationTextView = (TextView) findViewById(R.id.tv_duration);
    }

    public void setImageURL(String url) {
        PMPUtil.setImageWithURL(thumbImageView, url);
    }
    public void setImagePadding(int left, int top, int right, int bottom) {
        thumbImageView.setPadding(left, top, right, bottom);
    }

    public void setPOIName(String poiName) {
        poiNameTextView.setText(poiName);
    }

    public void setAddress(String address) {
        addressTextView.setText(address);
    }

    public void setDuration(Context context, float duration) {
        String str = PMPUtil.getLocalizedTime(context, (int)duration);
        durationTextView.setText(str);
    }

}
