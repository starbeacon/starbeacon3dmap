package com.pmp.mapsdk.app;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.cherrypicks.pmpmap.PMPDataManager;
import com.cherrypicks.pmpmap.analytics.AnalyticsLogger;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.cms.PMPPOIByDuration;
import com.pmp.mapsdk.cms.model.Brands;
import com.pmp.mapsdk.cms.model.PoiCategories;
import com.pmp.mapsdk.cms.model.Pois;
import com.pmp.mapsdk.external.PMPMapSDK;
import com.pmp.mapsdk.utils.PMPUtil;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by joeyyc on 30/11/2016.
 */

public class PMPSearchListComponentFragment extends Fragment {
    public static final String ARGS_AROUNDMEPOIS = "ARGS_AROUNDMEPOIS";
    public static final String ARGS_AROUNDMESHOW_ICON = "ARGS_AROUNDMESHOW_ICON";

    private ListView listView;
    private ArrayList<PMPPOIByDuration> pois;
    private AroundListAdapter aroundListAdapter = new AroundListAdapter();
    private boolean shouldShowIcon = false;
    public boolean isComeFromGate = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            pois = bundle.getParcelableArrayList(ARGS_AROUNDMEPOIS);
            shouldShowIcon = bundle.getBoolean(ARGS_AROUNDMESHOW_ICON);
        } else {
            pois = new ArrayList<>();
        }

        View view = (View)inflater.inflate(R.layout.pmp_search_list_component, container, false);
        listView = (ListView) view.findViewById(R.id.listview);
        listView.setAdapter(aroundListAdapter);

        return view;
    }

    private class AroundListAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return pois.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_poi, null);
            }

            PMPPOIByDuration poi = pois.get(position);

            convertView.setTag(position);
            ImageView imageView = (ImageView)convertView.findViewById(R.id.iv_icon);
            imageView.setImageDrawable(null);

            if (shouldShowIcon) {
                PoiCategories cat = null;
                if (poi.poi.getPoiCategoryIds() != null &&
                        poi.poi.getPoiCategoryIds().size() != 0) {
                    int catNum = poi.poi.getPoiCategoryIds().get(poi.poi.getPoiCategoryIds().size()-1);
                    cat = PMPDataManager.getSharedPMPManager(null).getResponseData().getPoiCategoryMap().get(catNum);
                }
                if (cat == null && poi.poi.getBrandId() != 0) {
                    Brands brand = PMPDataManager.getSharedPMPManager(null).getResponseData().getBrandMap().get(poi.poi.getBrandId());
                    if (brand.getPoiCategoryIds() != null &&
                            brand.getPoiCategoryIds().size() != 0) {
                        int catNum = brand.getPoiCategoryIds().get(0);
                        cat = PMPDataManager.getSharedPMPManager(null).getResponseData().getPoiCategoryMap().get(catNum);
                    }
                }
                if (cat != null) {
                    if(poi.poi.getMarkerImage() != null){
                        PMPUtil.setImageWithURLForListView(imageView, poi.poi.getMarkerImage() );
                    }else {
                        String catImg = cat.getImage();
                        PMPUtil.setImageWithURLForListView(imageView, catImg);
                    }
                    int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20,  getResources().getDisplayMetrics());
                    imageView.setPadding(padding, padding, padding, padding);
                }
            }else {
                PMPUtil.setImageWithURL(imageView, poi.poi.getImage());
            }

            TextView tv_name = (TextView)convertView.findViewById(R.id.tv_name);
            tv_name.setTextColor(PMPMapSDK.getMapUISetting().getGenericCellTextColor());
            tv_name.setText(PMPUtil.getLocalizedString(poi.poi.getName()));
            TextView tv_location = (TextView)convertView.findViewById(R.id.tv_address);
            tv_location.setText(PMPUtil.getLocalizedAddress(getActivity(), poi.poi));
            TextView tv_duration = (TextView)convertView.findViewById(R.id.tv_duration);
            tv_duration.setText(PMPUtil.getLocalizedTime(getActivity(), (int)poi.duration));

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int)v.getTag();
                    if (pois != null && position < pois.size()) {
                        PMPPOIByDuration poi = pois.get(position);
                        ArrayList<Pois> poiList = new ArrayList<Pois>();
                        poiList.add(poi.poi);

                        PMPMapFragment mapFragment = (PMPMapFragment) getParentFragment();
                        if(pois.size() > 1) {
//                            mapFragment.isShowCloseButtonOnTop.set(true);
                            mapFragment.showPOIListOnMap(poiList, null, false, true, true, true);
                        }else{
                            mapFragment.showPOIListOnMap(poiList, null, false, false, true, true);

                        }
//                        for (int i = 0; i < getFragmentManager().getBackStackEntryCount(); i++) {
//                            getFragmentManager().popBackStack();
//                        }
                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                        if(poi.poi != null){
                            HashMap<String, Object> params = new HashMap<String, Object>();
                            params.put("selected_poi_id", (int)poi.poi.getId());
                            if(isComeFromGate) {
                                AnalyticsLogger.getInstance().logEvent("Filter_Boarding_Gate_Poi", params);
                            }else{
                                AnalyticsLogger.getInstance().logEvent("Filter_Around_Me_Poi", params);

                            }


                        }
                    }
                }
            });

            return convertView;
        }
    }
}
