/****************************************************************************
Copyright (c) 2015 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package com.pmp.mapsdk.app;

//import android.animation.Animator;
//import android.animation.ObjectAnimator;
//import android.bluetooth.BluetoothAdapter;
//import android.bluetooth.BluetoothManager;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.content.SharedPreferences;
//import android.content.pm.PackageManager;
//import android.content.res.Configuration;
//import android.graphics.PointF;
//import android.hardware.GeomagneticField;
//import android.hardware.Sensor;
//import android.hardware.SensorEvent;
//import android.hardware.SensorEventListener;
//import android.hardware.SensorManager;
//import android.location.Location;
//import android.location.LocationListener;
//import android.location.LocationManager;
//import android.net.ConnectivityManager;
//import android.net.NetworkInfo;
//import android.net.Uri;
//import android.os.Build;
//import android.os.Bundle;
//import android.support.v4.app.ActivityCompat;
//import android.support.v7.app.AlertDialog;
//import android.text.TextUtils;
//import android.util.DisplayMetrics;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.animation.AccelerateDecelerateInterpolator;
//import android.widget.AdapterView;
//import android.widget.BaseAdapter;
//import android.widget.Button;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.RadioGroup;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
//import com.cherrypicks.pmpmap.GPSLocationManager;
//import com.cherrypicks.pmpmap.PMPConfigActivity;
//import com.cherrypicks.pmpmap.PMPDataManager;
//import com.cherrypicks.pmpmap.PMPIndoorLocationManager;
//import com.cherrypicks.pmpmap.PMPMapConfig;
//import com.cherrypicks.pmpmap.analytics.AnalyticsLogger;
//import com.cherrypicks.pmpmap.circleprogress.CircleProgressView;
//import com.cherrypicks.pmpmap.circleprogress.TextMode;
//import com.cherrypicks.pmpmap.datamodel.MapState;
//import com.cherrypicks.pmpmap.datamodel.PathNode;
//import com.cherrypicks.pmpmap.datamodel.PathResult;
//import com.cherrypicks.pmpmap.ui.BottomHolderView;
//import com.cherrypicks.pmpmap.ui.ErrorHeaderView;
//import com.cherrypicks.pmpmap.ui.ErrorView;
//import com.cherrypicks.pmpmap.ui.NavTopView;
//import com.cherrypicks.pmpmap.ui.ProximityView;
//import com.cherrypicks.pmpmap.ui.SearchOverView;
//import com.cherrypicks.pmpmap.ui.ViewStateEvent;
//import com.cherrypicks.pmpmapsdk.R;
//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.GooglePlayServicesUtil;
//import com.google.android.gms.maps.CameraUpdate;
//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.MapFragment;
//import com.google.android.gms.maps.MapsInitializer;
//import com.google.android.gms.maps.OnMapReadyCallback;
//import com.google.android.gms.maps.model.BitmapDescriptor;
//import com.google.android.gms.maps.model.BitmapDescriptorFactory;
//import com.google.android.gms.maps.model.GroundOverlayOptions;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.LatLngBounds;
//import com.google.android.gms.maps.model.Marker;
//import com.google.android.gms.maps.model.MarkerOptions;
//import com.google.android.gms.maps.model.Polyline;
//import com.google.android.gms.maps.model.PolylineOptions;
//import com.pmp.mapsdk.cms.PMPServerManager;
//import com.pmp.mapsdk.cms.model.Maps;
//import com.pmp.mapsdk.cms.model.Pois;
//import com.pmp.mapsdk.cms.model.Promotions;
//import com.pmp.mapsdk.external.PMPMapSDK;
//import com.pmp.mapsdk.location.GeoCoordinate;
//import com.pmp.mapsdk.location.PMPBeacon;
//import com.pmp.mapsdk.location.PMPBeaconType;
//import com.pmp.mapsdk.location.PMPLocation;
//import com.pmp.mapsdk.location.PMPLocationManager;
//import com.pmp.mapsdk.location.PMPLocationManagerError;
//import com.pmp.mapsdk.utils.PMPUtil;
//import com.pmp.mapsdk.utils.PathJSONParser;
//
import org.cocos2dx.lib.Cocos2dxActivity;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Locale;
//import java.util.Map;


public class PMPMapActivity extends Cocos2dxActivity {}
//        implements ActivityCompat.OnRequestPermissionsResultCallback, OnMapReadyCallback, PMPMapConfig.MapModeChangeCallback{
//    public final static String POI_IDS = "POI_IDS";
//    public final static String LANG_ID = "LANG_ID";
//    public final static String MAP_STATE = "MAP_STATE";
//    public final static String DEBUG_ENABLE = "DEBUG_ENABLE";
//    public final static String CONTINUE_NAVIGATION = "CONTINUE_NAVIGATION";
//
//    private final String PUSH_ALREAD_SHOW = "PUSH_ALREAD_SHOW";
//    private final String TAG = "PMPMapActivity";
//    private static volatile boolean isEngineInitFinish = false;
//    private final int CHANGE_SETTING = 2;
//    private BroadcastReceiver receiver, proximityInzoneReceiver, proximityOutzoneReceiver;
//    private final String DRIVING = "DRIVING";
//    private final String WALKING = "WALKING";
//    private final PointF[] TAIKOOPLACE_REGION = new PointF[] {new PointF(22.287961f, 114.209696f),
//            new PointF(22.287891f, 114.214946f),
//            new PointF(22.285656f, 114.215030f),
//            new PointF(22.285866f, 114.210798f)};
//
//    private final int DefaultCompassTopConstraint = 10 ;
//    private final LatLng SWIRE_PIN_LOCATION =  new LatLng(22.287680, 114.212259);
//
//    private final float GMAPDefaultZoom = 19.2f;
//    private final int AnimationSpeed = 250;
//    private final int ResumeButtonHeight = 102 / 2;
//    private final int LocateMeButtonHeight = 138 / 2;
//    private final int DefaultBottomHolderHeightConstraint = 90;
//    private final int BEACON_MODE_LABEL_TIME_OUT_IN_SEC = 15;
//    private float SWITCH_FLOOR_BTN_ORI_Y;
//
//
//    private float deviceDensity;
//    private int gServicesStatus;
//
//    //UI Variable
//    private Button btn_switch_map;
//    private MapFragment mapFragment;
//    private Marker curLocationMarker;
//    private ListView lv_search;
//    private CircleProgressView cp_progress;
//    private RelativeLayout rl_loading, rl_search;
//    private NavTopView v_nav_top;
//    private ProximityView pv_banner;
//    private BottomHolderView v_bottom_view;
//    private Button btn_offline_retry;
//    private FrameLayout fl_offline_view;
//
//    //For Mode Label only
//    private TextView tv_mode;
//    private android.os.Handler modeHandler = new android.os.Handler();
//    private Runnable modeRunnale;
//
//    //Locale Variable
//    private PMPMapConfig mMapConfig;
//    protected boolean isIndoorMode = false, isFirstTimeDetectBeaconSignal = true;
//    private SensorManager sensorManager;
//    private float[] mAcc = new float[3];
//    private float[] mMag = new float[3];
//    private float[] mRotationMatrix = new float[9];
//    private float fAzimuth;
//    private SearchViewAdapter searchViewAdapter;
//    private List<Polyline> polylines = new ArrayList<Polyline>();
//    private ArrayList<Pois> pois;
//    private ArrayList<PathResult> currentSearchResult;
//    protected Pois selectedPOI;
//    private Marker selectedPoiMarker;
//    private boolean isNavigating = false, googleMapAlreadyLocateMe = false;
//
//    public boolean isOverViewModeEnable() {
//        return isOverViewModeEnable;
//    }
//
//    public void setOverViewModeEnable(boolean overViewModeEnable) {
//        isOverViewModeEnable = overViewModeEnable;
//        v_bottom_view.setOverViewModeEnable(overViewModeEnable);
//    }
//
//    private boolean isOverViewModeEnable = false;
//    private String travelingMode = DRIVING;
//    private RadioGroup toggle;
//    private SearchOverView sov_overview;
//    private int waitForJsonCount = 0;
//    private boolean isGoogleMapLocateMeClicked = false;
//
//    //Compass
//    private Sensor mAccelerometer;
//    private Sensor mMagnetometer;
//    // Gravity for accelerometer data
//    private float[] gravity = new float[3];
//    // magnetic data
//    private float[] geomagnetic = new float[3];
//    // Rotation data
//    private float[] rotation = new float[9];
//    // orientation (azimuth, pitch, roll)
//    private float[] orientation = new float[3];
//    // smoothed values
//    private float[] smoothed = new float[3];
//    private float bearing; // rotation angle to North
//    private GeomagneticField geomagneticField;
//    private SensorEventListener eventListener;
//    private PMPLocation currentIndoorLocation;
//    private Location currentOutdoorLocation;
//    protected MapState mapState = new MapState();
//    private boolean locateMe = true;
//    private boolean continueNavigation = false;
//    private ErrorHeaderView errorHeaderView;
//    private Button locateMeButton;
//
//    //----analytics use---
//    private boolean poiFromSearch;
//    //--------------------
//
//    //----Outdoor maps
//    GoogleMap outdoorMap;
//    boolean isOutdoorMapLoaded = false;
//    //-----
//
//    //compass
//    private float[] mGData = new float[3];
//    private float[] mMData = new float[3];
//    private float[] mR = new float[16];
//    private float[] mI = new float[16];
//    private float[] mOrientation = new float[3];
//
//
//    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
//        public void onReceive (Context context, Intent intent) {
//            String action = intent.getAction();
//
//            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
//                if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)
//                        == BluetoothAdapter.STATE_ON) {
//                    // Bluetooth is connected, do handling here
//                    if (!isOnline()) {
//                        showErrorHeader(PMPErrorType.NetworkConnection);
//                    } else if (!GPSLocationManager.checkIfLocationPermissionAllowed(context)) {
//                        showErrorHeader(PMPErrorType.LocationServiceDisabled);
//                    } else {
//                        showErrorHeader(PMPErrorType.None);
//                    }
//                } else {
//                    showErrorHeader(PMPErrorType.BluetoothDisabled);
//                }
//            }else if(LocationManager.PROVIDERS_CHANGED_ACTION.equals(action)){
//                final LocationManager manager = (LocationManager) context.getSystemService( Context.LOCATION_SERVICE );
//                if (manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) || manager.isProviderEnabled( LocationManager.NETWORK_PROVIDER )) {
//                    showErrorHeader(PMPErrorType.None);
//                }
//                else
//                {
//                    showErrorHeader(PMPErrorType.LocationServiceDisabled);
//                }
//            }
//        }
//    };
//
//    private final BroadcastReceiver mNetworkStateChangeReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//
//            if (isOnline()) {
//                BluetoothAdapter btAdapter = ((Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR1)
//                        ?((BluetoothManager)context.getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter()
//                        :(BluetoothAdapter.getDefaultAdapter()));
//
//                if(btAdapter == null || btAdapter.getState() != BluetoothAdapter.STATE_ON){
//                    //Bluetooth is OFF
//                    showErrorHeader(PMPErrorType.BluetoothDisabled);
//                } else if(!GPSLocationManager.checkIfLocationPermissionAllowed(context)) {
//                    showErrorHeader(PMPErrorType.LocationServiceDisabled);
//                } else {
//                    showErrorHeader(PMPErrorType.None);
//                }
//            } else {
//                showErrorHeader(PMPErrorType.NetworkConnection);
//            }
//        }
//    };
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        //----SETUP LANG-----//
//        int langId = getIntent().getIntExtra(LANG_ID, -1);
//        if (langId != -1) {
//            Configuration config = new Configuration(getResources().getConfiguration());
//            switch (langId){
//                case 1:
//                    config.locale = Locale.ENGLISH ;
//                    break;
//                case 2:
//                    config.locale = Locale.TRADITIONAL_CHINESE ;
//                    break;
//            }
//            getResources().updateConfiguration(config,getResources().getDisplayMetrics());
//            nativeSetLangId(langId);
//        }
//        //-------------------//
//
//        mMapConfig = PMPMapConfig.getSharedConfig(this);
//
//        MapsInitializer.initialize(this);
//
//        DisplayMetrics metrics = getResources().getDisplayMetrics();
//
//        deviceDensity = metrics.density;
//        nativeSetScreenInfo(deviceDensity, metrics.widthPixels, metrics.heightPixels);
//        Log.d(TAG, "deviceDensity:" + deviceDensity + " w: "+ metrics.widthPixels + "h:" + metrics.heightPixels);
////        Toast.makeText(this, mMapConfigActivity.this).getJniString(), Toast.LENGTH_SHORT).show();
//
//        //Clear push record when newly start
//        Map<String,?> keys = PMPUtil.getSharedPreferences(this).getAll();
//        SharedPreferences.Editor editor = PMPUtil.getSharedPreferences(this).edit();
//        for(Map.Entry<String,?> entry : keys.entrySet()){
//           if(entry.getKey().contains(PUSH_ALREAD_SHOW)){
//               editor.remove(entry.getKey());
//           }
//        }
//        editor.commit();
//
//        View mainView = LayoutInflater.from(this).inflate(R.layout.pmp_activity_main, null);
//        mainView.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
//        mFrameLayout.addView(mainView);
//
//        ImageView iv_back = (ImageView)mainView.findViewById(R.id.iv_back);
//
//        rl_search = (RelativeLayout)mainView.findViewById(R.id.rl_search);
//
//        iv_back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
//
//        ImageView iv_search = (ImageView)mainView.findViewById(R.id.iv_search);
//
//        iv_search.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(PMPMapActivity.this, PMPSearchActivity.class);
//                PMPMapActivity.this.startActivityForResult(intent, PMPSearchActivity.REQUEST_CODE);
//            }
//        });
//
//        receiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                View mainView = LayoutInflater.from(PMPMapActivity.this).inflate(R.layout.activity_main_invalid_session, null);
//                mainView.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
//                mFrameLayout.addView(mainView);
//            }
//        };
//
//        proximityInzoneReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//
//                int zoneID = intent.getIntExtra(PMPIndoorLocationManager.BROADCAST_ZONE_ID, -1);
//
//                Promotions promotion = PMPUtil.findPromotionByMajorID(getContext(), zoneID);
//                if(promotion == null){
//                    return;
//                }
//
//                String key = PUSH_ALREAD_SHOW + promotion.getId();
//                boolean isPushAlreadyShown = PMPUtil.getSharedPreferences(context).getBoolean(key, false);
//                if(isPushAlreadyShown){
//                    return;
//                }
//
//
//                PMPUtil.getSharedPreferences(context).edit().putBoolean(key, true).commit();
//
//                if(promotion != null && promotion.getMessage().size() > 0 && promotion.getPromotionType() == Promotions.PROMOTION_TYPE_ZONAL){
//                    pv_banner.setPromotion(promotion);
//                    pv_banner.setMajorID(zoneID);
//                    setProximityBannerVisibility(View.VISIBLE);
//                }
//
//
//            }
//        };
//
//        proximityOutzoneReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                int zoneID = intent.getIntExtra(PMPIndoorLocationManager.BROADCAST_ZONE_ID, -1);
//
////                setProximityBannerVisibility(View.GONE);
//            }
//        };
//
//        errorHeaderView = (ErrorHeaderView) findViewById(R.id.error_header);
//
//        IntentFilter filter = new IntentFilter(PMPIndoorLocationManager.BROADCAST_INVALID_SESSION);
//        IntentFilter filter_proximity_inzone = new IntentFilter(PMPIndoorLocationManager.BROADCAST_PROXIMITY_ENTER_ZONE);
//        IntentFilter filter_proximity_outzone = new IntentFilter(PMPIndoorLocationManager.BROADCAST_PROXIMITY_OUT_ZONE);
//
//
//        registerReceiver(receiver, filter);
//        registerReceiver(proximityInzoneReceiver, filter_proximity_inzone);
//        registerReceiver(proximityOutzoneReceiver, filter_proximity_outzone);
//
//        configUI(mainView);
//        configMapConfig();
//        configSensor();
//        waitForJsonDownload();
//        changeInOutDoorMode(true);
//
////        new android.os.Handler().postDelayed(new Runnable() {
////            @Override
////            public void run() {
////                pv_banner.setPromotion(PMPDataManager.getSharedPMPManager(getContext()).getResponseData().getPromotions().get(1));
////                setProximityBannerVisibility(View.VISIBLE);
////            }
////        }, 5000);
//        AnalyticsLogger.getInstance().logEvent("MAP_OPEN");
//    }
//
//
//    @Override
//    protected void onPostCreate(Bundle savedInstanceState) {
//        super.onPostCreate(savedInstanceState);
//        //init MapEngine
//        initNativeEngine();
//    }
//
//
//    private void onMapInitFinished(){
//        isEngineInitFinish = true;
//
//        GPSLocationManager gpsLocationManager = GPSLocationManager.getSharedManager(PMPMapActivity.this);
//        gpsLocationManager.requestLocationUpdates(PMPMapActivity.this);
//        PMPIndoorLocationManager.getSharedPMPManager(PMPMapActivity.this).requireLocationPermissionIfNeeded(PMPMapActivity.this);
//
//        pois = PMPDataManager.getSharedPMPManager(PMPMapActivity.this).getResponseData().getPois();
//        searchViewAdapter.notifyDataSetChanged();
//
//        if(gpsLocationManager.getLatestLocation(PMPMapActivity.this) != null){
//            updateUserGPSLocation(gpsLocationManager.getLatestLocation(PMPMapActivity.this));
//        }
//        mMapConfig.pmpStartLoadingScene();
//
//        configIndoorLocationCallback();
//
//        if (getIntent().hasExtra(MAP_STATE)) {
//            locateMe = false;
//            mapState = (MapState)getIntent().getSerializableExtra(MAP_STATE);
//
//            isIndoorMode = mapState.isIndoor();
//            changeInOutDoorMode(isIndoorMode);
//            mMapConfig.pmpSetMapMode(PMPMapConfig.PMPMapModeBrowsing);
//            //Quick fix only... Bug(?)for cocos2dx when resume
//            new android.os.Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//
//                    if (mapState.getMapMode() == PMPMapConfig.PMPMapModeNavigating) {
//
//                        for (Pois poi : PMPDataManager.getSharedPMPManager(PMPMapActivity.this).getResponseData().getPois()) {
//                            if (poi.getId() == mapState.getSelectedPOIId()) {
//                                selectedPOI = poi;
//                                break;
//                            }
//                        }
//                        showOverViewModeWithPOI(selectedPOI);
//                        startNavigation();
//                        runOnGLThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                nativeSetMapState(mapState);
//                            }
//                        });
//                    }else {
//                        if (mapState.getSelectedPOIId() > 0) {
//                            for (final Pois poi : PMPServerManager.getShared(PMPMapActivity.this).getServerResponse().getPois()) {
//                                if (poi.getId() == mapState.getSelectedPOIId()) {
//                                    onSearchPOIClicked(poi);
//                                    break;
//                                }
//                            }
//                        }else {
//
//                            runOnGLThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    nativeSetMapState(mapState);
//                                }
//                            });
//
//                        }
//                    }
//
//                }
//            },500);
//
//
//
//        }else {
//            locateMe = false;
//            //-----Select POI------//
//            String poiExternalId = getIntent().getStringExtra(POI_IDS);
//            if (TextUtils.isEmpty(poiExternalId) == false) {
//                for (final Pois poi : PMPServerManager.getShared(this).getServerResponse().getPois()) {
//                    if (poi.getExternalId().equals(poiExternalId)) {
//                        //Quick fix only... Bug(?)for cocos2dx when resume
//                        new android.os.Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                onSearchPOIClicked(poi);
//                            }
//                        },500);
//                        break;
//                    }
//                }
//            }
//            //--------------------//
//        }
//
//        hideLoading();
//    }
//
//    private void waitForJsonDownload(){
//        //40 * 500 ~ 20sec
//        if(waitForJsonCount < 40){
//            waitForJsonCount++;
//            new android.os.Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    PMPDataManager pmpDataManager = PMPDataManager.getSharedPMPManager(PMPMapActivity.this);
//                    if(pmpDataManager.isDataDownloadFinish()){
//                        fl_offline_view.setVisibility(View.GONE);
//                        if(isEngineInitFinish){
//                            onMapInitFinished();
//                        }else{
//                            //Pass JSON to Engine
//                            String jsonString = pmpDataManager.getResponseData().getJsonObject().toString();
//                            mMapConfig.pmpNativeParseJson(jsonString);
//                            mMapConfig.applyNewConfig();
//                        }
//                    }else{
//                        waitForJsonDownload();
//                    }
//                }
//            },500);
//        }else{
//            //Download Json timeout!!!
//            fl_offline_view.setVisibility(View.VISIBLE);
//            hideLoading();
//            //// TODO: 21/3/16 Add Error handling for network timeout/error
//        }
//
//    }
//
//    private void initNativeEngine(){
//        mMapConfig.pmpInit();
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        PMPLocationManager.getShared(this).onMapActivityResume();
//        registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
//        registerReceiver(mReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
//        registerReceiver(mNetworkStateChangeReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
//        sensorManager.registerListener(eventListener, mAccelerometer, SensorManager.SENSOR_DELAY_GAME);
//        sensorManager.registerListener(eventListener, mMagnetometer, SensorManager.SENSOR_DELAY_GAME);
//        PMPIndoorLocationManager.getSharedPMPManager(this).startDetection();
//        if (isOverViewModeEnable) {
//            //Quick fix only... Bug(?)for cocos2dx when resume
//            new android.os.Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    showOverViewModeWithPOI(selectedPOI);
//                }
//            }, 500);
//        }
//        if (GPSLocationManager.checkIfLocationPermissionAllowed(this)) {
//            BluetoothAdapter btAdapter = ((Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR1)
//                    ?((BluetoothManager)this.getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter()
//                    :(BluetoothAdapter.getDefaultAdapter()));
//
//            if(btAdapter == null || btAdapter.getState() != BluetoothAdapter.STATE_ON){
//                //Bluetooth is OFF
//                showErrorHeader(PMPErrorType.BluetoothDisabled);
//            } else if(!isOnline()) {
//                showErrorHeader(PMPErrorType.NetworkConnection);
//            } else {
//                showErrorHeader(PMPErrorType.None);
//            }
//        } else {
//            showErrorHeader(PMPErrorType.LocationServiceDisabled);
//        }
//
//        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
//        if (manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) || manager.isProviderEnabled( LocationManager.NETWORK_PROVIDER )) {
//            showErrorHeader(PMPErrorType.None);
//        }else{
//            showErrorHeader(PMPErrorType.LocationServiceDisabled);
//        }
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        PMPLocationManager.getShared(this).onMapActivityPause();
//        unregisterReceiver(mReceiver);
//        sensorManager.unregisterListener(eventListener, mAccelerometer);
//        sensorManager.unregisterListener(eventListener, mMagnetometer);
//    }
//
//    @Override
//    protected void onDestroy() {
//        //clear status
//        mMapConfig.pmpResetEngine();
//        unregisterReceiver(receiver);
//        unregisterReceiver(proximityOutzoneReceiver);
//        unregisterReceiver(proximityInzoneReceiver);
//        GPSLocationManager.getSharedManager(this).cancelLocationUpdate();
//        PMPIndoorLocationManager.getSharedPMPManager(this).setLocationListener(null);
//        AnalyticsLogger.getInstance().logEvent("MAP_CLOSE");
//        super.onDestroy();
//    }
//
//
//    private void showLoading(){
//        rl_loading.setVisibility(View.VISIBLE);
//    }
//
//    private void hideLoading(){
//        rl_loading.setVisibility(View.GONE);
//    }
//
//    private void configUI(View v){
//        //Config loading
//        cp_progress = (CircleProgressView)v.findViewById(R.id.cp_progress);
//        rl_loading = (RelativeLayout)v.findViewById(R.id.rl_loading);
//        cp_progress.setText(getString(R.string.loading));
//        cp_progress.setTextMode(TextMode.VALUE);
//        cp_progress.setShowTextWhileSpinning(false);
//        cp_progress.setBarWidth(20);
//        cp_progress.setRimWidth(20);
//        cp_progress.setContourSize(0);
//        cp_progress.spin();
//        rl_loading.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                //prevent touch
//                return true;
//            }
//        });
//        showLoading();
//
//        //Back button
//        ImageView iv_back = (ImageView)v.findViewById(R.id.iv_back);
//
//        iv_back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
//
//        //config hidden setting
//        if (getIntent().getBooleanExtra(DEBUG_ENABLE, false)) {
//            rl_search.setOnLongClickListener(new View.OnLongClickListener() {
//
//                @Override
//                public boolean onLongClick(View v) {
//                    Intent goSettingPage = new Intent(PMPMapActivity.this, PMPConfigActivity.class);
//                    startActivityForResult(goSettingPage, CHANGE_SETTING);
//
//                    return true;
//                }
//
//
//            });
//        }
//
//        //config proximity
//        pv_banner = (ProximityView) v.findViewById(R.id.pv_banner);
//        pv_banner.setProximityListener(new ProximityView.ProximityListener() {
//            @Override
//            public void onProximityBannerClicked() {
//                if(pv_banner.getPromotion() != null){
//                    launchPromotion(pv_banner.getMajorID(), pv_banner.getPromotion().getExternalId(), mapState);
//                    HashMap<String, Object> param = new HashMap<>();
//                    param.put("origin", "banner");
//                    param.put("promotionId", pv_banner.getPromotion().getExternalId());
//                    param.put("proximityId", pv_banner.getMajorID());
//                    AnalyticsLogger.getInstance().logEvent("PROXIMITY_OPEN_DETAIL");
//                }
//
//            }
//
//            @Override
//            public void onProximityBannerCancel() {
//                setProximityBannerVisibility(View.GONE);
//            }
//        });
//
//        //Bottom holder
//        v_bottom_view = (BottomHolderView) v.findViewById(R.id.v_bottom_view);
//        v_bottom_view.setPMPMapConfigReference(mMapConfig);
//        v_bottom_view.setBottomHolderListener(new BottomHolderView.BottomHolderListener() {
//
//            @Override
//            public void onModeChanged(int mode) {
//                adjustCompassPosition();
//            }
//
//            @Override
//            public void onStartNavClicked(Pois poi) {
//                showOverViewModeWithPOI(poi);
//                if(currentSearchResult.size() > 0){
//                    HashMap<String, Object> param = new HashMap<String, Object>();
//                    PathResult pathResult = currentSearchResult.get(0);
//                    StringBuffer sb = new StringBuffer();
//                    for (int i = 0; i < pathResult.getPathNodes().size(); i++) {
//                        PathNode node = pathResult.getPathNodes().get(i);
//                        sb.append(node.getCurrentVertex().getNode_id());
//                        if (i != pathResult.getPathNodes().size() - 1) {
//                            sb.append(',');
//                        }
//                    }
//                    param.put("poiId", (int)poi.getId());
//                    param.put("pathIds", sb.toString());
//                    param.put("origin", poiFromSearch ? "search" : "map");
//                    AnalyticsLogger.getInstance().logEvent("NAVIGATION_START_PREVIEW", param);
//                }
//            }
//
//            @Override
//            public void onPOIDetailClicked(Pois poi) {
//                updateMapState();
//                PMPMapActivity.this.onPOIDetailClicked(poi.getExternalId(), mapState);
//
//            }
//        });
//
//        //config Google travel mode
//        toggle = (RadioGroup)v.findViewById(R.id.toggle);
//        toggle.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                if(checkedId == R.id.rbtn_walk){
//                    travelingMode = WALKING;
//                }else{
//                    travelingMode = DRIVING;
//                }
//                updateGoogleNavigationPath(selectedPOI);
//            }
//        });
//
//        //Search Over View
//        sov_overview = (SearchOverView)v.findViewById(R.id.sov_overview);
//        sov_overview.setViewStatEvent(new ViewStateEvent() {
//            @Override
//            public void onVisibilityChanged(int visibility) {
//                setOverViewModeEnable(visibility == View.VISIBLE);
//            }
//        });
//        sov_overview.setNavigationListener(new SearchOverView.NavigationListener() {
//            @Override
//            public void onCloseNavBtnClicked() {
//                //-------
//                if(currentSearchResult != null && currentSearchResult.size() > 0){
//                    HashMap<String, Object> param = new HashMap<String, Object>();
//                    PathResult pathResult = currentSearchResult.get(0);
//                    StringBuffer sb = new StringBuffer();
//                    for (int i = 0; i < pathResult.getPathNodes().size(); i++) {
//                        PathNode node = pathResult.getPathNodes().get(i);
//                        sb.append(node.getCurrentVertex().getNode_id());
//                        if (i != pathResult.getPathNodes().size() - 1) {
//                            sb.append(',');
//                        }
//                    }
//                    param.put("poiId", (int)selectedPOI.getId());
//                    param.put("pathIds", sb.toString());
//                    AnalyticsLogger.getInstance().logEvent("NAVIGATION_CLOSE_PREVIEW", param);
//                }
//                //-------
//
//                mMapConfig.pmpClearSearhResult();
//                mMapConfig.pmpHideOverViewMode();
//                hideOverViewUI();
//                continueNavigation = false;
//
//                Pois poi = selectedPOI;
//                selectedPOI = null;
//
//                Maps map = PMPUtil.findFloorByPoi(PMPMapActivity.this, poi);
//                boolean isStaticMapPOI= false;
//                if (map != null) {
//                    isStaticMapPOI = map.getStaticMap();
//                }
//                if(!isStaticMapPOI && poi != null && !poi.getNotCovered()){
//                    onSearchPOIClicked(poi);
//                }else {
//                    v_bottom_view.showBottomHolderByMode(BottomHolderView.BottomHolderModeHidden);
//                }
//
////                PMPPois *poi = self.selectedPOI;
////                self.selectedPOI = nil;
////
////                bool isStaticMapPOI = false;
////                for (PMPMaps* p in [PMPServerManager sharedInstance].serverResponse.maps) {
////                    if (p.mapsIdentifier == poi.mapId && p.staticMap) {
////                        isStaticMapPOI = true;
////                        break;
////                    }
////                }
////
////                //POI on static Map and out-of-range POI should go overview directly (Map logic defined at 20/7)
////                if(!isStaticMapPOI && !poi.notCovered){
////                    [self onSearchPOIClicked:poi];
////                }else {
////                    [self showBottomHolderByMode:BottomHolderModeHidden];
////                }
////
////                [self clearSearhResult];
////                weakSelf.indoorMapButton.hidden = NO;
//            }
//
//            @Override
//            public void onStartNavBtnClicked() {
//                startNavigation();
//            }
//
//            @Override
//            public void onPathOptionIndexChanged(int index, SearchOverView view) {
//                mMapConfig.pmpSetCurrentPathResult(view.getCurrentPathIndex());
//                updateOverviewText();
//                showNativeOverViewWithPOI(selectedPOI);
//                //-------
//                if(currentSearchResult != null && currentSearchResult.size() > 0){
//                    HashMap<String, Object> param = new HashMap<String, Object>();
//                    PathResult pathResult = currentSearchResult.get(0);
//                    StringBuffer sb = new StringBuffer();
//                    for (int i = 0; i < pathResult.getPathNodes().size(); i++) {
//                        PathNode node = pathResult.getPathNodes().get(i);
//                        sb.append(node.getCurrentVertex().getNode_id());
//                        if (i != pathResult.getPathNodes().size() - 1) {
//                            sb.append(',');
//                        }
//                    }
//                    param.put("poiId", (int)selectedPOI.getId());
//                    param.put("pathIds", sb.toString());
//                    AnalyticsLogger.getInstance().logEvent("NAVIGATION_SELECT_ALTERNATIVE_PATH", param);
//                }
//                //-------
//            }
//        });
//
//        //config Search table
//        lv_search = (ListView)v.findViewById(R.id.lv_search);
//        searchViewAdapter = new SearchViewAdapter();
//        lv_search.setAdapter(searchViewAdapter);
//        lv_search.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                currentSearchResult = null;
//                Pois poi = (Pois) parent.getAdapter().getItem(position);
//                selectedPOI = poi;
//                hideSearchTable();
//                forceSelectPOI((int) selectedPOI.getId());
////                showOverViewModeWithPOI(poi);
////                if(currentSearchResult != null && currentSearchResult.size() > 0){
////                    ArrayList<Map> maps = PMPDataManager.getSharedPMPManager(PMPMapActivity.this).getResponseData().getMap();
////                    String floorName = "";
////                    for(Map curMap : maps) {
////                        if(curMap.getId() == poi.getMapId()){
////                            floorName = curMap.getName();
////                            break;
////                        }
////                    }
////
////                    Intent goPmpNavToPoiActivity = new Intent(PMPMapActivity.this, PMPNavToPoiActivity.class);
////                    goPmpNavToPoiActivity.putExtra(PMPNavToPoiActivity.DES_NAME, poi.getName());
////                    goPmpNavToPoiActivity.putExtra(PMPNavToPoiActivity.TO_FLOOR, floorName);
////                    goPmpNavToPoiActivity.putExtra(PMPNavToPoiActivity.FROM_FLOOR, mMapConfig.pmpGetCurrentFloor());
////                    goPmpNavToPoiActivity.putExtra(PMPNavToPoiActivity.TOTAL_DST, String.format("%.0f", calculateDistance()));
////                    goPmpNavToPoiActivity.putExtra(PMPNavToPoiActivity.TOTAL_TIME, calculateTimeInMin());
////                    startActivityForResult(goPmpNavToPoiActivity, REQUEST_NAV);
////                }else{
////                    Toast.makeText(PMPMapActivity.this, getString(R.string.Path_Not_Found), Toast.LENGTH_LONG).show();
////                }
//
//            }
//        });
//        lv_search.setVisibility(View.GONE);
//
//        //Config navigation mode UI
//        v_nav_top = (NavTopView)v.findViewById(R.id.v_nav_top);
//        v_nav_top.setVisibility(View.GONE);
//        v_nav_top.setCloseNavigationListener(new NavTopView.CloseNavigationListener() {
//            @Override
//            public void onCloseNavBtnClicked() {
//                //-------
//                if(selectedPOI != null){
//                    HashMap<String, Object> param = new HashMap<String, Object>();
//                    param.put("poiId", (int)selectedPOI.getId());
//                    String state = "navigating";
//                    if (mMapConfig.pmpGetCurrentNavitaionStep() == PMPMapConfig.NavigationStep_Arrived) {
//                        state = "destination_arrival";
//                    }else if (mMapConfig.pmpGetCurrentNavitaionStep() == PMPMapConfig.NavigationStep_ByPassed) {
//                        state = "bypass_destination";
//                    }
//                    param.put("state", state);
//                    AnalyticsLogger.getInstance().logEvent("NAVIGATION_CLOSE", param);
//                }
//                //-------
//
//                isNavigating = false;
//                toggle.setVisibility(View.GONE);
//                clearPolyLine();
//
//                PMPMapConfig config = mMapConfig;
//                config.pmpSetMapMode(PMPMapConfig.PMPMapModeBrowsing);
//                config.pmpClearSearhResult();
//                selectedPOI = null;
//                v_bottom_view.showBottomHolderByMode(BottomHolderView.BottomHolderModeHidden);
//            }
//        });
//
//        btn_switch_map = (Button)v.findViewById(R.id.btn_switch_map);
//        locateMeButton = (Button)v.findViewById(R.id.btn_locate_me);
//        v_bottom_view.setGoogleMapLocateMeButtonReference(locateMeButton);
//        v_bottom_view.setIndoorButtonReference(btn_switch_map);
//        btn_switch_map.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                isIndoorMode = !isIndoorMode;
//                changeInOutDoorMode(isIndoorMode);
//                String mode = "IDP";
//                if (mMapConfig.pmpIsEnableOverViewMode()) {
//                    mode = "Preview";
//                }else  if (mMapConfig.pmpGetCurrentNavitaionStep() != PMPMapConfig.NavigationStep_NotNavigating) {
//                    mode = "Navigation";
//                }
//                AnalyticsLogger.getInstance().logEvent("MAP_TOGGLE_INDOOR_OUTDOOR");
//            }
//        });
//        locateMeButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (currentOutdoorLocation != null) {
//                    LatLng newLocation = new LatLng(currentOutdoorLocation.getLatitude(), currentOutdoorLocation.getLongitude());
//                    if (outdoorMap != null && isOutdoorMapLoaded) {
//                        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(newLocation, GMAPDefaultZoom);
//                        outdoorMap.animateCamera(update);
//                    }
//
//                }
//
//                isGoogleMapLocateMeClicked = true;
//                locateMe = true;
//                locateMeButton.setBackgroundResource(R.drawable.btn_locate_on);
//
//            }
//        });
//
//        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);
//
//
//
//
//        GPSLocationManager.getSharedManager(this).setGpsLocationListener(new LocationListener() {
//            @Override
//            public void onLocationChanged(Location location) {
//                if(!googleMapAlreadyLocateMe){
//                    googleMapAlreadyLocateMe = true;
//                    updateUserGPSLocation(location);
//                }else if(locateMe){
//                    updateUserGPSLocation(location);
//                }
//            }
//
//            @Override
//            public void onStatusChanged(String provider, int status, Bundle extras) {
//                updateUserGPSLocation(GPSLocationManager.getSharedManager(PMPMapActivity.this).getLatestLocation(PMPMapActivity.this));
//            }
//
//            @Override
//            public void onProviderEnabled(String provider) {
//
//            }
//
//            @Override
//            public void onProviderDisabled(String provider) {
//
//            }
//        });
//
//        gServicesStatus = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
//        if(gServicesStatus != ConnectionResult.SUCCESS) {
//            //GoogleMap not available...
//            GooglePlayServicesUtil.getErrorDialog(gServicesStatus, this, gServicesStatus).show();
//            changeInOutDoorMode(true);
//            btn_switch_map.setVisibility(View.GONE);
//        }
//
//        //Config mode textview
//        tv_mode = (TextView)v.findViewById(R.id.tv_mode);
//
//        btn_offline_retry = (Button)v.findViewById(R.id.btn_offline_retry);
//        fl_offline_view = (FrameLayout)v.findViewById(R.id.fl_offline_view);
//        btn_offline_retry.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showLoading();
//                PMPIndoorLocationManager.getSharedPMPManager(PMPMapActivity.this).initBeacon();
//                waitForJsonCount = 0;
//                waitForJsonDownload();
//            }
//        });
//    }
//
//    private void configMapConfig(){
//        PMPMapConfig config = mMapConfig;
//
//
//        config.setMapEngineInitialFinishCallback(new PMPMapConfig.MapEngineInitialFinishCallback() {
//            @Override
//            public void onMapEngineInitialFinish() {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        onMapInitFinished();
//                    }
//                });
//            }
//        });
//        config.setMapModeChangeCallback(this);
//
//        config.setPoiCallback(new PMPMapConfig.POICallback() {
//            @Override
//            public void onSelectPOI(final int poi_id) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        poiFromSearch = false;
//                        Pois poi = PMPUtil.findPOIByPoiID(PMPMapActivity.this, poi_id);
//                        PMPMapActivity.this.onSelectPOI(poi);
//                    }
//                });
//            }
//
//            @Override
//            public void onDeselectPOI() {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        PMPMapActivity.this.onDeselectPOI();
//                    }
//                });
//            }
//
//            @Override
//            public void onSelectPOIPin(final int poi_id) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Promotions promotions = PMPUtil.findPromotionByPOIID(PMPMapActivity.this, poi_id);
//                        if(promotions != null){
//                            HashMap<String, Object> param = new HashMap<>();
//                            param.put("origin", "poi");
//                            param.put("promotionId", promotions.getExternalId());
//                            param.put("proximityId", promotions.getId());
//                            AnalyticsLogger.getInstance().logEvent("PROXIMITY_OPEN_DETAIL");
//                            launchPromotion(-1, promotions.getExternalId(), mapState);
//                        }
//                    }
//                });
//            }
//        });
//
//        config.setArrivedDestinationCallback(new PMPMapConfig.ArrivedDestinationCallback() {
//            @Override
//            public void onArrivedDestination() {
//                if(selectedPOI != null && selectedPOI.getNotCovered()){
//                    v_nav_top.setArrivedNotCoveredDst();
//                }else{
//                    v_nav_top.setArrivedDst();
//                }
//
//                v_bottom_view.showBottomHolderByMode(BottomHolderView.BottomHolderModeHidden);
//            }
//
//            @Override
//            public void onBypassedDestination() {
//                v_nav_top.setBypassedDst();
//                v_bottom_view.showBottomHolderByMode(BottomHolderView.BottomHolderModeShowByPass);
//            }
//        });
//
//        config.setStepMessageUpdateCallback(new PMPMapConfig.StepMessageUpdateCallback() {
//            @Override
//            public void onStepMessageUpdate(int mode, String message, String poiMessage, String iconImageName, float totalDuration, float totalDistance) {
//                v_nav_top.setStepMessageUpdate(mode, message, poiMessage, iconImageName, totalDuration, totalDistance);
//            }
//        });
//    }
//
//    private void configIndoorLocationCallback(){
//        PMPIndoorLocationManager.getSharedPMPManager(this).setLocationListener(new PMPIndoorLocationManager.IndoorLocationListener() {
//            @Override
//            public void didIndoorLocationUpdated(final PMPBeaconType beaconType, final PMPLocation location) {
////                Toast.makeText(getContext(), "BeaconType = " + beaconType , Toast.LENGTH_SHORT).show();
//                Log.i(TAG, "didIndoorLocationUpdated " + location.getX() + " " + location.getY());
//                if(mMapConfig.isInitFinish()){
//                    mMapConfig.pmpOnIndoorLocationUpdate(beaconType.getValue(), location);
//                }
//                currentIndoorLocation = location;
//
//                //First time detect indoor location, we should auto-swith it into indoor mode
//                if(isFirstTimeDetectBeaconSignal){
//                    isFirstTimeDetectBeaconSignal = false;
//                    changeInOutDoorMode(true);
//                }
//
//                {
//                    //iBeacon mode label
//                    tv_mode.setVisibility(View.GONE);
//                    if(modeRunnale != null){
//                        modeHandler.removeCallbacks(modeRunnale);
//                    }
//                    modeRunnale = new Runnable() {
//                        @Override
//                        public void run() {
//                            tv_mode.setVisibility(View.GONE);
//                        }
//                    };
//                    modeHandler.postDelayed(modeRunnale, BEACON_MODE_LABEL_TIME_OUT_IN_SEC * 1000);
//                }
//
//                {
//                    if(mMapConfig.pmpGetMapMode() == PMPMapConfig.PMPMapModeNavigating){
//                        String distance = calculateDistance();
//                        String time = calculateTimeInMin();
//                        v_bottom_view.setNavDistance(distance + getString(R.string.PMPMAP_METER));
//                        v_bottom_view.setNavTime(time+getString(R.string.PMPMAP_MINS));
//
//                        if(selectedPOI != null)
//                            v_bottom_view.setDestinationName(PMPUtil.getLocalizedString(selectedPOI.getName()));
////                        v_bottom_view.setDestinationIcon(selectedPOI.getCategoryIcon());
//                    }
//                }
//            }
//
//            @Override
//            public void didIndoorTransmissionsUpdated(List<PMPBeacon> transmissions) {
////                runOnUiThread(new Runnable() {
////                    @Override
////                    public void run() {
////                        errorHeaderView.setVisibility(View.GONE);
////                    }
////                });
//                Log.i(TAG, "didIndoorTransmissionsUpdated size = " + (transmissions == null? 0  : transmissions.size()));
//                mMapConfig.pmpSetBeacons(transmissions);
//            }
//
//            @Override
//            public void didIndoorExitRegion() {
//                Log.i(TAG, "didIndoorExitRegion");
//                currentIndoorLocation = null;
//                mMapConfig.pmpOnExitIndoor();
//
//                boolean needShowDialog = true;
//
//
//                if(mMapConfig.pmpGetMapMode() == PMPMapConfig.PMPMapModeNavigating){
//                    v_nav_top.forcePerformCloseNavigation();
//                }else if(isOverViewModeEnable){
//                    sov_overview.forecPerformCloseNavigation();
//                }else{
//                    needShowDialog = false;
//                }
//
//                if(needShowDialog){
//                    AlertDialog dialog = new AlertDialog.Builder(getContext())
//                            .setTitle("")
//                            .setMessage(getString(R.string.PMPMAP_NAV_EXIT_REGION))
//                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int which) {
//                                    // continue with delete
//                                }
//                            })
//                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int which) {
//                                    // do nothing
//                                }
//                            })
//                            .setIcon(android.R.drawable.ic_dialog_alert)
//                            .show();
//                }
//            }
//
//            @Override
//            public void didOutdoorLocationsUpdated() {
//                Log.i(TAG, "didOutdoorLocationsUpdated ");
//            }
//
//            @Override
//            public void didCompassUpdated(double direction) {
//
//            }
//
//            @Override
//            public void didError(PMPLocationManagerError error) {
//                Log.d(TAG, "didError:" + error);
////                if (error == PMPLocationManagerError.BLUETOOTH_POWER_OFF) {
////                    errorHeaderView.setVisibility(View.VISIBLE);
////                }
//            }
//        });
//
//    }
//
//    private void configSensor(){
//        sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
//
//        //Listener for magnetic and accelerometer events
//        eventListener = new SensorEventListener() {
//            @Override
//            public void onSensorChanged(SensorEvent event) {
//                /*
//                boolean accelOrMagnetic = false;
//                // get accelerometer data
//                if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
//                    // we need to use a low pass filter to make data smoothed
//                    smoothed = lowPass(event.values, gravity);
//                    gravity[0] = smoothed[0];
//                    gravity[1] = smoothed[1];
//                    gravity[2] = smoothed[2];
//                    accelOrMagnetic = true;
//
//                } else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
//                    smoothed = lowPass(event.values, geomagnetic);
//                    geomagnetic[0] = smoothed[0];
//                    geomagnetic[1] = smoothed[1];
//                    geomagnetic[2] = smoothed[2];
//                    accelOrMagnetic = true;
//
//                }
//
//                // get rotation matrix to get gravity and magnetic data
//                SensorManager.getRotationMatrix(rotation, null, gravity, geomagnetic);
//                // get bearing to target
//                SensorManager.getOrientation(rotation, orientation);
//                // east degrees of true North
//                bearing = orientation[0];
//                // convert from radians to degrees
//                bearing = (float)Math.toDegrees(bearing);
//
//                // fix difference between true North and magnetical North
//                if (geomagneticField != null) {
//                    bearing += geomagneticField.getDeclination();
//                }
//
//                // bearing must be in 0-360
//                if (bearing < 0) {
//                    bearing += 360;
//                }*/
//
//                //-----GOOGLE---//
//                /*
//                int type = event.sensor.getType();
//                float[] data;
//                if (type == Sensor.TYPE_ACCELEROMETER) {
//                    data = mGData;
//                } else if (type == Sensor.TYPE_MAGNETIC_FIELD) {
//                    data = mMData;
//                } else {
//                    // we should not be here.
//                    return;
//                }
//                for (int i=0 ; i<3 ; i++)
//                    data[i] = event.values[i];
//                SensorManager.getRotationMatrix(mR, mI, mGData, mMData);
//
//                SensorManager.getOrientation(mR, mOrientation);
//
//                final float rad2deg = (float)(180.0f/Math.PI);
//                bearing = mOrientation[0] * rad2deg;
//                if (bearing < 0) {
//                    bearing += 360;
//                }*/
//
//                final float alpha = 0.97f;
//
//                synchronized (this) {
//                    if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
//
//                        mGData[0] = alpha * mGData[0] + (1 - alpha)
//                                * event.values[0];
//                        mGData[1] = alpha * mGData[1] + (1 - alpha)
//                                * event.values[1];
//                        mGData[2] = alpha * mGData[2] + (1 - alpha)
//                                * event.values[2];
//
//                        // mGravity = event.values;
//
//                        // Log.e(TAG, Float.toString(mGravity[0]));
//                    }
//
//                    if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
//                        // mGeomagnetic = event.values;
//
//                        mMData[0] = alpha * mMData[0] + (1 - alpha)
//                                * event.values[0];
//                        mMData[1] = alpha * mMData[1] + (1 - alpha)
//                                * event.values[1];
//                        mMData[2] = alpha * mMData[2] + (1 - alpha)
//                                * event.values[2];
//                        // Log.e(TAG, Float.toString(event.values[0]));
//
//                    }
//
//                    float R[] = new float[9];
//                    float I[] = new float[9];
//                    boolean success = SensorManager.getRotationMatrix(R, I, mGData,
//                            mMData);
//                    if (success) {
//                        float orientation[] = new float[3];
//                        SensorManager.getOrientation(R, orientation);
//                        // Log.d(TAG, "azimuth (rad): " + azimuth);
//                        bearing = (float) Math.toDegrees(orientation[0]); // orientation
//                        bearing = (bearing + 360) % 360;
//
//                    }
//                }
//                mMapConfig.pmpOnCompassUpdate(bearing);
//            }
//
//            @Override
//            public void onAccuracyChanged(Sensor sensor, int accuracy) {
//
//            }
//        };
//
//        //Register listeners for magnetic and accelerometer sensors
////        sensorManager.registerListener(eventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION), SensorManager.SENSOR_DELAY_GAME);
////        sensorManager.registerListener(eventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
//
//        mAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
//        mMagnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
//
//    }
//
//    private void adjustCompassPosition() {
//        int baseTopOffset = DefaultCompassTopConstraint;
//
//
//        if(pv_banner != null && pv_banner.getVisibility() == View.VISIBLE && v_bottom_view.getBottomHolderStage() != BottomHolderView.BottomHolderModeHiddenWithOverView){
//            baseTopOffset += 30;
//        }
//
//        switch (v_bottom_view.getBottomHolderStage()) {
//            case BottomHolderView.BottomHolderModeHidden:
//                mMapConfig.pmpSetCompassTopOffsetPx(baseTopOffset);
//                break;
//            case BottomHolderView.BottomHolderModeShowByPass:
//                mMapConfig.pmpSetCompassTopOffsetPx(baseTopOffset + 20);
//                break;
//            case BottomHolderView.BottomHolderModeShowNavigation:{
//                mMapConfig.pmpSetCompassTopOffsetPx(baseTopOffset + 8);
//            }
//            break;
//            case BottomHolderView.BottomHolderModeHiddenWithOverView: {
//                mMapConfig.pmpSetCompassTopOffsetPx(baseTopOffset + (int)(SearchOverView.OVERVIEW_BOTTOM_HOLDER_HEIGHT / deviceDensity * 2) );
//                break;
//            }
//
//            default:
//                mMapConfig.pmpSetCompassTopOffsetPx(baseTopOffset);
//                break;
//        }
//
//
//    }
//
//    protected float[] lowPass( float[] input, float[] output ) {
//        if ( output == null ) return input;
//
//        for ( int i=0; i<input.length; i++ ) {
//            output[i] = output[i] + 1 * (input[i] - output[i]);
//        }
//        return output;
//    }
//
//
//    private void showOverViewModeWithPOI(Pois selectedPOI){
//        PMPMapConfig mapConfig = mMapConfig;
//
//        mapConfig.pmpClearSearhResult();
//        int startPointID = -1;
//        this.selectedPOI = selectedPOI;
//
//        currentSearchResult = mapConfig.pmpSearchPossiblePathByPOI(startPointID, (int)selectedPOI.getId(), true);
//
//        showNativeOverViewWithPOI(selectedPOI);
//        showOverViewAppUI();
//
//    }
//
//    private void showNativeOverViewWithPOI(Pois selectedPOI){
//        float windowWidth = mFrameLayout.getWidth() / deviceDensity;
//        float windowHeight = mFrameLayout.getHeight()/ deviceDensity - SearchOverView.OVERVIEW_BOTTOM_HOLDER_HEIGHT - SearchOverView.OVERVIEW_TOP_HOLDER_HEIGHT;
//        float centreX=mFrameLayout.getX() + mFrameLayout.getWidth()  / 2 / deviceDensity;
//        float centreY=mFrameLayout.getY() + mFrameLayout.getHeight() / 2 / deviceDensity;
//
//        mMapConfig.pmpShowOverViewMode(centreX, centreY, windowWidth, windowHeight, (float)selectedPOI.getX(), (float)selectedPOI.getY(), true);
//    }
//
//    private void showOverViewAppUI(){
//        sov_overview.setVisibility(View.VISIBLE);
//        btn_switch_map.setVisibility(View.GONE);
//
//        Boolean networkEnable = isOnline();
//        Boolean bluetoothEnable = false;
//        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
//        if (mBluetoothAdapter == null) {
//            // Device does not support Bluetooth
//        } else {
//            bluetoothEnable = mBluetoothAdapter.isEnabled();
//        }
//
//        if(selectedPOI == null || selectedPOI.getNodeIds().size() == 0){
//            sov_overview.setErrorType(ErrorView.ErrorType_PathNotFound, new ErrorView.RetryCallback() {
//                @Override
//                public void onRetry() {
//                    showOverViewModeWithPOI(selectedPOI);
//                }
//            });
//        } else if (currentIndoorLocation == null) {
//            sov_overview.setShowRouteSelection(false);
//            if (currentOutdoorLocation != null){
//                LatLngBounds bounds = new LatLngBounds.Builder()
//                        .include(new LatLng(currentOutdoorLocation.getLatitude(),currentOutdoorLocation.getLongitude()))
//                        .include(new LatLng(selectedPOI.getMapLat(), selectedPOI.getMapLng()))
//                        .build();
//                if (outdoorMap != null && isOutdoorMapLoaded) {
//                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, (int) (30 * deviceDensity));
//                    outdoorMap.moveCamera(cameraUpdate);
//                }
//
//                updateOverviewText();
//                v_bottom_view.setIndoorLocationDetected(false);
//                v_bottom_view.showBottomHolderByMode(BottomHolderView.BottomHolderModeHiddenWithOverView);
//            }
//            if (!networkEnable) {
//                sov_overview.setErrorType(ErrorView.ErrorType_NetworkConnection, new ErrorView.RetryCallback() {
//                    @Override
//                    public void onRetry() {
//                        showOverViewModeWithPOI(selectedPOI);
//                    }
//                });
//            } else if (!bluetoothEnable && !networkEnable) {
//                sov_overview.setErrorType(ErrorView.ErrorType_BluetoothDisabled, new ErrorView.RetryCallback() {
//                    @Override
//                    public void onRetry() {
//                        showOverViewModeWithPOI(selectedPOI);
//                    }
//                });
//            } else if (GPSLocationManager.checkIfLocationPermissionAllowed(this)) {
//                if (currentOutdoorLocation == null) {
//                    sov_overview.setErrorType(ErrorView.ErrorType_NoGPSSignal, new ErrorView.RetryCallback() {
//                        @Override
//                        public void onRetry() {
//                            showOverViewModeWithPOI(selectedPOI);
//                        }
//                    });
//                }else {
//                    sov_overview.hideError();
//                }
//            } else {
//                sov_overview.setErrorType(ErrorView.ErrorType_LocationServiceDisabled, new ErrorView.RetryCallback(){
//
//                    @Override
//                    public void onRetry() {
//                        GPSLocationManager.requireLocationPermissionIfNeeded(PMPMapActivity.this);
//                    }
//                });
//            }
//
//        }else {
//            //force change to indoor mode if indoor signal is available
//            changeInOutDoorMode(true);
//            v_bottom_view.setIndoorLocationDetected(true);
//            v_bottom_view.showBottomHolderByMode(BottomHolderView.BottomHolderModeHiddenWithOverView);
//            if (currentSearchResult.size() != 0) {
//                sov_overview.hideError();
//                sov_overview.setShowRouteSelection(true);
//                sov_overview.setCurrentPathIndex(0);
//                updateOverviewText();
//            }else {
//                sov_overview.setErrorType(ErrorView.ErrorType_PathNotFound, new ErrorView.RetryCallback() {
//                    @Override
//                    public void onRetry() {
//                        showOverViewModeWithPOI(selectedPOI);
//                    }
//                });
//            }
//        }
//    }
//
//    public boolean isOnline() {
//        ConnectivityManager cm =
//                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo netInfo = cm.getActiveNetworkInfo();
//        return netInfo != null && netInfo.isConnectedOrConnecting();
//    }
//
//    private void hideOverViewUI(){
//        sov_overview.setVisibility(View.GONE);
//        btn_switch_map.setVisibility(View.VISIBLE);
//        v_bottom_view.showBottomHolderByMode(BottomHolderView.BottomHolderModeHidden);
//    }
//
//    private void updateOverviewText() {
//        sov_overview.setTotalNumberOfResult(currentSearchResult.size());
//        sov_overview.setRouteDistance( "(" + calculateDistance() + getString(R.string.PMPMAP_METER) + ")");
//        sov_overview.setTime( calculateTimeInMin() + getString(R.string.PMPMAP_MINS));
//        sov_overview.setDestName(PMPUtil.getLocalizedString(selectedPOI.getName()));
//    }
//
//    private void updateProximityViewIfNeeded(){
//        if(pv_banner.getVisibility() == View.VISIBLE){
//            PMPMapConfig config = mMapConfig;
//            int mode = config.pmpGetMapMode();
//            if(mode == PMPMapConfig.PMPMapModeBrowsing || mode == PMPMapConfig.PMPMapModeLocateMe){
//                pv_banner.setY(44 * deviceDensity);
//            }else{
//                pv_banner.setY(95 * deviceDensity);
//            }
//        }
//    }
//
//    private void updateGoogleNavigationPath(Pois dstPOI){
//        clearPolyLine();
//
//        Pois selectedPOI = dstPOI;
//        GPSLocationManager gpsLocationManager = GPSLocationManager.getSharedManager(this);
//
//        if(selectedPOI != null && gpsLocationManager.getLatestLocation(this) != null){
////            double destinationLat = selectedPOI.getOutdoorEntryPoint().getLatitude();
////            double destinationLong = selectedPOI.getOutdoorEntryPoint().getLongitude();
//            double destinationLat = 23.232312;
//            double destinationLong = 22.323223;
//
//            //navModeSelectView
//
//            Location myLocation = gpsLocationManager.getLatestLocation(this);
//            if (outdoorMap != null && isOutdoorMapLoaded) {
//                MarkerOptions myMarker = new MarkerOptions().position(new LatLng(myLocation.getLatitude(), myLocation.getLongitude())).icon(BitmapDescriptorFactory.fromResource(R.drawable.pin));
//                outdoorMap.addMarker(myMarker);
//
//                MarkerOptions dstMarker = new MarkerOptions().position(new LatLng(destinationLat, destinationLong)).icon(BitmapDescriptorFactory.fromResource(R.drawable.pin));
//                outdoorMap.addMarker(dstMarker);
//            }
//
//            //draw GoogleMap path...
//            String waypoints = "origin="
//                    + myLocation.getLatitude() + "," + myLocation.getLongitude()
//                    + "&destination=" + destinationLat + ","
//                    + destinationLong + "&travelMode" + travelingMode;
//
//            String sensor = "sensor=false";
//            String params = waypoints + "&" + sensor;
//            String output = "json";
//            String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + params;
//
//            StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
//                @Override
//                public void onResponse(String response) {
//                    JSONObject jObject;
//                    List<List<HashMap<String, String>>> routes = null;
//
//                    try {
//                        jObject = new JSONObject(response);
//                        PathJSONParser parser = new PathJSONParser();
//                        routes = parser.parse(jObject);
//
//                        ArrayList<LatLng> points = null;
//                        PolylineOptions polyLineOptions = null;
//
//                        if(routes.size() > 0){
//                            toggle.setVisibility(View.VISIBLE);
//                        }else{
//                            toggle.setVisibility(View.GONE);
//                        }
//
//                        // traversing through routes
//                        for (int i = 0; i < routes.size(); i++) {
//                            points = new ArrayList<LatLng>();
//                            polyLineOptions = new PolylineOptions();
//                            List<HashMap<String, String>> path = routes.get(i);
//
//                            for (int j = 0; j < path.size(); j++) {
//                                HashMap<String, String> point = path.get(j);
//
//                                double lat = Double.parseDouble(point.get("lat"));
//                                double lng = Double.parseDouble(point.get("lng"));
//                                LatLng position = new LatLng(lat, lng);
//
//                                points.add(position);
//                            }
//
//                            polyLineOptions.addAll(points);
//                            polyLineOptions.width(18);
//                            polyLineOptions.color(getResources().getColor(R.color.themeColor));
//                        }
//                        if (outdoorMap != null && isOutdoorMapLoaded) {
//                            polylines.add(outdoorMap.addPolyline(polyLineOptions));
//                        }
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    Log.e(TAG, error.getMessage());
//                }
//            });
//
//            RequestQueue queue = Volley.newRequestQueue(this);
//            queue.start();
//
//            queue.add(request);
//
//        }
//    }
//
//
//
//    private void showSearchTable(){
//        ObjectAnimator prepare = ObjectAnimator.ofFloat(lv_search, "translationY", -lv_search.getHeight());
//        prepare.setInterpolator(new AccelerateDecelerateInterpolator());
//        prepare.setDuration(0);
//        prepare.start();
//        lv_search.setVisibility(View.VISIBLE);
//
//        ObjectAnimator animateDown = ObjectAnimator.ofFloat(lv_search, "translationY", 0);
//        animateDown.setInterpolator(new AccelerateDecelerateInterpolator());
//        animateDown.setDuration(300);
//        animateDown.start();
//
//    }
//
//    private void hideSearchTable() {
//        ObjectAnimator animateDown = ObjectAnimator.ofFloat(lv_search, "translationY", -lv_search.getHeight());
//        animateDown.setInterpolator(new AccelerateDecelerateInterpolator());
//        animateDown.setDuration(300);
//        animateDown.addListener(new Animator.AnimatorListener() {
//            @Override
//            public void onAnimationStart(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                lv_search.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onAnimationCancel(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationRepeat(Animator animation) {
//
//            }
//        });
//        animateDown.start();
//    }
//
//    private String calculateDistance(){
//        PMPMapConfig config = mMapConfig;
//        return String.format("%.0f", config.pmpGetLastSearchRemainDistanceInMeter());
//
//    }
//
//    private String calculateTimeInMin(){
//        PMPMapConfig config = mMapConfig;
//
//
//        return String.format("%.0f", config.pmpGetLastSearchRemainTimeInSec() / 60);
//    }
//
//    private void forceSelectPOI(int poiID) {
//        Pois poi = PMPUtil.findPOIByPoiID(this, poiID);
//        if(poi == null){
//            return;
//        }
//
//        if(!isIndoorMode){
//            btn_switch_map.performClick();
//        }
//
//        onSelectPOI(poi);
//        float centerX = mFrameLayout.getX() / deviceDensity + mFrameLayout.getWidth() / 2 / deviceDensity;
//        float centerY = mFrameLayout.getY() / deviceDensity + mFrameLayout.getHeight() / 2 / deviceDensity;
//        mMapConfig.pmpForceSelectPOI(centerX, centerY, (int)poi.getId(), (float)poi.getX(),(float) poi.getY(), (int)poi.getMapId());
//
//    }
//
//    private void clearSearchResult(){
//        if(currentSearchResult != null){
//            currentSearchResult.clear();
//        }
//        clearPolyLine();
//    }
//
//    private void clearPolyLine(){
//        for(Polyline line : polylines)
//        {
//            line.remove();
//        }
//
//        polylines.clear();
//    }
//
//    @Override
//    public void onMapModeChanged(final int mode) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//
//                updateProximityViewIfNeeded();
//                switch (mode) {
//                    case PMPMapConfig.PMPMapModeBrowsing:
//                        if(isNavigating){
//                            //Re-center mode...
//
//                        }else{
//                            v_nav_top.setVisibility(View.GONE);
//
//                            if(!mMapConfig.pmpIsEnableOverViewMode()){
//                                btn_switch_map.setVisibility(View.VISIBLE);
//                            }
//                        }
//                        continueNavigation = false;
//                        break;
//
//                    case PMPMapConfig.PMPMapModeNavigating:
//                        if(currentSearchResult != null && currentSearchResult.size() > 0){
//                            v_bottom_view.showBottomHolderByMode(BottomHolderView.BottomHolderModeShowNavigation);
//                            btn_switch_map.setVisibility(View.GONE);
//                            v_nav_top.setVisibility(View.VISIBLE);
//                        }
//
//                        break;
//                    case PMPMapConfig.PMPMapModeLocateMe:
//                        continueNavigation = false;
//                        break;
//
//                }
//            }
//        });
//    }
//
//    private class SearchViewAdapter extends BaseAdapter {
//
//        @Override
//        public int getCount() {
//            return pois == null? 0 : pois.size();
//        }
//
//        @Override
//        public Pois getItem(int position) {
//            return pois.get(position);
//        }
//
//        @Override
//        public long getItemId(int position) {
//            return (long)pois.get(position).getId();
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            if(convertView == null){
//                convertView = LayoutInflater.from(PMPMapActivity.this).inflate(R.layout.pmp_cell_search, null);
//            }
//
//            TextView tv_name = (TextView)convertView.findViewById(R.id.tv_name);
//            tv_name.setText(PMPUtil.getLocalizedString(getItem(position).getName()));
//
//            return convertView;
//        }
//    }
//
//    private boolean startNavigation(Pois poi){
//        if (currentIndoorLocation == null) {
//            continueNavigation = true;
//            Uri gmmIntentUri = Uri.parse(String.format("google.navigation:q=%f,%f&mode=d",poi.getMapLat(),poi.getMapLng()));
//            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//            mapIntent.setPackage("com.google.android.apps.maps");
//            if (mapIntent.resolveActivity(getPackageManager()) != null) {
//                startActivity(mapIntent);
//            }else {
//                Uri uri = Uri.parse(String.format("https://maps.google.com?saddr=Current+Location&daddr=%f,%f", poi.getMapLat(), poi.getMapLng()));
//                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                startActivity(intent);
//            }
//        }else {
//            if(poi.getNodeIds().size() > 0){
//                v_nav_top.resetStatus();
////            currentSearchResult = mMapConfig.pmpSearchPossiblePath(-1, getPOINodeId(poi).get(0), true);
//                if(currentSearchResult != null && currentSearchResult.size() > 0){
//                    isNavigating = true;
//                    mMapConfig.pmpSetMapMode(PMPMapConfig.PMPMapModeNavigating);
//                    v_nav_top.setPoi(poi);
//
//                    //-------
//                    HashMap<String, Object> param = new HashMap<String, Object>();
//                    PathResult pathResult = currentSearchResult.get(0);
//                    StringBuffer sb = new StringBuffer();
//                    for (int i = 0; i < pathResult.getPathNodes().size(); i++) {
//                        PathNode node = pathResult.getPathNodes().get(i);
//                        sb.append(node.getCurrentVertex().getNode_id());
//                        if (i != pathResult.getPathNodes().size() - 1) {
//                            sb.append(',');
//                        }
//                    }
//                    param.put("poiId", (int)selectedPOI.getId());
//                    param.put("pathIds", sb.toString());
//                    param.put("distance", mMapConfig.pmpGetLastSearchTotalDistance());
//                    param.put("duration", mMapConfig.pmpGetLastSearchTotalTimeInSec());
//                    AnalyticsLogger.getInstance().logEvent("NAVIGATION_START", param);
//                    //-------
//
//                    return true;
//                }
//            }
//        }
//        return false;
//    }
//
//    private void updateUserGPSLocation(Location location){
//        if(location == null || gServicesStatus != ConnectionResult.SUCCESS){
//            return;
//        }
//        currentOutdoorLocation = location;
//        LatLng newLocation = new LatLng(location.getLatitude(), location.getLongitude());
//        if(curLocationMarker == null){
//            if (outdoorMap != null && isOutdoorMapLoaded) {
//                MarkerOptions option = new MarkerOptions().position(newLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.outdoor_indicator));
//                curLocationMarker = outdoorMap.addMarker(option);
//            }
//
//        }else{
//            curLocationMarker.setPosition(newLocation);
//        }
//
//        if (locateMe && outdoorMap != null && isOutdoorMapLoaded) {
//            //pretend to click locate me
//            isGoogleMapLocateMeClicked = true;
//            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(newLocation, GMAPDefaultZoom);
//            outdoorMap.animateCamera(update);
//        }
//    }
//
//
//    public void onSearchPOIClicked(Pois poi) {
//        if(poi == null)
//            return;
//
//        Maps map = PMPUtil.findFloorByPoi(this, poi);
//
//        //POI on static Map and out-of-range POI should go overview directly (Map logic defined at 20/7)
//        if((map != null && map.getStaticMap()) || poi.getNotCovered()){
//            showOverViewModeWithPOI(poi);
//        }else{
//            forceSelectPOI((int)poi.getId());
//        }
//    }
//
//    public void onSelectPOI(Pois poi) {
//        if (poi == null || mMapConfig.pmpIsEnableOverViewMode()) {
//            return;
//        }
//
//        if(selectedPOI != null && selectedPOI.equals(poi)){
//            //same POI
//            return;
//        }
//
//        selectedPOI = poi;
//        v_bottom_view.setPOI(poi);
//        v_bottom_view.showBottomHolderByMode(BottomHolderView.BottomHolderModeShowPOI);
//
//        LatLng latLng = new LatLng(selectedPOI.getMapLat(), selectedPOI.getMapLng());
//        //update poi pin
//        if (selectedPoiMarker != null) {
//            selectedPoiMarker.remove();
//        }
//        MarkerOptions selectedPoiMarkerOption = new MarkerOptions()
//                .position(latLng)
//                .icon(BitmapDescriptorFactory
//                        .fromResource(R.drawable.pin));
//        if (outdoorMap != null && isOutdoorMapLoaded) {
//            selectedPoiMarker = outdoorMap.addMarker(selectedPoiMarkerOption);
//
//            //update map camera
//            outdoorMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, GMAPDefaultZoom));
//        }
//
//    }
//
//    public void onDeselectPOI() {
//        if(selectedPOI != null && mMapConfig.pmpGetMapMode() != PMPMapConfig.PMPMapModeNavigating && !mMapConfig.pmpIsEnableOverViewMode() && !isOverViewModeEnable){
//            v_bottom_view.showBottomHolderByMode(BottomHolderView.BottomHolderModeHidden);
//            selectedPOI = null;
//            if (selectedPoiMarker != null) {
//                selectedPoiMarker.remove();
//                selectedPoiMarker = null;
//            }
//        }
//    }
//
//    public void onPOIDetailClicked(String poiID, MapState mapStage) {
//        if(PMPMapSDK.getLaunchDetailCallback() != null){
//            onBackPressed();
//            PMPMapSDK.getLaunchDetailCallback().onLaunchDetailClicked(selectedPOI.getExternalId(), mapStage);
//        }
//    }
//
//    public void launchPromotion(int proximityID, String promotionID, MapState mapStage) {
//        updateMapState();
//
//        if(PMPMapSDK.getProximityCallback() != null){
//            onBackPressed();
//            PMPMapSDK.getProximityCallback().onProximityClicked(proximityID,promotionID, mapStage);
//        }
//    }
//
//    private void setProximityBannerVisibility(int visibility){
//        pv_banner.setVisibility(visibility);
//        updateProximityViewIfNeeded();
//        adjustCompassPosition();
//    }
//
//    private void changeInOutDoorMode(boolean isChangeToIndoorMode) {
//        isIndoorMode = isChangeToIndoorMode;
//        if (isChangeToIndoorMode) {
//            btn_switch_map.setBackground(getResources().getDrawable(R.drawable.btn_outdoor));
//            mapFragment.getView().setVisibility(View.GONE);
//            if(toggle.getVisibility() == View.VISIBLE){
//                toggle.setVisibility(View.INVISIBLE);
//                toggle.setEnabled(false);
//            }
//            locateMeButton.setVisibility(View.INVISIBLE);
//        } else {
//            btn_switch_map.setBackground(getResources().getDrawable(R.drawable.btn_indoor));
//            mapFragment.getView().setVisibility(View.VISIBLE);
//            if(toggle.getVisibility() == View.INVISIBLE){
//                toggle.setVisibility(View.VISIBLE);
//                toggle.setEnabled(true);
//            }
//            locateMeButton.setVisibility(View.VISIBLE);
//        }
//    }
//
//    @Override
//    public void onBackPressed() {
//
//        finish();
//    }
//
//
// @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//     if(requestCode == CHANGE_SETTING){
//            mMapConfig.applyNewConfig();
//        }else if (requestCode == PMPSearchActivity.REQUEST_CODE) {
//            if (data != null) {
//                poiFromSearch = true;
//                int selectPOIId = data.getIntExtra(PMPSearchActivity.POI_IDS, 0);
//                Log.d(TAG, "Select POI Id:" + selectPOIId);
//
//                currentSearchResult = null;
//                Pois poi = null;
//                for (Pois p : PMPDataManager.getSharedPMPManager(this).getResponseData().getPois()) {
//                    if ((int)p.getId() == selectPOIId) {
//                        poi = p;
//                        break;
//                    }
//                }
//
//                hideSearchTable();
//
//
//                final Pois finalPOI = poi;
//                //Quick fix only... Bug(?)for cocos2dx when resume
//                new android.os.Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        onSearchPOIClicked(finalPOI);
//                    }
//                },500);
//            }
//
//        }
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case GPSLocationManager.PMP_PERMISSIONS_REQUEST_LOCATION: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//                    // permission was granted, yay! Do the
//                    // contacts-related task you need to do.
//                    PMPIndoorLocationManager.getSharedPMPManager(this).startDetection();
//                    GPSLocationManager.getSharedManager(this).requestLocationUpdates(this);
//                    updateUserGPSLocation(GPSLocationManager.getSharedManager(PMPMapActivity.this).getLatestLocation(this));
//                    if (sov_overview.getVisibility() == View.VISIBLE) {
//                        showOverViewModeWithPOI(selectedPOI);
//                    }
//
//                    BluetoothAdapter btAdapter = ((Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR1)
//                            ?((BluetoothManager)this.getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter()
//                            :(BluetoothAdapter.getDefaultAdapter()));
//
//                    if(btAdapter == null || btAdapter.getState() != BluetoothAdapter.STATE_ON){
//                        //Bluetooth is OFF
//                        showErrorHeader(PMPErrorType.BluetoothDisabled);
//                    } else if(!isOnline()) {
//                        showErrorHeader(PMPErrorType.NetworkConnection);
//                    } else {
//                        showErrorHeader(PMPErrorType.None);
//                    }
//
//                } else {
//                    showErrorHeader(PMPErrorType.LocationServiceDisabled);
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                }
//                return;
//            }
//
//            // other 'case' lines to check for other
//            // permissions this app might request
//        }
//    }
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        outdoorMap = googleMap;
//        outdoorMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
//            @Override
//            public void onMapLoaded() {
//                isOutdoorMapLoaded = true;
//                LatLngBounds bounds = new LatLngBounds.Builder()
//                        .include(new LatLng(22.288249, 114.210049))
//                        .include(new LatLng(22.285777, 114.213809))
//                        .build();
//
//                CameraUpdate update = CameraUpdateFactory.newLatLngBounds(bounds, (int) (30 * deviceDensity));
//                outdoorMap.animateCamera(update);
//            }
//        });
//        outdoorMap.getUiSettings().setIndoorLevelPickerEnabled(false);
//        outdoorMap.getUiSettings().setMapToolbarEnabled(false);
//
//        LatLngBounds bounds = new LatLngBounds.Builder()
//                .include(new LatLng(22.288249, 114.210049))
//                .include(new LatLng(22.285777, 114.213809))
//                .build();
////        LatLngBounds bounds = new LatLngBounds(new LatLng(22.285777, 114.210049), new LatLng(22.288249, 114.213809));
//
////        CameraUpdate update = CameraUpdateFactory.newLatLngBounds(bounds, (int) (30 * deviceDensity));
////        outdoorMap.animateCamera(update);
//        outdoorMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
//            @Override
//            public void onMapClick(LatLng latLng) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        isGoogleMapLocateMeClicked = false;
//                        PMPMapActivity.this.onDeselectPOI();
//                    }
//                });
//            }
//        });
//
//        BitmapDescriptor overlayBitmap = BitmapDescriptorFactory.fromResource(R.drawable.outline_building);
//        //hardcode value for TaiKoo place
////        LatLngBounds bounds = new LatLngBounds(new LatLng(22.288249, 114.210049), new LatLng(22.285777, 114.213809));
////        LatLngBounds bounds = new LatLngBounds(new LatLng(22.285777, 114.210049), new LatLng(22.288249, 114.213809));
//
//        GroundOverlayOptions overlay = new GroundOverlayOptions().image(overlayBitmap).positionFromBounds(bounds);
//        outdoorMap.addGroundOverlay(overlay);
//
//        MarkerOptions option = new MarkerOptions().position(SWIRE_PIN_LOCATION).icon(BitmapDescriptorFactory.fromResource(R.drawable.swire_pin));
//        outdoorMap.addMarker(option);
//
//        outdoorMap.setOnCameraMoveCanceledListener(new GoogleMap.OnCameraMoveCanceledListener() {
//            @Override
//            public void onCameraMoveCanceled() {
//                Log.e(TAG, "onCameraMoveCanceled");
//                isGoogleMapLocateMeClicked = false;
//                locateMe = false;
//                locateMeButton.setBackgroundResource(R.drawable.btn_locate);
//            }
//        });
//
//        outdoorMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
//            @Override
//            public void onCameraMove() {
//                Log.e(TAG, "onCameraMove");
//
//                boolean isInsideTaikooPlace = GeoCoordinate.isInsidePolygon(new PointF((float) outdoorMap.getCameraPosition().target.latitude, (float)outdoorMap.getCameraPosition().target.longitude), TAIKOOPLACE_REGION);
////    BOOL isInsideTaikooPlace = YES;
////                    NSLog(@"%@", isInsideTaikooPlace ? @"Inside": @"Outside");
//                if (!isIndoorMode && !isOverViewModeEnable) {
//                    btn_switch_map.setVisibility(!isInsideTaikooPlace || outdoorMap.getCameraPosition().zoom < 18.5 ? View.GONE : View.VISIBLE);
//                }
//            }
//        });
//
//        outdoorMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
//            @Override
//            public void onCameraMoveStarted(int i) {
//                Log.e(TAG, "onCameraMoveStarted");
//                locateMe = false;
//                locateMeButton.setBackgroundResource(R.drawable.btn_locate);
//            }
//        });
//
//    }
//
//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }
//
//    public Pois getSelectedPOI() {
//        return selectedPOI;
//    }
//
//    public void startNavigation() {
//        if (startNavigation(selectedPOI)) {
//            mMapConfig.pmpHideOverViewMode();
//            hideOverViewUI();
//        }
//
//    }
//
//    private void updateMapState() {
//        mapState.setIndoor(isIndoorMode);
//        nativeUpdateMapState();
//    }
//
//    public boolean isContinueNavigation() {
//        return continueNavigation;
//    }
//
//    public native void nativeSetLangId(int langId);
//    public native void nativeUpdateMapState();
//    public native void nativeSetMapState(MapState mapState);
//    public native void nativeSetScreenInfo(float density, int width, int height);
//
//    public static String getLocalizedString(String name){
//        if(getContext() != null){
//            int id = getContext().getResources().getIdentifier(name, "string", getContext().getPackageName());
//            return getContext().getString(id);
//        }
//        return "";
//    }
//
//    private void showErrorHeader(PMPErrorType errorType) {
//        errorHeaderView.setVisibility(View.VISIBLE);
//        switch (errorType) {
//            case NoGPSSignal: {
//                errorHeaderView.errorLabel.setText(getLocalizedString("PMPMAP_ERROR_HEADER_NO_GPS"));
//            } break;
//            case BluetoothDisabled: {
//                errorHeaderView.errorLabel.setText(getLocalizedString("PMPMAP_ERROR_HEADER_BLT_MSG"));
//            } break;
//            case NetworkConnection: {
//                errorHeaderView.errorLabel.setText(getLocalizedString("PMPMAP_ERROR_HEADER_NETWORK_CONNECTION"));
//            } break;
//            case LocationServiceDisabled: {
//                errorHeaderView.errorLabel.setText(getLocalizedString("PMPMAP_ERROR_HEADER_NO_LOCATION_SERVICE"));
//            } break;
//            case None:
//            default:
//                errorHeaderView.setVisibility(View.GONE);
//                break;
//        }
//    }
//}
