package com.pmp.mapsdk.app;

import java.io.Serializable;

/**
 * Created by Yu on 24/2/2017.
 */

public interface PMPPermissionListener extends Serializable {
    void permissionResult(boolean granted);
}
