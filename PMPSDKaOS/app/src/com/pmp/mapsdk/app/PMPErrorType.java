package com.pmp.mapsdk.app;

/**
 * Created by Yu on 8/8/16.
 */
public enum PMPErrorType {
    None,
    NetworkConnection,
    LocationServiceDisabled,
    PathNotFound,
    NoGPSSignal,
    BluetoothDisabled
}
