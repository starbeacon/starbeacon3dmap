package com.pmp.mapsdk.app;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.cms.model.PoiCategories;
import com.pmp.mapsdk.external.PMPMapSDK;
import com.pmp.mapsdk.utils.PMPUtil;

/**
 * Created by joeyyc on 30/11/2016.
 */

public class PMPSearchLandingPagerFragment extends Fragment {
    private final static String TAG = "SearchLandingPager";
    public static final String ARGS_POI_CATEGORIES = "ARGS_POI_CATEGORIES";

    private TabLayout tabLayout;
    private ViewPager pager;
    private TextView lblHeaderTitle;
    private ImageButton btnBack;
    private ImageButton btnBookmark;

    private PoiCategories poiCategory;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            poiCategory = (PoiCategories) bundle.getSerializable(ARGS_POI_CATEGORIES);
        }

        LinearLayout view = (LinearLayout) inflater.inflate(R.layout.pmp_search_view_pager, container, false);
        View header = view.findViewById(R.id.header);
        header.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());

        btnBack = (ImageButton) view.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        ImageButton btnMidRight = (ImageButton) view.findViewById(R.id.btnMidRight);
        btnMidRight.setVisibility(View.GONE);
        btnBookmark = (ImageButton) view.findViewById(R.id.btnMostRight);
        lblHeaderTitle = (TextView) view.findViewById(R.id.lblHeadTitle);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        pager = (ViewPager)view.findViewById(R.id.viewpager);
        pager.setAdapter(new LandingFragmentPagerAdapter(getFragmentManager(), getActivity()));

        // Give the TabLayout the ViewPager
        tabLayout = (TabLayout) view.findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(pager);

        lblHeaderTitle.setText(getResources().getText(R.string.PMPMAP_FILTER_SHOP_DINE));

        return view;
    }

    public class LandingFragmentPagerAdapter extends FragmentStatePagerAdapter {
        private Context context;

        public LandingFragmentPagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        @Override
        public int getCount() {
            return poiCategory.getSub_categories().size();
        }

        @Override
        public Fragment getItem(int position) {
            PoiCategories bCat = poiCategory.getSub_categories().get(position);
            Log.d(TAG, "cat:" + bCat);
            PMPSearchLandingListFragment fragment = new PMPSearchLandingListFragment();

            Bundle bundle = new Bundle();
            bundle.putSerializable(PMPSearchLandingListFragment.ARGS_BRANDCATEGORY, bCat);
            fragment.setArguments(bundle);

            return fragment;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            PoiCategories bCat = poiCategory.getSub_categories().get(position);

            return PMPUtil.getLocalizedString(bCat.getName());
        }
    }
}
