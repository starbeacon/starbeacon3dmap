package com.pmp.mapsdk.app;

import android.animation.LayoutTransition;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Space;
import android.widget.TextView;

import com.cherrypicks.pmpmap.CocosFragment;
import com.cherrypicks.pmpmap.Coord;
import com.cherrypicks.pmpmap.GPSLocationManager;
import com.cherrypicks.pmpmap.PMPAugmentedRealityCallback;
import com.cherrypicks.pmpmap.PMPCameraManager;
import com.cherrypicks.pmpmap.PMPConfigActivity;
import com.cherrypicks.pmpmap.PMPDataManager;
import com.cherrypicks.pmpmap.PMPIndoorLocationManager;
import com.cherrypicks.pmpmap.PMPMapConfig;
import com.cherrypicks.pmpmap.PMPMapController;
import com.cherrypicks.pmpmap.PMPMapControllerCallback;
import com.cherrypicks.pmpmap.analytics.AnalyticsLogger;
import com.cherrypicks.pmpmap.core.CoreEngine;
import com.cherrypicks.pmpmap.core.CoreEngineListener;
import com.cherrypicks.pmpmap.core.RouteStepIndicator;
import com.cherrypicks.pmpmap.datamodel.Edge;
import com.cherrypicks.pmpmap.sensor.MotionManager;
import com.cherrypicks.pmpmap.tts.TTSManager;
import com.cherrypicks.pmpmap.ui.ErrorHeaderView;
import com.cherrypicks.pmpmap.ui.PromotionType;
import com.cherrypicks.pmpmap.ui.ProximityView;
import com.cherrypicks.pmpmapsdk.BuildConfig;
import com.cherrypicks.pmpmapsdk.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pmp.mapsdk.app.adapter.FloorAdapter;
import com.pmp.mapsdk.app.adapter.POIGroupAdapter;
import com.pmp.mapsdk.app.view.ARNavigatingFooterView;
import com.pmp.mapsdk.app.view.FloorItemView;
import com.pmp.mapsdk.app.view.NavigatingFooterView;
import com.pmp.mapsdk.app.view.NavigatingHeaderView;
import com.pmp.mapsdk.app.view.NotificationView;
import com.pmp.mapsdk.app.view.POIGroupFooterView;
import com.pmp.mapsdk.app.view.POISelectionFooterView;
import com.pmp.mapsdk.app.view.RouteOverviewFooterView;
import com.pmp.mapsdk.app.view.RouteOverviewHeaderView;
import com.pmp.mapsdk.app.view.StartPointSelectionFooterView;
import com.pmp.mapsdk.app.view.TTSControlPanelView;
import com.pmp.mapsdk.cms.PMPPOIByDuration;
import com.pmp.mapsdk.cms.PMPProximityServerManager;
import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.cms.model.Areas;
import com.pmp.mapsdk.cms.model.Brands;
import com.pmp.mapsdk.cms.model.MapImage;
import com.pmp.mapsdk.cms.model.Maps;
import com.pmp.mapsdk.cms.model.Message;
import com.pmp.mapsdk.cms.model.PMPProximityServerManagerNotifier;
import com.pmp.mapsdk.cms.model.PoiCategories;
import com.pmp.mapsdk.cms.model.Pois;
import com.pmp.mapsdk.cms.model.PromotionMessage;
import com.pmp.mapsdk.cms.model.Promotions;
import com.pmp.mapsdk.cms.model.ResponseData;
import com.pmp.mapsdk.cms.model.Result;
import com.pmp.mapsdk.cms.model.SaveUserFlightResponse;
import com.pmp.mapsdk.cms.model.SaveUserLocationResponse;
import com.pmp.mapsdk.cms.model.Tags;
import com.pmp.mapsdk.external.PMPMapSDK;
import com.pmp.mapsdk.external.PMPNotification;
import com.pmp.mapsdk.location.GPSPositioningManager;
import com.pmp.mapsdk.location.PMPBeacon;
import com.pmp.mapsdk.location.PMPBeaconType;
import com.pmp.mapsdk.location.PMPLocation;
import com.pmp.mapsdk.location.PMPLocationManager;
import com.pmp.mapsdk.location.PMPLocationManagerError;
import com.pmp.mapsdk.utils.Comm;
import com.pmp.mapsdk.utils.PMPUtil;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

import static android.content.Context.MODE_PRIVATE;
import static com.cherrypicks.pmpmap.analytics.aws.mobilehelper.util.ThreadUtils.runOnUiThread;
import static com.cherrypicks.pmpmapsdk.R.id.menu_search;
import static com.cherrypicks.pmpmapsdk.R.id.pmp_map_holder;
import static com.pmp.mapsdk.location.PMPApplication.getPmpApplication;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class PMPMapFragment extends Fragment implements Toolbar.OnMenuItemClickListener,
        CoreEngineListener,
        PMPMapControllerCallback,
        PMPAugmentedRealityCallback,
        PMPIndoorLocationManager.IndoorLocationListener,
        TextToSpeech.OnInitListener,
        OnMapReadyCallback
{

    public interface PMPMapFragmentCallback {
        public void onCreate();
        public void onSetupContent();
    }

    public final static int PMPMapFragmentDisplayOptionDefault = 0;
    public final static int PMPMapFragmentDisplayOptionAroundMe = 1;
    public final static int PMPMapFragmentDisplayOptionAroundBoardingGate = 2;

    private final static String TAG = "Map Fragment";

    public final static String POI_IDS = "POI_IDS";
    public final static String POI_TITLE = "POI_TITLE";
    public final static String BRAND_ID = "BRAND_ID";
    public final static String POI_CATEGORY_ID = "POI_CATEGORY_ID";
    public final static String FLOOR_NAME = "FLOOR_NAME";

    public final static int PMPMapViewModeNormal = 0;
    public final static int PMPMapViewModePOIInfo = 1;
    public final static int PMPMapViewModeRouteOverview = 2;
    public final static int PMPMapViewModePOIGroupExpend = 3;
    public final static int PMPMapViewModePOIGroupCollapse = 4;
    public final static int PMPMapViewModeNavigating = 5;
    public final static int PMPMapViewModePOIGroupOnly = 6;
    public final static int PMPMapViewModePOIGroupWithPOIInfo = 7;
    public final static int PMPMapViewModePOISelection = 8;

    public final static int PMPMapHeaderNormal = 0;
    public final static int PMPMapHeaderTitleAndClose = 1;

    public final static String PMPMapViewBlockerNone = "0";
    public final static String PMPMapViewBlockerFollowPath = "1";
    public final static String PMPMapViewBlockerFaceDown = "2";
    public final static String PMPMapViewBlockerBypassed = "3";
    public final static String PMPMapViewBlockerTransport = "4";
    public final static String PMPMapViewBlockerOthers = "5";

    private int viewMode = PMPMapViewModeNormal;
    private int headerMode = PMPMapHeaderNormal;

    private CocosFragment cocosFragment;
    private CocosFragment.CocosFragmentCallback cocosFragmentCallback;

    private Toolbar toolbar;

    //Proximity broadcast
    private BroadcastReceiver proximityInzoneReceiver, proximityOutzoneReceiver, showNotificationReceiver;
    private ProximityView pv_banner;

    //header
    private RouteOverviewHeaderView routeOverviewHeaderView;
    private NavigatingHeaderView navigatingHeaderView;
    private ErrorHeaderView errorHeaderView;
    private ImageButton compassButton;

    //tts
    private  View ttsButton;
    private TTSControlPanelView ttsControlLayout;
    private FrameLayout ttsControlPanelFrame;
    boolean isARViewShowing = false;

    //footer
    private ListView floorSwitchListV;
    private FloorItemView routeOverviewFloorView;
    private ImageButton locateMeButton;
    private ImageButton switchMapButton;
    private ImageButton chooseStartPtButton;
    private Button recenterButton;
    private ImageButton startNavButton;
    private Button startNavRectButton;
    private View footerContentView;
    private POISelectionFooterView poiSelectionFooterView;
    private POIGroupFooterView poiGroupFooterView;
    private NavigatingFooterView navigatingFooterView;
    private ARNavigatingFooterView arNavigatingFooterView;
    private RouteOverviewFooterView routeOverviewFooterView;
    private StartPointSelectionFooterView startPointSelectionFooterView;
    private Space btnSpace;

    //selection header
    private View selectionHeaderView;
    //area
    private View areaView;
    //proximity
    private NotificationView notificationView;

    private boolean showSelectionHeader;
    private boolean showArea;
    private boolean showNotification;

    //blocker view
    private ImageView interactionBlockerIconImageView;
    private TextView interactionBlockerMessageLabel;
    private RelativeLayout interactionBlockerView;

    //data
    private List<Maps> floors;
    private FloorAdapter floorAdapter;
//    private PMPLocation indoorLocation;
    private Pois selectedPOI;
    private boolean showPOITable;
    private int routeStepIndex = 0;
    private int currentPathType = 0;

    private boolean isPathSearching;
    private boolean isChoosingStartingNotDestination;
    private Pois selectedStartingPOI;
    private Pois selectedDestinationPOI;
    private Pois selectedDestinationGatePOI;

    private boolean firstLocation = true;
    private static boolean setupCocos = false;
    PMPErrorType errorType = PMPErrorType.None;
    PMPMapFragmentCallback callback;

    private FrameLayout fragmentContainer;

    private AtomicBoolean isNavigatingToGate;
    public AtomicBoolean isShowCloseButtonOnTop;
    private boolean mapScaleIsSmallerThanDefault = true;
    private int currentNavigationType = 0;

    //AR Camera
    FrameLayout mapContainer;
    RelativeLayout cameraContainer;
    FrameLayout preview;
//    private Button btn_switch_map;
    private long lastOverlayTimeStamp;

    //dissmiss arrive destination message after 20 sec
    Timer dismissDestinationNoitceTimer;

    //TTS
    private TTSManager ttsManager;
    private  boolean shouldUnmuteTTSManagerOnResume;

    //analytics
    private boolean showPoiListFromExternal;
    private boolean arrivalMsgTriggered;
    private boolean isShowingTiltReminder;

    //GoogleMap
    private Marker selectedPoiMarker;
    private Marker currentPinView;
    private GoogleMap googleMapView;
    private boolean isShowingIndoorMap = true;
    private Location gpsLocation;
    private final float GMAPDefaultZoom = 16.2f;
    private SupportMapFragment mapFragment;


    private final BroadcastReceiver handleErrorReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            BluetoothAdapter btAdapter = ((Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR1)
                    ?((BluetoothManager)context.getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter()
                    :(BluetoothAdapter.getDefaultAdapter()));

            if(btAdapter == null || btAdapter.getState() != BluetoothAdapter.STATE_ON) {
                errorType = PMPErrorType.BluetoothDisabled;
            }else if (!GPSLocationManager.checkIfLocationPermissionAllowed(context)) {
                errorType = PMPErrorType.LocationServiceDisabled;
            }else if (!PMPUtil.isOnline(context)) {
                errorType = PMPErrorType.NetworkConnection;
            } else {
                errorType = PMPErrorType.None;
                showOutOfCoverageAreaIfNeeded();
            }
            /*
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)
                        == BluetoothAdapter.STATE_ON) {
                    // Bluetooth is connected, do handling here
                    if (!PMPUtil.isOnline(getActivity())) {
                        errorType = PMPErrorType.NetworkConnection;
                    } else if (!GPSLocationManager.checkIfLocationPermissionAllowed(context)) {
                        errorType = PMPErrorType.LocationServiceDisabled;
                    } else {
                        errorType = PMPErrorType.None;
                    }
                } else {
                    errorType = PMPErrorType.BluetoothDisabled;
                }
            }else if(LocationManager.PROVIDERS_CHANGED_ACTION.equals(action)){
                final LocationManager manager = (LocationManager) context.getSystemService( Context.LOCATION_SERVICE );
                if (manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) || manager.isProviderEnabled( LocationManager.NETWORK_PROVIDER )) {
                    errorType = PMPErrorType.None;
                } else {
                    errorType = PMPErrorType.LocationServiceDisabled;
                }
            }else if (action.equals("android.net.conn.CONNECTIVITY_CHANGE")) {

            }*/
            updateErrorHeader();
        }
    };

    public PMPMapFragment() {
        // Required empty public constructor
        isNavigatingToGate = new AtomicBoolean();
        isNavigatingToGate = new AtomicBoolean();
        isShowCloseButtonOnTop = new AtomicBoolean();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AnalyticsLogger.getInstance().logEvent("Main_Open_Map");

//        IntentFilter filter_proximity_inzone = new IntentFilter(PMPIndoorLocationManager.BROADCAST_PROXIMITY_ENTER_ZONE);
//        IntentFilter filter_proximity_outzone = new IntentFilter(PMPIndoorLocationManager.BROADCAST_PROXIMITY_OUT_ZONE);
        IntentFilter filter_notification = new IntentFilter(PMPMapSDK.BROADCAST_NOTIFICATION_ACTION);

//        proximityInzoneReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//
//                Promotions promotion = getPmpApplication().getCurrentPromotion();
//                if(promotion != null) {
//                    pv_banner.addPromotion(promotion);
//                }
////                int zoneID = intent.getIntExtra(PMPIndoorLocationManager.BROADCAST_ZONE_ID, -1);
////
////                Promotions promotion = PMPUtil.findPromotionByMajorID(getActivity(), zoneID);
////                if(promotion == null){
////                    return;
////                }
////
////                String key = PUSH_ALREAD_SHOW + promotion.getId();
////                boolean isPushAlreadyShown = PMPUtil.getSharedPreferences(context).getBoolean(key, false);
////                if(isPushAlreadyShown){
////                    return;
////                }
////
////
////                PMPUtil.getSharedPreferences(context).edit().putBoolean(key, true).commit();
////
////                if(promotion != null && promotion.getMessage().size() > 0 && promotion.getPromotionType() == Promotions.PROMOTION_TYPE_ZONAL){
////                    pv_banner.setPromotion(promotion);
////                    pv_banner.setMajorID(zoneID);
////                    setProximityBannerVisibility(View.VISIBLE);
////                }
//
//
//            }
//        };
//
//        proximityOutzoneReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                int zoneID = intent.getIntExtra(PMPIndoorLocationManager.BROADCAST_ZONE_ID, -1);
//
////                setProximityBannerVisibility(View.GONE);
//            }
//        };

        showNotificationReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "Notification!");
                PMPNotification notification = (PMPNotification) intent.getSerializableExtra(PMPMapSDK.NOTIFICATION_KEY);
                if(notification != null) {
                    pv_banner.addPromotion(notification);
                }
            }
        };

//        getActivity().registerReceiver(proximityInzoneReceiver, filter_proximity_inzone);
//        getActivity().registerReceiver(proximityOutzoneReceiver, filter_proximity_outzone);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(showNotificationReceiver, filter_notification);

        //apply default
        SharedPreferences preference = getContext().getSharedPreferences("PREF_PMP_LOCATION_SETTINGS", 0);

        CoreEngine.getInstance().setPathForeSeeingDistance(preference.getInt(getString(R.string.Preference_PathForeSeeingDistance), 20));
        GPSPositioningManager.shared().setUp(new GPSPositioningManager.LatLngHolder(22.301960, 113.917493),new GPSPositioningManager.LatLngHolder(22.316262, 113.912265),
                new GPSPositioningManager.LatLngHolder(22.311569, 113.948786),new GPSPositioningManager.LatLngHolder(22.326270, 113.943425),10752, 21504);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CoreEngine.getInstance().clearSearchResult();
        Configuration config = new Configuration(getResources().getConfiguration());
        switch (PMPMapSDK.getLangID()){
            case PMPMapSDK.Language_English:
                config.locale = Locale.ENGLISH ;
                break;
            case PMPMapSDK.Language_TraditionalChinese:
                config.locale = Locale.TRADITIONAL_CHINESE ;
                break;
            case PMPMapSDK.Language_SimplifiedChinese:
                config.locale = Locale.SIMPLIFIED_CHINESE ;
                break;
            case PMPMapSDK.Language_Japanese:
                config.locale = Locale.JAPANESE ;
                break;
            case PMPMapSDK.Language_Korean:
                config.locale = Locale.KOREAN ;
                break;
            default:
                config.locale = Locale.ENGLISH ;
                break;
        }
        getResources().updateConfiguration(config,getResources().getDisplayMetrics());

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.pmp_map_fragment, container, false);

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());


        //header
        routeOverviewHeaderView = (RouteOverviewHeaderView) view.findViewById(R.id.view_header_route_overview);
        routeOverviewHeaderView.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
        navigatingHeaderView = (NavigatingHeaderView) view.findViewById(R.id.view_header_navigating);
        navigatingHeaderView.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
        areaView = view.findViewById(R.id.area_view);
        areaView.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
        selectionHeaderView = view.findViewById(R.id.selection_header_view);
        selectionHeaderView.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
        RelativeLayout rl_header_holder = (RelativeLayout) view.findViewById(R.id.rl_header_holder);
        rl_header_holder.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());

        notificationView = (NotificationView) view.findViewById(R.id.notification_view);
        errorHeaderView = (ErrorHeaderView) view.findViewById(R.id.error_header);

        compassButton = (ImageButton) view.findViewById(R.id.btn_compass);
        compassButton.setImageResource(PMPMapSDK.getMapUISetting().getCompassImageResource());

//        ttsButton = view.findViewById(R.id.btn_tts);
//        ttsControlPanelFrame = (FrameLayout)view.findViewById(R.id.frame_layout_tts_control_panel);
//        ttsControlLayout = (TTSControlPanelView)view.findViewById(R.id.tts_contol_panel_view);
//        try {
//           getContext().getPackageManager().getPackageInfo(ttsManager.googleTTSEnginePackageName, 0);
//            ttsManager = new TTSManager(getActivity(), this);
//        } catch (PackageManager.NameNotFoundException e) {
//            ttsButton.setVisibility(View.INVISIBLE);
//        }


        floorSwitchListV = (ListView) view.findViewById(R.id.floor_switch_listview);
        ViewGroup.LayoutParams params =  floorSwitchListV.getLayoutParams();
        params.width = PMPMapSDK.getMapUISetting().getFloorSwitchWidth();
        floorSwitchListV.setLayoutParams(params);

        locateMeButton = (ImageButton) view.findViewById(R.id.btn_locate_me);
        recenterButton = (Button)view.findViewById(R.id.btn_recenter);
        recenterButton.setText(getLocalizedString("PMPMAP_NAV_RECENTER"));
        recenterButton.setTextColor(Color.parseColor("#878787"));
        startNavButton = (ImageButton) view.findViewById(R.id.btn_start_nav);
        startNavRectButton = (Button) view.findViewById(R.id.btn_rect_start_nav);
        startNavRectButton.setBackgroundResource(PMPMapSDK.getMapUISetting().getNavRectButtonImageResource());
        startNavRectButton.setTextColor(PMPMapSDK.getMapUISetting().getNavRectButtonTitleColor());
        startNavRectButton.setText(PMPMapSDK.getMapUISetting().getNavRectButtonTitle());

        startNavButton.setBackgroundResource(PMPMapSDK.getMapUISetting().getStartNavButtonImageResource());

        btnSpace = (Space) view.findViewById(R.id.btnSpace);
        chooseStartPtButton = (ImageButton)view.findViewById(R.id.btn_choose_start_pt);

        //footer
        footerContentView = view.findViewById(R.id.footer_content);
        poiSelectionFooterView = (POISelectionFooterView) view.findViewById(R.id.view_footer_poi_selection);
        poiGroupFooterView = (POIGroupFooterView)view.findViewById(R.id.view_footer_poi_group);
        navigatingFooterView = (NavigatingFooterView) view.findViewById(R.id.view_footer_navigating);
        arNavigatingFooterView = (ARNavigatingFooterView) view.findViewById(R.id.view_footer_navigating_ar);
        routeOverviewFooterView = (RouteOverviewFooterView) view.findViewById(R.id.view_footer_route_overview);
        routeOverviewFloorView = (FloorItemView) view.findViewById(R.id.route_overview_floor_holder);
        routeOverviewFloorView.setSelectedStatus(true);
        startPointSelectionFooterView = (StartPointSelectionFooterView) view.findViewById(R.id.view_footer_startpoint_selection);

        navigatingFooterView.btnShowAROption.setVisibility(View.GONE);
        navigatingFooterView.btnShowAROption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (navigatingFooterView.layoutAROptions.getVisibility() == View.VISIBLE) {
                    navigatingFooterView.layoutAROptions.setVisibility(View.GONE);
                } else {
                    navigatingFooterView.layoutAROptions.setVisibility(View.VISIBLE);

                    if ((MotionManager.getInstance(getActivity()).getGameRotationVectgorSensor() != null ||
                            MotionManager.getInstance(getActivity()).getRotationVectorSensor() != null)) {
                        navigatingFooterView.tvEnableARNav.setTextColor(Color.parseColor("#000000"));

                        navigatingFooterView.switchARNav.setChecked(PMPMapConfig.getSharedConfig().getEnableARNavigation(getContext()));

                    } else {
                        navigatingFooterView.tvEnableARNav.setTextColor(Color.parseColor("#8a000000"));
                    }
                    navigatingFooterView.tvEnableARNav.setText(getResources().getString(R.string.PMP_ENABLE_AR_NAV));

                }
            }
        });

        navigatingFooterView.btnEnableARNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((MotionManager.getInstance(getActivity()).getGameRotationVectgorSensor() != null ||
                        MotionManager.getInstance(getActivity()).getRotationVectorSensor() != null)) {
                    navigatingFooterView.switchARNav.setChecked(!navigatingFooterView.switchARNav.isChecked());

                    //save settings
                    PMPMapConfig.getSharedConfig().setEnableARNavigation(getContext(), navigatingFooterView.switchARNav.isChecked());

                    //show warning if necessary
                    if (navigatingFooterView.switchARNav.isChecked()) {
                        /*
                        if (!isARViewShowing) {
                            PMPLocationManager.getShared().setNumOfFloorBeaconDetection(8);
                            showAR();
                            isARViewShowing = true;
                        }*/

                        showARTiltReminder();
                    }
                } else {
                    //show confirm alert

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("").setMessage("This device does not support AR Navigation.");
                    builder.setPositiveButton("OK", null);
                    builder.show();
                }
            }
        });

        fragmentContainer = (FrameLayout)view.findViewById(R.id.fragment_container);
        mapContainer = (FrameLayout)view.findViewById(pmp_map_holder);

        //AR Camera
        cameraContainer = (RelativeLayout)view.findViewById(R.id.cameraContainer);

        //proixmity
        pv_banner = (ProximityView)view.findViewById(R.id.pv_banner);
        pv_banner.setProximityListener(new ProximityView.ProximityListener() {
            @Override
            public void onProximityBannerClicked() {
                if (pv_banner.getPromotion() instanceof Promotions) {
                    Promotions promotion = (Promotions) pv_banner.getPromotion();
                    Log.i("test","onBannerClicked Promotions url = " + PMPUtil.getLocalizedString(promotion.getPromotionMessages().get(0).getActionUrls()));

                    if(PMPUtil.getLocalizedString(promotion.getPromotionMessages().get(0).getDetails()).length() > 0){

                        PMPBrowserFragment browserFragment = new PMPBrowserFragment();
                        browserFragment.setContent(PMPUtil.getLocalizedString(promotion.getPromotionMessages().get(0).getDetails()),false);
                        getChildFragmentManager()
                                .beginTransaction()
                                .add(R.id.fragment_container, browserFragment)
                                .addToBackStack(null)
                                .commit();
                    } else if (PMPMapSDK.getOpenBrowserCallback() != null) {
                        PMPMapSDK.getOpenBrowserCallback().openBrowser(PMPUtil.getLocalizedString(promotion.getPromotionMessages().get(0).getActionUrls()));
                    }

                    HashMap<String, Object> params = new HashMap<>();
                    params.put("promotion_id", "" + (int)promotion.getId());
                    params.put("type", "" + promotion.getMessageType());
                    params.put("sent_major_id", "" + promotion.getReceivedMajor());

                    SaveUserFlightResponse response = PMPProximityServerManager.getShared(getContext()).getSaveUserFlightResponse();
                    if(response != null && response.getResult() != null){
                        Result result = response.getResult();

                        if(result.getGate() != null && result.getGate().length() > 0) params.put("gate_code",  result.getGate());
                        if(result.getFlightNo() != null && result.getFlightNo().length() > 0) params.put("flight_no", result.getFlightNo());
                        params.put("best_of_time", "" + (int)result.getBestOfTime());
                        params.put("travel_time", "" + (int)result.getTravelTime());
                    }

                    AnalyticsLogger.getInstance().logEvent("Detail_Promotion", params);
                }else if (pv_banner.getPromotion() instanceof PMPNotification) {
                    PMPNotification notification = (PMPNotification) pv_banner.getPromotion();
                    Log.i("test","onBannerClicked PMPNotification url = " + (notification.getUrl()));

                    if (PMPMapSDK.getOpenBrowserCallback() != null) {
                        PMPMapSDK.getOpenBrowserCallback().openBrowser(notification.getUrl());
                    }
                }else {
                    Log.i("test", "onBannerClicked Unknow");

                }
                //showPromotionDetail(pv_banner.getPromotion());


            }

            @Override
            public void onProximityBannerCancel() {

            }

            @Override
            public void onExploreAroundClicked() {
                pv_banner.setVisibility(View.GONE);
                selectedDestinationGatePOI = selectedDestinationPOI;
//                setViewMode(PMPMapViewModeNormal);
//                CoreEngine.getInstance().setMapMode(CoreEngine.MapModeBrowsing);
                showAroundGate(true);
                onClose();
            }
        });

        //blocker
        interactionBlockerView = (RelativeLayout) view.findViewById(R.id.rl_blocker);
        interactionBlockerIconImageView = (ImageView) view.findViewById(R.id.iv_blocker);
        interactionBlockerMessageLabel = (TextView) view.findViewById(R.id.tv_blocker);
        interactionBlockerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //prevent touch event...
                return true;
            }
        });

        setViewMode(PMPMapViewModeNormal);
        setShowSelectionHeader(false);
        setShowArea(false);

        poiSelectionFooterView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPOIDetail();
            }
        });

        View.OnClickListener onStartNavListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (viewMode) {
                    case PMPMapViewModePOIInfo:
                    case PMPMapViewModePOIGroupWithPOIInfo:
                    case PMPMapViewModePOISelection:
                        showRouteOverview();
                        break;
                    case PMPMapViewModeRouteOverview:
                        showNavigating();
                        if ((MotionManager.getInstance(getActivity()).getGameRotationVectgorSensor() != null ||
                                MotionManager.getInstance(getActivity()).getRotationVectorSensor() != null) &&
                                PMPMapConfig.getSharedConfig().getEnableARNavigation(getContext())) {
                            showARTiltReminder();

                            /*
                            PMPLocationManager.getShared().setNumOfFloorBeaconDetection(8);
                            showAR();
                            isARViewShowing = true;
                            */
                        }
                        break;
                    default:
                        break;
                }
            }
        };
        startNavButton.setOnClickListener(onStartNavListener);
        startNavRectButton.setOnClickListener(onStartNavListener);

        chooseStartPtButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRouteOverview();
            }
        });

        routeOverviewHeaderView.setOnCloseClickedListener(new Runnable() {
            @Override
            public void run() {
                notificationView.setNotificationType(NotificationView.NotificationTypeNone);
                onClose();
                showPOIGroupIfNeeded();
                stopAndResetDismissArrivedDestinationTimer();
                HashMap<String, Object> params = new HashMap<>();
                params.put("from_POI", selectedStartingPOI != null? ""+(int)selectedStartingPOI.getId() : "0");
                params.put("to_POI", selectedDestinationPOI != null? ""+(int)selectedDestinationPOI.getId() : "0");
                AnalyticsLogger.getInstance().logEvent("Navigate_Close_Preview", params);
            }
        });

        routeOverviewHeaderView.setOnChangeStartLocationListener(
            new Runnable() {
                 @Override
                 public void run() {
                     selectStartLocation();
                 }
             }
        );

        routeOverviewHeaderView.setOnChangeDestinationListener(
                new Runnable() {
                    @Override
                    public void run() {
                        selectDestination();
                    }
                }
        );

        routeOverviewHeaderView.setOnChangeDisabilityModeListener(
                new Runnable() {
                    @Override
                    public void run() {
                        PMPMapSDK.setIsDisability(routeOverviewHeaderView.getDisabilityModeSwitchState());
                        drawWalkingPath();
                    }
                }
        );

        navigatingHeaderView.setOnCloseClickedListener(new Runnable() {
            @Override
            public void run() {
                if (cameraContainer.getVisibility() == View.VISIBLE) {
                    setCameraVisibility(false);
                    exitAR();
                }
                onClose();
//                onLocateMeClicked();

                HashMap<String, Object> params = new HashMap<>();
                params.put("from_POI", selectedStartingPOI != null? ""+(int)selectedStartingPOI.getId() : "0");
                params.put("to_POI", selectedDestinationPOI != null? ""+(int)selectedDestinationPOI.getId() : "0");
                switch (currentNavigationType){
                    case CoreEngine.NavigationStepOnTheWay:
                        params.put("state", "navigating");
                        break;
                    case CoreEngine.NavigationStepArrived:
                        params.put("state", "destination_arrival");
                        break;
                    case CoreEngine.NavigationStepBypassed:
                        params.put("state", "bypass_destination");
                        break;
                }
                AnalyticsLogger.getInstance().logEvent("Navigate_Close", params);

                onClose();
            }
        });

        poiGroupFooterView.setOnExpandButtonClickedListener(new Runnable() {
            @Override
            public void run() {
                showPOIGroupList();
            }
        });
        poiGroupFooterView.setOnCollapseEventListener(new Runnable() {
            @Override
            public void run() {
                setViewMode(PMPMapViewModePOIGroupCollapse);
            }
        });
        locateMeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLocateMeClicked();
            }
        });
        compassButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CoreEngine.getInstance().getMapMode() == CoreEngine.MapModeNavigating) {
                    if (CoreEngine.getInstance().isCompassEnable()) {
                        CoreEngine.getInstance().setCompassEnable(false);
                    }else {
                        CoreEngine.getInstance().setCompassEnable(true);
                    }
                }else {
                    if (!CoreEngine.getInstance().isCompassEnable()) {
                        Coord coord = PMPMapController.getInstance().getMapCoord();
                        coord.rotation = 0;
                        //coord.scale = (float) PMPDataManager.getSharedPMPManager(null).getResponseData().getMapDefaultZoom();
                        PMPMapController.getInstance().setMapCoord(coord);
                    }
                }

            }
        });

//        ttsButton.setOnClickListener(
//                new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (ttsControlPanelFrame.getVisibility() != View.VISIBLE) {
//                            ttsControlPanelFrame.setVisibility(View.VISIBLE);
//                        } else {
//                            ttsControlPanelFrame.setVisibility(View.INVISIBLE);
//                        }
//                    }
//                }
//        );

        recenterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CoreEngine.getInstance().getIndoorLocation() == null) {
                    return;
                }
                int fromFloorID = 0;
                int from_x = 0;
                int from_y = 0;
                if(floorAdapter != null){
                    fromFloorID = (int)floorAdapter.getSelectedMap().getId();
                    from_x = PMPMapController.getInstance().getScreenCenterX();
                    from_y = PMPMapController.getInstance().getScreenCenterY();
                }

                int curMapId = Integer.parseInt(CoreEngine.getInstance().getIndoorLocation().getName());
                Maps map = null;
                for (Maps m : PMPDataManager.getSharedPMPManager(null).getResponseData().getMaps()) {
                    if (((int)m.getId()) == curMapId) {
                        map = m;
                        break;
                    }
                }
                if (floorAdapter != null) {
                    if (floorAdapter.getSelectedMap().getId() != curMapId) {
                        setSelectedMap(map, false);
                    }
                }
                if (CoreEngine.getInstance().isRecenterModeEnable()) {
                    CoreEngine.getInstance().setRecenterModeEnable(false);
                }else {
                    CoreEngine.getInstance().setRecenterModeEnable(true);
                }

                Map<String, Object> params = new HashMap<>();
                params.put("from_map", ""+fromFloorID);
                params.put("to_map", floorAdapter != null? "" + (int)floorAdapter.getSelectedMap().getId() : "0");
                params.put("from_x", from_x);
                params.put("from_y", from_y);
                params.put("to_x", ""+PMPMapController.getInstance().getScreenCenterX());
                params.put("to_y", ""+PMPMapController.getInstance().getScreenCenterY());
                AnalyticsLogger.getInstance().logEvent("Navigate_Recenter", params);
            }
        });
        poiSelectionFooterView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPOIDetail();
            }
        });
        updateToolbar();

        //setupTest(view);
        if (!BuildConfig.PROD) {
            toolbar.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    PMPMapConfig.getSharedConfig(getActivity());
                    Intent goSettingPage = new Intent(getActivity(), PMPConfigActivity.class);
                    startActivityForResult(goSettingPage, 2);
                    return true;
                }
            });
        }


        routeOverviewFooterView.setOnNextClickListner(new Runnable() {
            @Override
            public void run() {
                routeStepIndex++;
                if (routeStepIndex >= CoreEngine.getInstance().getRouteStepIndicators().length - 2) {
                    routeStepIndex = CoreEngine.getInstance().getRouteStepIndicators().length - 2;
                }
                CoreEngine coreEngine = CoreEngine.getInstance();
                RouteStepIndicator[] indicators = coreEngine.getRouteStepIndicators();
                RouteStepIndicator step1 = indicators[routeStepIndex];
                RouteStepIndicator step2 = indicators[routeStepIndex + 1];
                if (step1.getPathType() == CoreEngine.PathTypeAPM &&
                        step2.getPathType() == CoreEngine.PathTypeAPM) {
                    this.run();
                    return;
                }
                updateRouteStepButtons();
                moveToCurrentRouteStep();

            }
        });

        routeOverviewFooterView.setOnPreviousClickListener(new Runnable() {
            @Override
            public void run() {
                routeStepIndex--;
                boolean needOverview = needOverview();
                int firstIdx = 0;
                if (needOverview) {
                    firstIdx = -1;
                }
                if (routeStepIndex < firstIdx) {
                    routeStepIndex = firstIdx;
                }
                CoreEngine coreEngine = CoreEngine.getInstance();
                if (routeStepIndex >= 0) {
                    RouteStepIndicator[] indicators = coreEngine.getRouteStepIndicators();
                    RouteStepIndicator step1 = indicators[routeStepIndex];
                    RouteStepIndicator step2 = indicators[routeStepIndex + 1];
                    if (step1.getPathType() == CoreEngine.PathTypeAPM &&
                            step2.getPathType() == CoreEngine.PathTypeAPM) {
                        this.run();
                        return;
                    }
                }
                updateRouteStepButtons();
                moveToCurrentRouteStep();
            }
        });

        LayoutTransition layoutTransition = ((ViewGroup)view).getLayoutTransition();
        layoutTransition.enableTransitionType(LayoutTransition.CHANGING);
        GPSLocationManager.getSharedManager(getActivity()).setGpsLocationListener(new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                updateUserGPSLocation(location);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        });


        Log.v(TAG,"on create view");
        if (getArguments() != null) {
            Bundle args = getArguments();
            String[] poiIds = args.getStringArray(POI_IDS);
            String brandId = args.getString(BRAND_ID);
            String poiCategoryId = args.getString(POI_CATEGORY_ID);
            if ((poiIds != null && poiIds.length !=0) || brandId != null || poiCategoryId != null) {
                setViewMode(PMPMapViewModePOIGroupOnly);
            }
        }

        if (callback != null) {
            callback.onCreate();
        }

//        if (PMPMapController.getInstance().isMapSceneInitated() &&
//                PMPDataManager.getSharedPMPManager(null).getResponseData() != null) {
//            setupContent();
//            showPOIGroupIfNeeded();
//        }

        setTabBarVisible(true);
        setUpGoogleMapIfNeeded(view);

        return view;
    }

    private void setUpGoogleMapIfNeeded(View v) {
        if(PMPMapSDK.getConfiguration().isShouldEnableGoogleMap()){
            switchMapButton = (ImageButton) v.findViewById(R.id.btn_switch_map);
            switchMapButton.setVisibility(View.VISIBLE);
            switchMapButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setIsShowingIndoorMap(!isShowingIndoorMap);
                }
            });

            mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);

            setIsShowingIndoorMap(false);
        }
        if(PMPMapSDK.getConfiguration().isShouldEnableGoogleMap() || PMPMapSDK.getConfiguration().isShouldEnableGPSPositioning()){
            GPSLocationManager.getSharedManager(getActivity()).requestLocationUpdates(getActivity());
        }
    }

    private void setIsShowingIndoorMap(boolean isShowingIndoorMap){
        this.isShowingIndoorMap = isShowingIndoorMap;

        mapFragment.getView().setVisibility(isShowingIndoorMap? View.GONE : View.VISIBLE);

        mapContainer.setVisibility(isShowingIndoorMap? View.VISIBLE : View.GONE);
        compassButton.setVisibility(isShowingIndoorMap? View.VISIBLE : View.GONE);
        floorSwitchListV.setVisibility(isShowingIndoorMap? View.VISIBLE : View.GONE);

        if(isShowingIndoorMap){
            switchMapButton.setBackground(getResources().getDrawable(PMPMapSDK.getMapUISetting().getOutdoorButtonImageResource()));
        }else{
            switchMapButton.setBackground(getResources().getDrawable(PMPMapSDK.getMapUISetting().getIndoorButtonImageResource()));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getArguments() != null) {
            Bundle args = getArguments();
            int displayOption = args.getInt("display_option",0);
            if(displayOption == PMPMapFragmentDisplayOptionAroundMe || displayOption == PMPMapFragmentDisplayOptionAroundBoardingGate){
                showSearch();
            }
        }
        if(CoreEngine.getInstance().getMapMode() == CoreEngine.MapModeUnknown) {
            CoreEngine.getInstance().setMapMode(CoreEngine.MapModeBrowsing);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.v(TAG,"on Resume");
        PMPIndoorLocationManager.getSharedPMPManager(getActivity()).startDetection();
        CoreEngine.getInstance().addListener(this);
        PMPMapController.getInstance().addCallback(this);
        PMPMapController.getInstance().setCocosFragment(cocosFragment);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(handleErrorReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(handleErrorReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(handleErrorReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
//        final Runnable setupContentRunnable = new Runnable() {
//            @Override
//            public void run() {
////                if (CoreEngine.getInstance().getMapMode() != CoreEngine.MapModeNavigating) {
////                    Log.d(TAG, "Reset 2d map");
////                    if (nativeShowingARScene()) {
////                        PMPMapController.getInstance().replaceSceneWithMapScene();
////                    }
////                }
//
//                if (PMPDataManager.getSharedPMPManager(getActivity()).getResponseData() != null) {
//                    boolean needRestPOIGroup = floorAdapter != null;
//                    setupContent();
//                    if (needRestPOIGroup) {
//                        showPOIGroupIfNeeded();
//                    }
//                    showOutOfCoverageAreaIfNeeded();
//                }
//            }
//        };
        if (cocosFragment != null) {
//            if (cocosFragment.getGLSurfaceView() == null) {
//                cocosFragment.setCallback(new CocosFragment.CocosFragmentCallback() {
//                    @Override
//                    public void onCreate(CocosFragment fragment) {
//                        if (cocosFragmentCallback != null) {
//                            cocosFragmentCallback.onCreate(fragment);
//                        }
//                    }
//
//                    @Override
//                    public void surfaceCreated(final SurfaceHolder holder) {
//
//                        if (cocosFragmentCallback != null) {
//                            cocosFragmentCallback.surfaceCreated(holder);
//                        }
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                setupContentRunnable.run();
//                            }
//                        }, 700);
//                    }
//
//                    @Override
//                    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
//                        if (cocosFragmentCallback != null) {
//                            cocosFragmentCallback.surfaceChanged(holder, format, width, height);
//                        }
//                    }
//
//                    @Override
//                    public void surfaceDestroyed(SurfaceHolder holder) {
//                        if (cocosFragmentCallback != null) {
//                            cocosFragmentCallback.surfaceDestroyed(holder);
//                        }
//                    }
//                });
//            }else {
//
//            }
            if (PMPMapController.getInstance().isMapSceneInitated()) {
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        getActivity().runOnUiThread(setupContentRunnable);
//                    }
//                }, 500);


            }else {
                /*
                PMPMapController.getInstance().addCallback(new PMPMapControllerCallback() {
                    @Override
                    public void onPOISelect(int poiId) {

                    }

                    @Override
                    public void onDeselectAllPOI() {

                    }

                    @Override
                    public void onMapCoordUpdate(Coord coord) {

                    }

                    @Override
                    public void onMapSceneInitated() {
                        setupContentRunnable.run();
                        PMPMapController.getInstance().removeCallback(this);
                    }
                });*/
            }

        }


        GPSLocationManager.checkIfLocationPermissionAllowed(getActivity());

        //------------Load default ---------
        Resources resources = this.getResources();
        SharedPreferences preferences = getActivity().getSharedPreferences("PREF_PMP_LOCATION_SETTINGS", 0);
        SharedPreferences.Editor editor = preferences.edit();

        if(!preferences.contains(resources.getString(R.string.Preference_Debug))){
            editor.putBoolean(resources.getString(R.string.Preference_Debug), false);

            //This cmenu_map_filteran also mean first time open app...
            PMPIndoorLocationManager locationManager = PMPIndoorLocationManager.getSharedPMPManager(getActivity());
            locationManager.setEnableLowPassFilter(true);
            locationManager.setEnableMaxNumOfBeaconsDetection(true);
            locationManager.setEnableBeaconAccuracyFilter(true);
            locationManager.setEnableFloorSwitchingFilters(true);
            locationManager.setLowPassFilterFactor(0.5f);
            locationManager.setMaxNumOfBeaconsDetection(10);
            locationManager.setBeaconAccuracyFilter(30);
            locationManager.setEnableWalkingDetection(true);
            locationManager.setEnableFloorSwitchingFilters(true);
        }

        if (!preferences.contains(resources.getString(R.string.Preference_DestinationThreshold))) {
            editor.putFloat(resources.getString(R.string.Preference_DestinationThreshold), CoreEngine.getInstance().getDestinationThreshold());
        }
        if (!preferences.contains(resources.getString(R.string.Preference_PassDestinataionThreshold))) {
            editor.putFloat(resources.getString(R.string.Preference_PassDestinataionThreshold), CoreEngine.getInstance().getBypassDestinationThreshold());
        }
        if (!preferences.contains(resources.getString(R.string.Preference_RerouteThreshold))) {
            editor.putFloat(resources.getString(R.string.Preference_RerouteThreshold), CoreEngine.getInstance().getRerouteThreshold());
        }

        editor.commit();
//        //------------**END Load default** ---------
//        if(ttsManager != null && shouldUnmuteTTSManagerOnResume){
//            shouldUnmuteTTSManagerOnResume = false;
//            ttsManager.mute = false;
//        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        attachCocosFragment();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(handleErrorReceiver);
        PMPMapController.getInstance().setCocosFragment(null);
        PMPMapController.getInstance().removeCallback(this);
//        if(ttsManager != null && !ttsManager.mute){
//            shouldUnmuteTTSManagerOnResume = true;
//            ttsManager.mute = true;
//        }

    }
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, "onDestroyView");
        //clear status
        if (googleMapView != null) {
            googleMapView.clear();
        }
        AnalyticsLogger.getInstance().logEvent("Main_Close_Map");
        CoreEngine.getInstance().setMapMode(CoreEngine.MapModeUnknown);
        CoreEngine.getInstance().removeListener(this);
        stopAndResetDismissArrivedDestinationTimer();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        if(ttsManager != null) {
//            ttsManager.shutDownTTSObject();
//        }
//        getActivity().unregisterReceiver(proximityInzoneReceiver);
//        getActivity().unregisterReceiver(proximityOutzoneReceiver);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(showNotificationReceiver);
    }

    public int getViewMode() {
        return viewMode;
    }

    public void setViewMode(int viewMode) {
        this.viewMode = viewMode;
        if(viewMode != PMPMapViewModeNormal) {
            if (floorSwitchListV.getVisibility() == View.VISIBLE) {
                if (mapScaleIsSmallerThanDefault) {
                    floorSwitchListV.setVisibility(View.INVISIBLE);
                } else {
                    floorSwitchListV.setVisibility(View.VISIBLE);
                }
            }
            if (floorAdapter != null)
                floorSwitchListV.setVisibility(View.VISIBLE);
        }
        RelativeLayout.LayoutParams locateMeLayoutParams = (RelativeLayout.LayoutParams)locateMeButton.getLayoutParams();//new RelativeLayout.LayoutParams(locateMeButton.getLayoutParams());
        locateMeLayoutParams.removeRule(RelativeLayout.ABOVE);
        int locateMeAboveElementId = footerContentView.getId();

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        RelativeLayout.LayoutParams floorSwitchLayoutParams = (RelativeLayout.LayoutParams)floorSwitchListV.getLayoutParams();
        floorSwitchLayoutParams.setMargins(((int)(25*metrics.densityDpi)/DisplayMetrics.DENSITY_DEFAULT),0,0,((int)(16*metrics.densityDpi)/DisplayMetrics.DENSITY_DEFAULT));

        routeOverviewHeaderView.setVisibility(View.GONE);
        navigatingHeaderView.setVisibility(View.GONE);

        poiSelectionFooterView.setVisibility(View.GONE);
        poiGroupFooterView.setVisibility(View.GONE);
        navigatingFooterView.setVisibility(View.GONE);
        arNavigatingFooterView.setVisibility(View.GONE);
        routeOverviewFooterView.setVisibility(View.GONE);
        startPointSelectionFooterView.setVisibility(View.GONE);
        toolbar.setVisibility(View.VISIBLE);
//        btn_switch_map.setVisibility(View.GONE);

        routeOverviewFloorView.setVisibility(View.GONE);
        recenterButton.setVisibility(View.GONE);


        if(getPmpApplication().getCurrentPromotion() != null){
            pv_banner.showPromotionIfNeeded();
        }

        setShowSelectionHeader(false);
        int menuId = R.menu.pmp_map_toolbar_default;
        switch (viewMode) {
            case PMPMapViewModeNormal:
                if (isShowCloseButtonOnTop.get()) {
                    menuId = R.menu.pmp_map_toolbar_close;
                }
                locateMeAboveElementId = btnSpace.getId();
                chooseStartPtButton.setVisibility(View.GONE);
                startNavButton.setVisibility(View.GONE);
                locateMeButton.setVisibility(View.VISIBLE);
                if (isNavigatingToGate.get()) {
                    if (pv_banner.isShowExploreAround()) {
                        pv_banner.setVisibility(View.GONE);
                    }
                    isNavigatingToGate.set(false);
                }
                pv_banner.removePromotion(PromotionType.Gate);
                break;

            case PMPMapViewModePOIInfo:
                if (isShowCloseButtonOnTop.get()) {
                    menuId = R.menu.pmp_map_toolbar_close;
                }
                locateMeAboveElementId = startNavButton.getId();
                chooseStartPtButton.setVisibility(View.GONE);
                poiSelectionFooterView.setVisibility(View.VISIBLE);

                if(PMPMapSDK.getMapUISetting().getNavButtonMode() == PMPMapSDK.MapUISetting.PMPNavButtonMode_Rectangle_Btn){
                    startNavRectButton.setVisibility(View.VISIBLE);
                }else{
                    startNavButton.setVisibility(View.VISIBLE);
                }
                locateMeButton.setVisibility(View.VISIBLE);
                break;
            case PMPMapViewModeRouteOverview:
                locateMeButton.setVisibility(View.VISIBLE);
                floorSwitchListV.setVisibility(View.GONE);
                toolbar.setVisibility(View.GONE);
                chooseStartPtButton.setVisibility(View.GONE);
                startNavButton.setVisibility(View.VISIBLE);
                routeOverviewHeaderView.setVisibility(View.VISIBLE);
                locateMeAboveElementId = startNavButton.getId();
                routeOverviewFooterView.setVisibility(View.VISIBLE);
                routeOverviewFloorView.setVisibility(View.VISIBLE);
                pv_banner.setVisibility(View.GONE); //OverView hide the proximity banner
                break;
            case PMPMapViewModePOIGroupExpend:
                locateMeAboveElementId = btnSpace.getId();
                menuId = R.menu.pmp_map_toolbar_close;
                chooseStartPtButton.setVisibility(View.GONE);
                locateMeButton.setVisibility(View.GONE);
                startNavButton.setVisibility(View.GONE);
                floorSwitchListV.setVisibility(View.GONE);
                poiGroupFooterView.setVisibility(View.VISIBLE);
                poiGroupFooterView.setExpaned(true);
//                navigatingHeaderViewFollowRoute.setVisibility(View.GONE);
                floorSwitchLayoutParams.setMargins(((int)(25*metrics.densityDpi)/DisplayMetrics.DENSITY_DEFAULT),0,0,((int)(60*metrics.densityDpi)/DisplayMetrics.DENSITY_DEFAULT));
                break;
            case PMPMapViewModePOIGroupCollapse:
                menuId = R.menu.pmp_map_toolbar_close;
                if (isPathSearching) {
                    locateMeButton.setVisibility(View.GONE);
                } else {
                    locateMeButton.setVisibility(View.GONE);
                }
                chooseStartPtButton.setVisibility(View.GONE);
                startNavButton.setVisibility(View.GONE);
                poiGroupFooterView.setVisibility(View.VISIBLE);
                poiGroupFooterView.setExpaned(false);
//                navigatingHeaderViewFollowRoute.setVisibility(View.GONE);
                floorSwitchLayoutParams.setMargins(((int)(20*metrics.densityDpi)/DisplayMetrics.DENSITY_DEFAULT),0,0,((int)(60*metrics.densityDpi)/DisplayMetrics.DENSITY_DEFAULT));
                break;
            case PMPMapViewModeNavigating:
                if (isARViewShowing) {
                    recenterButton.setVisibility(View.GONE);
                    floorSwitchListV.setVisibility(View.INVISIBLE);

                    navigatingHeaderView.setVisibility(View.GONE);
                    arNavigatingFooterView.setVisibility(View.VISIBLE);
                } else {
                    recenterButton.setVisibility(View.VISIBLE);
                    if (!mapScaleIsSmallerThanDefault) {
                        floorSwitchListV.setVisibility(View.VISIBLE);
                    }

                    navigatingHeaderView.setVisibility(View.VISIBLE);
                    navigatingFooterView.setVisibility(View.VISIBLE);
                }
                toolbar.setVisibility(View.GONE);
                locateMeAboveElementId = btnSpace.getId();
                locateMeButton.setVisibility(View.VISIBLE);
                startNavButton.setVisibility(View.GONE);
                navigatingHeaderView.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
                chooseStartPtButton.setVisibility(View.GONE);
//                btn_switch_map.setVisibility(View.VISIBLE);
                break;
            case PMPMapViewModePOIGroupWithPOIInfo:
                menuId = R.menu.pmp_map_toolbar_close;
                locateMeAboveElementId = startNavButton.getId();
                poiSelectionFooterView.setVisibility(View.VISIBLE);
                chooseStartPtButton.setVisibility(View.GONE);

                if (isPathSearching) {
                    locateMeButton.setVisibility(View.GONE);
                    startNavButton.setVisibility(View.GONE);
                    poiSelectionFooterView.setVisibility(View.GONE);
                    chooseStartPtButton.setVisibility(View.VISIBLE);
                    startPointSelectionFooterView.setVisibility(View.VISIBLE);
                } else {
                    locateMeButton.setVisibility(View.VISIBLE);
                    startNavButton.setVisibility(View.VISIBLE);
                    poiSelectionFooterView.setVisibility(View.VISIBLE);
                    chooseStartPtButton.setVisibility(View.GONE);
                    startPointSelectionFooterView.setVisibility(View.GONE);
                }

                break;
            case PMPMapViewModePOIGroupOnly:
                locateMeAboveElementId = btnSpace.getId();
                locateMeButton.setVisibility(View.VISIBLE);
                startNavButton.setVisibility(View.GONE);
                menuId = R.menu.pmp_map_toolbar_close;
                chooseStartPtButton.setVisibility(View.GONE);
                break;
            case PMPMapViewModePOISelection:
                setShowSelectionHeader(true);
                menuId = R.menu.pmp_map_toolbar_close;
                locateMeButton.setVisibility(View.GONE);
                startNavButton.setVisibility(View.GONE);
                if (selectedPOI != null) {
                    startPointSelectionFooterView.setVisibility(View.VISIBLE);
                    chooseStartPtButton.setVisibility(View.VISIBLE);
                } else {
                    chooseStartPtButton.setVisibility(View.GONE);
                }
                break;
            default:
                locateMeAboveElementId = btnSpace.getId();
                chooseStartPtButton.setVisibility(View.GONE);
                locateMeButton.setVisibility(View.VISIBLE);
                startNavButton.setVisibility(View.GONE);
                //floorSwitchListV.setVisibility(View.VISIBLE);
                break;
        }
        if (locateMeAboveElementId != 0) {
            locateMeLayoutParams.addRule(RelativeLayout.ABOVE, locateMeAboveElementId);
        }

        toolbar.getMenu().clear();
        toolbar.inflateMenu(menuId);
        if (menuId == R.menu.pmp_map_toolbar_close) {
            toolbar.setNavigationIcon(R.drawable.icon_blank);
        }else {
            toolbar.setTitle(R.string.PMPMAP_TITLE);
            toolbar.setNavigationIcon(R.drawable.icon_back);
            //create extra poi cat
            if (PMPDataManager.getSharedPMPManager(null).getResponseData() != null) {
                MenuItem catMenuItem = toolbar.getMenu().findItem(R.id.menu_map_filter);
                int order = 1;
                for(PoiCategories poiCategory : PMPDataManager.getSharedPMPManager(null).getResponseData().getPoiCategories()) {
                    if (poiCategory.isStartPoint()) continue;
                    String name = PMPUtil.getLocalizedString(poiCategory.getName());
                    catMenuItem.getSubMenu().add(Menu.NONE, (int) poiCategory.getId(), order, name);
                    order++;
                }
            }
        }
        MenuItem menuFilter = toolbar.getMenu().findItem(R.id.menu_map_filter);
        MenuItem menuSearch = toolbar.getMenu().findItem(R.id.menu_search);
        if(menuFilter != null) {
            menuFilter.setIcon(PMPMapSDK.getMapUISetting().getNavFilterImageResource());
        }
        if(menuSearch != null) {
            menuSearch.setIcon(PMPMapSDK.getMapUISetting().getNavSearchImageResource());
        }

        locateMeButton.setLayoutParams(locateMeLayoutParams);
        locateMeButton.requestLayout();

        floorSwitchListV.setLayoutParams(floorSwitchLayoutParams);
        floorSwitchListV.requestLayout();

        updateMapFilterMenu();
        Log.d(TAG, "View Mode:" + viewMode);
    }

    public void adjustToolbarHeight(int heightDelta){
        toolbar.getLayoutParams().height += heightDelta;
    }

    public int getHeaderMode() {
        return headerMode;
    }

    public void setHeaderMode(int headerMode) {
        this.headerMode = headerMode;
        switch (headerMode) {
            case PMPMapHeaderNormal:
                break;
            case PMPMapHeaderTitleAndClose:
                break;
            default:
                break;
        }
    }

    public void setShowSelectionHeader(boolean showSelectionHeader) {
        this.showSelectionHeader = showSelectionHeader;
        if (showSelectionHeader) {
            TextView msgView = (TextView) selectionHeaderView.findViewById(R.id.area_msg);
            msgView.setText(getResources().getString(R.string.PMPMAP_SELECTION_MESSAGE));
            selectionHeaderView.setVisibility(View.VISIBLE);
        } else {
            selectionHeaderView.setVisibility(View.GONE);
        }
    }

    public boolean isShowArea() {
        return showArea;
    }

    public void setShowArea(boolean showArea) {
        this.showArea = showArea;
        if (showArea) {
            areaView.setVisibility(View.VISIBLE);
        }else {
            areaView.setVisibility(View.GONE);
        }
    }

    public Pois getSelectedPOI() {
        return selectedPOI;
    }

    public void setSelectedPOI(Pois selectedPOI) {
        this.selectedPOI = selectedPOI;
        if (selectedPOI != null) {
            firstLocation = false;
            poiSelectionFooterView.setPOIName(PMPUtil.getLocalizedString(selectedPOI.getName()));
            poiSelectionFooterView.setAddress(PMPUtil.getLocalizedAddress(getActivity(), selectedPOI));
            startPointSelectionFooterView.setPOIName(getResources().getString(R.string.PMPMAP_CHOOSE_START_PT_SUBTITLE));
            startPointSelectionFooterView.setAddress(getResources().getString(R.string.PMPMAP_SELECT_CONFIRMATION));
            if (floorAdapter != null) {
                Maps selectFloor = floorAdapter.getSelectedMap();
                if (selectedPOI.getMapId() != selectFloor.getId()) {
                    for (Maps map : PMPDataManager.getSharedPMPManager(null).getResponseData().getMaps()) {
                        if (map.getId() == selectedPOI.getMapId()) {
                            setSelectedMap(map, true);
                            break;
                        }
                    }
                }
            }

            switch (viewMode) {
                case PMPMapViewModePOIGroupCollapse:
                case PMPMapViewModePOIGroupExpend:
                case PMPMapViewModePOIGroupOnly:
                case PMPMapViewModePOIGroupWithPOIInfo:
                    setViewMode(PMPMapViewModePOIGroupWithPOIInfo);
                    break;
                case PMPMapViewModeNavigating:
                case PMPMapViewModeRouteOverview:
                    break;
                case PMPMapViewModePOISelection:
                    setViewMode(PMPMapViewModePOISelection);
                    break;
                default:
                    routeOverviewHeaderView.setToLocation(PMPUtil.getLocalizedString(selectedPOI.getName()));
                    setViewMode(PMPMapViewModePOIInfo);
                    break;
            }
        }else {
            PMPMapController.getInstance().deselectAllPOI();
            switch (viewMode) {
                case PMPMapViewModeNavigating:
                case PMPMapViewModeRouteOverview:
                    break;
                case PMPMapViewModePOIGroupCollapse:
                case PMPMapViewModePOIGroupOnly:
                    break;
                case PMPMapViewModePOIGroupExpend:
                    setViewMode(PMPMapViewModePOIGroupCollapse);
                    break;
                case PMPMapViewModePOIGroupWithPOIInfo:
                {
                    if(showPOITable) {
                        setViewMode(PMPMapViewModePOIGroupExpend);
                    }else {
                        setViewMode(PMPMapViewModePOIGroupOnly);
                    }
                }
                    break;
                case PMPMapViewModePOISelection:
                    setViewMode(PMPMapViewModePOISelection);
                    break;
                default:
                    if (isShowCloseButtonOnTop.get()) {
                        selectedDestinationPOI = selectedDestinationGatePOI;
                        showAroundGate(false);
                }
                    setViewMode(PMPMapViewModeNormal);
                    break;
            }
        }

    }

    private void updateMapFilterMenu() {
        int focusPOICatId = PMPMapController.getInstance().getFocusPOICategoryId();
        MenuItem catMenuItem = toolbar.getMenu().findItem(R.id.menu_map_filter);
        if (catMenuItem != null) {
            int normalColor = Color.parseColor("#757575");
            int selectedColor = PMPMapSDK.getMapUISetting().getThemeColor();

            for (int i = 0; i <catMenuItem.getSubMenu().size() ; i++) {
                MenuItem catItem = catMenuItem.getSubMenu().getItem(i);
                SpannableString spanString;
                spanString = new SpannableString(catItem.getTitle().toString());
                spanString.setSpan(new TypefaceSpan("sans-serif-medium"), 0, spanString.length(), 0);
                spanString.setSpan(new AbsoluteSizeSpan(15, true), 0, spanString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                if (i == 0) {
                    if (focusPOICatId == 0) {
                        catItem.setIcon(R.drawable.icon_tick);
                        spanString.setSpan(new ForegroundColorSpan(selectedColor), 0, spanString.length(), 0);
                    }else {
                        catItem.setIcon(null);
                        spanString.setSpan(new ForegroundColorSpan(normalColor), 0, spanString.length(), 0);
                    }
                }else {
                    if (focusPOICatId <= 0 || catItem.getItemId() != focusPOICatId) {
                        catItem.setIcon(null);
                        spanString.setSpan(new ForegroundColorSpan(normalColor), 0, spanString.length(), 0);
                    }else {
                        catItem.setIcon(R.drawable.icon_tick);
                        spanString.setSpan(new ForegroundColorSpan(selectedColor), 0, spanString.length(), 0);
                    }
                }
                catItem.setTitle(spanString);
            }
        }
    }

    //test only
    private void setupTest(View view) {
        //temp

        if (PMPDataManager.getSharedPMPManager(getActivity()).getResponseData() != null) {
            ResponseData responseData = PMPDataManager.getSharedPMPManager(getActivity()).getResponseData();
            List<Pois> pois = new ArrayList<>();
            for (int i = 0; i < responseData.getPois().size(); i++) {
                if (i > 5) {
                    break;
                }
                pois.add(responseData.getPois().get(i));
            }
            POIGroupAdapter adapter = new POIGroupAdapter(pois);
            poiGroupFooterView.setGroupAdapter(adapter, fragmentContainer.getHeight()-toolbar.getHeight());
        }

        //----TEST ONLY-----
        View testBtn = view.findViewById(R.id.btn_test);
        testBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int viewMode = getViewMode() + 1;
                if (viewMode > PMPMapViewModePOIGroupOnly) {
                    viewMode = 0;
                }
                String modeStr = "Normal";
                switch (viewMode) {
                    case PMPMapViewModeNormal:
                        break;
                    case PMPMapViewModePOIInfo:
                        modeStr = "POI Info";
                        break;
                    case PMPMapViewModeRouteOverview:
                        modeStr = "Route Overview";
                        break;
                    case PMPMapViewModePOIGroupExpend:
                        modeStr = "POI Group Expand";
                        break;
                    case PMPMapViewModePOIGroupCollapse:
                        modeStr = "POI Group Collapse";
                        break;
                    case PMPMapViewModeNavigating:
                        modeStr = "Navigting";
                        break;
                    case PMPMapViewModePOIGroupOnly:
                        modeStr = "POI Group Only";
                        break;
                    default:
                        break;
                }
                ((Button)v).setText(modeStr);
                setViewMode(viewMode);
            }
        });

        View showAreaBtn = view.findViewById(R.id.btn_show_area);
        showAreaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setShowArea(!showArea);
            }
        });
        //--------------------
    }

    private void attachCocosFragment() {
        if (!setupCocos) {
            DisplayMetrics metrics = getResources().getDisplayMetrics();
            float deviceDensity = metrics.density;
            int w = (int) (metrics.widthPixels / deviceDensity);
            int h = (int) (metrics.heightPixels / deviceDensity);
            deviceDensity = 2;
            nativeSetGLFrameSize(deviceDensity, w, h);
            setupCocos = true;
        }

        FragmentManager childFragMgr = getChildFragmentManager();
        cocosFragment = new CocosFragment();
        final Runnable setupContentRunnable = new Runnable() {
            @Override
            public void run() {

                if (PMPDataManager.getSharedPMPManager(getActivity()).getResponseData() != null) {
                    boolean needRestPOIGroup = floorAdapter != null;
                    setupContent();
                    if (needRestPOIGroup) {
                        showPOIGroupIfNeeded();
                    }
                    showOutOfCoverageAreaIfNeeded();
                }
            }
        };
        cocosFragmentCallback = new CocosFragment.CocosFragmentCallback() {
            @Override
            public void onCreate(CocosFragment fragment) {

            }

            @Override
            public void onResume() {
                getActivity().runOnUiThread(setupContentRunnable);
            }

            @Override
            public void surfaceCreated(SurfaceHolder holder) {

            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

            }
        };
        cocosFragment.setCallback(cocosFragmentCallback);
        childFragMgr.beginTransaction().add(pmp_map_holder, cocosFragment).commit();
        poiGroupFooterView.setCocosFragmentReference(cocosFragment);
    }

    private void updateToolbar() {
        toolbar.setTitle(R.string.PMPMAP_TITLE);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.icon_back);
        toolbar.setOnMenuItemClickListener(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PMPMapController.getInstance().setFocusPOIIds(null);
                PMPMapController.getInstance().setFocusPOICategoryId(0);
                PMPMapController.getInstance().deselectAllPOI();

                if(PMPMapSDK.getOnBackButtonClickedCallback() != null) {
                    PMPMapSDK.getOnBackButtonClickedCallback().onMenuClicked(PMPMapFragment.this);
                }

            }
        });
    }

    private void updateErrorHeader() {
        errorHeaderView.setVisibility(View.VISIBLE);
        switch (errorType) {
            case NoGPSSignal: {
                errorHeaderView.errorLabel.setText(getLocalizedString("PMPMAP_ERROR_HEADER_NO_GPS"));
            } break;
            case BluetoothDisabled: {
                errorHeaderView.errorLabel.setText(getLocalizedString("PMPMAP_ERROR_HEADER_BLT_MSG"));
            } break;
            case NetworkConnection: {
                errorHeaderView.errorLabel.setText(getLocalizedString("PMPMAP_ERROR_HEADER_NETWORK_CONNECTION"));
            } break;
            case LocationServiceDisabled: {
                errorHeaderView.errorLabel.setText(getLocalizedString("PMPMAP_ERROR_HEADER_NO_LOCATION_SERVICE"));
            } break;
            case None:
            default:
                errorHeaderView.setVisibility(View.GONE);
                break;
        }
    }

    private void setupContent() {
        Log.d(TAG, "setupContent");
        String langCode = "en";
        switch (PMPMapSDK.getLangID()) {
            case PMPMapSDK.Language_TraditionalChinese:
                langCode = "zh-Hant";
                break;
            case PMPMapSDK.Language_SimplifiedChinese:
                langCode = "zh-Hans";
                break;
            case PMPMapSDK.Language_Japanese:
                langCode = "ja";
                break;
            case PMPMapSDK.Language_Korean:
                langCode = "ko";
                break;
            case PMPMapSDK.Language_English:
            default:
                langCode = "en";
                break;
        }

        nativeSetLangId(PMPMapSDK.getLangID(), langCode);
        if (floorAdapter != null) {
            floorSwitchListV.setAdapter(floorAdapter);
            return;
        }
        ResponseData responseData = PMPDataManager.getSharedPMPManager(getActivity()).getResponseData();
        if (responseData == null) {
            return;
        }
        if (responseData.getMaps() != null) {
            floorAdapter = new FloorAdapter(responseData.getMaps());
            if (floorAdapter.getSelectedMap() == null) {
                boolean mapIsSet = false;
                for(Maps map : responseData.getMaps()) {
                    if (map.getId() == PMPMapController.getInstance().getCurrentFloorId()) {
                        floorAdapter.setSelectedMap(map);
                        mapIsSet = true;
                        break;
                    }
                }
                if(!mapIsSet){
                    for(Maps map : responseData.getMaps()) {
                        if (map.getDefaultProperty()) {
                            floorAdapter.setSelectedMap(map);
                            break;
                        }
                    }
                }

            }
            floorAdapter.setOnItemClickListener(new FloorAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(Maps item) {
                    setSelectedMap(item, true);
                    if (getViewMode() == PMPMapViewModeNavigating) {
                        CoreEngine.getInstance().setRecenterModeEnable(true);
                    }else {
                        CoreEngine.getInstance().setMapMode(CoreEngine.MapModeBrowsing);
                    }
                }
            });
            floorSwitchListV.setAdapter(floorAdapter);
        }

        ViewGroup.LayoutParams params =  floorSwitchListV.getLayoutParams();
        if(PMPMapSDK.getMapUISetting().getNumOfVisibleCell() == 0){
            params.height = PMPMapSDK.getMapUISetting().getFloorCellHeight() * responseData.getMaps().size();
        }else{
            params.height = (int) (PMPMapSDK.getMapUISetting().getFloorCellHeight() * PMPMapSDK.getMapUISetting().getNumOfVisibleCell());
        }
        floorSwitchListV.setLayoutParams(params);

        updateMapFilterMenu();
        //setupTest(getView());
        updateErrorHeader();

        setViewMode(this.viewMode);
        if (getArguments() != null){
            if(getArguments().getStringArray(POI_IDS) != null
                    || getArguments().getString(BRAND_ID) != null
                    || getArguments().getString(POI_CATEGORY_ID) != null) {
                this.showPoiListFromExternal = true;
            }
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isAdded()) {
                    if (callback != null) {
                        callback.onSetupContent();
                    }
//                    if (CoreEngine.getInstance().getMapMode() != CoreEngine.MapModeNavigating) {
//                        Log.d(TAG, "Reset 2d map");
//                        if (nativeShowingARScene()) {
//                            PMPMapController.getInstance().replaceSceneWithMapScene();
//                        }
//                    }
                    if (selectedPOI != null) {
                        setSelectedPOI(selectedPOI);
                    }else {
                        showPOIGroupIfNeeded();
                    }
                    setSelectedMap(floorAdapter.getSelectedMap(),true);
                }

            }
        }, 700);


    }


    private void showSearch() {
        if (PMPDataManager.getSharedPMPManager(getActivity()).getResponseData() == null) {
            return;
        }
        PMPSearchFragment fragment = new PMPSearchFragment();
        fragment.setArguments(getArguments());
        FragmentManager childFragMgr = getChildFragmentManager();
//        getFragmentManager().beginTransaction().addToBackStack(null).commit();

        FragmentTransaction transaction = childFragMgr.beginTransaction();
        transaction.addToBackStack(null)
                .replace(R.id.fragment_container, fragment)
                .commit();
        AnalyticsLogger.getInstance().logEvent("Search_Open");
    }

    /*
    public void toggleMapMode() {
        switch (CoreEngine.getInstance().getMapMode()) {
            case CoreEngine.MapModeBrowsing:
                CoreEngine.getInstance().setMapMode(CoreEngine.MapModeLocateMe);
                break;
            case CoreEngine.MapModeLocateMe:
                if (CoreEngine.getInstance().isCompassEnable()) {
                    CoreEngine.getInstance().setMapMode(CoreEngine.MapModeLocateMe);
                }else {
                    CoreEngine.getInstance().setCompassEnable(true);
                }
                break;
            case CoreEngine.MapModeNavigating:
                break;
        }
    }*/

    private boolean examinePossibleRouteFromCurrentLocation() {
        boolean isRouteExist = false;

        if (CoreEngine.getInstance().getIndoorLocation() != null && selectedPOI != null) {
            isRouteExist = CoreEngine.getInstance().searchPathByPOI((int)selectedPOI.getId());
        }

        return isRouteExist;
    }

    public void showRouteOverview() {
        if (selectedPOI == null) return;

        if (CoreEngine.getInstance().getIndoorLocation() != null &&
                selectedPOI != null &&
                !examinePossibleRouteFromCurrentLocation()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("").setMessage(getResources().getString(R.string.PMPMAP_ERROR_PATH_NOT_FOUND_TITLE));
            builder.show();
            return;
        }

//        if(ttsManager!=null){
//            ttsButton.setVisibility(View.GONE);
//        }

        PMPMapController.getInstance().setFocusPOIIds(null);

        FragmentManager childFragMgr = getChildFragmentManager();
        if(childFragMgr.getBackStackEntryCount() > 0){
            //Inside search flow
            childFragMgr.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        if (isPathSearching) {
            CoreEngine.getInstance().setMapMode(CoreEngine.MapModeBrowsing);

            setViewMode(PMPMapViewModeRouteOverview);
            PMPMapController.getInstance().setOverviewing(true);
            selectLocationForPath(this.selectedPOI);
            setTabBarVisible(false);
            return;
        }

        isPathSearching = true;
        selectedStartingPOI = null;
        selectedDestinationPOI = selectedPOI;
        routeOverviewHeaderView.setToLocation(PMPUtil.getLocalizedString(selectedDestinationPOI.getName()));
        CoreEngine.getInstance().setMapMode(CoreEngine.MapModeBrowsing);
        int curMapId = -1;
        if (CoreEngine.getInstance().getIndoorLocation() != null) {
            curMapId = Integer.parseInt(CoreEngine.getInstance().getIndoorLocation().getName());
            if (curMapId != floorAdapter.getSelectedMap().getId()) {
                for (Maps map : PMPDataManager.getSharedPMPManager(null).getResponseData().getMaps()) {
                    if (map.getId() == curMapId) {
                        PMPMapController.getInstance().setCurrnetFloorById((int) map.getId());
                        break;
                    }
                }
            }
        }

        setViewMode(PMPMapViewModeRouteOverview);
        PMPMapController.getInstance().setOverviewing(true);


        if (CoreEngine.getInstance().getIndoorLocation() != null) {
            locateMeButton.setVisibility(View.VISIBLE);
            routeOverviewHeaderView.setFromLocationAlpha(1.0f);
            routeOverviewHeaderView.setFromLocation(getResources().getString(R.string.PMPMAP_CURRENT_LOCATION));
            boolean result = CoreEngine.getInstance().searchPathByPOI((int) selectedPOI.getId());

            if (result ) {
                routeOverviewFooterView.setDuration(CoreEngine.getInstance().getNavigationTotalTime());
                if (floorAdapter != null) {
                    for (Maps map : PMPDataManager.getSharedPMPManager(getActivity()).getResponseData().getMaps()) {
                        if (map.getId() == curMapId) {
                            setSelectedMap(map, true);
                            break;
                        }
                    }
                    routeOverviewFloorView.setTitle(floorAdapter.getSelectedMap().getName());
                }
                routeOverviewFloorView.setIndictorText(null);
                routeOverviewFloorView.setSelected(true);

                int[] isOpeningFlag = new int[1];
                CoreEngine.getInstance().parseScheduleByPOIID((int) selectedPOI.getId(), isOpeningFlag);
                if(isOpeningFlag[0] <= 0){
                    String msg = getString(R.string.PMPMAP_NAV_POI_CLOSED_MSG, PMPUtil.getLocalizedString(selectedPOI.getName()) , PMPUtil.getLocalizedString(selectedPOI.getBusinessHours()));
                    String proceed = getString(R.string.PMPMAP_PROCEED);
                    String cancel = getString(R.string.PMPMAP_CANCEL);

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("").setMessage(msg);
                    builder.setPositiveButton(proceed, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.setNegativeButton(cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            routeOverviewHeaderView.getOnCloseClickedListener().run();
                        }
                    });
                    builder.show();
                }
                //TODO Immigration Check

                if (CoreEngine.getInstance().checkResultPathType(Edge.PathType.Immigration_Check)) {
                    notificationView.setNotificationType(NotificationView.NotificationTypeImmigration);
                } else if (CoreEngine.getInstance().checkResultPathType(Edge.PathType.Security_Check)) {
                    notificationView.setNotificationType(NotificationView.NotificationTypeSecurity);
                } else {
                    notificationView.setNotificationType(NotificationView.NotificationTypeNone);
                }

//                PathResult result = results.get(0);
//                if (result.isIntersectPathType(Edge.PathType.Immigration_Check)) {
//                    notificationView.setNotificationType(NotificationView.NotificationTypeImmigration);
//                }else if(isOpeningFlag[0] == 0){
//                    notificationView.setNotificationType(NotificationView.NotificationTypePOIClose);
//                }else{
//                    notificationView.setNotificationType(NotificationView.NotificationTypeNone);
//                }
//                notificationView.setNotificationType(NotificationView.NotificationTypeNone);

                /*
                float minX = Float.MAX_VALUE,
                        minY = Float.MAX_VALUE,
                        maxX = Float.MIN_VALUE,
                        maxY = Float.MIN_VALUE;
                PathResult pathResult = results.get(0);
                for (PathNode node : pathResult.getPathNodes()) {
                    minX = (float) Math.min(minX, node.getCurrentVertex().getX());
                    minY = (float) Math.min(minY, node.getCurrentVertex().getY());
                    maxX = (float) Math.max(maxX, node.getCurrentVertex().getX());
                    maxY = (float) Math.max(maxY, node.getCurrentVertex().getY());
                }

                final RectF bounds = new RectF(minX, minY, maxX, maxY);
                final DisplayMetrics metrics = getResources().getDisplayMetrics();
                float screenWidth = metrics.widthPixels / metrics.density;
                float screenHeight = metrics.heightPixels / metrics.density;
                View.OnLayoutChangeListener layoutChangeListener = new View.OnLayoutChangeListener() {
                    @Override
                    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                        if (routeOverviewHeaderView.getHeight() != 0 && routeOverviewFooterView.getHeight() != 0) {
                            RectF padding = new RectF();

                            padding.left = 20;
                            padding.right = 20;
                            padding.top = routeOverviewHeaderView.getHeight() / metrics.density + 20.0f;
                            padding.bottom = routeOverviewFooterView.getHeight() / metrics.density + 20.0f;

                            PMPMapController.getInstance().fitBounds(bounds, padding, true);
                        }
                        v.removeOnLayoutChangeListener(this);
                    }
                };
                routeOverviewHeaderView.addOnLayoutChangeListener(layoutChangeListener);
                routeOverviewFloorView.addOnLayoutChangeListener(layoutChangeListener);
                */
                if (needOverview()) {
                    routeStepIndex = -1;
                }else {
                    routeStepIndex = 0;
                }

                updateRouteStepButtons();
                if (CoreEngine.getInstance().getRouteStepIndicators().length > 2) {
                    routeOverviewFooterView.setShowStep(true);
                }else {
                    routeOverviewFooterView.setShowStep(false);
                }
                View.OnLayoutChangeListener layoutChangeListener = new View.OnLayoutChangeListener() {
                    @Override
                    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                        if (routeOverviewHeaderView.getHeight() != 0 && routeOverviewFooterView.getHeight() != 0) {
                            moveToCurrentRouteStep();
                        }
                        v.removeOnLayoutChangeListener(this);
                    }
                };
                routeOverviewHeaderView.addOnLayoutChangeListener(layoutChangeListener);
                routeOverviewFloorView.addOnLayoutChangeListener(layoutChangeListener);

                setTabBarVisible(false);
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("").setMessage(getResources().getString(R.string.PMPMAP_ERROR_PATH_NOT_FOUND_TITLE));
                builder.show();

                locateMeButton.setVisibility(View.GONE);
                startNavButton.setVisibility(View.GONE);
                routeOverviewFloorView.setVisibility(View.GONE);
                routeOverviewFooterView.setVisibility(View.GONE);
            }
        } else {
            routeOverviewHeaderView.setFromLocationAlpha(0.5f);
            routeOverviewHeaderView.setFromLocation(getResources().getString(R.string.PMPMAP_CHOOSE_START_POINT));

            locateMeButton.setVisibility(View.GONE);
            startNavButton.setVisibility(View.GONE);
            routeOverviewFloorView.setVisibility(View.GONE);
            routeOverviewFooterView.setVisibility(View.GONE);
            setTabBarVisible(false);
        }

        HashMap<String, Object> params = new HashMap<>();
        params.put("from_POI", selectedStartingPOI!=null? ""+(int)selectedStartingPOI.getId() : "0");
        params.put("to_POI", selectedDestinationPOI!=null? ""+(int)selectedDestinationPOI.getId() : "0");
        params.put("route_distance", CoreEngine.getInstance().getNavigationTotalDistance());
        params.put("total_route_phases", CoreEngine.getInstance().getRouteStepIndictorsSize());
        AnalyticsLogger.getInstance().logEvent("Navigate_Start_Preview", params);
    }

    private void showNavigating() {
        if (CoreEngine.getInstance().getIndoorLocation() != null && selectedPOI != null){
            //--TODO-UPDATE NOTIFCATION
            notificationView.setNotificationType(NotificationView.NotificationTypeNone);
            navigatingFooterView.setPOIName(PMPUtil.getLocalizedString(selectedPOI.getName()));
            arNavigatingFooterView.setPOIName(PMPUtil.getLocalizedString(selectedPOI.getName()));
            boolean results = CoreEngine.getInstance().searchPathByPOI((int) selectedPOI.getId());
            if (results) {
                PMPMapController.getInstance().setEnablePathOverview(false);
                onStepStepMessageUpdate(0, null, null, null,0,0);
                CoreEngine.getInstance().setMapMode(CoreEngine.MapModeNavigating);
                Coord coord = PMPMapController.getInstance().getMapCoord();
                coord.scale = (float) PMPDataManager.getSharedPMPManager(null).getResponseData().getMapDefaultZoom();
                PMPMapController.getInstance().setMapCoord(coord);
                setViewMode(PMPMapViewModeNavigating);
                int curMapId = Integer.parseInt(CoreEngine.getInstance().getIndoorLocation().getName());

                for (Maps map : PMPDataManager.getSharedPMPManager(getActivity()).getResponseData().getMaps()) {
                    if (map.getId() == curMapId) {
                        setSelectedMap(map, true);
                        break;
                    }
                }
                setTabBarVisible(false);

            }
            PMPMapController.getInstance().setOverviewing(false);

            Integer found = CollectionUtils.find(PMPDataManager.getSharedPMPManager(getActivity()).getResponseData().getGateIds(), new Predicate<Integer>() {
                @Override
                public boolean evaluate(Integer object) {
                    return object == selectedDestinationPOI.getId();
                }
            });

            isNavigatingToGate.set(found != null);

            HashMap<String, Object> params = new HashMap<>();
            params.put("from_POI", selectedStartingPOI != null? ""+(int)selectedStartingPOI.getId() : "0");
            params.put("to_POI", selectedDestinationPOI != null? ""+(int)selectedDestinationPOI.getId() : "0");
            params.put("route_distance", "" + CoreEngine.getInstance().getNavigationTotalDistance());
            AnalyticsLogger.getInstance().logEvent("Navigate_Start", params);

            changeMapIndicator(new HashMap(), false);
        }
    }

    private void showNavigatingFromAR() {
        if (CoreEngine.getInstance().getIndoorLocation() != null && selectedPOI != null){
            //--TODO-UPDATE NOTIFCATION
            notificationView.setNotificationType(NotificationView.NotificationTypeNone);
            navigatingFooterView.setPOIName(PMPUtil.getLocalizedString(selectedPOI.getName()));
            arNavigatingFooterView.setPOIName(PMPUtil.getLocalizedString(selectedPOI.getName()));
            boolean results = CoreEngine.getInstance().searchPathByPOI((int) selectedPOI.getId());
            if (results) {
                PMPMapController.getInstance().setEnablePathOverview(false);
                Coord coord = PMPMapController.getInstance().getMapCoord();
                coord.scale = (float) PMPDataManager.getSharedPMPManager(null).getResponseData().getMapDefaultZoom();
                PMPMapController.getInstance().setMapCoord(coord);
//                setViewMode(PMPMapViewModeNavigating);
                int curMapId = Integer.parseInt(CoreEngine.getInstance().getIndoorLocation().getName());

                for (Maps map : PMPDataManager.getSharedPMPManager(getActivity()).getResponseData().getMaps()) {
                    if (map.getId() == curMapId) {
                        setSelectedMap(map, true);
                        break;
                    }
                }
                setTabBarVisible(false);

            }
            PMPMapController.getInstance().setOverviewing(false);

//            Integer found = CollectionUtils.find(PMPDataManager.getSharedPMPManager(getActivity()).getResponseData().getGateIds(), new Predicate<Integer>() {
//                @Override
//                public boolean evaluate(Integer object) {
//                    return object == selectedDestinationPOI.getId();
//                }
//            });
//
//            isNavigatingToGate.set(found != null);
        }
    }

    private void onClose() {
        setShowSelectionHeader(false);
        isPathSearching = false;
        selectedStartingPOI = null;
        selectedDestinationPOI = null;
        isARViewShowing = false;
        setSelectedPOI(null);
        PMPMapController.getInstance().setFocusPOIIds(null);
        changeMapIndicator(new HashMap(), false);
        setViewMode(PMPMapViewModeNormal);
        CoreEngine.getInstance().setMapMode(CoreEngine.MapModeBrowsing);
        PMPMapController.getInstance().setOverviewing(false);
        //reset blocker view
        toggleInteractionBlocker(CoreEngine.PathTypeNormal, null, null);
        PMPMapController.getInstance().setEnablePathOverview(false);
        setTabBarVisible(true);

        cocosFragment.runOnGLThread(new Runnable() {
            @Override
            public void run() {
                CoreEngine.getInstance().clearSearchResult();
            }
        });
        stopAndResetDismissArrivedDestinationTimer();

//        if(ttsManager!=null){
//            ttsButton.setVisibility(View.VISIBLE);
//        }
    }

    private void closeNavigating() {
        onClose();
        showPOIGroupIfNeeded();

    }

    private void showAroundGate(boolean cancelNavigation) {
        Pois pois = null;
        if(selectedDestinationPOI != null){
            pois = CollectionUtils.find(PMPDataManager.getSharedPMPManager(null).getResponseData().getPois(), new Predicate<Pois>() {
                @Override
                public boolean evaluate(Pois object) {
                    return object.getId() == selectedDestinationPOI.getId();
                }
            });
        }

        PMPSearchAroundFragment aroundFragment = new PMPSearchAroundFragment();
        Bundle bundle = new Bundle();
        if (pois != null) {
            bundle.putParcelable(PMPSearchAroundFragment.ARGS_GATE, pois);
            bundle.putBoolean(PMPSearchAroundFragment.ARGS_BACK_TO_BROWSING, cancelNavigation);
        }
        aroundFragment.setArguments(bundle);
        getChildFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, aroundFragment, "PMPSearchAroundFragment")
                .addToBackStack(null)
                .commit();
    }

    private void showPromotionDetail(Promotions promotion) {
        PMPPromotionDetailFragment detailFragment = new PMPPromotionDetailFragment();
        detailFragment.setPromotion(promotion);
        getChildFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, detailFragment)
                .addToBackStack(null)
                .commit();
    }

    private void showPOIDetail() {
        boolean hasDetails = true;
        boolean breakTheLoop = false;
        if (selectedPOI != null) {
            hasDetails = selectedPOI.getBrandId() != 0;

            if (!hasDetails &&
                selectedPOI.getPoiCategoryIds() != null &&
                selectedPOI.getPoiCategoryIds().size() != 0) {
                for (int mId : selectedPOI.getPoiCategoryIds()) {
                    for (PoiCategories poiCat : PMPDataManager.getSharedPMPManager(getActivity()).getResponseData().getFlattenPoiCategories()) {
                        if (poiCat.getId() == mId) {
                            if (poiCat.isHasDetails()) {
                                hasDetails = poiCat.isHasDetails();
                                breakTheLoop = true;
                            }
                            break;
                        }
                    }
                    if (breakTheLoop) {
                        break;
                    }
                }
            }
            if (hasDetails) {
                PMPPOIDetailFragment detailFragment = new PMPPOIDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString(PMPPOIDetailFragment.POI_ID, ""+((int)selectedPOI.getId()));
                detailFragment.setArguments(bundle);
                getChildFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragment_container, detailFragment)
                        .addToBackStack(null)
                        .commit();

                HashMap<String, Object> params = new HashMap<>();
                params.put("selected_poi_id", "" + (int)selectedPOI.getId());
                AnalyticsLogger.getInstance().logEvent("Detail_View_Poi", params);
            }


        }
    }

    public void showPOIGroupList() {
        setViewMode(PMPMapViewModePOIGroupExpend);
    }

    private void showCurrentArea(boolean showOutOfCoverageAreaMsg) {
        if (isAdded()) {
            TextView msgView = (TextView) areaView.findViewById(R.id.area_msg);

            if(showOutOfCoverageAreaMsg) {
                String msg = getString(R.string.PMPMAP_NOT_IN_COVERAGE);
                msgView.setText(msg);
                setShowArea(true);
                getView().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setShowArea(false);
                    }
                }, 3000);
                return;
            }
            Areas area = CoreEngine.getInstance().getArea();
            if (area == null && !showOutOfCoverageAreaMsg) {
                return;
            }

            if (area != null) {
                String areaName = PMPUtil.getLocalizedString(area.getName());
                if(areaName == null || areaName.length() == 0){
                    areaName = getString(R.string.PMPMAP_AIRPORT);
                }
                String msg = String.format(getString(R.string.PMPMAP_CURRENT_AREA_MSG), areaName);
                SpannableString finalMsg = new SpannableString(msg);
                StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
                finalMsg.setSpan( boldSpan, msg.length() - areaName.length(), msg.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                msgView.setText(finalMsg);
            } else if (showOutOfCoverageAreaMsg) {
                String msg = getString(R.string.PMPMAP_NOT_IN_COVERAGE);
                msgView.setText(msg);
            }

            setShowArea(true);
            getView().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setShowArea(false);
                }
            }, 3000);
        }
    }

    private void updateRouteStepButtons() {
        CoreEngine coreEngine = CoreEngine.getInstance();
        int count = coreEngine.getRouteStepIndicators().length;
        int firstIdx = 0;
        if (needOverview()) {
            firstIdx = -1;
        }
        if (routeStepIndex == count -2) {
            routeOverviewFooterView.setEnableNextButton(false);
            routeOverviewFooterView.setEnablePreviousButton(true);
        }else if (routeStepIndex == firstIdx) {
            routeOverviewFooterView.setEnableNextButton(true);
            routeOverviewFooterView.setEnablePreviousButton(false);
        }else {
            routeOverviewFooterView.setEnableNextButton(true);
            routeOverviewFooterView.setEnablePreviousButton(true);
        }
    }

    private void moveToCurrentRouteStep() {
        /*
        CoreEngine coreEngine = CoreEngine.getInstance();
        RouteStepIndicator[] steps = coreEngine.getRouteStepIndicators();
        if (steps.length < routeStepIndex + 1) return;
        RouteStepIndicator step1 = steps[routeStepIndex];
        RouteStepIndicator step2 = steps[routeStepIndex+1];

        float minX = Float.MAX_VALUE,
                minY = Float.MAX_VALUE,
                maxX = Float.MIN_VALUE,
                maxY = Float.MIN_VALUE;
        minX = Math.min(minX, step1.getX());
        minY = Math.min(minY, step1.getY());
        maxX = Math.max(maxX, step1.getX());
        maxY = Math.max(maxY, step1.getY());

        minX = Math.min(minX, step2.getX());
        minY = Math.min(minY, step2.getY());
        maxX = Math.max(maxX, step2.getX());
        maxY = Math.max(maxY, step2.getY());

        final RectF bounds = new RectF(minX, minY, maxX, maxY);
        final DisplayMetrics metrics = getResources().getDisplayMetrics();
        RectF padding = new RectF();

        padding.left = 20;
        padding.right = 20;
        padding.top = (routeOverviewHeaderView.getHeight() +
                notificationView.getHeight() +
                errorHeaderView.getHeight() + 56) / metrics.density + 20.0f;
        padding.bottom = (routeOverviewFooterView.getHeight()+ 24) / metrics.density + 20.0f;

        PMPMapController.getInstance().fitBounds(bounds, padding, true);
        int mapId = step1.getNextMapId();
        if ( routeStepIndex == steps.length - 2) {
            mapId = step2.getNextMapId();
        }
        if (floorAdapter != null) {
            if (floorAdapter.getSelectedMap().getId() != mapId) {
                for (Maps map : PMPDataManager.getSharedPMPManager(null).getResponseData().getMaps()) {
                    if (map.getId() == mapId) {
                        setSelectedMap(map, true);
                        break;
                    }
                }
            }
            routeOverviewFloorView.setTitle(floorAdapter.getSelectedMap().getName());
        }

        routeOverviewFloorView.setIndictorText(null);
        routeOverviewFloorView.setSelected(true);
        */
        CoreEngine coreEngine = CoreEngine.getInstance();
        RouteStepIndicator[] steps = coreEngine.getRouteStepIndicators();
        if (routeStepIndex == -1) {
            PMPMapController.getInstance().setEnablePathOverview(true);
            moveToRouteStep(0, steps.length-1);
        }else {
            PMPMapController.getInstance().setEnablePathOverview(false);
            moveToRouteStep(routeStepIndex, routeStepIndex+1);
        }
    }

    private void moveToRouteStep(int startIdx, int endIdx) {
        CoreEngine coreEngine = CoreEngine.getInstance();
        RouteStepIndicator[] steps = coreEngine.getRouteStepIndicators();
        if (steps.length <= endIdx || steps.length <= startIdx) return;
        RouteStepIndicator step1 = steps[startIdx];
        RouteStepIndicator step2 = steps[endIdx];

        float minX = Float.MAX_VALUE,
                minY = Float.MAX_VALUE,
                maxX = Float.MIN_VALUE,
                maxY = Float.MIN_VALUE;
        minX = Math.min(minX, step1.getX());
        minY = Math.min(minY, step1.getY());
        maxX = Math.max(maxX, step1.getX());
        maxY = Math.max(maxY, step1.getY());

        minX = Math.min(minX, step2.getX());
        minY = Math.min(minY, step2.getY());
        maxX = Math.max(maxX, step2.getX());
        maxY = Math.max(maxY, step2.getY());

        final RectF bounds = new RectF(minX, minY, maxX, maxY);
        final DisplayMetrics metrics = getResources().getDisplayMetrics();
        RectF padding = new RectF();

        padding.left = 20;
        padding.right = 20;
        padding.top = (routeOverviewHeaderView.getHeight() +
                notificationView.getHeight() +
                errorHeaderView.getHeight() + 56) / metrics.density + 20.0f;
        padding.bottom = (routeOverviewFooterView.getHeight()+ 24) / metrics.density + 20.0f;

        PMPMapController.getInstance().fitBounds(bounds, padding, true);
        int mapId = step1.getNextMapId();
        if (startIdx <= 0) {
            mapId = step1.getMapId();
        }else {
            mapId = step1.getNextMapId();
            if ( startIdx == steps.length - 2) {
                mapId = step2.getNextMapId();
            }
        }

        if (floorAdapter != null) {
            if (floorAdapter.getSelectedMap().getId() != mapId) {
                for (Maps map : PMPDataManager.getSharedPMPManager(null).getResponseData().getMaps()) {
                    if (map.getId() == mapId) {
                        setSelectedMap(map, true);
                        break;
                    }
                }
            }
            routeOverviewFloorView.setTitle(floorAdapter.getSelectedMap().getName());
        }

        routeOverviewFloorView.setIndictorText(null);
        routeOverviewFloorView.setSelected(true);
    }

    public void toggleInteractionBlocker(int pathType, String message, String imageName){
        if(currentPathType == pathType){
            return;
        }
        currentPathType = pathType;

        boolean isBlockerShown = false;
        LayoutTransition layoutTransition = ((ViewGroup)getView()).getLayoutTransition();
        layoutTransition.disableTransitionType(LayoutTransition.CHANGING);
        switch (pathType){
            case CoreEngine.PathTypeLiftUp: //lift up
            case CoreEngine.PathTypeLiftDown: //lift down
                if (message!=null && message.length()>0) navigatingHeaderView.setInstruction(message);
                if (imageName!=null && imageName.length()>0) navigatingHeaderView.setIconImage(imageName);
                navigatingHeaderView.setDistance(0);
                navigatingHeaderView.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
                interactionBlockerView.setVisibility(View.VISIBLE);
                interactionBlockerIconImageView.setImageResource(R.drawable.icon_inside_lift);
                interactionBlockerMessageLabel.setText(getString(R.string.PMPMAP_NAV_LIFT_INSIDE));
                interactionBlockerView.setTag(PMPMapViewBlockerOthers);
                isBlockerShown = true;
                break;
            case CoreEngine.PathTypeImmigrationCheck: //Immigration check
            case CoreEngine.PathTypeArrivalImmigrationCheck:
                if (message!=null && message.length()>0) navigatingHeaderView.setInstruction(message);
                if (imageName!=null && imageName.length()>0) navigatingHeaderView.setIconImage(imageName);
//                navigatingHeaderViewFollowRoute.setVisibility(View.GONE);
//                PMPMapController.getInstance().setARScreenBlocked(true);
                navigatingHeaderView.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
                interactionBlockerView.setVisibility(View.VISIBLE);
                interactionBlockerIconImageView.setImageResource(R.drawable.icon_immigration_check);
                interactionBlockerMessageLabel.setText(getString(R.string.PMPMAP_NAV_BLOCKER_MSG_IMMIGRATION_CHECK_NO_PHONE));
                interactionBlockerView.setTag(PMPMapViewBlockerOthers);
                isBlockerShown = true;
                break;
            case CoreEngine.PathTypeAPM: //APM
                if (message!=null && message.length()>0) navigatingHeaderView.setInstruction(message);
                if (imageName!=null && imageName.length()>0) navigatingHeaderView.setIconImage(imageName);
//                navigatingHeaderViewFollowRoute.setVisibility(View.GONE);

                if (message!=null && message.length()>0) navigatingHeaderView.setInstruction(message);
                if (imageName!=null && imageName.length()>0) navigatingHeaderView.setIconImage(imageName);
                navigatingHeaderView.setDistance(0);
                navigatingHeaderView.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
                interactionBlockerView.setVisibility(View.VISIBLE);
                interactionBlockerIconImageView.setImageResource(R.drawable.icon_taking_apm);
                interactionBlockerMessageLabel.setText(getString(R.string.PMPMAP_NAV_BLOCKER_MSG_IN_TRANSIT));
                interactionBlockerView.setTag(PMPMapViewBlockerTransport);
                isBlockerShown = true;
                break;
            case CoreEngine.PathTypeShuttleBus: //Shuttle bus
                if (message!=null && message.length()>0) navigatingHeaderView.setInstruction(message);
                if (imageName!=null && imageName.length()>0) navigatingHeaderView.setIconImage(imageName);
                navigatingHeaderView.setDistance(0);
                navigatingHeaderView.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
                interactionBlockerView.setVisibility(View.VISIBLE);
                interactionBlockerIconImageView.setImageResource(R.drawable.icon_taking_shuttle);
                interactionBlockerMessageLabel.setText(getString(R.string.PMPMAP_NAV_BLOCKER_MSG_IN_TRANSIT));
                interactionBlockerView.setTag(PMPMapViewBlockerTransport);
                isBlockerShown = true;
                break;
            case CoreEngine.PathTypeCustomCheck: //Custom check
                if (message!=null && message.length()>0) navigatingHeaderView.setInstruction(message);
                if (imageName!=null && imageName.length()>0) navigatingHeaderView.setIconImage(imageName);
//                navigatingHeaderViewFollowRoute.setVisibility(View.GONE);
//                PMPMapController.getInstance().setARScreenBlocked(true);
                navigatingHeaderView.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
                interactionBlockerView.setVisibility(View.VISIBLE);
                interactionBlockerIconImageView.setImageResource(R.drawable.icon_customs_and_excise);
                interactionBlockerMessageLabel.setText(getString(R.string.PMPMAP_NAV_BLOCKER_MSG_CUSTOM_WARNING));
                interactionBlockerView.setTag(PMPMapViewBlockerOthers);
                isBlockerShown = true;
                break;
            case CoreEngine.PathTypeSecurityCheck: //Custom check

                if (message!=null && message.length()>0) navigatingHeaderView.setInstruction(message);
                if (imageName!=null && imageName.length()>0) navigatingHeaderView.setIconImage(imageName);
//                navigatingHeaderViewFollowRoute.setVisibility(View.GONE);
//                PMPMapController.getInstance().setARScreenBlocked(true);
                navigatingHeaderView.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
                interactionBlockerView.setVisibility(View.VISIBLE);
                interactionBlockerIconImageView.setImageResource(R.drawable.icon_immigration_check);
                interactionBlockerMessageLabel.setText(getString(R.string.PMPMAP_NAV_BLOCKER_MSG_SECURITY_WARNING));
                interactionBlockerView.setTag(PMPMapViewBlockerOthers);
                isBlockerShown = true;
                break;
            case CoreEngine.PathTypeEscalatorDown:
            case CoreEngine.PathTypeEscalatorUp:
                if (message!=null && message.length()>0) navigatingHeaderView.setInstruction(message);
                if (imageName!=null && imageName.length()>0) navigatingHeaderView.setIconImage(imageName);
//                navigatingHeaderViewFollowRoute.setVisibility(View.GONE);
                if (isARViewShowing) {
//                    PMPMapController.getInstance().setARScreenBlocked(true);
                    navigatingHeaderView.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
                    interactionBlockerView.setVisibility(View.VISIBLE);
                    interactionBlockerIconImageView.setImageResource(R.drawable.icon_block_escalator);
                    interactionBlockerMessageLabel.setText(getString(R.string.PMPMAP_NAV_TAKING_ESCALATOR));
                    interactionBlockerView.setTag(PMPMapViewBlockerOthers);
                } else {
//                    PMPMapController.getInstance().setARScreenBlocked(false);
                    interactionBlockerView.setVisibility(View.GONE);
                }
                break;
//            case CoreEngine.PathTypeEscalatorDown:
//            case CoreEngine.PathTypeEscalatorUp:
//                //navigatingHeaderViewFollowRoute.setVisibility(View.GONE);
//                if (isARViewShowing) {
//                    navigatingHeaderView.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
//                    interactionBlockerView.setVisibility(View.VISIBLE);
//                    interactionBlockerIconImageView.setImageResource(R.drawable.icon_block_escalator);
//                    interactionBlockerMessageLabel.setText(getString(R.string.PMPMAP_NAV_TAKING_ESCALATOR));
//                    interactionBlockerView.setTag(PMPMapViewBlockerOthers);
//                } else {
//                    PMPMapController.getInstance().setARScreenBlocked(false);
//                    interactionBlockerView.setVisibility(View.GONE);
//                }
//                break;
            default:
                interactionBlockerView.setVisibility(View.GONE);
                break;
        }

        if (isBlockerShown && cameraContainer.getVisibility()==View.VISIBLE) {
            isARViewShowing = false;
            exitAR();
            floorSwitchListV.setVisibility(View.VISIBLE);
        }
        layoutTransition.enableTransitionType(LayoutTransition.CHANGING);
    }

    public void showPOIListOnMap(List<Pois> pois,
                                 Object info,
                                 final boolean showTable,
                                 final boolean showIndicator,
                                 final boolean jumpToFirstPoi,
                                 final boolean autoSelect) {
        Log.d(TAG, "showPOIListOnMap");

        if (pois == null || pois.size() == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(getResources().getString(R.string.PMPMAP_ERROR_RESULT_NOT_FOUND)).setMessage("");
            builder.show();
            return;
        }
        LayoutTransition layoutTransition = null;
        if (getView() != null) {
            layoutTransition = ((ViewGroup) getView()).getLayoutTransition();
            layoutTransition.disableTransitionType(LayoutTransition.CHANGING);
        }

        this.showPOITable = showTable;

        float minX = Float.MAX_VALUE,
                minY = Float.MAX_VALUE,
                maxX = Float.MIN_VALUE,
                maxY = Float.MIN_VALUE;
        final HashMap<Integer, String> indicatorDict = new HashMap<>();
        List<Integer> poiIds = new ArrayList<Integer>();
        Pois currentFloorPoi = null;

        String floorName = null;
        if (getArguments() != null) {
            floorName = getArguments().getString(FLOOR_NAME);
        }

        for (Pois poi : pois) {
            poiIds.add((int) poi.getId());

            if (showIndicator) {
                int numOfPois = 0;
                if (indicatorDict.containsKey((int) poi.getMapId())) {
                    numOfPois = Integer.parseInt(indicatorDict.get((int) poi.getMapId()));
                }
                numOfPois++;
                indicatorDict.put((int) poi.getMapId(), "" + numOfPois);
            }
            if (floorAdapter != null) {
                if (currentFloorPoi == null &&
                        poi.getMapId() == floorAdapter.getSelectedMap().getId()) {
                    currentFloorPoi = poi;
                }
            }
            if (floorAdapter != null) {
                if (poi.getMapId() == floorAdapter.getSelectedMap().getId()) {
                    minX = (float) Math.min(minX, poi.getX());
                    minY = (float) Math.min(minY, poi.getY());
                    maxX = (float) Math.max(maxX, poi.getX());
                    maxY = (float) Math.max(maxY, poi.getY());
                }
            }
        }

        //Clear previous pin first
        PMPMapController.getInstance().deselectAllPOI();

        if (showIndicator) {
            PMPMapController.getInstance().setFocusPOIIds(poiIds);
            changeMapIndicator(indicatorDict, true);
        } else {
            PMPMapController.getInstance().setFocusPOIIds(new ArrayList<Integer>());
            changeMapIndicator(indicatorDict, true);
        }

        //update navigation bar
        final Pois poi = pois.get(0);
        if (info != null && info instanceof PoiCategories) {
            toolbar.setTitle(PMPUtil.getLocalizedString(((PoiCategories) info).getName()));
        } else if (info != null && info instanceof Tags) {
            toolbar.setTitle(((Tags)info).getContent());
        } else {
            toolbar.setTitle(PMPUtil.getLocalizedString(poi.getName()));
        }

        PMPMapConfig.getSharedConfig().pmpSetMapMode(PMPMapConfig.PMPMapModeBrowsing);

        POIGroupAdapter adapter = new POIGroupAdapter(pois);
        adapter.setOnItemClickListener(new POIGroupAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Pois item) {
                CoreEngine.getInstance().setMapMode(CoreEngine.MapModeBrowsing);
                setSelectedPOI(item);
                PMPMapController.getInstance().setSelectedPOIId((int) item.getId());
                DisplayMetrics metrics = getResources().getDisplayMetrics();
                PMPMapController.getInstance().jumpToPosition((float) item.getX(),
                        (float) item.getY(),
                        (float) metrics.widthPixels / metrics.density / 2,
                        (float ) metrics.heightPixels / metrics.density / 2);

                HashMap<String, Object> params = new HashMap<>();
                params.put("selected_poi_id", (int)item.getId());
                params.put("selected_floor", (int)item.getMapId());
                params.put("from_external", showPoiListFromExternal? "1" : "0");
                AnalyticsLogger.getInstance().logEvent("Search_Multi_Floors_Poi" ,params);

            }
        });
        poiGroupFooterView.setGroupAdapter(adapter, fragmentContainer.getHeight()-toolbar.getHeight());
        if (showTable) {
            setViewMode(PMPMapViewModePOIGroupExpend);
        } else if (pois.size() > 1) {
            setViewMode(PMPMapViewModePOIGroupOnly);
        } else {
            setViewMode(PMPMapViewModeNormal);
        }

        List<PMPPOIByDuration> poisWithDuration = PMPPOIByDuration.getAll() ;
        List<PMPPOIByDuration> selectedPoisWithDuration = new ArrayList<PMPPOIByDuration>();

        for (PMPPOIByDuration tempPoiWithDurations : poisWithDuration) {
            for (Pois tempPoi : pois) {
                if(tempPoiWithDurations.poi.getId() == tempPoi.getId()) {
                    PMPPOIByDuration tempPoiDuration = new PMPPOIByDuration(tempPoi,tempPoiWithDurations.duration);
                    selectedPoisWithDuration.add(tempPoiDuration);
                    break;
                }
            }
        }
//        for(Pois tempPoi : pois){
//            PMPPOIByDuration tempPoiDuration = new PMPPOIByDuration(tempPoi,Float.MAX_VALUE);
//            selectedPoisWithDuration.add(tempPoiDuration);
//        }
//        for (PMPPOIByDuration tempPoi : poisWithDuration) {
//            for(PMPPOIByDuration selectedPoi :selectedPoisWithDuration) {
//                if(tempPoi.poi.getId() == selectedPoi.poi.getId()){
//                    selectedPoi.duration = tempPoi.duration;
//                    break;
//                }
//            }
//        }


        Collections.sort(selectedPoisWithDuration, new Comparator<PMPPOIByDuration> (){
            @Override
            public int compare(PMPPOIByDuration o1, PMPPOIByDuration o2){
                if((o1.duration)<(o2.duration))return -1;
                if((o1.duration)==(o2.duration))return 0;
                return 1;
            }
        });

        if (selectedPoisWithDuration.size() > 0) {
            currentFloorPoi = selectedPoisWithDuration.get(0).poi;
        }else{
            currentFloorPoi = pois.get(0);

        }

        if (jumpToFirstPoi && TextUtils.isEmpty(floorName)) {
            //Change to floor of poi
            if (floorAdapter != null) {
                if (currentFloorPoi.getMapId() != floorAdapter.getSelectedMap().getId()) {
                    for (Maps map : PMPDataManager.getSharedPMPManager(null).getResponseData().getMaps()) {
                        if (map.getId() == currentFloorPoi.getMapId()) {
                            setSelectedMap(map, true);
                            break;
                        }
                    }
                }
            }
        }

        final DisplayMetrics metrics = getResources().getDisplayMetrics();
        final float screenWidth = metrics.widthPixels / metrics.density;
        final float screenHeight = metrics.heightPixels / metrics.density;
        if (autoSelect) {
            CoreEngine.getInstance().setMapMode(CoreEngine.MapModeBrowsing);
            setSelectedPOI(currentFloorPoi);
            PMPMapController.getInstance().setSelectedPOIId((int) currentFloorPoi.getId());
            PMPMapController.getInstance().jumpToPosition((float) currentFloorPoi.getX(), (float) currentFloorPoi.getY(), screenWidth /2.0f, screenHeight/2.0f);
        }else if (jumpToFirstPoi) {
            final RectF bounds = new RectF(minX, minY, maxX, maxY);

            final float poiX;
            final float poiY;
            if (currentFloorPoi != null) {
                poiX = (float) currentFloorPoi.getX();
                poiY = (float) currentFloorPoi.getY();
            }else {
                poiX = 0;
                poiY = 0;
            }

            final Pois finalCurrentFloorPoi = currentFloorPoi;
            View.OnLayoutChangeListener layoutChangeListener = new View.OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
            int count = 0;
            if (floorAdapter != null) {
                if (indicatorDict.containsKey(floorAdapter.getSelectedMap().getId())) {
                    count = Integer.parseInt(indicatorDict.get(floorAdapter.getSelectedMap().getId()));
                }
            }

                float screenX, screenY;
                if(showTable) {
                    screenX = screenWidth / 2;
                    float footerHeight = poiGroupFooterView.getHeight() / metrics.density;
                    float tabBarheight = getView().getBottom()/ metrics.density;
                    screenY = screenHeight / 2 - (screenHeight - footerHeight - tabBarheight) /2;
                }else {
                    screenX = screenWidth / 2;
                    screenY = screenHeight / 2;
                }
                PMPMapController.getInstance().jumpToPosition(poiX,
                        poiY,
                        screenX,
                        screenY);
                        v.removeOnLayoutChangeListener(this);
                    }
                };
                poiGroupFooterView.addOnLayoutChangeListener(layoutChangeListener);

                if(!showTable){
                    layoutChangeListener.onLayoutChange(poiGroupFooterView,0,0,0,0,0,0,0,0);
                }
            }

        if (layoutTransition != null) {
            layoutTransition.enableTransitionType(LayoutTransition.CHANGING);
        }
    }

    public void setForcuPOIIds(List<Integer> poiIds) {

        final HashMap<Integer, String> indicatorDict = new HashMap<>();
        if (poiIds != null) {
            Map<Integer, Pois> poisMap = PMPDataManager.getSharedPMPManager(null).getResponseData().getPoisMap();
            for (int poiId : poiIds) {
                Pois poi = poisMap.get(poiId);
                int numOfPois = 0;
                if (indicatorDict.containsKey((int) poi.getMapId())) {
                    numOfPois = Integer.parseInt(indicatorDict.get((int) poi.getMapId()));
                }
                numOfPois++;
                indicatorDict.put((int) poi.getMapId(), "" + numOfPois);
            }
        }
        PMPMapController.getInstance().setFocusPOIIds(poiIds);
        changeMapIndicator(indicatorDict, false);
    }

    private void gpsLocateMe() {
        if(googleMapView != null && gpsLocation != null){
            googleMapMoveToLocation(gpsLocation.getLatitude(), gpsLocation.getLongitude());
        }
    }

    private void googleMapMoveToLocation(double lat, double lng){
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), GMAPDefaultZoom);
        googleMapView.animateCamera(update);
    }

    private void updateUserGPSLocation(Location location) {
        if (location == null) {
            return;
        }
        gpsLocation = location;
        LatLng newLocation = new LatLng(location.getLatitude(), location.getLongitude());
        if (currentPinView == null) {
            if (googleMapView != null) {
                MarkerOptions option = new MarkerOptions().position(newLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.outdoor_indicator));
                currentPinView = googleMapView.addMarker(option);
            }

        } else {
            currentPinView.setPosition(newLocation);
        }

        if (googleMapView != null) {
            //pretend to click locate me
            gpsLocateMe();
        }

        GPSPositioningManager.shared().converToXY(location);

        TextView tv_gps_debug = (TextView)getView().findViewById(R.id.tv_gps_debug);
        SharedPreferences preferences = getActivity().getSharedPreferences("PREF_PMP_LOCATION_SETTINGS", 0);
        //alan
        if(preferences.getBoolean(getResources().getString(R.string.Preference_Debug), false)) {
            tv_gps_debug.setText("GPS accuracy:\n" + location.getAccuracy());
            tv_gps_debug.setVisibility(View.VISIBLE);
        }else{
            tv_gps_debug.setVisibility(View.GONE);
        }
    }

    private void onLocateMeClicked() {
        if(PMPMapSDK.getConfiguration().isShouldEnableGoogleMap() && !isShowingIndoorMap){
            gpsLocateMe();
        }else{
            if (CoreEngine.getInstance().getIndoorLocation() == null ||
                    PMPDataManager.getSharedPMPManager(null).getResponseData() == null) {
                return;
            }

            int fromFloorID = 0;
            int from_x = 0;
            int from_y = 0;
            if(floorAdapter != null){
                fromFloorID = (int)floorAdapter.getSelectedMap().getId();
                from_x = PMPMapController.getInstance().getScreenCenterX();
                from_y = PMPMapController.getInstance().getScreenCenterY();
            }

            Coord coord = PMPMapController.getInstance().getMapCoord();
            coord.scale = (float) PMPDataManager.getSharedPMPManager(null).getResponseData().getMapDefaultZoom();
            PMPMapController.getInstance().setMapCoord(coord);
            switch (CoreEngine.getInstance().getMapMode()) {
                case CoreEngine.MapModeBrowsing:
                {
                    int curMapId = Integer.parseInt(CoreEngine.getInstance().getIndoorLocation().getName());
                    Maps map = null;
                    for (Maps m : PMPDataManager.getSharedPMPManager(null).getResponseData().getMaps()) {
                        if (((int)m.getId()) == curMapId) {
                            map = m;
                            break;
                        }
                    }
                    if (floorAdapter != null && floorAdapter.getSelectedMap().getId() != curMapId) {
                        setSelectedMap(map, true);
                    }
                    CoreEngine.getInstance().setMapMode(CoreEngine.MapModeLocateMe);

                    Map<String, Object> params = new HashMap<>();
                    params.put("from_floor_id", ""+fromFloorID);
                    params.put("to_floor_id", floorAdapter != null? "" + (int)floorAdapter.getSelectedMap().getId() : "0");
                    params.put("from_x", from_x);
                    params.put("from_y", from_y);
                    params.put("to_x", ""+PMPMapController.getInstance().getScreenCenterX());
                    params.put("to_y", ""+PMPMapController.getInstance().getScreenCenterY());
                    if(viewMode == PMPMapViewModeRouteOverview || viewMode == PMPMapViewModeNavigating){
                        AnalyticsLogger.getInstance().logEvent("Navigate_Locate_Me", params);
                    }else{
                        AnalyticsLogger.getInstance().logEvent("Main_Locate_Me", params);
                    }
                }
                break;
                case CoreEngine.MapModeLocateMe:
                {
                    if (CoreEngine.getInstance().isCompassEnable()) {
                        CoreEngine.getInstance().setCompassEnable(false);
                    }else {
                        CoreEngine.getInstance().setCompassEnable(true);
                    }
                    CoreEngine.getInstance().setMapMode(CoreEngine.MapModeLocateMe);
                }
                break;
                default:
                    break;
            }
        }
    }

    //native functions
    private native void nativeSetLangId(int langId, String langCode);
    private native void nativeSetGLFrameSize(float density, int width, int height);
    private native boolean nativeShowingARScene();

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int id = item.getItemId();
        PMPMapController mapController = PMPMapController.getInstance();

        if (id == menu_search) {
            showSearch();
        }else if (id == R.id.menu_close) {
            onClose();

            HashMap<String, Object> params = new HashMap<>();
            if ( poiGroupFooterView.getGroupAdapter() != null &&
                    poiGroupFooterView.getGroupAdapter().getPois() != null &&
                    poiGroupFooterView.getGroupAdapter().getPois().size() > 0) {

                ArrayList<Integer> ids = new ArrayList();
                for(PMPPOIByDuration pmppoiByDuration : poiGroupFooterView.getGroupAdapter().getPois()){
                    ids.add((int)pmppoiByDuration.poi.getId());
                }

                params.put("searched_poi_id", Comm.arrayListToString(ids));
                params.put("from_external",this.showPoiListFromExternal ? "1" : "0");
                this.showPoiListFromExternal = false;
            }
            AnalyticsLogger.getInstance().logEvent("Navigate_Cancel_Poi", params);

            if (getArguments() != null) {
                Log.v(TAG,"get argument in onMenuItemClick()");
                Bundle args = getArguments();
                String[] poiIds = args.getStringArray(POI_IDS);
                String brandId = args.getString(BRAND_ID);
                String poiCategoryId = args.getString(POI_CATEGORY_ID);
                if ((poiIds != null && poiIds.length !=0) || brandId != null || poiCategoryId != null) {
                    if(PMPMapSDK.getOnBackButtonClickedCallback() != null) {
                        PMPMapSDK.getOnBackButtonClickedCallback().onMenuClicked(PMPMapFragment.this);
                    }
                }
            }
        }else if (id == R.id.menu_map_filter) {

            AnalyticsLogger.getInstance().logEvent("Filter_Display");

        }else {
            String catAbbrivation = "";
            if(id != R.id.menu_map_filter_all) {
                mapController.setFocusPOICategoryId(id);
                String catName = PMPServerManager.getShared().getServerResponse().getPoiCategories().get(id - 1).getName().get(0).getContent();
                boolean getNextLetter = true;
                for(int i = 0; i < catName.length();i++){
                    if(getNextLetter){
                        if(Character.isLetter(catName.charAt(i))){
                            catAbbrivation += catName.charAt(i);
                            getNextLetter = false;
                        }
                    } else {
                        if(!Character.isLetter(catName.charAt(i))){
                            getNextLetter = true;
                        }
                    }
                }
                catAbbrivation += "_";
                catAbbrivation += id;

            }else{
                mapController.setFocusPOICategoryId(0);
                catAbbrivation = "All";
            }
            updateMapFilterMenu();
            HashMap<String, Object> params = new HashMap<>();
            params.put("type", "" + catAbbrivation);
            AnalyticsLogger.getInstance().logEvent("Filter_Display_By_Catalog", params);
        }
        return true;
    }

    @Override
    public void onEngineInitialDone() {
        setupContent();
    }

    @Override
    public void onEntryLiftDetected(double x, double y) {
        if (isAdded()) {
            toggleInteractionBlocker(CoreEngine.PathTypeLiftUp, null, null);
        }

    }

    @Override
    public void onEntryShuttleBusDetected(double x, double y) {
        if (isAdded()) {
            toggleInteractionBlocker(CoreEngine.PathTypeShuttleBus, null, null);
        }
    }

    @Override
    public void onArrivalDestination() {
//        navigatingHeaderView.setIconImage(R.drawable.icon_nav_des_white);
//        navigatingHeaderView.setInstruction(R.string.PMPMAP_NAV_ARRIVED);
        if (cameraContainer.getVisibility() != View.GONE) {
//            PMPMapController.getInstance().setARViewArrived(true);
            if (interactionBlockerView.getTag() != null &&
                    interactionBlockerView.getTag().equals(PMPMapViewBlockerBypassed)) {
//                PMPMapController.getInstance().setARScreenBlocked(false);
                interactionBlockerView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onBypassDestination() {
//        navigatingHeaderView.setIconImage(R.drawable.icon_nav_bypass);
//        navigatingHeaderView.setInstruction(R.string.PMPMAP_NAV_BYPASS_DEST);
//        if (PMPMapController.getInstance().isARViewShowing()) {
        if (cameraContainer.getVisibility() != View.GONE) {
//            PMPMapController.getInstance().setARViewBypassed(true);
//            PMPMapController.getInstance().setARScreenBlocked(true);
            interactionBlockerView.setVisibility(View.GONE);
            interactionBlockerIconImageView.setImageResource(R.drawable.icon_bypass);
            interactionBlockerMessageLabel.setText(getString(R.string.PMPMAP_NAV_BYPASS_DEST));
            interactionBlockerView.setTag(PMPMapViewBlockerBypassed);
        } else {
            if (interactionBlockerView.getTag() != null &&
                    interactionBlockerView.getTag().equals(PMPMapViewBlockerBypassed)) {
//                PMPMapController.getInstance().setARScreenBlocked(false);
                interactionBlockerView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onResetBypassDest() {
        if (cameraContainer.getVisibility() != View.GONE) {
            lastOverlayTimeStamp = 0;
//            PMPMapController.getInstance().resetBypassed();
//            PMPMapController.getInstance().setARScreenBlocked(false);
            if (interactionBlockerView.getTag() != null &&
                    interactionBlockerView.getTag().equals(PMPMapViewBlockerBypassed)) {
                interactionBlockerView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onReroute() {

    }

    @Override
    public void onClearSearchResult() {

    }

    @Override
    public void onStepStepMessageUpdate(int type, String message, String poiMessage, String imageName, float totalDuration, float totalDistance) {
        Log.d(TAG, "onStepStepMessageUpdate :" + message);
        if (isAdded() == false) {
            return;
        }
        currentNavigationType = type;
        switch (type) {
            case CoreEngine.NavigationStepOnTheWay:

                navigatingHeaderView.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
                navigatingHeaderView.setInstruction(message);
                navigatingHeaderView.setDistance(totalDistance);
                navigatingHeaderView.setIconImage(imageName);

                double mins = Math.ceil(CoreEngine.getInstance().getNavigationTimeRemaining() / 60.0f);
                if (mins < 1) {
                    mins = 1;
                }
                String strFormat = getString(R.string.PMPMAP_N_MINS);
                if (mins < 2) {
                    strFormat = getString(R.string.PMPMAP_N_MIN);
                }
                navigatingFooterView.setDurationText(String.format(strFormat, String.format("%.0f", mins)));
                arNavigatingFooterView.setDurationText(String.format(strFormat, String.format("%.0f", mins)));
                arrivalMsgTriggered = false;
                break;
            case CoreEngine.NavigationStepArrived: {
                if(this.dismissDestinationNoitceTimer == null){
                    scheduledismissArrivedDestinationTimer();
                }
//                navigatingHeaderViewFollowRoute.setVisibility(View.GONE);
                navigatingHeaderView.setBackgroundColor(getResources().getColor(R.color.nav_arrived_view_color));
                navigatingHeaderView.setInstruction(message);
                navigatingHeaderView.setDistance(0);
                navigatingHeaderView.setIconImage(imageName);
                navigatingFooterView.setDurationText(getString(R.string.PMPMAP_NAV_ARRIVED));
                arNavigatingFooterView.setDurationText(getString(R.string.PMPMAP_NAV_ARRIVED));
                if (isNavigatingToGate.get()) {
                    pv_banner.addPromotion(this.getResources().getString(R.string.PMPMAP_SEARCH_AROUND_GATE));
                    asyncCheckSavedFlight();
                }

                HashMap<String, Object> params = new HashMap<>();
                params.put("arrival_poi_id", selectedPOI != null ? "" + (int) selectedPOI.getId() : "0");
                AnalyticsLogger.getInstance().logEvent("Navigate_Arrival", params);
                if(!arrivalMsgTriggered){
                    arrivalMsgTriggered = true;
                    AnalyticsLogger.getInstance().logEvent("Navigate_Arrival_Dest", params);
                }
            }
                break;
            case CoreEngine.NavigationStepBypassed: {
                stopAndResetDismissArrivedDestinationTimer();
//                navigatingHeaderViewFollowRoute.setVisibility(View.GONE);
                navigatingHeaderView.setBackgroundColor(getResources().getColor(R.color.nav_bypass_view_color));
                navigatingHeaderView.setInstruction(message);
                navigatingHeaderView.setDistance(0);
                navigatingHeaderView.setIconImage(imageName);
                navigatingFooterView.setDurationText(getString(R.string.PMPMAP_NAV_ARRIVED));
                arNavigatingFooterView.setDurationText(getString(R.string.PMPMAP_NAV_ARRIVED));
                if (isNavigatingToGate.get()) {
                    pv_banner.removePromotionAndDismissIfNeeded(PromotionType.Gate);
                }
                HashMap<String, Object> params = new HashMap<>();
                params.put("destination_poi_id", selectedPOI != null ? "" + (int) selectedPOI.getId() : "0");
                AnalyticsLogger.getInstance().logEvent("Navigate_Bypass", params);
                if(arrivalMsgTriggered){
                    arrivalMsgTriggered = false;
                    AnalyticsLogger.getInstance().logEvent("Navigate_Bypass_Dest", params);
                }
            }
                break;
        }

    }

    @Override
    public void onMapModeChanged(int newMapMode) {
        Log.d(TAG, "onMapModeChanged:" + newMapMode);

        if (isPathSearching && (selectedStartingPOI!=null || (CoreEngine.getInstance().getIndoorLocation() == null))) {
            locateMeButton.setVisibility(View.GONE);
            return;
        }

        int locateMeDrawableId = PMPMapSDK.getMapUISetting().getLocateMeButtonOffImageResource();
        int locateMeVisibility = View.VISIBLE;
        int recenterVisibility = View.GONE;

        switch (newMapMode) {
            case CoreEngine.MapModeBrowsing:
                break;
            case CoreEngine.MapModeLocateMe:
                if (CoreEngine.getInstance().isCompassEnable()) {
                    locateMeDrawableId = PMPMapSDK.getMapUISetting().getLocateMeButtonCompassImageResource();//;R.drawable.btn_compass_on;
                }else {
                    locateMeDrawableId = PMPMapSDK.getMapUISetting().getLocateMeButtonOnImageResource();//R.drawable.btn_location_on;
                }
                break;
            case CoreEngine.MapModeNavigating:
                locateMeVisibility = View.GONE;
                if (CoreEngine.getInstance().isRecenterModeEnable()) {
                    recenterVisibility = View.VISIBLE;
                }else {
                    recenterVisibility = View.GONE;
                }
                break;
            default:
                break;
        }
        if (locateMeButton.getVisibility() != locateMeVisibility) {
            locateMeButton.setVisibility(locateMeVisibility);
        }

        if (recenterButton.getVisibility() != recenterVisibility) {
            recenterButton.setVisibility(recenterVisibility);
        }

        final int drawableId = locateMeDrawableId;
        locateMeButton.setImageResource(drawableId);

    }

    @Override
    public void onAreaChanged(Areas area) {
        showCurrentArea(false);
    }

    @Override
    public void onPathTypeChecked(int pathType, String message, String imageName) {
        if (isAdded()) {
            toggleInteractionBlocker(pathType, message, imageName);
        }
    }

    @Override
    public void onShopNearByPromoMessage(final int promoId, final int poiId) {
        if(getActivity() == null){
            return;
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (PMPMapSDK.isEnableNotifiation()) {
                    SaveUserLocationResponse saveUserLocationResponse = PMPProximityServerManager.getShared().getSaveUserLocationResponse();
                    ResponseData responseData = PMPDataManager.getSharedPMPManager(getActivity()).getResponseData();
                    if (responseData != null && responseData.getPromotions() != null) {
                        for (Promotions promo : responseData.getPromotions()) {
                            if (promo.getId() == promoId) {
                                long promotionDisplayUntilTimestamp = Long.MAX_VALUE;
                                if(saveUserLocationResponse != null &&
                                        saveUserLocationResponse.getResult() != null &&
                                        saveUserLocationResponse.getResult().getPromotionDisplayUntilTimestamp() > 0){
                                    promotionDisplayUntilTimestamp = saveUserLocationResponse.getResult().getPromotionDisplayUntilTimestamp();
                                }

                                if (System.currentTimeMillis() < promotionDisplayUntilTimestamp) {
                                    Message fakeMessage = new Message();
                                    if(promo.getPromotionMessages() != null){
                                        for (PromotionMessage promotionMessages : promo.getPromotionMessages()) {
                                            fakeMessage.setContent(PMPUtil.getLocalizedString(promotionMessages.getMessages()));
                                            promo.setMessage(new ArrayList<Message>(Arrays.asList(fakeMessage)));
                                        }
                                        promo.setReferenceId(poiId);//Indicate which POI is matched
                                        pv_banner.addPromotion(promo);
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onInZone(Promotions promotion, boolean isBackground) {
        pv_banner.addPromotion(promotion);
    }

    @Override
    public void onPOISelect(int poiId) {
        switch (getViewMode()) {
            case PMPMapViewModeNavigating:
            case PMPMapViewModeRouteOverview:
                return;
        }

        Pois poi = null;
        for (Pois p : PMPDataManager.getSharedPMPManager(getActivity()).getResponseData().getPois()) {
            if (p.getId() == poiId) {
                poi = p;
                break;
            }
        }
        if (poi != null) {
            setSelectedPOI(poi);
        }

        HashMap<String, Object> params = new HashMap<>();
        params.put("searched_poi_id", ""+poiId);
        AnalyticsLogger.getInstance().logEvent("Search_By_Map", params);
    }

    @Override
    public void onDeselectAllPOI() {
        setSelectedPOI(null);
    }

    @Override
    public void onMapCoordUpdate(Coord coord) {
        float deg = (float) PMPServerManager.getShared().getServerResponse().getMapDirection() + coord.rotation;
        compassButton.setRotation(deg);
        if (CoreEngine.getInstance().getMapMode() == CoreEngine.MapModeNavigating) {
            if (!isARViewShowing) {
                compassButton.setVisibility(View.VISIBLE);
            } else {
                compassButton.setVisibility(View.GONE);
            }
        }else {
            if (coord.rotation == 0) {
                compassButton.setVisibility(View.GONE);
            }else {
                compassButton.setVisibility(View.VISIBLE);
            }
        }
        float map_overview_threshold = 0;
        if (floorAdapter != null && floorAdapter.getSelectedMap() != null) {
            ArrayList<MapImage> imgs = floorAdapter.getSelectedMap().getImages();
            for (MapImage img : imgs) {
                if (map_overview_threshold == 0 || img.getThreshold() < map_overview_threshold) {
                    map_overview_threshold = img.getThreshold();
                }
            }

            if( coord.scale < map_overview_threshold){
                mapScaleIsSmallerThanDefault = true;
            } else if ( coord.scale >= map_overview_threshold){
                mapScaleIsSmallerThanDefault = false;
            }
        }

        if(this.viewMode == PMPMapViewModeNormal) {
            if (mapScaleIsSmallerThanDefault) {
                floorSwitchListV.setVisibility(View.INVISIBLE);
            } else {
                floorSwitchListV.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onMapSceneInitated() {

        setupContent();
        boolean needRestPOIGroup = floorAdapter != null;
        if (needRestPOIGroup) {
            showPOIGroupIfNeeded();
        }

        showOutOfCoverageAreaIfNeeded();
    }

    public static String getLocalizedString(String name){
        if(PMPMapSDK.getApplication() != null){
            int id = PMPMapSDK.getApplication().getResources().getIdentifier(name, "string", PMPMapSDK.getApplication().getPackageName());
            return PMPMapSDK.getApplication().getString(id);
        }
        return "";
    }

    public static String getLocalizedStringWithSpecifiedLanguage(String name, String language) {
        if(PMPMapSDK.getApplication() != null) {
            Context context = PMPMapSDK.getApplication();
            Configuration conf = context.getResources().getConfiguration();
            conf = new Configuration(conf);
            switch (language) {
                case "en":
                    conf.setLocale(Locale.ENGLISH);
                    break;
                case "zh-Hant":
                    conf.setLocale(Locale.TRADITIONAL_CHINESE);
                    break;
                case "zh-Hans":
                    conf.setLocale(Locale.SIMPLIFIED_CHINESE);
                    break;
                case "ja":
                    conf.setLocale(Locale.JAPANESE);
                    break;
                case "ko":
                    conf.setLocale(Locale.KOREAN);
                    break;
                default:
                    conf.setLocale(Locale.ENGLISH);
                    break;
            }

            Context localizedContext = context.createConfigurationContext(conf);
            int id = PMPMapSDK.getApplication().getResources().getIdentifier(name, "string", PMPMapSDK.getApplication().getPackageName());
            return localizedContext.getResources().getString(id);
        }
        return "";
    }

        public static String getLocalizedStringWithSpecifiedLanguage(String name, int languageId){
        if(PMPMapSDK.getApplication() != null) {
            Context context = PMPMapSDK.getApplication();
            Configuration conf = context.getResources().getConfiguration();
            conf = new Configuration(conf);
            switch (languageId) {
                case 1:
                    conf.setLocale(Locale.ENGLISH);
                    break;
                case 2:
                    conf.setLocale(Locale.TRADITIONAL_CHINESE);
                    break;
                case 3:
                    conf.setLocale(Locale.SIMPLIFIED_CHINESE);
                    break;
                case 4:
                    conf.setLocale(Locale.JAPANESE);
                    break;
                case 5:
                    conf.setLocale(Locale.KOREAN);
                    break;
                default:
                    conf.setLocale(Locale.ENGLISH);
                    break;
            }

            Context localizedContext = context.createConfigurationContext(conf);
            int id = PMPMapSDK.getApplication().getResources().getIdentifier(name, "string", PMPMapSDK.getApplication().getPackageName());
            return localizedContext.getResources().getString(id);
        }
        return "";
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult:requestCode" + requestCode + " resultCode:" + resultCode);
        PMPMapConfig.getSharedConfig().applyNewConfig();
    }
    
    @Override
    public void didIndoorLocationUpdated(PMPBeaconType beaconType, PMPLocation location) {
        if (firstLocation && !isShowingIndoorMap) {
            setIsShowingIndoorMap(true);
        }
        if (PMPMapController.getInstance().isMapSceneInitated() == false ||
                cocosFragment.getGLSurfaceView() == null ||
                !isResumed()) {
            return;
        }
        if (firstLocation) {
            CoreEngine.getInstance().setMapMode(CoreEngine.MapModeBrowsing);
            onLocateMeClicked();
            showCurrentArea(false);
            updateFloorSwithTableView(location);
            firstLocation = false;
        }else {
            int mapMode = CoreEngine.getInstance().getMapMode();
            if ((mapMode == CoreEngine.MapModeNavigating ||
                    mapMode == CoreEngine.MapModeLocateMe)) {
                int previousMapId = -1;
                if (floorAdapter != null && floorAdapter.getSelectedMap() != null) {
                    previousMapId = (int) floorAdapter.getSelectedMap().getId();
                }

                int curMapId = Integer.parseInt(location.getName());
                if (previousMapId != curMapId) {
                    updateFloorSwithTableView(location);
                }
            }
        }

        this.arNavigatingFooterView.setCurrentLoc(CoreEngine.getInstance().getCurrentAreaName()+" "+CoreEngine.getInstance().getCurrentMapName());
    }

    private void updateFloorSwithTableView(PMPLocation location){
        for (Maps map : PMPDataManager.getSharedPMPManager(null).getResponseData().getMaps()) {
            if (map.getId() == Integer.parseInt(location.getName())) {
                setSelectedMap(map, true);
                break;
            }
        }
    }

    @Override
    public void didIndoorTransmissionsUpdated(List<PMPBeacon> transmissions) {
        if (!PMPLocationManager.getShared().isPlayback()) {
            PMPMapConfig.getSharedConfig().setBeacons(transmissions);
        }
    }

    @Override
    public void didOutdoorLocationsUpdated() {

    }

    @Override
    public void didIndoorExitRegion() {
        //redmine : #7710 - [Map - aOS] Out of coverage message not shown in AOS
        //sync logic with iOS
        showCurrentArea(true);
    }

    @Override
    public void didCompassUpdated(double direction) {

    }

    @Override
    public void didError(PMPLocationManagerError error) {

    }




    public void closeSelectLocation() {
        drawWalkingPath();
    }

    public void selectStartLocation() {
        /*
        rule : only continue when there is no beacon signal and starting point is not chosen
         */
        if (this.selectedStartingPOI != null ||
                CoreEngine.getInstance().getIndoorLocation() != null) {
            return;
        }

        isChoosingStartingNotDestination = true;
        CoreEngine.getInstance().clearSearchResult();
        if (PMPDataManager.getSharedPMPManager(getActivity()).getResponseData() == null) {
            return;
        }
        PMPSearchStartLocationFragment fragment = new PMPSearchStartLocationFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable(PMPSearchStartLocationFragment.ARGS_DESTINATION, selectedDestinationPOI);
        fragment.setArguments(bundle);

        FragmentManager childFragMgr = getChildFragmentManager();
//        getFragmentManager().beginTransaction().addToBackStack(null).commit();
        FragmentTransaction transaction = childFragMgr.beginTransaction();
        transaction.addToBackStack(null)
                .replace(R.id.fragment_container, fragment)
                .commit();
    }

    public void selectDestination() {
        isChoosingStartingNotDestination = false;
    }

    public void selectPOIOnMap() {
        setSelectedPOI(null);
        setTabBarVisible(true);
        changeMapIndicator(new HashMap(), true);
        toolbar.setTitle(getResources().getString(R.string.PMPMAP_CHOOSE_ON_MAP));
        setViewMode(PMPMapViewModePOISelection);
        PMPMapController.getInstance().setOverviewing(false);
        PMPMapController.getInstance().setFocusPOIIds(null);
    }

    public void drawWalkingPath() {
        PMPMapController.getInstance().setOverviewing(true);
        PMPMapController.getInstance().setFocusPOIIds(null);
        if (CoreEngine.getInstance().getIndoorLocation() != null || selectedStartingPOI != null) {
            int startIdentifier = (selectedStartingPOI!=null)?(int)selectedStartingPOI.getId():-1;
            int destIdentifier = (selectedDestinationPOI!=null)?(int)selectedDestinationPOI.getId():-1;
            boolean results;

            if (startIdentifier == -1) {
                results = CoreEngine.getInstance().searchPathByPOI(destIdentifier);
                startNavButton.setVisibility(View.VISIBLE);
                locateMeButton.setVisibility(View.VISIBLE);
            } else {
                results = startIdentifier != destIdentifier? CoreEngine.getInstance().searchPathFromPOIToPOI(startIdentifier, destIdentifier) : false;
                startNavButton.setVisibility(View.GONE);
            }
            if (destIdentifier != -1) {
                PMPMapController.getInstance().setSelectedPOIId(destIdentifier);
            }
            if (results) {
                routeOverviewFloorView.setVisibility(View.VISIBLE);
                routeOverviewFooterView.setVisibility(View.VISIBLE);

                routeOverviewFooterView.setDuration(CoreEngine.getInstance().getNavigationTotalTime());
                int curMapId;

                if (startIdentifier == -1) {
                    curMapId = Integer.parseInt(CoreEngine.getInstance().getIndoorLocation().getName());
                } else {
                    curMapId = (int)selectedStartingPOI.getMapId();
                }

                if (floorAdapter != null) {
                    for (Maps map : PMPDataManager.getSharedPMPManager(getActivity()).getResponseData().getMaps()) {
                        if (map.getId() == curMapId) {
                            setSelectedMap(map, true);
                            break;
                        }
                    }

                    routeOverviewFloorView.setTitle(floorAdapter.getSelectedMap().getName());
                }
                routeOverviewFloorView.setIndictorText(null);
                routeOverviewFloorView.setSelected(true);

                int[] isOpeningFlag = new int[1];
                CoreEngine.getInstance().parseScheduleByPOIID((int) selectedPOI.getId(), isOpeningFlag);
                //TODO Immigration Check

                if (CoreEngine.getInstance().checkResultPathType(Edge.PathType.Immigration_Check)) {
                    notificationView.setNotificationType(NotificationView.NotificationTypeImmigration);
                } else if (CoreEngine.getInstance().checkResultPathType(Edge.PathType.Security_Check)) {
                    notificationView.setNotificationType(NotificationView.NotificationTypeSecurity);
                } else {
                    notificationView.setNotificationType(NotificationView.NotificationTypeNone);
                }

                //TODO Immigration Check
//                PathResult result = results.get(0);
//                if (result.isIntersectPathType(Edge.PathType.Immigration_Check)) {
//                    notificationView.setNotificationType(NotificationView.NotificationTypeImmigration);
//                }else if(isOpeningFlag[0] == 0){
//                    notificationView.setNotificationType(NotificationView.NotificationTypePOIClose);
//                }else{
//                    notificationView.setNotificationType(NotificationView.NotificationTypeNone);
//                }

                /*
                float minX = Float.MAX_VALUE,
                        minY = Float.MAX_VALUE,
                        maxX = Float.MIN_VALUE,
                        maxY = Float.MIN_VALUE;
                PathResult pathResult = results.get(0);
                for (PathNode node : pathResult.getPathNodes()) {
                    minX = (float) Math.min(minX, node.getCurrentVertex().getX());
                    minY = (float) Math.min(minY, node.getCurrentVertex().getY());
                    maxX = (float) Math.max(maxX, node.getCurrentVertex().getX());
                    maxY = (float) Math.max(maxY, node.getCurrentVertex().getY());
                }

                final RectF bounds = new RectF(minX, minY, maxX, maxY);
                final DisplayMetrics metrics = getResources().getDisplayMetrics();
                float screenWidth = metrics.widthPixels / metrics.density;
                float screenHeight = metrics.heightPixels / metrics.density;
                View.OnLayoutChangeListener layoutChangeListener = new View.OnLayoutChangeListener() {
                    @Override
                    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                        if (routeOverviewHeaderView.getHeight() != 0 && routeOverviewFooterView.getHeight() != 0) {
                            RectF padding = new RectF();

                            padding.left = 20;
                            padding.right = 20;
                            padding.top = routeOverviewHeaderView.getHeight() / metrics.density + 20.0f;
                            padding.bottom = routeOverviewFooterView.getHeight() / metrics.density + 20.0f;

                            PMPMapController.getInstance().fitBounds(bounds, padding, true);
                        }
                        v.removeOnLayoutChangeListener(this);
                    }
                };
                routeOverviewHeaderView.addOnLayoutChangeListener(layoutChangeListener);
                routeOverviewFloorView.addOnLayoutChangeListener(layoutChangeListener);
                */
                if (needOverview()) {
                    routeStepIndex = -1;
                }else {
                    routeStepIndex = 0;
                }
                updateRouteStepButtons();
                if (CoreEngine.getInstance().getRouteStepIndicators().length > 2) {
                    routeOverviewFooterView.setShowStep(true);
                }else {
                    routeOverviewFooterView.setShowStep(false);
                }
                View.OnLayoutChangeListener layoutChangeListener = new View.OnLayoutChangeListener() {
                    @Override
                    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                        if (routeOverviewHeaderView.getHeight() != 0 && routeOverviewFooterView.getHeight() != 0) {
                            moveToCurrentRouteStep();
                        }
                        v.removeOnLayoutChangeListener(this);
                    }
                };
                routeOverviewHeaderView.addOnLayoutChangeListener(layoutChangeListener);
                routeOverviewFloorView.addOnLayoutChangeListener(layoutChangeListener);

                setTabBarVisible(false);
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("").setMessage(getResources().getString(R.string.PMPMAP_ERROR_PATH_NOT_FOUND_TITLE));
                builder.show();

                if (destIdentifier != -1) {
                    PMPMapController.getInstance().setSelectedPOIId(destIdentifier);
                }
                PMPMapController.getInstance().setOverviewing(true);
                selectedStartingPOI = null;
                routeOverviewHeaderView.setFromLocationAlpha(0.5f);
                routeOverviewHeaderView.setFromLocation(getResources().getString(R.string.PMPMAP_CHOOSE_START_POINT));
                locateMeButton.setVisibility(View.GONE);
                startNavButton.setVisibility(View.GONE);
                routeOverviewFloorView.setVisibility(View.GONE);
                routeOverviewFooterView.setVisibility(View.GONE);
            }
        } else {
            locateMeButton.setVisibility(View.GONE);
            startNavButton.setVisibility(View.GONE);
            routeOverviewFloorView.setVisibility(View.GONE);
            routeOverviewFooterView.setVisibility(View.GONE);
        }
    }

    public void selectLocationForPath(Pois poi) {
        if (poi != null) {
            if (isChoosingStartingNotDestination) {
                selectedStartingPOI = poi;
                routeOverviewHeaderView.setFromLocationAlpha(1.0f);
                routeOverviewHeaderView.setFromLocation(PMPUtil.getLocalizedString(selectedStartingPOI.getName()));
            } else {
                selectedDestinationPOI = poi;
                routeOverviewHeaderView.setToLocation(PMPUtil.getLocalizedString(selectedDestinationPOI.getName()));
            }
            drawWalkingPath();
        } else {
//            if (isChoosingStartingNotDestination) {
//                selectedStartingPOI = poi;
//                routeOverviewHeaderView.setFromLocationAlpha(1.0f);
//                routeOverviewHeaderView.setFromLocation(getResources().getString(R.string.PMPMAP_CURRENT_LOCATION));
//            } else {
//                selectedDestinationPOI = poi;
//                routeOverviewHeaderView.setToLocation(getResources().getString(R.string.PMPMAP_CURRENT_LOCATION));
//            }
        }
        PMPMapController.getInstance().setOverviewing(true);

        setTabBarVisible(false);
    }

    public void changeMapIndicator(HashMap indicator, boolean scrollToSelected) {
        if (floorAdapter != null) {
            floorAdapter.setIndicatorMap(indicator);
            floorAdapter.notifyDataSetChanged();
            if (scrollToSelected) {
                if (floorAdapter.getSelectedMap() != null) {
                    floorSwitchListV.setSelection(floorAdapter.getMapIndex(floorAdapter.getSelectedMap()));
                } else {
                    floorSwitchListV.setSelection(0);
                }
            }
        }
    }

    public void setSelectedMap(Maps map, boolean scrollToSelected) {
        if (floorAdapter != null && map != null) {
            floorAdapter.setSelectedMap(map);
            floorAdapter.notifyDataSetChanged();
            if (scrollToSelected) {
                final Maps selectedMap = map;
                floorSwitchListV.setSelection(floorAdapter.getMapIndex(selectedMap));
            }

            HashMap<String, Object> params = new HashMap<>();
            params.put("selected_floor_id", "" + (int)map.getId());
            AnalyticsLogger.getInstance().logEvent("Main_Select_Floor", params);
        }
    }

    public void showPOIGroupIfNeeded() {
        Log.v(TAG,"showPoiGroupOfNeeded");
        if(getViewMode() ==  PMPMapViewModeNavigating) return;
        if (getArguments() != null) {
            Bundle args = getArguments();
            String[] poiIds = args.getStringArray(POI_IDS);
            String brandId = args.getString(BRAND_ID);
            String poiCategoryId = args.getString(POI_CATEGORY_ID);
            String floorName = args.getString(FLOOR_NAME);
            if (!TextUtils.isEmpty(floorName)) {
                for (Maps map : PMPDataManager.getSharedPMPManager(null).getResponseData().getMaps()) {
                    if (map.getName().equals(floorName)) {
                        setSelectedMap(map, true);
                        PMPMapController.getInstance().setCurrnetFloorById((int) map.getId());
                        break;
                    }
                }
            }

            Log.d(TAG, "POI_IDS: " + poiIds);
            Log.d(TAG, "BRAND_ID:" + brandId);
            Log.d(TAG, "poiCategoryId: "+ poiCategoryId);
            if (poiIds != null && poiIds.length != 0) {
                ArrayList<Pois> pois = new ArrayList<Pois>();
                ArrayList<String> titles = new ArrayList<>();
                for (String id :
                        poiIds) {
                    for (Pois poi : PMPDataManager.getSharedPMPManager(null).getResponseData().getPois()) {
                        if (poi.getExternalId() != null && poi.getExternalId().equals(id)) {
                            pois.add(poi);
                            titles.add(PMPUtil.getLocalizedString(poi.getName()));
                            break;
                        }
                    }
                }
                String title = args.getString(POI_TITLE, null);
                if (TextUtils.isEmpty(title)) {
                    title = TextUtils.join(" / ", titles);
                }
                showPOIListOnMap(pois, title, true, true, true, pois.size() == 1);

                //}
            }else if (TextUtils.isEmpty(brandId) == false) {
                ResponseData response = PMPServerManager.getShared().getServerResponse();
                for (Brands b : response.getBrands()) {
                    if (b.getExternalId() != null && b.getExternalId().equals(brandId)) {
                        ArrayList<Pois> pois = new ArrayList<>();
                        for (Pois p : response.getPois()) {
                            if (p.getBrandId() == b.getId()) {
                                pois.add(p);
                            }
                        }
                        showPOIListOnMap(pois, b, true, true, true, pois.size() == 1);
                        break;
                    }
                }

            }else if (TextUtils.isEmpty(poiCategoryId) == false) {
                ResponseData response = PMPServerManager.getShared().getServerResponse();
                for (PoiCategories cat : response.getFlattenPoiCategories()) {
                    if (cat.getExternalId() != null && cat.getExternalId().equals(poiCategoryId)) {
                        ArrayList<Pois> pois = new ArrayList<>();
                        for (Pois p : response.getPois()) {
                            for (int catId : p.getPoiCategoryIds()) {
                                if (catId == cat.getId()) {
                                    pois.add(p);
                                    break;
                                }
                            }
                        }
                        showPOIListOnMap(pois, cat, cat.isHasDetails(), true, true, pois.size() == 1);
                        break;
                    }
                }
            }
        }
    }

    public CocosFragment.CocosFragmentCallback getCocosFragmentCallback() {
        return cocosFragmentCallback;
    }

    public void setCocosFragmentCallback(CocosFragment.CocosFragmentCallback cocosFragmentCallback) {
        this.cocosFragmentCallback = cocosFragmentCallback;
    }

    public PMPMapFragmentCallback getCallback() {
        return callback;
    }

    public void setCallback(PMPMapFragmentCallback callback) {
        this.callback = callback;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PMPCameraManager.PMP_PERMISSIONS_REQUEST_CAMERA: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        System.out.println("Permissions --> " + "Permission Granted: " + permissions[i]);
                        addCameraView();
                        showARWithoutChecking();
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        System.out.println("Permissions --> " + "Permission Denied: " + permissions[i]);

                    }
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                PMPIndoorLocationManager.getSharedPMPManager(getActivity()).startDetection();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMapView = googleMap;



        googleMapView.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                if(PMPServerManager.getShared(getContext()) != null &&
                        PMPServerManager.getShared(getContext()).getServerResponse() != null){
                    ResponseData response = PMPServerManager.getShared(getContext()).getServerResponse();
                    googleMapMoveToLocation(response.getMapLat(), response.getMapLng());
                }
            }
        });
        googleMapView.getUiSettings().setIndoorLevelPickerEnabled(false);
        googleMapView.getUiSettings().setMapToolbarEnabled(false);

        googleMapView.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        PMPMapFragment.this.onDeselectAllPOI();
                    }
                });
            }
        });
/*
        BitmapDescriptor overlayBitmap = BitmapDescriptorFactory.fromResource(R.drawable.outline_building);
        //hardcode value for TaiKoo place
//        LatLngBounds bounds = new LatLngBounds(new LatLng(22.288249, 114.210049), new LatLng(22.285777, 114.213809));
//        LatLngBounds bounds = new LatLngBounds(new LatLng(22.285777, 114.210049), new LatLng(22.288249, 114.213809));

        GroundOverlayOptions overlay = new GroundOverlayOptions().image(overlayBitmap).positionFromBounds(bounds);
        googleMapView.addGroundOverlay(overlay);

        MarkerOptions option = new MarkerOptions().position(SWIRE_PIN_LOCATION).icon(BitmapDescriptorFactory.fromResource(R.drawable.swire_pin));
        googleMapView.addMarker(option);*/
//        btn_switch_map.setVisibility(View.VISIBLE);

        googleMapView.setOnCameraMoveCanceledListener(new GoogleMap.OnCameraMoveCanceledListener() {
            @Override
            public void onCameraMoveCanceled() {
                Log.e(TAG, "onCameraMoveCanceled");
//                locateMeButton.setBackgroundResource(R.drawable.btn_locate);
            }
        });

        googleMapView.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                Log.e(TAG, "onCameraMove");
/*
                boolean isInsideTaikooPlace = GeoCoordinate.isInsidePolygon(new PointF((float) googleMapView.getCameraPosition().target.latitude, (float)googleMapView.getCameraPosition().target.longitude), TAIKOOPLACE_REGION);
//    BOOL isInsideTaikooPlace = YES;
//                    NSLog(@"%@", isInsideTaikooPlace ? @"Inside": @"Outside");
                if (!isIndoorMode && !isOverViewModeEnable) {
                    btn_switch_map.setVisibility(!isInsideTaikooPlace || googleMapView.getCameraPosition().zoom < 18.5 ? View.GONE : View.VISIBLE);
                }
                */
            }
        });

        googleMapView.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int i) {
                Log.e(TAG, "onCameraMoveStarted");
//                locateMeButton.setBackgroundResource(R.drawable.btn_locate);
            }
        });

    }

    private boolean needOverview() {

        CoreEngine coreEngine = CoreEngine.getInstance();
        RouteStepIndicator[] steps = coreEngine.getRouteStepIndicators();
        RouteStepIndicator step1 = steps[0];
        RouteStepIndicator step2 = steps[steps.length - 1];

        float minX = Float.MAX_VALUE,
                minY = Float.MAX_VALUE,
                maxX = Float.MIN_VALUE,
                maxY = Float.MIN_VALUE;
        minX = Math.min(minX, step1.getX());
        minY = Math.min(minY, step1.getY());
        maxX = Math.max(maxX, step1.getX());
        maxY = Math.max(maxY, step1.getY());

        minX = Math.min(minX, step2.getX());
        minY = Math.min(minY, step2.getY());
        maxX = Math.max(maxX, step2.getX());
        maxY = Math.max(maxY, step2.getY());

        final RectF bounds = new RectF(minX, minY, maxX, maxY);
        final DisplayMetrics metrics = getResources().getDisplayMetrics();
        RectF padding = new RectF();

        padding.left = 20;
        padding.right = 20;
        padding.top = (routeOverviewHeaderView.getHeight() +
                notificationView.getHeight() +
                errorHeaderView.getHeight() + 56) / metrics.density + 20.0f;
        padding.bottom = (routeOverviewFooterView.getHeight()+ 24) / metrics.density + 20.0f;
        float scale = PMPMapController.getInstance().scaleForBounds(bounds, padding);
        float overviewScale = Float.MAX_VALUE;
        Maps selectedMap = floorAdapter.getSelectedMap();
        if (selectedMap.getImages() != null) {
            for (MapImage img : selectedMap.getImages()) {
                if (overviewScale > img.getThreshold()) {
                    overviewScale = img.getThreshold();
                }
            }
        }
        boolean needOverview = scale <= overviewScale;
        return needOverview;
    }



    private void asyncCheckSavedFlight() {
        PMPProximityServerManager.getShared(this.getContext()).getUserSavedFlight(new PMPProximityServerManagerNotifier() {
            @Override
            public void didSuccess(Object response) {
                if (response instanceof SaveUserFlightResponse) {
                    SaveUserFlightResponse r = (SaveUserFlightResponse) response;
                    if (r != null && r.getResult() != null) {
                        String mtelRecordId = TextUtils.isEmpty(r.getResult().getMtelRecordId())?"":r.getResult().getMtelRecordId();
                        String flightNo = TextUtils.isEmpty(r.getResult().getPreferredIdentifier())?"":r.getResult().getPreferredIdentifier();
                        String gate = TextUtils.isEmpty(r.getResult().getGate())?"":r.getResult().getGate();
                        String flightStatus = TextUtils.isEmpty(r.getResult().getFlightStatus())?"":r.getResult().getFlightStatus();
                        String departureTime = TextUtils.isEmpty(r.getResult().getDepartureTime())?"":r.getResult().getDepartureTime();

                        PMPMapController.getInstance().updateUserFlightForARView(mtelRecordId, flightNo, gate, flightStatus, departureTime);
                    }
                }
            }

            @Override
            public void didFailure() {}
        });
    }

    private void setTabBarVisible(boolean b) {
        if (PMPMapSDK.getTabBarVisibilityUpdateCallback() != null) {
            PMPMapSDK.getTabBarVisibilityUpdateCallback().updateTabBarVisibilityUpdate(b);
        }
    }

    private void showOutOfCoverageAreaIfNeeded() {
        //redmine : #7710 - [Map - aOS] Out of coverage message not shown in AOS
        //sync logic with iOS
        //*** if([PMPCoreEngine sharedInstance].bluetoothEnabled && [PMPLocationHelper authorizated])
        boolean bluetoothEnabled, locationServiceEnabled;

        BluetoothAdapter btAdapter = ((Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR1)
                ?((BluetoothManager)getContext().getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter()
                :(BluetoothAdapter.getDefaultAdapter()));

        bluetoothEnabled = (btAdapter == null || btAdapter.getState() != BluetoothAdapter.STATE_ON)?false:true;
        locationServiceEnabled = (!GPSLocationManager.checkIfLocationPermissionAllowed(getContext()))?false:true;
        if (bluetoothEnabled &&
                locationServiceEnabled &&
                CoreEngine.getInstance().getIndoorLocation() == null) {
            showCurrentArea(true);
        }
    }

    // adding camera
    private void addCameraView() {
        if(preview == null) {
            preview = new FrameLayout(getContext());
            preview.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            PMPCameraManager.getSharedManager(getContext()).addCameraView(getContext(), preview);

            cameraContainer.addView(preview);
        }
    }

    public void setCameraVisibility(final boolean isVisible) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (cameraContainer != null){
                    cameraContainer.setVisibility(isVisible ? View.VISIBLE : View.GONE);
                }
            }
        });
    }

    private boolean configCamera(){
        if (PMPCameraManager.checkIfCameraHardwareAvailable(getContext())){
            if (PMPCameraManager.checkIfCameraPermissionAllowed(getContext())){
                addCameraView();
                return true;
            } else {
                PMPCameraManager.getSharedManager(getContext()).requestCameraPermissionIfNeeded(this);
            }
        } else {
            Log.e(TAG, "no camera in this device");
        }
        return false;
    }

    private void setARViewInterfaceForStatus(int status) {
        if (status == CoreEngine.NavigationStepArrived) {
//            PMPMapController.getInstance().setARViewArrived(true);
//            PMPMapController.getInstance().setARScreenBlocked(false);
            interactionBlockerView.setVisibility(View.GONE);
        } else if (status == CoreEngine.NavigationStepBypassed) {
//            PMPMapController.getInstance().setARViewBypassed(true);
//            PMPMapController.getInstance().setARScreenBlocked(true);
            interactionBlockerView.setVisibility(View.GONE);
            interactionBlockerIconImageView.setImageResource(R.drawable.icon_bypass);
            interactionBlockerMessageLabel.setText(getString(R.string.PMPMAP_NAV_BYPASS_DEST));
            interactionBlockerView.setTag(PMPMapViewBlockerBypassed);
        }
    }

    private void showAR() {
        if (configCamera()) {
            showARWithoutChecking();
        }
    }

    private void showARWithoutChecking() {
        setCameraVisibility(true);
//        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)mapContainer.getLayoutParams();
//        layoutParams.addRule(RelativeLayout.ABOVE, R.id.footer_content);
//        mapContainer.setLayoutParams(layoutParams);

        boolean isScrBlocked = false;
        boolean isArrived = false;
        boolean isBypassed = false;

        int status = CoreEngine.getInstance().getCurrentNavigationStep();
        if (status == CoreEngine.NavigationStepArrived) {
            isArrived = true;
        } else if (status == CoreEngine.NavigationStepBypassed) {
            isScrBlocked = true;
            isArrived = true;
            isBypassed = true;
        }

        SaveUserFlightResponse response = PMPProximityServerManager.getShared().getSaveUserFlightResponse();
        String mtelRecordId = "";
        String flightNo = "";
        String gateCode = "";
        String flightStatus = "";
        String departureTime = "";
        if (response != null && response.getResult() != null) {
            com.pmp.mapsdk.cms.model.Result result = response.getResult();
            mtelRecordId = TextUtils.isEmpty(result.getMtelRecordId())?"":result.getMtelRecordId();
            flightNo = TextUtils.isEmpty(result.getFlightNo())?"":result.getFlightNo();
            gateCode = TextUtils.isEmpty(result.getGateCode())?"":result.getGateCode();
            flightStatus = TextUtils.isEmpty(result.getFlightStatus())?"":result.getFlightStatus();
            departureTime = TextUtils.isEmpty(result.getDepartureTime())?"":result.getDepartureTime();
        }

        PMPMapController.getInstance().showAR(toolbar.getHeight(), arNavigatingFooterView.getHeight(), PMPCameraManager.getSharedManager(getContext()).getHorizontalViewAngle(), isScrBlocked, isArrived, isBypassed,
                mtelRecordId,
                flightNo,
                gateCode,
                flightStatus,
                departureTime);

//        PMPMapController.getInstance().addARCallback(this);
        floorSwitchListV.setVisibility(View.INVISIBLE);
        navigatingHeaderView.setVisibility(View.GONE);
        navigatingFooterView.setVisibility(View.GONE);
        arNavigatingFooterView.setVisibility(View.VISIBLE);
        compassButton.setVisibility(View.GONE);
        recenterButton.setVisibility(View.GONE);
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

//        if(ttsManager != null) {
//            ttsButton.setVisibility(View.GONE);
//        }
        setARViewInterfaceForStatus(status);

        asyncCheckSavedFlight();
    }

    private void exitAR() {
        PMPMapController.getInstance().removeARCallback(this);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)mapContainer.getLayoutParams();
        layoutParams.removeRule(RelativeLayout.ABOVE);
        mapContainer.setLayoutParams(layoutParams);
        PMPMapController.getInstance().replaceSceneWithMapScene();
        setCameraVisibility(false);

//        navigatingHeaderViewFollowRoute.setVisibility(View.GONE);
//        btn_switch_map.setBackgroundResource(R.drawable.icon_ar_live);

        navigatingHeaderView.setVisibility(View.VISIBLE);
        navigatingFooterView.setVisibility(View.VISIBLE);
        arNavigatingFooterView.setVisibility(View.GONE);
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//        this.btn_switch_map.setAlpha(1);
//
//        if(ttsManager != null) {
//            ttsButton.setVisibility(View.VISIBLE);
//        }
    }

    @Override
    public void onRequestShowFlightDetails(final String recordId, final String preferredIdentifier) {
        if (!TextUtils.isEmpty(recordId)) {
            if (recordId != null && recordId.length() > 0) {
                String url = String.format("hkgmyflight://flightDetail?recordId=%s&flightNum=%s", recordId, preferredIdentifier.replace(" ", ""));

                if (PMPMapSDK.getOpenBrowserCallback() != null) {
                    PMPMapSDK.getOpenBrowserCallback().openBrowser(url);
                }
            }
        }
    }

    @Override
    public void onAwayFromRoute() {
//        if (isARViewShowing &&
//                interactionBlockerView.getVisibility() == View.GONE &&
//                lastOverlayTimeStamp + 1000 < System.currentTimeMillis()) {
//            lastOverlayTimeStamp = System.currentTimeMillis();
//            PMPMapController.getInstance().setARScreenBlocked(false);
//            interactionBlockerView.setVisibility(View.VISIBLE);
//            interactionBlockerIconImageView.setImageResource(R.drawable.icon_follow_the_route);
//            interactionBlockerMessageLabel.setText(getString(R.string.PMPMAP_NAV_FOLLOW_ROUTE));
//            interactionBlockerView.setTag(PMPMapViewBlockerFollowPath);
//
//            navigatingHeaderViewFollowRoute.setVisibility(View.VISIBLE);
//            navigatingHeaderViewFollowRoute.setIconImage(R.drawable.icon_nav_follow_the_route_small);
//            navigatingHeaderViewFollowRoute.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
//            navigatingHeaderViewFollowRoute.setInstruction(R.string.PMPMAP_NAV_FOLLOW_THE_ROUTE);
//            navigatingHeaderViewFollowRoute.setDistance(0);
//        }
    }

    @Override
    public void onReturnToRoute() {
//        if (isARViewShowing) {
//            if (interactionBlockerView.getVisibility() == View.VISIBLE &&
//                    interactionBlockerView.getTag() != null &&
//                    interactionBlockerView.getTag().equals(PMPMapViewBlockerFollowPath)) {
//                PMPMapController.getInstance().setARScreenBlocked(false);
//                interactionBlockerView.setVisibility(View.GONE);
//                navigatingHeaderViewFollowRoute.setVisibility(View.GONE);
//            }
//        }
    }

    @Override
    public void onCameraFacedDown() {
        Log.d(TAG, "Map - onCameraFacedDown : " + nativeShowingARScene());
//        if (isARViewShowing &&
//                lastOverlayTimeStamp + 1000 < System.currentTimeMillis()) {
//            lastOverlayTimeStamp = System.currentTimeMillis();
//            if (nativeShowingARScene()) {
//                exitAR();
//                btn_switch_map.setVisibility(View.GONE);
//                floorSwitchListV.setVisibility(View.VISIBLE);
//            }
//        }
        if (CoreEngine.getInstance().getMapMode() == CoreEngine.MapModeNavigating) {
           ;
            if (isARViewShowing && nativeShowingARScene()) {
                Log.d(TAG, "Dismiss AR");
                isARViewShowing = false;
                exitAR();
                floorSwitchListV.setVisibility(View.VISIBLE);


            }
        }

    }

    @Override
    public void onCameraFacedFront() {
        if (isShowingTiltReminder) {
            return;
        }

        Log.d(TAG, "Map - onCameraFacedFront : " + nativeShowingARScene());
        if (CoreEngine.getInstance().getMapMode() == CoreEngine.MapModeNavigating) {
            if (!isARViewShowing &&
                    !nativeShowingARScene() &&
                    PMPMapConfig.getSharedConfig().getEnableARNavigation(getContext()) &&
                    interactionBlockerView.getVisibility() != View.VISIBLE) {
                Log.d(TAG, "Show AR");
                showAR();
                floorSwitchListV.setVisibility(View.GONE);
                isARViewShowing = true;
            } else {
//                if (PMPMapController.getInstance().isARMasking()) {
//                    this.btn_switch_map.setEnabled(false);
//                    this.btn_switch_map.setAlpha(.54f);
//                } else {
//                    this.btn_switch_map.setEnabled(true);
//                    this.btn_switch_map.setAlpha(1);
//                }
            }
        }

    }
    @Override
    public void onInit(int status) {
//        if (status == TextToSpeech.SUCCESS) {
//            ttsControlLayout.initControlPanel(ttsManager);
//            cocosFragment.getGLSurfaceView().setOnTouchListener(new View.OnTouchListener(){
//                public boolean onTouch(View v, MotionEvent m) {
//                        ttsControlPanelFrame.setVisibility(View.INVISIBLE);
//                    return false;
//                }
//             });
//        } else {
//            this.ttsButton.setVisibility(View.INVISIBLE);
//        }
    }

    private void dismissArrivedDestinationNotification(){
        runOnUiThread(new Runnable(){
            public void run() {
                navigatingHeaderView.getOnCloseClickedListener().run();
            }});
    }


    public class MyTimerTask extends TimerTask
    {
        public void run()
        {
            dismissArrivedDestinationNotification();
        }
    };


    private void scheduledismissArrivedDestinationTimer(){
        dismissDestinationNoitceTimer = new Timer(true);
        dismissDestinationNoitceTimer.schedule(new MyTimerTask(),20000);
    }

    private void stopAndResetDismissArrivedDestinationTimer(){
        if(dismissDestinationNoitceTimer!=null) {
            dismissDestinationNoitceTimer.cancel();
            this.dismissDestinationNoitceTimer = null;
        }
    }

    private void showARTiltReminder() {
        /*
        if (((PMPMapConfig)PMPMapConfig.getSharedConfig()).getShowARReminder(getContext())) {
            isShowingTiltReminder = true;

            View checkBoxView = View.inflate(getActivity(), R.layout.alert_dialog_checkbox, null);
            CheckBox checkBox = (CheckBox) checkBoxView.findViewById(R.id.checkbox);
            checkBox.setChecked(!((PMPMapConfig)PMPMapConfig.getSharedConfig()).getShowARReminder(getContext()));
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    // Save to shared preferences
                    ((PMPMapConfig)PMPMapConfig.getSharedConfig()).setShowARReminder(getContext(), !isChecked);
                }
            });
            checkBox.setText(R.string.PMP_AV_NAV_INSTRUCTION_DO_NOT_SHOW_AGAIN);

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("");
            builder.setMessage(R.string.PMP_AR_NAV_INSTRUCTION)
                    .setView(checkBoxView)
                    .setCancelable(false)
                    .setPositiveButton("OKAY", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            isShowingTiltReminder = false;
                        }
                    })
                    .show();
        }*/
    }

    public static SharedPreferences mySharedPreferences;

    public static String getUserDefaultString(String key, String defaultValue) {
        if(PMPMapSDK.getApplication() != null) {
            if(mySharedPreferences == null){
                Context context = PMPMapSDK.getApplication();
                mySharedPreferences = context.getSharedPreferences("data" , MODE_PRIVATE);
            }
            String temp = mySharedPreferences.getString(key , defaultValue);
            return temp;
        }
        return defaultValue;
    }

    public static String setUserDefaultString(String key, byte[] saveValue) {
        if(PMPMapSDK.getApplication() != null) {
            if(mySharedPreferences == null){
                Context context = PMPMapSDK.getApplication();
                mySharedPreferences = context.getSharedPreferences("data" , MODE_PRIVATE);
            }
            String value = new String(saveValue, Charset.forName("UTF-8"));
            mySharedPreferences.edit().putString(key, value).apply();
            return "success";
        }
        return "fail";
    }

}

