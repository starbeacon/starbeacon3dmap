package com.pmp.mapsdk.app;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmap.PMPDataManager;
import com.cherrypicks.pmpmap.analytics.AnalyticsLogger;
import com.cherrypicks.pmpmap.ui.KeyboardEditText;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.cms.model.Name;
import com.pmp.mapsdk.cms.model.PoiCategories;
import com.pmp.mapsdk.cms.model.Pois;
import com.pmp.mapsdk.cms.model.ResponseData;
import com.pmp.mapsdk.external.PMPMapSDK;
import com.pmp.mapsdk.utils.PMPUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;


/**
 * Created by joeyyc on 24/11/2016.
 */

public class PMPSearchListFragment extends Fragment {
    public static final String ARGS_CATEGORY = "ARGS_CATEGORY";
    public static final String ARGS_TITLE = "ARGS_TITLE";

    private ImageButton btnBack;
    private ImageButton btnClear;
    private TextView tvHeader;
    private KeyboardEditText searchView;
    private Button btnSearch;
    private ListView listView;
    private List dataList;
    private List<Pois> filteredGates;
    private BaseAdapter mAdapter;
    private PoiCategories poiCategory;

    public static <V extends View> Collection<V> findChildrenByClass(ViewGroup viewGroup, Class<V> clazz) {

        return gatherChildrenByClass(viewGroup, clazz, new ArrayList<V>());
    }

    private static <V extends View> Collection<V> gatherChildrenByClass(ViewGroup viewGroup, Class<V> clazz, Collection<V> childrenFound) {

        for (int i = 0; i < viewGroup.getChildCount(); i++)
        {
            final View child = viewGroup.getChildAt(i);
            if (clazz.isAssignableFrom(child.getClass())) {
                childrenFound.add((V)child);
            }
            if (child instanceof ViewGroup) {
                gatherChildrenByClass((ViewGroup) child, clazz, childrenFound);
            }
        }

        return childrenFound;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle args = getArguments();

        //set views
        View view = inflater.inflate(R.layout.pmp_search_list, container, false);
        View header = view.findViewById(R.id.header);
        header.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());

        btnBack = (ImageButton)view.findViewById(R.id.btnBack);
        tvHeader = (TextView)view.findViewById(R.id.tvHeader);
        searchView = (KeyboardEditText)view.findViewById(R.id.searchView);
        RelativeLayout relativeLayout = (RelativeLayout)view.findViewById(R.id.rl_searchField);

        listView = (ListView)view.findViewById(R.id.listview);
        btnSearch = (Button)view.findViewById(R.id.btnSearch);
        btnClear = (ImageButton) view.findViewById(R.id.btn_clear);
        filteredGates = new ArrayList<Pois>();

        if (args != null) {
            poiCategory = (PoiCategories) args.getSerializable(ARGS_CATEGORY);
            tvHeader.setText(args.getString(PMPSearchListFragment.ARGS_TITLE));
        } else {
            tvHeader.setText("");
        }

        if (poiCategory != null) {
            relativeLayout.setVisibility(View.GONE);
            mAdapter = new CategoryAdapter();
        } else { //Around Me
            mAdapter = new AroundGateAdapter();
        }

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setText("");
                btnClear.setVisibility(View.GONE);
                searchView.requestFocus();

                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(searchView, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PMPUtil.hideKeyboard(getActivity());
                getFragmentManager().popBackStack();
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSearch.setVisibility(View.GONE);
                searchView.requestFocus();

                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(searchView, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    btnClear.setVisibility(View.GONE);
                    filteredGates.clear();
                } else {
                    if (btnClear != null) {
                        btnClear.setVisibility(View.VISIBLE);
                    }
                    filteredGates.clear();

                    for (Object obj : dataList) {
                        Pois poi = (Pois)obj;
                        for (Name name : poi.getName()) {
                            if (name.getContent().toLowerCase().contains(s.toString().toLowerCase())) {
                                filteredGates.add(poi);
                                break;
                            }
                        }
                    }
                }
                mAdapter.notifyDataSetInvalidated();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        searchView.setOnKeyboardListener(new KeyboardEditText.KeyboardListener() {
            public void onStateChanged(KeyboardEditText keyboardEditText, boolean showing) {
                if (showing == false) {
                    searchView.clearFocus();
                    if (searchView.getText().length()>0) {
                        btnSearch.setText("");
                    } else {
                        btnSearch.setText(getString(R.string.PMPMAP_SEARCH_GATE_PLACEHOLDER));
                    }
                    btnSearch.setVisibility(View.VISIBLE);
                } else {
                    btnSearch.setVisibility(View.GONE);
                }
            }
        });

        loadData();

        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PMPUtil.hideKeyboard(getActivity());

                PoiCategories item = (PoiCategories) mAdapter.getItem(position);
                ResponseData response = PMPDataManager.getSharedPMPManager(getActivity()).getResponseData();
                List<Pois> poisByCat = new ArrayList<Pois>();
                for (Pois poi : response.getPois()) {
                    for (int catId : poi.getPoiCategoryIds()) {
                        if (item.getId() == catId) {
                            poisByCat.add(poi);
                            break;
                        }
                    }
                }
                PMPMapFragment mapFragment = (PMPMapFragment) getParentFragment();
                for (int i = 0; i < getFragmentManager().getBackStackEntryCount(); i++) {
                    getFragmentManager().popBackStack();
                }
                mapFragment.showPOIListOnMap(poisByCat, item, item.isHasDetails(), true, true, !item.isHasDetails());

                if(item != null && item.getParentPoiCategory() != null && item.getParentPoiCategory().getExternalId() != null && item.getParentPoiCategory().getExternalId().equals("facilities_and_services")){
                    HashMap<String, Object> params = new HashMap<String, Object>();
                    params.put("selected_catalog", PMPUtil.getLocalizedStringByLangID(item.getName(), PMPMapSDK.Language_English));
                    AnalyticsLogger.getInstance().logEvent("Filter_FS_Poi", params);
                }
            }
        });

        return view;
    }

    private void loadData() {
        final PMPServerManager serverManager = PMPServerManager.getShared(getActivity());
        if (serverManager.getServerResponse() != null) {
            if (poiCategory != null) {
                dataList = poiCategory.getSub_categories();
                if(poiCategory.getExternalId() != null && poiCategory.getExternalId().equals("facilities_and_services")){
                    //facilities_and_services need to sort...
                    sortFacitityArray(dataList);
                }
            } else {
                dataList = new ArrayList<Pois>();
                ArrayList<Integer> gateIds = serverManager.getServerResponse().getGateIds();
                ArrayList<Pois> pois = serverManager.getServerResponse().getPois();
                if (gateIds != null && pois != null) {
                    for (int id : gateIds) {
                        for (Pois poi : pois) {
                            if ((int) poi.getId() == id) {
                                dataList.add(poi);
                                break;
                            }
                        }
                    }
                }

                sortGateArray(dataList);
            }
        } else {
            dataList = new ArrayList();
        }
    }

    public String greatestCommonPrefix(String a, String b) {
        int minLength = Math.min(a.length(), b.length());
        for (int i = 0; i < minLength; i++) {
            if (a.charAt(i) != b.charAt(i) || "0123456789".contains(a.substring(i, i+1))) {
                return a.substring(0, i);
            }
        }
        return a.substring(0, minLength);
    }

    public void sortFacitityArray(List<PoiCategories> array) {
        Collections.sort(array, new Comparator() {
            @Override
            public int compare(Object lhs, Object rhs) {
                PoiCategories lp = (PoiCategories)lhs;
                PoiCategories rp = (PoiCategories)rhs;

                String lnumber = "";
                String rnumber = "";

                lnumber = PMPUtil.getLocalizedStringByLangID(lp.getName(), PMPMapSDK.Language_English);
                rnumber = PMPUtil.getLocalizedStringByLangID(rp.getName(), PMPMapSDK.Language_English);


                return lnumber.compareTo(rnumber);
            }
        });
    }

    public void sortGateArray(List<Pois> array) {
        final String prefix;
        if (array.size() >= 2) {
            prefix = greatestCommonPrefix(PMPUtil.getLocalizedString(array.get(0).getName()), PMPUtil.getLocalizedString(array.get(1).getName()));
        } else {
            prefix = "";
        }

        Collections.sort(array, new Comparator() {
            @Override
            public int compare(Object lhs, Object rhs) {
                Pois lp = (Pois)lhs;
                Pois rp = (Pois)rhs;

                int lnumber = 0;
                int rnumber = 0;

                try {
                    lnumber = Integer.parseInt(PMPUtil.getLocalizedString(lp.getName()).replace(prefix, ""));
                } catch (Exception e) {}

                try {
                    rnumber = Integer.parseInt(PMPUtil.getLocalizedString(rp.getName()).replace(prefix, ""));
                } catch (Exception e) {}

                if (lnumber < rnumber) return -1;
                if (lnumber == rnumber) return 0;
                return 1;
            }
        });
    }

    private class CategoryAdapter extends BaseAdapter {
        public CategoryAdapter() {
        }

        @Override
        public Object getItem(int i) {
            return dataList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            if(convertView == null){
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_poi_cat, null);
            }

            PoiCategories poiCat;
            poiCat = (PoiCategories)dataList.get(position);
            TextView tv_name = (TextView)convertView.findViewById(R.id.tv_name);
            tv_name.setTextColor(PMPMapSDK.getMapUISetting().getGenericCellTextColor());
            tv_name.setText(PMPUtil.getLocalizedString(poiCat.getName()));
            ImageView iv = (ImageView)convertView.findViewById(R.id.iv_icon);
            iv.setImageDrawable(null);
            PMPUtil.setImageWithURL(iv, poiCat.getImage());
            return convertView;
        }

        @Override
        public int getCount() {
            return dataList.size();
        }
    }

    private class AroundGateAdapter extends BaseAdapter {
        public AroundGateAdapter() {
        }

        @Override
        public String getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            if(convertView == null){
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_poi_cat, null);
            }

            Pois poi;
            if (searchView.getText().length()>0) {
                poi = filteredGates.get(position);
            } else {
                poi = (Pois)dataList.get(position);
            }
            TextView tv_name = (TextView)convertView.findViewById(R.id.tv_name);
            tv_name.setTextColor(PMPMapSDK.getMapUISetting().getGenericCellTextColor());
            tv_name.setText(PMPUtil.getLocalizedString(poi.getName()));
            ImageView iv = (ImageView)convertView.findViewById(R.id.iv_icon);
            iv.setImageDrawable(null);
            PMPUtil.setImageWithURLForListView(iv, poi.getMarkerImage());
            convertView.setTag(position);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PMPUtil.hideKeyboard(getActivity());

                    PMPSearchAroundFragment fragment = new PMPSearchAroundFragment();
                    Pois poi;
                    if (searchView.getText().length()>0) {
                        poi = filteredGates.get((int)v.getTag());
                    } else {
                        poi = (Pois)dataList.get((int)v.getTag());
                    }

                    Bundle bundle = new Bundle();
                    bundle.putParcelable(PMPSearchAroundFragment.ARGS_GATE, poi);
                    fragment.setArguments(bundle);
                    if(poi != null && poi.getExternalId().contains("gate")){
                        //is gate POI
                        fragment.isComeFromGate = true;

                        HashMap<String, Object> params = new HashMap<String, Object>();
                        params.put("gate_id", (int)poi.getId());
                        AnalyticsLogger.getInstance().logEvent("Filter_Boarding_Gate_Id", params);
                    }

                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fragment_container, fragment, "PMPSearchAroundFragment")
                            .addToBackStack("")
                            .commit();

                }
            });

            return convertView;
        }

        @Override
        public int getCount() {
            if (searchView.getText().length()>0) {
                return filteredGates.size();
            }
            return dataList.size();
        }
    }
}
