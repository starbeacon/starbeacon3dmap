package com.pmp.mapsdk.app;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SwitchCompat;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cherrypicks.pmpmap.Coord;
import com.cherrypicks.pmpmap.PMPMapController;
import com.cherrypicks.pmpmap.PMPMapControllerWithFragment;
import com.cherrypicks.pmpmap.core.CoreEngine;
import com.cherrypicks.pmpmap.core.CoreEngineListener;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.cms.PMPPOIByDuration;
import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.cms.model.Areas;
import com.pmp.mapsdk.cms.model.Brands;
import com.pmp.mapsdk.cms.model.Maps;
import com.pmp.mapsdk.cms.model.Pois;
import com.pmp.mapsdk.cms.model.Promotions;
import com.pmp.mapsdk.cms.model.ResponseData;
import com.pmp.mapsdk.external.PMPMapSDK;
import com.pmp.mapsdk.location.PMPBeacon;
import com.pmp.mapsdk.location.PMPBeaconType;
import com.pmp.mapsdk.location.PMPLocation;
import com.pmp.mapsdk.location.PMPLocationManagerError;
import com.pmp.mapsdk.utils.PMPUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.pmp.mapsdk.app.PMPMapFragment.PMPMapViewModeNormal;

/**
 * Created by andrew on 31/10/2016.
 */

public class PMPPOIDetailFragment extends Fragment {
    private final static String TAG = "POI Detail";
    public static final String POI_ID = "PMP_POI_ID";
    public static final String BRAND_ID = "PMP_BRAND_ID";
    private final int CALL_PHONE_PREMISSION_REQUEST = 1;
    public String poiId;
    private String brandId;
    private ArrayList<Pois> myPoiList = new ArrayList<Pois>();
    private boolean myHaveBrandId;
    private boolean myHaveEshopUrl;
    private boolean myHaveWebsiteUrl;
    private TextView lblHeadTitle;
    private View btnShare;
    private ImageButton btnBack;
    private View loadingView;

    private ListView myListView;
    private String myExternalBrandId;
    private String myLocalBrandId;
    private int myTouchingShopRow;
    private boolean mySwitchOn;
    private Brands myBrand;
    private PMPPOIDetailFragment.ListViewAdapter myListViewAdapter = new PMPPOIDetailFragment.ListViewAdapter();
    private CoreEngineListener coreEngineListener = new CoreEngineListener() {
        @Override
        public void onEngineInitialDone() {
            CoreEngine.getInstance().removeListener(this);
            init();
        }

        @Override
        public void onEntryLiftDetected(double x, double y) {

        }

        @Override
        public void onEntryShuttleBusDetected(double x, double y) {

        }

        @Override
        public void onArrivalDestination() {

        }

        @Override
        public void onBypassDestination() {

        }

        @Override
        public void onResetBypassDest() {

        }

        @Override
        public void onReroute() {

        }

        @Override
        public void onClearSearchResult() {

        }

        @Override
        public void onStepStepMessageUpdate(int beaconType, String message, String poiMessage, String imageName, float totalDuration, float totalDistance) {

        }

        @Override
        public void onMapModeChanged(int newMapMode) {

        }

        @Override
        public void onAreaChanged(Areas area) {

        }

        @Override
        public void didIndoorLocationUpdated(PMPBeaconType beaconType, PMPLocation location) {

        }

        @Override
        public void didIndoorTransmissionsUpdated(List<PMPBeacon> transmissions) {

        }

        @Override
        public void didOutdoorLocationsUpdated() {

        }

        @Override
        public void didIndoorExitRegion() {

        }

        @Override
        public void didCompassUpdated(double direction) {

        }

        @Override
        public void didError(PMPLocationManagerError error) {

        }

        @Override
        public void onPathTypeChecked(int pathType, String message, String imageName) {

        }

        @Override
        public void onShopNearByPromoMessage(int promoId, int poiId) {

        }

        @Override
        public void onInZone(Promotions promotion, boolean isBackground) {

        }

        @Override
        public void onCameraFacedDown() {

        }

        @Override
        public void onCameraFacedFront() {

        }
    };

    private PMPMapControllerWithFragment mapControllerCallback = new PMPMapControllerWithFragment() {
        @Override
        public void onPOISelect(int poiId) {

        }

        @Override
        public void onDeselectAllPOI() {

        }

        @Override
        public void onMapCoordUpdate(Coord coord) {

        }

        @Override
        public void onMapSceneInitated(PMPMapFragment mapFragment) {

            CoreEngine.getInstance().setMapMode(CoreEngine.MapModeBrowsing);
            mapFragment.isShowCloseButtonOnTop.set(false);
            mapFragment.setViewMode(PMPMapViewModeNormal);
            mapFragment.setSelectedPOI(myPoiList.get(myTouchingShopRow));
            final DisplayMetrics metrics = getResources().getDisplayMetrics();
            PMPMapController.getInstance().setSelectedPOIId((int) myPoiList.get(myTouchingShopRow).getId());
            PMPMapController.getInstance().jumpToPosition((float) myPoiList.get(myTouchingShopRow).getX(),
                    (float) myPoiList.get(myTouchingShopRow).getY(),
                    (float) metrics.widthPixels / metrics.density / 2,
                    (float ) metrics.heightPixels / metrics.density / 2);

            PMPMapController.getInstance().removeCallback(this);

            for (Maps map : PMPServerManager.getShared().getServerResponse().getMaps()) {
                if (map.getId() == myPoiList.get(myTouchingShopRow).getMapId()) {
                    mapFragment.setSelectedMap(map,true);
                }
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //set views
        View view = inflater.inflate(R.layout.pmp_detail_poi_fragment, container, false);
        View topbar = view.findViewById(R.id.topbar);
        topbar.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
        myListView = (ListView) view.findViewById(R.id.myListView);
        myListView.setAdapter(myListViewAdapter);
        myListView.setBackgroundColor(Color.rgb(227,227,229));

        lblHeadTitle = (TextView) view.findViewById(R.id.lblHeadTitle);
        btnShare = view.findViewById(R.id.btnShare);
        btnBack = (ImageButton) view.findViewById(R.id.btnBack);
        loadingView = view.findViewById(R.id.loading_view);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onShareClicked();
            }
        });

//        btnMap.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onMapClicked();
//            }
//        });
//        btnCall.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onCallClicked();
//            }
//        });
//
//        btnWebsite.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onWebsiteClicked();
//            }
//        });

        float fontScale = PMPMapSDK.getFontScale();
//        lblImgTtl.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontScale * lblImgTtl.getTextSize());
//        lblPoiStatus.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontScale * lblPoiStatus.getTextSize());
//        lblAddress.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontScale * lblAddress.getTextSize());
//        lblOpenHrsValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontScale * lblOpenHrsValue.getTextSize());
//        lblDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontScale * lblDesc.getTextSize());
//        lblBookmarkTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontScale * lblBookmarkTitle.getTextSize());
        lblHeadTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX,fontScale * lblHeadTitle.getTextSize());
//        btnMap.setTextSize(TypedValue.COMPLEX_UNIT_PX,fontScale * btnMap.getTextSize());
//        btnCall.setTextSize(TypedValue.COMPLEX_UNIT_PX,fontScale * btnCall.getTextSize());
//        btnWebsite.setTextSize(TypedValue.COMPLEX_UNIT_PX,fontScale * btnWebsite.getTextSize());

        //get Arguments
        if (getArguments() != null) {
            poiId = getArguments().getString(POI_ID);
            brandId = getArguments().getString(BRAND_ID);
        }

        PMPServerManager serverManager = PMPServerManager.getShared(getActivity());
        if (serverManager.getServerResponse() == null) {
            CoreEngine.getInstance().addListener(coreEngineListener);
        } else {
            init();
        }


        return view;
    }

    public void init() {
        PMPServerManager serverManager = PMPServerManager.getShared(getActivity());

        if (serverManager.getServerResponse() == null) {
            return;
        }

        myHaveBrandId = false;
        myHaveEshopUrl = false;
        myHaveWebsiteUrl = false;
        myExternalBrandId = "0";
        myLocalBrandId = "0";
        myPoiList.clear();

        if(brandId!=null && !brandId.equals("")){
            ResponseData response = PMPServerManager.getShared().getServerResponse();
            for (Brands b : response.getBrands()) {
                if (b.getExternalId() != null && b.getExternalId().equals(brandId)) {
                    //this brandId have found a brand equal to its external id
                    myHaveBrandId = true;
                    myBrand = b;
                    ArrayList<Pois> pois = new ArrayList<>();
                    for (Pois p : response.getPois()) {
                        if (p.getBrandId() == b.getId()) {
                            myLocalBrandId = String.format("%f", p.getBrandId());
                            myExternalBrandId = b.getExternalId();
                            myPoiList.add(p);
                            String url = PMPUtil.getLocalizedString(myBrand.getEshopUrl());
                            if(url != null && b.getEshopUrl()!=null && b.getEshopUrl().size()!=0 && !url.equals("")){
                                myHaveEshopUrl = true;
                            }
                            if(p.getUrl()!=null && !p.getUrl().equals("")){
                                myHaveWebsiteUrl = true;
                            }
                        }
                    }
                    break;
                }
            }

            if( myPoiList.size()==0 || !myHaveBrandId){
                Toast.makeText(getActivity(), "No brand was found by the brandId", Toast.LENGTH_LONG).show();
                return;
            }

        }else{
            for (Pois p : serverManager.getServerResponse().getPois()) {
//                Log.d(TAG, "getExternalId: " + p.getExternalId());
                if (String.format("%.0f", p.getId()).equals(poiId) ||
                        (p.getExternalId() != null && p.getExternalId().equals(poiId))) {
                    ResponseData response = PMPServerManager.getShared().getServerResponse();
                    for (Brands b : response.getBrands()) {
                        if (b.getId () == p.getBrandId()) {
                            myExternalBrandId = b.getExternalId();
                            myLocalBrandId = String.format("%f", p.getBrandId());
                            myBrand = b;
                        }
                    }

                    if(p.getBrandId() != 0){
                        myHaveBrandId = true;
                    }

                    String url = null;
                    if(myBrand != null){
                        url = PMPUtil.getLocalizedString(myBrand.getEshopUrl());
                    }

                    if(url != null && myBrand!= null && url.length() > 0){
                        myHaveEshopUrl = true;
                    }
                    if(p.getUrl()!=null && !p.getUrl().equals("")){
                        myHaveWebsiteUrl = true;
                    }
                    myPoiList.add(p);
                    break;
                }
            }
        }

        if (CoreEngine.getInstance().getIndoorLocation() != null) {

            List<PMPPOIByDuration> poisWithDuration = PMPPOIByDuration.getAll();
            List<PMPPOIByDuration> selectedPoisWithDuration = new ArrayList<PMPPOIByDuration>();

            for (PMPPOIByDuration tempPoiWithDurations : poisWithDuration) {
                for (Pois tempPoi : myPoiList) {
                    if (tempPoiWithDurations.poi.getId() == tempPoi.getId()) {
                        PMPPOIByDuration tempPoiDuration = new PMPPOIByDuration(tempPoi, tempPoiWithDurations.duration);
                        selectedPoisWithDuration.add(tempPoiDuration);
                        break;
                    }
                }
            }

            Collections.sort(selectedPoisWithDuration, new Comparator<PMPPOIByDuration>() {
                @Override
                public int compare(PMPPOIByDuration o1, PMPPOIByDuration o2) {
                    if ((o1.duration) < (o2.duration)) return -1;
                    if ((o1.duration) == (o2.duration)) return 0;
                    return 1;
                }
            });

            myPoiList.clear();
            for (PMPPOIByDuration poiWithDuration : selectedPoisWithDuration) {
                myPoiList.add(poiWithDuration.poi);
            }
        }else{

            Collections.sort(myPoiList, new Comparator<Pois>(){
                public int compare(Pois obj1, Pois obj2) {
                    if(obj1.getAreaId()>obj2.getAreaId()){
                        return 1;
                    }else if(obj1.getAreaId()<obj2.getAreaId()){
                        return -1;
                    }else{
                        return 0;
                    }
                }
            });
            Collections.sort(myPoiList, new Comparator<Pois>(){
                public int compare(Pois obj1, Pois obj2) {
                    if(obj1.getRestricted() && !obj2.getRestricted()){
                        return 1;
                    }else if(!obj1.getRestricted() && obj2.getRestricted()){
                        return -1;
                    }else{
                        return 0;
                    }
                }
            });

        }



        lblHeadTitle.setText(PMPUtil.getLocalizedString(myPoiList.get(0).getName()));

        mySwitchOn = false;
        if(brandId!=null && !brandId.equals("")) {
            boolean selected = false;
            if (PMPMapSDK.getPoiDetailCallback() != null) {
                selected = PMPMapSDK.getPoiDetailCallback().retrievePOIBookmarkStatus(myExternalBrandId);
                mySwitchOn = selected;
                Log.d(TAG, "retrievePOIBookmarkStatus :" + selected);
            } else {
                Log.d(TAG, "retrievePOIBookmarkStatus is null");
            }
        }else{
            if(myHaveBrandId) {
                if (PMPMapSDK.getPoiDetailCallback() != null) {
                    boolean selected = false;
                    selected = PMPMapSDK.getPoiDetailCallback().retrievePOIBookmarkStatus(myExternalBrandId);
                    mySwitchOn = selected;
                    Log.d(TAG, "retrievePOIBookmarkStatus :" + selected);
                }else{
                    Log.d(TAG, "retrievePOIBookmarkStatus is null");
                }
            }
        }

        loadingView.setVisibility(View.GONE);

    }

    private void onCallClicked(int pos) {
        // Here, thisActivity is the current activity
        String tel = myPoiList.get(pos).prepareCalling();
        final String premission = Manifest.permission.CALL_PHONE;

        if (tel.length() == 0) {
            //show alert message
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Error").setMessage("Failed to call");
            builder.show();
        }
        if (ContextCompat.checkSelfPermission(getActivity(),
                premission)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (shouldShowRequestPermissionRationale(premission)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {
                myTouchingShopRow = pos;
                // No explanation needed, we can request the permission.
                requestPermissions(new String[]{premission}, CALL_PHONE_PREMISSION_REQUEST);
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + tel));
            startActivity(intent);
        }

    }

//    private void onMailClicked() {
//        Intent intent = new Intent(Intent.ACTION_SEND);
//
//        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{poi.getEmail()});
//        intent.putExtra(Intent.EXTRA_SUBJECT, "");
//        intent.putExtra(Intent.EXTRA_TEXT, "");
//
//        intent.setType("message/rfc822");
//
//        startActivity(Intent.createChooser(intent, "Select Email Sending App :"));
//    }

    private void onWebsiteClicked() {
        if (PMPMapSDK.getOpenBrowserCallback() != null) {
            PMPMapSDK.getOpenBrowserCallback().openBrowser(myPoiList.get(0).getUrl());
        }
    }

    private void onShopNowClicked(){
//        if (PMPMapSDK.getOpenBrowserCallback() != null) {
//            PMPMapSDK.getOpenBrowserCallback().openBrowser(PMPUtil.getLocalizedString(myBrand.getEshopUrl()));
//        }
        String url = PMPUtil.getLocalizedString(myBrand.getEshopUrl());
        if(url != null && URLUtil.isValidUrl(url)){
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        }
    }

    private void onShareClicked() {
        Log.d(TAG, "onShareClicked: myHaveBrandId = "+ myHaveBrandId + ", callback != null?" + (PMPMapSDK.getPoiDetailCallback() != null) );
        if(myHaveBrandId) {
            String brandId = myExternalBrandId,
                    brandName = PMPUtil.getLocalizedString(myPoiList.get(0).getName());

            if (PMPMapSDK.getPoiDetailCallback() != null) {
                PMPMapSDK.getPoiDetailCallback().onBrandShareButtonClicked(brandId, brandName);
            }
        }
        /*
        String shareBody = "Here is the share content body";
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(intent, "Select Share Platform_Android"));
        */
    }

    private void onMapClicked(int pos) {
        final DisplayMetrics metrics = getResources().getDisplayMetrics();
        Fragment parentFragment = getParentFragment();
        if (parentFragment instanceof PMPMapFragment) {
            int count = getFragmentManager().getBackStackEntryCount();
            while (count != 0) {
                getFragmentManager().popBackStack();
                count--;
            }

            PMPMapFragment mapFragment = (PMPMapFragment) getParentFragment();

            mapFragment.setSelectedPOI(myPoiList.get(pos));
            PMPMapController.getInstance().jumpToPosition((float) myPoiList.get(pos).getX(),
                    (float) myPoiList.get(pos).getY(),
                    (float) metrics.widthPixels / metrics.density / 2,
                    (float ) metrics.heightPixels / metrics.density / 2);
            for (Maps map : PMPServerManager.getShared().getServerResponse().getMaps()) {
                if (map.getId() == myPoiList.get(pos).getMapId()) {
                    ((PMPMapFragment) parentFragment).setSelectedMap(map,true);
                }
            }
        } else {
            final PMPMapFragment mapFragment = new PMPMapFragment();
            myTouchingShopRow = pos;
            mapFragment.setCallback(new PMPMapFragment.PMPMapFragmentCallback() {
                @Override
                public void onCreate() {

                }

                @Override
                public void onSetupContent() {
                    mapControllerCallback.onMapSceneInitated(mapFragment);
                }
            });

            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.map_detail_content_holder, mapFragment)
                    .commit();

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CALL_PHONE_PREMISSION_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    onCallClicked(myTouchingShopRow);
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }
            break;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        CoreEngine.getInstance().removeListener(coreEngineListener);
        PMPMapController.getInstance().removeCallback(mapControllerCallback);
    }

    private  boolean checkIfUsingPoiId(){
        if(brandId!=null && !brandId.equals("")) {
            return false;
        }else{
            return true;
        }
    }

    private class ListViewAdapter extends BaseAdapter {
        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {

            if(position == 0){
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_poi_detail_image,null);
                ImageViewHolder holder = new ImageViewHolder(
                        (ImageView)convertView.findViewById(R.id.imgVwPOI),
                        (TextView)convertView.findViewById(R.id.lblImgTtl)
                );

                if(checkIfUsingPoiId()){
                    PMPUtil.setImageWithURL(holder.myImageView, myPoiList.get(0).getImage());
                    holder.myShopNameTextView.setText(PMPUtil.getLocalizedString(myPoiList.get(0).getName()));
                }else {
                    PMPUtil.setImageWithURL(holder.myImageView, myPoiList.get(0).getImage());
                    holder.myShopNameTextView.setText(PMPUtil.getLocalizedString(myPoiList.get(0).getName()));
                }
                convertView.setTag(holder);
            }else if(position == 1){
                //save shop
                if(myHaveBrandId && myHaveEshopUrl){
                    convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_poi_detail_topic, null);
                    TopicViewHolder holder = new TopicViewHolder(
                            (SwitchCompat) convertView.findViewById(R.id.bookmark_switch),
                            (TextView) convertView.findViewById(R.id.bookmark_title),
                            (Button) convertView.findViewById(R.id.btn_shop_now)
                    );
                    holder.myShopNowButton.setText(PMPMapFragment.getLocalizedStringWithSpecifiedLanguage("PMPMAP_POIDETAIL_SHOP_ONLINE_NOW",PMPMapSDK.getLangID()));
                    holder.myShopNowButton.setTextColor(Color.WHITE);

                    holder.myShopNowButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onShopNowClicked();
                        }
                    });
                    holder.mySwitchCompat.setChecked(mySwitchOn);
                    holder.mySwitchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            Log.d(TAG,"bookmark");
                            if (PMPMapSDK.getPoiDetailCallback() != null) {
                                PMPMapSDK.getPoiDetailCallback().onBookmarkClicked(myExternalBrandId);
                                Log.d(TAG, "bookmark clicked :" + myExternalBrandId);
                            }
                        }
                    });

                    holder.myBookMarkTextView.setText(PMPMapFragment.getLocalizedStringWithSpecifiedLanguage("PMPMAP_POIDETAIL_SAVE_SHOP",PMPMapSDK.getLangID()));
                    convertView.setTag(holder);

                }else if(myHaveBrandId && !myHaveEshopUrl) {
                    convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_poi_detail_save_shop, null);
                    SaveShopViewHolder holder = new SaveShopViewHolder(
                            (SwitchCompat) convertView.findViewById(R.id.bookmark_switch),
                            (TextView) convertView.findViewById(R.id.bookmark_title)
                    );
                    holder.mySwitchCompat.setChecked(mySwitchOn);
                    holder.mySwitchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            Log.d(TAG,"bookmark");
                            if (PMPMapSDK.getPoiDetailCallback() != null) {
                                PMPMapSDK.getPoiDetailCallback().onBookmarkClicked(myExternalBrandId);
                                Log.d(TAG, "bookmark clicked :" + myExternalBrandId);
                            }
                        }
                    });

                    holder.myBookMarkTextView.setText(PMPMapFragment.getLocalizedStringWithSpecifiedLanguage("PMPMAP_POIDETAIL_SAVE_SHOP",PMPMapSDK.getLangID()));
                    convertView.setTag(holder);


                }else if(!myHaveBrandId && myHaveEshopUrl){
                    convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_poi_detail_shop_now, null);
                    ShopNowViewHolder holder = new ShopNowViewHolder(
                            (Button) convertView.findViewById(R.id.btn_shop_now)
                    );
                    holder.myShopNowButton.setText("Shop Online Now");
                    holder.myShopNowButton.setTextColor(Color.WHITE);
                    convertView.setTag(holder);
                    holder.myShopNowButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onShopNowClicked();
                        }
                    });
                }else{
                    convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_poi_detail_dummy, null);
                }

            }
            else if(position <= myPoiList.size()+1){
                //shops cell
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_poi_detail_shops,null);

                RelativeLayout temp = (RelativeLayout)convertView.findViewById(R.id.viewGroupAddress);

                if (position == 2) {
                    if(position%2==1) {
                        temp.setBackgroundResource(R.drawable.cell_poi_detail_round_top_corner_white);
                    }else{
                        temp.setBackgroundResource(R.drawable.cell_poi_detail_round_top_corner);
                    }
                    if(position == myPoiList.size()+1){
                        if(!myHaveWebsiteUrl){
                            if(position%2==0){
                                temp.setBackgroundResource(R.drawable.cell_poi_detail_round_all_corner_white);
                            }else{
                                temp.setBackgroundResource(R.drawable.cell_poi_detail_round_all_corner);
                            }
                        }
                    }
                }else{
                    if(position == myPoiList.size()+1){
                        if(!myHaveWebsiteUrl) {
                            if (position % 2 == 1) {
                                temp.setBackgroundResource(R.drawable.cell_poi_detail_round_bottom_corner_white);
                            } else {
                                temp.setBackgroundResource(R.drawable.cell_poi_detail_round_bottom_corner);
                            }
                        }else{
                            temp.setBackgroundResource(R.drawable.cell_poi_detail_normal);
                            if(position%2==1){
                                temp.setBackgroundResource(R.drawable.cell_poi_detail_normal_white);
                            }
                        }
                    }else {
                        temp.setBackgroundResource(R.drawable.cell_poi_detail_normal);
                        if(position%2==1){
                            temp.setBackgroundResource(R.drawable.cell_poi_detail_normal_white);
                        }
                    }
                }

                ShopsViewHolder holder = new ShopsViewHolder(
                        (TextView)convertView.findViewById(R.id.lblStatus),
                        (TextView)convertView.findViewById(R.id.lblOpenHrsValue),
                        (TextView)convertView.findViewById(R.id.lblAddress),
                        (Button)convertView.findViewById(R.id.btnMap),
                        (Button)convertView.findViewById(R.id.btnCall)
                );

                if(myPoiList.get(position-2).getTelephone()==null ||myPoiList.get(position-2).getTelephone()==""){
                    holder.myCallBtn.setEnabled(false);
                }
                holder.myCallBtn.setTag(position-1);

                holder.myCallBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (Integer)v.getTag();
                        onCallClicked(pos-1);
                    }
                });

                holder.myLocationBtn.setTag(position-1);
                holder.myLocationBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (Integer)v.getTag();
                        onMapClicked(pos-1);
                    }
                });


                holder.myAddressTextView.setText(PMPUtil.getLocalizedAddress(getActivity(), myPoiList.get(position-2)));
                holder.myAddressTextView.setTextColor(Color.rgb(135,135,135));

                int[] isOpenFlag = new int[1];

            String openingHr;
                if(checkIfUsingPoiId()) {
                    openingHr = CoreEngine.getInstance().parseScheduleByPOIID((int) myPoiList.get(position-2).getId(), isOpenFlag);
                }else{
                    openingHr = CoreEngine.getInstance().parseScheduleByPOIID((int) myPoiList.get(position-2).getId(), isOpenFlag);
                }

            boolean poiClosed = (isOpenFlag[0] == 0);
            if (poiClosed) {
                holder.myOpenNowTextView.setEnabled(false);
                holder.myOpenNowTextView.setText(PMPMapFragment.getLocalizedStringWithSpecifiedLanguage("PMPMAP_POIDETAIL_CLOSED",PMPMapSDK.getLangID()));
            } else {
                holder.myOpenNowTextView.setEnabled(true);
                holder.myOpenNowTextView.setText(PMPMapFragment.getLocalizedStringWithSpecifiedLanguage("PMPMAP_POIDETAIL_OPEN_NOW",PMPMapSDK.getLangID()));
            }

                PMPServerManager serverManager = PMPServerManager.getShared(getActivity());

                String openingHrsString = PMPUtil.getLocalizedString(myPoiList.get(position-2).getBusinessHours());
                openingHrsString = openingHrsString.replace("), ","), \n");
//                openingHrsString = openingHrsString.replace("\u21B5","\n");
                holder.myOpenHrsTextView.setText(openingHrsString);
//                try {
//                    if(checkIfUsingPoiId()) {
//                        holder.myOpenHrsTextView.setText(PMPUtil.getLocalizedString(myPoiList.get(position-2).getBusinessHours()));
//                    }else{
//                        holder.myOpenHrsTextView.setText(PMPUtil.getLocalizedString(myPoiList.get(position-2).getBusinessHours()));
//                    }
//                } catch (Exception e) {
//
//                }
                convertView.setTag(holder);
            }else if(position == myPoiList.size()+2){
                //website
                if(myHaveWebsiteUrl) {
                    convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_poi_detail_website, null);
                    LinearLayout temp = (LinearLayout) convertView.findViewById(R.id.websiteLinearLayout);
//                    if(position%2==1) {
                        temp.setBackgroundResource(R.drawable.cell_poi_detail_round_bottom_corner_white);
//                    }else{
//                        temp.setBackgroundResource(R.drawable.cell_poi_detail_round_bottom_corner);
//                    }

                    WebsiteViewHolder holder = new WebsiteViewHolder(
                            (Button)convertView.findViewById(R.id.btn_website)
                    );
                    float fontScale = PMPMapSDK.getFontScale();

                    holder.myWebsiteBtn.setTextSize(TypedValue.COMPLEX_UNIT_PX,fontScale * holder.myWebsiteBtn.getTextSize());
                    holder.myWebsiteBtn.setText(myPoiList.get(0).getUrl());
                    holder.myWebsiteBtn.setTextColor(Color.rgb(135,135,135));


                    holder.myWebsiteBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onWebsiteClicked();
                        }
                    });
                    convertView.setTag(holder);
                }else{
                    convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_poi_detail_dummy,null);
                }
            }else if(position == myPoiList.size()+3){

                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_poi_detail_content,null);
                ContentViewHolder holder = new ContentViewHolder(
                        (TextView)convertView.findViewById(R.id.lblDesc)
                );

//                 holder.myContentTextView.setBackgroundResource(R.drawable.cell_poi_detail_normal_white);

                String descStr = null;
                PMPServerManager serverManager = PMPServerManager.getShared(getActivity());
                try {
                    if(checkIfUsingPoiId()){
                        if (PMPUtil.getLocalizedString(myPoiList.get(0).getDescription()).length() > 0) {
                            descStr = PMPUtil.getLocalizedString(myPoiList.get(0).getDescription());
                        } else {
                            for (Brands b : serverManager.getServerResponse().getBrands()) {
                                if (String.format("%.0f", b.getId()).equals(String.format("%.0f", myPoiList.get(0).getBrandId()))) {
                                    descStr = PMPUtil.getLocalizedString(b.getDescription());
                                    break;
                                }
                            }
                        }
                    }else{
                        if (PMPUtil.getLocalizedString(myPoiList.get(0).getDescription()).length() > 0) {
                            descStr = PMPUtil.getLocalizedString(myPoiList.get(0).getDescription());
                        } else {
                            for (Brands b : serverManager.getServerResponse().getBrands()) {
                                if (String.format("%.0f", b.getId()).equals(String.format("%.0f", myPoiList.get(0).getBrandId()))) {
                                    descStr = PMPUtil.getLocalizedString(b.getDescription());
                                    break;
                                }
                            }
                        }
                    }

                } catch (Exception e) {

                }
                if (TextUtils.isEmpty(descStr)) {
                    holder.myContentTextView.setText(null);
                    convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_poi_detail_dummy, null);
                    convertView.setTag(holder);
                }else {
                    CharSequence desc = Html.fromHtml(descStr);
                    holder.myContentTextView.setText(desc);
                    holder.myContentTextView.setTextColor(Color.rgb(135,135,135));
                    convertView.setTag(holder);
                }


            }else{
                int temp = position;
            }

            return convertView;
        }

        @Override
        public int getCount() {

            if(brandId !=null && brandId !="") {
                if (!myHaveBrandId) {
                    return 0;
                }
            }
            if(myPoiList.size()==0){
                return 0;
            }
            return 4 + myPoiList.size();
        }

        private class ImageViewHolder{
            ImageView myImageView;
            TextView myShopNameTextView;
            public ImageViewHolder(ImageView imageView, TextView shopNameTextView){
                this.myImageView = imageView;
                this.myShopNameTextView = shopNameTextView;
            };
        }

        private class TopicViewHolder{
            SwitchCompat mySwitchCompat;
            TextView myBookMarkTextView;
            Button myShopNowButton;
            public TopicViewHolder( SwitchCompat switchCompat, TextView bookMarkTextView, Button shopNowButton){
                this.mySwitchCompat = switchCompat;
                this.myBookMarkTextView = bookMarkTextView;
                this.myShopNowButton = shopNowButton;
            }
        }

        private class SaveShopViewHolder{
            SwitchCompat mySwitchCompat;
            TextView myBookMarkTextView;
            public SaveShopViewHolder(SwitchCompat switchCompat, TextView bookMarkTextView){
                this.mySwitchCompat = switchCompat;
                this.myBookMarkTextView = bookMarkTextView;
            };
        }
        private class ShopNowViewHolder{
            Button myShopNowButton;
            public ShopNowViewHolder(Button shopNowButton){
                this.myShopNowButton = shopNowButton;
            }
        }

        private class ShopsViewHolder {
            TextView myOpenNowTextView;
            TextView myOpenHrsTextView;
            TextView myAddressTextView;
            Button myLocationBtn;
            Button myCallBtn;
            public ShopsViewHolder(TextView openNowTextView, TextView openHrsTextView, TextView addressTextView, Button locationBtn, Button callBtn){
                this.myOpenNowTextView = openNowTextView;
                this.myOpenHrsTextView = openHrsTextView;
                this.myAddressTextView = addressTextView;
                this.myLocationBtn = locationBtn;
                this.myCallBtn = callBtn;
            };
        }

        private class WebsiteViewHolder {
            Button myWebsiteBtn;

            public WebsiteViewHolder(Button websiteBtn) {
              this.myWebsiteBtn = websiteBtn;
            };
        }

        private class ContentViewHolder {
            TextView myContentTextView;
            public ContentViewHolder(TextView contentTextView) {
                this.myContentTextView = contentTextView;
            };
        }
    }

}
