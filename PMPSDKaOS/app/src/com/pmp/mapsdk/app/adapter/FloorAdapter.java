package com.pmp.mapsdk.app.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.cherrypicks.pmpmap.PMPMapController;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.app.view.FloorItemView;
import com.pmp.mapsdk.cms.model.Maps;
import com.pmp.mapsdk.external.PMPMapSDK;

import java.util.HashMap;
import java.util.List;

/**
 * Created by andre on 11/30/2016.
 */

public class FloorAdapter extends BaseAdapter {
    private List<Maps> floors;
    private Maps selectedMap;
    private OnItemClickListener onItemClickListener;
    private HashMap<Integer, String> indicatorMap;
    public interface OnItemClickListener {
        public void onItemClick(Maps item);
    }

    public FloorAdapter( List<Maps> floors) {
        this.floors = floors;
    }

    public OnItemClickListener getOnItemClickListener() {
        return onItemClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public Maps getSelectedMap() {
        return selectedMap;
    }

    public int getMapIndex(Maps map) {
        for (int i = 0; i <  floors.size(); i++ ){
            if(floors.get(i).getId() == map.getId()){
                return i;
            }
        }

        return -1;
    }

    public void setSelectedMap(Maps selectedMap) {
        this.selectedMap = selectedMap;
        PMPMapController.getInstance().setCurrnetFloorById((int) selectedMap.getId());
    }

    public HashMap<Integer, String> getIndicatorMap() {
        return indicatorMap;
    }

    public void setIndicatorMap(HashMap<Integer, String> indicatorMap) {
        this.indicatorMap = indicatorMap;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FloorItemView floorItemView = (FloorItemView) LayoutInflater.from(parent.getContext()).inflate(R.layout.pmp_cell_floor, parent, false);
        ViewGroup.LayoutParams params = floorItemView.getLayoutParams();
        params.height = PMPMapSDK.getMapUISetting().getFloorCellHeight();
        floorItemView.setLayoutParams(params);

        final Maps map = floors.get(position);
        String indicatorStr = null;
        if (indicatorMap != null) {
            if (indicatorMap.containsKey((int)map.getId())) {
                indicatorStr = indicatorMap.get((int)map.getId());
            }
        }

        floorItemView.setTitle(map.getName());
        floorItemView.setSelectedStatus( map == selectedMap );
        floorItemView.setIndictorText(indicatorStr);
        floorItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(map);
                }
            }
        });

        return floorItemView;
    }

    @Override
    public long getItemId(int position) {
        Maps map = floors.get(position);

        return (long)map.getId();
    }

    @Override
    public Object getItem(int position) {
        return floors.get(position);
    }

    @Override
    public int getCount() {
        if (floors != null) {
            return floors.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private Maps map;
        private FloorItemView floorItemView;
        public ViewHolder(View itemView) {
            super(itemView);
            floorItemView = (FloorItemView) itemView;
            floorItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(map);
                    }
                }
            });
        }

        public Maps getMap() {
            return map;
        }

        public void setMap(Maps map, boolean selected, String indicator) {
            this.map = map;
            floorItemView.setTitle(map.getName());
            floorItemView.setSelected( selected );
            floorItemView.setIndictorText(indicator);
        }
    }
}
