package com.pmp.mapsdk.app.adapter;

/**
 * Created by andre on 11/30/2016.
 */

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.cherrypicks.pmpmap.PMPDataManager;
import com.cherrypicks.pmpmap.core.CoreEngine;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.app.view.POIGroupItemView;
import com.pmp.mapsdk.cms.PMPPOIByDuration;
import com.pmp.mapsdk.cms.model.Brands;
import com.pmp.mapsdk.cms.model.Maps;
import com.pmp.mapsdk.cms.model.PoiCategories;
import com.pmp.mapsdk.cms.model.Pois;
import com.pmp.mapsdk.utils.PMPUtil;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by andre on 11/30/2016.
 */

public class POIGroupAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private static final int FOOTER_VIEW = 1;
    private List<PMPPOIByDuration> pois;
    private OnItemClickListener onItemClickListener;
    public int footerHeight = 0;

    public POIGroupAdapter(List<Pois> pois) {
        this.pois = new ArrayList<PMPPOIByDuration>();

        List<PMPPOIByDuration> durationPOIs = PMPPOIByDuration.getAll();
        if (CoreEngine.getInstance().getIndoorLocation() == null) {
            //find center vertex
            Maps defaultMap = null;
            for (Maps map: PMPDataManager.getSharedPMPManager(null).getResponseData().getMaps()
                 ) {
                if (map.getDefaultProperty()) {
                    defaultMap = map;
                    break;
                }
            }

            if (defaultMap != null) {

            }

        }else {
            durationPOIs = PMPPOIByDuration.getAll();
        }
        for (Pois q : pois) {
            boolean found = false;
            for (PMPPOIByDuration p : durationPOIs) {
                if (p.poi.getId() == q.getId()) {
                    PMPPOIByDuration poiByDuration = new PMPPOIByDuration(q, p.duration);
                    PMPPOIByDuration.addDurationPOI(poiByDuration, this.pois);
                    found = true;
                    break;
                }
            }
            //set duration to -1 when we cannot deduce the distance between current location and poi
            if (!found) {
                PMPPOIByDuration poiByDuration = new PMPPOIByDuration(q, Float.MAX_VALUE);
                PMPPOIByDuration.addDurationPOI(poiByDuration, this.pois);
            }
        }
        if (CoreEngine.getInstance().getIndoorLocation() == null) {
            for (PMPPOIByDuration p : this.pois) {
                p.duration = Float.MAX_VALUE;
            }
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(Pois item);
    }

    public OnItemClickListener getOnItemClickListener() {
        return onItemClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public int getPOISize() {
        if (pois != null) {
            return pois.size();
        }

        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if (viewType == FOOTER_VIEW) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pmp_cell_poi_footer, parent, false);
            view.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, footerHeight));
            FooterViewHolder fvh = new FooterViewHolder(view);

            return fvh;
        }

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pmp_cell_poi_group, parent, false);
        POIGroupAdapter.ViewHolder viewHolder = new POIGroupAdapter.ViewHolder(parent.getContext(), view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        try {
            if (holder instanceof POIGroupAdapter.ViewHolder) {
                PMPPOIByDuration poi = pois.get(position);
                ((POIGroupAdapter.ViewHolder)holder).setPoi(poi);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (pois != null) {
            return pois.size()+1;
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == pois.size()) {
            return FOOTER_VIEW;
        }

        return super.getItemViewType(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private Context context;
        private PMPPOIByDuration poi;
        private POIGroupItemView groupItemView;

        public ViewHolder(Context context, View itemView) {
            super(itemView);
            this.context = context;
            groupItemView = (POIGroupItemView) itemView;
            groupItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(poi.poi);
                    }
                }
            });
        }

        public PMPPOIByDuration getPoi() {
            return poi;
        }

        public void setPoi(PMPPOIByDuration poi) {
            this.poi = poi;
            if(poi.poi.getImage() != null && poi.poi.getImage().length() > 0) {
                groupItemView.setImageURL(poi.poi.getImage());
            }else{

                PoiCategories cat = null;
                if (poi.poi.getPoiCategoryIds() != null &&
                        poi.poi.getPoiCategoryIds().size() != 0) {
                    int catNum = poi.poi.getPoiCategoryIds().get(poi.poi.getPoiCategoryIds().size()-1);
                    cat = PMPDataManager.getSharedPMPManager(null).getResponseData().getPoiCategoryMap().get(catNum);
                }
                if (cat == null && poi.poi.getBrandId() != 0) {
                    Brands brand = PMPDataManager.getSharedPMPManager(null).getResponseData().getBrandMap().get(poi.poi.getBrandId());
                    if (brand.getPoiCategoryIds() != null &&
                            brand.getPoiCategoryIds().size() != 0) {
                        int catNum = brand.getPoiCategoryIds().get(0);
                        cat = PMPDataManager.getSharedPMPManager(null).getResponseData().getPoiCategoryMap().get(catNum);
                    }
                }
                if (cat != null) {
                    String catImg = cat.getImage();
                    groupItemView.setImageURL(catImg);
                    int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, context.getResources().getDisplayMetrics());

                    groupItemView.setImagePadding(padding, padding, padding, padding);
                }
            }

            groupItemView.setDuration(this.context, poi.duration);
            groupItemView.setPOIName(PMPUtil.getLocalizedString(poi.poi.getName()));
            groupItemView.setAddress(PMPUtil.getLocalizedAddress(this.context, poi.poi));
        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {
        public FooterViewHolder(View itemView) {
            super(itemView);
        }
    }

    public List<PMPPOIByDuration> getPois() {
        return pois;
    }
}
