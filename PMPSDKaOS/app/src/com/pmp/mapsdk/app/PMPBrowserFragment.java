package com.pmp.mapsdk.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;

import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.external.PMPMapSDK;

/**
 * Created by gordonwong on 6/6/2017.
 */

public class PMPBrowserFragment extends Fragment {
    private ImageButton btnBack;
    private WebView wvBrowser;
    private String webViewContent;
    private boolean isUrl;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //set views
        View view = inflater.inflate(R.layout.pmp_browser_fragment, container, false);
        View topbar = view.findViewById(R.id.topbar);
        topbar.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
        btnBack = (ImageButton) view.findViewById(R.id.btn_back);
        wvBrowser = (WebView) view.findViewById(R.id.wv_browser);
        wvBrowser.setWebViewClient(new PMPWebViewClient());
        wvBrowser.getSettings().setLoadWithOverviewMode(true);
        wvBrowser.getSettings().setUseWideViewPort(true);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        if(this.isUrl) {
            this.wvBrowser.loadUrl(this.webViewContent);
        }else{
            this.wvBrowser.loadDataWithBaseURL(null, this.webViewContent, "text/html", "UTF-8", null);
        }
        return view;
    }

    public void setContent(String content, boolean isUrl) {
        this.webViewContent = content;
        this.isUrl = isUrl;
    }
    private class PMPWebViewClient extends WebViewClient {
        @Override public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }
    }
}
