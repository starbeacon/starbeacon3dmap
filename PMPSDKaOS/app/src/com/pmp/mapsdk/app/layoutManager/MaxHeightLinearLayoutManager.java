package com.pmp.mapsdk.app.layoutManager;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.TypedValue;

/**
 * Created by andrew on 1/12/2016.
 */

public class MaxHeightLinearLayoutManager extends LinearLayoutManager {
    private int maxHeight;
    private Context context;
    public MaxHeightLinearLayoutManager(Context context) {
        super(context);
        this.context = context;
    }

    public MaxHeightLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
        this.context = context;
    }

    public MaxHeightLinearLayoutManager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
    }

    public void setMaxHeightByDP(int dp) {
        maxHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,  context.getResources().getDisplayMetrics());
    }

    @Override
    public void setMeasuredDimension(int widthSize, int heightSize) {
        if (heightSize > maxHeight) {
            heightSize = maxHeight;
        }
        super.setMeasuredDimension(widthSize, heightSize);
    }
}
