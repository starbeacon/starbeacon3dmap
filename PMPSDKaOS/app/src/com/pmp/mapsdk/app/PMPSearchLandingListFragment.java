package com.pmp.mapsdk.app;

import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.cms.model.Areas;
import com.pmp.mapsdk.cms.model.Brands;
import com.pmp.mapsdk.cms.model.PoiCategories;
import com.pmp.mapsdk.cms.model.Pois;
import com.pmp.mapsdk.utils.PMPUtil;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by joeyyc on 28/11/2016.
 */

public class PMPSearchLandingListFragment extends Fragment {
    public static final String ARGS_BRANDCATEGORY = "ARGS_BRANDCATEGORY";
    public static final String KEY_POI = "POI";
    public static final String KEY_SUBCATEGORY = "SUBCATEGORY";
    private static final int MAX_NUM_PAGES = 5;

    private ArrayList<Pois> pois;
    private ArrayList<Pois> pickupPOIs;
    private ArrayList<Areas> areas;
    private ArrayList<ArrayList> subCatPOIs;
    private ArrayList<Map<String, Object>> subCats;

    private PoiCategories poiCategory;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        poiCategory = (PoiCategories) getArguments().getSerializable(ARGS_BRANDCATEGORY);

        //Load all necessary data from serverResponse
        generateData();

        RelativeLayout view = (RelativeLayout) inflater.inflate(
                R.layout.pmp_search_landing_list_fragment, container, false);

        LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.linearLayout);
        LinearLayout ll_poi = (LinearLayout)view.findViewById(R.id.ll_poi);

        int Measuredwidth = 0;
        int Measuredheight = 0;
        Point size = new Point();
        WindowManager w = getActivity().getWindowManager();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            w.getDefaultDisplay().getSize(size);
            Measuredwidth = size.x;
            Measuredheight = size.y;
        }else{
            Display d = w.getDefaultDisplay();
            Measuredwidth = d.getWidth();
            Measuredheight = d.getHeight();
        }

        for (int i=0; i<5 && i<pickupPOIs.size(); i++) {
            Pois poi = pickupPOIs.get(i);

            RelativeLayout rl = (RelativeLayout)inflater.inflate(R.layout.pmp_search_landing_slide_page, null);
            ImageView imageView = (ImageView)rl.findViewById(R.id.imageView);
            TextView tvTitle = (TextView)rl.findViewById(R.id.tvTitle);
            TextView tvLocation = (TextView)rl.findViewById(R.id.tvLocation);
            TextView tvOpenHrsTtl = (TextView)rl.findViewById(R.id.tvOpenHrsTtl);
            tvOpenHrsTtl.setText(getResources().getText(R.string.PMPMAP_SEARCH_LANDING_OPEN_NOW) + " : ");
            TextView tvOpenHrsValue = (TextView)rl.findViewById(R.id.tvOpenHrsValue);
            rl.setLayoutParams(new RelativeLayout.LayoutParams(Measuredwidth, RelativeLayout.LayoutParams.MATCH_PARENT));

            PMPUtil.setImageWithURL(imageView, poi.getImage());
            tvTitle.setText(PMPUtil.getLocalizedString(poi.getName()));
            tvLocation.setText(PMPUtil.getLocalizedAddress(getActivity(), poi));
            tvOpenHrsValue.setText(PMPUtil.getLocalizedString(poi.getBusinessHours()));

            ll_poi.addView(rl);
        }

        //Setup Search By Location
        View vSearchLocation = inflater.inflate(R.layout.pmp_search_landing_list_grid, container, false);
        TextView tvLoc = (TextView)vSearchLocation.findViewById(R.id.textView);
        tvLoc.setText("Search by location");
        Button btnLoc = (Button)vSearchLocation.findViewById(R.id.button);
        btnLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PMPCatalogueMoreMasterFragment fragment = new PMPCatalogueMoreMasterFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean(PMPCatalogueMoreMasterFragment.ARGS_IS_LOADING_AREA, true);
                bundle.putSerializable(PMPCatalogueMoreMasterFragment.ARGS_BRANDCATEGORY, poiCategory);
                fragment.setArguments(bundle);

                getFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragment_container,fragment)
                        .addToBackStack("")
                        .commit();
            }
        });
        LinearLayout ll_location = (LinearLayout)vSearchLocation.findViewById(R.id.ll_location);

        for (int i=0; i<areas.size(); i++) {
            Areas area = areas.get(i);
            View gridView  = inflater.inflate(R.layout.pmp_search_landing_list_grid_cell, null);
            gridView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PMPCatalogueMoreMasterFragment fragment = new PMPCatalogueMoreMasterFragment();
                    Bundle bundle = new Bundle();
                    //Todo add index information
                    bundle.putBoolean(PMPCatalogueMoreMasterFragment.ARGS_IS_LOADING_AREA, true);
                    bundle.putSerializable(PMPCatalogueMoreMasterFragment.ARGS_BRANDCATEGORY, poiCategory);
                    fragment.setArguments(bundle);

                    getFragmentManager()
                            .beginTransaction()
                            .add(R.id.fragment_container,fragment)
                            .addToBackStack("")
                            .commit();
                }
            });

            // set value into textview
            TextView textView = (TextView) gridView
                    .findViewById(R.id.textView);
            textView.setText(PMPUtil.getLocalizedString(area.getName()));

            // set image based on selected text
            ImageView imageView = (ImageView) gridView
                    .findViewById(R.id.imageView);
//            PMPUtil.setImageWithURL(imageView, area.getImage());

            ll_location.addView(gridView);
        }

        linearLayout.addView(vSearchLocation);


        //Setup search by catalogues
        View vSearchCategory = inflater.inflate(R.layout.pmp_search_landing_list_grid, container, false);
        TextView tvCat = (TextView)vSearchCategory.findViewById(R.id.textView);
        tvCat.setText("Shopping catalogues");
        Button btnCat = (Button)vSearchCategory.findViewById(R.id.button);
        btnCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ArrayList<SubCategories> subCatList = new ArrayList<SubCategories>();
//                for (Map<String, Object> data : subCats) {
//                    subCatList.add(((SubCategories)data.get(KEY_SUBCATEGORY)));
//                }

                PMPCatalogueMoreMasterFragment fragment = new PMPCatalogueMoreMasterFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean(PMPCatalogueMoreMasterFragment.ARGS_IS_LOADING_AREA, true);
                //bundle.putParcelable(PMPCatalogueMoreMasterFragment.ARGS_BRANDCATEGORY, poiCategory);
                //bundle.putParcelableArrayList(PMPCatalogueMoreMasterFragment.ARGS_SUBCATEGORIES, subCatList);
                fragment.setArguments(bundle);

                getFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragment_container,fragment)
                        .addToBackStack("")
                        .commit();
            }
        });
        LinearLayout ll_category = (LinearLayout)vSearchCategory.findViewById(R.id.ll_location);

        for (int i=0; i<poiCategory.getSub_categories().size(); i++) {
            final PoiCategories cat = poiCategory.getSub_categories().get(i);
            //SubCategories sCat = (SubCategories)data.get(KEY_SUBCATEGORY);

            View gridView  = inflater.inflate(R.layout.pmp_search_landing_list_grid_cell, null);
            gridView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    PMPCatalogueMoreMasterFragment fragment = new PMPCatalogueMoreMasterFragment();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(PMPCatalogueMoreMasterFragment.ARGS_IS_LOADING_AREA, false);
                    bundle.putSerializable(PMPCatalogueMoreMasterFragment.ARGS_SUBCATEGORIES, poiCategory);
                    bundle.putSerializable(PMPCatalogueMoreMasterFragment.ARGS_SELECTED_POI_CATEGORY, cat);
                    fragment.setArguments(bundle);

                    getFragmentManager()
                            .beginTransaction()
                            .add(R.id.fragment_container,fragment)
                            .addToBackStack("")
                            .commit();
                }
            });

            // set value into textview
            TextView textView = (TextView) gridView
                    .findViewById(R.id.textView);
            textView.setText(PMPUtil.getLocalizedString(cat.getName()));

            // set image based on selected text
            ImageView imageView = (ImageView) gridView
                    .findViewById(R.id.imageView);
            PMPUtil.setImageWithURL(imageView, cat.getImage());

            ll_category.addView(gridView);
        }

        linearLayout.addView(vSearchCategory);

        return view;
    }

    private void generateData() {
        final PMPServerManager serverManager = PMPServerManager.getShared(getActivity());

        pickupPOIs = new ArrayList<Pois>();
        subCatPOIs = new ArrayList<ArrayList>();
        if (serverManager.getServerResponse() != null) {
            areas = serverManager.getServerResponse().getAreas();
        } else {
            areas = new ArrayList<Areas>();
        }
        subCats = new ArrayList();

        Bundle bundle = getArguments();
        if (bundle != null) {
            poiCategory = (PoiCategories) bundle.getSerializable(ARGS_BRANDCATEGORY);
            if (poiCategory == null) {
                return;
            }
        }

        ArrayList<Brands> brands = new ArrayList<Brands>();
        ArrayList<ArrayList> subCatBrands = new ArrayList<ArrayList>();

//        for (int i = 0; i< poiCategory.getSubCategories().size(); i++) {
//            subCatPOIs.add(new ArrayList<Pois>());
//            subCatBrands.add(new ArrayList<Brands>());
//        }
//
//        for (Brands b : serverManager.getServerResponse().getBrands()) {
//            Boolean brandMatched = false;
//            for (int brandCatId : b.getBrandCategoryIds()) {
//                for (int i = 0; i< poiCategory.getSubCategories().size(); i++) {
//                    SubCategories subCat = poiCategory.getSubCategories().get(i);
//
//                    if (brandCatId == (int)subCat.getId()) {
//                        if (!brandMatched) {
//                            brandMatched = true;
//                            brands.add(b);
//                        }
//
//                        ArrayList<Brands> subBrands = subCatBrands.get(i);
//                        subBrands.add(b);
//                    }
//                }
//            }
//        }
//
//        for (Pois p : serverManager.getServerResponse().getPois()) {
//            if (pickupPOIs.size() < MAX_NUM_PAGES) {
//                for (Brands b : brands) {
//                    if (b.getId() == p.getBrandId()) {
//                        pickupPOIs.add(p);
//                        //do not add same brand in same category
//                        brands.remove(b);
//                        break;
//                    }
//                }
//            }
//
//            for (int i=0; i<subCatBrands.size(); i++) {
//                ArrayList<Brands> brandArr = subCatBrands.get(i);
//                for (Brands b : brandArr) {
//                    if (b.getId() == p.getBrandId()) {
//                        ArrayList<Pois> arr = subCatPOIs.get(i);
//                        arr.add(p);
//                        //do not add same brand in same category
//                        brandArr.remove(b);
//                        break;
//                    }
//                }
//            }
//        }

        subCats = new ArrayList<Map<String, Object>>();
//        for (int i = 0; i< poiCategory.getSubCategories().size(); i++) {
//            if (((ArrayList<Pois>)subCatPOIs.get(i)).size() > 0) {
//                Map<String, Object> data = new HashMap<String, Object>();
//                data.put(KEY_POI, ((ArrayList<Pois>)subCatPOIs.get(i)).get(0));
//                data.put(KEY_SUBCATEGORY, poiCategory.getSubCategories().get(i));
//                subCats.add(data);
//            }
//        }
    }
}
