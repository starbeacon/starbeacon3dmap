package com.pmp.mapsdk.app;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmap.PMPDataManager;
import com.cherrypicks.pmpmap.PMPMapController;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.cms.model.Brands;
import com.pmp.mapsdk.cms.model.Name;
import com.pmp.mapsdk.cms.model.PoiCategories;
import com.pmp.mapsdk.cms.model.Pois;
import com.pmp.mapsdk.cms.model.ResponseData;
import com.pmp.mapsdk.cms.model.Tag;
import com.pmp.mapsdk.cms.model.Tags;
import com.pmp.mapsdk.external.PMPMapSDK;
import com.pmp.mapsdk.utils.PMPUtil;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by joeyyc on 19/1/2017.
 */

public class PMPSearchStartLocationFragment extends Fragment {

    public static int REQUEST_CODE = 111;
    public static String SELECTED_POI_ID = "POI_IDS";

    public static final String ARGS_DESTINATION = "ARGS_DESTINATION";

    private EditText searchText;
    private ImageButton closeButton;
    private ListView listViewSearch;
    private ListView listViewCurrentLocation;
    private ListView listViewSelectPOI;
    private ListView listViewTransportation;
    private Map<Integer, List<Pois>> poisMappedByBrand;
    private Map<Integer, List<Pois>> poisMappedByCat;
    private Map<Integer, List<Pois>> poisMappedByTag;
    private PMPSearchStartLocationFragment.SearchResultAdapter searchResultAdapter = new PMPSearchStartLocationFragment.SearchResultAdapter();
    private PMPSearchStartLocationFragment.CurrentLocationAdapter currentLocationAdapter = new PMPSearchStartLocationFragment.CurrentLocationAdapter();
    private PMPSearchStartLocationFragment.TransportationAdapter transportationAdapter = new PMPSearchStartLocationFragment.TransportationAdapter();
    private List<Pois> searchResultPOIs;
    private List<PoiCategories> transportations;
    private LinearLayout recentSearchHolder;
    private Vector<String> recentSearchKeywords;
    private String recentSearchFileName = "RecentSearch.bin";

    private Pois destination;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Bundle bundle = getArguments();
        if (bundle != null) {
            destination = bundle.getParcelable(ARGS_DESTINATION);
        }

        View view = inflater.inflate(R.layout.pmp_search_start_location_fragment, container, false);
        View header = view.findViewById(R.id.header);
        header.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
        searchText = (EditText)view.findViewById(R.id.edit_search);
        searchText.setHint(getResources().getString(R.string.PMPMAP_CHOOSE_START_POINT));
        closeButton = (ImageButton)view.findViewById(R.id.btn_close);
        listViewSearch = (ListView)view.findViewById(R.id.listViewSearch);
        listViewCurrentLocation = (ListView)view.findViewById(R.id.listViewCurrentLocation);
        listViewTransportation = (ListView)view.findViewById(R.id.listViewTransportation);
        recentSearchHolder = (LinearLayout)view.findViewById(R.id.recent_search_holder);

        final PMPServerManager serverManager = PMPServerManager.getShared(getActivity());

        transportations = new ArrayList<PoiCategories>();
        for (PoiCategories cat : serverManager.getServerResponse().getTransportationList()) {
            for (PoiCategories locCat : cat.getSub_categories()) {
                transportations.addAll(locCat.getSub_categories());
            }
        }

        poisMappedByBrand = new HashMap<Integer, List<Pois>>();
        if (serverManager.getServerResponse() != null) {

            listViewSearch.setAdapter(searchResultAdapter);
            listViewSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                    if(imm.isAcceptingText()) { // verify if the soft keyboard is open
                        imm.hideSoftInputFromWindow(searchText.getWindowToken(), 0);
                    }
                    getFragmentManager().popBackStack();

                    if (adapterView.getAdapter() == searchResultAdapter) {
                        Object item = searchResultAdapter.getItem(i);
                        boolean hasDetail = true;
                        PMPMapFragment mapFragment = (PMPMapFragment) getParentFragment();
                        List<Pois> pois = new ArrayList<Pois>();
                        if (item instanceof Tags) {
                            Tags t = (Tags) item;
                            if(t.getTagArray() != null) {
                                for (Pois p : PMPDataManager.getSharedPMPManager(getActivity()).getResponseData().getPois()) {
                                    for (Tag tag : t.getTagArray()) {
                                        if (tag.getTagType().equals("Poi")) {
                                            ArrayList<Integer> poiIdArray = tag.getIdsArray();
                                            for (int id : poiIdArray) {
                                                if (id == (int) p.getId()) {
                                                    pois.add(p);
                                                    break;
                                                }
                                            }
                                        } else if (tag.getTagType().equals("PoiCategory")) {
                                            ArrayList<Integer> catIdArray = tag.getIdsArray();
                                            boolean nextPoi = false;
                                            for (int id : catIdArray) {
                                                for (int catId : p.getPoiCategoryIds()) {
                                                    if (id == catId) {
                                                        pois.add(p);
                                                        nextPoi = true;
                                                        break;
                                                    }
                                                }
                                                if (nextPoi) break;
                                            }
                                        } else if (tag.getTagType().equals("Brand")) {
                                            ArrayList<Integer> brandIdArray = tag.getIdsArray();
                                            for (int id : brandIdArray) {
                                                if (id == (int) p.getBrandId()) {
                                                    pois.add(p);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else if (item instanceof PoiCategories) {
                            ResponseData response = PMPDataManager.getSharedPMPManager(getActivity()).getResponseData();

                            for (Pois poi : response.getPois()) {
                                for (int catId : poi.getPoiCategoryIds()) {
                                    if (((PoiCategories)item).getId() == catId) {
                                        pois.add(poi);
                                        break;
                                    }
                                }
                            }
                            hasDetail = ((PoiCategories)item).isHasDetails();
                        } else if (item instanceof Brands) {
                            Brands b = (Brands) item;
                            for (Pois p : PMPDataManager.getSharedPMPManager(getActivity()).getResponseData().getPois()) {
                                if (b.getId() == p.getBrandId()) {
                                    pois.add(p);
                                }
                            }
                        }

                        addKeywordHistory(item);
                        boolean shouldShowTable = false;

                        if (pois.size() > 1) {
                            loop:
                            for (Pois p : pois) {
                                if (p.getBrandId() > 0) {
                                    shouldShowTable = true;
                                    break loop;
                                }
                                for (PoiCategories cat : PMPDataManager.getSharedPMPManager(getActivity()).getResponseData().getFlattenPoiCategories()) {
                                    for (int catID : p.getPoiCategoryIds()) {
                                        if ((int) cat.getId() == catID && !cat.isHasDetails()) {
                                            shouldShowTable = shouldShowTable | cat.isHasDetails();
                                            if (shouldShowTable) {
                                                break loop;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        PMPMapController.getInstance().setOverviewing(false);
                        if (pois.size() > 1) {
                            mapFragment.showPOIListOnMap(pois, item, shouldShowTable, true, true, false);
                        } else if (pois.size() == 1) {
                            mapFragment.selectLocationForPath(pois.get(0));
                        } else{
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle("").setMessage(getResources().getString(R.string.PMPMAP_ERROR_RESULT_NOT_FOUND));
                            builder.show();
                        }
                        for (int n = 0; n < getFragmentManager().getBackStackEntryCount(); n++) {
                            getFragmentManager().popBackStack();
                        }
                    }
                }
            });


            listViewCurrentLocation.setAdapter(currentLocationAdapter);
            listViewCurrentLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    /* Bug #7424 disable it due to requirement changes */
//                    if (position == 0) {
//                        PMPMapFragment mapFragment = (PMPMapFragment) getParentFragment();
//                        if (CoreEngine.getInstance().getIndoorLocation() != null) {
//                            mapFragment.selectLocationForPath(null);
//                            for (int n = 0; n < getFragmentManager().getBackStackEntryCount(); n++) {
//                                getFragmentManager().popBackStack();
//                            }
//                        } else {
//                            if (GPSLocationManager.getSharedManager(getActivity()).isInHongKongOrChina) {
//                                PMPSearchTransportFragment fragment = new PMPSearchTransportFragment();
//
//                                Bundle bundle = new Bundle();
//                                bundle.putInt(PMPSearchTransportFragment.ARGS_VIEW_TYPE, PMPSearchTransportFragment.VIEW_TYPE_IN_HK);
//                                bundle.putDouble(PMPSearchTransportFragment.ARGS_DEST_AREA_ID, destination.getAreaId());
//
//                                fragment.setArguments(bundle);
//                                getFragmentManager()
//                                        .beginTransaction()
//                                        .replace(R.id.fragment_container, fragment)
//                                        .addToBackStack("")
//                                        .commit();
//                            } else {
//                                AlertDialog dialog = new AlertDialog.Builder(getActivity())
//                                        .setTitle("")
//                                        .setMessage(getResources().getString(R.string.PMPMAP_ALERT_NOT_IN_HK))
//                                        .setPositiveButton(getResources().getString(R.string.PMPMAP_SEARCH_BUTTON_CONFIRM), new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int which) {
//                                                // continue with oversee transportation
//                                                PMPSearchTransportFragment fragment = new PMPSearchTransportFragment();
//
//                                                Bundle bundle = new Bundle();
//                                                bundle.putInt(PMPSearchTransportFragment.ARGS_VIEW_TYPE, PMPSearchTransportFragment.VIEW_TYPE_NOT_IN_HK);
//                                                bundle.putDouble(PMPSearchTransportFragment.ARGS_DEST_AREA_ID, destination.getAreaId());
//
//                                                fragment.setArguments(bundle);
//                                                getFragmentManager()
//                                                        .beginTransaction()
//                                                        .replace(R.id.fragment_container, fragment)
//                                                        .addToBackStack("")
//                                                        .commit();
//                                            }
//                                        })
//                                        .setIcon(android.R.drawable.ic_dialog_alert)
//                                        .show();
//                            }
//                        }
//                    } else {

                    PMPUtil.hideKeyboard(getActivity());

                        PMPMapFragment mapFragment = (PMPMapFragment) getParentFragment();
                        mapFragment.selectPOIOnMap();
                        for (int n = 0; n < getFragmentManager().getBackStackEntryCount(); n++) {
                            getFragmentManager().popBackStack();
                        }
//                    }
                }
            });

            listViewTransportation.setAdapter(transportationAdapter);
            listViewTransportation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    PMPUtil.hideKeyboard(getActivity());

                    PoiCategories cat = transportations.get(position);
                    if (cat.getId() == PMPSearchTransportFragment.CAT_BU) {
                        PMPSearchTransportFragment fragment = new PMPSearchTransportFragment();

                        Bundle bundle = new Bundle();
                        bundle.putInt(PMPSearchTransportFragment.ARGS_VIEW_TYPE, PMPSearchTransportFragment.VIEW_TYPE_BUS);
                        bundle.putDouble(PMPSearchTransportFragment.ARGS_DEST_AREA_ID, destination.getAreaId());

                        fragment.setArguments(bundle);
                        getFragmentManager()
                                .beginTransaction()
                                .replace(R.id.fragment_container, fragment)
                                .addToBackStack("")
                                .commit();

                        return;
                    } else {
                        final ArrayList<Pois> poiList = PMPServerManager.getShared(getActivity()).getServerResponse().getPois();
                        ArrayList<Pois> pois = new ArrayList<Pois>();
                        for (Pois poi : poiList) {
                            if (poi.getPoiCategoryIds().contains((int)cat.getId())) {
                                if (destination != null && poi.getAreaId() == destination.getAreaId()) {
                                    PMPMapFragment mapFragment = (PMPMapFragment) getParentFragment();
                                    mapFragment.selectLocationForPath(poi);
                                    for (int n = 0; n < getFragmentManager().getBackStackEntryCount(); n++) {
                                        getFragmentManager().popBackStack();
                                    }
                                    return;
                                }
                                pois.add(poi);
                            }
                        }
                        if (pois.size() > 0) {
                            PMPMapFragment mapFragment = (PMPMapFragment) getParentFragment();
                            mapFragment.selectLocationForPath(pois.get(0));
                            for (int n = 0; n < getFragmentManager().getBackStackEntryCount(); n++) {
                                getFragmentManager().popBackStack();
                            }
                            return;
                        }
                    }
                }
            });

        }

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0) {
                    listViewSearch.setVisibility(View.GONE);
                }else {
                    listViewSearch.setVisibility(View.VISIBLE);
                    ArrayList result = new ArrayList();
                    if (serverManager.getServerResponse() != null) {
                        if (serverManager.getServerResponse().getTags() != null) {
                            for (Tags tag : serverManager.getServerResponse().getTags()) {
                                if (tag.getContent() != null &&
                                        tag.getContent().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                                    result.add(tag);
                                    break;
                                }
                            }
                        }

                        if (serverManager.getServerResponse().getFlattenPoiCategories() != null) {
                            for (PoiCategories cat : serverManager.getServerResponse().getFlattenPoiCategories()) {
                                if (!cat.isUnsearchable()) {
                                    for (Name name : cat.getName()) {
                                        if (name.getContent() != null &&
                                                name.getContent().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                                            result.add(cat);
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        if (serverManager.getServerResponse().getBrands() != null) {
                            for (Brands brand : serverManager.getServerResponse().getBrands()) {
                                for (Name name : brand.getName()) {
                                    if (name.getContent() != null &&
                                            name.getContent().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                                        result.add(brand);
                                        break;
                                    }
                                }
                            }
                        }

                    }
                    searchResultPOIs = result;
                    searchResultAdapter.notifyDataSetInvalidated();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PMPUtil.hideKeyboard(getActivity());
                getFragmentManager().popBackStack();
                if (PMPMapSDK.getTabBarVisibilityUpdateCallback() != null) {
                    PMPMapSDK.getTabBarVisibilityUpdateCallback().updateTabBarVisibilityUpdate(false);
                }
                /*finish();*/
            }
        });

        recentSearchKeywords = new Vector<String>();

        return view;
    }

    private void addKeywordHistory(Object obj) {
        if (searchText.length() != 0) {
            String keyword = null;

            if (obj instanceof Tags) {
                keyword = ((Tags)obj).getContent();
            } else if (obj instanceof PoiCategories) {
                PoiCategories cat = (PoiCategories)obj;
                for (Name name : cat.getName()) {
                    if (name.getContent().toLowerCase().contains(searchText.getText().toString().toLowerCase())) {
                        keyword = name.getContent();
                        break;
                    }
                }
            } else if (obj instanceof Brands) {
                Brands brand = (Brands)obj;
                for (Name name : brand.getName()) {
                    if (name.getContent().toLowerCase().contains(searchText.getText().toString().toLowerCase())) {
                        keyword = name.getContent();
                        break;
                    }
                }
            }

            if (keyword == null) {
                keyword = searchText.getText().toString();
            }

            if (recentSearchKeywords.contains(keyword)) {
                recentSearchKeywords.remove(keyword);
            }
            recentSearchKeywords.insertElementAt(keyword, 0);

            while (recentSearchKeywords.size() > PMPDataManager.getSharedPMPManager(getActivity()).getResponseData().getMapNumRecentSearch()) {
                recentSearchKeywords.remove(recentSearchKeywords.size() - 1);
            }
            FileOutputStream fos = null;
            ObjectOutputStream oos = null;
            try {
                fos = getActivity().openFileOutput(recentSearchFileName, MODE_PRIVATE);
                oos = new ObjectOutputStream(fos);
                oos.writeObject(recentSearchKeywords);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                if (oos != null) {
                    try {
                        oos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (PMPMapSDK.getTabBarVisibilityUpdateCallback() != null) {
            PMPMapSDK.getTabBarVisibilityUpdateCallback().updateTabBarVisibilityUpdate(true);
        }
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = getActivity().openFileInput(recentSearchFileName);
            ois = new ObjectInputStream(fis);
            recentSearchKeywords = (Vector<String>) ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
            recentSearchHolder.removeAllViews();
            if (recentSearchKeywords != null && recentSearchKeywords.size() != 0) {
                View firstConvertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_search, null);
                TextView firstTv_name = (TextView) firstConvertView.findViewById(R.id.tv_name);
                firstTv_name.setText(getResources().getText(R.string.PMPMAP_SEARCH_RECENT_SEARCH));
                ImageView firstIv = (ImageView)firstConvertView.findViewById(R.id.iv_icon);
                firstIv.setVisibility(View.GONE);
                recentSearchHolder.addView(firstConvertView);

                for (final String keyword : recentSearchKeywords) {
                    View convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_search, null);
                    TextView tv_name = (TextView) convertView.findViewById(R.id.tv_name);
                    tv_name.setText(keyword);
                    ImageView iv = (ImageView)convertView.findViewById(R.id.iv_icon);
                    iv.setImageResource(R.drawable.search_icon_history);
                    recentSearchHolder.addView(convertView);

                    convertView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            searchText.setText(keyword);
                        }
                    });
                }
            }
        }
    }

    private class CurrentLocationAdapter extends BaseAdapter {
        public CurrentLocationAdapter() {
        }

        @Override
        public String getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            if(convertView == null){
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_aroundme, null);
            }

            TextView tv_name = (TextView)convertView.findViewById(R.id.tv_name);
            ImageView iv = (ImageView)convertView.findViewById(R.id.iv_icon);
            /* Bug #7424 disable it due to requirement changes */
//            if (position == 0) {
//                tv_name.setText(getResources().getString(R.string.PMPMAP_YOUR_LOCATIONS));
//                iv.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.icon_around_your_location));
//            } else {
                tv_name.setText(getResources().getString(R.string.PMPMAP_CHOOSE_ON_MAP));
                iv.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.icon_choose_on_map));
//            }
            return convertView;
        }

        @Override
        public int getCount() {
            return 1;
        }
    }

    private class TransportationAdapter extends BaseAdapter {
        public TransportationAdapter() {
        }

        @Override
        public String getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            if(convertView == null){
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_aroundme, null);
            }

            PoiCategories cat = transportations.get(position);

            TextView tv_name = (TextView)convertView.findViewById(R.id.tv_name);
            tv_name.setText(PMPUtil.getLocalizedString(cat.getName()));
            tv_name.setTextColor(Color.parseColor("#848484"));
            tv_name.setTextSize(14);
            tv_name.setTypeface(Typeface.DEFAULT);
            ImageView iv = (ImageView)convertView.findViewById(R.id.iv_icon);
            PMPUtil.setImageWithURL(iv, cat.getImage());
            return convertView;
        }

        @Override
        public int getCount() {
            return transportations.size();
        }
    }

    private class SearchResultAdapter extends BaseAdapter {
        @Override
        public Object getItem(int i) {
            return searchResultPOIs.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            if(convertView == null){
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_search, null);
            }

            Object obj = getItem(position);
            RelativeLayout relativeLayout = (RelativeLayout)convertView.findViewById(R.id.rl_background);
            relativeLayout.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
            TextView tv_name = (TextView)convertView.findViewById(R.id.tv_name);
            ImageView iv_icon = (ImageView)convertView.findViewById(R.id.iv_icon);
            String searchTitle = searchText.getText().toString().toLowerCase();
            String title;
            SpannableString titleToSpan;

            if (obj.getClass().getSimpleName().equalsIgnoreCase("PoiCategories")) {
                title = PMPUtil.getLocalizedString(((PoiCategories)obj).getName()).toLowerCase();
                titleToSpan = new SpannableString(PMPUtil.getLocalizedString(((PoiCategories)obj).getName()));
                PMPUtil.setImageWithURL(iv_icon, ((PoiCategories)obj).getImage());
            } else if (obj.getClass().getSimpleName().equalsIgnoreCase("Brands")) {
                title = PMPUtil.getLocalizedString(((Brands)obj).getName()).toLowerCase();
                titleToSpan = new SpannableString(PMPUtil.getLocalizedString(((Brands)obj).getName()));
                iv_icon.setImageResource(R.drawable.icon_shopping_n_dining);
            } else if (obj.getClass().getSimpleName().equalsIgnoreCase("Tags")) {
                title = ((Tags)obj).getContent().toLowerCase();
                titleToSpan = new SpannableString(((Tags)obj).getContent());
                iv_icon.setImageResource(R.drawable.icon_search_blue);
            } else {
                title = "";
                titleToSpan = new SpannableString("");
                iv_icon.setImageDrawable(null);
            }

            //set background color
            if (position%2 == 0) {
                relativeLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));
            } else {
                relativeLayout.setBackgroundColor(Color.parseColor("#E9E9E9"));
            }

            int index = title.indexOf(searchTitle);
            if (index != -1) {
                titleToSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.nav_arrived_notcoverd_view_color)), index, index+searchTitle.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            tv_name.setTextColor(PMPMapSDK.getMapUISetting().getRecentSearchTextColor());
            tv_name.setText(titleToSpan);

            return convertView;
        }

        @Override
        public int getCount() {
            if (searchResultPOIs == null)
                return 0;

            return searchResultPOIs.size();
        }
    }

}
