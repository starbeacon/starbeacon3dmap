package com.pmp.mapsdk.app;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.cherrypicks.pmpmap.PMPMapController;
import com.cherrypicks.pmpmap.analytics.AnalyticsLogger;
import com.cherrypicks.pmpmap.core.CoreEngine;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.cms.PMPPOIByDuration;
import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.cms.model.Brands;
import com.pmp.mapsdk.cms.model.Maps;
import com.pmp.mapsdk.cms.model.PoiCategories;
import com.pmp.mapsdk.cms.model.Pois;
import com.pmp.mapsdk.external.PMPMapSDK;
import com.pmp.mapsdk.location.PMPLocation;
import com.pmp.mapsdk.utils.PMPUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.pmp.mapsdk.app.PMPMapFragment.PMPMapViewModeNormal;

/**
 * Created by joeyyc on 30/11/2016.
 */

public class PMPSearchAroundFragment extends Fragment {
    public static final String ARGS_GATE = "ARGS_GATE";
    public static final String ARGS_BACK_TO_BROWSING = "ARGS_BACK_TO_BROWSING";

    private ViewPager pager;
    private TabLayout tabLayout;
    private TextView lblHeaderTitle;
    private ImageButton btnMap;
    private ImageButton btnFilter;
    private ImageButton btnSearch;
    private ImageButton btnBack;
    private boolean backToBrowsingMode = false;
    private Pois gate;
    private ArrayList<ArrayList<PMPPOIByDuration>> dataList;
    private ArrayList<String> titles;
    private float _durationValue = 5;
    private ListFragmentPagerAdapter listAdapter;
    private ArrayList<PoiCategories> poiCategories = new ArrayList<PoiCategories>();

    public float aroundMeDuration = 60 * _durationValue;//5mins
    private boolean restrictedOn = true;
    private boolean nonRestrictedOn = true;
    public boolean isComeFromGate = false;

    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            updatePageContent(position);

            HashMap<String, Object> params = new HashMap<>();
            if(position == 0){
                params.put("type","SD_1");
            }else if(position == 1){
                params.put("type","FS_2");
            }else if(position == 2){
                params.put("type","AC_3");
            }
            if(isComeFromGate){
                AnalyticsLogger.getInstance().logEvent("Filter_Boarding_Gate_Catalog", params);
            }else{
                AnalyticsLogger.getInstance().logEvent("Filter_Around_Me_Catalog", params);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        listAdapter = new ListFragmentPagerAdapter(getFragmentManager(), getActivity());

        Bundle bundle = getArguments();
        if (bundle != null) {
            gate = bundle.getParcelable((ARGS_GATE));
            if (gate != null) {
                restrictedOn = true;
                nonRestrictedOn = false;
            }
            backToBrowsingMode = bundle.getBoolean(ARGS_BACK_TO_BROWSING, false);
        }
        generateData();

        View view = (View)inflater.inflate(R.layout.pmp_search_view_pager, container, false);
        View topbar = view.findViewById(R.id.topbar);
        topbar.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());

        view.setBackgroundColor(Color.TRANSPARENT);
        btnBack = (ImageButton)view.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gate != null) {
                    PMPMapFragment mapFragment = (PMPMapFragment) getParentFragment();
                    mapFragment.isShowCloseButtonOnTop.set(false);
                    CoreEngine.getInstance().setMapMode(CoreEngine.MapModeBrowsing);
                    mapFragment.setViewMode(PMPMapViewModeNormal);
                    mapFragment.setSelectedPOI(gate);
                    PMPMapController.getInstance().setSelectedPOIId((int) mapFragment.getSelectedPOI().getId());
                    DisplayMetrics metrics = getResources().getDisplayMetrics();
                    PMPMapController.getInstance().jumpToPosition((float) mapFragment.getSelectedPOI().getX(),
                            (float) mapFragment.getSelectedPOI().getY(),
                            (float) metrics.widthPixels / metrics.density / 2,
                            (float) metrics.heightPixels / metrics.density / 2);
                }
                getFragmentManager().popBackStack();
            }
        });
        btnMap = (ImageButton)view.findViewById(R.id.btnMidRight);
        btnFilter = (ImageButton)view.findViewById(R.id.btnMostRight);
        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PMPSearchAroundMeFilterFragment fragment = new PMPSearchAroundMeFilterFragment();

                Bundle bundle = new Bundle();
                bundle.putFloat(PMPSearchAroundMeFilterFragment.ARGS_DURATION, _durationValue*60);
                bundle.putBoolean(PMPSearchAroundMeFilterFragment.ARGS_RESTRICTED, restrictedOn);
                bundle.putBoolean(PMPSearchAroundMeFilterFragment.ARGS_NON_RESTRICTED, nonRestrictedOn);
                bundle.putBoolean(PMPSearchAroundMeFilterFragment.ARGS_SHOW_RESTRICTED_OPTION, gate == null);
                fragment.setArguments(bundle);

                getFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragment_container, fragment)
                        .addToBackStack("")
                        .commit();
            }
        });
        btnSearch = (ImageButton)view.findViewById(R.id.btnLeastRight);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PMPSearchListFragment fragment = new PMPSearchListFragment();

                Bundle bundle = new Bundle();
                bundle.putString(PMPSearchListFragment.ARGS_TITLE, getResources().getString(PMPSearchFragment.AROUNDME_NAMES[1]));
                fragment.setArguments(bundle);

                getFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragment_container, fragment)
                        .addToBackStack("")
                        .commit();
            }
        });
        lblHeaderTitle = (TextView)view.findViewById(R.id.lblHeadTitle);

        if (gate != null) {
            Pois poi = PMPPOIByDuration.getSavedFlightGate();
            String title;
            if (poi != null && poi.getId() == gate.getId()) {
                btnSearch.setVisibility(View.VISIBLE);
                title = String.format(getResources().getString(R.string.PMPMAP_AROUND_MY_GATE_TITLE), PMPUtil.getLocalizedString(gate.getName()));
            } else {
                btnSearch.setVisibility(View.GONE);
                title = String.format(getResources().getString(R.string.PMPMAP_AROUND_GATE_TITLE), PMPUtil.getLocalizedString(gate.getName()));
            }
            lblHeaderTitle.setText(title);
        } else {
            lblHeaderTitle.setText(getResources().getString(R.string.PMPMAP_SEARCH_AROUND_ME));
            btnSearch.setVisibility(View.GONE);
        }

        pager = (ViewPager)view.findViewById(R.id.viewpager);
        pager.setAdapter(listAdapter);

        tabLayout = (TabLayout)view.findViewById(R.id.sliding_tabs);
        tabLayout.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
        tabLayout.setupWithViewPager(pager);
        pager.addOnPageChangeListener(onPageChangeListener);

        PMPMapFragment mapFragment = (PMPMapFragment) getParentFragment();
        //adjust toolbar's height so that error header could be properly displayed
        mapFragment.adjustToolbarHeight(120);

        btnMap.setImageResource(R.drawable.icon_map_view);
        btnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pager.getVisibility() == View.GONE) {
                    pager.setVisibility(View.VISIBLE);
                    btnMap.setImageResource(R.drawable.icon_map_view);
                }else {
                    pager.setVisibility(View.GONE);
                    btnMap.setImageResource(R.drawable.icon_list_view);
                }
                PMPMapFragment mapFragment = (PMPMapFragment) getParentFragment();
                if (gate != null) {
                    CoreEngine.getInstance().setMapMode(CoreEngine.MapModeBrowsing);
                    for (Maps map : PMPServerManager.getShared().getServerResponse().getMaps()) {
                        if (map.getId() == gate.getMapId()) {
                            mapFragment.setSelectedMap(map, true);
                            break;
                        }
                    }
                    DisplayMetrics metrics = getResources().getDisplayMetrics();
                    PMPMapController.getInstance().jumpToPosition((float) gate.getX(),
                            (float) gate.getY(),
                            (float) metrics.widthPixels / metrics.density / 2,
                            (float ) metrics.heightPixels / metrics.density / 2);
                }else {
                    PMPLocation location = CoreEngine.getInstance().getIndoorLocation();
                    int mapId = Integer.parseInt(location.getName());
                    for (Maps map : PMPServerManager.getShared().getServerResponse().getMaps()) {
                        if (map.getId() == mapId) {
                            mapFragment.setSelectedMap(map, true);
                            break;
                        }
                    }
                    DisplayMetrics metrics = getResources().getDisplayMetrics();
                    PMPMapController.getInstance().jumpToPosition((float) location.getX(),
                            (float) location.getY(),
                            (float) metrics.widthPixels / metrics.density / 2,
                            (float ) metrics.heightPixels / metrics.density / 2);
                }

                HashMap<String, Object> params = new HashMap<String, Object>();
                params.put("type", (pager.getVisibility() == View.GONE)? "Map":"List");
                AnalyticsLogger.getInstance().logEvent( gate != null? "Filter_Boarding_Gate_Toggle" : "Filter_Around_Me_Toggle", params);
            }
        });
        btnFilter.setImageResource(PMPMapSDK.getMapUISetting().getNavFilterImageResource());
        updatePageContent(pager.getCurrentItem());
        return view;
    }

    public void updateDurationValue(float duration, boolean restricted, boolean nonRestricted) {
        _durationValue = duration/60;
        aroundMeDuration = duration;
        restrictedOn = restricted;
        nonRestrictedOn = nonRestricted;

        generateData();
    }

    private void generateData() {
        List<PMPPOIByDuration> aroundMePOIs = null;
        if (gate != null) {
            aroundMePOIs = PMPPOIByDuration.filterAroundPOIByDuration(gate, aroundMeDuration);
        }else {
            aroundMePOIs = PMPPOIByDuration.filterAroundMeByDuration(aroundMeDuration);
        }
        final PMPServerManager serverManager = PMPServerManager.getShared(getActivity());
        ArrayList<PoiCategories> flattenPoiCategories = serverManager.getServerResponse().getFlattenPoiCategories();
        ArrayList<Brands> brands = serverManager.getServerResponse().getBrands();
        HashMap<Integer, ArrayList<PMPPOIByDuration>> poiMapByBrand = new HashMap<Integer, ArrayList<PMPPOIByDuration>>();
        for (PMPPOIByDuration poi : aroundMePOIs) {

            ArrayList<Integer> catIds = poi.poi.getPoiCategoryIds();
            if (poi.poi.getBrandId() > 0) {
                if (PMPServerManager.getShared().getServerResponse().getBrandMap().containsKey((int)poi.poi.getBrandId())) {
                    Brands brand = PMPServerManager.getShared().getServerResponse().getBrandMap().get((int)poi.poi.getBrandId());
                    catIds = brand.getPoiCategoryIds();
                }

            }
            boolean breakLoop = false;
            for (int catId : catIds) {
                for (PoiCategories cat : flattenPoiCategories) {
                    PoiCategories parant = cat;
                    boolean skipTheCat = false;
                    do {
                        if (parant.isStartPoint()) {
                            skipTheCat = true;
                            break;
                        }
                        parant = parant.getParentPoiCategory();
                    }while (parant != null);
                    if (skipTheCat) {
                        continue;
                    }
                    if (cat.getId() == catId) {
                        if ((restrictedOn && nonRestrictedOn) ||
                                (restrictedOn && poi.poi.getRestricted()) ||
                                (nonRestrictedOn && !poi.poi.getRestricted())) {
                            PoiCategories rootCat = cat;
                            while (rootCat.getParentPoiCategory() != null) {
                                rootCat = rootCat.getParentPoiCategory();
                            }

                            if (rootCat.isStartPoint()) {
                                breakLoop = true;
                                break;
                            }

                            ArrayList<PMPPOIByDuration> pois = poiMapByBrand.get((int)rootCat.getId());
                            if (pois == null) {
                                pois = new ArrayList<PMPPOIByDuration>();
                                poiMapByBrand.put((int)rootCat.getId(), pois);
                            }
                            PMPPOIByDuration.addDurationPOI(poi, pois);
                            breakLoop = true;
                            break;
                        }
                    }
                }
                if (breakLoop) {
                    break;
                }
            }
        }

        //this part is used for hardcode the display order of categories
        String[] orderedExternalId = new String[] {"shopping_and_dining", "facilities_and_services", "art_and_culture"};

        dataList = new ArrayList<ArrayList<PMPPOIByDuration>>();
        titles = new ArrayList<String>();
        for (int i=0; i<orderedExternalId.length; i++) {
            String externalID = orderedExternalId[i];
            boolean found = false;
            for (int key :
                    poiMapByBrand.keySet()) {
                for (PoiCategories cat : flattenPoiCategories) {
                    if (key == cat.getId() && cat.getExternalId() != null && cat.getExternalId().compareToIgnoreCase(externalID)==0) {
                        found = true;
                        dataList.add(poiMapByBrand.get(key));
                        titles.add(PMPUtil.getLocalizedString(cat.getName()));
                        poiCategories.add(cat);
                        break;
                    }
                }
                if (found) break;
            }
        }

        /*
        for (BrandCategories bCat : poiCategories) {
            ArrayList<PMPPOIByDuration> pois = poiMapByBrand.get((int)bCat.getId());
            if (pois == null) continue;
            dataList.add(pois);
            titles.add(PMPUtil.getLocalizedString(bCat.getName()));
        }*/
        listAdapter.notifyDataSetChanged();
    }

    private void updatePageContent(int pageIdx) {
        if(dataList.size() > pageIdx){
            PMPMapFragment mapFragment = (PMPMapFragment) getParentFragment();
            List<Integer> poiIds = new ArrayList<Integer>();
            ArrayList<PMPPOIByDuration> pois = dataList.get(pageIdx);
            for (PMPPOIByDuration poiByDur : pois) {
                poiIds.add((int) poiByDur.poi.getId());
            }
            mapFragment.setForcuPOIIds(poiIds);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        pager.removeOnPageChangeListener(onPageChangeListener);
        PMPMapFragment mapFragment = (PMPMapFragment) getParentFragment();
        mapFragment.adjustToolbarHeight(-120);
        mapFragment.setForcuPOIIds(null);
    }

    public class ListFragmentPagerAdapter extends FragmentStatePagerAdapter {
        private Context context;

        public ListFragmentPagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        @Override
        public int getCount() {
            return titles.size();
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public Fragment getItem(int position) {
            PoiCategories poiCategory = poiCategories.get(position);

            PMPSearchListComponentFragment fragment = new PMPSearchListComponentFragment();
            fragment.isComeFromGate = isComeFromGate;
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(PMPSearchListComponentFragment.ARGS_AROUNDMEPOIS, dataList.get(position));
            if (poiCategory.getExternalId().equals("facilities_and_services")) {
                bundle.putBoolean(PMPSearchListComponentFragment.ARGS_AROUNDMESHOW_ICON, true);
            }else {
                bundle.putBoolean(PMPSearchListComponentFragment.ARGS_AROUNDMESHOW_ICON, false);
            }
            fragment.setArguments(bundle);

            return fragment;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return titles.get(position);
        }
    }

}
