package com.pmp.mapsdk.app;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmap.PMPDataManager;
import com.cherrypicks.pmpmap.PMPMapController;
import com.cherrypicks.pmpmap.analytics.AnalyticsLogger;
import com.cherrypicks.pmpmap.core.CoreEngine;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.cms.PMPProximityServerManager;
import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.cms.model.Brands;
import com.pmp.mapsdk.cms.model.Name;
import com.pmp.mapsdk.cms.model.PMPProximityServerManagerNotifier;
import com.pmp.mapsdk.cms.model.PoiCategories;
import com.pmp.mapsdk.cms.model.Pois;
import com.pmp.mapsdk.cms.model.ResponseData;
import com.pmp.mapsdk.cms.model.SaveUserFlightResponse;
import com.pmp.mapsdk.cms.model.Tag;
import com.pmp.mapsdk.cms.model.Tags;
import com.pmp.mapsdk.external.PMPMapSDK;
import com.pmp.mapsdk.utils.Comm;
import com.pmp.mapsdk.utils.PMPUtil;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import static android.content.Context.MODE_PRIVATE;

//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by joeyyc on 23/11/2016.
 */

public class PMPSearchFragment extends Fragment {

    public static int REQUEST_CODE = 111;
    public static String SELECTED_POI_ID = "POI_IDS";
    //public static int CATEGORY_IMAGES[] = new int[] {R.drawable.icon_facilities_n_services, R.drawable.icon_shopping_n_dining, R.drawable.icon_art_n_culture};
    //public static int CATEGORY_NAMES[] = new int[] {R.string.PMPMAP_FILTER_FACILITIES_SERVICES, R.string.PMPMAP_FILTER_SHOP_DINE, R.string.PMPMAP_FILTER_ART_CULTURE};
    public static int AROUNDME_IMAGES[] = new int[] {PMPMapSDK.getMapUISetting().getAroundMeIconImageResource(), R.drawable.icon_around_boarding_gate};
    public static int AROUNDME_NAMES[] = new int[] {R.string.PMPMAP_SEARCH_AROUND_ME, R.string.PMPMAP_SEARCH_AROUND_GATE};

    private EditText searchText;
    private ImageButton closeButton;
    private ListView listViewSearch;
    private ListView listViewCategory;
    private ListView listViewAround;
    private Map<Integer, List<Pois>> poisMappedByBrand;
    private Map<Integer, List<Pois>> poisMappedByCat;
    private Map<Integer, List<Pois>> poisMappedByTag;
    private PMPSearchFragment.POICategoriesAdapter poiCategoriesAdapter;
    private PMPSearchFragment.SearchResultAdapter searchResultAdapter = new PMPSearchFragment.SearchResultAdapter();
    private PMPSearchFragment.AroundMeAdapter aroundMeAdapter = new PMPSearchFragment.AroundMeAdapter();
    private List<Pois> searchResultPOIs;
    private LinearLayout recentSearchHolder;
    private Vector<String> recentSearchKeywords;
    private List<PoiCategories> poiCategoriesList;
    private String recentSearchFileName = "RecentSearch.bin";
    private String currentGateId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.pmp_search_activity, container, false);
        searchText = (EditText)view.findViewById(R.id.edit_search);
        closeButton = (ImageButton)view.findViewById(R.id.btn_close);
        listViewSearch = (ListView)view.findViewById(R.id.listViewSearch);
        listViewCategory = (ListView)view.findViewById(R.id.listViewCategory);
        listViewAround = (ListView)view.findViewById(R.id.listViewAround);
        recentSearchHolder = (LinearLayout)view.findViewById(R.id.recent_search_holder);

        View header = view.findViewById(R.id.search_header);
        header.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());

        final PMPServerManager serverManager = PMPServerManager.getShared(getActivity());
        poisMappedByBrand = new HashMap<Integer, List<Pois>>();
        if (serverManager.getServerResponse() != null) {
//            List<Brands> brands = serverManager.getServerResponse().getBrands();
//            List<PoiCategories> poiCategories = serverManager.getServerResponse().getPoiCategories();
//            List<Tags> tags = serverManager.getServerResponse().getTags();

//            for (Pois poi : serverManager.getServerResponse().getPois()) {
//                for (Brands b : brands) {
//                    if (b.getId() == poi.getBrandId()) {
//
//                    }
//                }
//
//                for (int catId : poi.getPoiCategoryIds()) {
//                    List<Pois> arr = poisMappedByCat.get(catId);
//                    if (arr == null) {
//                        arr = new ArrayList<Pois>();
//                        poisMappedByCat.put(catId, arr);
//                    }
//                    arr.add(poi);
//                }
//            }

            listViewSearch.setAdapter(searchResultAdapter);
            listViewSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                    if(imm.isAcceptingText()) { // verify if the soft keyboard is open
                        imm.hideSoftInputFromWindow(searchText.getWindowToken(), 0);
                    }
//                    getFragmentManager().popBackStack();

                    if (adapterView.getAdapter() == searchResultAdapter) {
                        Object item = searchResultAdapter.getItem(i);
                        boolean hasDetail = true;
                        PMPMapFragment mapFragment = (PMPMapFragment) getParentFragment();
                        List<Pois> pois = new ArrayList<Pois>();
                        if (item instanceof Tags) {
                            Tags t = (Tags) item;
                            if(t.getTagArray() != null) {
                                for (Pois p : PMPDataManager.getSharedPMPManager(getActivity()).getResponseData().getPois()) {
                                    for (Tag tag : t.getTagArray()) {
                                        if (tag.getTagType().equals("Poi")) {
                                            ArrayList<Integer> poiIdArray = tag.getIdsArray();
                                            for (int id : poiIdArray) {
                                                if (id == (int) p.getId()) {
                                                    pois.add(p);
                                                    break;
                                                }
                                            }
                                        } else if (tag.getTagType().equals("PoiCategory")) {
                                            ArrayList<Integer> catIdArray = tag.getIdsArray();
                                            boolean nextPoi = false;
                                            for (int id : catIdArray) {
                                                for (int catId : p.getPoiCategoryIds()) {
                                                    if (id == catId) {
                                                        pois.add(p);
                                                        nextPoi = true;
                                                        break;
                                                    }
                                                }
                                                if (nextPoi) break;
                                            }
                                        } else if (tag.getTagType().equals("Brand")) {
                                            ArrayList<Integer> brandIdArray = tag.getIdsArray();
                                            for (int id : brandIdArray) {
                                                if (id == (int) p.getBrandId()) {
                                                    pois.add(p);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else if (item instanceof PoiCategories) {
                            ResponseData response = PMPDataManager.getSharedPMPManager(getActivity()).getResponseData();

                            for (Pois poi : response.getPois()) {
                                for (int catId : poi.getPoiCategoryIds()) {
                                    if (((PoiCategories)item).getId() == catId) {
                                        pois.add(poi);
                                        break;
                                    }
                                }
                            }
                            hasDetail = ((PoiCategories)item).isHasDetails();
                        } else if (item instanceof Brands) {
                            Brands b = (Brands) item;
                            for (Pois p : PMPDataManager.getSharedPMPManager(getActivity()).getResponseData().getPois()) {
                                if (b.getId() == p.getBrandId()) {
                                    pois.add(p);
                                }
                            }
                        }

                        addKeywordHistory(item, pois);
                        boolean shouldShowTable = false;

                        if (pois.size() > 1) {
                            loop:
                            for (Pois p : pois) {
                                if (p.getBrandId() > 0) {
                                    shouldShowTable = true;
                                    break loop;
                                }
                                for (PoiCategories cat : PMPDataManager.getSharedPMPManager(getActivity()).getResponseData().getFlattenPoiCategories()) {
                                    for (int catID : p.getPoiCategoryIds()) {
                                        if ((int) cat.getId() == catID && !cat.isHasDetails()) {
                                            shouldShowTable = shouldShowTable | cat.isHasDetails();
                                            if (shouldShowTable) {
                                                break loop;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        PMPMapController.getInstance().setOverviewing(false);
                        if (pois.size() > 1) {
                            mapFragment.showPOIListOnMap(pois, item, shouldShowTable, true, true,  true);
                        } else {
                            mapFragment.showPOIListOnMap(pois, item, shouldShowTable, false, true, true);
                        }
                        for (int n = 0; n < getFragmentManager().getBackStackEntryCount(); n++) {
                            getFragmentManager().popBackStack();
                        }
                    }
                }
            });

            poiCategoriesList = new ArrayList<>();
            final ArrayList<PoiCategories> allCats = PMPDataManager.getSharedPMPManager(null).getResponseData().getPoiCategories();
            for (PoiCategories cat : allCats) {
                if (cat.isInvisible() ||
                        cat.isStartPoint()) {
                    continue;
                }
                poiCategoriesList.add(cat);
            }
            poiCategoriesAdapter = new PMPSearchFragment.POICategoriesAdapter();
            listViewCategory.setAdapter(poiCategoriesAdapter);
            listViewCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    PMPUtil.hideKeyboard(getActivity());

                    if (adapterView.getAdapter() == poiCategoriesAdapter) {
                        HashMap<String, Object> params = new HashMap<String, Object>();

                        PoiCategories poiCategory = PMPDataManager.getSharedPMPManager(null).getResponseData().getPoiCategories().get(i);
                        String externalId = poiCategory.getExternalId();

                        if(externalId == null)return;

                        if (externalId.equals("facilities_and_services")) { //Facilities and Services
                            PMPSearchListFragment fragment = new PMPSearchListFragment();
                            Bundle bundle = new Bundle();
                            String name = PMPUtil.getLocalizedString(poiCategory.getName());
                            bundle.putString(PMPSearchListFragment.ARGS_TITLE, name);
                            bundle.putSerializable(PMPSearchListFragment.ARGS_CATEGORY, poiCategory);
                            fragment.setArguments(bundle);

                            getFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.fragment_container, fragment)
                                    .addToBackStack("")
                                    .commit();

                            params.put("type", "FS_1");
                        } else if (externalId.equals("shopping_and_dining")) {
                            if (PMPMapSDK.getOpenShoppingAndDiningCallback() != null) {
                                PMPMapSDK.getOpenShoppingAndDiningCallback().run();
                            }
                            params.put("type", "SD_2");

                        } else if (externalId.equals("art_and_culture")) {
                            if (PMPMapSDK.getOpenArtAndCultureCallback() != null) {
                                PMPMapSDK.getOpenArtAndCultureCallback().run();
                            }
                            params.put("type", "AC_2");

                        }

                        AnalyticsLogger.getInstance().logEvent("Filter_Select_Catalog", params);
                    }
                }
            });

            listViewAround.setAdapter(aroundMeAdapter);
            listViewAround.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    PMPUtil.hideKeyboard(getActivity());

                    if (adapterView.getAdapter() == aroundMeAdapter) {
                        if (i==0) {
                            showAroundMe();
                        } else {
                            showAroundBoardingGate(currentGateId);
                        }
                    }
                }
            });
        }

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0) {
                    listViewSearch.setVisibility(View.GONE);
                }else {
                    listViewSearch.setVisibility(View.VISIBLE);
                    ArrayList result = new ArrayList();
                    if (serverManager.getServerResponse() != null) {
                        if (serverManager.getServerResponse().getTags() != null) {
                            for (Tags tag : serverManager.getServerResponse().getTags()) {
                                if (tag.getContent().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                                    result.add(tag);
//                                    break;
                                }
                            }
                        }

                        if (serverManager.getServerResponse().getFlattenPoiCategories() != null) {
                            for (PoiCategories cat : serverManager.getServerResponse().getFlattenPoiCategories()) {
                                if (!cat.isUnsearchable()) {
                                    for (Name name : cat.getName()) {
                                        if (name != null && name.getContent() != null) {
                                            if (name.getContent().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                                                result.add(cat);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (serverManager.getServerResponse().getBrands() != null) {
                            for (Brands brand : serverManager.getServerResponse().getBrands()) {
                                for (Name name : brand.getName()) {
                                    if (name != null && name.getContent() != null) {
                                        if (name.getContent().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                                            result.add(brand);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    searchResultPOIs = result;
                    searchResultAdapter.notifyDataSetInvalidated();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String, Object> params = new HashMap<>();
                if(searchText.getText().length() > 0){
                    params.put("state", "1");
                }else{
                    params.put("state", "0");
                }
                AnalyticsLogger.getInstance().logEvent("Search_Close", params);

                PMPUtil.hideKeyboard(getActivity());
                getFragmentManager().popBackStack();
                /*finish();*/
            }
        });

        recentSearchKeywords = new Vector<String>();

        return view;
    }

    @Override
    public void onStart(){
        super.onStart();
        if (getArguments() != null) {
            Bundle args = getArguments();
            int displayOption = args.getInt("display_option",PMPMapFragment.PMPMapFragmentDisplayOptionDefault);
            if(displayOption == PMPMapFragment.PMPMapFragmentDisplayOptionAroundMe){
                showAroundMe();
            }else if(displayOption == PMPMapFragment.PMPMapFragmentDisplayOptionAroundBoardingGate){
                String gateId = "";
                if(getArguments() != null){
                    gateId = getArguments().getString("gate_id");
                }
                showAroundBoardingGate(gateId);
            }
            args.putInt("display_option",PMPMapFragment.PMPMapFragmentDisplayOptionDefault);
        }
    }

    private void showAroundMe(){
        if (CoreEngine.getInstance().getIndoorLocation() == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.PMPMAP_AROUND_ME_OUT_ZONE_TITLE);
            builder.setMessage(R.string.PMPMAP_AROUND_ME_OUT_ZONE_MSG);
            builder.setPositiveButton(R.string.PMPMAP_SEARCH_BUTTON_CONFIRM, null);
            builder.show();
        }else {
            PMPSearchAroundFragment fragment = new PMPSearchAroundFragment();
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment, "PMPSearchAroundFragment")
                    .addToBackStack("")
                    .commit();
        }
        AnalyticsLogger.getInstance().logEvent("Filter_Around_Me");
    }

    private void showAroundBoardingGate(String gateId){
        Pois poi = null;
        if (!TextUtils.isEmpty(gateId)) {
            if (PMPServerManager.getShared().getServerResponse() != null) {
                for (Pois p : PMPServerManager.getShared().getServerResponse().getPois()) {
                    if (p.getExternalId() != null && gateId.equals(p.getExternalId())) {
                        poi = p;
                        break;
                    }
                }
            }
        }

        //if no saved flight, open search List showing all gates,
        //else open poi list around saved gate
        if (poi == null) {
            PMPSearchListFragment fragment = new PMPSearchListFragment();

            Bundle bundle = new Bundle();
            bundle.putString(PMPSearchListFragment.ARGS_TITLE, getResources().getString(PMPSearchFragment.AROUNDME_NAMES[1]));
            fragment.setArguments(bundle);

            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .addToBackStack("")
                    .commit();
        } else {
            PMPSearchAroundFragment fragment = new PMPSearchAroundFragment();

            Bundle bundle = new Bundle();
            bundle.putParcelable(PMPSearchAroundFragment.ARGS_GATE, poi);
            fragment.setArguments(bundle);

            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment, "PMPSearchAroundFragment")
                    .addToBackStack("")
                    .commit();
        }

        AnalyticsLogger.getInstance().logEvent("Filter_Boarding_Gate");
    }

    private void onSelectPOICategory(PoiCategories category) {
        List<Pois> pois = poisMappedByCat.get((int)category.getId());
        int poiIds[] = new int[pois.size()];
        int i = 0;
        for (Pois poi : pois) {
            poiIds[i] = (int)poi.getId();
            i++;
        }
//        Intent intent = new Intent(this, PMPSearchResultByFilterActivity.class);
//        intent.putExtra(PMPSearchResultByFilterActivity.POI_IDS, poiIds);
//        intent.putExtra(PMPSearchResultByFilterActivity.HEADER, PMPUtil.getLocalizedString(category.getName()));
//        startActivityForResult(intent, REQUEST_CODE);
    }

    private void onSelectPOI(Pois poi) {

        Intent data = new Intent();
        data.putExtra(SELECTED_POI_ID, (int) poi.getId());
//        setResult(Activity.RESULT_OK, data);
//        finish();

        List<Pois> list = new ArrayList<>();
        list.add(poi);
        addKeywordHistory(poi, list);

    }

    private void addKeywordHistory(Object obj, List<Pois> pois) {
        if (searchText.length() != 0) {
            String keyword = null;

            HashMap<String, Object> params = new HashMap<>();
            ArrayList<Integer> poiIDs = new ArrayList<>();
            for(Pois poi : pois){
                poiIDs.add((int)poi.getId());
            }
            if(poiIDs.size() > 0)params.put("searched_poi_id", Comm.arrayListToString(poiIDs));

            if (obj instanceof Tags) {
                Tags tagsObj = ((Tags)obj);
                keyword = tagsObj.getContent();
                params.put("type", "tag");

            } else if (obj instanceof PoiCategories) {
                PoiCategories cat = (PoiCategories)obj;
                for (Name name : cat.getName()) {
                    if (name.getContent().toLowerCase().contains(searchText.getText().toString().toLowerCase())) {
                        keyword = name.getContent();
                        break;
                    }
                }
                params.put("type", "poi_cat");

            } else if (obj instanceof Brands) {
                Brands brand = (Brands)obj;
                for (Name name : brand.getName()) {
                    if (name.getContent().toLowerCase().contains(searchText.getText().toString().toLowerCase())) {
                        keyword = name.getContent();
                        break;
                    }
                }
                params.put("type", "brand");

            }

            if (keyword == null) {
                keyword = searchText.getText().toString();
                params.put("type", "poi");
            }

            params.put("keyword", keyword);
            if(searchText.getText().toString().equals(keyword)){
                AnalyticsLogger.getInstance().logEvent("Search_By_Recent", params);
            }else{
                AnalyticsLogger.getInstance().logEvent("Search_By_Keyword", params);
            }


            if (recentSearchKeywords.contains(keyword)) {
                recentSearchKeywords.remove(keyword);
            }
            recentSearchKeywords.insertElementAt(keyword, 0);

            while (recentSearchKeywords.size() > PMPDataManager.getSharedPMPManager(getActivity()).getResponseData().getMapNumRecentSearch()) {
                recentSearchKeywords.remove(recentSearchKeywords.size() - 1);
            }
            FileOutputStream fos = null;
            ObjectOutputStream oos = null;
            try {
                fos = getActivity().openFileOutput(recentSearchFileName, MODE_PRIVATE);
                oos = new ObjectOutputStream(fos);
                oos.writeObject(recentSearchKeywords);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                if (oos != null) {
                    try {
                        oos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (PMPMapSDK.getTabBarVisibilityUpdateCallback() != null) {
            PMPMapSDK.getTabBarVisibilityUpdateCallback().updateTabBarVisibilityUpdate(true);
        }
        PMPProximityServerManager.getShared(getActivity())
                .getUserSavedFlight(new PMPProximityServerManagerNotifier() {
            @Override
            public void didSuccess(Object response) {
                if (response instanceof SaveUserFlightResponse) {
                    SaveUserFlightResponse r = (SaveUserFlightResponse) response;
                    if (r != null && r.getResult() != null) {
                        if (!TextUtils.isEmpty(r.getResult().getMtelRecordId())) {
                            currentGateId = r.getResult().getGateCode();
                            aroundMeAdapter.notifyDataSetInvalidated();
                        }
                    }
                }
            }

            @Override
            public void didFailure() {

            }
        });
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = getActivity().openFileInput(recentSearchFileName);
            ois = new ObjectInputStream(fis);
            recentSearchKeywords = (Vector<String>) ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
            recentSearchHolder.removeAllViews();
            if (recentSearchKeywords != null && recentSearchKeywords.size() != 0) {
                //list top layout
                View firstConvertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_search_top, null);
                firstConvertView.setBackgroundColor(PMPMapSDK.getMapUISetting().getRecentSearchBgColor());
                TextView firstTv_name = (TextView) firstConvertView.findViewById(R.id.tv_name);
                firstTv_name.setTextColor(PMPMapSDK.getMapUISetting().getRecentSearchTextColor());
                firstTv_name.setText(getResources().getText(R.string.PMPMAP_SEARCH_RECENT_SEARCH));
                recentSearchHolder.addView(firstConvertView);

                //list middle layout
                for (int i=0; i<recentSearchKeywords.size()-1; i++) {
                    final String keyword = recentSearchKeywords.get(i);
                    View convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_search, null);
                    convertView.setBackgroundColor(PMPMapSDK.getMapUISetting().getRecentSearchBgColor());
                    TextView tv_name = (TextView) convertView.findViewById(R.id.tv_name);
                    tv_name.setText(keyword);
                    tv_name.setTextColor(PMPMapSDK.getMapUISetting().getRecentSearchTextColor());
                    ImageView iv = (ImageView)convertView.findViewById(R.id.iv_icon);
                    iv.setImageResource(R.drawable.search_icon_history);
                    recentSearchHolder.addView(convertView);

                    convertView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            searchText.setText(keyword);
                        }
                    });
                }

                //list bottom layout
                final String keyword = recentSearchKeywords.lastElement();
                View convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_search_bottom, null);
                convertView.setBackgroundColor(PMPMapSDK.getMapUISetting().getRecentSearchBgColor());
                TextView tv_name = (TextView) convertView.findViewById(R.id.tv_name);
                tv_name.setText(keyword);
                tv_name.setTextColor(PMPMapSDK.getMapUISetting().getRecentSearchTextColor());
                ImageView iv = (ImageView)convertView.findViewById(R.id.iv_icon);
                iv.setImageResource(R.drawable.search_icon_history);
                recentSearchHolder.addView(convertView);

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        searchText.setText(keyword);
                    }
                });
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private class POICategoriesAdapter extends BaseAdapter {
        public POICategoriesAdapter() {
        }

        @Override
        public String getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            if(convertView == null){
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_poi_cat, null);
            }
            PoiCategories poiCategories = PMPDataManager.getSharedPMPManager(null).getResponseData().getPoiCategories().get(position);
            DisplayMetrics metrics = getResources().getDisplayMetrics();
            TextView tv_name = (TextView)convertView.findViewById(R.id.tv_name);
            String name = PMPUtil.getLocalizedString(poiCategories.getName());
            tv_name.setText(name);
            tv_name.setTextColor(Color.parseColor("#848484"));
            tv_name.setTextSize(14);
            tv_name.setTypeface(Typeface.DEFAULT);
            ((RelativeLayout.LayoutParams)tv_name.getLayoutParams()).setMargins(((int)(32*metrics.densityDpi)/DisplayMetrics.DENSITY_DEFAULT),0,((int)(16*metrics.densityDpi)/DisplayMetrics.DENSITY_DEFAULT),0);
            ImageView iv = (ImageView)convertView.findViewById(R.id.iv_icon);
            PMPUtil.setImageWithURL(iv, poiCategories.getImage());
//            iv.setImageDrawable(ContextCompat.getDrawable(getContext(), PMPSearchFragment.CATEGORY_IMAGES[position]));
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(((int)(24*metrics.densityDpi)/DisplayMetrics.DENSITY_DEFAULT), ((int)(24*metrics.densityDpi)/DisplayMetrics.DENSITY_DEFAULT));
            params.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
            params.setMargins(((int)(16*metrics.densityDpi)/DisplayMetrics.DENSITY_DEFAULT),0,0,0);
            iv.setLayoutParams(params);

            return convertView;
        }

        @Override
        public int getCount() {
            return poiCategoriesList.size();
        }
    }

    private class AroundMeAdapter extends BaseAdapter {
        public AroundMeAdapter() {
        }

        @Override
        public String getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            if(convertView == null){
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_aroundme, null);
            }

            TextView tv_name = (TextView)convertView.findViewById(R.id.tv_name);
            tv_name.setText(getResources().getText(PMPSearchFragment.AROUNDME_NAMES[position]));
            tv_name.setTextColor(PMPMapSDK.getMapUISetting().getThemeColor());
            if (position == 1) {
                if (TextUtils.isEmpty(currentGateId)) {
                    tv_name.setText(getResources().getText(PMPSearchFragment.AROUNDME_NAMES[position]));
                }else {
                    Pois poi = null;
                    if (PMPServerManager.getShared().getServerResponse() != null) {
                        for (Pois p : PMPServerManager.getShared().getServerResponse().getPois()) {
                            if (currentGateId.equals(p.getExternalId())) {
                                poi = p;
                                break;
                            }
                        }
                    }

                    if (poi != null) {
                        String gateName = PMPUtil.getLocalizedString(poi.getName());
                        String strFormat = getString(R.string.PMPMAP_AROUND_MY_GATE_TITLE);
                        String s = String.format(strFormat, gateName);
                        tv_name.setText(s);
                    }else {
                        tv_name.setText(getResources().getText(PMPSearchFragment.AROUNDME_NAMES[position]));
                    }
                }
            }

            ImageView iv = (ImageView)convertView.findViewById(R.id.iv_icon);
            iv.setImageDrawable(ContextCompat.getDrawable(getContext(), PMPSearchFragment.AROUNDME_IMAGES[position]));
            return convertView;
        }

        @Override
        public int getCount() {
            return PMPSearchFragment.AROUNDME_NAMES.length;
        }
    }

    private class SearchResultAdapter extends BaseAdapter {
        @Override
        public Object getItem(int i) {
            return searchResultPOIs.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            if(convertView == null){
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_search_result, null);
            }

            Object obj = getItem(position);
            RelativeLayout relativeLayout = (RelativeLayout)convertView.findViewById(R.id.rl_background);
            TextView tv_name = (TextView)convertView.findViewById(R.id.tv_name);
            ImageView iv_icon = (ImageView)convertView.findViewById(R.id.iv_icon);
            String searchTitle = searchText.getText().toString().toLowerCase();
            String title;
            SpannableString titleToSpan;

            if (obj.getClass().getSimpleName().equalsIgnoreCase("PoiCategories")) {
                title = PMPUtil.getLocalizedString(((PoiCategories)obj).getName()).toLowerCase();
                titleToSpan = new SpannableString(PMPUtil.getLocalizedString(((PoiCategories)obj).getName()));
                PMPUtil.setImageWithURL(iv_icon, ((PoiCategories)obj).getImage());
            } else if (obj.getClass().getSimpleName().equalsIgnoreCase("Brands")) {
                title = PMPUtil.getLocalizedString(((Brands)obj).getName()).toLowerCase();
                titleToSpan = new SpannableString(PMPUtil.getLocalizedString(((Brands)obj).getName()));
                iv_icon.setImageResource(R.drawable.icon_shopping_n_dining);
            } else if (obj.getClass().getSimpleName().equalsIgnoreCase("Tags")) {
                title = ((Tags)obj).getContent().toLowerCase();
                titleToSpan = new SpannableString(((Tags)obj).getContent());
                iv_icon.setImageResource(R.drawable.icon_search_blue);
            } else {
                title = "";
                titleToSpan = new SpannableString("");
                iv_icon.setImageDrawable(null);
            }

            //set background color
            if (position%2 == 0) {
                relativeLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));
            } else {
                relativeLayout.setBackgroundColor(Color.parseColor("#E9E9E9"));
            }

            int index = title.indexOf(searchTitle);
            if (index != -1) {
                titleToSpan.setSpan(new ForegroundColorSpan(PMPMapSDK.getMapUISetting().getThemeColor()), index, index+searchTitle.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            tv_name.setTextColor(Color.parseColor("#949494"));
            tv_name.setText(titleToSpan);

            return convertView;
        }

        @Override
        public int getCount() {
            if (searchResultPOIs == null)
                return 0;

            return searchResultPOIs.size();
        }
    }

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }


}
