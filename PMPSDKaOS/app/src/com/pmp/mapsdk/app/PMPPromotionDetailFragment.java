package com.pmp.mapsdk.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.cms.model.Pois;
import com.pmp.mapsdk.cms.model.Promotions;
import com.pmp.mapsdk.external.PMPMapSDK;
import com.pmp.mapsdk.utils.PMPUtil;

/**
 * Created by andrew on 31/10/2016.
 */

public class PMPPromotionDetailFragment extends Fragment {
    private final static String TAG = "Promotion Detail";
    private Promotions promotion;
    private TextView lblImgTtl, lblImgMsg, lblDurationValue, lblPromotionDetailTitle, lblDesc;
    private ImageView imgVwPOI;
    private ImageButton btnClose;
    private RelativeLayout viewGroupAddress;
    private View loadingView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //set views
        View view = inflater.inflate(R.layout.pmp_detail_promotion_fragment, container, false);
        imgVwPOI = (ImageView) view.findViewById(R.id.imgVwPOI);
        lblImgTtl = (TextView) view.findViewById(R.id.lblImgTtl);
        lblDesc = (TextView) view.findViewById(R.id.lblDesc);
        lblPromotionDetailTitle = (TextView) view.findViewById(R.id.lblPromotionDetailTitle);
        lblDurationValue = (TextView) view.findViewById(R.id.lblDurationValue);
        lblImgMsg = (TextView) view.findViewById(R.id.lblImgMsg);
        viewGroupAddress = (RelativeLayout) view.findViewById(R.id.viewGroupAddress);
        loadingView = view.findViewById(R.id.loading_view);
        btnClose = (ImageButton) view.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });



        float fontScale = PMPMapSDK.getFontScale();
        lblImgTtl.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontScale * lblImgTtl.getTextSize());
        lblImgMsg.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontScale * lblImgMsg.getTextSize());
        lblDurationValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontScale * lblDurationValue.getTextSize());
        lblPromotionDetailTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontScale * lblPromotionDetailTitle.getTextSize());
        lblDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontScale * lblDesc.getTextSize());

        init();

        return view;
    }

    public void setPromotion(Promotions promotion) {
        this.promotion = promotion;
    }

    public void init() {
        if (promotion != null) {
//            lblImgTtl, lblImgMsg, lblDurationValue, lblPromotionDetailTitle, lblDesc;
            lblImgTtl.setText("");
            lblImgMsg.setText("");
            lblDurationValue.setText("");
            lblPromotionDetailTitle.setText("");
            lblDesc.setText("");
            for (Pois poi : PMPServerManager.getShared(getContext()).getServerResponse().getPois()) {
                if (poi.getId() == promotion.getReferenceId()) {
                    //Found the related poi!
                    lblImgTtl.setText(PMPUtil.getLocalizedString(poi.getName()));
                    break;
                }
            }

            lblImgMsg.setText(PMPUtil.getLocalizedString(promotion.getMessage()));
            PMPUtil.setImageWithURL(imgVwPOI, promotion.getBannerIcon());
            if(promotion.getPromotionMessages() != null && promotion.getPromotionMessages().size() > 0)
                lblPromotionDetailTitle.setText(PMPUtil.getLocalizedString(promotion.getPromotionMessages().get(0).getDetails()));
            lblDurationValue.setText(promotion.getStartAt() + " - " + promotion.getEndAt());
        }
    }
}
