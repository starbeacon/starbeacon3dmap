package com.pmp.mapsdk.app;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.external.PMPMapSDK;

/**
 * Created by joeyyc on 12/12/2016.
 */

public class PMPSearchAroundMeFilterFragment extends Fragment {
    public static final String ARGS_DURATION = "ARGS_DURATION";
    public static final String ARGS_RESTRICTED = "ARGS_RESTRICTED";
    public static final String ARGS_NON_RESTRICTED = "ARGS_NON_RESTRICTED";
    public static final String ARGS_SHOW_RESTRICTED_OPTION = "ARGS_SHOW_RESTRICTED_OPTION";

    ImageButton btnClose;
    ImageButton btnSubstract;
    ImageButton btnAdd;
    Button btnNonRestrictedArea;
    Button btnRestrictedArea;
    Button btnConfirm;
    TextView tv_Duration;
    TextView tv_DurationTitle;
    TextView tv_areaTitle;

    private float[] durationOptions = {5*60, 10*60, 15*60};

    private int _durationIdx = 0;
    private boolean _isRestrictedOn = false;
    private boolean _isNonRestrictedOn = false;
    private boolean showRestrictedOption = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pmp_search_around_filter_fragment, container, false);

        btnClose = (ImageButton)view.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClose();
            }
        });
        btnSubstract = (ImageButton)view.findViewById(R.id.btnSubstract);
        btnSubstract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDurationChanged(v);
            }
        });
        btnAdd = (ImageButton)view.findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDurationChanged(v);
            }
        });
        btnNonRestrictedArea = (Button) view.findViewById(R.id.btnNonRestrictedArea);
        btnNonRestrictedArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onToggleAreaButton(v);
            }
        });
        btnRestrictedArea = (Button) view.findViewById(R.id.btnRestrictedArea);
        btnRestrictedArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onToggleAreaButton(v);
            }
        });
        btnConfirm = (Button)view.findViewById(R.id.btnConfirm);
        btnConfirm.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onConfirm();
            }
        });
        tv_Duration = (TextView) view.findViewById(R.id.tv_Duration);
        tv_DurationTitle = (TextView) view.findViewById(R.id.textViewDurationTitle);
        tv_areaTitle =  (TextView) view.findViewById(R.id.area_title);

        tv_Duration.setTextColor(PMPMapSDK.getMapUISetting().getThemeColor());

        tv_DurationTitle.setText(getLocalizedString("PMPMAP_SEARCH_FILTER_DURATION"));
        btnRestrictedArea.setText(getLocalizedString("PMPMAP_SEARCH_OPTION_RESTRICTED_AREA"));
        btnNonRestrictedArea.setText(getLocalizedString("PMPMAP_SEARCH_OPTION_NON_RESTRICTED_AREA"));
        tv_areaTitle.setText(getLocalizedString("PMPMAP_SEARCH_FILTER_AREA"));
        btnConfirm.setText(getLocalizedString("PMPMAP_SEARCH_BUTTON_CONFIRM"));

        Bundle bundle = getArguments();
        float duration_args = bundle.getFloat(ARGS_DURATION);
        _isRestrictedOn = !bundle.getBoolean(ARGS_RESTRICTED);
        _isNonRestrictedOn = !bundle.getBoolean(ARGS_NON_RESTRICTED);
        showRestrictedOption = bundle.getBoolean(ARGS_SHOW_RESTRICTED_OPTION, true);
        if (showRestrictedOption) {
            view.findViewById(R.id.area_holder).setVisibility(View.VISIBLE);
        }else {
            view.findViewById(R.id.area_holder).setVisibility(View.GONE);
            _isRestrictedOn = !true;
            _isNonRestrictedOn = !false;
        }
        onToggleAreaButton(btnNonRestrictedArea);
        onToggleAreaButton(btnRestrictedArea);

        for (int i=0; i<durationOptions.length; i++) {
            if (duration_args == durationOptions[i]) {
                _durationIdx = i;
                break;
            }
        }
        setDurationDisplay();
        setControlButtonColour();

        return view;
    }

    private void onClose() {
        getFragmentManager().popBackStack();
    }

    private void onConfirm() {
        Fragment fragment = getFragmentManager().findFragmentByTag("PMPSearchAroundFragment");

        if (fragment != null && fragment instanceof PMPSearchAroundFragment) {
            ((PMPSearchAroundFragment)fragment).updateDurationValue(durationOptions[_durationIdx], _isRestrictedOn, _isNonRestrictedOn);
            getFragmentManager().popBackStack();
        }
    }

    private void onDurationChanged(View v) {
        if (v == btnSubstract) {
            if (_durationIdx > 0) {
                _durationIdx--;
                setDurationDisplay();
            }
        } else if (v == btnAdd) {
            if (_durationIdx < durationOptions.length - 1) {
                _durationIdx++;
                setDurationDisplay();
            }
        }

        setControlButtonColour();
    }

    private void setControlButtonColour() {
        if (_durationIdx == 0) {
            btnSubstract.setImageResource(R.drawable.icon_subtract_grey);
        } else {
            btnSubstract.setImageResource(R.drawable.icon_subtract);
        }

        if (_durationIdx == durationOptions.length-1) {
            btnAdd.setImageResource(R.drawable.icon_add_grey);
        } else {
            btnAdd.setImageResource(R.drawable.icon_add);
        }
    }

    private void onToggleAreaButton(View v) {
        if (v == btnNonRestrictedArea) {
            _isNonRestrictedOn = !_isNonRestrictedOn;
            if (_isNonRestrictedOn) {
                btnNonRestrictedArea.setTextColor(ContextCompat.getColor(getActivity(), R.color.Search_Filter_Button_Color_Selected));
                btnNonRestrictedArea.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.search_filter_button_bounds_selected));
            } else {
                btnNonRestrictedArea.setTextColor(ContextCompat.getColor(getActivity(), R.color.Search_Filter_Button_Color_Deselect));
                btnNonRestrictedArea.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.search_filter_button_bounds_deselected));
            }
        } else if (v == btnRestrictedArea) {
            _isRestrictedOn = !_isRestrictedOn;
            if (_isRestrictedOn) {
                btnRestrictedArea.setTextColor(ContextCompat.getColor(getActivity(), R.color.Search_Filter_Button_Color_Selected));
                btnRestrictedArea.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.search_filter_button_bounds_selected));
            } else {
                btnRestrictedArea.setTextColor(ContextCompat.getColor(getActivity(), R.color.Search_Filter_Button_Color_Deselect));
                btnRestrictedArea.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.search_filter_button_bounds_deselected));
            }
        }
    }

    private void setDurationDisplay() {
        tv_Duration.setText(String.format(getResources().getString(R.string.PMPMAP_N_MINS), String.format("%.0f", (durationOptions[_durationIdx]/60))));
    }
    public static String getLocalizedString(String name){
        if(PMPMapSDK.getApplication() != null){
            int id = PMPMapSDK.getApplication().getResources().getIdentifier(name, "string", PMPMapSDK.getApplication().getPackageName());
            return PMPMapSDK.getApplication().getString(id);
        }
        return "";
    }
}
