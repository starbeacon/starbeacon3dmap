package com.pmp.mapsdk.app;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.cms.model.PoiCategories;
import com.pmp.mapsdk.cms.model.Pois;
import com.pmp.mapsdk.external.PMPMapSDK;
import com.pmp.mapsdk.utils.PMPUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joeyyc on 2/2/2017.
 */

public class PMPSearchTransportFragment extends Fragment {
    public static final String ARGS_VIEW_TYPE = "ARGS_VIEW_TYPE";
    public static final String ARGS_DEST_AREA_ID = "ARGS_DEST_AREA_ID";
    public static final int CAT_AF = 121;
    public static final int CAT_FE = 122;
    public static final int CAT_AE = 130;
    public static final int CAT_BU = 131;
    public static final int CAT_TA = 132;
    public static final int VIEW_TYPE_IN_HK = 1;
    public static final int VIEW_TYPE_NOT_IN_HK = 2;
    public static final int VIEW_TYPE_BUS = 3;

    private ListView listView;
    private ArrayList data;
    private TransportationAdapter transportationAdapter = new TransportationAdapter();
    private BusAdapter busAdapter = new BusAdapter();
    private AdapterView.OnItemClickListener categoryListener;
    private AdapterView.OnItemClickListener busListener;
    private int currentViewType;
    private double destAreaID = -1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            currentViewType = bundle.getInt(ARGS_VIEW_TYPE);
            destAreaID = bundle.getDouble(ARGS_DEST_AREA_ID, -1);
        }

        View view = inflater.inflate(R.layout.pmp_search_list_transport, container, false);
        View header = view.findViewById(R.id.header);
        header.setBackgroundColor(PMPMapSDK.getMapUISetting().getThemeColor());
        TextView tvHeader = (TextView)view.findViewById(R.id.tvHeader);
        ImageButton btnBack = (ImageButton)view.findViewById(R.id.btnBack);
        listView = (ListView)view.findViewById(R.id.listview);

        tvHeader.setText(getResources().getString(R.string.PMPMAP_YOUR_LOCATIONS));
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        categoryListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PoiCategories cat = (PoiCategories) data.get(position);
                if (cat.getId() == CAT_BU) {
                    PMPSearchTransportFragment fragment = new PMPSearchTransportFragment();

                    Bundle bundle = new Bundle();
                    bundle.putInt(ARGS_VIEW_TYPE, VIEW_TYPE_BUS);
                    bundle.putDouble(ARGS_DEST_AREA_ID, destAreaID);

                    fragment.setArguments(bundle);
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fragment_container, fragment)
                            .addToBackStack("")
                            .commit();

                    return;
                }
                ArrayList<Pois> array = findPOIForCat(cat);
                if (array.size() > 1) {
                    for (Pois poi : array) {
                        if (poi.getAreaId() == destAreaID) {
                            PMPMapFragment mapFragment = (PMPMapFragment) getParentFragment();
                            mapFragment.selectLocationForPath(poi);
                            for (int n = 0; n < getFragmentManager().getBackStackEntryCount(); n++) {
                                getFragmentManager().popBackStack();
                            }
                            return;
                        }
                    }
                }
                if (array.size() > 0) {
                    PMPMapFragment mapFragment = (PMPMapFragment) getParentFragment();
                    mapFragment.selectLocationForPath(array.get(0));
                    for (int n = 0; n < getFragmentManager().getBackStackEntryCount(); n++) {
                        getFragmentManager().popBackStack();
                    }
                    return;
                }
            }
        };

        busListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pois poi = (Pois)data.get(position);
                PMPMapFragment mapFragment = (PMPMapFragment) getParentFragment();
                mapFragment.selectLocationForPath(poi);
                for (int n = 0; n < getFragmentManager().getBackStackEntryCount(); n++) {
                    getFragmentManager().popBackStack();
                }
                return;
            }
        };

        loadData();

        return view;
    }

    private void loadData() {
        data = new ArrayList<>();
        ArrayList<PoiCategories> transportations = new ArrayList<PoiCategories>();

        final PMPServerManager serverManager = PMPServerManager.getShared(getActivity());

        transportations = new ArrayList<PoiCategories>();
        for (PoiCategories cat : serverManager.getServerResponse().getTransportationList()) {
            for (PoiCategories locCat : cat.getSub_categories()) {
                transportations.addAll(locCat.getSub_categories());
            }
        }

        PoiCategories busCat;
        switch (currentViewType) {
            case VIEW_TYPE_IN_HK:
                for (PoiCategories cat : transportations) {
                    if (cat.getId() != CAT_AF) {
                        data.add(cat);
                    }
                }
                listView.setAdapter(transportationAdapter);
                listView.setOnItemClickListener(categoryListener);
                transportationAdapter.notifyDataSetInvalidated();
                break;
            case VIEW_TYPE_NOT_IN_HK:
                for (PoiCategories cat : transportations) {
                    if (cat.getId() == CAT_AF ||
                            cat.getId() == CAT_FE) {
                        data.add(cat);
                    }
                }
                listView.setAdapter(transportationAdapter);
                listView.setOnItemClickListener(categoryListener);
                transportationAdapter.notifyDataSetInvalidated();
                break;
            case VIEW_TYPE_BUS:
                for (PoiCategories cat : transportations) {
                    if (cat.getId() == CAT_BU) {
                        busCat = cat;

                        final List<Pois> poiList = serverManager.getServerResponse().getPois();
                        for (Pois poi : poiList) {
                            if (poi.getPoiCategoryIds().contains((int)busCat.getId())) {
                                data.add(poi);
                            }
                        }
                        break;
                    }
                }
                listView.setAdapter(busAdapter);
                listView.setOnItemClickListener(busListener);
                busAdapter.notifyDataSetInvalidated();
                break;
        }
    }

    private ArrayList<Pois> findPOIForCat(PoiCategories category) {
        final ArrayList<Pois> poiList = PMPServerManager.getShared(getActivity()).getServerResponse().getPois();
        ArrayList<Pois> pois = new ArrayList<Pois>();
        if (category != null) {
            for (Pois poi : poiList) {
                if (poi.getPoiCategoryIds().contains((int)category.getId())) {
                    pois.add(poi);
                }
            }
        }
        return pois;
    }

    private class TransportationAdapter extends BaseAdapter {
        public TransportationAdapter() {
        }

        @Override
        public String getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            if(convertView == null){
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_transport, null);
            }

            PoiCategories cat = (PoiCategories)data.get(position);

            TextView tvTitle = (TextView)convertView.findViewById(R.id.tvTitle);
            tvTitle.setText(PMPUtil.getLocalizedString(cat.getName()));
            ImageView iv = (ImageView)convertView.findViewById(R.id.iv_Poi);
            switch ((int)cat.getId()) {
                case CAT_AF:
                    iv.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.img_arrival_flight));
                    break;
                case CAT_FE:
                    iv.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.img_ferry));
                    break;
                case CAT_AE:
                    iv.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.img_airport_express));
                    break;
                case CAT_BU:
                    iv.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.img_bus));
                    break;
                case CAT_TA:
                    iv.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.img_taxi_coach_hotel_shuttle));
                    break;
            }
            return convertView;
        }

        @Override
        public int getCount() {
            return data.size();
        }
    }

    private class BusAdapter extends BaseAdapter {
        public BusAdapter() {
        }

        @Override
        public String getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            if(convertView == null){
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_transport, null);
            }

            Pois poi = (Pois) data.get(position);

            TextView tv_name = (TextView)convertView.findViewById(R.id.tvTitle);
            tv_name.setText(PMPUtil.getLocalizedString(poi.getName()));
            ImageView iv = (ImageView)convertView.findViewById(R.id.iv_Poi);
            PMPUtil.setImageWithURL(iv, poi.getImage());
            return convertView;
        }

        @Override
        public int getCount() {
            return data.size();
        }
    }
}
