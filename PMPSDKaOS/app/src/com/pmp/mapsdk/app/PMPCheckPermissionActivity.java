package com.pmp.mapsdk.app;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.cherrypicks.pmpmap.GPSLocationManager;

import static com.cherrypicks.pmpmap.GPSLocationManager.PMP_PERMISSIONS_REQUEST_LOCATION;

/**
 * Created by Yu on 24/2/2017.
 */

public class PMPCheckPermissionActivity extends Activity {
    public static final String PMP_PERMISSION_DID_CHANGED = "PMP_PERMISSION_DID_CHANGED";
    public static boolean isRequestingPermission = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkPermissions();
    }

    private void checkPermissions() {
        if (!GPSLocationManager.checkIfLocationPermissionAllowed(this)) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        GPSLocationManager.PMP_PERMISSIONS_REQUEST_LOCATION);
            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        GPSLocationManager.PMP_PERMISSIONS_REQUEST_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            Intent intent = new Intent(PMP_PERMISSION_DID_CHANGED);
            intent.putExtra(PMP_PERMISSION_DID_CHANGED, true);
            this.getApplicationContext().sendBroadcast(intent);
            isRequestingPermission = false;
            finish();
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PMP_PERMISSIONS_REQUEST_LOCATION) {
            boolean granted = false;
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                granted = true;
            }
            isRequestingPermission = false;
            Intent intent = new Intent(PMP_PERMISSION_DID_CHANGED);
            intent.putExtra(PMP_PERMISSION_DID_CHANGED, granted);
            this.getApplicationContext().sendBroadcast(intent);
            finish();
        }
    }
}
