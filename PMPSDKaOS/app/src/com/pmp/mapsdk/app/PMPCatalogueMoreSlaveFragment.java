package com.pmp.mapsdk.app;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.cms.model.Pois;
import com.pmp.mapsdk.utils.PMPUtil;

import java.util.ArrayList;

/**
 * Created by joeyyc on 1/12/2016.
 */

public class PMPCatalogueMoreSlaveFragment extends Fragment {
    public static final String ARGS_BY_AREA = "ARGS_BY_AREA";
    public static final String ARGS_POI_ARRAY = "ARGS_POI_ARRAY";
    private static final int OptionIndexAllArea = 0;
    private static final int OptionIndexRestrictedArea = 1;
    private static final int OptionIndexNonRestrictedArea = 2;

    private ListView listView;
    private ArrayList<Pois> pois;
    private ArrayList<Pois> filterPois;
    private Boolean _filteringActive;
    private Boolean _selectionExpanded = false;
    private Boolean _byArea;
    private int currentAreaType = OptionIndexAllArea;

    private CatalogAdapter catalogAdapter = new CatalogAdapter();
    private AreaAdapter areaAdapter = new AreaAdapter();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            _byArea = bundle.getBoolean(ARGS_BY_AREA, true);
            _filteringActive = _byArea;
            pois = bundle.getParcelableArrayList(ARGS_POI_ARRAY);
            filterPois = new ArrayList<Pois>();
            filterPois.addAll(pois);

            if (pois == null) {
                pois = new ArrayList<Pois>();
            }
        }

        View view = (View) inflater.inflate(R.layout.pmp_search_list_component, container, false);

        listView = (ListView) view.findViewById(R.id.listview);
        listView.setBackgroundColor(getResources().getColor(R.color.Search_Landing_Grid_Background));

        if (_byArea) {
            listView.setAdapter(areaAdapter);
        } else {
            listView.setAdapter(catalogAdapter);
        }

        return view;
    }

    private void expandOptions() {
        _selectionExpanded = true;
        areaAdapter.notifyDataSetInvalidated();
    }

    private void selectOption(int index) {
        if (currentAreaType != index) {
            currentAreaType = index;
            filterPois.clear();
            switch (currentAreaType) {
                case OptionIndexAllArea:
                    filterPois.addAll(pois);
                    break;
                case OptionIndexRestrictedArea:
                    for (Pois poi : pois) {
                        if (poi.getRestricted()) {
                            filterPois.add(poi);
                        }
                    }
                    break;
                case OptionIndexNonRestrictedArea:
                    for (Pois poi : pois) {
                        if (!poi.getRestricted()) {
                            filterPois.add(poi);
                        }
                    }
                    break;
            }
        }
        _selectionExpanded = false;
        areaAdapter.notifyDataSetInvalidated();
    }

    private void selectPoi(int index) {
        if (pois != null && index < pois.size()) {
            Pois poi = pois.get(index);
            ArrayList<Pois> poiList = new ArrayList<Pois>();
            poiList.add(poi);

            PMPMapFragment mapFragment = (PMPMapFragment) getParentFragment().getParentFragment();
            mapFragment.showPOIListOnMap(poiList, null, false, false, true, true);
            for (int i = 0; i < getParentFragment().getFragmentManager().getBackStackEntryCount(); i++) {
                getParentFragment().getFragmentManager().popBackStack();
            }
        }
    }

    private class AreaAdapter extends BaseAdapter {
        public AreaAdapter() {
        }

        @Override
        public String getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {

            if (_selectionExpanded && position < 3) {
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_area_restriction, null);
                convertView.setTag(position);
                TextView tvOptionName = (TextView) convertView.findViewById(R.id.tvName);
                switch (position) {
                    case OptionIndexAllArea:
                        tvOptionName.setText(getResources().getText(R.string.PMPMAP_SEARCH_OPTION_ALL_AREA));
                        break;
                    case OptionIndexRestrictedArea:
                        tvOptionName.setText(getResources().getText(R.string.PMPMAP_SEARCH_OPTION_RESTRICTED_AREA));
                        break;
                    case OptionIndexNonRestrictedArea:
                        tvOptionName.setText(getResources().getText(R.string.PMPMAP_SEARCH_OPTION_NON_RESTRICTED_AREA));
                        break;
                }
                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectOption((int)v.getTag());
                    }
                });
                return convertView;
            } else if (position == 0) {
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_area_restriction, null);
                TextView tvOptionName = (TextView) convertView.findViewById(R.id.tvName);
                switch (currentAreaType) {
                    case OptionIndexAllArea:
                        tvOptionName.setText(getResources().getText(R.string.PMPMAP_SEARCH_OPTION_ALL_AREA));
                        break;
                    case OptionIndexRestrictedArea:
                        tvOptionName.setText(getResources().getText(R.string.PMPMAP_SEARCH_OPTION_RESTRICTED_AREA));
                        break;
                    case OptionIndexNonRestrictedArea:
                        tvOptionName.setText(getResources().getText(R.string.PMPMAP_SEARCH_OPTION_NON_RESTRICTED_AREA));
                        break;
                }
                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        expandOptions();
                    }
                });
                return convertView;
            }

            convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_catalogue_poi, null);
            convertView.setTag(position - (_selectionExpanded?3:1));
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectPoi((int)v.getTag());
                }
            });
            Pois poi;
            poi = pois.get(position - (_selectionExpanded?3:1));
            TextView tv_name = (TextView)convertView.findViewById(R.id.tvTitle);
            tv_name.setText(PMPUtil.getLocalizedString(poi.getName()));
            ImageView iv = (ImageView)convertView.findViewById(R.id.iv_Poi);
            iv.setImageDrawable(null);
            PMPUtil.setImageWithURL(iv, poi.getImage());
            TextView tv_location = (TextView)convertView.findViewById(R.id.tvLocation);
            tv_location.setText(PMPUtil.getLocalizedAddress(getActivity(), poi));
            TextView tv_OpenHrs = (TextView)convertView.findViewById(R.id.tvOpenHrs);
            tv_OpenHrs.setText(PMPUtil.getLocalizedString(poi.getBusinessHours()));
            return convertView;
        }

        @Override
        public int getCount() {
            return pois.size() + (_selectionExpanded?3:1);
        }
    }

    private class CatalogAdapter extends BaseAdapter {
        public CatalogAdapter() {
        }

        @Override
        public String getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            if(convertView == null){
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.pmp_cell_catalogue_poi, null);
            }

            convertView.setTag(position);

            Pois poi;
            poi = pois.get(position);
            TextView tv_name = (TextView)convertView.findViewById(R.id.tvTitle);
            tv_name.setText(PMPUtil.getLocalizedString(poi.getName()));
            ImageView iv = (ImageView)convertView.findViewById(R.id.iv_Poi);
            iv.setImageDrawable(null);
            PMPUtil.setImageWithURL(iv, poi.getImage());
            TextView tv_location = (TextView)convertView.findViewById(R.id.tvLocation);
            tv_location.setText(PMPUtil.getLocalizedAddress(getActivity(), poi));
            TextView tv_OpenHrs = (TextView)convertView.findViewById(R.id.tvOpenHrs);
            tv_OpenHrs.setText(PMPUtil.getLocalizedString(poi.getBusinessHours()));

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectPoi((int)v.getTag());
                }
            });

            return convertView;
        }

        @Override
        public int getCount() {
            return pois.size();
        }
    }

}
