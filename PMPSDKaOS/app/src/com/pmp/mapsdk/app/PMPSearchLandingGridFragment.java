package com.pmp.mapsdk.app;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.cherrypicks.pmpmapsdk.R;

/**
 * Created by joeyyc on 28/11/2016.
 */

public class PMPSearchLandingGridFragment extends Fragment {
    public static final int LandingGridViewTypeLocation = 0;
    public static final int LandingGridViewTypeCategory = 1;
    public static final String ARGSLandingGridType = "ARGSLandingGridType";

    private int type;
    private TextView textView;
    private Button button;
    private GridView gridView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        type = bundle.getInt(PMPSearchLandingGridFragment.ARGSLandingGridType);

        View view = inflater.inflate(R.layout.pmp_search_landing_list_grid, container, false);
        textView = (TextView)view.findViewById(R.id.textView);
        button = (Button)view.findViewById(R.id.button);
//        gridView = (GridView)view.findViewById(R.id.gridView);
//
//        gridView.setAdapter(new GridAdapter());

        return view;
    }


    public class GridAdapter extends BaseAdapter {
        private final String[] mobileValues = {"item 1", "item 2"};

        public GridAdapter() {
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) getActivity()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View gridView;

            if (convertView == null) {

                gridView = new View(getActivity());

                // get layout from mobile.xml
                gridView = inflater.inflate(R.layout.pmp_search_landing_list_grid_cell, null);

                // set value into textview
                TextView textView = (TextView) gridView
                        .findViewById(R.id.textView);
                textView.setText(mobileValues[position]);

                // set image based on selected text
                ImageView imageView = (ImageView) gridView
                        .findViewById(R.id.imageView);

            } else {
                gridView = (View) convertView;
            }

            return gridView;
        }

        @Override
        public int getCount() {
            return mobileValues.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

    }
}
