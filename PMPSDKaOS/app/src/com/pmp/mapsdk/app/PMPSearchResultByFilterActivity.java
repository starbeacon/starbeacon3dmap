package com.pmp.mapsdk.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cherrypicks.pmpmap.analytics.AnalyticsLogger;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.cms.model.Pois;
import com.pmp.mapsdk.utils.PMPUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by andrewman on 7/22/16.
 */
public class PMPSearchResultByFilterActivity extends FragmentActivity {

    public static String POI_IDS = "POI_IDS";
    public static String HEADER = "Header";
    private ListView listView;
    private ImageButton backButton;
    private TextView headerTextView;
    private List<Pois> pois;
    private SearchResultAdapter searchResultAdapter = new SearchResultAdapter();
    private RelativeLayout fragment_container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pmp_search_result_by_filter);
//        backButton = findViewById(R.id.btn_back);
        backButton = (ImageButton)findViewById(R.id.btn_back);
        headerTextView = (TextView)findViewById(R.id.tv_header);
        listView = (ListView)findViewById(R.id.listview);
        fragment_container = (RelativeLayout)findViewById(R.id.fragment_container);

        int poiIds[] = getIntent().getIntArrayExtra(POI_IDS);
        PMPServerManager serverManager = PMPServerManager.getShared(this);

        if (poiIds == null) {
            Log.d("", "Empty POI Ids");
        }else {
            ArrayList<Pois> arr = new ArrayList<Pois>();
            for (Pois p : serverManager.getServerResponse().getPois()) {
                for (int poiId : poiIds) {
                    if (((int)p.getId()) == poiId) {
                        arr.add(p);
                        break;
                    }
                }
            }
            pois = arr;
        }

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        headerTextView.setText(getIntent().getStringExtra(HEADER));

        listView.setAdapter(searchResultAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                onSelectPOI(searchResultAdapter.getItem(i));

            }
        });
    }

    private void onSelectPOI(Pois poi) {
//        Intent data = new Intent();
//        data.putExtra(PMPSearchActivity.POI_IDS, (int) poi.getId());
//        setResult(Activity.RESULT_OK, data);
//        finish();
//
//        HashMap<String, Object> param = new HashMap<>();
//        param.put("poiId", (int) poi.getId());
//        param.put("catId", pois.get(0).getPoiCategoryIds().get(0));
//
////        finishActivity(PMPSearchActivity.REQUEST_CODE);

        //listView.setVisibility(View.GONE);

        PMPPOIDetailFragment fragment = new PMPPOIDetailFragment();

        Bundle args = new Bundle();
        args.putString(PMPPOIDetailFragment.POI_ID, poi.getExternalId());
        fragment.setArguments(args);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, fragment)
                .addToBackStack("")
                .commit();

    }

    private class SearchResultAdapter extends BaseAdapter {
        public SearchResultAdapter() {
        }

        @Override
        public Pois getItem(int i) {
            return pois.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            if(convertView == null){
                convertView = LayoutInflater.from(PMPSearchResultByFilterActivity.this).inflate(R.layout.pmp_cell_poi, null);
            }

            Pois poi = getItem(position);
            TextView tv_name = (TextView)convertView.findViewById(R.id.tv_name);
            tv_name.setText(PMPUtil.getLocalizedString(poi.getName()));
            TextView tv_address = (TextView) convertView.findViewById(R.id.tv_address);
            tv_address.setText(PMPUtil.getLocalizedAddress(getApplicationContext(), poi));
            ImageView iv = (ImageView)convertView.findViewById(R.id.iv_icon);
            Log.d("Search", "IMG URL: " + poi.getImage() + "["+ position+ "]");
            iv.setImageDrawable(null);
            PMPUtil.setImageWithURL(iv, poi.getImage());

            return convertView;
        }

        @Override
        public int getCount() {
            return pois.size();
        }
    }

}
