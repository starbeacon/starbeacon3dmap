package com.pmp.mapsdk.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.cherrypicks.pmpmap.PMPDataManager;
import com.cherrypicks.pmpmap.analytics.AnalyticsLogger;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.cms.model.Name;
import com.pmp.mapsdk.cms.model.PoiCategories;
import com.pmp.mapsdk.cms.model.Pois;
import com.pmp.mapsdk.utils.PMPUtil;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;


/**
 * Created by andrewman on 7/22/16.
 */
public class PMPSearchActivity extends Activity {

    public static int REQUEST_CODE = 111;
    public static String SELECTED_POI_ID = "POI_IDS";

    private EditText searchText;
    private ImageButton closeButton;
    private ListView listView;
    private List<PoiCategories> poiCategories;
    private Map<Integer, List<Pois>> poisMappedByCat;
    private POICategoriesAdapter poiCategoriesAdapter = new POICategoriesAdapter();
    private SearchResultAdapter searchResultAdapter = new SearchResultAdapter();
    private List<Pois> searchResultPOIs;
    private LinearLayout recentSearchHolder;

    private Vector<String> recentSearchKeywords;
    private String recentSearchFileName = "RecentSearch.bin";
//    private SharedPreferences recentSearchPerferences;
//    private String preferenceName = "RecentSearch";
//    private String recentSearchKey = "Searches";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pmp_search_activity);
        searchText = (EditText)findViewById(R.id.edit_search);
        closeButton = (ImageButton)findViewById(R.id.btn_close);
        listView = (ListView)findViewById(R.id.listview);
        recentSearchHolder = (LinearLayout)findViewById(R.id.recent_search_holder);

        final PMPServerManager serverManager = PMPServerManager.getShared(this);
        poisMappedByCat = new HashMap<Integer, List<Pois>>();
        if (serverManager.getServerResponse() != null) {
            poiCategories = serverManager.getServerResponse().getPoiCategories();

            for (Pois poi : serverManager.getServerResponse().getPois()) {
                for (int catId : poi.getPoiCategoryIds()) {
                    List<Pois> arr = poisMappedByCat.get(catId);
                    if (arr == null) {
                        arr = new ArrayList<Pois>();
                        poisMappedByCat.put(catId, arr);
                    }
                    arr.add(poi);
                }
            }

            listView.setAdapter(poiCategoriesAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    if (adapterView.getAdapter() == poiCategoriesAdapter) {
                        onSelectPOICategory(poiCategoriesAdapter.getItem(i));
                    }else if (adapterView.getAdapter() == searchResultAdapter) {
                        onSelectPOI(searchResultAdapter.getItem(i));
                    }
                }
            });
        }

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0) {
                    listView.setAdapter(poiCategoriesAdapter);
                    recentSearchHolder.setVisibility(View.VISIBLE);
                }else {
                    recentSearchHolder.setVisibility(View.GONE);
                    ArrayList<Pois> pois = new ArrayList<Pois>();
                    if (serverManager.getServerResponse() != null) {
                        for (Pois poi : serverManager.getServerResponse().getPois()) {
                            for (Name name : poi.getName()) {
                                if (name.getContent().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                                    pois.add(poi);
                                    break;
                                }
                            }
                        }
                    }
                    searchResultPOIs = pois;
                    listView.setAdapter(searchResultAdapter);
                    searchResultAdapter.notifyDataSetInvalidated();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        recentSearchKeywords = new Vector<String>();
    }

    private void onSelectPOICategory(PoiCategories category) {
        List<Pois> pois = poisMappedByCat.get((int)category.getId());
        int poiIds[] = new int[pois.size()];
        int i = 0;
        for (Pois poi : pois) {
//            poiIds.add((int)poi.getId());
            poiIds[i] = (int)poi.getId();
            i++;
        }
        Intent intent = new Intent(this, PMPSearchResultByFilterActivity.class);
        intent.putExtra(PMPSearchResultByFilterActivity.POI_IDS, poiIds);
        intent.putExtra(PMPSearchResultByFilterActivity.HEADER, PMPUtil.getLocalizedString(category.getName()));
        startActivityForResult(intent, REQUEST_CODE);
    }

    private void onSelectPOI(Pois poi) {

        Intent data = new Intent();
        data.putExtra(SELECTED_POI_ID, (int) poi.getId());
        setResult(Activity.RESULT_OK, data);
        finish();

        if (searchText.length() != 0) {
            String keyword = searchText.getText().toString();
            if (recentSearchKeywords.contains(keyword)) {
                recentSearchKeywords.remove(keyword);
            }
            recentSearchKeywords.insertElementAt(keyword, 0);

            while (recentSearchKeywords.size() > PMPDataManager.getSharedPMPManager(this).getResponseData().getMapNumRecentSearch()) {
                recentSearchKeywords.remove(recentSearchKeywords.size() - 1);
            }
            FileOutputStream fos = null;
            ObjectOutputStream oos = null;
            try {
                fos = openFileOutput(recentSearchFileName, MODE_PRIVATE);
                oos = new ObjectOutputStream(fos);
                oos.writeObject(recentSearchKeywords);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                if (oos != null) {
                    try {
                        oos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            setResult(resultCode, data);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = openFileInput(recentSearchFileName);
            ois = new ObjectInputStream(fis);
            recentSearchKeywords = (Vector<String>) ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
            recentSearchHolder.removeAllViews();
            if (recentSearchKeywords != null && recentSearchKeywords.size() != 0) {
                for (final String keyword : recentSearchKeywords) {
                    View convertView = LayoutInflater.from(PMPSearchActivity.this).inflate(R.layout.pmp_cell_search, null);
                    TextView tv_name = (TextView) convertView.findViewById(R.id.tv_name);
                    tv_name.setText(keyword);
                    ImageView iv = (ImageView)convertView.findViewById(R.id.iv_icon);
                    iv.setImageResource(R.drawable.search_icon_history);
                    recentSearchHolder.addView(convertView);

                    convertView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            searchText.setText(keyword);
                        }
                    });
                }
            }
        }
    }

    private class POICategoriesAdapter extends BaseAdapter {
        public POICategoriesAdapter() {
        }

        @Override
        public PoiCategories getItem(int i) {
            return poiCategories.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            if(convertView == null){
                convertView = LayoutInflater.from(PMPSearchActivity.this).inflate(R.layout.pmp_cell_poi_cat, null);
            }

            PoiCategories poiCat = getItem(position);
            TextView tv_name = (TextView)convertView.findViewById(R.id.tv_name);
            tv_name.setText(PMPUtil.getLocalizedString(poiCat.getName()));
            ImageView iv = (ImageView)convertView.findViewById(R.id.iv_icon);
            PMPUtil.setImageWithURL(iv, poiCat.getImage());
            return convertView;
        }

        @Override
        public int getCount() {
            return poiCategories.size();
        }
    }

    private class SearchResultAdapter extends BaseAdapter {
        @Override
        public Pois getItem(int i) {
            return searchResultPOIs.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            if(convertView == null){
                convertView = LayoutInflater.from(PMPSearchActivity.this).inflate(R.layout.pmp_cell_search, null);
            }

            Pois poi = getItem(position);
            TextView tv_name = (TextView)convertView.findViewById(R.id.tv_name);
            String title = PMPUtil.getLocalizedString(poi.getName()).toLowerCase();
            String searchTitle = searchText.getText().toString().toLowerCase();
            SpannableString titleToSpan = new SpannableString(PMPUtil.getLocalizedString(poi.getName()));
            int index = title.indexOf(searchTitle);
            if (index != -1) {
                titleToSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.nav_arrived_notcoverd_view_color)), index, index+searchTitle.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            tv_name.setText(titleToSpan);

            ImageView iv_icon = (ImageView)convertView.findViewById(R.id.iv_icon);

            ArrayList<PoiCategories> poiCats = PMPServerManager.getShared(PMPSearchActivity.this).getServerResponse().getPoiCategories();
            for (PoiCategories cat : poiCats){
                if(poi.getPoiCategoryIds().size() > 0 && (int)cat.getId() == poi.getPoiCategoryIds().get(0)){
                    PMPUtil.setImageWithURL(iv_icon, cat.getImage());
                    break;
                }
            }

            return convertView;
        }

        @Override
        public int getCount() {
            return searchResultPOIs.size();
        }
    }

}
