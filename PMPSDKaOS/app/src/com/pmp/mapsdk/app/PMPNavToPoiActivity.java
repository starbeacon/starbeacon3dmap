/****************************************************************************
Copyright (c) 2015 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package com.pmp.mapsdk.app;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.external.PMPMapSDK;

public class PMPNavToPoiActivity extends Activity {
    private final String TAG = "PMPNavToPoiActivity";
    public static final String DES_NAME = "DES_NAME";
    public static final String TO_FLOOR = "TO_FLOOR";
    public static final String FROM_FLOOR = "FROM_FLOOR";
    public static final String TOTAL_DST = "TOTAL_DST";
    public static final String TOTAL_TIME = "TOTAL_TIME";

    //UI Variable
    private TextView tv_name, tv_from_floor, tv_to_floor, tv_total_distance, tv_total_time;
    private Button btn_start, btn_close;

    //Locale Variable
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pmp_activity_nav_poi_view);

        tv_name = (TextView)findViewById(R.id.tv_name);
//        tv_from_floor = (TextView)findViewById(R.id.tv_from_floor);
//        tv_to_floor = (TextView)findViewById(R.id.tv_to_floor);
        tv_total_distance = (TextView)findViewById(R.id.tv_total_distance);
        tv_total_distance.setTextColor(PMPMapSDK.getMapUISetting().getThemeColor());
        tv_total_time = (TextView)findViewById(R.id.tv_total_time);
        btn_start = (Button)findViewById(R.id.btn_start);
        btn_close = (Button)findViewById(R.id.btn_close);

        String des_name = getIntent().getStringExtra(DES_NAME);
        String to_floor = getIntent().getStringExtra(TO_FLOOR);
        String from_floor = getIntent().getStringExtra(FROM_FLOOR);
        String total_dst = getIntent().getStringExtra(TOTAL_DST);
        String total_time = getIntent().getStringExtra(TOTAL_TIME);

        tv_name.setText(des_name);
        tv_from_floor.setText(from_floor);
        tv_to_floor.setText(to_floor);
        tv_total_distance.setText(total_dst);
        tv_total_time.setText(total_time);
        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                finish();
            }
        });

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);


    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }



}
