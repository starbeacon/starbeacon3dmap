package com.pmp.mapsdk.location;

import android.location.Location;

import com.cherrypicks.pmpmap.core.CoreEngine;
import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.external.PMPMapSDK;

/**
 * Created by alanchan on 29/8/2017.
 */

public class GPSPositioningManager {
    private int mapWidth, mapHeight;
    private double centerLat, centerLng, rotatedAngle, topLeftLat, topLeftLng, bottomRightLat, bottomRightLng;

    public static class LatLngHolder{
        public double latitude;
        public double longitude;

        public LatLngHolder(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }
    }

    private static GPSPositioningManager gpsPositioningManager;
    public static GPSPositioningManager shared(){
        if(gpsPositioningManager == null){
            gpsPositioningManager = new GPSPositioningManager();
        }
        return gpsPositioningManager;
    }

    public void setUp(LatLngHolder topLeft, LatLngHolder topRight, LatLngHolder bottomLeft, LatLngHolder bottomRight, int mapWidth, int mapHeight) {
        this.mapWidth = mapWidth;
        this.mapHeight = mapHeight;

        double minLat = Math.min(topLeft.latitude, Math.min(topRight.latitude, Math.min(bottomLeft.latitude, bottomRight.latitude)));
        double minLng = Math.min(topLeft.longitude, Math.min(topRight.longitude, Math.min(bottomLeft.longitude, bottomRight.longitude)));
        double maxLat = Math.max(topLeft.latitude, Math.max(topRight.latitude, Math.max(bottomLeft.latitude, bottomRight.latitude)));
        double maxLng = Math.max(topLeft.longitude, Math.max(topRight.longitude, Math.max(bottomLeft.longitude, bottomRight.longitude)));

        centerLat = (maxLat - minLat) / 2 + minLat;
        centerLng = (maxLng - minLng) / 2 + minLng;

        rotatedAngle = Math.toRadians(PMPServerManager.getShared().getServerResponse().getMapDirection());

        topLeftLat = rotateLatitude(topLeft.latitude, topLeft.longitude, rotatedAngle, centerLat, centerLng);
        topLeftLng = rotateLongitude(topLeft.latitude, topLeft.longitude, rotatedAngle, centerLat, centerLng);

        bottomRightLat = rotateLatitude(bottomRight.latitude, bottomRight.longitude, rotatedAngle, centerLat, centerLng);
        bottomRightLng = rotateLongitude(bottomRight.latitude, bottomRight.longitude, rotatedAngle, centerLat, centerLng);

    }

    public void converToXY(Location location) {
        if(!PMPMapSDK.getConfiguration().isShouldEnableGPSPositioning()){
            return;
        }

        double rotatedLatitude = rotateLatitude(location.getLatitude(), location.getLongitude(), rotatedAngle, centerLat, centerLng);
        double rotatedLongitude = rotateLongitude(location.getLatitude(), location.getLongitude(), rotatedAngle, centerLat, centerLng);

        /////////Method 1
//    double diffLat = bottomRightLat - topLeftLat;
//    double diffLng = bottomRightLng - topLeftLng;
//    double realWidth = sqrt(diffLat * diffLat + diffLng * diffLng);
//
//    double ratioLng = fabs(_mapWidth / diffLng);
//    double ratioLat = fabs(_mapHeight / diffLat);
//
//
//        minLat = MIN(topLeftLat, bottomRightLat);
//        maxLat = MAX(topLeftLat, bottomRightLat);
//        minLng = MIN(topLeftLng, bottomRightLng);
//        maxLng = MAX(topLeftLng, bottomRightLng);
////    double widthBeforeRotate = (maxLng - minLng) * ratio;
////    double heightBeforeRotate = (maxLat - minLat) * ratio;
//
//    double xBeforeRotate = ratioLng * (rotatedLongitude - minLng);
//    double yBeforeRotate = _mapHeight - (ratioLat * (rotatedLatitude - minLat));
        ///////////
        /////////Method 2
        double minLat = Math.min(topLeftLat, bottomRightLat);
        double maxLat = Math.max(topLeftLat, bottomRightLat);
        double minLng = Math.min(topLeftLng, bottomRightLng);
        double maxLng = Math.max(topLeftLng, bottomRightLng);
        double yBeforeRotate = mapHeight - (((rotatedLatitude - minLat) / (maxLat - minLat)) * (mapHeight - 1));
        double xBeforeRotate = ((rotatedLongitude - minLng) / (maxLng - minLng)) * (mapWidth - 1);
        ////////////

        if(xBeforeRotate >= 0 && xBeforeRotate <= mapWidth &&
                yBeforeRotate >= 0 && yBeforeRotate <= mapHeight){
            CoreEngine.getInstance().onGPSIndoorLocationUpdate(xBeforeRotate, yBeforeRotate, location.getAccuracy());
        }

    }

    private double rotateLatitude(double lat, double lon, double angle, double centerLat, double centerLon) {
        double n_lat = centerLat + (Math.cos(angle) * (lat - centerLat) - Math.sin(angle) * (lon - centerLon));
        return n_lat;
    }

    private double rotateLongitude(double lat, double lon, double angle, double centerLat, double centerLon) {
        double n_lon = centerLon + (Math.sin(angle) * (lat - centerLat) + Math.cos(angle) * (lon - centerLon));
        return n_lon;
    }
}
