package com.pmp.mapsdk.location;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.pinpoint.PinpointConfiguration;
import com.amazonaws.mobileconnectors.pinpoint.PinpointManager;
import com.amazonaws.regions.Regions;
import com.cherrypicks.pmpmap.PMPDataManager;
import com.cherrypicks.pmpmap.PMPMapController;
import com.cherrypicks.pmpmap.analytics.AnalyticsHelper;
import com.cherrypicks.pmpmap.analytics.AnalyticsLogger;
import com.cherrypicks.pmpmap.analytics.aws.AWSConfiguration;
import com.cherrypicks.pmpmap.analytics.aws.AWSMobileClient;
import com.cherrypicks.pmpmap.core.CoreEngine;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.cms.PMPProximityServerManager;
import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.cms.model.AaZones;
import com.pmp.mapsdk.cms.model.Areas;
import com.pmp.mapsdk.cms.model.Devices;
import com.pmp.mapsdk.cms.model.PMPProximityServerManagerNotifier;
import com.pmp.mapsdk.cms.model.Pois;
import com.pmp.mapsdk.cms.model.PromotionMessage;
import com.pmp.mapsdk.cms.model.Promotions;
import com.pmp.mapsdk.cms.model.ResponseData;
import com.pmp.mapsdk.cms.model.Result;
import com.pmp.mapsdk.cms.model.SaveUserFlightResponse;
import com.pmp.mapsdk.cms.model.Zones;
import com.pmp.mapsdk.external.PMPMapSDK;
import com.pmp.mapsdk.external.ProximityMessage;
import com.pmp.mapsdk.logging.LogManager;
import com.pmp.mapsdk.utils.PMPBackgroundService;
import com.pmp.mapsdk.utils.PMPUtil;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.pmp.mapsdk.cms.model.Promotions.PMPPromotionMessageType_AELnotOperating;
import static com.pmp.mapsdk.cms.model.Promotions.PMPPromotionMessageType_LastAELNotification;
import static com.pmp.mapsdk.cms.model.Promotions.PMPPromotionMessageType_NONE;
import static com.pmp.mapsdk.cms.model.Promotions.PMPPromotionMessageType_WelcomeMesaage;
import static com.pmp.mapsdk.cms.model.Promotions.PMPPromotionMessageType_WelcomeMesaageSeaToAir;
import static com.pmp.mapsdk.cms.model.Promotions.PMPPromotionMessageType_WelcomeMessageArrival;
import static com.pmp.mapsdk.cms.model.Promotions.PMPPromotionMessageType_WelcomeMessageTransfer;
import static com.pmp.mapsdk.cms.model.Promotions.PROMOTION_TYPE_DEVICE;
import static com.pmp.mapsdk.cms.model.Promotions.PROMOTION_TYPE_ZONAL;
import static com.pmp.mapsdk.location.PMPBackgroundStatus.Background;
import static com.pmp.mapsdk.utils.PMPUtil.getDate;

/**
 * Created by Yu on 22/2/16.
 */

public class PMPApplication implements
        Application.ActivityLifecycleCallbacks {
    private final static String TAG = PMPApplication.class.getSimpleName();
    private final static String LAST_SEND_DATA_TIME_MILLIS = "lastSendDataTimeMillis";
    public final static String externalIDForAEL = "AirportExpress_1254";

    private final static int CONTINUE_NOTIFICATATION_ID = 777;
    private static PMPApplication pmpApplication;
    private static Application application;
    private boolean isFirstStarted = true;
    private boolean enableKillAppDetection = true;
    private boolean ENABLE_PROXIMITY_LOGGING = true;

    private PMPBackgroundDetectionNotifier backgroundDetectionNotifier;
    private PMPBackgroundStatus bgStatus;
    private Activity currentActivity;
    private Promotions currentPromotion;
    private int currentProximityID;
    private int currentProximityMinor;
    private int currentDeviceGroupId;
    private double currentProximityRssi;
    private double currentProximityDistance;


    private int batteryLevel;
    private PinpointManager pinpointManager;

    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context ctxt, Intent intent) {
            batteryLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
        }
    };

    private PMPAppLifeCycle appCycleAdapter = new PMPAppLifeCycle() {
        private Timer backgroundTimer;
        private int appActivitiesCount = 0;

        @Override
        public void onCreate() {
            appActivitiesCount = 0;
            onBackground(0);
        }

        @Override
        public void onResume() {
            appActivitiesCount++;
            stopBGTimer();
            onBackground(1);
        }

        @Override
        public void onPause() {
            appActivitiesCount--;
            startBGTimer();
        }

        private void startBGTimer() {
            stopBGTimer();
            backgroundTimer = new Timer();
            backgroundTimer.schedule(new TimerTask() {

                @Override
                public void run() {
                    if (appActivitiesCount == 0) {
                        onBackground(0);
                    }
                }
            }, 1000);
        }

        private void stopBGTimer() {
            if (backgroundTimer != null) {
                backgroundTimer.cancel();
                backgroundTimer.purge();
                backgroundTimer = null;
            }
        }

        private void onBackground(int background) {
            Message msg = new Message();
            msg.what = background;
            Handler handler = new Handler(Looper.getMainLooper()) {
                @Override
                public void handleMessage(Message inputMessage) {
                    if (inputMessage.what == 0) {
                        bgStatus = PMPBackgroundStatus.Background;
                        LogManager.e(TAG, "Enter Background");
						/* Only do this logic if engine started */

                        if (getBackgroundDetectionNotifier() != null) {
                            getBackgroundDetectionNotifier().onDidEnterBackground();
                        }
                    } else {
                        bgStatus = PMPBackgroundStatus.Foreground;
                        LogManager.e(TAG, "Enter Foreground");
						/* Only do this logic if engine started */

                        if (getBackgroundDetectionNotifier() != null) {
                            getBackgroundDetectionNotifier().onDidEnterForeground();
                        }
                    }
                }
            };
            handler.sendMessage(msg);
        }
    };
    private Intent backgroundService;

    public void onCreate(Application application) {
        LogManager.e(TAG, "onCreate");
        PMPApplication.application = application;
        PMPApplication.pmpApplication = this;
        if (android.os.Build.VERSION.SDK_INT >= 14) {
            application.registerActivityLifecycleCallbacks(this);
            appCycleAdapter.onCreate();
        }

        if (this.isEnableKillAppDetection()) {
            PMPLocationManager.getShared(application).start();
        }

        AWSMobileClient.initializeMobileClientIfNecessary(application);
        CognitoCachingCredentialsProvider cognitoCachingCredentialsProvider = new CognitoCachingCredentialsProvider(application,"ap-northeast-1:3d3c64ea-678e-4a92-93de-8fc68fc34838",Regions.AP_NORTHEAST_1);

        PinpointConfiguration config = new PinpointConfiguration(application, AWSConfiguration.AMAZON_MOBILE_ANALYTICS_APP_ID, Regions.US_EAST_1, cognitoCachingCredentialsProvider);

        this.pinpointManager = new PinpointManager(config);

        AWSMobileClient.defaultMobileClient();

        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                application.getApplicationContext(),    /* get the context for the application */
                "ap-northeast-1:3d3c64ea-678e-4a92-93de-8fc68fc34838",    /* Identity Pool ID */
                Regions.AP_NORTHEAST_1           /* Region for your identity pool--US_EAST_1 or EU_WEST_1*/
        );

        LocalBroadcastManager.getInstance(application).registerReceiver(this.mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        //testing only!!!!
//        runAndWait();
    }

    private void runAndWait(){
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(PMPServerManager.getShared().getServerResponse() != null){
                    for(Promotions promotion : PMPServerManager.getShared().getServerResponse().getPromotions()){
                        if(promotion.getId() == 4){
                            showProximityMessage(100, promotion, false);
//                            onShopNearByPromoMessage(1);
                        }
//                        showProximityMessage(100, promotion, false);
                    }
                }else{
                    runAndWait();
                }

            }
        },10000);
    }


    @Override
    public void onActivityCreated(Activity activity, Bundle arg1) {
        LogManager.e(TAG, "onActivityCreated");
//        if(isFirstStarted) {
//            isFirstStarted = false;
//            PMPLocationManager.getShared(application).stop();
//        }
    }

    @Override
    public void onActivityStarted(Activity activity) {
        LogManager.d(TAG, "onActivityStarted");
        currentActivity = activity;
    }

    @Override
    public void onActivityResumed(Activity activity) {
        LogManager.d(TAG, "onActivityResumed");
        appCycleAdapter.onResume();
    }

    @Override
    public void onActivityPaused(Activity activity) {
        LogManager.d(TAG, "onActivityPaused");
        appCycleAdapter.onPause();
    }

    @Override
    public void onActivityStopped(Activity activity) {
        LogManager.d(TAG, "onActivityStopped");
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle arg1) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        if (currentActivity == activity) {
            currentActivity = null;
        }
    }

    public boolean isEnableKillAppDetection() {
        SharedPreferences _userDefaults = application.getSharedPreferences("PREF_PMP_LOCATION_SETTINGS", 0);
        return _userDefaults.getBoolean(application.getString(R.string.Preference_EnableKillAppDetection), true);
    }

    public void setEnableKillAppDetection(boolean enableKillAppDetection) {
        this.enableKillAppDetection = enableKillAppDetection;
        SharedPreferences _userDefaults = application.getSharedPreferences("PREF_PMP_LOCATION_SETTINGS", 0);
        _userDefaults.edit().putBoolean(application.getString(R.string.Preference_EnableKillAppDetection), enableKillAppDetection);
        if (enableKillAppDetection) {
            backgroundService = new Intent(application.getBaseContext(),
                    PMPBackgroundService.class);
            application.startService(backgroundService);
        } else {
            if (backgroundService != null) {
                application.stopService(backgroundService);
                backgroundService = null;
            }
        }
    }

    public PMPBackgroundDetectionNotifier getBackgroundDetectionNotifier() {
        return backgroundDetectionNotifier;
    }

    public void setBackgroundDetectionNotifier(PMPBackgroundDetectionNotifier backgroundDetectionNotifier) {
        this.backgroundDetectionNotifier = backgroundDetectionNotifier;
    }

    public PMPBackgroundStatus getBgStatus() {
        return bgStatus;
    }

    public Promotions getCurrentPromotion() {
        return currentPromotion;
    }

    public void notifyProximityEnterRegionCallback(final int proximityId, int minor, double rssi, double distance, final boolean isBackground) {
        Log.d(TAG, "notifyProximityEnterRegionCallback:" + proximityId + " background:"+ isBackground);
        currentProximityID = proximityId;
        currentProximityMinor = minor;
        currentProximityRssi = rssi;
        currentProximityDistance = distance;
        ResponseData response = PMPDataManager.getSharedPMPManager(null).getResponseData();
        Devices device = CollectionUtils.find(response.getDevices(), new Predicate<Devices>() {
            @Override
            public boolean evaluate(Devices object) {
                return object.getMajorNo() == proximityId;
            }
        });
        if(device != null){
            currentDeviceGroupId = (int)device.getDeviceGroupId();
        }

        //logging
        onProximityLogging(proximityId, new SendingProximityLogging() {
            @Override
            public void onDidCompleted() {
            }
        });

        //callback
        if(PMPMapSDK.getProximityCallback() != null){
            PMPMapSDK.getProximityCallback().onProximityEnterRegion(proximityId, rssi, distance, isBackground);
        }

        //Promotion logic
        if(PMPMapSDK.isEnableNotifiation()) {
            ResponseData responseData = PMPDataManager.getSharedPMPManager(getApplication()).getResponseData();
            if (responseData != null && responseData.getPromotions() != null) {
                for (Promotions temp : responseData.getPromotions()) {
                    if(temp.getPromotionType() == PROMOTION_TYPE_DEVICE) {
                        if (temp.getDeviceMajors() == null) {
                            continue;
                        }
                        for (Integer major : temp.getDeviceMajors()) {
                            if (major == proximityId) {
                                Promotions selectedPromotion = temp;
                                if (selectedPromotion != null) {

                                    //internal call back
                                    showProximityMessage(proximityId, selectedPromotion, isBackground);

                                    if (currentPromotion != null && selectedPromotion.getId() == currentPromotion.getId()) {
                                        //same promotion...
                                        return;
                                    }
                                }
                                break;
                            }
                        }
                    }else if(temp.getPromotionType() == PROMOTION_TYPE_ZONAL){
                        for (Integer deviceGroup : temp.getReferenceIds()) {
                            if (deviceGroup == currentDeviceGroupId) {
                                Promotions selectedPromotion = temp;
                                if (selectedPromotion != null) {

                                    //internal call back
                                    showProximityMessage(proximityId, selectedPromotion, isBackground);

                                    if (currentPromotion != null && selectedPromotion.getId() == currentPromotion.getId()) {
                                        //same promotion...
                                        return;
                                    }
                                }
                                break;
                            }
                        }
                    }


                }
            }
        }
    }

    private ProximityMessage convertPromotionToProximityMessage(Promotions currentPromotion){
        Map<Integer, String> title = new HashMap<Integer, String>();
        Map<Integer, String> messages = new HashMap<Integer, String>();
        Map<Integer, String> actionUrl = new HashMap<Integer, String>();
        Map<Integer, String> details = new HashMap<Integer, String>();


        if(currentPromotion.getMessageType() == PMPPromotionMessageType_WelcomeMesaage ||
                currentPromotion.getMessageType() == PMPPromotionMessageType_WelcomeMesaageSeaToAir ||
                currentPromotion.getMessageType() == PMPPromotionMessageType_WelcomeMessageTransfer ||
                currentPromotion.getMessageType() == PMPPromotionMessageType_WelcomeMessageArrival){
            title.put(PMPMapSDK.Language_English, getApplication().getResources().getString(R.string.PMPMAP_WELCOME));
            title.put(PMPMapSDK.Language_SimplifiedChinese, getApplication().getResources().getString(R.string.PMPMAP_WELCOME));
            title.put(PMPMapSDK.Language_TraditionalChinese, getApplication().getResources().getString(R.string.PMPMAP_WELCOME));
            title.put(PMPMapSDK.Language_Japanese, getApplication().getResources().getString(R.string.PMPMAP_WELCOME));
            title.put(PMPMapSDK.Language_Korean, getApplication().getResources().getString(R.string.PMPMAP_WELCOME));
        }else{
            title.put(PMPMapSDK.Language_English, getApplication().getResources().getString(R.string.PMPMAP_ANNOUNCEMENT));
            title.put(PMPMapSDK.Language_SimplifiedChinese, getApplication().getResources().getString(R.string.PMPMAP_ANNOUNCEMENT));
            title.put(PMPMapSDK.Language_TraditionalChinese, getApplication().getResources().getString(R.string.PMPMAP_ANNOUNCEMENT));
            title.put(PMPMapSDK.Language_Japanese, getApplication().getResources().getString(R.string.PMPMAP_ANNOUNCEMENT));
            title.put(PMPMapSDK.Language_Korean, getApplication().getResources().getString(R.string.PMPMAP_ANNOUNCEMENT));
        }

        messages.put(PMPMapSDK.Language_English, PMPUtil.getLocalizedStringByLangID(currentPromotion.getMessage(), PMPMapSDK.Language_English));
        messages.put(PMPMapSDK.Language_SimplifiedChinese, PMPUtil.getLocalizedStringByLangID(currentPromotion.getMessage(), PMPMapSDK.Language_SimplifiedChinese));
        messages.put(PMPMapSDK.Language_TraditionalChinese, PMPUtil.getLocalizedStringByLangID(currentPromotion.getMessage(), PMPMapSDK.Language_TraditionalChinese));
        messages.put(PMPMapSDK.Language_Japanese, PMPUtil.getLocalizedStringByLangID(currentPromotion.getMessage(), PMPMapSDK.Language_Japanese));
        messages.put(PMPMapSDK.Language_Korean, PMPUtil.getLocalizedStringByLangID(currentPromotion.getMessage(), PMPMapSDK.Language_Korean));

        if(currentPromotion.getPromotionMessages() != null && currentPromotion.getPromotionMessages().size() > 0){
            PromotionMessage currentPromotionMessage = currentPromotion.getPromotionMessages().get(0);
            actionUrl.put(PMPMapSDK.Language_English, PMPUtil.getLocalizedStringByLangID(currentPromotionMessage.getActionUrls(), PMPMapSDK.Language_English));
            actionUrl.put(PMPMapSDK.Language_SimplifiedChinese, PMPUtil.getLocalizedStringByLangID(currentPromotionMessage.getActionUrls(), PMPMapSDK.Language_SimplifiedChinese));
            actionUrl.put(PMPMapSDK.Language_TraditionalChinese, PMPUtil.getLocalizedStringByLangID(currentPromotionMessage.getActionUrls(), PMPMapSDK.Language_TraditionalChinese));
            actionUrl.put(PMPMapSDK.Language_Japanese, PMPUtil.getLocalizedStringByLangID(currentPromotionMessage.getActionUrls(), PMPMapSDK.Language_Japanese));
            actionUrl.put(PMPMapSDK.Language_Korean, PMPUtil.getLocalizedStringByLangID(currentPromotionMessage.getActionUrls(), PMPMapSDK.Language_Korean));

            details.put(PMPMapSDK.Language_English, PMPUtil.getLocalizedStringByLangID(currentPromotionMessage.getDetails(), PMPMapSDK.Language_English));
            details.put(PMPMapSDK.Language_SimplifiedChinese, PMPUtil.getLocalizedStringByLangID(currentPromotionMessage.getDetails(), PMPMapSDK.Language_SimplifiedChinese));
            details.put(PMPMapSDK.Language_TraditionalChinese, PMPUtil.getLocalizedStringByLangID(currentPromotionMessage.getDetails(), PMPMapSDK.Language_TraditionalChinese));
            details.put(PMPMapSDK.Language_Japanese, PMPUtil.getLocalizedStringByLangID(currentPromotionMessage.getDetails(), PMPMapSDK.Language_Japanese));
            details.put(PMPMapSDK.Language_Korean, PMPUtil.getLocalizedStringByLangID(currentPromotionMessage.getDetails(), PMPMapSDK.Language_Korean));
        }


        String iconURL = currentPromotion.getBannerIcon();
        boolean showInNotifiationCenter = currentPromotion.isShowInNotificationCenter();
        String externalId = currentPromotion.getExternalId();
        ProximityMessage proximityMessage = new ProximityMessage(externalId, actionUrl, title, messages, details, iconURL, showInNotifiationCenter);
        return proximityMessage;
    }

    public void showProximityMessage(int zoneId, Promotions promotion, boolean isBackground) {
        String string = getDate(System.currentTimeMillis(), "dd-MM-yyyy");
        String key = String.format("KeyPromotionID:%d", (int) promotion.getId());

        //Hardcode for HKIA only!!!! this is request by user and if you are not doing HKIA project then you can remove below code...
        if(promotion.getMessageType() == PMPPromotionMessageType_WelcomeMesaage ||
                promotion.getMessageType() == PMPPromotionMessageType_WelcomeMesaageSeaToAir ||
                promotion.getMessageType() == PMPPromotionMessageType_WelcomeMessageTransfer ||
                promotion.getMessageType() == PMPPromotionMessageType_WelcomeMessageArrival){
            key = "KeyWelcomeMessage";
        }

        String previousDate = PMPUtil.getSharedPreferences(getApplication()).getString(key, null);

        if (previousDate != null && string.equals(previousDate)) {
            //Same date...
            return;
        }

        promotion.setReceivedMajor(zoneId);

        PMPUtil.getSharedPreferences(getApplication()).edit().putString(key, string).commit();

        switch (promotion.getMessageType()) {
            case PMPPromotionMessageType_NONE: {
                com.pmp.mapsdk.cms.model.Message fakeMessage = new com.pmp.mapsdk.cms.model.Message();
                for (PromotionMessage promotionMessages : promotion.getPromotionMessages()) {
                    fakeMessage.setContent(PMPUtil.getLocalizedString(promotionMessages.getMessages()));
                    promotion.setMessage(new ArrayList<com.pmp.mapsdk.cms.model.Message>(Arrays.asList(fakeMessage)));
                }
                showProximityMsgWithPromotion(promotion);
            } break;
            case PMPPromotionMessageType_WelcomeMessageArrival: {
                showWelcomeMsgWithSavedFlightResult(null, promotion);
//                com.pmp.mapsdk.cms.model.Message fakeUrl = new com.pmp.mapsdk.cms.model.Message();
//                fakeUrl.setContent("hkgmyflight://");
//                promotion.setActionUrls(new ArrayList<com.pmp.mapsdk.cms.model.Message>(Arrays.asList(fakeUrl)));
//                com.pmp.mapsdk.cms.model.Message fakeMessage = new com.pmp.mapsdk.cms.model.Message();
//                for (PromotionMessage promotionMessages : promotion.getPromotionMessages()) {
//                    fakeMessage.setContent(PMPUtil.getLocalizedString(promotionMessages.getMessages()));
//                    promotion.setMessage(new ArrayList<com.pmp.mapsdk.cms.model.Message>(Arrays.asList(fakeMessage)));
//                }
//                showProximityMsgWithPromotion(promotion);
            }
            break;
            case PMPPromotionMessageType_WelcomeMesaage:
            case PMPPromotionMessageType_WelcomeMesaageSeaToAir:
            case PMPPromotionMessageType_WelcomeMessageTransfer: {
                checkIfUserHasSavedFlight(promotion);
            }
            break;
            case PMPPromotionMessageType_AELnotOperating:
            case PMPPromotionMessageType_LastAELNotification: {
                int[] isOpening = new int[1];
                String hr = CoreEngine.getInstance().parseScheduleByPromotionID((int) promotion.getId(), isOpening);
                if (isOpening[0] == 1) {
                    showAELMsg(promotion, hr);
                }else{
                    PMPUtil.getSharedPreferences(getApplication()).edit().remove(key).commit();
                }
            }
            break;
            default:
                break;
        }
    }

    private void checkIfUserHasSavedFlight(final Promotions promotion) {
        PMPProximityServerManager.getShared(this.getApplication()).getUserSavedFlight(new PMPProximityServerManagerNotifier() {
            @Override
            public void didSuccess(Object response) {
                if (response instanceof SaveUserFlightResponse) {
                    Result result = ((SaveUserFlightResponse) response).getResult();
                    //user has saved flight!

                    //Update AR View
                    if (result != null) {
                        String mtelRecordId = TextUtils.isEmpty(result.getMtelRecordId()) ? "" : result.getMtelRecordId();
                        String flightNo = TextUtils.isEmpty(result.getPreferredIdentifier()) ? "" : result.getPreferredIdentifier();
                        String gate = TextUtils.isEmpty(result.getGate()) ? "" : result.getGate();
                        String flightStatus = TextUtils.isEmpty(result.getFlightStatus()) ? "" : result.getFlightStatus();
                        String departureTime = TextUtils.isEmpty(result.getDepartureTime()) ? "" : result.getDepartureTime();

                        PMPMapController.getInstance().updateUserFlightForARView(mtelRecordId, flightNo, gate, flightStatus, departureTime);
                    }

                    if (result.getMtelRecordId() != null && result.getMtelRecordId().length() > 0) {
                        //special handling for sea to air
                        if(promotion.getMessageType() == PMPPromotionMessageType_WelcomeMesaageSeaToAir){
                            long time = ((SaveUserFlightResponse) response).getResult().getBestOfTime();
                            long secsUtc1970 = System.currentTimeMillis();
                            long diff = time - secsUtc1970;
                            if(diff < 12 * 60 * 60 * 1000){
                                //within 12hr...
                                showWelcomeMsgWithSavedFlightResult(result, promotion);
                            }else{
                                showWelcomeMsgWithSavedFlightResult(null, promotion);
                            }
                            return;
                        }
                        SaveUserFlightResponse userFlightResponse = (SaveUserFlightResponse) response;
                        showWelcomeMsgWithSavedFlightResult(userFlightResponse.getResult(), promotion);
                    } else {
                        showWelcomeMsgWithSavedFlightResult(null, promotion);
                    }
                }else{
                    showWelcomeMsgWithSavedFlightResult(null, promotion);
                }
            }

            @Override
            public void didFailure() {
                showWelcomeMsgWithSavedFlightResult(null, promotion);
            }
        });
    }

    private void showProximityMsgWithPromotion(final Promotions promotion) {
        HashMap<String, Object> param = new HashMap<>();
        param.put("promotion_id", "" + (int)promotion.getId());
        param.put("type", promotion.getMessageType());
        param.put("sent_major_id", "" + promotion.getReceivedMajor());
        param.put("is_background", bgStatus == PMPBackgroundStatus.Background? "1" : "0");
        AnalyticsLogger.getInstance().logEvent("Detail_Show_Promotion", param);

        final PMPBackgroundStatus bgStatus = getPmpApplication().getBgStatus();
        if (bgStatus == Background) {
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplication());
            mBuilder.setSmallIcon(application.getApplicationInfo().icon);
//            mBuilder.setSmallIcon(R.drawable.small_hkia_appicon);
//            mBuilder.setSmallIcon(R.drawable.notification_icon);
//            mBuilder.setContentTitle("Notification Alert, Click Me!");
            if(PMPMapSDK.getMainActivityClass() != null) {
                Intent resultIntent = new Intent(getApplication(), PMPMapSDK.getMainActivityClass());
                if(promotion.getPromotionMessages() != null && promotion.getPromotionMessages().size() > 0){
                    resultIntent.putExtra(PMPMapSDK.NotificationURL, PMPUtil.getLocalizedString(promotion.getPromotionMessages().get(0).getActionUrls()));
                }
                PendingIntent resultPendingIntent = PendingIntent.getActivity(getApplication(),0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                mBuilder.setContentIntent(resultPendingIntent);

                if(promotion.getMessageType() == PMPPromotionMessageType_NONE){
                    mBuilder.setContentTitle(getApplication().getResources().getString(R.string.PMPMAP_PROMOTION));
                }else if(promotion.getMessageType() == PMPPromotionMessageType_LastAELNotification ||
                        promotion.getMessageType() == PMPPromotionMessageType_AELnotOperating){
                    mBuilder.setContentTitle(getApplication().getResources().getString(R.string.PMPMAP_TRANSPORT_ALERT));
                }else{
                    mBuilder.setContentTitle(getApplication().getResources().getString(R.string.PMPMAP_WELCOME));
                }

                mBuilder.setAutoCancel(true);
                mBuilder.setContentText(PMPUtil.getLocalizedString(promotion.getMessage()));
                NotificationManager mNotificationManager = (NotificationManager) this.getApplication().getSystemService(Context.NOTIFICATION_SERVICE);

                // notificationID allows you to update the notification later on.
                mNotificationManager.notify((int)System.currentTimeMillis(), mBuilder.build());
            }
        } else{
            CoreEngine.getInstance().onInZone(promotion, false);

            final ProximityMessage proximityMessage = convertPromotionToProximityMessage(promotion);

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if(PMPMapSDK.getPromotionCallback() != null){
                        PMPMapSDK.getPromotionCallback().onPromotionDetected(currentProximityID, promotion.getExternalId(), bgStatus == PMPBackgroundStatus.Background, proximityMessage);
                    }
                }
            });
        }
    }

    private void showAELMsg(Promotions promotion, String openingHr) {
        com.pmp.mapsdk.cms.model.Message fakeUrl = new com.pmp.mapsdk.cms.model.Message();
        fakeUrl.setContent("hkgmyflight://LocalTransportation");
        if(promotion.getPromotionMessages() != null && promotion.getPromotionMessages().size() > 0){
            promotion.getPromotionMessages().get(0).setActionUrls(new ArrayList<com.pmp.mapsdk.cms.model.Message>(Arrays.asList(fakeUrl)));
        }

        //check AEL
        Pois poi = CollectionUtils.find(PMPDataManager.getSharedPMPManager(getApplication()).getResponseData().getPois(), new Predicate<Pois>() {
            @Override
            public boolean evaluate(Pois object) {
                return object.getExternalId().equals(externalIDForAEL);
            }
        });
        if (poi == null) {
            return;
        }
        int[] isOpening = new int[1];
        String hr = CoreEngine.getInstance().parseScheduleByPOIID((int) poi.getId(), isOpening);
        String[] components = hr.split(" - ");
        String openHr;
        String closeHr;
        if (components.length > 1) {
            openHr = components[0];
            closeHr = components[1];

            Map stringReplaceDict = new HashMap();
            stringReplaceDict.put("<AEL_FIRST_TRAIN>", openHr);
            stringReplaceDict.put("<AEL_LAST_TRAIN>", closeHr);


            String msg = findSuitableTextByProximityLogic(promotion.getPromotionMessages(), stringReplaceDict);
            if (msg != null && msg.length() > 0) {
                com.pmp.mapsdk.cms.model.Message fakeMessage = new com.pmp.mapsdk.cms.model.Message();
                fakeMessage.setContent(msg);
                promotion.setMessage(new ArrayList<com.pmp.mapsdk.cms.model.Message>(Arrays.asList(fakeMessage)));
                showProximityMsgWithPromotion(promotion);
            }
        }
    }

    private void showWelcomeMsgWithSavedFlightResult(Result savedFlight, Promotions promotion) {
        String msg = null;
        String preferredIdentifier = savedFlight != null ? savedFlight.getPreferredIdentifier() : null;
        String gate = savedFlight != null ? savedFlight.getGate() : null;
        String flightNo = savedFlight != null ? savedFlight.getFlightNo() : null;
        String transferDesk = savedFlight != null ? savedFlight.getTransferDesk() : null;
        String aisle = savedFlight != null ? savedFlight.getAisle() : null;
        String travelTime = (savedFlight != null && savedFlight.getTravelTime() >= 0) ? String.format("%d", (long)savedFlight.getTravelTime()) : "";
        String departureTime = null;
        String terminal = null;
        String mtelRecordId = savedFlight != null ? savedFlight.getMtelRecordId() : null;

        if (savedFlight != null && savedFlight.getBestOfTime() > 0) {
            departureTime = getDate(savedFlight.getBestOfTime(), "HH:mm");
        }

        if(aisle != null && aisle.length() > 1){
            //get one aisle is enough
            aisle = "" + aisle.charAt(0);
        }
        final String externalIDForAisel = String.format("checkIn_%s", aisle);
        final Pois foundPoi = CollectionUtils.find(PMPDataManager.getSharedPMPManager(null).getResponseData().getPois(), new Predicate<Pois>() {
            @Override
            public boolean evaluate(Pois object) {
                return object.getExternalId().equals(externalIDForAisel);
            }
        });
        if (foundPoi != null) {
            Areas foundArea = CollectionUtils.find(PMPDataManager.getSharedPMPManager(null).getResponseData().getAreas(), new Predicate<Areas>() {
                @Override
                public boolean evaluate(Areas object) {
                    return object.getId() == foundPoi.getAreaId();
                }
            });
            if (foundArea != null) {
                terminal = PMPUtil.getLocalizedString(foundArea.getName());
            }
        }

        com.pmp.mapsdk.cms.model.Message fakeUrl = new com.pmp.mapsdk.cms.model.Message();
        if (!TextUtils.isEmpty(mtelRecordId) && promotion.getMessageType() != PMPPromotionMessageType_WelcomeMessageTransfer) {
            if (mtelRecordId != null && mtelRecordId.length() > 0) {
                fakeUrl.setContent(String.format("hkgmyflight://flightDetail?recordId=%s&flightNum=%s", mtelRecordId, preferredIdentifier.replace(" ", "")));
            }

        } else if (promotion.getMessageType() == PMPPromotionMessageType_WelcomeMessageArrival) {
            fakeUrl.setContent("hkgmyflight://");
        } else {
            fakeUrl.setContent("hkgmyflight://searchflight");
        }

        if(promotion.getPromotionMessages() != null && promotion.getPromotionMessages().size() > 0){
            promotion.getPromotionMessages().get(0).setActionUrls(new ArrayList<com.pmp.mapsdk.cms.model.Message>(Arrays.asList(fakeUrl)));
        }

        Map<String, String> stringReplaceDict = new HashMap<>();
        stringReplaceDict.put("<FLIGHT_NO>", preferredIdentifier != null ? preferredIdentifier:"");
        stringReplaceDict.put("<DEPARTURE_TIME>", departureTime != null ? departureTime:"");
        stringReplaceDict.put("<GATE_NO>", gate != null ? gate:"");
        stringReplaceDict.put("<ESTIMATED_TIME>", travelTime != null ? travelTime:"");
        stringReplaceDict.put("<TRANSFER_DESK>", transferDesk != null ? transferDesk:"");
        stringReplaceDict.put("<CHECKIN_AISLE>", aisle != null ? aisle:"");
        stringReplaceDict.put("<TERMINAL>", terminal != null ? terminal:"");


        msg = findSuitableTextByProximityLogic(promotion.getPromotionMessages(), stringReplaceDict);

        if (msg.length() > 0) {
            com.pmp.mapsdk.cms.model.Message fakeMessage = new com.pmp.mapsdk.cms.model.Message();
            fakeMessage.setContent(msg);
            promotion.setMessage(new ArrayList<com.pmp.mapsdk.cms.model.Message>(Arrays.asList(fakeMessage)));
            showProximityMsgWithPromotion(promotion);
        }
    }

    private String findSuitableTextByProximityLogic(ArrayList<PromotionMessage> msgs, Map<String, String> stringReplaceDict) {
        String finalMsg = "";
        for (PromotionMessage promotionMessages : msgs){
            finalMsg = PMPUtil.getLocalizedString(promotionMessages.getMessages());
            boolean isCurrentStringValid = true;

            for (Map.Entry<String, String> entry : stringReplaceDict.entrySet()) {
                if(finalMsg.indexOf(entry.getKey()) >= 0){
                    if(entry.getValue().length() == 0){
                        //user does not have this value, e.g user not saved flight
                        finalMsg = "";
                        isCurrentStringValid = false;
                        break;
                    }

                    finalMsg = finalMsg.replace(entry.getKey(), entry.getValue());
                }
            }

            if(isCurrentStringValid){
                break;
            }
        }


        return finalMsg;
    }

    private double lastZoneId = -1;
    private double lastAAZoneId = -1;
    private int lastMajorId = -1;

    public void onProximityLogging(final int proximityId, SendingProximityLogging logging) {
        ResponseData response = PMPDataManager.getSharedPMPManager(null).getResponseData();
        int zoneDidChangeLoggingTimeout = response.getZoneDidChangeLoggingTimeout();
        int zoneLoggingTimeout = response.getZoneLoggingTimeout();
        if (!ENABLE_PROXIMITY_LOGGING) {
            if (logging != null) {
                logging.onDidCompleted();
            }
            return;
        }

        final Devices device = CollectionUtils.find(response.getDevices(), new Predicate<Devices>() {
            @Override
            public boolean evaluate(Devices object) {
                return object.getMajorNo() == proximityId;
            }
        });

        if (device != null) {
            AaZones aaZone = CollectionUtils.find(response.getAaZones(), new Predicate<AaZones>() {
                @Override
                public boolean evaluate(AaZones object) {
                    return object.getId() == device.getAaZoneId();
                }
            });

            Zones zone = CollectionUtils.find(response.getZones(), new Predicate<Zones>() {
                @Override
                public boolean evaluate(Zones object) {
                    return object.getId() == device.getZoneId();
                }
            });
            if (zone != null && aaZone != null) {
                double zoneId = zone.getId();
                double aaZoneId = aaZone.getId();
                long lastSendDataTimeMillis = PMPUtil.getSharedPreferences(getApplication().getBaseContext()).getLong(LAST_SEND_DATA_TIME_MILLIS, System.currentTimeMillis());
                long currentTimeMillis = System.currentTimeMillis();
                if (lastZoneId != zoneId || lastAAZoneId != aaZoneId) {
                    lastZoneId = zoneId;
                    lastAAZoneId = aaZoneId;
                    lastMajorId = proximityId;
                    onSendProximityLogging(logging);
                } else if (lastMajorId != proximityId) {
                    if ((currentTimeMillis - lastSendDataTimeMillis) > zoneDidChangeLoggingTimeout * 1000 && zoneDidChangeLoggingTimeout > 0) {
                        lastMajorId = proximityId;
                        onSendProximityLogging(logging);
                    } else {
                        if (logging != null) {
                            logging.onDidCompleted();
                        }
                    }
                } else {
                    if ((currentTimeMillis - lastSendDataTimeMillis) > zoneLoggingTimeout * 1000 && zoneLoggingTimeout > 0) {
                        lastMajorId = proximityId;
                        onSendProximityLogging(logging);
                    } else {
                        if (logging != null) {
                            logging.onDidCompleted();
                        }
                    }
                }
                lastMajorId = proximityId;
            }
        } else {
            if (logging != null) {
                logging.onDidCompleted();
            }
        }
    }

    private void onSendProximityLogging(final SendingProximityLogging logging){
        PMPUtil.getSharedPreferences(getApplication().getBaseContext()).edit().putLong(LAST_SEND_DATA_TIME_MILLIS, System.currentTimeMillis()).commit();

//        String params = "zone_id=" + lastZoneId + "&device_name=" + android.os.Build.MODEL + "&device_os_version=" + android.os.Build.VERSION.RELEASE + "&is_background=" + ((getPmpApplication().bgStatus == PMPBackgroundStatus.Background) ? 1 : 0) + "&proximity_major=" + lastMajorId;

//        executePost("http://hkia-tools.cherrypicksalpha.com/api/v1/zone_change", params);

        HashMap<String, Object> param = new HashMap<>();
        addStaticParam(param);
        AnalyticsLogger.getInstance().logBackgroundEvent("Location History", param);

        PMPProximityServerManager.getShared(getApplication().getBaseContext()).saveUserLocation("" + (int)lastAAZoneId, new PMPProximityServerManagerNotifier() {
            @Override
            public void didSuccess(Object response) {
                if (logging != null) {
                    logging.onDidCompleted();
                }
            }

            @Override
            public void didFailure() {
                if (logging != null) {
                    logging.onDidCompleted();
                }
            }
        });
    }

    private void addStaticParam(HashMap<String, Object> finalParam){
        finalParam.put("is_background", "" + ((getPmpApplication().bgStatus == PMPBackgroundStatus.Background) ? 1 : 0));
        finalParam.put("client_timestamp", "" + System.currentTimeMillis());
        finalParam.put("zone_id", "" + lastAAZoneId);
        finalParam.put("major_id", "" + currentProximityID);
        finalParam.put("minor_id", "" + currentProximityMinor);
        finalParam.put("rssi", "" + currentProximityRssi);
        finalParam.put("map_center_x", ""+ PMPMapController.getInstance().getScreenCenterX());
        finalParam.put("map_center_y", ""+ PMPMapController.getInstance().getScreenCenterY());
        finalParam.put("distance", "" + currentProximityDistance);
        if(CoreEngine.getInstance().majorOfBeaconsForTrilateration != null && CoreEngine.getInstance().majorOfBeaconsForTrilateration.length() > 0){
            finalParam.put("wf_major_id", CoreEngine.getInstance().majorOfBeaconsForTrilateration);
        }

//        if(CoreEngine.getInstance().majorOfNeighbourBeacons != null && CoreEngine.getInstance().majorOfNeighbourBeacons.length() > 0){
//            finalParam.put("nb_major_id", CoreEngine.getInstance().majorOfNeighbourBeacons);
//        }

        //Battery...
        finalParam.put("battery_level", "" + batteryLevel);

        //IP address
        finalParam.put("ip_address", "" + AnalyticsHelper.getIPAddress(true));

        //Storage capacity
        finalParam.put("storage_capacity", "" + AnalyticsHelper.getTotalInternalMemorySize());
        finalParam.put("storage_available", "" + AnalyticsHelper.getAvailableInternalMemorySize());

        //Ram...
        ActivityManager actManager = (ActivityManager) getApplication().getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memInfo = new ActivityManager.MemoryInfo();
        actManager.getMemoryInfo(memInfo);
        finalParam.put("total_memory", "" + memInfo.totalMem);
        finalParam.put("user_memory", "" + memInfo.availMem);

        //Screen
        try {
            int curBrightnessValue = android.provider.Settings.System.getInt(getApplication().getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS);
            finalParam.put("brightness", "" + curBrightnessValue);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }


        //Language...
        finalParam.put("language", "" + PMPMapSDK.getLangID());

        //Wifi
        ConnectivityManager connManager = (ConnectivityManager) getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mWifi.isConnected()) {
            // Do whatever
            finalParam.put("wifi_on_off", "1");
        }else{
            finalParam.put("wifi_on_off", "0");
        }

//        WifiManager wifiMan = (WifiManager) getApplication().getSystemService(
//                Context.WIFI_SERVICE);
//        WifiInfo wifiInf = wifiMan.getConnectionInfo();
//        String macAddr = wifiInf.getMacAddress();
//        finalParam.put("Wifi MAC", "" + macAddr);


        //Blue tooth
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        boolean isBlueToothOn = false;
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                // Bluetooth is not enable :)
            }else{
                isBlueToothOn = true;
            }
        }
        finalParam.put("bluetooth_on_off", isBlueToothOn?"1":"0");

//        finalParam.put("Map ID", aaa);
        //Location
        PMPLocation location = CoreEngine.getInstance().getIndoorLocation();
        if(location != null){
            finalParam.put("x", "" + (int)location.getX());
            finalParam.put("y", "" + (int)location.getY());
            finalParam.put("floorId", location.getName());
        }
        if(CoreEngine.getInstance().getMapMode() != CoreEngine.MapModeUnknown) {
            finalParam.put("nav_mode", CoreEngine.getInstance().getMapMode());
        }
        finalParam.put("is_disability", PMPMapSDK.isDisability()?"1":"0");
    }

//    public static String executePost(String targetURL, String urlParameters) {
//        HttpURLConnection connection = null;
//        try {
//            //Create connection
//            URL url = new URL(targetURL);
//            connection = (HttpURLConnection) url.openConnection();
//            connection.setRequestMethod("POST");
//            connection.setRequestProperty("Content-Type",
//                    "application/x-www-form-urlencoded");
//
//            connection.setRequestProperty("Content-Length",
//                    Integer.toString(urlParameters.getBytes().length));
//            connection.setRequestProperty("Content-Language", "en-US");
//
//            connection.setUseCaches(false);
//            connection.setDoOutput(true);
//
//            //Send request
//            DataOutputStream wr = new DataOutputStream (
//                    connection.getOutputStream());
//            wr.writeBytes(urlParameters);
//            wr.close();
//
//            //Get Response
//            InputStream is = connection.getInputStream();
//            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
//            StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
//            String line;
//            while ((line = rd.readLine()) != null) {
//                response.append(line);
//                response.append('\r');
//            }
//            rd.close();
//            return response.toString();
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        } finally {
//            if (connection != null) {
//                connection.disconnect();
//            }
//        }
//    }

    public static Application getApplication() {
        return application;
    }

    public static PMPApplication getPmpApplication() {
        return pmpApplication;
    }

    public int getCurrentProximityID() {
        return currentProximityID;
    }

    public int getCurrentProximityMinor() {
        return currentProximityMinor;
    }

    public void setEnableProximityLogging(boolean enable) {
        ENABLE_PROXIMITY_LOGGING = enable;
    }

    private static interface SendingProximityLogging {
        void onDidCompleted();
    }
}
