package com.pmp.mapsdk.external;

/**
 * This interface consists proximity marketing related callback that will triggered
 * by proximity logic. For example, receiving proximity signal will trigger a call from the PMP engine.
 *
 * @author  Alan Chan
 * @since   1.0
 */
public interface ProximityCallback {
    /**
     * The method will be called when the PMP engine receive proximity signal
     *
     * @param proximityId the proximity ID reported by the proximity signal
     * @param promotionId the ID of promotion that associated with the proximity ID
     * @param isBackground the app state when receiving the proximity singal
     */
    void onProximityEnterRegion(int proximityId, double rssi, double distance, boolean isBackground);

    /**
     * The method will be called when the application leave an proximity area
     *
     * @param proximityId the proximity ID reported by the proximity signal
     */
    void onProximityExitRegion(int proximityId);
}
