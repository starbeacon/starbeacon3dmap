package com.pmp.mapsdk.external;

/**
 * Created by andrewman on 1/2/2017.
 */

public interface OpenBrowserCallback {
    public void openBrowser(String url);
}
