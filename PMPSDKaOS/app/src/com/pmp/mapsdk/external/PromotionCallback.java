package com.pmp.mapsdk.external;

import com.cherrypicks.pmpmap.datamodel.MapState;

/**
 * This interface consists proximity marketing related callback that will triggered
 * by proximity logic. For example, receiving proximity signal will trigger a call from the PMP engine.
 *
 * @author  Alan Chan
 * @since   1.0
 */
public interface PromotionCallback {
    /**
     * The method will be called when the PMP engine receive proximity signal
     *
     * @param proximityId the proximity ID reported by the proximity signal
     * @param promotionId the ID of promotion that associated with the proximity ID
     * @param isBackground the app state when receiving the proximity singal
     */
    void onPromotionDetected(int proximityId, String promotionId, boolean isBackground, ProximityMessage proximityMessage);


    /**
     * The method will be called when user clicked on UIs inside PMPMap that associated
     * with promotion.
     *
     * @param proximityId the proximity ID reported by the proximity signal
     * @param promotionId the ID of promotion
     * @param mapState current state of the PMPMap
     */
    void onPromotionClicked(int proximityId, String promotionId, MapState mapState);

}
