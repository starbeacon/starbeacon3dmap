package com.pmp.mapsdk.external;

import java.util.Set;

/**
 * Created by andrew on 27/10/2016.
 */

public interface RetrieveFlightStatusBackgroundJobCallback {
    public void onRetrieveFlightStatus(Set<String> recordIds);
}
