package com.pmp.mapsdk.external;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.cherrypicks.hkia.FlightStatus;
import com.cherrypicks.hkia.FlightStatusParser;
import com.cherrypicks.pmpmap.PMPIndoorLocationManager;
import com.cherrypicks.pmpmap.PMPMapController;
import com.cherrypicks.pmpmap.analytics.AnalyticsEventCallback;
import com.cherrypicks.pmpmap.analytics.AnalyticsHelper;
import com.cherrypicks.pmpmap.analytics.AnalyticsLogger;
import com.cherrypicks.pmpmap.core.CoreEngine;
import com.cherrypicks.pmpmap.datamodel.MapState;
import com.cherrypicks.pmpmap.sensor.MotionManager;
import com.cherrypicks.pmpmapsdk.BuildConfig;
import com.cherrypicks.pmpmapsdk.R;
import com.pmp.mapsdk.cms.PMPProximityServerManager;
import com.pmp.mapsdk.cms.PMPServerManager;
import com.pmp.mapsdk.cms.model.PMPProximityServerManagerNotifier;
import com.pmp.mapsdk.cms.model.Pois;
import com.pmp.mapsdk.cms.model.SaveUserFlightResponse;
import com.pmp.mapsdk.location.PMPApplication;
import com.pmp.mapsdk.utils.PMPUtil;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import static com.pmp.mapsdk.external.PMPMapSDK.MapUISetting.PMPBlueDotMode_SpotLight;
import static com.pmp.mapsdk.external.PMPMapSDK.MapUISetting.PMPNavButtonMode_Arrow;


/**
 * This class consists static methods that configure the PMPMap. It contains Map
 * state control, language control, analytics callback and User interaction callback.
 * <p>Some methods of this class will throw a <tt>NullPointerException</tt>
 * if the parameter objects provided to them are null.
 *
 * @author Alan Chan
 * @since 1.0
 */
public class PMPMapSDK {
    public static class PathCalculationResult {
        int totalTime;//second
        int totalDistance;//meter

        public PathCalculationResult(int totalTime, int totalDistance) {
            this.totalTime = totalTime;
            this.totalDistance = totalDistance;
        }

        public int getTotalTime() {
            return totalTime;
        }

        public int getTotalDistance() {
            return totalDistance;
        }
    }

    public static class PMPConfiguration {
        private boolean shouldEnableGoogleMap;
        private boolean shouldEnableGPSPositioning;

        private int gpsAccuracyThreshold;
        private int bleToGpsTimeoutInSec;

        public boolean isShouldEnableGoogleMap() {
            return shouldEnableGoogleMap;
        }

        public void setShouldEnableGoogleMap(boolean shouldEnableGoogleMap) {
            this.shouldEnableGoogleMap = shouldEnableGoogleMap;
        }

        public boolean isShouldEnableGPSPositioning() {
            return shouldEnableGPSPositioning;
        }

        public void setShouldEnableGPSPositioning(boolean shouldEnableGPSPositioning) {
            this.shouldEnableGPSPositioning = shouldEnableGPSPositioning;
        }

        /**
         * When GPS signal's accuracy is smaller than the GpsAccuracyThreshold, in other word
         * the GPS sinal is quite accurate, then it will override the BLE positioning logic
         * @return gpsAccuracyThreshold in meter
         */
        public int getGpsAccuracyThreshold() {
            return gpsAccuracyThreshold;
        }

        /**
         * Set the GpsAccuracyThreshold in meter.
         * When GPS signal's accuracy is smaller than the GpsAccuracyThreshold, in other word
         * the GPS sinal is quite accurate, then it will override the BLE positioning logic
         *
         * @param gpsAccuracyThreshold
         */
        public void setGpsAccuracyThreshold(int gpsAccuracyThreshold) {
            this.gpsAccuracyThreshold = gpsAccuracyThreshold;
        }

        public int getBleToGpsTimeoutInSec() {
            return bleToGpsTimeoutInSec;
        }

        public void setBleToGpsTimeoutInSec(int bleToGpsTimeoutInSec) {
            this.bleToGpsTimeoutInSec = bleToGpsTimeoutInSec;
        }
    }

    public static class MapUISetting {
        public static final int PMPBlueDotMode_SpotLight = 1;
        public static final int PMPBlueDotMode_CircleRegion = 2;
        public static final int PMPBlueDotMode_CircleRegionWithDirection = 3;

        public static final int PMPNavButtonMode_Arrow = 1;
        public static final int PMPNavButtonMode_Rectangle_Btn = 2;


        private float density;

        //App theme color
        private int themeColor;

        //Recent search
        private int recentSearchBgColor;
        private int recentSearchTextColor;

        //Around me
        private int aroundMeIconImageResource;

        //Nav bar
        private int navSearchImageResource;
        private int navFilterImageResource;

        //Listview cell
        private int genericCellTextColor;

        //locate me button
        private int locateMeButtonOnImageResource;
        private int locateMeButtonOffImageResource;
        private int locateMeButtonCompassImageResource;

        //in-out door switch
        private int indoorButtonImageResource;
        private int outdoorButtonImageResource;

        //compass
        private int compassImageResource;

        //tts
        private int ttsButtonOnImageResource;
        private int ttsButtonOffImageResource;

        //navigation
        private int startNavButtonImageResource;

        //floor control
        private int floorCellOffResource;
        private int floorCellOnResource;
        private int floorNameTextColorOn;
        private int floorNameTextColorOff;
        private int floorCellHeight;
        private int floorSwitchWidth;
        private double numOfVisibleCell; // 0 = all

        private int blueDotMode;

        private int navButtonMode;
        private int navRectButtonImageResource;
        private String navRectButtonTitle;
        private int navRectButtonTitleColor;

        public MapUISetting(){
            final DisplayMetrics metrics = getApplication().getResources().getDisplayMetrics();
            density = metrics.density;
        }

        public int getThemeColor() {
            return themeColor;
        }

        public int getLocateMeButtonOnImageResource() {
            return locateMeButtonOnImageResource;
        }

        public int getLocateMeButtonOffImageResource() {
            return locateMeButtonOffImageResource;
        }

        public int getLocateMeButtonCompassImageResource() {
            return locateMeButtonCompassImageResource;
        }

        public int getIndoorButtonImageResource() {
            return indoorButtonImageResource;
        }

        public int getOutdoorButtonImageResource() {
            return outdoorButtonImageResource;
        }

        public int getCompassImageResource() {
            return compassImageResource;
        }

        public int getTtsButtonOnImageResource() {
            return ttsButtonOnImageResource;
        }

        public int getTtsButtonOffImageResource() {
            return ttsButtonOffImageResource;
        }

        public int getStartNavButtonImageResource() {
            return startNavButtonImageResource;
        }

        public int getFloorCellOffResource() {
            return floorCellOffResource;
        }

        public int getFloorCellOnResource() {
            return floorCellOnResource;
        }

        public int getFloorNameTextColorOn() {
            return floorNameTextColorOn;
        }

        public int getFloorNameTextColorOff() {
            return floorNameTextColorOff;
        }

        public int getFloorCellHeight() {
            return (int) (floorCellHeight * density);
        }

        public int getFloorSwitchWidth() {
            return (int) (floorSwitchWidth * density);
        }

        public double getNumOfVisibleCell() {
            return numOfVisibleCell;
        }

        public int getAroundMeIconImageResource() {
            return aroundMeIconImageResource;
        }

        public int getRecentSearchBgColor() {
            return recentSearchBgColor;
        }

        public int getRecentSearchTextColor() {
            return recentSearchTextColor;
        }

        public int getNavSearchImageResource() {
            return navSearchImageResource;
        }

        public int getNavFilterImageResource() {
            return navFilterImageResource;
        }

        public int getGenericCellTextColor() {
            return genericCellTextColor;
        }

        public void setThemeColor(int themeColor) {
            this.themeColor = themeColor;
        }

        public void setLocateMeButtonOnImageResource(int locateMeButtonOnImageResource) {
            this.locateMeButtonOnImageResource = locateMeButtonOnImageResource;
        }

        public void setLocateMeButtonOffImageResource(int locateMeButtonOffImageResource) {
            this.locateMeButtonOffImageResource = locateMeButtonOffImageResource;
        }

        public void setLocateMeButtonCompassImageResource(int locateMeButtonCompassImageResource) {
            this.locateMeButtonCompassImageResource = locateMeButtonCompassImageResource;
        }

        public void setIndoorButtonImageResource(int indoorButtonImageResource) {
            this.indoorButtonImageResource = indoorButtonImageResource;
        }

        public void setOutdoorButtonImageResource(int outdoorButtonImageResource) {
            this.outdoorButtonImageResource = outdoorButtonImageResource;
        }

        public void setCompassImageResource(int compassImageResource) {
            this.compassImageResource = compassImageResource;
        }

        public void setTtsButtonOnImageResource(int ttsButtonOnImageResource) {
            this.ttsButtonOnImageResource = ttsButtonOnImageResource;
        }

        public void setTtsButtonOffImageResource(int ttsButtonOffImageResource) {
            this.ttsButtonOffImageResource = ttsButtonOffImageResource;
        }

        public void setStartNavButtonImageResource(int startNavButtonImageResource) {
            this.startNavButtonImageResource = startNavButtonImageResource;
        }

        public void setFloorCellOffResource(int floorCellOffResource) {
            this.floorCellOffResource = floorCellOffResource;
        }

        public void setFloorCellOnResource(int floorCellOnResource) {
            this.floorCellOnResource = floorCellOnResource;
        }

        public void setFloorNameTextColorOn(int floorNameTextColorOn) {
            this.floorNameTextColorOn = floorNameTextColorOn;
        }

        public void setFloorNameTextColorOff(int floorNameTextColorOff) {
            this.floorNameTextColorOff = floorNameTextColorOff;
        }

        public void setFloorCellHeight(int floorCellHeight) {
            this.floorCellHeight = floorCellHeight;
        }

        public void setFloorSwitchWidth(int floorSwitchWidth) {
            this.floorSwitchWidth = floorSwitchWidth;
        }

        public void setNumOfVisibleCell(double numOfVisibleCell) {
            this.numOfVisibleCell = numOfVisibleCell;
        }

        public void setRecentSearchBgColor(int recentSearchBgColor) {
            this.recentSearchBgColor = recentSearchBgColor;
        }

        public void setRecentSearchTextColor(int recentSearchTextColor) {
            this.recentSearchTextColor = recentSearchTextColor;
        }

        public void setNavSearchImageResource(int navSearchImageResource) {
            this.navSearchImageResource = navSearchImageResource;
        }

        public void setNavFilterImageResource(int navFilterImageResource) {
            this.navFilterImageResource = navFilterImageResource;
        }

        public void setGenericCellTextColor(int genericCellTextColor) {
            this.genericCellTextColor = genericCellTextColor;
        }

        public void setAroundMeIconImageResource(int aroundMeIconImageResource) {
            this.aroundMeIconImageResource = aroundMeIconImageResource;
        }

        public int getBlueDotMode() {
            return blueDotMode;
        }

        public void setBlueDotMode(int blueDotMode) {
            this.blueDotMode = blueDotMode;
            CoreEngine.getInstance().setBlueDotMode(blueDotMode);
        }

        public int getNavButtonMode() {
            return navButtonMode;
        }

        public void setNavButtonMode(int navButtonMode) {
            this.navButtonMode = navButtonMode;
        }

        public int getNavRectButtonImageResource() {
            return navRectButtonImageResource;
        }

        public void setNavRectButtonImageResource(int navRectButtonImageResource) {
            this.navRectButtonImageResource = navRectButtonImageResource;
        }

        public String getNavRectButtonTitle() {
            return navRectButtonTitle;
        }

        public void setNavRectButtonTitle(String navRectButtonTitle) {
            this.navRectButtonTitle = navRectButtonTitle;
        }

        public int getNavRectButtonTitleColor() {
            return navRectButtonTitleColor;
        }

        public void setNavRectButtonTitleColor(int navRectButtonTitleColor) {
            this.navRectButtonTitleColor = navRectButtonTitleColor;
        }
    }

    public static final String NotificationURL = "NotificationURL";
    public static final int Language_English = 1;
    public static final int Language_Chinese = 2;
    public static final int Language_TraditionalChinese = 2;
    public static final int Language_SimplifiedChinese = 3;
    public static final int Language_Japanese = 4;
    public static final int Language_Korean = 5;
    public static final String Platform_Android = "Android";
    public static final String BROADCAST_NOTIFICATION_ACTION = "BROADCAST_NOTIFICATION_ACTION";
    public static final String NOTIFICATION_KEY = "NOTIFICATION_KEY";
    private final static String PREFERENCE_NAME = "PMPMapSDK";
    private final static String TAG = "PMPMapSDK";
    private static int langID = Language_English;
    private final static String MonitoringIDS = "MonitoringIDS";

    private static ProximityCallback proximityCallback;
    private static PromotionCallback promotionCallback;
    private static LaunchDetailCallback launchDetailCallback;
    private static IndoorLocationUpdateCallback indoorLocationUpdateCallback;
    private static RetrieveFlightStatusBackgroundJobCallback retrieveFlightStatusBackgroundJobCallback;
    private static Application application;
    private static SharedPreferences sharedPreferences;
    private static BackButtonOnClickCallback onBackButtonClickedCallback;
    private static POIDetailCallback poiDetailCallback;
    private static TabBarVisibilityUpdateCallback tabBarVisibilityUpdateCallback;
    private static Runnable openShoppingAndDiningCallback;
    private static Runnable openArtAndCultureCallback;
    private static OpenBrowserCallback openBrowserCallback;
    private static Class<? extends Activity> mainActivityClass;
    private static RequestQueue requestQueue;
    private static float fontScale = 1.0f;
    private static String currentGate = null;
    private static boolean requestingFlightStatus = false;
    private static boolean FakeData = false;
    private static MapUISetting mapUISetting;
    private static PMPConfiguration configuration;

    private static Handler myHandle = new Handler();
    //integration test only
    private static Handler integrationHandler = new Handler();
    private static Runnable fireCallback = new Runnable() {
        @Override
        public void run() {
            if (retrieveFlightStatusBackgroundJobCallback != null) {
                Set<String> recordIds = sharedPreferences.getStringSet(MonitoringIDS, null);
                if (recordIds != null && !recordIds.isEmpty()) {
                    Log.d(TAG, "request flight statuses");
                    requestingFlightStatus = true;
                    retrieveFlightStatusBackgroundJobCallback.onRetrieveFlightStatus(recordIds);

                }
            }
            integrationHandler.postDelayed(this, 15 * 1000);
        }
    };

    private static Runnable startCacheAndDownloadJson = new Runnable() {
        @Override
        public void run() {
            PMPIndoorLocationManager.getSharedPMPManager(application).initBeacon();
        }
    };

    private static void copyCache(String folder, File dest) {
        final AssetManager assetManager = application.getAssets();
        try {
            String list[] = assetManager.list(folder);
            if (!TextUtils.isEmpty(FilenameUtils.getExtension(folder))) {
                list = new String[]{folder};
                folder = "";
            }
            for (String file :
                    list) {
                File dir = new File(dest, folder);
                if (!TextUtils.isEmpty(FilenameUtils.getExtension(file))) {

                    if (dir.exists() == false) {
                        if (!dir.mkdirs()) {
                            Log.d(TAG, "Can't create dir");
                        }
                    }

                    File outputFile = new File(dir, file);
//                    Log.d(TAG, "Copy file: " + file + " outputFile:" + outputFile);
                    if (!outputFile.exists()) {
                        try {
                            String filePath = null;
                            if (folder.length() == 0) {
                                filePath = file;
                            } else {
                                filePath = folder + File.separator + file;
                            }
                            InputStream fis = assetManager.open(filePath);
                            FileOutputStream fos = new FileOutputStream(outputFile);
                            IOUtils.copy(fis, fos);
                            fis.close();
                            fos.close();
                        } catch (IOException exception) {
                            exception.printStackTrace();
                        }
                    }

                } else {
                    File newDest = new File(dest, folder + File.separator + file);
                    newDest.mkdirs();
                    Log.d(TAG, "Create folder:" + newDest);
                    copyCache(folder + File.separator + file, dest);
                }

            }
//            Log.d(TAG, "-------");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Applications using this library must registers this callback
     * in their Application onCreate function. Therefore developer must
     * create a custom application which extent android.app.Application and
     * put this method inside onCreate() and pass the application context to
     * it.
     *
     * @param context the application context.  If the it is null,
     *                it will throw a NullPointerException.
     * @throws NullPointerException if the application context is null
     */
    public static void onApplicationCreate(Application context) {
        if (context == null) {
            throw new NullPointerException("application can't be null");
        }

        application = context;
        setUpDefaultUI();
        setUpDefaultConfiguration();

//        context.registerReceiver(new NetworkStateReceiver(),new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        MotionManager.getInstance(context);
        sharedPreferences = application.getSharedPreferences(PREFERENCE_NAME, 0);

        try {
            ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            String libName = bundle.getString("android.app.lib_name");
            System.loadLibrary(libName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        PMPIndoorLocationManager.getSharedPMPManager(context).onCreate(context);

//        if (sharedPreferences.getBoolean("PMPMapSDK_FirstInstall", true))
//        {
//            final String folders[] = new String[] {
//                    "maps.json",
//                    "maps",
//                    "markers",
//                    "poi",
//                    "poi_marker",
//                    "poi_cat",
//                    "promotion",
//                    "tags.json"
//            };
//            final File dir = application.getFilesDir();
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    for (String folder :
//                            folders) {
//                        copyCache(folder, dir);
//                    }
//                    Log.v(TAG,"copyCache done");
//                    myHandle.post(startCacheAndDownloadJson);
//                    sharedPreferences.edit().putBoolean("PMPMapSDK_FirstInstall", false).apply();
//                }
//            }).start();
//        }
        /*
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setFontAttrId(R.attr.fontPath)
                .build()
        );*/


        CoreEngine.getInstance();
        requestQueue = Volley.newRequestQueue(application);
        integrationHandler = new Handler();
        fireCallback.run();

        //---Analytics---
        HashMap<String, Object> param = new HashMap<>();
        param.put("platform", "Android");
        param.put("device_name", "" + android.os.Build.MODEL);
        param.put("device_os_version", "" + android.os.Build.VERSION.RELEASE);
        param.put("time_zone", "" + TimeZone.getDefault().getID());
        //Screen
        WindowManager wm = (WindowManager) getApplication().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        param.put("width", "" + size.x);
        param.put("height", "" + size.y);
        //CPU
        param.put("cpu", "" + AnalyticsHelper.getCpuName());
        //Carrier
        TelephonyManager manager = (TelephonyManager) getApplication().getSystemService(Context.TELEPHONY_SERVICE);
        String carrierName = manager.getNetworkOperatorName();
        param.put("carrier", "" + carrierName);
        param.put("sdk_version", BuildConfig.VERSION_NAME);
        AnalyticsLogger.getInstance().logBackgroundEvent("User Profile", param);
        //---------------
        IntentFilter intentFilter = new IntentFilter("com.cherrypicks.arsignagecamerasdk.analytics.LOGEVENT");
        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String eventName = intent.getStringExtra("event_name");
                Bundle bundle = intent.getBundleExtra("params");
                HashMap<String, Object> params = new HashMap<String, Object>();

                for (String key : bundle.keySet()) {
                    params.put(key, bundle.getString(key));
                }
                AnalyticsLogger.getInstance().logEvent(eventName, params);
            }
        };
        LocalBroadcastManager.getInstance(application.getApplicationContext()).registerReceiver(receiver, intentFilter);

        Boolean isDisability = sharedPreferences.getBoolean("isDisability", false);
        setIsDisability(isDisability);
    }

    private static void setUpDefaultConfiguration() {
        configuration = new PMPConfiguration();
        configuration.setShouldEnableGoogleMap(false);
        configuration.setShouldEnableGPSPositioning(false);
        configuration.setGpsAccuracyThreshold(10);
        configuration.setBleToGpsTimeoutInSec(15);
    }

    private static void setUpDefaultUI(){
        mapUISetting = new MapUISetting();

        mapUISetting.setThemeColor(Color.parseColor("#3D76C0"));
        mapUISetting.setRecentSearchBgColor(Color.parseColor("#2D56B0"));
        mapUISetting.setRecentSearchTextColor(Color.WHITE);
        mapUISetting.setGenericCellTextColor(Color.parseColor("#3769BA"));

        mapUISetting.aroundMeIconImageResource = R.drawable.icon_around_me;
        mapUISetting.navFilterImageResource = R.drawable.icon_filter;
        mapUISetting.navSearchImageResource = R.drawable.btn_header_search;

        mapUISetting.locateMeButtonOnImageResource = R.drawable.btn_location_on;
        mapUISetting.locateMeButtonOffImageResource = R.drawable.btn_location_off;
        mapUISetting.locateMeButtonCompassImageResource = R.drawable.btn_compass_on;
        mapUISetting.indoorButtonImageResource = R.drawable.btn_indoor;
        mapUISetting.outdoorButtonImageResource = R.drawable.btn_outdoor;
        mapUISetting.compassImageResource = R.drawable.btn_compass;
        mapUISetting.ttsButtonOnImageResource = R.drawable.icon_voice_disable_on;
        mapUISetting.ttsButtonOffImageResource = R.drawable.icon_voice_enable_off;
        mapUISetting.startNavButtonImageResource = R.drawable.btn_fab;

        mapUISetting.floorCellOnResource = R.drawable.btn_floor_mid_on;
        mapUISetting.floorCellOffResource = R.drawable.btn_floor_mid_off;
        mapUISetting.floorNameTextColorOn = Color.WHITE;
        mapUISetting.floorNameTextColorOff = Color.parseColor("#444444");
        mapUISetting.floorCellHeight = 56;
        mapUISetting.floorSwitchWidth = 78;
        mapUISetting.numOfVisibleCell = 3.2f;

        mapUISetting.blueDotMode = PMPBlueDotMode_SpotLight;
        mapUISetting.navButtonMode = PMPNavButtonMode_Arrow;
        mapUISetting.navRectButtonTitle = "GO";
        mapUISetting.navRectButtonImageResource = R.drawable.btn_blank;
        mapUISetting.navRectButtonTitleColor = Color.BLACK;

    }

    public static MapUISetting getMapUISetting() {
        return mapUISetting;
    }

    public static void setMapUISetting(MapUISetting mapUISetting) {
        PMPMapSDK.mapUISetting = mapUISetting;
    }

    public static PMPConfiguration getConfiguration() {
        return configuration;
    }

    public static void setConfiguration(PMPConfiguration configuration) {
        PMPMapSDK.configuration = configuration;
    }

    /**
     * Return the ProximityCallback set by application, which mainly used
     * by PMPMap when proximity signal received or user perform actions which
     * associated with the proximity logic inside PMPMap.
     *
     * @return the ProximityCallback set by application     * associated with the proximity logic inside PMPMap.
     */
    public static ProximityCallback getProximityCallback() {
        return proximityCallback;
    }

    /**
     * The ProximityCallback is set to listen proximity signal received or
     * user perform actions which associated with the proximity logic inside PMPMap.
     * For example, user clicks on the Proximity banner in MapView will trigger a callback event.
     *
     * @param proximityCallback the callback set by application to listen proximity related event
     */
    public static void setProximityCallback(ProximityCallback proximityCallback) {
        PMPMapSDK.proximityCallback = proximityCallback;
    }

    public static PromotionCallback getPromotionCallback() {
        return promotionCallback;
    }

    public static void setPromotionCallback(PromotionCallback promotionCallback) {
        PMPMapSDK.promotionCallback = promotionCallback;
    }

    /**
     * Return the LaunchDetailCallback set by application, which mainly used
     * by PMPMap when user perform actions which associated with the POI information inside PMPMap.
     *
     * @return the ProximityCallback set by application
     */
    public static LaunchDetailCallback getLaunchDetailCallback() {
        return launchDetailCallback;
    }

    /**
     * The IndoorLocationUpdateCallback is set to listen indoolocation update
     * which associated with the wayfinding logic inside PMPMap.
     *
     * @param IndoorLocationUpdateCallback the callback set by application to listen wayfinding related event
     */
    public static void setIndoorLocationUpdateCallback(IndoorLocationUpdateCallback indoorLocationUpdateCallback) {
        PMPMapSDK.indoorLocationUpdateCallback = indoorLocationUpdateCallback;
    }

    public static IndoorLocationUpdateCallback getIndoorLocationUpdateCallback() {
        return indoorLocationUpdateCallback;
    }

    /**
     * The LaunchDetailCallback is set to listen user performed actions which associated with the
     * POI information inside PMPMap.
     * For example, user clicks on the POI name label in MapView will trigger a callback event.
     *
     * @param launchDetailCallback the callback set by application to listen POI related event
     */
    public static void setLaunchDetailCallback(LaunchDetailCallback launchDetailCallback) {
        PMPMapSDK.launchDetailCallback = launchDetailCallback;
    }

    /**
     * The AnalyticsEventCallback is set to listen analytics event happened in PMPMap.
     * PMPMap will report different kind of user event to application. For example, user
     * click on a POI, user search for a POI.
     * <p>
     * Application can choose whether log or not to log those event.
     *
     * @param eventCallback the callback set by application to listen POI related event
     */
    public static void setAnalyticsEventCallback(AnalyticsEventCallback eventCallback) {
        if (eventCallback != null) {
            AnalyticsLogger logger = AnalyticsLogger.getInstance();
            logger.setEventCallback(eventCallback);
        }
    }

    /**
     * Start a new PMPMapView with startActivity from original activity.
     * The language or the PMPMapView will be English by default.
     *
     * @param fromActivity the activity which will launch PMPMapAcitivty
     * @throws NullPointerException if the fromActivity is null
     */
    public static void showMapView(Activity fromActivity) {
        showMapView(fromActivity, Language_English, null);
    }

    /**
     * Start a new MapView with startActivity from original activity.
     * It will resume the previous state according to the mapState given by application.
     * It will also set the UI language according to the language input.
     *
     * @param fromActivity the activity which will launch PMPMapAcitivty
     * @param language     the language enum
     * @param mapState     map state of previous map session
     * @throws NullPointerException if the fromActivity is null
     */
    public static void showMapView(Activity fromActivity, int language, MapState mapState) {
        showMapView(fromActivity, language, null, mapState);
    }

    /**
     * Start a new MapView with startActivity from original activity.
     * It will resume the previous state according to the mapState given by application,
     * set the UI language according to the language input and also start a navigation UI
     * to the POI indicated by poiID
     *
     * @param fromActivity the activity which will launch PMPMapAcitivty
     * @param language     the language enum
     * @param mapState     map state of previous map session
     * @param poiID        The id of destination POI
     * @throws NullPointerException if the fromActivity is null
     */

    public static void showMapView(Activity fromActivity, int language, String poiID, MapState mapState) {
        if (fromActivity == null) {
            throw new NullPointerException("fromActivity can't be null");
        }

        //Todo Show Map Fragment instead of Activity

        //Intent goMap = new Intent(fromActivity, PMPMapActivity.class);
        /*
        goMap.putExtra(PMPMapActivity.POI_IDS,  poiID);
        goMap.putExtra(PMPMapActivity.LANG_ID, language);
        if(mapState != null)goMap.putExtra(PMPMapActivity.MAP_STATE, mapState);
        if(poiID != null)goMap.putExtra(PMPMapActivity.POI_IDS, poiID);
        */
        //fromActivity.startActivity(goMap);
    }

    //2.5 Enable / Disable monitor flight status
    public static boolean setEnableMonitoring(boolean enable,
                                              String gid,
                                              String recordId,
                                              String flightNo,
                                              String flightPreferredId,
                                              String flightDate,
                                              String flightTime,
                                              String isArrival,
                                              String userId,
                                              String pushToken,
                                              String platform) {
        Log.i("PMPMapSDK", "setEnableMonitoring: enable = " + enable + ", gid = " + gid + ", recordId = " + recordId
                + ", flightNo = " + flightNo + ", flightDate = " + flightDate + ", flightTime = " + flightTime + ", isArrival = "
                + isArrival + ", userId = " + userId + ", pushToken = " + pushToken + ", platform = " + platform);


        Set<String> recordIds = sharedPreferences.getStringSet(MonitoringIDS, null);
        if (recordIds == null) {
            recordIds = new HashSet<>();
        }
        if (enable) {
            if (!recordIds.contains(recordId)) {
                recordIds.add(recordId);
            }
        } else {
            if (recordIds.contains(recordId)) {
                recordIds.remove(recordId);
            }
        }

        PMPProximityServerManager manager = PMPProximityServerManager.getShared(application.getApplicationContext());

        sharedPreferences.edit().putStringSet(MonitoringIDS, recordIds).apply();
        return manager.saveUserFlight(userId, pushToken, recordId, flightNo, flightPreferredId, flightDate, flightTime, isArrival, enable ? 1 : 0, gid);
    }

    //2.6 Is flight monitoring
    public static boolean isEnableMonitoring(String recordId) {
        Set<String> recordIds = sharedPreferences.getStringSet(MonitoringIDS, null);
        if (recordIds != null) {
            return recordIds.contains(recordId);
        }
        return false;
    }

    //2.7 Enable / Disable notification
    public static void setEnableNotification(boolean enable) {
        String key = "PUSH_NOTIFICATION";
        sharedPreferences.edit().putBoolean(key, enable).apply();
        PMPApplication.getPmpApplication().setEnableKillAppDetection(enable);
    }

    //2.8 Is notification enabled
    public static boolean isEnableNotifiation() {
        String key = "PUSH_NOTIFICATION";
        if (!sharedPreferences.contains(key)) {
            //set it default to true
            setEnableNotification(true);
        }
        return sharedPreferences.getBoolean(key, true);
    }

    //2.16 Click menu button callback
    public static void setOnBackButtonClickedCallback(BackButtonOnClickCallback callback) {
        onBackButtonClickedCallback = callback;
    }

    public static BackButtonOnClickCallback getOnBackButtonClickedCallback() {
        return onBackButtonClickedCallback;
    }

    public static Application getApplication() {
        return application;
    }

    public static void setLangID(int id, final String userId, final String pushToken, String platform) {
        Log.i("PMPMapSDK", "setLangID: id = " + id + ", userId = " + userId + ",pushToken = " + pushToken + ",platform = " + platform);
        langID = id;
        SharedPreferences.Editor editor = PMPUtil.getSharedPreferences(getApplication().getBaseContext()).edit();
        editor.putInt("LangId", langID);
        editor.commit();

        PMPProximityServerManager.getShared(application.getApplicationContext()).saveUserToken(userId, pushToken, langID, new PMPProximityServerManagerNotifier() {
            @Override
            public void didSuccess(Object response) {
                SharedPreferences.Editor editor = PMPUtil.getSharedPreferences(getApplication().getBaseContext()).edit();
                editor.putString("MtelUserId", userId);
                editor.putString("MtelPushToken", pushToken);
                editor.commit();
            }

            @Override
            public void didFailure() {

            }
        });
    }

    public static int getLangID() {
//        return langID;
        return PMPUtil.getSharedPreferences(getApplication().getBaseContext()).getInt("LangId", langID);
    }

    public static void setMainActivityClass(Class<? extends Activity> cls) {
        mainActivityClass = cls;
    }

    public static Class<? extends Activity> getMainActivityClass() {
        return mainActivityClass;
    }

    //2.14 Trigger background job
    public static void setRetrieveFlightStatusBackgroundJobCallback(RetrieveFlightStatusBackgroundJobCallback callback) {
        retrieveFlightStatusBackgroundJobCallback = callback;
    }

    //2.15 Update flight details
    public static void updateFlightDetailsByJSONString(String jsonStr) {
        try {
            List<FlightStatus> flightStatuses = FlightStatusParser.parse(jsonStr);
            FlightStatus firstFlightStatus = flightStatuses.get(0);
            FlightStatus lastFlightStatus = flightStatuses.get(flightStatuses.size() - 1);
            Log.d(TAG, "First: " + firstFlightStatus.getScheduledDate() + " Last:" + lastFlightStatus.getScheduledDate());
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        requestingFlightStatus = false;
    }
    /*
    + (void)getBrandsDataAsync:(PMPJSONResponseBlock)block;
    + (void)getArtAndCultureDataAsync:(PMPJSONResponseBlock)block;
    + (void)getFacilityAndServicesDataAsync:(PMPJSONResponseBlock)block;
    + (void)getPromotionDataAsync:(PMPJSONResponseBlock)block;
    */

    private static String stringFromAssetPath(String path) {
        String str = null;
        try {
            StringBuffer sb = new StringBuffer();
            InputStream is = application.getAssets().open(path);
            BufferedReader in =
                    new BufferedReader(new InputStreamReader(is, "UTF-8"));

            while ((str = in.readLine()) != null) {
                sb.append(str);
            }
            str = sb.toString();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return str;
    }

    public static void getBrandsDataAsync(final JSONResponseCallback callback) {
        /*
        if (FakeData) {
            integrationHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    String json = stringFromAssetPath("brands.json");
                    if (callback != null) {
                        callback.onJSONResponse(json);
                    }
                }
            },1000);
        }else {
            String lang = "en";
            switch (getLangID()) {
                case PMPMapSDK.Language_TraditionalChinese:
                    lang = "zh-hk";
                    break;
                case PMPMapSDK.Language_SimplifiedChinese:
                    lang = "zh-cn";
                    break;
            }
            String url = "https://s3-ap-southeast-1.amazonaws.com/hkia-sit/json/brands_"+lang;//BuildConfig.BaseURL + "api/projects/44/" + lang +"/brands";
            StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    callback.onJSONResponse(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    callback.onJSONResponse(null);
                }
            });
            requestQueue.add(request);
        }*/
        String lang = "en";
        switch (getLangID()) {
            case PMPMapSDK.Language_TraditionalChinese:
                lang = "zh-hk";
                break;
            case PMPMapSDK.Language_SimplifiedChinese:
                lang = "zh-cn";
                break;
            case PMPMapSDK.Language_Japanese:
                lang = "jp";
                break;
            case PMPMapSDK.Language_Korean:
                lang = "ko";
                break;
        }
        PMPServerManager.getShared().getBrandDataAsync(lang, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onJSONResponse(response);
            }
        });

    }

    public static void getArtAndCultureDataAsync(final JSONResponseCallback callback) {

        /*
        if (FakeData) {
            integrationHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    String json = stringFromAssetPath("art_and_culture.json");
                    if (callback != null) {
                        callback.onJSONResponse(json);
                    }
                }
            },1000);
        }else {
            String lang = "en";
            switch (getLangID()) {
                case PMPMapSDK.Language_TraditionalChinese:
                    lang = "zh-hk";
                    break;
                case PMPMapSDK.Language_SimplifiedChinese:
                    lang = "zh-cn";
                    break;
            }

            String url = "https://s3-ap-southeast-1.amazonaws.com/hkia-sit/json/art_and_culture_"+lang;//BuildConfig.BaseURL + "api/v2/projects/44/information/" + lang + "/art_and_culture";
            StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    callback.onJSONResponse(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    callback.onJSONResponse(null);
                }
            });
            requestQueue.add(request);
        }*/
        String lang = "en";
        switch (getLangID()) {
            case PMPMapSDK.Language_TraditionalChinese:
                lang = "zh-hk";
                break;
            case PMPMapSDK.Language_SimplifiedChinese:
                lang = "zh-cn";
                break;
            case PMPMapSDK.Language_Japanese:
                lang = "jp";
                break;
            case PMPMapSDK.Language_Korean:
                lang = "ko";
                break;
        }
        PMPServerManager.getShared().getArtAndCultureDataAsync(lang, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onJSONResponse(response);
            }
        });

    }


//    public static void getFacilityAndServicesDataAsync(final JSONResponseCallback callback) {
//        if (FakeData) {
//            integrationHandler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    String json = stringFromAssetPath("facilities_and_services.json");
//                    if (callback != null) {
//                        callback.onJSONResponse(json);
//                    }
//                }
//            },1000);
//        }else {
//            String lang = "en";
//            switch (getLangID()) {
//                case PMPMapSDK.Language_TraditionalChinese:
//                    lang = "zh-hk";
//                    break;
//                case PMPMapSDK.Language_SimplifiedChinese:
//                    lang = "zh-cn";
//                    break;
//            }
//            String url = BuildConfig.BaseURL + "/api/projects/44/information/" + lang + "/facilities_and_services";
//            StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
//                @Override
//                public void onResponse(String response) {
//                    callback.onJSONResponse(response);
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    callback.onJSONResponse(null);
//                }
//            });
//            requestQueue.add(request);
//        }
//    }

//    public static void getPromotionDataAsync(final JSONResponseCallback callback) {
//        String lang = "en";
//        switch (getLangID()) {
//            case PMPMapSDK.Language_TraditionalChinese:
//                lang = "zh-hk";
//                break;
//            case PMPMapSDK.Language_SimplifiedChinese:
//                lang = "zh-cn";
//                break;
//        }
//        String url = BuildConfig.BaseURL + "api/v1/projects/44/" + lang +"/promotion_banners";
//        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                callback.onJSONResponse(response);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                callback.onJSONResponse(null);
//            }
//        });
//        requestQueue.add(request);
//
//    }

    public static void setOnPOIDetailCallback(POIDetailCallback callback) {
        poiDetailCallback = callback;
    }

    public static void logAnalyticsEvent(String event, Map<String, Object> params) {
        AnalyticsLogger.getInstance().logEvent(event, params);
    }

    public static POIDetailCallback getPoiDetailCallback() {
        return poiDetailCallback;
    }

    public static void setTabBarVisibilityUpdateCallback(TabBarVisibilityUpdateCallback callback) {
        tabBarVisibilityUpdateCallback = callback;
    }

    public static TabBarVisibilityUpdateCallback getTabBarVisibilityUpdateCallback() {
        return tabBarVisibilityUpdateCallback;
    }

    public static void showProximityMessage() {
        //TODO - Send Event to display proximity message
    }

    public static Runnable getOpenArtAndCultureCallback() {
        return openArtAndCultureCallback;
    }

    public static void setOpenArtAndCultureCallback(Runnable openArtAndCultureCallback) {
        PMPMapSDK.openArtAndCultureCallback = openArtAndCultureCallback;
    }

    public static Runnable getOpenShoppingAndDiningCallback() {
        return openShoppingAndDiningCallback;
    }

    public static void setOpenShoppingAndDiningCallback(Runnable openShoppingAndDiningCallback) {
        PMPMapSDK.openShoppingAndDiningCallback = openShoppingAndDiningCallback;
    }

    public static float getFontScale() {
        return fontScale;
    }

    public static void setFontScale(float fontScale) {
        PMPMapSDK.fontScale = fontScale;
    }

    public static boolean isDisability() {
        return CoreEngine.getInstance().isDisability();
    }

    public static void setIsDisability(boolean isDisability) {
        CoreEngine.getInstance().setIsDisability(isDisability);
        sharedPreferences.edit().putBoolean("isDisability", isDisability).commit();
    }

    public static void registerUserId(final String userId, final String pushToken, String platform) {
        registerUserId(userId, pushToken, platform, null);
    }

    public static void registerUserId(final String userId, final String pushToken, String platform, String deviceId) {
        Log.i("PMPMapSDK", "registerUserId: userId = " + userId + ", pushToken = " + pushToken + ", platform = " + platform);
        if (deviceId != null && deviceId.length() > 0) {
            SharedPreferences.Editor editor = PMPUtil.getSharedPreferences(getApplication().getBaseContext()).edit();
            editor.putString("MtelDeviceId", deviceId);
            editor.commit();
        }

        PMPProximityServerManager.getShared(application.getApplicationContext()).saveUserToken(userId, pushToken, langID, new PMPProximityServerManagerNotifier() {
            @Override
            public void didSuccess(Object response) {
                SharedPreferences.Editor editor = PMPUtil.getSharedPreferences(getApplication().getBaseContext()).edit();
                editor.putString("MtelUserId", userId);
                editor.putString("MtelPushToken", pushToken);
                editor.commit();
            }

            @Override
            public void didFailure() {

            }
        });
    }

    public static void setOnOpenBrowserCallback(OpenBrowserCallback openBrowserCallback) {
        PMPMapSDK.openBrowserCallback = openBrowserCallback;
    }

    public static OpenBrowserCallback getOpenBrowserCallback() {
        return openBrowserCallback;
    }

    public static void showNotification(String title, String message, String url) {
        Log.i("PMPMapSDK", "showNotification: title = " + title + ", message = " + message + ", url = " + url);

        String packedMessage = "";
        if (message != null && message.length() > 0) {
            packedMessage = message.replace(" ", "");
        }

        if (PMPMapController.getInstance().isARViewShowing()) {
            SaveUserFlightResponse response = PMPProximityServerManager.getShared().getSaveUserFlightResponse();

            if (response != null &&
                    response.getResult() != null &&
                    response.getResult().getPreferredIdentifier() != null &&
                    response.getResult().getPreferredIdentifier().length() > 0) {
                String flightNo = response.getResult().getPreferredIdentifier().replace(" ", "");
                if (packedMessage.indexOf(flightNo) != -1) {
                    return;
                } else {
                    showNotificationNow(title, message, url);
                }
            } else {
                final String _packedMessage = packedMessage;
                final String _message = message;
                final String _title = title;
                final String _url = url;

                PMPProximityServerManager.getShared().getUserSavedFlight(new PMPProximityServerManagerNotifier() {
                    @Override
                    public void didSuccess(Object response) {
                        if (response != null &&
                                response instanceof SaveUserFlightResponse)

                        {
                            SaveUserFlightResponse flightResponse = (SaveUserFlightResponse) response;
                            if (flightResponse.getResult() != null &&
                                    flightResponse.getResult().getPreferredIdentifier() != null &&
                                    flightResponse.getResult().getPreferredIdentifier().length() > 0) {
                                String flightNo = flightResponse.getResult().getPreferredIdentifier().replace(" ", "");
                                if (_packedMessage.indexOf(flightNo) != -1) {
                                    return;
                                }
                            }
                        }

                        showNotificationNow(_title, _message, _url);
                    }

                    @Override
                    public void didFailure() {
                        showNotificationNow(_title, _message, _url);
                    }
                });
            }
        } else {
            showNotificationNow(title, message, url);
        }
    }

    private static void showNotificationNow(String title, String message, String url) {
        Intent intent = new Intent();
        intent.setAction(BROADCAST_NOTIFICATION_ACTION);
        PMPNotification notification = new PMPNotification(title, message, url);
        intent.putExtra(NOTIFICATION_KEY, notification);
        PMPApplication.getApplication().sendBroadcast(intent);
    }

    /**
     * @param fromPoiId null if start from user location
     * @param toPoiId
     * @return Calculation result
     */
    public static PathCalculationResult getEstimatedTimeAndDistanceFromPoiId(String fromPoiId, String toPoiId) {
        PathCalculationResult result = new PathCalculationResult(0, 0);

        if (!CoreEngine.getInstance().isInitialized()) {
            return result;
        }

        int fromId = 0;
        int toId = 0;
        ArrayList<Pois> pois = PMPServerManager.getShared().getServerResponse().getPois();
        for (Pois poi : pois) {
            if (fromPoiId != null && fromPoiId.equals(poi.getExternalId())) {
                if (poi.getNodeIds() != null && poi.getNodeIds().size() > 0) {
                    fromId = poi.getNodeIds().get(0);
                }
            }

            if (toPoiId != null && toPoiId.equals(poi.getExternalId())) {
                if (poi.getNodeIds() != null && poi.getNodeIds().size() > 0) {
                    toId = poi.getNodeIds().get(0);
                }
            }
        }

        if (fromPoiId == null && CoreEngine.getInstance().getIndoorLocation() != null) {
            fromId = -1;
        }

        if (fromId == 0 || toId == 0) {
            return result;
        }


        return CoreEngine.getInstance().calculateDistanceFromNodeToNode(fromId, toId);
    }

}