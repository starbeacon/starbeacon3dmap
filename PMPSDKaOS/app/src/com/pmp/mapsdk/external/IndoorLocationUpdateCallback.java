package com.pmp.mapsdk.external;

/**
 * This interface consists Indoor Location related callback
 *
 * @author  Alan Chan
 * @since   1.0
 */
public interface IndoorLocationUpdateCallback {

    /**
     * Call when location has been updated by beacon signal
     * @param x
     * @param y
     * @param mapId
     * @param remainingTime
     * @param remainingDistance
     */
    void onIndoorLocationUpdate(int x, int y, int mapId, int remainingTime, int remainingDistance);

}
