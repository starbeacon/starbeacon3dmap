package com.pmp.mapsdk.external;

import java.util.Map;

/**
 * Created by andrew on 13/1/2017.
 */

public class ProximityMessage {
    private String externalId;
    private Map<Integer, String> actionURL;
    private Map<Integer, String> titles;
    private Map<Integer, String> messages;
    private Map<Integer, String> details;
    private String iconURL;
    private boolean showInNotifiationCenter;

    public ProximityMessage(String externalId,
                            Map<Integer, String> actionURL,
                            Map<Integer, String> titles,
                            Map<Integer, String> messages,
                            Map<Integer, String> details,
                            String iconURL,
                            boolean showInNotifiationCenter) {
        this.externalId = externalId;
        this.actionURL = actionURL;
        this.titles = titles;
        this.messages = messages;
        this.details = details;
        this.iconURL = iconURL;
        this.showInNotifiationCenter = showInNotifiationCenter;
    }

    public String getActionUrl(int lang) {return  actionURL.get(lang);}

    public String getTitle(int lang) {
        return titles.get(lang);
    }

    public String getMessage(int lang) {
        return messages.get(lang);
    }

    public String getDetails(int lang) {
        return details.get(lang);
    }

    public String getIconURL() {
        return iconURL;
    }

    public boolean isShowInNotifiationCenter() {
        return showInNotifiationCenter;
    }

    public String getExternalId() {
        return externalId;
    }
}
