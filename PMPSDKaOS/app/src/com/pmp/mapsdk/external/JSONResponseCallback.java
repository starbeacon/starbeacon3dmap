package com.pmp.mapsdk.external;

/**
 * Created by andrew on 3/11/2016.
 */

public interface JSONResponseCallback {
    public void onJSONResponse(String json);
}
