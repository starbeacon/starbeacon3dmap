package com.pmp.mapsdk.external;

import java.io.Serializable;

/**
 * Created by andrewman on 7/3/2017.
 */

public class PMPNotification implements Serializable {
    private String title;
    private String message;
    private String url;

    public PMPNotification(String title, String message, String url) {
        this.title = title;
        this.message = message;
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
