package com.pmp.mapsdk.external;

import com.cherrypicks.pmpmap.datamodel.MapState;

/**
 * This interface consists POI information related callback that will be called by PMPMapView
 *
 * @author  Alan Chan
 * @since   1.0
 */
public interface LaunchDetailCallback {

    /**
     * The method will be called when user request for POI information.
     * For example, user clicks on the POI name label in MapView will trigger this event
     *
     * @param  poiID the ID of POI that user focus on
     * @param mapState current state of the PMPMap
     */
    void onLaunchDetailClicked(String poiID, MapState mapState);

}
