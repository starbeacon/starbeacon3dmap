package com.pmp.mapsdk.external;

/**
 * Created by andrew on 3/11/2016.
 */

public interface POIDetailCallback {
    public void onBookmarkClicked(String poiId);
    public boolean retrievePOIBookmarkStatus(String poiId);
    public void onShareButtonClicked(String poiId, String poiName, String poiDesc, String openingHour);
    public void onBrandShareButtonClicked(String brandId, String brandName);
}
