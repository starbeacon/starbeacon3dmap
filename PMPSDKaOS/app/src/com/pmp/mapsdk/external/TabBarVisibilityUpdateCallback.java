package com.pmp.mapsdk.external;

/**
 * Created by andrew on 13/1/2017.
 */

public interface TabBarVisibilityUpdateCallback {
    public void updateTabBarVisibilityUpdate(boolean visable);
}

