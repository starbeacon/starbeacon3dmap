package com.pmp.mapsdk.external;

import com.pmp.mapsdk.app.PMPMapFragment;

/**
 * Created by andrew on 3/11/2016.
 */

public interface BackButtonOnClickCallback {
    public void onMenuClicked(PMPMapFragment mapFragment);
}
