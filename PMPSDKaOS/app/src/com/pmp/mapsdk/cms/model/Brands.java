package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;

import org.json.*;
import java.util.ArrayList;

public class Brands {

    @SerializedName("id")
    private double id;
    @SerializedName("name")
    private ArrayList<Name> name;
    @SerializedName("eshop_url")
    private ArrayList<EshopUrl> eshopUrl;
    @SerializedName("description")
    private ArrayList<Description> description;
    @SerializedName("poi_category_ids")
    private ArrayList<Integer> poiCategoryIds;
    @SerializedName("external_id")
    private String externalId;
    
	public Brands () {
		
	}	
        
    public Brands (JSONObject json) {

        this.id = json.optDouble("id");

        this.name = new ArrayList<Name>();
        JSONArray arrayName = json.optJSONArray("name");
        
        if (null != arrayName) {
            int nameLength = arrayName.length();
            
            for (int i = 0; i < nameLength; i++) {
                JSONObject item = arrayName.optJSONObject(i);
                
                if (null != item) {
                    this.name.add(new Name(item));
                }
            }
        } else {
            JSONObject item = json.optJSONObject("name");
            
            if (null != item) {
                this.name.add(new Name(item));
            }
        }

        this.eshopUrl = new ArrayList<EshopUrl>();
        JSONArray arrayEshopUrl = json.optJSONArray("eshop_url");

        if (null != arrayEshopUrl) {
            int eshopUrlLength = arrayEshopUrl.length();

            for (int i = 0; i < eshopUrlLength; i++) {
                JSONObject item = arrayEshopUrl.optJSONObject(i);

                if (null != item) {
                    this.eshopUrl.add(new EshopUrl(item));
                }
            }
        } else {
            JSONObject item = json.optJSONObject("eshop_url");

            if (null != item) {
                this.eshopUrl.add(new EshopUrl(item));
            }
        }


        this.description = new ArrayList<Description>();
        JSONArray arrayDescription = json.optJSONArray("description");
        
        if (null != arrayDescription) {
            int descriptionLength = arrayDescription.length();
            
            for (int i = 0; i < descriptionLength; i++) {
                JSONObject item = arrayDescription.optJSONObject(i);
                
                if (null != item) {
                    this.description.add(new Description(item));
                }
            }
        } else {
            JSONObject item = json.optJSONObject("description");
            
            if (null != item) {
                this.description.add(new Description(item));
            }
        }

        this.poiCategoryIds = new ArrayList<Integer>();
        JSONArray arrayCategoryIds = json.optJSONArray("poi_category_ids");

        if (null != arrayCategoryIds) {
            int categoryIdsLength = arrayCategoryIds.length();

            for (int i = 0; i < categoryIdsLength; i++) {
                this.poiCategoryIds.add(arrayCategoryIds.optInt(i, 0));
            }
        }
        this.externalId = json.optString("external_id");
    }

    public double getId() {
        return this.id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public ArrayList<Name> getName() {
        return this.name;
    }

    public void setName(ArrayList<Name> name) {
        this.name = name;
    }

    public ArrayList<EshopUrl> getEshopUrl() {
        return this.eshopUrl;
    }

    public void setEshopUrl(ArrayList<EshopUrl> eshopUrl) {
        this.eshopUrl = eshopUrl;
    }

    public ArrayList<Description> getDescription() {
        return this.description;
    }

    public void setDescription(ArrayList<Description> description) {
        this.description = description;
    }

    public ArrayList<Integer> getPoiCategoryIds() {
        return poiCategoryIds;
    }

    public String getExternalId() {
        return externalId;
    }
}
