package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;

import org.json.*;


public class Paths {

    @SerializedName("computed_distance")
    private double computedDistance;
    @SerializedName("duration_reverse")
    private double durationReverse;
    @SerializedName("id")
    private double id;
    @SerializedName("directed")
    private double directed;
    @SerializedName("out_door")
    private boolean outDoor;
    @SerializedName("path_type_id")
    private double pathTypeId;
    @SerializedName("end_node_id")
    private double endNodeId;
    @SerializedName("computed_duration")
    private double computedDuration;
    @SerializedName("start_node_id")
    private double startNodeId;
    
    
	public Paths () {
		
	}	
        
    public Paths (JSONObject json) {
    
        this.computedDistance = json.optDouble("computed_distance");
        this.durationReverse = json.optDouble("duration_reverse");
        this.id = json.optDouble("id");
        this.directed = json.optDouble("directed");
        this.outDoor = json.optBoolean("out_door");
        this.pathTypeId = json.optDouble("path_type_id");
        this.endNodeId = json.optDouble("end_node_id");
        this.computedDuration = json.optDouble("computed_duration");
        this.startNodeId = json.optDouble("start_node_id");

    }
    
    public double getComputedDistance() {
        return this.computedDistance;
    }

    public void setComputedDistance(double computedDistance) {
        this.computedDistance = computedDistance;
    }

    public double getDurationReverse() {
        return this.durationReverse;
    }

    public void setDurationReverse(double durationReverse) {
        this.durationReverse = durationReverse;
    }

    public double getId() {
        return this.id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public double getDirected() {
        return this.directed;
    }

    public void setDirected(double directed) {
        this.directed = directed;
    }

    public boolean getOutDoor() {
        return this.outDoor;
    }

    public void setOutDoor(boolean outDoor) {
        this.outDoor = outDoor;
    }

    public double getPathTypeId() {
        return this.pathTypeId;
    }

    public void setPathTypeId(double pathTypeId) {
        this.pathTypeId = pathTypeId;
    }

    public double getEndNodeId() {
        return this.endNodeId;
    }

    public void setEndNodeId(double endNodeId) {
        this.endNodeId = endNodeId;
    }

    public double getComputedDuration() {
        return this.computedDuration;
    }

    public void setComputedDuration(double computedDuration) {
        this.computedDuration = computedDuration;
    }

    public double getStartNodeId() {
        return this.startNodeId;
    }

    public void setStartNodeId(double startNodeId) {
        this.startNodeId = startNodeId;
    }


    
}
