package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by andrewman on 20/2/2017.
 */

public class ErrorCode {
    @SerializedName("errorCode")
    private double errorCode;

    public double getErrorCode() {
        return errorCode;
    }
}
