package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;

import org.json.*;
import java.util.ArrayList;

public class Maps {

    @SerializedName("id")
    private double id;
    @SerializedName("path_color")
    private String pathColor;
    @SerializedName("background_color")
    private String backgroundColor;
    @SerializedName("duplicated_tile_ids")
    private ArrayList<String> duplicatedTileIds;
    @SerializedName("default")
    private boolean defaultProperty;
    @SerializedName("width")
    private double width;
    @SerializedName("static_map")
    private boolean staticMap;
    @SerializedName("zoom_min")
    private double zoomMin;
    @SerializedName("zoom_max")
    private double zoomMax;
    @SerializedName("height")
    private double height;
    @SerializedName("tile_size")
    private double tileSize;
    @SerializedName("tile_version")
    private String tileVersion;
    @SerializedName("name")
    private String name;
    @SerializedName("map_images")
    private ArrayList<MapImage> images;
    @SerializedName("default_location_x")
    private float defaultLocationX;
    @SerializedName("default_location_y")
    private float defaultLocationY;
    
	public Maps () {
		
	}	
        
    public Maps (JSONObject json) {
    
        this.id = json.optDouble("id");
        this.pathColor = json.optString("path_color");
        this.backgroundColor = json.optString("background_color");

        this.duplicatedTileIds = new ArrayList<String>();
        JSONArray arrayDuplicatedTileIds = json.optJSONArray("duplicated_tile_ids");
        if (null != arrayDuplicatedTileIds) {
            int duplicatedTileIdsLength = arrayDuplicatedTileIds.length();
            for (int i = 0; i < duplicatedTileIdsLength; i++) {
                String item = arrayDuplicatedTileIds.optString(i);
                if (null != item) {
                    this.duplicatedTileIds.add(item);
                }
            }
        }
        else {
            String item = json.optString("duplicated_tile_ids");
            if (null != item) {
                this.duplicatedTileIds.add(item);
            }
        }

        this.defaultProperty = json.optBoolean("default");
        this.width = json.optDouble("width");
        this.staticMap = json.optBoolean("static_map");
        this.zoomMin = json.optDouble("zoom_min");
        this.zoomMax = json.optDouble("zoom_max");
        this.height = json.optDouble("height");
        this.tileSize = json.optDouble("tile_size");
        this.tileVersion = json.optString("tile_version");
        this.name = json.optString("name");

    }
    
    public double getId() {
        return this.id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getPathColor() {
        return this.pathColor;
    }

    public void setPathColor(String pathColor) {
        this.pathColor = pathColor;
    }

    public String getBackgroundColor() {
        return this.backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public ArrayList<String> getDuplicatedTileIds() {
        return this.duplicatedTileIds;
    }

    public void setDuplicatedTileIds(ArrayList<String> duplicatedTileIds) {
        this.duplicatedTileIds = duplicatedTileIds;
    }

    public boolean getDefaultProperty() {
        return this.defaultProperty;
    }

    public void setDefaultProperty(boolean defaultProperty) {
        this.defaultProperty = defaultProperty;
    }

    public double getWidth() {
        return this.width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public boolean getStaticMap() {
        return this.staticMap;
    }

    public void setStaticMap(boolean staticMap) {
        this.staticMap = staticMap;
    }

    public double getZoomMin() {
        return this.zoomMin;
    }

    public void setZoomMin(double zoomMin) {
        this.zoomMin = zoomMin;
    }

    public double getZoomMax() {
        return this.zoomMax;
    }

    public void setZoomMax(double zoomMax) {
        this.zoomMax = zoomMax;
    }

    public double getHeight() {
        return this.height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getTileSize() {
        return this.tileSize;
    }

    public void setTileSize(double tileSize) {
        this.tileSize = tileSize;
    }

    public String getTileVersion() {
        return this.tileVersion;
    }

    public void setTileVersion(String tileVersion) {
        this.tileVersion = tileVersion;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<MapImage> getImages() {
        return images;
    }

    public float getDefaultLocationX() {
        return defaultLocationX;
    }

    public float getDefaultLocationY() {
        return defaultLocationY;
    }
}
