package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;

import org.json.*;

import java.io.Serializable;
import java.util.ArrayList;

public class PoiCategories implements Serializable {

    @SerializedName("id")
    private double id;
    @SerializedName("is_gate")
    private boolean isGate;
    @SerializedName("name")
    private ArrayList<Name> name;
    @SerializedName("image")
    private String image;
    @SerializedName("has_details")
    private boolean hasDetails;
    @SerializedName("invisible")
    private boolean isInvisible;
    @SerializedName("unsearchable")
    private boolean isUnsearchable;
    @SerializedName("is_start_point")
    private boolean isStartPoint;
    @SerializedName("is_oversea")
    private boolean isOversea;
    @SerializedName("sub_categories")
    private ArrayList<PoiCategories> sub_categories;
    private PoiCategories parentPoiCategory;
    @SerializedName("external_id")
    private String externalId;

	public PoiCategories () {
		
	}	
        
    public PoiCategories (JSONObject json) {
    
        this.id = json.optDouble("id");
        this.isGate = json.optBoolean("is_gate");

        this.name = new ArrayList<Name>();
        JSONArray arrayName = json.optJSONArray("name");
        if (null != arrayName) {
            int nameLength = arrayName.length();
            for (int i = 0; i < nameLength; i++) {
                JSONObject item = arrayName.optJSONObject(i);
                if (null != item) {
                    this.name.add(new Name(item));
                }
            }
        }
        else {
            JSONObject item = json.optJSONObject("name");
            if (null != item) {
                this.name.add(new Name(item));
            }
        }

        this.image = json.optString("image");
        this.hasDetails = json.optBoolean("has_details");
        this.isInvisible = json.optBoolean("invisible");
        this.isUnsearchable = json.optBoolean("unsearchable");
        this.isStartPoint = json.optBoolean("is_start_point");
        this.isOversea = json.optBoolean("is_oversea");

        this.sub_categories = new ArrayList<PoiCategories>();
        JSONArray arrayPoiCategories = json.optJSONArray("sub_categories");
        if (null != arrayPoiCategories) {
            int poiCategoriesLength = arrayPoiCategories.length();
            for (int i = 0; i < poiCategoriesLength; i++) {
                JSONObject item = arrayPoiCategories.optJSONObject(i);
                if (null != item) {
                    PoiCategories subCat = new PoiCategories(item);
                    subCat.parentPoiCategory = this;
                    this.sub_categories.add(subCat);
                }
            }
        }
        else {
            JSONObject item = json.optJSONObject("sub_categories");
            if (null != item) {
                PoiCategories subCat = new PoiCategories(item);
                subCat.parentPoiCategory = this;
                this.sub_categories.add(subCat);
            }
        }
        this.externalId = json.optString("external_id");

    }
    
    public double getId() {
        return this.id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public boolean getIsGate() {
        return this.isGate;
    }

    public void setIsGate(boolean isGate) {
        this.isGate = isGate;
    }

    public ArrayList<Name> getName() {
        return this.name;
    }

    public void setName(ArrayList<Name> name) {
        this.name = name;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isHasDetails() {
        return hasDetails;
    }

    public void setHasDetails(boolean hasDetails) {
        this.hasDetails = hasDetails;
    }

    public boolean isInvisible() {
        return isInvisible;
    }

    public void setInvisible(boolean isInvisible) {
        this.isInvisible = isInvisible;
    }

    public boolean isUnsearchable() {
        return isUnsearchable;
    }

    public void setIsUnsearchable(boolean isUnsearchable) {
        this.isUnsearchable = isUnsearchable;
    }

    public boolean isStartPoint() {
        return isStartPoint;
    }

    public void setIsStartPoint(boolean isStartPoint) {
        this.isStartPoint = isStartPoint;
    }

    public boolean isOversea() {
        return isOversea;
    }

    public void setIsOversea(boolean isOversea) {
        this.isOversea = isOversea;
    }

    public ArrayList<PoiCategories> getSub_categories() {
        return sub_categories;
    }

    public void setSub_categories(ArrayList<PoiCategories> sub_categories) {
        this.sub_categories = sub_categories;
    }

    public PoiCategories getParentPoiCategory() {
        return parentPoiCategory;
    }

    public void setParentPoiCategory(PoiCategories parent) {
        this.parentPoiCategory = parent;
    }

    public String getExternalId() {
        return externalId;
    }

    public boolean isGate() {
        return isGate;
    }
}
