package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;


public class SaveUserLocationResult {

    @SerializedName("promotionDisplayUntilTimestamp")
    private long promotionDisplayUntilTimestamp;


	public SaveUserLocationResult() {

	}

    public long getPromotionDisplayUntilTimestamp() {
        return promotionDisplayUntilTimestamp;
    }
}
