package com.pmp.mapsdk.cms.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class Pois implements Parcelable {

    @SerializedName("map_zoom_min")
    private double mapZoomMin;
    @SerializedName("location")
    private ArrayList<Location> location;
    @SerializedName("not_covered")
    private boolean notCovered;
    @SerializedName("url")
    private String url;
    @SerializedName("tags")
    private ArrayList<Tags> tags;
    @SerializedName("poi_category_ids")
    private ArrayList<Integer> poiCategoryIds;
    @SerializedName("image")
    private String image;
    @SerializedName("telephone")
    private String telephone;
    @SerializedName("pin_image")
    private String pinImage;
    @SerializedName("restricted")
    private boolean restricted;
    @SerializedName("map_id")
    private double mapId;
    @SerializedName("brand_id")
    private double brandId;
    @SerializedName("name")
    private ArrayList<Name> name;
    @SerializedName("business_hours")
    private ArrayList<BusinessHours> businessHours;
    @SerializedName("marker_image")
    private String markerImage;
    @SerializedName("id")
    private double id;
    @SerializedName("x")
    private double x;
    @SerializedName("node_ids")
    private ArrayList<Integer> nodeIds;
    @SerializedName("map_zoom_max")
    private double mapZoomMax;
    @SerializedName("y")
    private double y;
    @SerializedName("email")
    private String email;
    @SerializedName("z")
    private double z;
    @SerializedName("external_id")
    private String externalId;
    @SerializedName("z_index")
    private double zIndex;
    @SerializedName("area_id")
    private double areaId;
    @SerializedName("last_step_message")
    private ArrayList<LastStepMessage> lastStepMessage;
    @SerializedName("description")
    private ArrayList<Description> description;
    @SerializedName("map_lat")
    private double map_lat;
    @SerializedName("map_lng")
    private double map_lng;
    
    
	public Pois () {
		
	}	
        
    public Pois (JSONObject json) {
        this.mapZoomMin = json.optDouble("map_zoom_min");

        this.location = new ArrayList<Location>();
        JSONArray arrayLocation = json.optJSONArray("location");
        if (null != arrayLocation) {
            int locationLength = arrayLocation.length();
            for (int i = 0; i < locationLength; i++) {
                JSONObject item = arrayLocation.optJSONObject(i);
                if (null != item) {
                    this.location.add(new Location(item));
                }
            }
        }
        else {
            JSONObject item = json.optJSONObject("location");
            if (null != item) {
                this.location.add(new Location(item));
            }
        }

        this.notCovered = json.optBoolean("not_covered");
        this.url = json.optString("url");

        this.tags = new ArrayList<Tags>();
        JSONArray arrayTags = json.optJSONArray("tags");
        if (null != arrayTags) {
            int tagsLength = arrayTags.length();
            for (int i = 0; i < tagsLength; i++) {
                JSONObject item = arrayTags.optJSONObject(i);
                if (null != item) {
                    this.tags.add(new Tags(item));
                }
            }
        }
        else {
            JSONObject item = json.optJSONObject("tags");
            if (null != item) {
                this.tags.add(new Tags(item));
            }
        }


        this.poiCategoryIds = new ArrayList<Integer>();
        JSONArray arrayPoiCategoryIds = json.optJSONArray("poi_category_ids");
        if (null != arrayPoiCategoryIds) {
            int poiCategoryIdsLength = arrayPoiCategoryIds.length();
            for (int i = 0; i < poiCategoryIdsLength; i++) {
                int item = arrayPoiCategoryIds.optInt(i);
                this.poiCategoryIds.add(item);

            }
        }
        else {
            int item = json.optInt("poi_category_ids");
            this.poiCategoryIds.add(item);

        }

        this.image = json.optString("image");
        this.telephone = json.optString("telephone");
        this.pinImage = json.optString("pin_image");
        this.restricted = json.optBoolean("restricted");
        this.mapId = json.optDouble("map_id");
        this.brandId = json.optDouble("brand_id");

        this.name = new ArrayList<Name>();
        JSONArray arrayName = json.optJSONArray("name");
        if (null != arrayName) {
            int nameLength = arrayName.length();
            for (int i = 0; i < nameLength; i++) {
                JSONObject item = arrayName.optJSONObject(i);
                if (null != item) {
                    this.name.add(new Name(item));
                }
            }
        }
        else {
            JSONObject item = json.optJSONObject("name");
            if (null != item) {
                this.name.add(new Name(item));
            }
        }

        this.businessHours = new ArrayList<BusinessHours>();
        JSONArray arrayBusinessHours = json.optJSONArray("business_hours");
        if (null != arrayBusinessHours) {
            int businessHoursLength = arrayBusinessHours.length();
            for (int i = 0; i < businessHoursLength; i++) {
                JSONObject item = arrayBusinessHours.optJSONObject(i);
                if (null != item) {
                    this.businessHours.add(new BusinessHours(item));
                }
            }
        }
        else {
            JSONObject item = json.optJSONObject("business_hours");
            if (null != item) {
                this.businessHours.add(new BusinessHours(item));
            }
        }

        this.markerImage = json.optString("marker_image");
        this.id = json.optDouble("id");
        this.x = json.optDouble("x");

        this.nodeIds = new ArrayList<Integer>();
        JSONArray arrayNodeIds = json.optJSONArray("node_ids");
        if (null != arrayNodeIds) {
            int nodeIdsLength = arrayNodeIds.length();
            for (int i = 0; i < nodeIdsLength; i++) {
                int item = arrayNodeIds.optInt(i);
                this.nodeIds.add(item);

            }
        }
        else {
            int item = json.optInt("node_ids");
            this.nodeIds.add(item);

        }

        this.mapZoomMax = json.optDouble("map_zoom_max");
        this.y = json.optDouble("y");
        this.email = json.optString("email");
        this.z = json.optDouble("z");
        this.externalId = json.optString("external_id");
        this.zIndex = json.optDouble("z_index");
        this.areaId = json.optDouble("area_id");

        this.lastStepMessage = new ArrayList<LastStepMessage>();
        JSONArray arrayLastStepMessage = json.optJSONArray("last_step_message");
        
        if (null != arrayLastStepMessage) {
            int lastStepMessageLength = arrayLastStepMessage.length();
            
            for (int i = 0; i < lastStepMessageLength; i++) {
                JSONObject item = arrayLastStepMessage.optJSONObject(i);
                
                if (null != item) {
                    this.lastStepMessage.add(new LastStepMessage(item));
                }
            }
        } else {
            JSONObject item = json.optJSONObject("last_step_message");
            
            if (null != item) {
                this.lastStepMessage.add(new LastStepMessage(item));
            }
        }

        this.mapZoomMin = json.optDouble("map_zoom_min");

        this.description = new ArrayList<Description>();
        JSONArray arrayDescription = json.optJSONArray("description");
        if (null != arrayDescription) {
            int descriptionLength = arrayDescription.length();
            for (int i = 0; i < descriptionLength; i++) {
                JSONObject item = arrayDescription.optJSONObject(i);
                if (null != item) {
                    this.description.add(new Description(item));
                }
            }
        }
        else {
            JSONObject item = json.optJSONObject("description");
            if (null != item) {
                this.description.add(new Description(item));
            }
        }


    }
    
    public double getMapZoomMin() {
        return this.mapZoomMin;
    }

    public void setMapZoomMin(double mapZoomMin) {
        this.mapZoomMin = mapZoomMin;
    }

    public ArrayList<Location> getLocation() {
        return this.location;
    }

    public void setLocation(ArrayList<Location> location) {
        this.location = location;
    }

    public boolean getNotCovered() {
        return this.notCovered;
    }

    public void setNotCovered(boolean notCovered) {
        this.notCovered = notCovered;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ArrayList<Tags> getTags() {
        return this.tags;
    }

    public void setTags(ArrayList<Tags> tags) {
        this.tags = tags;
    }

    public ArrayList<Integer> getPoiCategoryIds() {
        return this.poiCategoryIds;
    }

    public void setPoiCategoryIds(ArrayList<Integer> poiCategoryIds) {
        this.poiCategoryIds = poiCategoryIds;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getPinImage() {
        return this.pinImage;
    }

    public void setPinImage(String pinImage) {
        this.pinImage = pinImage;
    }

    public boolean getRestricted() {
        return this.restricted;
    }

    public void setRestricted(boolean restricted) {
        this.restricted = restricted;
    }

    public double getMapId() {
        return this.mapId;
    }

    public void setMapId(double mapId) {
        this.mapId = mapId;
    }

    public double getBrandId() {
        return this.brandId;
    }

    public void setBrandId(double brandId) {
        this.brandId = brandId;
    }

    public ArrayList<Name> getName() {
        return this.name;
    }

    public void setName(ArrayList<Name> name) {
        this.name = name;
    }

    public ArrayList<BusinessHours> getBusinessHours() {
        return this.businessHours;
    }

    public void setBusinessHours(ArrayList<BusinessHours> businessHours) {
        this.businessHours = businessHours;
    }

    public String getMarkerImage() {
        return this.markerImage;
    }

    public void setMarkerImage(String markerImage) {
        this.markerImage = markerImage;
    }

    public double getId() {
        return this.id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public double getX() {
        return this.x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public ArrayList<Integer> getNodeIds() {
        return this.nodeIds;
    }

    public void setNodeIds(ArrayList<Integer> nodeIds) {
        this.nodeIds = nodeIds;
    }

    public double getMapZoomMax() {
        return this.mapZoomMax;
    }

    public void setMapZoomMax(double mapZoomMax) {
        this.mapZoomMax = mapZoomMax;
    }

    public double getY() {
        return this.y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getZ() {
        return this.z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public String getExternalId() {
        return this.externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public double getZIndex() {
        return this.zIndex;
    }

    public void setZIndex(double zIndex) {
        this.zIndex = zIndex;
    }

    public double getAreaId() {
        return this.areaId;
    }

    public void setAreaId(double areaId) {
        this.areaId = areaId;
    }

    public ArrayList<LastStepMessage> getLastStepMessage() {
        return this.lastStepMessage;
    }

    public void setLastStepMessage(ArrayList<LastStepMessage> lastStepMessage) {
        this.lastStepMessage = lastStepMessage;
    }

    public ArrayList<Description> getDescription() {
        return this.description;
    }

    public void setDescription(ArrayList<Description> description) {
        this.description = description;
    }

    public double getMapLat() {
        return map_lat;
    }

    public double getMapLng() {
        return map_lng;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.mapZoomMin);
        dest.writeList(this.location);
        dest.writeByte(this.notCovered ? (byte) 1 : (byte) 0);
        dest.writeString(this.url);
        dest.writeList(this.tags);
        dest.writeList(this.poiCategoryIds);
        dest.writeString(this.image);
        dest.writeString(this.telephone);
        dest.writeString(this.pinImage);
        dest.writeByte(this.restricted ? (byte) 1 : (byte) 0);
        dest.writeDouble(this.mapId);
        dest.writeDouble(this.brandId);
        dest.writeList(this.name);
        dest.writeList(this.businessHours);
        dest.writeString(this.markerImage);
        dest.writeDouble(this.id);
        dest.writeDouble(this.x);
        dest.writeList(this.nodeIds);
        dest.writeDouble(this.mapZoomMax);
        dest.writeDouble(this.y);
        dest.writeString(this.email);
        dest.writeDouble(this.z);
        dest.writeString(this.externalId);
        dest.writeDouble(this.zIndex);
        dest.writeDouble(this.areaId);
        dest.writeList(this.lastStepMessage);
        dest.writeList(this.description);
    }

    protected Pois(Parcel in) {
        this.mapZoomMin = in.readDouble();
        this.location = new ArrayList<Location>();
        in.readList(this.location, Location.class.getClassLoader());
        this.notCovered = in.readByte() != 0;
        this.url = in.readString();
        this.tags = new ArrayList<Tags>();
        in.readList(this.tags, Tags.class.getClassLoader());
        this.poiCategoryIds = new ArrayList<Integer>();
        in.readList(this.poiCategoryIds, Integer.class.getClassLoader());
        this.image = in.readString();
        this.telephone = in.readString();
        this.pinImage = in.readString();
        this.restricted = in.readByte() != 0;
        this.mapId = in.readDouble();
        this.brandId = in.readDouble();
        this.name = new ArrayList<Name>();
        in.readList(this.name, Name.class.getClassLoader());
        this.businessHours = new ArrayList<BusinessHours>();
        in.readList(this.businessHours, BusinessHours.class.getClassLoader());
        this.markerImage = in.readString();
        this.id = in.readDouble();
        this.x = in.readDouble();
        this.nodeIds = new ArrayList<Integer>();
        in.readList(this.nodeIds, Integer.class.getClassLoader());
        this.mapZoomMax = in.readDouble();
        this.y = in.readDouble();
        this.email = in.readString();
        this.z = in.readDouble();
        this.externalId = in.readString();
        this.zIndex = in.readDouble();
        this.areaId = in.readDouble();
        this.lastStepMessage = new ArrayList<LastStepMessage>();
        in.readList(this.lastStepMessage, LastStepMessage.class.getClassLoader());
        this.description = new ArrayList<Description>();
        in.readList(this.description, Description.class.getClassLoader());
    }

    public static final Parcelable.Creator<Pois> CREATOR = new Parcelable.Creator<Pois>() {
        @Override
        public Pois createFromParcel(Parcel source) {
            return new Pois(source);
        }

        @Override
        public Pois[] newArray(int size) {
            return new Pois[size];
        }
    };

    public String prepareCalling() {
        String str = "";
        if (telephone != null && telephone.length() > 0) {
            String result = "";
            str = telephone.trim();

            for (int i=0; i<str.length(); i++) {
                String examiningStr = str.substring(i, i+1);
                if (i==0) {
                    if ("+0123456789".contains(examiningStr)) {
                        result += examiningStr;
                    } else {
                        break;
                    }
                } else {
                    if (" 0123456789".contains(examiningStr)) {
                        if (!examiningStr.equalsIgnoreCase(" ")) {
                            result += examiningStr;
                        }
                    } else {
                        break;
                    }
                }
            }

            str = result;
        }
        return str;
    }
}
