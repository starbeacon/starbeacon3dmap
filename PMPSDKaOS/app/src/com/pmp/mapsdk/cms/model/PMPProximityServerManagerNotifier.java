package com.pmp.mapsdk.cms.model;

/**
 * Created by Yu on 18/2/16.
 */
public interface PMPProximityServerManagerNotifier {
    void didSuccess(Object response);
    void didFailure();
}
