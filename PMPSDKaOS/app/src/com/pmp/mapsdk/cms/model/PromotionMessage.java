package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Yu on 9/3/2017.
 */

public class PromotionMessage {
    @SerializedName("message")
    private ArrayList<Message> messages;

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }

    @SerializedName("details")
    private ArrayList<Details> details;

    public ArrayList<Details> getDetails() {
        return details;
    }

    public void setDetails(ArrayList<Details> details) {
        this.details = details;
    }

    @SerializedName("action_urls")
    private ArrayList<Message> actionUrls;

    public ArrayList<Message> getActionUrls() {
        return actionUrls;
    }

    public void setActionUrls(ArrayList<Message> actionUrls) {
        this.actionUrls = actionUrls;
    }
}
