package com.pmp.mapsdk.cms.model;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Yu on 16/2/16.
 */
public class ResponseData extends Response {
    public static final int ID_BEACON_PUSH = 2;
    public static final int ID_BEACON_POSITIONING = 1;
    public static final int ID_BEACON_ENTRANCE = 3;

    private ArrayList<Devices> beaconsPush;
    private ArrayList<Devices> beaconsPositioning;
    private Map<Integer, Pois> poisMap;
    private Map<Integer, Nodes> nodesMap;
    private Map<Integer, Devices> deviceMapByMajor;
    private Map<Integer, DeviceGroups> deviceGroupsMap;
    private Map<Integer, Areas> areasMap;
    private ArrayList<PoiCategories> flattenPoiCategories;
    private Map<Integer, PoiCategories> poiCategoryMap = new TreeMap<>();
    private Map<Integer, Brands> brandMap;
    private ArrayList<PoiCategories> transportationList;

    private HashMap<Double, String> uuidsMap;
    private HashMap<Double, Double> mapDeviceTypesMap;

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    private JSONObject jsonObject;

    public ResponseData() {

    }

//    public ResponseData (JSONObject json) {
//        super(json);
//        jsonObject = json;
//        beaconsPush = new ArrayList<Devices>();
//        beaconsPositioning = new ArrayList<Devices>();
//        for (Devices beacon : this.getDevices()) {
//            switch ((int)beacon.getMapDeviceTypeId()) {
//                case ID_BEACON_PUSH:
//                    beaconsPush.add(beacon);
//                    break;
//                default:
//                    beaconsPositioning.add(beacon);
//                    break;
//            }
//        }
//
//        poisMap = new HashMap<Integer, Pois>();
//        if (getPois() != null) {
//            for (Pois poi :
//                    getPois()) {
//                poisMap.put((int)poi.getId(), poi);
//            }
//        }
//
//        nodesMap = new HashMap<Integer, Nodes>();
//        if (getNodes() != null) {
//            for (Nodes node : getNodes()) {
//                nodesMap.put((int)node.getId(), node);
//            }
//        }
//
//        areasMap = new HashMap<>();
//        for (Areas area : getAreas()) {
//            areasMap.put((int) area.getId(), area);
//        }
//
//        deviceMapByMajor = new HashMap<>();
//        for (Devices device :
//                getDevices()) {
//            deviceMapByMajor.put((int) device.getMajorNo(), device);
//        }
//
//        deviceGroupsMap = new HashMap<>();
//        for (DeviceGroups group : getDeviceGroups()) {
//            deviceGroupsMap.put((int) group.getId(), group);
//        }
//
//        flattenPoiCategories = appendFlattenPoiCategories(getPoiCategories(), new ArrayList<PoiCategories>());
//
//        brandMap = new TreeMap<>();
//        for (Brands brand : getBrands()) {
//            brandMap.put((int) brand.getId(), brand);
//        }
//
//        transportationList = new ArrayList<PoiCategories>();
//        for (PoiCategories cat : getPoiCategories()) {
//            if (cat.isInvisible() &&
//                    cat.isUnsearchable() &&
//                    cat.isStartPoint()) {
//                transportationList.add(cat);
//            }
//        }
//    }

    private ArrayList<PoiCategories> appendFlattenPoiCategories(Collection<PoiCategories> cats, ArrayList<PoiCategories> parentPoiCategories) {
        if (cats != null) {
            for (PoiCategories cat :
                    cats) {
                poiCategoryMap.put((int) cat.getId(), cat);
                parentPoiCategories.add(cat);
                assignParentRole(cat);
                appendFlattenPoiCategories(cat.getSub_categories(), parentPoiCategories);
            }
        }
        return parentPoiCategories;
    }

    private void createBeaconsPushAndPositioningIfNeeded() {
        if (beaconsPush == null || beaconsPositioning == null) {
            beaconsPush = new ArrayList<Devices>();
            beaconsPositioning = new ArrayList<Devices>();
            for (Devices beacon : this.getDevices()) {
                switch ((int)beacon.getMapDeviceTypeId()) {
                    case ID_BEACON_PUSH:
                        beaconsPush.add(beacon);
                        break;
                    default:
                        beaconsPositioning.add(beacon);
                        break;
                }
            }
        }
    }

    private void createPoisMapIfNeeded() {
        if (poisMap == null) {
            poisMap = new HashMap<Integer, Pois>();
            if (getPois() != null) {
                for (Pois poi :
                        getPois()) {
                    poisMap.put((int)poi.getId(), poi);
                }
            }
        }
    }

    public Map<Integer, Pois> getPoisMap() {
        createPoisMapIfNeeded();
        return poisMap;
    }

    private void createNodesMapIfNeeded() {
        if (nodesMap == null) {
            nodesMap = new HashMap<Integer, Nodes>();
            if (getNodes() != null) {
                for (Nodes node : getNodes()) {
                    nodesMap.put((int)node.getId(), node);
                }
            }
        }
    }

    public Map<Integer, Nodes> getNodesMap() {
        createNodesMapIfNeeded();
        return nodesMap;
    }

    private void createDeviceMapByMajorIfNeeded() {
        if (deviceMapByMajor == null) {
            deviceMapByMajor = new HashMap<>();
            for (Devices device :
                    getDevices()) {
                deviceMapByMajor.put((int) device.getMajorNo(), device);
            }
        }
    }

    public Map<Integer, Devices> getDeviceMapByMajor() {
        createDeviceMapByMajorIfNeeded();
        return deviceMapByMajor;
    }

    private void createDeviceGroupsMapIfNeeded() {
        if (deviceGroupsMap == null) {
            deviceGroupsMap = new HashMap<>();
            for (DeviceGroups group : getDeviceGroups()) {
                deviceGroupsMap.put((int) group.getId(), group);
            }
        }
    }

    public Map<Integer, DeviceGroups> getDeviceGroupsMap() {
        createDeviceGroupsMapIfNeeded();
        return deviceGroupsMap;
    }

    private void createAreasMapIfNeeded() {
        if (areasMap == null) {
            areasMap = new HashMap<>();
            for (Areas area : getAreas()) {
                areasMap.put((int) area.getId(), area);
            }
        }
    }

    public Map<Integer, Areas> getAreasMap() {
        createAreasMapIfNeeded();
        return areasMap;
    }

//    private void createFlattenPoiCategoriesIfNeeded() {
//        if (flattenPoiCategories == null) {
//            flattenPoiCategories = appendFlattenPoiCategories(getPoiCategories(), new ArrayList<PoiCategories>());
//        }
//    }

    public ArrayList<PoiCategories> getFlattenPoiCategories() {

        return flattenPoiCategories;
    }

    public Map<Integer, PoiCategories> getPoiCategoryMap() {
        return poiCategoryMap;
    }

    private void createBrandMapIfNeeded() {
        if (brandMap == null) {
            brandMap = new TreeMap<>();
            for (Brands brand : getBrands()) {
                brandMap.put((int) brand.getId(), brand);
            }
        }
    }

    public Map<Integer, Brands> getBrandMap() {
        createBrandMapIfNeeded();
        return brandMap;
    }

    private void createTransportationListIfNeeded() {
        transportationList = new ArrayList<PoiCategories>();
        for (PoiCategories cat : getPoiCategories()) {
            if (cat.isInvisible() &&
                    cat.isUnsearchable() &&
                    cat.isStartPoint()) {
                transportationList.add(cat);
            }
        }
    }
    public ArrayList<PoiCategories> getTransportationList() {
        createTransportationListIfNeeded();
        return transportationList;
    }

    @Override
    public ArrayList<PoiCategories> getPoiCategories() {
        return super.getPoiCategories();
    }

    public void build() {
        uuidsMap = new HashMap<>();
        for (Uuids uuid : getUuids()) {
            uuidsMap.put(uuid.getId(), uuid.getContent());
        }

        mapDeviceTypesMap = new HashMap<>();
        ArrayList<MapDeviceTypes> mapDeviceType = getMapDeviceTypes();
        if(mapDeviceType != null)
        for (MapDeviceTypes type : mapDeviceType) {
            mapDeviceTypesMap.put(type.getId(), type.getThreshold());
        }

        ArrayList<Devices> devices = getDevices();
        if(devices != null)
        for (Devices device : devices) {
            device.setUuidMap(uuidsMap);
            device.setDeviceTypes(mapDeviceTypesMap);
            device.setNodes(getNodes());
            device.build();
        }
        flattenPoiCategories = appendFlattenPoiCategories(getPoiCategories(), new ArrayList<PoiCategories>());
    }

    private void assignParentRole(PoiCategories rootCat) {
        if (rootCat.getSub_categories() != null) {
            for (PoiCategories subCat : rootCat.getSub_categories()) {
                subCat.setParentPoiCategory(rootCat);
            }
        }
    }
}
