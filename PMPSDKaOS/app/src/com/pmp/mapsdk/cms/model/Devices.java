package com.pmp.mapsdk.cms.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.pmp.mapsdk.beacon.BaseDevices;
import com.pmp.mapsdk.beacon.BaseNodes;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class Devices extends BaseDevices implements Parcelable {
	@SerializedName("id")
    private double id;
    @SerializedName("x")
    private double x;
    @SerializedName("device_options")
    private DeviceOptions deviceOptions;
    @SerializedName("y")
    private double y;
    @SerializedName("z")
    private double z;
    @SerializedName("ble_zonal_push_uuid_id")
    private double bleZonalPushUuidId;
    @SerializedName("map_device_type_id")
    private double mapDeviceTypeId;
    @SerializedName("device_name")
    private String deviceName;
    @SerializedName("device_group_id")
    private double deviceGroupId;
    @SerializedName("major_no")
    private double majorNo;
    @SerializedName("map_id")
    private double mapId;
    @SerializedName("ble_way_finding_uuid_id")
    private double bleWayFindingUuidId;
    @SerializedName("aa_zone_id")
    private double aaZoneId;
    @SerializedName("zone_id")
    private double zoneId;

    private double status;
    private String bleWayFindingUuid;
    private String bleZonalPushUuid;
    private double threshold;
    //parent
    private HashMap<Double, String> uuidMap;
    private HashMap<Double, Double> deviceTypes;
    private ArrayList<? extends BaseNodes> nodes;

    public Devices () {
		
	}

    public Devices (JSONObject json, HashMap<Double, String> uuidMap, HashMap<Double, Double> deviceTypes, ArrayList<Nodes> nodes) {

        this.id = json.optDouble("id");
        this.x = json.optDouble("x");
        JSONObject deviceOptions = json.optJSONObject("device_options");
        if (null != deviceOptions) {
            this.deviceOptions = new DeviceOptions(json.optJSONObject("device_options"), nodes);
        }
        this.y = json.optDouble("y");
        this.z = json.optDouble("z");
        this.bleZonalPushUuidId = json.optDouble("ble_zonal_push_uuid_id");
        this.mapDeviceTypeId = json.optDouble("map_device_type_id");
        this.deviceName = json.optString("device_name");
        this.deviceGroupId = json.optDouble("device_group_id");
        this.majorNo = json.optDouble("major_no");
        this.mapId = json.optDouble("map_id");
        this.bleWayFindingUuidId = json.optDouble("ble_way_finding_uuid_id");
        this.status = json.optDouble("status");
        this.bleWayFindingUuid = uuidMap.get(this.bleWayFindingUuidId);
        this.bleZonalPushUuid = uuidMap.get(this.bleZonalPushUuidId);
        this.threshold = deviceTypes.get(this.mapDeviceTypeId);

    }
    
    public double getId() {
        return this.id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public double getX() {
        return this.x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public DeviceOptions getDeviceOptions() {
        return this.deviceOptions;
    }

    public void setDeviceOptions(DeviceOptions deviceOptions) {
        this.deviceOptions = deviceOptions;
    }

    public double getY() {
        return this.y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return this.z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public double getBleZonalPushUuidId() {
        return this.bleZonalPushUuidId;
    }

    public void setBleZonalPushUuidId(double bleZonalPushUuidId) {
        this.bleZonalPushUuidId = bleZonalPushUuidId;
    }

    public double getMapDeviceTypeId() {
        return this.mapDeviceTypeId;
    }

    public void setMapDeviceTypeId(double mapDeviceTypeId) {
        this.mapDeviceTypeId = mapDeviceTypeId;
    }

    public String getDeviceName() {
        return this.deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public double getDeviceGroupId() {
        return this.deviceGroupId;
    }

    public void setDeviceGroupId(double deviceGroupId) {
        this.deviceGroupId = deviceGroupId;
    }

    public double getMajorNo() {
        return this.majorNo;
    }

    public void setMajorNo(double majorNo) {
        this.majorNo = majorNo;
    }

    public double getMapId() {
        return this.mapId;
    }

    public void setMapId(double mapId) {
        this.mapId = mapId;
    }

    public double getBleWayFindingUuidId() {
        this.bleWayFindingUuid = uuidMap.get(this.bleWayFindingUuidId);
        return this.bleWayFindingUuidId;
    }

    public void setBleWayFindingUuidId(double bleWayFindingUuidId) {
        this.bleWayFindingUuidId = bleWayFindingUuidId;
    }

    public double getStatus() {
        return this.status;
    }

    public void setStatus(double status) {
        this.status = status;
    }

    public String getBleZonalPushUuid() {
        this.bleZonalPushUuid = uuidMap.get(this.bleZonalPushUuidId);
        return bleZonalPushUuid;
    }

    public String getBleWayFindingUuid() {
        this.bleWayFindingUuid = uuidMap.get(this.bleWayFindingUuidId);
        return bleWayFindingUuid;
    }

    public double getThreshold() {
        this.threshold = deviceTypes.get(this.mapDeviceTypeId);
        return threshold;
    }

    public ArrayList<? extends BaseNodes> getNodes() {
        return nodes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(x);
        dest.writeDouble(y);
        dest.writeDouble(z);
        dest.writeParcelable(deviceOptions, flags);
        dest.writeDouble(bleZonalPushUuidId);
        dest.writeDouble(mapDeviceTypeId);
        dest.writeString(deviceName);
        dest.writeDouble(deviceGroupId);
        dest.writeDouble(majorNo);
        dest.writeDouble(mapId);
        dest.writeDouble(bleWayFindingUuidId);
        dest.writeDouble(status);
        dest.writeString(bleWayFindingUuid);
        dest.writeString(bleZonalPushUuid);
        dest.writeDouble(threshold);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Devices createFromParcel(Parcel in) {
            return new Devices(in);
        }
        public Devices[] newArray(int size) {
            return new Devices[size];
        }
    };

    private Devices(Parcel in) {
        x = in.readDouble();
        y = in.readDouble();
        z = in.readDouble();
        deviceOptions = in.readParcelable(DeviceOptions.class.getClassLoader());
        bleZonalPushUuidId = in.readDouble();
        mapDeviceTypeId = in.readDouble();
        deviceName = in.readString();
        deviceGroupId = in.readDouble();
        majorNo = in.readDouble();
        mapId = in.readDouble();
        bleWayFindingUuidId = in.readDouble();
        status = in.readDouble();
        bleWayFindingUuid = in.readString();
        bleZonalPushUuid = in.readString();
        threshold = in.readDouble();
    }

    public HashMap<Double, String> getUuidMap() {
        return uuidMap;
    }

    public void setUuidMap(HashMap<Double, String> uuidMap) {
        this.uuidMap = uuidMap;
    }

    public HashMap<Double, Double> getDeviceTypes() {
        return deviceTypes;
    }

    public void setDeviceTypes(HashMap<Double, Double> deviceTypes) {
        this.deviceTypes = deviceTypes;
    }


    @Override
    public void setNodes(ArrayList<? extends BaseNodes> nodes) {
        this.nodes = nodes;
    }

    public void build() {
        if (uuidMap != null) {
            this.bleWayFindingUuid = uuidMap.get(this.bleWayFindingUuidId);
            this.bleZonalPushUuid = uuidMap.get(this.bleZonalPushUuidId);
        }

        if (deviceTypes != null) {
            this.threshold = deviceTypes.get(this.mapDeviceTypeId);
        }

        if (deviceOptions != null) {
            deviceOptions.setNodes(nodes);
        }

    }

    public double getAaZoneId() {
        return aaZoneId;
    }

    public double getZoneId() {
        return zoneId;
    }

    public void setAaZoneId(double aaZoneId) {
        this.aaZoneId = aaZoneId;
    }

    public void setZoneId(double zoneId) {
        this.zoneId = zoneId;
    }
}
