package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;

import org.json.*;
import java.util.ArrayList;

public class StepMessages {

    @SerializedName("id")
    private double id;
    @SerializedName("message")
    private ArrayList<Message> message;
    
    
	public StepMessages () {
		
	}	
        
    public StepMessages (JSONObject json) {
    
        this.id = json.optDouble("id");

        this.message = new ArrayList<Message>();
        JSONArray arrayMessage = json.optJSONArray("message");
        if (null != arrayMessage) {
            int messageLength = arrayMessage.length();
            for (int i = 0; i < messageLength; i++) {
                JSONObject item = arrayMessage.optJSONObject(i);
                if (null != item) {
                    this.message.add(new Message(item));
                }
            }
        }
        else {
            JSONObject item = json.optJSONObject("message");
            if (null != item) {
                this.message.add(new Message(item));
            }
        }


    }
    
    public double getId() {
        return this.id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public ArrayList<Message> getMessage() {
        return this.message;
    }

    public void setMessage(ArrayList<Message> message) {
        this.message = message;
    }


    
}
