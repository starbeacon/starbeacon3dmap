package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;

import org.json.*;
import java.util.ArrayList;

public class CustomStepMessages {

    @SerializedName("id")
    private double id;
    @SerializedName("paths")
    private ArrayList<Integer> paths;
    @SerializedName("message")
    private ArrayList<Message> message;
    
    
	public CustomStepMessages () {
		
	}	
        
    public CustomStepMessages (JSONObject json) {
    
        this.id = json.optDouble("id");

        this.paths = new ArrayList<Integer>();
        JSONArray arrayPaths = json.optJSONArray("paths");
        if (null != arrayPaths) {
            int pathsLength = arrayPaths.length();
            for (int i = 0; i < pathsLength; i++) {
                Integer item = arrayPaths.optInt(i);
                if (null != item) {
                    this.paths.add(item);
                }
            }
        }
        else {
            Integer item = json.optInt("paths");
            if (null != item) {
                this.paths.add(item);
            }
        }


        this.message = new ArrayList<Message>();
        JSONArray arrayMessage = json.optJSONArray("message");
        if (null != arrayMessage) {
            int messageLength = arrayMessage.length();
            for (int i = 0; i < messageLength; i++) {
                JSONObject item = arrayMessage.optJSONObject(i);
                if (null != item) {
                    this.message.add(new Message(item));
                }
            }
        }
        else {
            JSONObject item = json.optJSONObject("message");
            if (null != item) {
                this.message.add(new Message(item));
            }
        }


    }
    
    public double getId() {
        return this.id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public ArrayList<Integer> getPaths() {
        return this.paths;
    }

    public void setPaths(ArrayList<Integer> paths) {
        this.paths = paths;
    }

    public ArrayList<Message> getMessage() {
        return this.message;
    }

    public void setMessage(ArrayList<Message> message) {
        this.message = message;
    }


    
}
