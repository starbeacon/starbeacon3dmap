package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;

import org.json.*;


public class Uuids {

    @SerializedName("id")
    private double id;
    @SerializedName("content")
    private String content;
    
    
	public Uuids () {
		
	}	
        
    public Uuids (JSONObject json) {
    
        this.id = json.optDouble("id");
        this.content = json.optString("content");

    }
    
    public double getId() {
        return this.id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    
}
