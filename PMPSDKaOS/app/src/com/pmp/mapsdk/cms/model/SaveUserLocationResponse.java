package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;


public class SaveUserLocationResponse {

    @SerializedName("errorMessage")
    private String errorMessage;
    @SerializedName("errorCode")
    private String errorCode;
    @SerializedName("result")
    private SaveUserLocationResult result;


	public SaveUserLocationResponse() {

	}

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public SaveUserLocationResult getResult() {
        return result;
    }
}
