package com.pmp.mapsdk.cms.model;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by gordonwong on 22/6/2017.
 */

public class Tag {
    @SerializedName("name")
    private String tagType;
    @SerializedName("ids")
    private ArrayList<Integer> idsArray;

    public  Tag(JSONObject jsonObject){
        tagType = jsonObject.optString("name");
        idsArray = new ArrayList<Integer>();
        JSONArray idJsonArray = jsonObject.optJSONArray("ids");
        if(idJsonArray != null){
            for(int i = 0; i < idJsonArray.length(); i++){
                idsArray.add(idJsonArray.optInt(i));
            }
        }
    }

    public String getTagType(){
        return this.tagType;
    }

    public ArrayList<Integer> getIdsArray(){
        return  this.idsArray;
    }
}
