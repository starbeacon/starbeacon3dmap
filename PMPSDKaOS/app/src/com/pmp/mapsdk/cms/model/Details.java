package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;

import org.json.*;


public class Details {

    @SerializedName("language_id")
    private double languageId;
    @SerializedName("content")
    private String content;
    
    
	public Details () {
		
	}	
        
    public Details (JSONObject json) {
    
        this.languageId = json.optDouble("language_id");
        this.content = json.isNull("content")?"" : json.optString("content");

    }
    
    public double getLanguageId() {
        return this.languageId;
    }

    public void setLanguageId(double languageId) {
        this.languageId = languageId;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    
}
