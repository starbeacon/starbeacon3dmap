package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;

import org.json.*;


public class MapDeviceTypes {

    @SerializedName("id")
    private double id;
    @SerializedName("threshold")
    private double threshold;
    
    
	public MapDeviceTypes () {
		
	}	
        
    public MapDeviceTypes (JSONObject json) {
    
        this.id = json.optDouble("id");
        this.threshold = json.optDouble("threshold");

    }
    
    public double getId() {
        return this.id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public double getThreshold() {
        return this.threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }


    
}
