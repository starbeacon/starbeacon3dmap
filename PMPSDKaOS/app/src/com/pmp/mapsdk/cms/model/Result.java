package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;

import org.json.*;

import static com.pmp.mapsdk.utils.PMPUtil.getDate;


public class Result {
    @SerializedName("preferredIdentifier")
    private String preferredIdentifier;
    @SerializedName("gateCode")
    private String gateCode;
    @SerializedName("travelTime")
    private long travelTime;
    @SerializedName("gate")
    private String gate;
    @SerializedName("flightNo")
    private String flightNo;
    @SerializedName("bestOfTime")
    private long bestOfTime;
    @SerializedName("transferDesk")
    private String transferDesk;
    @SerializedName("aisle")
    private String aisle;
    @SerializedName("mtelRecordId")
    private String mtelRecordId;
    @SerializedName("flightStatus")
    private String flightStatus;
    
	public Result () {
		
	}	
        
    public Result (JSONObject json) {
    
        this.preferredIdentifier = json.optString("preferredIdentifier");
        this.gateCode = json.optString("gateCode");
        this.travelTime = json.optLong("travelTime");
        this.gate = json.optString("gate");
        this.flightNo = json.optString("flightNo");
        this.bestOfTime = json.optLong("bestOfTime");
        this.transferDesk = json.optString("transferDesk");
        this.aisle = json.optString("aisle");
        this.flightStatus = json.optString("flightStatus");
    }
    
    public String getPreferredIdentifier() {
        return this.preferredIdentifier;
    }

    public void setPreferredIdentifier(String preferredIdentifier) {
        this.preferredIdentifier = preferredIdentifier;
    }

    public String getGateCode() {
        return this.gateCode;
    }

    public void setGateCode(String gateCode) {
        this.gateCode = gateCode;
    }

    public long getTravelTime() {
        return this.travelTime;
    }

    public void setTravelTime(long travelTime) {
        this.travelTime = travelTime;
    }

    public String getGate() {
        return this.gate;
    }

    public void setGate(String gate) {
        this.gate = gate;
    }

    public String getFlightNo() {
        return this.flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public long getBestOfTime() {
        return this.bestOfTime;
    }

    public void setBestOfTime(long bestOfTime) {
        this.bestOfTime = bestOfTime;
    }

    public String getTransferDesk() {
        return this.transferDesk;
    }

    public void setTransferDesk(String transferDesk) {
        this.transferDesk = transferDesk;
    }

    public String getAisle() {
        return this.aisle;
    }

    public void setAisle(String aisle) {
        this.aisle = aisle;
    }

    public String getMtelRecordId() {
        return mtelRecordId;
    }

    public void setMtelRecordId(String mtelRecordId) {
        this.mtelRecordId = mtelRecordId;
    }

    public String getFlightStatus() {
        return this.flightStatus;
    }

    public void setFlightStatus(String flightStatus) {
        this.flightStatus = flightStatus;
    }

    public String getDepartureTime() {
        if (getBestOfTime() > 0) {
            return getDate(getBestOfTime(), "HH:mm");
        }
        return "";
    }
}
