package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;

import org.json.*;

import java.util.ArrayList;


public class Areas {

    @SerializedName("id")
    private double id;
    @SerializedName("name")
    private ArrayList<Name> name;
    
    
	public Areas () {
		
	}	
        
    public Areas (JSONObject json) {
    
        this.id = json.optDouble("id");
        this.name = new ArrayList<Name>();
        JSONArray arrayName = json.optJSONArray("name");

        if (null != arrayName) {
            int nameLength = arrayName.length();

            for (int i = 0; i < nameLength; i++) {
                JSONObject item = arrayName.optJSONObject(i);

                if (null != item) {
                    this.name.add(new Name(item));
                }
            }
        } else {
            JSONObject item = json.optJSONObject("name");

            if (null != item) {
                this.name.add(new Name(item));
            }
        }

    }
    
    public double getId() {
        return this.id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public ArrayList<Name> getName() {
        return this.name;
    }

    public void setName(ArrayList<Name> name) {
        this.name = name;
    }


    
}
