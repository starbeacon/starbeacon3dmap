package com.pmp.mapsdk.cms.model;

import android.content.IntentFilter;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Promotions {
    public static final int PROMOTION_TYPE_ZONAL = 1;
    public static final int PROMOTION_TYPE_POI = 2;
    public static final int PROMOTION_TYPE_DEVICE = 3;
    public static final int PMPPromotionMessageType_NONE = 0;
    public static final int PMPPromotionMessageType_WelcomeMesaage = 1;
    public static final int PMPPromotionMessageType_WelcomeMesaageSeaToAir = 2;
    public static final int PMPPromotionMessageType_WelcomeMessageTransfer = 3;
    public static final int PMPPromotionMessageType_WelcomeMessageArrival = 4;
    public static final int PMPPromotionMessageType_LastAELNotification = 5;
    public static final int PMPPromotionMessageType_AELnotOperating = 6;

    @SerializedName("end_at")
    private String endAt;
    @SerializedName("start_at")
    private String startAt;
    @SerializedName("id")
    private double id;
    @SerializedName("promotion_type")
    private int promotionType;
    //not to be serialized
    private double referenceId;

    @SerializedName("reference_ids")
    private  ArrayList<Integer> referenceIds;
    @SerializedName("device_group_id")
    private double deviceGroupId;
    @SerializedName("external_id")
    private String externalId;
    @SerializedName("message")
    private ArrayList<Message> message;
    @SerializedName("pin_image")
    private String pinImage;
    @SerializedName("reference_type")
    private String referenceType;
    @SerializedName("banner_icon")
    private String bannerIcon;
    @SerializedName("show_in_notification_center")
    private boolean showInNotificationCenter;
    @SerializedName("message_type")
    private int messageType;
    @SerializedName("device_majors")
    private ArrayList<Integer> deviceMajors;

    @SerializedName("promotion_messages")
    private ArrayList<PromotionMessage> promotionMessages;

    private int receivedMajor;

    public Promotions () {

    }

//    public Promotions (JSONObject json) {
//
//        this.endAt = json.optString("end_at");
//
//        this.details = new ArrayList<Details>();
//        JSONArray arrayDetails = json.optJSONArray("details");
//        if (null != arrayDetails) {
//            int detailsLength = arrayDetails.length();
//            for (int i = 0; i < detailsLength; i++) {
//                JSONObject item = arrayDetails.optJSONObject(i);
//                if (null != item) {
//                    this.details.add(new Details(item));
//                }
//            }
//        }
//        else {
//            JSONObject item = json.optJSONObject("details");
//            if (null != item) {
//                this.details.add(new Details(item));
//            }
//        }
//
//        this.startAt = json.optString("start_at");
//        this.id = json.optDouble("id");
////        this.id = json.optInt("promotion_type");
//        this.referenceId = json.optDouble("reference_id");
//        this.deviceGroupId = json.optDouble("device_group_id");
//        this.externalId = json.optString("external_id");
//        this.promotionType = json.optInt("promotion_type");
//        this.message = new ArrayList<Message>();
//        JSONArray arrayMessage = json.optJSONArray("message");
//        if (null != arrayMessage) {
//            int messageLength = arrayMessage.length();
//            for (int i = 0; i < messageLength; i++) {
//                JSONObject item = arrayMessage.optJSONObject(i);
//                if (null != item) {
//                    this.message.add(new Message(item));
//                }
//            }
//        }
//        else {
//            JSONObject item = json.optJSONObject("message");
//            if (null != item) {
//                this.message.add(new Message(item));
//            }
//        }
//        this.messageType = json.optInt("message_type");
//
//        this.pinImage = json.optString("pin_image");
//        this.referenceType = json.optString("reference_type");
//        this.bannerIcon = json.optString("banner_icon");
//
//    }

    public int getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(int promotionType) {
        this.promotionType = promotionType;
    }

    public String getEndAt() {
        return this.endAt;
    }

    public void setEndAt(String endAt) {
        this.endAt = endAt;
    }

    public String getStartAt() {
        return this.startAt;
    }

    public void setStartAt(String startAt) {
        this.startAt = startAt;
    }

    public double getId() {
        return this.id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public double getReferenceId() {
        return this.referenceId;
    }

    public void setReferenceId(double referenceId) {
        this.referenceId = referenceId;
    }

    public ArrayList<Integer> getReferenceIds(){
        if(this.referenceIds == null) this.referenceIds = new ArrayList<Integer>();
        return this.referenceIds; };

    public void setReferenceIds(ArrayList<Integer> referenceIds){ this.referenceIds = referenceIds; };

    public double getDeviceGroupId() {
        return this.deviceGroupId;
    }

    public void setDeviceGroupId(double deviceGroupId) {
        this.deviceGroupId = deviceGroupId;
    }

    public String getExternalId() {
        return this.externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public ArrayList<Message> getMessage() {
        return this.message;
    }

    public void setMessage(ArrayList<Message> message) {
        this.message = message;
    }

    public String getPinImage() {
        return this.pinImage;
    }

    public void setPinImage(String pinImage) {
        this.pinImage = pinImage;
    }

    public String getReferenceType() {
        return this.referenceType;
    }

    public void setReferenceType(String referenceType) {
        this.referenceType = referenceType;
    }

    public String getBannerIcon() {
        return this.bannerIcon;
    }

    public void setBannerIcon(String bannerIcon) {
        this.bannerIcon = bannerIcon;
    }

    public boolean isShowInNotificationCenter() {
        return showInNotificationCenter;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public int getMessageType() {
        return messageType;
    }

    public ArrayList<PromotionMessage> getPromotionMessages() {
        return promotionMessages;
    }

    public void setPromotionMessages(ArrayList<PromotionMessage> promotionMessages) {
        this.promotionMessages = promotionMessages;
    }

    public ArrayList<Integer> getDeviceMajors() {
        return deviceMajors;
    }

    public void setDeviceMajors(ArrayList<Integer> deviceMajors) {
        this.deviceMajors = deviceMajors;
    }

    public int getReceivedMajor() {
        return receivedMajor;
    }

    public void setReceivedMajor(int receivedMajor) {
        this.receivedMajor = receivedMajor;
    }
}
