package com.pmp.mapsdk.cms.model;

import android.graphics.PointF;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.pmp.mapsdk.beacon.BaseDeviceOptions;
import com.pmp.mapsdk.beacon.BaseNodes;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.json.JSONObject;

import java.util.ArrayList;


public class DeviceOptions extends BaseDeviceOptions implements Parcelable {

    @SerializedName("outdoor_path_ratio")
    private String outdoorPathRatio;
    @SerializedName("nearby_outdoor_nodes")
    private String nearbyOutdoorNodesString;

    private ArrayList<PointF> nearbyOutdoorNodes;
    private ArrayList<? extends BaseNodes> nodes;
    
	public DeviceOptions () {
		
	}	
        
    public DeviceOptions (JSONObject json, ArrayList<Nodes> nodes) {

        this.nearbyOutdoorNodes = new ArrayList<>();
        if (json != null) {
            this.outdoorPathRatio = json.optString("outdoor_path_ratio");
            String nodesStr = json.optString("nearby_outdoor_nodes");
            if (nodesStr != null && !nodesStr.isEmpty() && nodes != null && !nodes.isEmpty()) {
                String[] tmp = nodesStr.replace("[", "").replace("]", "").split(", ");
                if (tmp != null && tmp.length > 0) {
                    for (String nodeId : tmp) {
                        try {
                            final Double nodeIdDouble = Double.valueOf(nodeId);
                            Nodes node = CollectionUtils.find(nodes, new Predicate<Nodes>() {
                                @Override
                                public boolean evaluate(Nodes object) {
                                    return object.getId() == nodeIdDouble;
                                }
                            });
                            if (node != null) {
                                this.nearbyOutdoorNodes.add(new PointF((float)node.getX(), (float)node.getY()));
                            }
                        } catch (NumberFormatException e) {

                        }

                    }
                }
            }
        }
    }
    
    public String getOutdoorPathRatio() {
        return this.outdoorPathRatio;
    }

    public void setOutdoorPathRatio(String outdoorPathRatio) {
        this.outdoorPathRatio = outdoorPathRatio;
    }

    public ArrayList<PointF> getNearbyOutdoorNodes() {
        if (nearbyOutdoorNodes == null && nearbyOutdoorNodesString != null) {
            this.nearbyOutdoorNodes = new ArrayList<>();
            if (nearbyOutdoorNodesString != null && !nearbyOutdoorNodesString.isEmpty() && nearbyOutdoorNodesString != null && !nearbyOutdoorNodesString.isEmpty()) {
                String[] tmp = nearbyOutdoorNodesString.replace("[", "").replace("]", "").split(", ");
                if (tmp != null && tmp.length > 0) {
                    for (String nodeId : tmp) {
                        try {
                            final Double nodeIdDouble = Double.valueOf(nodeId);
                            BaseNodes node = CollectionUtils.find(nodes, new Predicate<BaseNodes>() {
                                @Override
                                public boolean evaluate(BaseNodes object) {
                                    return object.getId() == nodeIdDouble;
                                }
                            });
                            if (node != null) {
                                this.nearbyOutdoorNodes.add(new PointF((float)node.getX(), (float)node.getY()));
                            }
                        } catch (NumberFormatException e) {

                        }

                    }
                }
            }
        }
        return this.nearbyOutdoorNodes;
    }

    public void setNearbyOutdoorNodes(ArrayList<PointF> nearbyOutdoorNodes) {
        this.nearbyOutdoorNodes = nearbyOutdoorNodes;
    }

    public static Creator getCREATOR() {
        return CREATOR;
    }

    public String getNearbyOutdoorNodesString() {
        return nearbyOutdoorNodesString;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(outdoorPathRatio);
        dest.writeTypedList(nearbyOutdoorNodes);
    }
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public DeviceOptions createFromParcel(Parcel in) {
            return new DeviceOptions(in);
        }
        public DeviceOptions[] newArray(int size) {
            return new DeviceOptions[size];
        }
    };
    private DeviceOptions(Parcel in) {
        outdoorPathRatio = in.readString();
        nearbyOutdoorNodes = in.createTypedArrayList(PointF.CREATOR);
    }

    public ArrayList<? extends BaseNodes> getNodes() {
        return nodes;
    }

    @Override
    public void setNodes(ArrayList<? extends BaseNodes> nodes) {
        this.nodes = nodes;
        getNearbyOutdoorNodes();
    }

}
