package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;

import org.json.*;


public class SaveUserFlightResponse {
    @SerializedName("result")
    private Result result;
    @SerializedName("errorMessage")
    private String errorMessage;
    @SerializedName("errorCode")
    private String errorCode;
    
    
	public SaveUserFlightResponse () {
		
	}	
        
    public SaveUserFlightResponse (JSONObject json) {
    
        this.result = new Result(json.optJSONObject("result"));
        this.errorMessage = json.optString("errorMessage");
        this.errorCode = json.optString("errorCode");

    }
    
    public Result getResult() {
        return this.result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }


    
}
