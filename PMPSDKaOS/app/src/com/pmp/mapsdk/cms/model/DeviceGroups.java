package com.pmp.mapsdk.cms.model;

import com.google.gson.annotations.SerializedName;

import org.json.*;


public class DeviceGroups {

    @SerializedName("id")
    private double id;
    @SerializedName("area_id")
    private int areaId;
    
    
	public DeviceGroups () {
		
	}	
        
    public DeviceGroups (JSONObject json) {
    
        this.id = json.optDouble("id");
        this.areaId = json.optInt("area_id");

    }
    
    public double getId() {
        return this.id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public int getAreaId() {
        return this.areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }


    
}
