//
// Created by Alan Chan on 16/2/16.
//
#include <android/log.h>
#include <android/asset_manager_jni.h>
#include <string.h>
#include <stdio.h>
#include <jni.h>

#include "cocos2d.h"
#include "AppDelegate.h"
#include "Utilities.hpp"
#include "LoadingScene.hpp"
#include "CurrentLocationNode.hpp"

#include "MapInfo.hpp"
#include "GeoCoordinate.hpp"
#include "Point.hpp"
#include "Map.hpp"
#include "Edge.hpp"
#include "Vertex.hpp"
#include "dijkstra.h"

#include "VertexMap.hpp"
#include "PathResult.hpp"

#include "BeaconNode.hpp"
#include "CoreEngine.hpp"
#include "MapController.hpp"

#include "LocalizationManager.hpp"

using namespace CPA;

#define WLOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "PMPMapFragment", __VA_ARGS__))
#define WLOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, "PMPMapFragment", __VA_ARGS__))
#define WLOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, "PMPMapFragment", __VA_ARGS__))
#define WLOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, "PMPMapFragment", __VA_ARGS__))

float androidScreenWidth = 0;
float androidScreenHeight = 0;
float androidDensity = 2;

extern "C" {
    JNIEXPORT void Java_com_pmp_mapsdk_app_PMPMapFragment_nativeSetLangId(JNIEnv* env, jobject thiz, int langId, jstring langCode){
        CPA::CoreEngine::getInstance()->setLangId(langId);

        const char *resultCStr = env->GetStringUTFChars(langCode, NULL);
        std::string resultStr(resultCStr);
        env->ReleaseStringUTFChars(langCode,resultCStr);

        CPA::LocalizationManager::getInstance()->setLanguage(resultStr);//jstring2string(env, langCode));
    }
    //com.pmp.mapsdk.app.PMPMapFragmentV;
    JNIEXPORT void Java_com_pmp_mapsdk_app_PMPMapFragment_nativeSetGLFrameSize(JNIEnv* env, jobject thiz, float density, int width, int height) {
        androidScreenWidth = width;
        androidScreenHeight = height;
        androidDensity = density;
    }

    JNIEXPORT jboolean Java_com_pmp_mapsdk_app_PMPMapFragment_nativeShowingARScene(JNIEnv* env, jobject thiz) {
        return (CPA::MapController::getInstance()->getARScene() != NULL);
    }

}