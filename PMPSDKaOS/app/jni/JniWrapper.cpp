//
// Created by Alan Chan on 16/2/16.
//
#include <android/log.h>
#include <android/asset_manager_jni.h>
#include <string.h>
#include <stdio.h>
#include <jni.h>

#include "cocos2d.h"
#include "AppDelegate.h"
#include "Utilities.hpp"
#include "LoadingScene.hpp"
#include "CurrentLocationNode.hpp"

#include "MapInfo.hpp"
#include "GeoCoordinate.hpp"
#include "Point.hpp"
#include "Map.hpp"
#include "Edge.hpp"
#include "Vertex.hpp"
#include "dijkstra.h"

#include "VertexMap.hpp"
#include "PathResult.hpp"

#include "BeaconNode.hpp"
#include "CoreEngine.hpp"

using namespace CPA;

#define WLOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "JniWrapper", __VA_ARGS__))
#define WLOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, "JniWrapper", __VA_ARGS__))
#define WLOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, "JniWrapper", __VA_ARGS__))
#define WLOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, "JniWrapper", __VA_ARGS__))

extern float androidScreenHeight;
extern float androidScreenWidth;
JavaVM *javaVm;

JNIEnv* getJNIEnv(){
    JavaVMAttachArgs args = { JNI_VERSION_1_6, __FUNCTION__, __null };
    JNIEnv* jniEvn = __null;
    int res = javaVm->AttachCurrentThread(&jniEvn, &args);
    return jniEvn;
}

/*
//int node_id;
//ArrayList<Edge> toEdges;
//ArrayList<Edge> fromEdges;
//float x, y, z;
//String floor;//0 - G/F, 1- 1/F
//String floor, ArrayList<Edge> fromEdges,  ArrayList<Edge> toEdges, int node_id, float x, float y, float z
jobject buildJavaVertex(JNIEnv* env, int node_id, jobject toEdges, jobject fromEdges, float x, float y, float z, int mapID){
    jclass vertexClazz = (*env).FindClass("com/cherrypicks/pmpmap/datamodel/Vertex");
    jmethodID constructor = (*env).GetMethodID(vertexClazz, "<init>", "(I)V");
    jobject vertex = (*env).NewObject(vertexClazz, constructor, node_id);

    (*env).CallVoidMethod(vertex, (*env).GetMethodID(vertexClazz, "setFromEdges", "(Ljava/util/ArrayList;)V"), fromEdges);
    (*env).CallVoidMethod(vertex, (*env).GetMethodID(vertexClazz, "setToEdges", "(Ljava/util/ArrayList;)V"), toEdges);
    (*env).CallVoidMethod(vertex, (*env).GetMethodID(vertexClazz, "setX", "(F)V"), x);
    (*env).CallVoidMethod(vertex, (*env).GetMethodID(vertexClazz, "setY", "(F)V"), y);
    (*env).CallVoidMethod(vertex, (*env).GetMethodID(vertexClazz, "setZ", "(F)V"), z);
    (*env).CallVoidMethod(vertex, (*env).GetMethodID(vertexClazz, "setMapId", "(I)V"), mapID);

    env->DeleteLocalRef(vertexClazz);

    return vertex;
}

jobject buildJavaVertex(JNIEnv* env, int node_id){
    jclass vertexClazz = (*env).FindClass("com/cherrypicks/pmpmap/datamodel/Vertex");
    jmethodID constructor = (*env).GetMethodID(vertexClazz, "<init>", "(I)V");
    jobject vertex = (*env).NewObject(vertexClazz, constructor, node_id);
    env->DeleteLocalRef(vertexClazz);

    return vertex;
}

//Vertex toVertex;
//Vertex fromVertex;
//double weight;
//int condition;
//int pathType;
//public Edge(Vertex fv, Vertex tv, double weight, int condition, int pathType)
jobject buildJavaEdge(JNIEnv* env, jobject fv, jobject tv, double weight, double distance, int condition, int pathType){
    jclass edgeClazz = (*env).FindClass("com/cherrypicks/pmpmap/datamodel/Edge");
    jmethodID constructor = (*env).GetMethodID(edgeClazz, "<init>", "(Lcom/cherrypicks/pmpmap/datamodel/Vertex;Lcom/cherrypicks/pmpmap/datamodel/Vertex;DDII)V");
    jobject edge = (*env).NewObject(edgeClazz, constructor, fv, tv, weight, distance, condition, pathType);

    env->DeleteLocalRef(edgeClazz);

    return edge;
}

jobject buildJavaEdge(JNIEnv* env, Edge edge){
    jobject fromVertext = NULL;
    jobject toVertex = NULL;
    if(edge.getFromVertex()){
        fromVertext = buildJavaVertex(env, edge.getFromVertex()->node_id);
    }

    if(edge.getToVertex()){
        toVertex = buildJavaVertex(env, edge.getToVertex()->node_id);
    }

    jobject result = buildJavaEdge(env, fromVertext, toVertex, edge.getWeight(), edge.getDistance(), edge.getCondition(), edge.getPathType());

    env->DeleteLocalRef(fromVertext);
    env->DeleteLocalRef(toVertex);

    return result;
}


jobject buildJavaPathNode(JNIEnv* env, jobject previousVertex, jobject currentVertex, jobject nextVertex){
    jclass pathNodeClazz = (*env).FindClass("com/cherrypicks/pmpmap/datamodel/PathNode");
    jmethodID constructor = (*env).GetMethodID(pathNodeClazz, "<init>", "(Lcom/cherrypicks/pmpmap/datamodel/Vertex;Lcom/cherrypicks/pmpmap/datamodel/Vertex;Lcom/cherrypicks/pmpmap/datamodel/Vertex;)V");
    jobject pathNode = (*env).NewObject(pathNodeClazz, constructor, previousVertex, nextVertex, currentVertex);

    env->DeleteLocalRef(pathNodeClazz);

    return pathNode;
}

//Vertex fromVertex, Vertex toVertex, ArrayList<PathNode> pathNodes, float totalDistance
jobject buildJavaPathResult(JNIEnv* env, jobject fromVertex, jobject toVertex, jobject pathNodes, float totalDistance){
    jclass pathNodeClazz = (*env).FindClass("com/cherrypicks/pmpmap/datamodel/PathResult");
    jmethodID constructor = (*env).GetMethodID(pathNodeClazz, "<init>", "(Lcom/cherrypicks/pmpmap/datamodel/Vertex;Lcom/cherrypicks/pmpmap/datamodel/Vertex;Ljava/util/ArrayList;F)V");
    jobject pathResult = (*env).NewObject(pathNodeClazz, constructor, fromVertex, toVertex, pathNodes, totalDistance);

    env->DeleteLocalRef(pathNodeClazz);

    return pathResult;
}

jobject buildJavaVertexByCVertex(JNIEnv* env, const Vertex *vertex){
    jclass arrayClazz = (*env).FindClass("java/util/ArrayList");

    jobject fromEdge = (*env).NewObject(arrayClazz, (*env).GetMethodID(arrayClazz, "<init>", "()V"));
    jobject toEdge = (*env).NewObject(arrayClazz, (*env).GetMethodID(arrayClazz, "<init>", "()V"));

    std::set<Edge*, Edge::Comparator> cToEdges = vertex->toEdges;
    std::set<Edge*, Edge::Comparator> cFromEdges = vertex->fromEdges;

    for (auto edge : cToEdges) {
        jobject jedge = buildJavaEdge(env, *edge);
        (*env).CallBooleanMethod(toEdge, (*env).GetMethodID(arrayClazz, "add", "(Ljava/lang/Object;)Z"), jedge);
        env->DeleteLocalRef(jedge);
    }

    for (auto edge : cFromEdges) {
        jobject jedge = buildJavaEdge(env, *edge);
        (*env).CallBooleanMethod(fromEdge, (*env).GetMethodID(arrayClazz, "add", "(Ljava/lang/Object;)Z"), jedge);
        env->DeleteLocalRef(jedge);
    }


    jobject result = buildJavaVertex(env, vertex->node_id, toEdge, fromEdge, vertex->x, vertex->y, vertex->z, vertex->mapId);


    env->DeleteLocalRef(fromEdge);
    env->DeleteLocalRef(toEdge);
    env->DeleteLocalRef(arrayClazz);

    return result;
}

jobject buildJavaPathResultByCPathResult(JNIEnv* env, std::vector<PathResult> vec){
    jclass pathResultClazz = (*env).FindClass("com/cherrypicks/pmpmap/datamodel/PathResult");
    jclass arrayClazz = (*env).FindClass("java/util/ArrayList");
    jclass pathNodeClazz = (*env).FindClass("com/cherrypicks/pmpmap/datamodel/PathNode");
    jclass vertexClazz = (*env).FindClass("com/cherrypicks/pmpmap/datamodel/Vertex");

    jobject jArrayPathResults = (*env).NewObject(arrayClazz, (*env).GetMethodID(arrayClazz, "<init>", "()V"));

//    jobject gjArrayPathResults = env->NewGlobalRef(jArrayPathResults);
//    env->DeleteLocalRef(jArrayPathResults);

    for (int n=0;n<vec.size();n++)
    {
        jobject jArrayPathNodes = (*env).NewObject(arrayClazz, (*env).GetMethodID(arrayClazz, "<init>", "()V"));

        PathResult result = vec.at(n);
        std::vector<PathNode> pathNodes = result.pathNodes;
        for (int i=0; i < pathNodes.size(); i++) {
            PathNode pathNode = pathNodes.at(i);

            jobject previousVertex = NULL;
            jobject currentVertex = NULL;
            jobject nextVertex = NULL;
            if(pathNode.previousVertex){
                jobject temp = buildJavaVertexByCVertex(env, pathNode.previousVertex);
                previousVertex = env->NewGlobalRef(temp);
                env->DeleteLocalRef(temp);
            }

            if(pathNode.currentVertex){
                jobject temp = buildJavaVertexByCVertex(env, pathNode.currentVertex);
                currentVertex = env->NewGlobalRef(temp);
                env->DeleteLocalRef(temp);
            }

            if(pathNode.nextVertex){
                jobject temp = buildJavaVertexByCVertex(env, pathNode.nextVertex);
                nextVertex = env->NewGlobalRef(temp);
                env->DeleteLocalRef(temp);
            }

            jobject jPathNode = buildJavaPathNode(env, previousVertex, currentVertex, nextVertex);


            (*env).CallBooleanMethod(jArrayPathNodes, (*env).GetMethodID(arrayClazz, "add", "(Ljava/lang/Object;)Z"), jPathNode);
            env->DeleteGlobalRef(previousVertex);
            env->DeleteGlobalRef(currentVertex);
            env->DeleteGlobalRef(nextVertex);

        }


        jobject fromVertex = NULL;
        jobject toVertex = NULL;
        if(result.fromVertex){
            jobject temp = buildJavaVertexByCVertex(env, result.fromVertex);
            fromVertex = env->NewGlobalRef(temp);
            env->DeleteLocalRef(temp);
        }

        if(result.toVertex){
            jobject temp = buildJavaVertexByCVertex(env, result.toVertex);
            toVertex = env->NewGlobalRef(temp);
            env->DeleteLocalRef(temp);
        }

        jobject jPathResult = buildJavaPathResult(env, fromVertex, toVertex, jArrayPathNodes, result.totalDistance);
        (*env).CallBooleanMethod(jArrayPathResults, (*env).GetMethodID(arrayClazz, "add", "(Ljava/lang/Object;)Z"), jPathResult);

        env->DeleteLocalRef(jArrayPathNodes);
        env->DeleteLocalRef(jPathResult);
        env->DeleteGlobalRef(fromVertex);
        env->DeleteGlobalRef(toVertex);
    }

    return jArrayPathResults;
}*/
extern jobject buildJavaVertex(JNIEnv* env, int node_id, jobject toEdges, jobject fromEdges, float x, float y, float z, int mapID);
extern jobject buildJavaVertex(JNIEnv* env, int node_id);
extern jobject buildJavaEdge(JNIEnv* env, jobject fv, jobject tv, double weight, double distance, int condition, int pathType);
extern jobject buildJavaEdge(JNIEnv* env, Edge edge);
extern jobject buildJavaPathNode(JNIEnv* env, jobject previousVertex, jobject currentVertex, jobject nextVertex);
//Vertex fromVertex, Vertex toVertex, ArrayList<PathNode> pathNodes, float totalDistance
extern jobject buildJavaPathResult(JNIEnv* env, jobject fromVertex, jobject toVertex, jobject pathNodes, float totalDistance);
extern jobject buildJavaVertexByCVertex(JNIEnv* env, const Vertex *vertex);
extern jobject buildJavaPathResultByCPathResult(JNIEnv* env, std::vector<PathResult> vec);

extern "C" {

class CoreEngineCallback : public CPA::CoreEngine::Callback {
public:
    virtual void onEngineInitialDone() {
        char className[] = "com/cherrypicks/pmpmap/PMPMapConfig";
        JNIEnv* jniEvn = getJNIEnv();
        jclass cls = (*jniEvn).FindClass(className);
        jmethodID methodid = (*jniEvn).GetStaticMethodID(cls, "onMapEngineInitialFinish", "()V");
        if(!methodid) {
              return;
        }
        jniEvn->CallStaticVoidMethod(cls, methodid);
    }
};

CoreEngineCallback _coreCallback;

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_init(JNIEnv* env, jobject thiz){
    WLOGI("PMPINIT");
    char className[] = "com/cherrypicks/pmpmap/PMPMapConfig";
    env->GetJavaVM(&javaVm);
    CPA::CoreEngine::getInstance()->setCallback(&_coreCallback);


}

JNIEXPORT jobject Java_com_cherrypicks_pmpmap_PMPMapConfig_searchPathByPOI(JNIEnv* env, jobject thiz, jint fromID, jint poiID, jboolean showPath){
    auto vec = CPA::CoreEngine::getInstance()->searchPathByPOI(fromID, poiID);
    CPA::CoreEngine::getInstance()->setShowCurrentPathResult(showPath);

    return buildJavaPathResultByCPathResult(env, vec);
}

JNIEXPORT jobject Java_com_cherrypicks_pmpmap_PMPMapConfig_searchPossiblePath(JNIEnv* env, jobject thiz, jint fromID, jint toID, jboolean showPath){

    auto vec = CPA::CoreEngine::getInstance()->searchPath(fromID, toID);

    CPA::CoreEngine::getInstance()->setShowCurrentPathResult(showPath);

    return buildJavaPathResultByCPathResult(env, vec);
}

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_startLoadingScene(JNIEnv* env, jobject thiz){

}

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_resetEngine(JNIEnv* env, jobject thiz){
    CPA::CoreEngine::getInstance()->resetEngine();
}

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_showOverViewMode(JNIEnv* env, jobject thiz, jfloat centerX, jfloat centerY, jfloat windowWidth, jfloat windowHeight, jfloat destX, jfloat destY, jboolean switchToUserFloor){
    //CPA::CoreEngine::getInstance()->getMapSceneReference()->showOverViewMode(centerX, centerY, windowWidth, windowHeight, destX, destY, switchToUserFloor);
}

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_setMapControlEnabled(JNIEnv* env, jobject thiz, jboolean shouldEnable){

}

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_changeMapByIndex(JNIEnv* env, jobject thiz, jint index){

}

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_forceSelectPOI(JNIEnv* env, jobject thiz, jfloat windowCenterX, jfloat windowCenterY, jint poiID, jfloat poiX, jfloat poiY, jint poiMapID){
//

}

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_setMapMode(JNIEnv* env, jobject thiz, jint mapMode){
    CPA::CoreEngine* coreEngine = CPA::CoreEngine::getInstance();
        switch (mapMode) {
            case CPA::CoreEngine::MapMode::Browsing:
                coreEngine->setMapMode(CPA::CoreEngine::MapMode::Browsing);
                break;
            case CPA::CoreEngine::MapMode::Navigating:
                coreEngine->setMapMode(CPA::CoreEngine::MapMode::Navigating);
                break;
            case CPA::CoreEngine::MapMode::LocateMe:
                coreEngine->setMapMode(CPA::CoreEngine::MapMode::LocateMe);
                break;
            default:
                break;
        }
}

JNIEXPORT jint Java_com_cherrypicks_pmpmap_PMPMapConfig_getLangId(JNIEnv* env, jobject thiz){
    return CPA::CoreEngine::getInstance()->getLangId();
}

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_nativeParseJson(JNIEnv* env, jobject thiz, jstring json){
     if (!json) {
         return;
     }
     const char *resultCStr = env->GetStringUTFChars(json, NULL);
     std::string resultStr(resultCStr);
     env->ReleaseStringUTFChars(json,resultCStr);

     CPA::DataManager::getInstance()->parseJson(resultStr);
}

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_clearSearhResult(JNIEnv* env, jobject thiz){
    CPA::CoreEngine::getInstance()->clearSearchResult();
}

JNIEXPORT jint Java_com_cherrypicks_pmpmap_PMPMapConfig_getMapMode(JNIEnv* env, jobject thiz){
    return CPA::CoreEngine::getInstance()->getMapMode();
}

JNIEXPORT jfloat Java_com_cherrypicks_pmpmap_PMPMapConfig_getMapScale(JNIEnv* env, jobject thiz){
    return 0;
}

JNIEXPORT jint Java_com_cherrypicks_pmpmap_PMPMapConfig_getCurrentFloor(JNIEnv* env, jobject thiz){
    return 0;
}

JNIEXPORT jfloat Java_com_cherrypicks_pmpmap_PMPMapConfig_getLastSearchTotalDistance(JNIEnv* env, jobject thiz){
    return CPA::CoreEngine::getInstance()->getNavigationTotalDistance();
}

JNIEXPORT jfloat Java_com_cherrypicks_pmpmap_PMPMapConfig_getLastSearchRemainDistanceInMeter(JNIEnv* env, jobject thiz){
    return CPA::CoreEngine::getInstance()->getNavigationDistanceRemaining();
}

JNIEXPORT jfloat Java_com_cherrypicks_pmpmap_PMPMapConfig_getLastSearchTotalTimeInSec(JNIEnv* env, jobject thiz){
    return CPA::CoreEngine::getInstance()->getNavigationTotalTime();
}

JNIEXPORT jfloat Java_com_cherrypicks_pmpmap_PMPMapConfig_getLastSearchRemainTimeInSec(JNIEnv* env, jobject thiz){
    return CPA::CoreEngine::getInstance()->getNavigationTimeRemaining();
}

//onIndoorLocationUpdate(int beaconType, double x, double y, double z, int mapID)
JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_onIndoorLocationUpdate(JNIEnv* env, jobject thiz, jint beaconType, jdouble x, jdouble y, jdouble z, jint mapId){
    CPA::SPLocation loc = CPA::SPLocation(x, y, z, mapId    );
    CPA::CoreEngine::getInstance()->onIndoorLocationUpdate(beaconType, loc);
}

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_onExitIndoor(JNIEnv* env, jobject thiz){
    CPA::CoreEngine::getInstance()->onExitIndoor();
}

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_onCompassUpdate(JNIEnv* env, jobject thiz, jfloat degree){
     CPA::CoreEngine::getInstance()->onCompassUpdate(degree);
}

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_setCompassTopOffsetPx(JNIEnv* env, jobject thiz, jint px){

}

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_setCompassRightOffsetPx(JNIEnv* env, jobject thiz, jint px){

}


/**
 *    Engine Config start here...
 */
JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_setEnableDebug(JNIEnv* env, jobject thiz, jboolean enableDebug){
    CPA::CoreEngine::Config config  = CPA::CoreEngine::getInstance()->getConfig();
        config.debugEnable = enableDebug;
        CPA::CoreEngine::getInstance()->setConfig(config);
}

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_setRerouteThreshold(JNIEnv* env, jobject thiz, jfloat rerouteThreshold){
    CPA::CoreEngine::Config config  = CPA::CoreEngine::getInstance()->getConfig();
    config.rerouteThreshold = rerouteThreshold;
    CPA::CoreEngine::getInstance()->setConfig(config);
}

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_setPassDestinataionThreshold(JNIEnv* env, jobject thiz, jfloat passDestinataionThreshold){
    CPA::CoreEngine::Config config  = CPA::CoreEngine::getInstance()->getConfig();
    config.bypassDestinationThreshold = passDestinataionThreshold;
    CPA::CoreEngine::getInstance()->setConfig(config);
}

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_setDestinationThreshold(JNIEnv* env, jobject thiz, jfloat destinationThreshold){
    CPA::CoreEngine::Config config  = CPA::CoreEngine::getInstance()->getConfig();
    config.destinationThreshold = destinationThreshold;
    CPA::CoreEngine::getInstance()->setConfig(config);
}

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_setBeacons(JNIEnv* env, jobject thiz, jobject arrayOfBeacon){

     jclass arrayClazz = (*env).FindClass("java/util/List");
     jclass pmpBeaconClazz = (*env).FindClass("com/pmp/mapsdk/location/PMPBeacon");
     int size = (*env).CallIntMethod(arrayOfBeacon, (*env).GetMethodID(arrayClazz, "size", "()I"));

     std::vector<CPA::SPBeacon> beacons;
     for (int i = 0; i < size; i++) {
         jobject pmpBeacon = (*env).CallObjectMethod(arrayOfBeacon, (*env).GetMethodID(arrayClazz, "get", "(I)Ljava/lang/Object;"), i);

        // jstring juuid = (jstring)(*env).CallObjectMethod(pmpBeacon, (*env).GetMethodID(pmpBeaconClazz, "getUuid", "()Ljava/lang/String;"));
        // if(juuid == NULL){
              // Error
          //  continue;
        // }
         //const char *resultCStr = env->GetStringUTFChars(juuid, NULL);
         //std::string resultStr(resultCStr);
         //env->ReleaseStringUTFChars(juuid,resultCStr);

         CPA::SPBeacon beacon;
         beacon.proximityUUID = "";
         beacon.major = (*env).CallIntMethod(pmpBeacon, (*env).GetMethodID(pmpBeaconClazz, "getMajorIntValue", "()I"));
         beacon.rssi = (*env).CallIntMethod(pmpBeacon, (*env).GetMethodID(pmpBeaconClazz, "getRssiIntValue", "()I"));
         beacon.accuracy = (*env).CallDoubleMethod(pmpBeacon, (*env).GetMethodID(pmpBeaconClazz, "getDistance", "()D"));
         beacon.usingForTrilateration = (*env).CallBooleanMethod(pmpBeacon, (*env).GetMethodID(pmpBeaconClazz, "getUsingForTrilateration", "()Z"));

         beacons.push_back(beacon);

     }
    CPA::CoreEngine::getInstance()->setBeacons(beacons);
}

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_hideOverViewMode(JNIEnv* env, jobject thiz) {
    //CPA::CoreEngine::getInstance()->getMapSceneReference()->hideOverViewMode();
}

JNIEXPORT jobject Java_com_cherrypicks_pmpmap_PMPMapConfig_getMapConfig(JNIEnv* env, jobject thiz){
     jclass clazz = (*env).FindClass("com/cherrypicks/pmpmap/datamodel/NativeMapConfig");
     jobject jConfig = (*env).NewObject(clazz, (*env).GetMethodID(clazz, "<init>", "()V"));

              CPA::CoreEngine::Config config  = CPA::CoreEngine::getInstance()->getConfig();

              (*env).CallVoidMethod(jConfig, (*env).GetMethodID(clazz, "setDestinationThreshold", "(F)V"), config.destinationThreshold);
              (*env).CallVoidMethod(jConfig, (*env).GetMethodID(clazz, "setRerouteThreshold", "(F)V"), config.rerouteThreshold);
              (*env).CallVoidMethod(jConfig, (*env).GetMethodID(clazz, "setPassDestinationAlertThreadhols", "(F)V"), config.bypassDestinationThreshold);
              (*env).CallVoidMethod(jConfig, (*env).GetMethodID(clazz, "setDebugEnable", "(Z)V"), config.debugEnable);
              (*env).CallVoidMethod(jConfig, (*env).GetMethodID(clazz, "setNavigationRecenterDelta", "(F)V"), config.navigationRecenterDelta);
              (*env).CallVoidMethod(jConfig, (*env).GetMethodID(clazz, "setNavigationRecenterWaitingTime", "(F)V"), config.navigationRecenterWaitingTime);


     return jConfig;
 }

 JNIEXPORT jboolean Java_com_cherrypicks_pmpmap_PMPMapConfig_isEnableOverViewMode(JNIEnv* env, jobject thiz){
      return CPA::CoreEngine::getInstance()->isEnableOverViewMode();
}


JNIEXPORT jobject Java_com_cherrypicks_pmpmap_PMPMapConfig_setCurrentPathResult(JNIEnv* env, jobject thiz, jint index){
    CPA::CoreEngine::getInstance()->setCurrentPathResult(index);
}

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_setLocateMeButtonContentInset(JNIEnv* env, jobject thiz, float left, float bottom){

}

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_setFloorControlContentInset(JNIEnv* env, jobject thiz, float top, float bottom, float left, float right){

}

JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapConfig_setWalkingDistanceBuffer(JNIEnv* env, jobject thiz, float buffer){
    CPA::CoreEngine::Config config  = CPA::CoreEngine::getInstance()->getConfig();
    config.walkingDistanceBufferRatio = buffer;
    CPA::CoreEngine::getInstance()->setConfig(config);
}

JNIEXPORT void Java_com_pmp_mapsdk_app_PMPMapActivity_nativeSetLangId(JNIEnv* env, jobject thiz, int langId){
    CPA::CoreEngine::getInstance()->setLangId(langId);
}

JNIEXPORT void Java_com_pmp_mapsdk_app_PMPMapActivity_nativeUpdateMapState(JNIEnv* env, jobject thiz){
//    CPA::MapScene* mapScene = CPA::CoreEngine::getInstance()->getMapSceneReference();
//
//    if(mapScene) {
//        jclass mapStateClazz = (*env).FindClass("com/cherrypicks/pmpmap/datamodel/MapState");
//        jfieldID f = env->GetFieldID(env->GetObjectClass(thiz), "mapState", "Lcom/cherrypicks/pmpmap/datamodel/MapState;");
//
//        jobject jmapState = env->GetObjectField(thiz, f);
//        CPA::GestureNode::Coord coord = mapScene->getMapCoord();
//
//        jobject jpoi = env->GetObjectField(thiz, env->GetFieldID(env->GetObjectClass(thiz), "selectedPOI", "Lcom/pmp/mapsdk/cms/model/Pois;"));
//        int poiId = 0;
//        if(jpoi) {
//            poiId = (int)(*env).CallDoubleMethod(jpoi, (*env).GetMethodID(env->GetObjectClass(jpoi), "getId", "()D"));
//        }
//        bool indoor = env->GetBooleanField(thiz, env->GetFieldID(env->GetObjectClass(thiz), "isIndoorMode", "Z"));
//
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapCoordPivotX", "(F)V"), coord.pivot.x);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapCoordPivotY", "(F)V"), coord.pivot.y);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapCoordPositionX", "(F)V"), coord.position.x);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapCoordPositionY", "(F)V"), coord.position.y);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapCoordScale", "(F)V"), coord.scale);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapCoordTilt", "(F)V"), coord.tilt);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapCoordRotation", "(F)V"), coord.rotation);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setSelectedPOIId", "(I)V"), poiId);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setIndoor", "(Z)V"), indoor);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapId", "(I)V"), mapScene->getCurrentFloorId());
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapMode", "(I)V"), (int)CPA::CoreEngine::getInstance()->getMapMode());
//
//    }

}

JNIEXPORT void Java_com_pmp_mapsdk_app_PMPMapActivity_nativeSetMapState(JNIEnv* env, jobject thiz, jobject jmapState){
//
//        CPA::MapScene* mapScene = CPA::CoreEngine::getInstance()->getMapSceneReference();
//        CPA::GestureNode::Coord coord;
//        coord.pivot.x = env->CallFloatMethod(jmapState, env->GetMethodID(env->GetObjectClass(jmapState), "getMapCoordPivotX", "()F"));
//        coord.pivot.y = env->CallFloatMethod(jmapState, env->GetMethodID(env->GetObjectClass(jmapState), "getMapCoordPivotY", "()F"));;
//        coord.position.x = env->CallFloatMethod(jmapState, env->GetMethodID(env->GetObjectClass(jmapState), "getMapCoordPositionX", "()F"));
//        coord.position.y = env->CallFloatMethod(jmapState, env->GetMethodID(env->GetObjectClass(jmapState), "getMapCoordPositionY", "()F"));
//        coord.scale = env->CallFloatMethod(jmapState, env->GetMethodID(env->GetObjectClass(jmapState), "getMapCoordScale", "()F"));
//        coord.tilt = env->CallFloatMethod(jmapState, env->GetMethodID(env->GetObjectClass(jmapState), "getMapCoordTilt", "()F"));
//        coord.rotation = env->CallFloatMethod(jmapState, env->GetMethodID(env->GetObjectClass(jmapState), "getMapCoordRotation", "()F"));
//        mapScene->setMapCoord(coord);
//
//        mapScene->setSelectedPOIId(env->CallIntMethod(jmapState, env->GetMethodID(env->GetObjectClass(jmapState), "getSelectedPOIId", "()I")));
//        mapScene->setCurrentFloorById(env->CallIntMethod(jmapState, env->GetMethodID(env->GetObjectClass(jmapState), "getMapId", "()I")));
//        CPA::CoreEngine::getInstance()->setMapMode((CPA::CoreEngine::MapMode)env->CallIntMethod(jmapState, env->GetMethodID(env->GetObjectClass(jmapState), "getMapMode", "()I")));
}

JNIEXPORT void Java_com_pmp_mapsdk_app_PMPMapActivity_nativeSetScreenInfo(JNIEnv* env, jobject thiz, float density, int width, int height) {
    androidScreenWidth = width / density;
    androidScreenHeight = height / density;
}

//----
//JNIEXPORT void Java_com_pmp_mapsdk_app_PMPMapFragment_nativeSetLangId(JNIEnv* env, jobject thiz, int langId){
//    CPA::CoreEngine::getInstance()->setLangId(langId);
//}

JNIEXPORT void Java_com_pmp_mapsdk_app_PMPMapFragment_nativeUpdateMapState(JNIEnv* env, jobject thiz){
//    CPA::MapScene* mapScene = CPA::CoreEngine::getInstance()->getMapSceneReference();
//
//    if(mapScene) {
//        jclass mapStateClazz = (*env).FindClass("com/cherrypicks/pmpmap/datamodel/MapState");
//        jfieldID f = env->GetFieldID(env->GetObjectClass(thiz), "mapState", "Lcom/cherrypicks/pmpmap/datamodel/MapState;");
//
//        jobject jmapState = env->GetObjectField(thiz, f);
//        CPA::GestureNode::Coord coord = mapScene->getMapCoord();
//
//        jobject jpoi = env->GetObjectField(thiz, env->GetFieldID(env->GetObjectClass(thiz), "selectedPOI", "Lcom/pmp/mapsdk/cms/model/Pois;"));
//        int poiId = 0;
//        if(jpoi) {
//            poiId = (int)(*env).CallDoubleMethod(jpoi, (*env).GetMethodID(env->GetObjectClass(jpoi), "getId", "()D"));
//        }
//        bool indoor = env->GetBooleanField(thiz, env->GetFieldID(env->GetObjectClass(thiz), "isIndoorMode", "Z"));
//
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapCoordPivotX", "(F)V"), coord.pivot.x);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapCoordPivotY", "(F)V"), coord.pivot.y);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapCoordPositionX", "(F)V"), coord.position.x);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapCoordPositionY", "(F)V"), coord.position.y);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapCoordScale", "(F)V"), coord.scale);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapCoordTilt", "(F)V"), coord.tilt);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapCoordRotation", "(F)V"), coord.rotation);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setSelectedPOIId", "(I)V"), poiId);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setIndoor", "(Z)V"), indoor);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapId", "(I)V"), mapScene->getCurrentFloorId());
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapMode", "(I)V"), (int)CPA::CoreEngine::getInstance()->getMapMode());
//
//    }

}

JNIEXPORT void Java_com_pmp_mapsdk_app_PMPMapFragment_nativeSetMapState(JNIEnv* env, jobject thiz, jobject jmapState){
//
//        CPA::MapScene* mapScene = CPA::CoreEngine::getInstance()->getMapSceneReference();
//        CPA::GestureNode::Coord coord;
//        coord.pivot.x = env->CallFloatMethod(jmapState, env->GetMethodID(env->GetObjectClass(jmapState), "getMapCoordPivotX", "()F"));
//        coord.pivot.y = env->CallFloatMethod(jmapState, env->GetMethodID(env->GetObjectClass(jmapState), "getMapCoordPivotY", "()F"));;
//        coord.position.x = env->CallFloatMethod(jmapState, env->GetMethodID(env->GetObjectClass(jmapState), "getMapCoordPositionX", "()F"));
//        coord.position.y = env->CallFloatMethod(jmapState, env->GetMethodID(env->GetObjectClass(jmapState), "getMapCoordPositionY", "()F"));
//        coord.scale = env->CallFloatMethod(jmapState, env->GetMethodID(env->GetObjectClass(jmapState), "getMapCoordScale", "()F"));
//        coord.tilt = env->CallFloatMethod(jmapState, env->GetMethodID(env->GetObjectClass(jmapState), "getMapCoordTilt", "()F"));
//        coord.rotation = env->CallFloatMethod(jmapState, env->GetMethodID(env->GetObjectClass(jmapState), "getMapCoordRotation", "()F"));
//        mapScene->setMapCoord(coord);
//
//        mapScene->setSelectedPOIId(env->CallIntMethod(jmapState, env->GetMethodID(env->GetObjectClass(jmapState), "getSelectedPOIId", "()I")));
//        mapScene->setCurrentFloorById(env->CallIntMethod(jmapState, env->GetMethodID(env->GetObjectClass(jmapState), "getMapId", "()I")));
//        CPA::CoreEngine::getInstance()->setMapMode((CPA::CoreEngine::MapMode)env->CallIntMethod(jmapState, env->GetMethodID(env->GetObjectClass(jmapState), "getMapMode", "()I")));
}

JNIEXPORT void Java_com_pmp_mapsdk_app_PMPMapFragment_nativeSetScreenInfo(JNIEnv* env, jobject thiz, float density, int width, int height) {
    androidScreenWidth = width / density;
    androidScreenHeight = height / density;
}

JNIEXPORT void Java_com_cherrypicks_pmpmap_analytics_AnalyticsLogger_nativeUpdateMapState(JNIEnv* env, jobject thiz){
//    CPA::MapScene* mapScene = CPA::CoreEngine::getInstance()->getMapSceneReference();
//
//    if(mapScene) {
//        jclass mapStateClazz = (*env).FindClass("com/cherrypicks/pmpmap/datamodel/MapState");
//        jfieldID f = env->GetFieldID(env->GetObjectClass(thiz), "mapState", "Lcom/cherrypicks/pmpmap/datamodel/MapState;");
//
//        jobject jmapState = env->GetObjectField(thiz, f);
//        CPA::GestureNode::Coord coord = mapScene->getMapCoord();
//
///*
//        jobject jpoi = env->GetObjectField(thiz, env->GetFieldID(env->GetObjectClass(thiz), "selectedPOI", "Lcom/pmp/mapsdk/cms/model/Pois;"));
//        int poiId = 0;
//        if(jpoi) {
//            poiId = (int)(*env).CallDoubleMethod(jpoi, (*env).GetMethodID(env->GetObjectClass(jpoi), "getId", "()D"));
//        }
//        */
//        //bool indoor = env->GetBooleanField(thiz, env->GetFieldID(env->GetObjectClass(thiz), "isIndoorMode", "Z"));
//
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapCoordPivotX", "(F)V"), coord.pivot.x);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapCoordPivotY", "(F)V"), coord.pivot.y);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapCoordPositionX", "(F)V"), coord.position.x);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapCoordPositionY", "(F)V"), coord.position.y);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapCoordScale", "(F)V"), coord.scale);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapCoordTilt", "(F)V"), coord.tilt);
//       // (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setSelectedPOIId", "(I)V"), poiId);
//        //(*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setIndoor", "(Z)V"), indoor);
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapId", "(I)V"), mapScene->getCurrentFloorId());
//        (*env).CallVoidMethod(jmapState, (*env).GetMethodID(mapStateClazz, "setMapMode", "(I)V"), (int)CPA::CoreEngine::getInstance()->getMapMode());
//
//    }

}

JNIEXPORT jint Java_com_cherrypicks_pmpmap_PMPMapConfig_pmpGetCurrentNavitaionStep(JNIEnv* env, jobject thiz){
    return (jint)CPA::CoreEngine::getInstance()->getCurrentNavitaionStep();
}


}