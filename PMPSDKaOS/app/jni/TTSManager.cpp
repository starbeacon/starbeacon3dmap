//
// Created by gordonwong on 18/4/2017.
//
#include <android/asset_manager_jni.h>
#include <string.h>
#include <stdio.h>
#include <jni.h>

#include "cocos2d.h"

#include "CoreEngine.hpp"

#include "platform/android/jni/JniHelper.h"



using namespace CPA;
#define WLOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, "JNITTSManager", __VA_ARGS__))

extern "C" {

    class AndroidTTSManagerBridge : public CPA::TTSManagerBridge {
    private:
        JNIEnv *env;
        jobject jlistener;
        jclass jlistenerClass;
        jmethodID onTTSMessageReceivedMethodID;
        jmethodID getLanguageMethodID;


    public:
        AndroidTTSManagerBridge(JNIEnv *env, jobject jlistener) {
            this->env = env;
            this->jlistener = env->NewGlobalRef(jlistener);
            jlistenerClass = (*env).FindClass("com/cherrypicks/pmpmap/tts/AndroidTTSManagerBridge");
            //jlistenerClass = env->NewGlobalRef(jlistenerClass);
            if (jlistenerClass == NULL) {
            }
            onTTSMessageReceivedMethodID = (*env).GetMethodID(jlistenerClass, "onTTSMessageReceived",
                                                              "(Ljava/lang/String;ZZ)V");
            if (onTTSMessageReceivedMethodID == NULL) {
            }

            getLanguageMethodID = (*env).GetMethodID(jlistenerClass, "getLanguage",
                                                     "()Ljava/lang/String;");
            if (getLanguageMethodID == NULL) {
            }
        }

        ~AndroidTTSManagerBridge() {
            env->DeleteGlobalRef(jlistener);
        }

        std::string jstring2string(JNIEnv *env, jstring jStr) {
            if (!jStr)
                return "";

            std::vector<char> charsCode;
            const jchar *chars = env->GetStringChars(jStr, NULL);
            jsize len = env->GetStringLength(jStr);
            jsize i;

            for( i=0;i<len; i++){
                int code = (int)chars[i];
                charsCode.push_back( code );
            }
            env->ReleaseStringChars(jStr, chars);

            return  std::string(charsCode.begin(), charsCode.end());
        }

        virtual void onTTSMessageReceived(std::string msg, bool disruptCurrentUtterance, bool isStepMessage) {
//            WLOGD("onTTSMessageReceived");
//            env = JniHelper::getEnv();
//            jstring jmessage = env->NewStringUTF(msg.c_str());
//
//            (*env).CallVoidMethod(jlistener, onTTSMessageReceivedMethodID, jmessage, disruptCurrentUtterance,
//                                  isStepMessage);
//            (*env).DeleteLocalRef(jmessage);
        };

        virtual std::string getLanguage() {
            env = JniHelper::getEnv();
            return jstring2string(env ,(jstring)(*env).CallObjectMethod(jlistener, getLanguageMethodID));
        };
    };


    JNIEXPORT void Java_com_cherrypicks_pmpmap_tts_TTSManager_nativeInitial(JNIEnv *env, jobject thiz) {
        AndroidTTSManagerBridge* bridge = new AndroidTTSManagerBridge(env, thiz);
        CPA::CoreEngine::getInstance()->setTTSManagerBridge(bridge);
    }
}