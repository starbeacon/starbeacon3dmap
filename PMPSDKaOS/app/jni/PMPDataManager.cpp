//
// Created by Alan Chan on 16/2/16.
//
#include <android/log.h>
#include <android/asset_manager_jni.h>
#include <string.h>
#include <stdio.h>
#include <jni.h>

#include "cocos2d.h"
#include "AppDelegate.h"
#include "Utilities.hpp"
#include "LoadingScene.hpp"
#include "CurrentLocationNode.hpp"

#include "MapInfo.hpp"
#include "GeoCoordinate.hpp"
#include "Point.hpp"
#include "Map.hpp"
#include "Edge.hpp"
#include "Vertex.hpp"
#include "dijkstra.h"

#include "VertexMap.hpp"
#include "PathResult.hpp"

#include "BeaconNode.hpp"
#include "CoreEngine.hpp"

using namespace CPA;

#define WLOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "JniWrapper", __VA_ARGS__))
#define WLOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, "JniWrapper", __VA_ARGS__))
#define WLOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, "JniWrapper", __VA_ARGS__))
#define WLOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, "JniWrapper", __VA_ARGS__))

extern "C" {
    //com.cherrypicks.pmpmap.PMPDataManager;
    JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPDataManager_nativeParseJson(JNIEnv *env,
                                                                                    jobject thiz, jstring json, jboolean isCache) {
        if (!json) {
            return;
        }
        const char *resultCStr = env->GetStringUTFChars(json, NULL);
        std::string resultStr(resultCStr);
        env->ReleaseStringUTFChars(json,resultCStr);

        CPA::DataManager::getInstance()->parseJson(resultStr,isCache);
    }

    JNIEXPORT jint Java_com_cherrypicks_pmpmap_PMPDataManager_nativeFindNearVertexId(JNIEnv *env, jobject thiz, jint mapId, jfloat x, jfloat y) {
        float dist = FLT_MAX;
        float vertId = -1;
        CPA::Point p(x, y, 0);
        //---find center vertex
        for (auto itr : *CPA::DataManager::getInstance()->getVertextMap()) {
            const CPA::Vertex* vert = itr.second;
            if(vert) {
                if (vert->getMapId() == mapId) {
                    float d = vert->getPosition().distance(p);
                    if (d < dist) {
                    vertId = vert->getId();
                    dist = d;
                    }
                }
            }
        }
        return vertId;
    }


}