LOCAL_PATH := $(call my-dir)


include $(CLEAR_VARS)
LOCAL_MODULE := libcurl-armeabi
LOCAL_SRC_FILES := ../../../PMPMapSDK/UI/curl/lib/armeabi/libcurl.a
LOCAL_EXPORT_C_INCLUDES := ../../../PMPMapSDK/UI/curl/include/curl
include $(PREBUILT_STATIC_LIBRARY)

ifeq ($(TARGET_ARCH_ABI), x86_64)
  include $(CLEAR_VARS)
  LOCAL_MODULE := libcurl-x86_64
  LOCAL_SRC_FILES := ../../../PMPMapSDK/UI/curl/lib/x86_64/libcurl.a
  LOCAL_EXPORT_C_INCLUDES := ../../../PMPMapSDK/UI/curl/include/curl
  include $(PREBUILT_STATIC_LIBRARY)
endif

ifeq ($(TARGET_ARCH_ABI), arm64-v8a)
  include $(CLEAR_VARS)
  LOCAL_MODULE := libcurl-arm64
  LOCAL_SRC_FILES := ../../../PMPMapSDK/UI/curl/lib/arm64-v8a/libcurl.a
  LOCAL_EXPORT_C_INCLUDES := ../../../PMPMapSDK/UI/curl/include/curl
  include $(PREBUILT_STATIC_LIBRARY)
endif



include $(CLEAR_VARS)



$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/cocos)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_LDFLAGS := -Wl,--allow-multiple-definition

LOCAL_MODULE_FILENAME := libcocos2dcpp

# LOCAL_SRC_FILES := hellocpp/main.cpp \
#                    ../../../Classes/AppDelegate.cpp \
#                    ../../../Classes/HelloWorldScene.cpp

MY_FILES_PATH  :=  $(LOCAL_PATH) \
                   $(LOCAL_PATH)/../../../PMPMapSDK

MY_FILES_SUFFIX := %.cpp %.c %.cc

My_All_Files := $(foreach src_path,$(MY_FILES_PATH), $(shell find $(src_path) -type f) )
My_All_Files := $(My_All_Files:$(MY_CPP_PATH)/./%=$(MY_CPP_PATH)%)
MY_SRC_LIST  := $(filter $(MY_FILES_SUFFIX),$(My_All_Files))
MY_SRC_LIST  := $(MY_SRC_LIST:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES := $(MY_SRC_LIST)
# LOCAL_SRC_FILES += hellocpp/main.cpp

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../../PMPMapSDK \
                    $(LOCAL_PATH)/../../../PMPMapSDK/API	\
										$(LOCAL_PATH)/../../../PMPMapSDK/App	\
                    $(LOCAL_PATH)/../../../PMPMapSDK/DataModel \
										$(LOCAL_PATH)/../../../PMPMapSDK/dijkstra \
                    $(LOCAL_PATH)/../../../PMPMapSDK/GeoCoordinate \
                    $(LOCAL_PATH)/../../../PMPMapSDK/Helper \
                    $(LOCAL_PATH)/../../../PMPMapSDK/SDK \
                    $(LOCAL_PATH)/../../../PMPMapSDK/UI \
                    $(LOCAL_PATH)/../../../PMPMapSDK/Wayfinding \
                    $(LOCAL_PATH)/../../../PMPMapSDK/Localization \
                    $(LOCAL_PATH)/../../../PMPMapSDK/UI/curl/include/curl \
										$(LOCAL_PATH)/../../../PMPMapSDK/Android \



# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END


LOCAL_STATIC_LIBRARIES := cocos2dx_static \
                          libcurl-armeabi \
                          libcurl-x86_64 \
                          libcurl-arm64


# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)

# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END
