//
// Created by Alan Chan on 16/2/16.
//
#include <android/log.h>
#include <android/asset_manager_jni.h>
#include <string.h>
#include <stdio.h>
#include <jni.h>

#include "cocos2d.h"
#include "AppDelegate.h"
#include "Utilities.hpp"
#include "LoadingScene.hpp"
#include "CurrentLocationNode.hpp"

#include "MapInfo.hpp"
#include "GeoCoordinate.hpp"
#include "Point.hpp"
#include "Map.hpp"
#include "Edge.hpp"
#include "Vertex.hpp"
#include "dijkstra.h"

#include "VertexMap.hpp"
#include "PathResult.hpp"

#include "AugmentedRealitySceneV2.hpp"

#include "BeaconNode.hpp"
#include "CoreEngine.hpp"
#include "MapController.hpp"
//#include "Coord.h"
#include "platform/android/jni/JniHelper.h"

using namespace CPA;

#define WLOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "PMPMapController", __VA_ARGS__))
#define WLOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, "PMPMapController", __VA_ARGS__))
#define WLOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, "PMPMapController", __VA_ARGS__))
#define WLOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, "PMPMapController", __VA_ARGS__))


extern "C" {
    class AndroidAugmentedRealityCallback;
    AndroidAugmentedRealityCallback* arCallback = NULL;

    jobject createJavaCoord(JNIEnv* env, float scale, float rotation){
        jclass cls = (*env).FindClass("com/cherrypicks/pmpmap/Coord");
        jmethodID constructor = (*env).GetMethodID(cls, "<init>", "(FF)V");
        jobject coord = (*env).NewObject(cls, constructor, scale, rotation);
        env->DeleteLocalRef(cls);

        return coord;
    }

    std::string jstring2string(JNIEnv *env, jstring jStr) {
        if (!jStr)
            return "";

        std::vector<char> charsCode;
        const jchar *chars = env->GetStringChars(jStr, NULL);
        jsize len = env->GetStringLength(jStr);
        jsize i;

        for( i=0;i<len; i++){
            int code = (int)chars[i];
            charsCode.push_back( code );
        }
        env->ReleaseStringChars(jStr, chars);

        return  std::string(charsCode.begin(), charsCode.end());
    }
/*
 *
 * public interface com.cherrypicks.pmpmap.PMPMapControllerCallback {
  public abstract void onPOISelect(int);
    descriptor: (I)V

  public abstract void onDeselectAllPOI();
    descriptor: ()V

  public abstract void onMapCoordUpdate(com.cherrypicks.pmpmap.Coord);
    descriptor: (Lcom/cherrypicks/pmpmap/Coord;)V
}
 */
    class AndroidMapControllerCallback : public CPA::MapController::Callback {
    public:
        JNIEnv* env;
        jobject jlistener;
        jclass jlistenerClass;
        jmethodID onPOISelectMethodID;
        jmethodID onDeselectAllPOIMethodID;
        jmethodID onMapCoordUpdateMethodID;
        jmethodID onMapSceneInitatedMethodID;
        AndroidMapControllerCallback(JNIEnv* env, jobject jlistener) {
            this->env = env;
            this->jlistener = env->NewGlobalRef(jlistener);
            jlistenerClass = (*env).FindClass("com/cherrypicks/pmpmap/PMPMapController");
            //jlistenerClass = env->NewGlobalRef(jlistenerClass);
            if (jlistenerClass == NULL) {
                WLOGD("JNICoreEngine Listener Class Not found");
            }
            onPOISelectMethodID = (*env).GetMethodID(jlistenerClass, "onPOISelect", "(I)V");
            onDeselectAllPOIMethodID = (*env).GetMethodID(jlistenerClass, "onDeselectAllPOI", "()V");
            onMapCoordUpdateMethodID = (*env).GetMethodID(jlistenerClass, "onMapCoordUpdate", "(Lcom/cherrypicks/pmpmap/Coord;)V");
            onMapSceneInitatedMethodID = (*env).GetMethodID(jlistenerClass, "onMapSceneInitated", "()V");
        }
        ~AndroidMapControllerCallback() {
            env = JniHelper::getEnv();
            env->DeleteGlobalRef(jlistener);
        }

        virtual void onPOISelect(int poiId) {
            env = JniHelper::getEnv();
            (*env).CallVoidMethod(jlistener, onPOISelectMethodID, poiId);
        };

        virtual void onDeselectAllPOI() {
            env = JniHelper::getEnv();
            (*env).CallVoidMethod(jlistener, onDeselectAllPOIMethodID);
        };

        virtual void onMapCoordUpdate(CPA::GestureNode::Coord coord) {
            env = JniHelper::getEnv();
            jobject jcoord = createJavaCoord(env, coord.scale, coord.rotation);
            (*env).CallVoidMethod(jlistener, onMapCoordUpdateMethodID, jcoord);
            env->DeleteLocalRef(jcoord);
        };

        virtual void onMapSceneInitated() {
            env = JniHelper::getEnv();
            (*env).CallVoidMethod(jlistener, onMapSceneInitatedMethodID);
        };
    };

    class AndroidAugmentedRealityCallback : public CPA::AugmentedRealityScene::Callback {
    public:
        JNIEnv *env;
        jobject jlistener;
        jclass jlistenerClass;
        jmethodID onAwayFromRouteMethodID;
        jmethodID onReturnToRouteMethodID;
        jmethodID onRequestShowFlightDetailsMethodID;

        AndroidAugmentedRealityCallback(JNIEnv *env, jobject jlistener) {
            this->env = env;
            this->jlistener = env->NewGlobalRef(jlistener);
            jlistenerClass = (*env).FindClass("com/cherrypicks/pmpmap/PMPMapController");
            if (jlistenerClass == NULL) {
                WLOGD("PMPMapController Listener Class Not found");
            }
            onAwayFromRouteMethodID = (*env).GetMethodID(jlistenerClass, "onAwayFromRoute", "()V");
            onReturnToRouteMethodID = (*env).GetMethodID(jlistenerClass, "onReturnToRoute", "()V");
            onRequestShowFlightDetailsMethodID = (*env).GetMethodID(jlistenerClass, "onRequestShowFlightDetails", "(Ljava/lang/String;Ljava/lang/String;)V");
        }

        ~AndroidAugmentedRealityCallback() {
            env = JniHelper::getEnv();
            env->DeleteGlobalRef(jlistener);
        }

        virtual void onAwayFromRoute() {
            env = JniHelper::getEnv();
            (*env).CallVoidMethod(jlistener, onAwayFromRouteMethodID);
        };

        virtual void onReturnToRoute() {
            env = JniHelper::getEnv();
            (*env).CallVoidMethod(jlistener, onReturnToRouteMethodID);
        };

        virtual void onRequestShowFlightDetails(std::string recordId, std::string preferredIdentifier) {
            env = JniHelper::getEnv();
            jstring jRecordId = env->NewStringUTF(recordId.c_str());
            jstring jPreferredId = env->NewStringUTF(preferredIdentifier.c_str());

            (*env).CallVoidMethod(jlistener, onRequestShowFlightDetailsMethodID, jRecordId, jPreferredId);

            env->DeleteLocalRef(jRecordId);
            env->DeleteLocalRef(jPreferredId);
        };
    };

    //com.cherrypicks.pmpmap.PMPMapController;
    JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapController_nativeInitial(JNIEnv *env,
                                                                                          jobject thiz) {
        AndroidMapControllerCallback* callback = new AndroidMapControllerCallback(env, thiz);
        CPA::MapController::getInstance()->setCallback(callback);
    }
    JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapController_nativeSetCurrnetFloorById(JNIEnv *env,
                                                                                    jobject thiz, jint floorId) {
        CPA::MapController::getInstance()->setCurrnetFloorById(floorId);
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapController_nativeSetSelectedPOIId(JNIEnv *env,
                                                                                          jobject thiz, jint poiId) {
        CPA::MapController::getInstance()->setSelectedPOIId(poiId);
    }

    JNIEXPORT jint Java_com_cherrypicks_pmpmap_PMPMapController_nativeGetCurrentFloorId(JNIEnv *env,
                                                                                    jobject thiz) {
        return CPA::MapController::getInstance()->getCurrnetFloorId();
    }


    JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapController_nativeDeselectAllPOI(JNIEnv *env,
                                                                                          jobject thiz) {
        CPA::MapController::getInstance()->deselectAllPOI();
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapController_nativeJumpToPosition(JNIEnv *env,
                                                                                     jobject thiz, jfloat x, jfloat y, jfloat screenX, jfloat screenY) {
        CPA::MapController::getInstance()->jumpToPosition(CPA::Point(x, y), CPA::Point(screenX, screenY));
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapController_nativeSetFocusPOIIds(JNIEnv *env,
                                                                                     jobject thiz, jobject jpoiIds) {
        WLOGD("nativeSetFocusPOIIds");
        std::vector<int> poiIds;
        if (jpoiIds) {
            jclass arrayClass = env->FindClass("java/util/List");
            jmethodID sizeMid = env->GetMethodID(arrayClass, "size", "()I");
            jclass integerClass = env->FindClass("java/lang/Integer");
            jmethodID intValueMid = env->GetMethodID(integerClass, "intValue", "()I");
            //jvalue arg;
            jint size = env->CallIntMethod(jpoiIds, sizeMid);
            WLOGD("nativeSetFocusPOIIds - size: %d", size);
            int* cppArray = new int[size];
            jmethodID getMid = env->GetMethodID(arrayClass, "get", "(I)Ljava/lang/Object;");
            for (int i = 0; i < size; i++) {
                jobject element = env->CallObjectMethod(jpoiIds, getMid, i);
                int val = env->CallIntMethod(element, intValueMid);
                WLOGD("nativeSetFocusPOIIds - [%d]-%d" + size, val);
                poiIds.push_back(val);
                env->DeleteLocalRef(element);
            }
        }
        CPA::MapController::getInstance()->setFocusPOIIds(poiIds);
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapController_nativeSetMapCoord(JNIEnv *env,
                                                                                     jobject thiz, jobject jcoord) {

        CPA::GestureNode::Coord coord = CPA::MapController::getInstance()->getMapCoord();
        jclass cls = (*env).FindClass("com/cherrypicks/pmpmap/Coord");
        jmethodID getRotationMethodId = (*env).GetMethodID(cls, "getRotation", "()F");
        jmethodID getScaleMethodId = (*env).GetMethodID(cls, "getScale", "()F");
        coord.rotation = env->CallFloatMethod(jcoord, getRotationMethodId);
        coord.scale = env->CallFloatMethod(jcoord, getScaleMethodId);
        CPA::MapController::getInstance()->setMapCoord(coord);
    }

    JNIEXPORT jobject Java_com_cherrypicks_pmpmap_PMPMapController_nativeGetMapCoord(JNIEnv *env,
                                                                                  jobject thiz) {
        CPA::GestureNode::Coord coord = CPA::MapController::getInstance()->getMapCoord();
        jobject jcoord = createJavaCoord(env, coord.scale, coord.rotation);
        return jcoord;
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapController_nativeFitBounds(JNIEnv *env,
                                                                                     jobject thiz, jobject jbounds, jobject jpadding, jboolean animated) {

        float minX, minY, maxX, maxY;
        float paddingLeft, paddingRight, paddingTop, paddingBottom;

        //convert from java
        jclass cls = env->GetObjectClass(jbounds);
        jfieldID topField = env->GetFieldID(cls, "top", "F");
        jfieldID bottomField = env->GetFieldID(cls, "bottom", "F");
        jfieldID leftField = env->GetFieldID(cls, "left", "F");
        jfieldID rightField = env->GetFieldID(cls, "right", "F");

        minX = env->GetFloatField(jbounds, leftField);
        minY = env->GetFloatField(jbounds, topField);
        maxX = env->GetFloatField(jbounds, rightField);
        maxY = env->GetFloatField(jbounds, bottomField);

        paddingLeft = env->GetFloatField(jpadding, leftField);
        paddingTop = env->GetFloatField(jpadding, topField);
        paddingRight = env->GetFloatField(jpadding, rightField);
        paddingBottom = env->GetFloatField(jpadding, bottomField);

        cocos2d::Vec2 min(minX, minY);
        cocos2d::Vec2 max(maxX, maxY);
        min = CPA::MapController::getInstance()->convertPointToMapCoordinate(min);
        max = CPA::MapController::getInstance()->convertPointToMapCoordinate(max);
        cocos2d::Rect rect(min.x, max.y, max.x - min.x, min.y - max.y);
        CPA::Padding padding;
        padding.left = paddingLeft;
        padding.right = paddingRight;
        padding.top = paddingTop;
        padding.bottom = paddingBottom;
        CPA::MapController::getInstance()->fitBounds(rect, padding, true);
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapController_nativeSetFocusPOICategoryId(JNIEnv *env,
                                                                                   jobject thiz, jint id) {
        CPA::MapController::getInstance()->setFocusPOICategoryId(id);
    }

    JNIEXPORT jint Java_com_cherrypicks_pmpmap_PMPMapController_nativeGetFocusPOICategoryId(JNIEnv *env,
                                                                                   jobject thiz) {
        return CPA::MapController::getInstance()->getFocusPOICategoryId();
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapController_nativeSetOverviewing(JNIEnv *env,
                                                                                            jobject thiz, jboolean b) {
        CPA::MapController::getInstance()->setOverviewing(b);
    }

    JNIEXPORT jboolean Java_com_cherrypicks_pmpmap_PMPMapController_nativeIsOverviewing(JNIEnv *env,
                                                                                            jobject thiz) {
        return CPA::MapController::getInstance()->isOverviewing();
    }

    JNIEXPORT jboolean Java_com_cherrypicks_pmpmap_PMPMapController_nativeIsMapSceneInitated(JNIEnv *env,
                                                                                        jobject thiz) {
        return CPA::MapController::getInstance()->isMapSceneInitiated();
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapController_nativeRedrawMapScene(JNIEnv *env,
                                                                                            jobject thiz) {
        CPA::MapController::getInstance()->redrawMapScene();
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapController_nativeSetEnablePathOverview(JNIEnv *env,
                                                                                     jobject thiz, jboolean b) {
        CPA::MapController::getInstance()->setEnablePathOverview(b);
    }

    JNIEXPORT jboolean Java_com_cherrypicks_pmpmap_PMPMapController_nativeIsEnablePathOverview(JNIEnv *env,
                                                                                             jobject thiz) {
        return CPA::MapController::getInstance()->isEnablePathOverview();
    }

    JNIEXPORT jfloat Java_com_cherrypicks_pmpmap_PMPMapController_nativeScaleForBounds(JNIEnv *env,
                                                                                jobject thiz, jobject jbounds, jobject jpadding, jboolean animated) {

        float minX, minY, maxX, maxY;
        float paddingLeft, paddingRight, paddingTop, paddingBottom;

        //convert from java
        jclass cls = env->GetObjectClass(jbounds);
        jfieldID topField = env->GetFieldID(cls, "top", "F");
        jfieldID bottomField = env->GetFieldID(cls, "bottom", "F");
        jfieldID leftField = env->GetFieldID(cls, "left", "F");
        jfieldID rightField = env->GetFieldID(cls, "right", "F");

        minX = env->GetFloatField(jbounds, leftField);
        minY = env->GetFloatField(jbounds, topField);
        maxX = env->GetFloatField(jbounds, rightField);
        maxY = env->GetFloatField(jbounds, bottomField);

        paddingLeft = env->GetFloatField(jpadding, leftField);
        paddingTop = env->GetFloatField(jpadding, topField);
        paddingRight = env->GetFloatField(jpadding, rightField);
        paddingBottom = env->GetFloatField(jpadding, bottomField);

        cocos2d::Vec2 min(minX, minY);
        cocos2d::Vec2 max(maxX, maxY);
        min = CPA::MapController::getInstance()->convertPointToMapCoordinate(min);
        max = CPA::MapController::getInstance()->convertPointToMapCoordinate(max);
        cocos2d::Rect rect(min.x, max.y, max.x - min.x, min.y - max.y);
        CPA::Padding padding;
        padding.left = paddingLeft;
        padding.right = paddingRight;
        padding.top = paddingTop;
        padding.bottom = paddingBottom;
        return CPA::MapController::getInstance()->scaleForBounds(rect, padding);
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapController_nativeSetZoomLevel(JNIEnv *env, jobject thiz, jfloat scale) {
        auto coord = CPA::MapController::getInstance()->getMapCoord();
        coord.scale = scale;
        CPA::MapController::getInstance()->setMapCoord(coord);
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapController_nativeShowAR(JNIEnv *env,
                                                                             jobject thiz,
                                                                             jfloat headerHeight,
                                                                             jfloat footerHeight,
                                                                             jfloat angle,
                                                                             jboolean isScrBlocked,
                                                                             jboolean isArrived,
                                                                             jboolean isBypassed,
                                                                            jstring mtelRecordId,
                                                                            jstring flightNo,
                                                                            jstring gateCode,
                                                                            jstring flightStatus,
                                                                            jstring departureTime) {
        CPA::CoreEngine* coreEngine = CPA::CoreEngine::getInstance();
        coreEngine->setEnableCompass(true);

        CPA::AugmentedRealitySceneV2* arScene = CPA::AugmentedRealitySceneV2::create();

        arScene->updateUserFlightDetails(jstring2string(env, mtelRecordId),
                                         jstring2string(env, flightNo),
                                         jstring2string(env, gateCode),
                                         jstring2string(env, flightStatus),
                                         jstring2string(env, departureTime));

        CPA::MapController::getInstance()->setMapScene(nullptr);
        Director::getInstance()->replaceScene(arScene);

        CPA::MapController::getInstance()->setARScene(arScene);
        CPA::MapController::getInstance()->setMapScene(nullptr);
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapController_nativeUpdateUserFlightForARView(JNIEnv *env,
                                                                                                 jobject thiz,
                                                                                                 jstring mtelRecordId,
                                                                                                 jstring flightNo,
                                                                                                 jstring gateCode,
                                                                                                 jstring flightStatus,
                                                                                                 jstring departureTime) {
        Scene *scene = Director::getInstance()->getRunningScene();
        if (dynamic_cast<CPA::AugmentedRealitySceneV2*>(scene) != NULL) {
            ((CPA::AugmentedRealitySceneV2*)scene)->updateUserFlightDetails(jstring2string(env, mtelRecordId),
                                                                          jstring2string(env, flightNo),
                                                                          jstring2string(env, gateCode),
                                                                          jstring2string(env, flightStatus),
                                                                          jstring2string(env, departureTime));
        }

    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_PMPMapController_nativeReplaceSceneWithMapScene(JNIEnv *env, jobject thiz) {
        Scene *scene = Director::getInstance()->getRunningScene();
        CPA::CoreEngine* coreEngine = CPA::CoreEngine::getInstance();
        coreEngine->setEnableCompass(false);

        CPA::MapController::getInstance()->setARScene(nullptr);
        CPA::MapController::getInstance()->replaceSceneWithMapScene();
    }

    JNIEXPORT jboolean Java_com_cherrypicks_pmpmap_PMPMapController_nativeIsARViewShowing(JNIEnv *env, jobject thiz) {
        Scene *scene = Director::getInstance()->getRunningScene();
        if (dynamic_cast<CPA::AugmentedRealitySceneV2*>(scene) != NULL) {
            return true;
        }
        return false;
    }

    JNIEXPORT jint Java_com_cherrypicks_pmpmap_PMPMapController_nativeGetScreenCenterX(JNIEnv *env, jobject thiz) {
        return CPA::MapController::getInstance()->getScreenCenter().x;
    }

    JNIEXPORT jint Java_com_cherrypicks_pmpmap_PMPMapController_nativeGetScreenCenterY(JNIEnv *env, jobject thiz) {
        return CPA::MapController::getInstance()->getScreenCenter().y;
    }

    JNIEXPORT jboolean Java_com_cherrypicks_pmpmap_PMPMapController_nativeIsARMasking(JNIEnv *env, jobject thiz) {
        Scene *scene = Director::getInstance()->getRunningScene();
        CPA::AugmentedRealitySceneV2* arScene = dynamic_cast<CPA::AugmentedRealitySceneV2*>(scene);
        if (arScene != NULL) {
            return arScene->isMasking();
        }
        return false;
    }
}
