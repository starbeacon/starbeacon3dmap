//
// Created by Alan Chan on 16/2/16.
//
#include <android/log.h>
#include <android/asset_manager_jni.h>
#include <string.h>
#include <stdio.h>
#include <jni.h>

#include "cocos2d.h"
#include "AppDelegate.h"
#include "LoadingScene.hpp"
#include "CurrentLocationNode.hpp"

#include "MapInfo.hpp"
#include "GeoCoordinate.hpp"
#include "Point.hpp"
#include "Map.hpp"
#include "Edge.hpp"
#include "Vertex.hpp"
#include "dijkstra.h"
#include "Utilities.hpp"
#include "DataManager.hpp"

#include "VertexMap.hpp"
#include "PathResult.hpp"

#include "BeaconNode.hpp"
#include "CoreEngine.hpp"

#include "platform/android/jni/JniHelper.h"

#include "ScheduleParser.hpp"
#import "BearingCalculator.hpp"

using namespace CPA;

#define WLOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "JNICoreEngine", __VA_ARGS__))
#define WLOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, "JNICoreEngine", __VA_ARGS__))
#define WLOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, "JNICoreEngine", __VA_ARGS__))
#define WLOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, "JNICoreEngine", __VA_ARGS__))


jobject buildJavaVertex(JNIEnv* env, int node_id, jobject toEdges, jobject fromEdges, float x, float y, float z, int mapID){

    jclass vertexClazz = (*env).FindClass("com/cherrypicks/pmpmap/datamodel/Vertex");
    jmethodID constructor = (*env).GetMethodID(vertexClazz, "<init>", "(I)V");
    jobject vertex = (*env).NewObject(vertexClazz, constructor, node_id);

    (*env).CallVoidMethod(vertex, (*env).GetMethodID(vertexClazz, "setFromEdges", "(Ljava/util/ArrayList;)V"), fromEdges);
    (*env).CallVoidMethod(vertex, (*env).GetMethodID(vertexClazz, "setToEdges", "(Ljava/util/ArrayList;)V"), toEdges);
    (*env).CallVoidMethod(vertex, (*env).GetMethodID(vertexClazz, "setX", "(F)V"), x);
    (*env).CallVoidMethod(vertex, (*env).GetMethodID(vertexClazz, "setY", "(F)V"), y);
    (*env).CallVoidMethod(vertex, (*env).GetMethodID(vertexClazz, "setZ", "(F)V"), z);
    (*env).CallVoidMethod(vertex, (*env).GetMethodID(vertexClazz, "setMapId", "(I)V"), mapID);

    env->DeleteLocalRef(vertexClazz);

    return vertex;
}

jobject buildJavaVertex(JNIEnv* env, int node_id){
    jclass vertexClazz = (*env).FindClass("com/cherrypicks/pmpmap/datamodel/Vertex");
    jmethodID constructor = (*env).GetMethodID(vertexClazz, "<init>", "(I)V");
    jobject vertex = (*env).NewObject(vertexClazz, constructor, node_id);
    env->DeleteLocalRef(vertexClazz);

    return vertex;
}

//Vertex toVertex;
//Vertex fromVertex;
//double weight;
//int condition;
//int pathType;
//public Edge(Vertex fv, Vertex tv, double weight, int condition, int pathType)
jobject buildJavaEdge(JNIEnv* env, jobject fv, jobject tv, double weight, double distance, int condition, int pathType){
    jclass edgeClazz = (*env).FindClass("com/cherrypicks/pmpmap/datamodel/Edge");
    jmethodID constructor = (*env).GetMethodID(edgeClazz, "<init>", "(Lcom/cherrypicks/pmpmap/datamodel/Vertex;Lcom/cherrypicks/pmpmap/datamodel/Vertex;DDII)V");
    jobject edge = (*env).NewObject(edgeClazz, constructor, fv, tv, weight, distance, condition, pathType);
    env->DeleteLocalRef(edgeClazz);

    return edge;
}

jobject buildJavaEdge(JNIEnv* env, Edge edge){
    jobject fromVertext = NULL;
    jobject toVertex = NULL;
    if(edge.getFromVertex()){
        fromVertext = buildJavaVertex(env, edge.getFromVertex()->node_id);
    }

    if(edge.getToVertex()){
        toVertex = buildJavaVertex(env, edge.getToVertex()->node_id);
    }

    jobject result = buildJavaEdge(env, fromVertext, toVertex, edge.getWeight(), edge.getDistance(), edge.getCondition(), edge.getPathType());

    env->DeleteLocalRef(fromVertext);
    env->DeleteLocalRef(toVertex);

    return result;
}


jobject buildJavaPathNode(JNIEnv* env, jobject previousVertex, jobject currentVertex, jobject nextVertex){
    jclass pathNodeClazz = (*env).FindClass("com/cherrypicks/pmpmap/datamodel/PathNode");
    jmethodID constructor = (*env).GetMethodID(pathNodeClazz, "<init>", "(Lcom/cherrypicks/pmpmap/datamodel/Vertex;Lcom/cherrypicks/pmpmap/datamodel/Vertex;Lcom/cherrypicks/pmpmap/datamodel/Vertex;)V");
    jobject pathNode = (*env).NewObject(pathNodeClazz, constructor, previousVertex, nextVertex, currentVertex);
    env->DeleteLocalRef(pathNodeClazz);

    return pathNode;
}

//Vertex fromVertex, Vertex toVertex, ArrayList<PathNode> pathNodes, float totalDistance
jobject buildJavaPathResult(JNIEnv* env, jobject fromVertex, jobject toVertex, jobject pathNodes, float totalDistance){
    jclass pathNodeClazz = (*env).FindClass("com/cherrypicks/pmpmap/datamodel/PathResult");
    jmethodID constructor = (*env).GetMethodID(pathNodeClazz, "<init>", "(Lcom/cherrypicks/pmpmap/datamodel/Vertex;Lcom/cherrypicks/pmpmap/datamodel/Vertex;Ljava/util/ArrayList;F)V");
    jobject pathResult = (*env).NewObject(pathNodeClazz, constructor, fromVertex, toVertex, pathNodes, totalDistance);

    env->DeleteLocalRef(pathNodeClazz);

    return pathResult;
}

jobject buildJavaVertexByCVertex(JNIEnv* env, const Vertex *vertex){
    jclass arrayClazz = (*env).FindClass("java/util/ArrayList");

    jobject fromEdge = (*env).NewObject(arrayClazz, (*env).GetMethodID(arrayClazz, "<init>", "()V"));
    jobject toEdge = (*env).NewObject(arrayClazz, (*env).GetMethodID(arrayClazz, "<init>", "()V"));

    std::set<Edge*, Edge::Comparator> cToEdges = vertex->toEdges;
    std::set<Edge*, Edge::Comparator> cFromEdges = vertex->fromEdges;

    for (auto edge : cToEdges) {
        jobject jedge = buildJavaEdge(env, *edge);
        (*env).CallBooleanMethod(toEdge, (*env).GetMethodID(arrayClazz, "add", "(Ljava/lang/Object;)Z"), jedge);
        env->DeleteLocalRef(jedge);
    }

    for (auto edge : cFromEdges) {
        jobject jedge = buildJavaEdge(env, *edge);
        (*env).CallBooleanMethod(fromEdge, (*env).GetMethodID(arrayClazz, "add", "(Ljava/lang/Object;)Z"), jedge);
        env->DeleteLocalRef(jedge);
    }


    jobject result = buildJavaVertex(env, vertex->node_id, toEdge, fromEdge, vertex->x, vertex->y, vertex->z, vertex->mapId);


    env->DeleteLocalRef(fromEdge);
    env->DeleteLocalRef(toEdge);
    env->DeleteLocalRef(arrayClazz);

    return result;
}

jobject buildJavaPathResultByCPathResult(JNIEnv* env, std::vector<PathResult> vec){
    jclass pathResultClazz = (*env).FindClass("com/cherrypicks/pmpmap/datamodel/PathResult");
    jclass arrayClazz = (*env).FindClass("java/util/ArrayList");
    jclass pathNodeClazz = (*env).FindClass("com/cherrypicks/pmpmap/datamodel/PathNode");
    jclass vertexClazz = (*env).FindClass("com/cherrypicks/pmpmap/datamodel/Vertex");

    jobject jArrayPathResults = (*env).NewObject(arrayClazz, (*env).GetMethodID(arrayClazz, "<init>", "()V"));

    //    jobject gjArrayPathResults = env->NewGlobalRef(jArrayPathResults);
    //    env->DeleteLocalRef(jArrayPathResults);

    for (int n=0;n<vec.size();n++)
    {
        jobject jArrayPathNodes = (*env).NewObject(arrayClazz, (*env).GetMethodID(arrayClazz, "<init>", "()V"));

        PathResult result = vec.at(n);
        std::vector<PathNode> pathNodes = result.pathNodes;
        for (int i=0; i < pathNodes.size(); i++) {
            PathNode pathNode = pathNodes.at(i);

            jobject previousVertex = NULL;
            jobject currentVertex = NULL;
            jobject nextVertex = NULL;
            if(pathNode.previousVertex){
                jobject temp = buildJavaVertexByCVertex(env, pathNode.previousVertex);
                previousVertex = temp;
            }

            if(pathNode.currentVertex){
                jobject temp = buildJavaVertexByCVertex(env, pathNode.currentVertex);
                currentVertex = temp;
            }

            if(pathNode.nextVertex){
                jobject temp = buildJavaVertexByCVertex(env, pathNode.nextVertex);
                nextVertex = temp;
            }

            jobject jPathNode = buildJavaPathNode(env, previousVertex, currentVertex, nextVertex);
            jfieldID fid = (*env).GetFieldID((*env).FindClass("com/cherrypicks/pmpmap/datamodel/PathNode"), "nextPathType", "I");
            (*env).SetIntField(jPathNode, fid, pathNode.nextPathType);

            (*env).CallBooleanMethod(jArrayPathNodes, (*env).GetMethodID(arrayClazz, "add", "(Ljava/lang/Object;)Z"), jPathNode);
            env->DeleteLocalRef(previousVertex);
            env->DeleteLocalRef(currentVertex);
            env->DeleteLocalRef(nextVertex);
            env->DeleteLocalRef(jPathNode);

        }


        jobject fromVertex = NULL;
        jobject toVertex = NULL;
        if(result.fromVertex){
            jobject temp = buildJavaVertexByCVertex(env, result.fromVertex);
            fromVertex = temp;
        }

        if(result.toVertex){
            jobject temp = buildJavaVertexByCVertex(env, result.toVertex);
            toVertex = temp;
        }

        jobject jPathResult = buildJavaPathResult(env, fromVertex, toVertex, jArrayPathNodes, result.totalDistance);
        (*env).CallBooleanMethod(jArrayPathResults, (*env).GetMethodID(arrayClazz, "add", "(Ljava/lang/Object;)Z"), jPathResult);

        env->DeleteLocalRef(jArrayPathNodes);
        env->DeleteLocalRef(jPathResult);
        env->DeleteLocalRef(fromVertex);
        env->DeleteLocalRef(toVertex);
    }

    return jArrayPathResults;
}

extern "C" {


    class AndroidCoreEngineCallback : public CPA::CoreEngine::Callback {
    private:
        JNIEnv* env;
        jobject jlistener;
        jclass jlistenerClass;
        jmethodID onEngineInitialDoneMethodID;
        jmethodID onEntryLiftDetectedMethodID;
        jmethodID onEntryShuttleBusDetectedMethodID;
        jmethodID onArrivalDestinationMethodID;
        jmethodID onBypassDestinationMethodID;
        jmethodID onResetBypassDestMethodID;
        jmethodID onRerouteMethodID;
        jmethodID onClearSearchResultMethodID;
        jmethodID onStepStepMessageUpdateMethodID;
        jmethodID onMapModeChangedMethodID;
        jmethodID onPathTypeCheckedMethodID;
        jmethodID onShopNearByPromoMessageMethodID;
        jmethodID onCameraFacedDownMethodID;
        jmethodID onCameraFacedFrontMethodID;

    public:
        AndroidCoreEngineCallback(JNIEnv* env, jobject jlistener) {
            this->env = env;
            this->jlistener = env->NewGlobalRef(jlistener);
            jlistenerClass = (*env).FindClass("com/cherrypicks/pmpmap/core/CoreEngineListener");
            //jlistenerClass = env->NewGlobalRef(jlistenerClass);
            if (jlistenerClass == NULL) {
                WLOGD("JNICoreEngine Listener Class Not found");
            }
            onEngineInitialDoneMethodID = (*env).GetMethodID(jlistenerClass, "onEngineInitialDone", "()V");
            if(onEngineInitialDoneMethodID == NULL) {
                WLOGD("onEngineInitialDoneMethodID Not found");
            }
            (*env).CallVoidMethod(jlistener, onEngineInitialDoneMethodID);
            onEntryLiftDetectedMethodID = (*env).GetMethodID(jlistenerClass, "onEntryLiftDetected", "(DD)V");
            if(onEntryLiftDetectedMethodID == NULL) {
                WLOGD("onEntryLiftDetectedMethodID Not found");
            }
            onEntryShuttleBusDetectedMethodID = (*env).GetMethodID(jlistenerClass, "onEntryShuttleBusDetected", "(DD)V");
            if(onEntryShuttleBusDetectedMethodID == NULL) {
                WLOGD("onEntryShuttleBusDetectedMethodID Not found");
            }
            onArrivalDestinationMethodID = (*env).GetMethodID(jlistenerClass, "onArrivalDestination", "()V");
            if(onArrivalDestinationMethodID == NULL) {
                WLOGD("onArrivalDestinationMethodID Not found");
            }
            onBypassDestinationMethodID = (*env).GetMethodID(jlistenerClass, "onBypassDestination", "()V");
            if(onBypassDestinationMethodID == NULL) {
                WLOGD("onBypassDestinationMethodID Not found");
            }
            onResetBypassDestMethodID = (*env).GetMethodID(jlistenerClass, "onResetBypassDest", "()V");
            if(onResetBypassDestMethodID == NULL) {
                WLOGD("onResetBypassDestMethodID Not found");
            }
            onRerouteMethodID = (*env).GetMethodID(jlistenerClass, "onReroute", "()V");
            if(onRerouteMethodID == NULL) {
                WLOGD("onRerouteMethodID Not found");
            }
            onClearSearchResultMethodID = (*env).GetMethodID(jlistenerClass, "onClearSearchResult", "()V");
            if(onClearSearchResultMethodID == NULL) {
                WLOGD("onClearSearchResultMethodID Not found");
            }
            onStepStepMessageUpdateMethodID = (*env).GetMethodID(jlistenerClass, "onStepStepMessageUpdate", "(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;FF)V");
            if(onStepStepMessageUpdateMethodID == NULL) {
                WLOGD("onStepStepMessageUpdateMethodID Not found");
            }
            onMapModeChangedMethodID = (*env).GetMethodID(jlistenerClass, "onMapModeChanged", "(I)V");
            if(onMapModeChangedMethodID == NULL) {
                WLOGD("onMapModeChangedMethodID Not found");
            }
            onPathTypeCheckedMethodID = (*env).GetMethodID(jlistenerClass, "onPathTypeChecked", "(ILjava/lang/String;Ljava/lang/String;)V");
            if(onPathTypeCheckedMethodID == NULL) {
                WLOGD("onPathTypeCheckedMethodID Not found");
            }
            onShopNearByPromoMessageMethodID = (*env).GetMethodID(jlistenerClass, "onShopNearByPromoMessage", "(II)V");
            if(onShopNearByPromoMessageMethodID == NULL) {
                WLOGD("onShopNearByPromoMessageMethodID Not found");
            }

            onCameraFacedDownMethodID = (*env).GetMethodID(jlistenerClass, "onCameraFacedDown", "()V");
            if(onCameraFacedDownMethodID == NULL) {
                WLOGD("onCameraFacedDownMethodID Not found");
            }

            onCameraFacedFrontMethodID = (*env).GetMethodID(jlistenerClass, "onCameraFacedFront", "()V");
            if(onCameraFacedFrontMethodID == NULL) {
                WLOGD("onCameraFacedFrontMethodID Not found");
            }
        }

        ~AndroidCoreEngineCallback() {
            env->DeleteGlobalRef(jlistener);
        }

        virtual void onEngineInitialDone() {
            WLOGD("onEngineInitialDone");
            env = JniHelper::getEnv();
            (*env).CallVoidMethod(jlistener, onEngineInitialDoneMethodID);
        };

        virtual void onEntryLiftDetected(double x, double y) {
            WLOGD("onEntryLiftDetectedCallback");
            env = JniHelper::getEnv();
            (*env).CallVoidMethod(jlistener, onEntryLiftDetectedMethodID, x, y);
        };

        virtual void onEntryShuttleBusDetected(double x, double y) {
            WLOGD("onEntryShuttleBusDetected");
            env = JniHelper::getEnv();
            (*env).CallVoidMethod(jlistener, onEntryShuttleBusDetectedMethodID, x, y);
        };

        virtual void onArrivalDestination() {
            WLOGD("onArrivalDestination");
            env = JniHelper::getEnv();
            (*env).CallVoidMethod(jlistener, onArrivalDestinationMethodID);
        };

        virtual void onBypassDestination() {
            WLOGD("onBypassDestination");
            env = JniHelper::getEnv();
            (*env).CallVoidMethod(jlistener, onBypassDestinationMethodID);
        };

        virtual void onResetBypassDest() {
            WLOGD("onResetBypassDest");
            env = JniHelper::getEnv();
            (*env).CallVoidMethod(jlistener, onResetBypassDestMethodID);
        };

        virtual void onReroute() {
            WLOGD("onReroute");
            env = JniHelper::getEnv();
            (*env).CallVoidMethod(jlistener, onRerouteMethodID);
        };

        virtual void onClearSearchResult() {
            WLOGD("onClearSearchResult");
            env = JniHelper::getEnv();
            (*env).CallVoidMethod(jlistener, onClearSearchResultMethodID);
        };

        virtual void onStepStepMessageUpdate(int beaconType, std::string message, std::string poiMessage, std::string imageName, float totalDurtaion, float totalDistance) {
            WLOGD("onStepStepMessageUpdate");
            env = JniHelper::getEnv();
            jstring jmessage = env->NewStringUTF(message.c_str());
            jstring jpoiMessage = env->NewStringUTF(poiMessage.c_str());;
            jstring jimageName = env->NewStringUTF(imageName.c_str());;
            (*env).CallVoidMethod(jlistener, onStepStepMessageUpdateMethodID, beaconType, jmessage, jpoiMessage, jimageName, totalDurtaion, totalDistance);
            env->DeleteLocalRef(jmessage);
            env->DeleteLocalRef(jpoiMessage);
            env->DeleteLocalRef(jimageName);
        };

        virtual void onMapModeChanged(int newMapMode){
            WLOGD("onMapModeChanged");
            env = JniHelper::getEnv();
            (*env).CallVoidMethod(jlistener, onMapModeChangedMethodID, newMapMode);
        };

        virtual void onPathTypeChecked(CPA::BaseEdge::PathType pathType, std::string message, std::string imageName){
            WLOGD("onPathTypeChecked");
            env = JniHelper::getEnv();
            jstring jmessage = env->NewStringUTF(message.c_str());
            jstring jimageName = env->NewStringUTF(imageName.c_str());
            (*env).CallVoidMethod(jlistener, onPathTypeCheckedMethodID, pathType, jmessage, jimageName);
            env->DeleteLocalRef(jmessage);
            env->DeleteLocalRef(jimageName);
        };

        virtual void onShopNearByPromoMessage(int promoId, int poiId){
            WLOGD("onShopNearByPromoMessage");
            env = JniHelper::getEnv();
            (*env).CallVoidMethod(jlistener, onShopNearByPromoMessageMethodID, promoId, poiId);
        };

        virtual void onCameraFacedDown() {
            env = JniHelper::getEnv();
            (*env).CallVoidMethod(jlistener, onCameraFacedDownMethodID);
        };
        virtual void onCameraFacedFront() {
            env = JniHelper::getEnv();
            (*env).CallVoidMethod(jlistener, onCameraFacedFrontMethodID);
        };
    };
    //com.cherrypicks.pmpmap.core.CoreEngine
    JNIEXPORT void Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeInitial(JNIEnv* env, jobject thiz) {
        AndroidCoreEngineCallback* androidCoreEngineCallback = new AndroidCoreEngineCallback(env, thiz);
        CPA::CoreEngine::getInstance()->setCallback(androidCoreEngineCallback);
        WLOGD("nativeInitial");
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeSetMapMode(JNIEnv* env, jobject thiz, jint mode) {
        CPA::CoreEngine::getInstance()->setMapMode((CPA::CoreEngine::MapMode)mode);
    }

    JNIEXPORT jint Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeGetMapMode(JNIEnv* env, jobject thiz) {
        return CPA::CoreEngine::getInstance()->getMapMode();
    }

    JNIEXPORT jstring Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeParseScheduleByPOIID(JNIEnv* env, jobject thiz, jint poiID, jintArray isOpening) {
        std::string openingHrStdString;
        bool isOpeningBool;
        CPA::ScheduleParser::parseScheduleByPOIID(poiID, &isOpeningBool, &openingHrStdString);

        jstring jResult = env->NewStringUTF(openingHrStdString.c_str());

        jint *isOpeningJint = env->GetIntArrayElements(isOpening, NULL);

        int newValue = isOpeningBool? 1 : 0;
        env->SetIntArrayRegion(isOpening, 0, 1, &newValue);

//        env->DeleteLocalRef(jResult);
//        env->ReleaseIntArrayElements(isOpening, isOpeningJint, 0);
        return jResult;
    }

    JNIEXPORT jstring Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeParseScheduleByPromotionID(JNIEnv* env, jobject thiz, jint promotionID, jintArray isOpening) {
        std::string openingHrStdString;
        bool isOpeningBool;
        CPA::ScheduleParser::parseScheduleByPromotionID(promotionID, &isOpeningBool, &openingHrStdString);

        jstring jResult = env->NewStringUTF(openingHrStdString.c_str());

        jint *isOpeningJint = env->GetIntArrayElements(isOpening, NULL);

        int newValue = isOpeningBool? 1 : 0;
        env->SetIntArrayRegion(isOpening, 0, 1, &newValue);

    //        env->DeleteLocalRef(jResult);
    //        env->ReleaseIntArrayElements(isOpening, isOpeningJint, 0);
        return jResult;
    }

    JNIEXPORT jboolean Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeSearchPathFromPOIToPOI(JNIEnv* env, jobject thiz, jint fromPoiID, jint toPoiID) {
        auto results = CPA::CoreEngine::getInstance()->searchPathFromPOIToPOI(fromPoiID, toPoiID);
        if (results.size()) {
            CPA::CoreEngine::getInstance()->getPathResult();
            CPA::CoreEngine::getInstance()->setShowCurrentPathResult(true);
        }
        return results.size() != 0;
    }

    JNIEXPORT jboolean Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeSearchPathByPOI(JNIEnv* env, jobject thiz, jint poiID, jboolean updateRouteStepIndicator) {
        auto results = CPA::CoreEngine::getInstance()->searchPathByPOI(-1, poiID, -1, updateRouteStepIndicator);
        if (results.size()) {
            CPA::CoreEngine::getInstance()->getPathResult();
            CPA::CoreEngine::getInstance()->setShowCurrentPathResult(true);
        }
        return results.size() != 0;
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeClearSearchResult(JNIEnv* env, jobject thiz) {
        CPA::CoreEngine::getInstance()->clearSearchResult();
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeSetCompassEnable(JNIEnv* env, jobject thiz, jboolean enable) {
        CPA::CoreEngine::getInstance()->setEnableCompass(enable);
    }

    JNIEXPORT jboolean Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeIsCompassEnable(JNIEnv* env, jobject thiz) {
        return CPA::CoreEngine::getInstance()->isEnableCompass();
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeSetRecenterModeEnable(JNIEnv* env, jobject thiz, jboolean enable) {
        CPA::CoreEngine::getInstance()->setRecenterModeEnabled(enable);
    }

    JNIEXPORT jboolean Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeIsRecenterModeEnable(JNIEnv* env, jobject thiz) {
        return CPA::CoreEngine::getInstance()->isRecenterModeEnabled()? JNI_TRUE : JNI_FALSE;
    }


    JNIEXPORT jfloat Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeGetNavigationTotalTime(JNIEnv* env, jobject thiz) {
        return CPA::CoreEngine::getInstance()->getNavigationTotalTime();
    }

    JNIEXPORT jfloat Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeGetNavigationTotalDistance(JNIEnv* env, jobject thiz) {
        return CPA::CoreEngine::getInstance()->getNavigationTotalDistance();
    }

    JNIEXPORT jint Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeGetRouteStepIndictorsSize(JNIEnv* env, jobject thiz) {
        return CPA::CoreEngine::getInstance()->getRouteStepIndictors().size();
    }

    JNIEXPORT jfloat Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeGetNavigationTimeRemaining(JNIEnv* env, jobject thiz) {
        return CPA::CoreEngine::getInstance()->getNavigationTimeRemaining();
    }

    JNIEXPORT jfloat Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeGetNavigationDistanceRemaining(JNIEnv* env, jobject thiz) {
        return CPA::CoreEngine::getInstance()->getNavigationDistanceRemaining();
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeOnIndoorLocationUpdate(JNIEnv* env, jobject thiz, jint type, jdouble x, jdouble y, jdouble z, jint mapId) {
        CPA::SPLocation spLocation(x, y, z, mapId);
        CPA::CoreEngine::getInstance()->onIndoorLocationUpdate(type, spLocation);
    }

    JNIEXPORT jfloat Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeOnCompassUpdate(JNIEnv* env, jobject thiz, jfloat compass) {
        CPA::CoreEngine::getInstance()->onCompassUpdate(compass);
    }

    JNIEXPORT jobject Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeFilterVertexByDuration(JNIEnv* env, jobject thiz, jint vertId, jfloat duration) {
        WLOGD("vertDur: %d", vertId);
        WLOGD("duration: %f", duration);
        auto curVert = CPA::DataManager::getInstance()->getVertextMap()->getVertexByID(vertId);
        if (vertId == -999) {
            curVert = CPA::CoreEngine::getInstance()->getCurrentLocationVertex();
        }
        std::vector<CPA::VertexMap::VertexByDuration> result = CPA::DataManager::getInstance()->getVertextMap()->filterVertexByDuration(curVert, duration);
        size_t size = result.size();
        jclass poiByDurationClass = env->FindClass("com/cherrypicks/pmpmap/core/VertexByDuration");
        jobjectArray ret = (jobjectArray)env->NewObjectArray(size,
                                                             poiByDurationClass,
                                                             NULL);
        jmethodID methodID = (*env).GetStaticMethodID(poiByDurationClass, "create", "(IF)Lcom/cherrypicks/pmpmap/core/VertexByDuration;");
        for(int i = 0; i < size; i++) {
            auto vertDur = result.at(i);
            WLOGD("vertDur.vertex->getId(): %d", vertDur.vertex->getId());
            WLOGD("vertDur.duration: %f", vertDur.duration);
            jobject jpoiByDuration = env->CallStaticObjectMethod(poiByDurationClass,
                                                                 methodID,
                                                                 vertDur.vertex->getId(),
                                                                 vertDur.duration);
            env->SetObjectArrayElement(
                    ret,i,jpoiByDuration);
            env->DeleteLocalRef(jpoiByDuration);
        }
        WLOGD("nativeFilterVertexByDuration: %d", size);
        return (ret);

    }

/*
 *

    private native void nativeSetRerouteThreshold(float threshold);
    private native float nativeGetRerouteThreshold();
 */

    JNIEXPORT void Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeSetDebug(JNIEnv* env, jobject thiz, jboolean debug) {
        CPA::CoreEngine::Config config  = CPA::CoreEngine::getInstance()->getConfig();
        config.debugEnable = debug;
        CPA::CoreEngine::getInstance()->setConfig(config);
    }

    JNIEXPORT jboolean Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeIsDebug(JNIEnv* env, jobject thiz) {
        CPA::CoreEngine::Config config  = CPA::CoreEngine::getInstance()->getConfig();
        return config.debugEnable;
    }
    JNIEXPORT void Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeSetDestinationThreshold(JNIEnv* env, jobject thiz, jfloat threshold) {
        CPA::CoreEngine::Config config  = CPA::CoreEngine::getInstance()->getConfig();
        config.destinationThreshold = threshold;
        CPA::CoreEngine::getInstance()->setConfig(config);
    }

    JNIEXPORT jfloat Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeGetDestinationThreshold(JNIEnv* env, jobject thiz) {
        CPA::CoreEngine::Config config  = CPA::CoreEngine::getInstance()->getConfig();
        return config.destinationThreshold;
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeSetBypassDestinationThreshold(JNIEnv* env, jobject thiz, jfloat threshold) {
        CPA::CoreEngine::Config config  = CPA::CoreEngine::getInstance()->getConfig();
        config.bypassDestinationThreshold = threshold;
        CPA::CoreEngine::getInstance()->setConfig(config);
    }

    JNIEXPORT jfloat Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeGetBypassDestinationThreshold(JNIEnv* env, jobject thiz) {
        CPA::CoreEngine::Config config  = CPA::CoreEngine::getInstance()->getConfig();
        return  config.bypassDestinationThreshold;
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeSetRerouteThreshold(JNIEnv* env, jobject thiz, jfloat threshold) {
        CPA::CoreEngine::Config config  = CPA::CoreEngine::getInstance()->getConfig();
        config.rerouteThreshold = threshold;
        CPA::CoreEngine::getInstance()->setConfig(config);
    }

    JNIEXPORT jfloat Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeGetRerouteThreshold(JNIEnv* env, jobject thiz) {
        CPA::CoreEngine::Config config  = CPA::CoreEngine::getInstance()->getConfig();
        return config.rerouteThreshold;
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeSetPathForeSeeingDistance(JNIEnv* env, jobject thiz, jfloat value) {
        CPA::CoreEngine::Config config  = CPA::CoreEngine::getInstance()->getConfig();
        config.pathForeSeeingDistance = value;
        CPA::CoreEngine::getInstance()->setConfig(config);
    }

    JNIEXPORT jobject Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeGetPathResult(JNIEnv* env, jobject thiz) {
        return nullptr;
    }

    JNIEXPORT jobject Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeGetRouteStepIndicators(JNIEnv* env, jobject thiz) {
        jclass cls = env->FindClass("com/cherrypicks/pmpmap/core/RouteStepIndicator");
        CPA::CoreEngine* coreEngine = CPA::CoreEngine::getInstance();
        auto steps = coreEngine->getRouteStepIndictors();
        int size = steps.size();
        jobjectArray ret = (jobjectArray)env->NewObjectArray(size,
                                                             cls,
                                                             NULL);
        jmethodID methodID = (*env).GetStaticMethodID(cls, "create", "(Ljava/lang/String;IFFFIII)Lcom/cherrypicks/pmpmap/core/RouteStepIndicator;");
        for(int i = 0; i < size; i++) {
            auto step = steps.at(i);
            jstring jfloorName = env->NewStringUTF(step.floorName.c_str());
            jobject obj = env->CallStaticObjectMethod(cls,
                                             methodID,
                                              jfloorName,
                                              step.pathType,
                                              step.position.x,
                                              step.position.y,
                                              step.position.z,
                                              step.mapId,
                                              step.nextMapId,
                                              step.pathId);
            env->SetObjectArrayElement(
                    ret,i,obj);
            env->DeleteLocalRef(obj);
            env->DeleteLocalRef(jfloorName);
        }
        WLOGD("nativeFilterVertexByDuration: %d", size);
        return (ret);
        return nullptr;
    }


    JNIEXPORT void Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeSetIsDisability(JNIEnv* env, jobject thiz, jboolean val) {
        CPA::CoreEngine::getInstance()->setFindPathForDisabled(val);
    }

    JNIEXPORT jboolean Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeIsDisability(JNIEnv* env, jobject thiz) {
        return CPA::CoreEngine::getInstance()->isFindPathForDisabled();
    }

    JNIEXPORT jboolean Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeCheckResultPathType(JNIEnv* env, jobject thiz, jint pathTypeID) {

        auto pathResult = CPA::CoreEngine::getInstance()->getPathResult();

        return pathResult.isIntersectPathType(pathTypeID);

    }

    JNIEXPORT jobject Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeCalculateDistanceFromNodeToNode(JNIEnv* env, jobject thiz, jint fromNodeId, jint toNodeId) {
        jclass cls = env->FindClass("com/pmp/mapsdk/external/PMPMapSDK$PathCalculationResult");
        jmethodID constructor = (*env).GetMethodID(cls, "<init>", "(II)V");

        std::vector<CPA::PathResult> results = CPA::CoreEngine::getInstance()->searchPath(fromNodeId, toNodeId, 1, false);
        if(results.size()){
            CPA::CoreEngine::PathCalculationResult calculationResult = CPA::CoreEngine::getInstance()->calculateNavigationTimeAndDistance(results.at(0), false, false);
            jobject result = (*env).NewObject(cls, constructor, (int)ceil(calculationResult.totalTime/60), (int)calculationResult.totalDistance);
            return result;
        }else{
            jobject result = (*env).NewObject(cls, constructor, 0, 0);
            return result;
        }


    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeSetInsideRegion(JNIEnv* env, jobject thiz, jboolean val) {
        CPA::CoreEngine::getInstance()->setInsideRegion(val);
    }
    JNIEXPORT jboolean Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeIsInsideRegion(JNIEnv* env, jobject thiz) {
        return CPA::CoreEngine::getInstance()->isInsideRegion();
    }
    JNIEXPORT jint Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeGetAreaIDByCoordinate(JNIEnv* env, jobject thiz, jint x, jint y) {
        CPA::Data::Area area = CPA::Utilities::getAreaByCoordinate(Vec2(x, y));
        return area.getId();
    }
    JNIEXPORT jint Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeGetCurrentNavigationStep(JNIEnv* env, jobject thiz) {
        return CPA::CoreEngine::getInstance()->getCurrentNavigationStep();
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeSetDirectionPredictionMode(JNIEnv* env, jobject thiz, jint mode) {
        CPA::CoreEngine::Config config  = CPA::CoreEngine::getInstance()->getConfig();
        config.stepPredictionMode = mode;
        CPA::CoreEngine::getInstance()->setConfig(config);
        }
    JNIEXPORT jboolean Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeIsInitialized(JNIEnv* env, jobject thiz) {
        return CPA::CoreEngine::getInstance()->isInitialized();
    }

    JNIEXPORT jstring Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeGetCurrentAreaName(JNIEnv* env, jobject thiz) {
        return env->NewStringUTF(CPA::CoreEngine::getInstance()->getCurrentAreaName().c_str());
    }

    JNIEXPORT jstring Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeGetCurrentMapName(JNIEnv* env, jobject thiz) {
        return env->NewStringUTF(CPA::CoreEngine::getInstance()->getCurrentMapName().c_str());
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeStartCalibration(JNIEnv* env, jobject thiz) {
        CPA::BearingCalculator::getInstance()->startCalibration();
    }

    JNIEXPORT jint Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeGetDeviceFacing(JNIEnv* env, jobject thiz) {
        return CPA::CoreEngine::getInstance()->getDeviceFacing();
    }

    JNIEXPORT void Java_com_cherrypicks_pmpmap_core_CoreEngine_nativeSetBlueDotMode(JNIEnv* env, jobject thiz, jint mode) {
        CPA::CoreEngine::UISetting setting = CPA::CoreEngine::getInstance()->getUISetting();
        setting.blueDotMode = static_cast<CPA::CoreEngine::BlueDotMode>(mode);
        CPA::CoreEngine::getInstance()->setUISetting(setting);
    }

}
