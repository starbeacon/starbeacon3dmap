//
//  ViewController.m
//  Starbeacon3DMapDemo
//
//  Created by gordonwong on 6/2/2018.
//  Copyright © 2018 Starberry Limited. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
@interface ViewController ()
@property BOOL isShowingUnityView;
@property UIViewController *unityViewController;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
     self.unityViewController = [((AppDelegate*)[UIApplication sharedApplication].delegate) getUnityViewController];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onShowUnity:(id)sender {

    if(!self.isShowingUnityView){
       self.unityViewController.view.frame = CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.height - 100);
    //show as subview
    [self.view addSubview:self.unityViewController.view];
    [self.view sendSubviewToBack:self.unityViewController.view];
    //push unity vc
//    [self.navigationController pushViewController:unityViewController animated:NO];
    
    //show as UIWindow
//    [((AppDelegate*)[UIApplication sharedApplication].delegate).unityWindow makeKeyAndVisible];
    }else{
        [self.unityViewController.view removeFromSuperview];
        
    }
    self.isShowingUnityView = !self.isShowingUnityView;
}

@end
