//
//  AppDelegate.h
//  Starbeacon3DMapDemo
//
//  Created by gordonwong on 6/2/2018.
//  Copyright © 2018 Starberry Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIWindow *unityWindow;
@property (strong, nonatomic) UnityAppController *unityController;

- (UIViewController*) getUnityViewController;
@end

